﻿using System.Data.Common;

namespace Etecno.Biblioteca.AccesoDatos
{
    public interface IDatabaseConnection
    {
        DbConnection Connection
        {
            get;
            set;
        }

        DbProviderFactory Factory
        {
            get;
            set;
        }

        void CloseConnection(object hashSign);

        void CommitTransaction(object hashSign);

        DbTransaction CreateTransaction(object hashSign);

        DbConnection GetConnection();

        DbConnection GetConnection(string connectionName);

        DbProviderFactory GetDbProviderFactory();

        DbProviderFactory GetDbProviderFactory(string factoryName);

        DbTransaction GetTransaction();

        void OpenConnection(object hashSign);

        void RollbackTransaction(object hashSign);
    }
}
