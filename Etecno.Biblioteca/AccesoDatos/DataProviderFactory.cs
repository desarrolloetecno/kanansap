﻿using Etecno.Biblioteca.Errores;
using System.Collections.Generic;
using System.Configuration;

namespace Etecno.Biblioteca.AccesoDatos
{
    public class DataProviderFactory
    {
        private Dictionary<string, IDataProvider> configurations;

        public DataProviderFactory()
        {
            this.LoadConfiguration();
        }

        public IDataProvider GetDataProvider(string connectionName)
        {
            if (!this.configurations.ContainsKey(connectionName))
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Biblioteca.DataAccess", "DataProviderFactory", "GetDataProvider", "La configuración Proporcionada no cuenta con los parametros necesarios");
            }
            return this.configurations[connectionName];
        }

        private void LoadConfiguration()
        {
            this.configurations = new Dictionary<string, IDataProvider>();
            foreach (ConnectionStringSettings connectionStringSettings in ConfigurationManager.ConnectionStrings)
            {
                DataProvider dataProvider = new DataProvider
                {
                    ConnectionName = connectionStringSettings.Name,
                    ConnectionString = connectionStringSettings.ConnectionString,
                    ProviderName = connectionStringSettings.ProviderName
                };
                if (ConfigurationManager.AppSettings[dataProvider.ConnectionName + "LikeSymbol"] == null)
                {
                    dataProvider.LikeSymbol = "%";
                }
                else
                {
                    dataProvider.LikeSymbol = ConfigurationManager.AppSettings[dataProvider.ConnectionName + "LikeSymbol"];
                }
                if (ConfigurationManager.AppSettings[dataProvider.ConnectionName + "DatabaseName"] != null)
                {
                    dataProvider.DatabaseName = ConfigurationManager.AppSettings[dataProvider.ConnectionName + "DatabaseName"];
                    if (ConfigurationManager.AppSettings[dataProvider.ConnectionName + "UserParameter"] != null)
                    {
                        dataProvider.UserParameter = ConfigurationManager.AppSettings[dataProvider.ConnectionName + "UserParameter"];
                    }
                    if (ConfigurationManager.AppSettings[dataProvider.ConnectionName + "PasswordParameter"] != null)
                    {
                        dataProvider.PasswordParameter = ConfigurationManager.AppSettings[dataProvider.ConnectionName + "PasswordParameter"];
                    }
                    if (ConfigurationManager.AppSettings[dataProvider.ConnectionName + "ParameterSymbol"] == null)
                    {
                        dataProvider.ParameterSymbol = "@";
                    }
                    else
                    {
                        dataProvider.ParameterSymbol = ConfigurationManager.AppSettings[dataProvider.ConnectionName + "ParameterSymbol"];
                    }
                    if (ConfigurationManager.AppSettings[dataProvider.ConnectionName + "NamedParameters"] == null)
                    {
                        dataProvider.NamedParameter = true;
                    }
                    else if (ConfigurationManager.AppSettings[dataProvider.ConnectionName + "NamedParameters"].ToLower().CompareTo("false") == 0)
                    {
                        dataProvider.NamedParameter = false;
                    }
                    else
                    {
                        dataProvider.NamedParameter = true;
                    }
                    this.configurations.Add(dataProvider.ConnectionName, dataProvider);
                }
            }
        }
    }
}