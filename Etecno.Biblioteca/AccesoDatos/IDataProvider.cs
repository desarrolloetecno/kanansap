﻿using System.Data.Common;

namespace Etecno.Biblioteca.AccesoDatos
{
    public interface IDataProvider
    {
        string ConnectionName
        {
            get;
            set;
        }

        string ConnectionString
        {
            get;
            set;
        }

        string DatabaseName
        {
            get;
            set;
        }

        string LikeSymbol
        {
            get;
            set;
        }

        bool NamedParameter
        {
            get;
            set;
        }

        string ParameterSymbol
        {
            get;
            set;
        }

        string PasswordParameter
        {
            get;
            set;
        }

        string ProviderName
        {
            get;
            set;
        }

        string UserParameter
        {
            get;
            set;
        }

        DbConnection CreateConnection();

        DbConnection CreateConnection(string user, string password);

        DbDataAdapter CreateDataAdapter();

        DbConnection CreateOpenedConnection(string user, string password);

        DbParameter CreateParameter();
    }
}
