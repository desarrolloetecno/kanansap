﻿using Etecno.Biblioteca.Errores;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace Etecno.Biblioteca.AccesoDatos
{
    public class DataContext : IDataContext
    {
        private IDataProvider dataProvider;

        private object signCon;

        private object signTrans;

        private DbConnection sqlCon;

        private DbTransaction sqlTrans;

        public ConnectionState ConnectionState
        {
            get
            {
                return this.sqlCon.State;
            }
        }

        public string LikeSymbol
        {
            get
            {
                return this.dataProvider.LikeSymbol;
            }
        }

        public string ParameterSymbol
        {
            get
            {
                return this.dataProvider.ParameterSymbol;
            }
        }

        public DataContext(IDataProvider dataProvider)
        {
            if (dataProvider == null)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Biblioteca.DataAccess", "DataContext", "DataContext", "El Proveedor de datos no puede ser nulo");
            }
            this.dataProvider = dataProvider;
            this.sqlCon = this.dataProvider.CreateConnection();
        }

        public void BeginTransaction(object sign)
        {
            if (this.sqlTrans == null)
            {
                this.sqlTrans = this.sqlCon.BeginTransaction();
                this.signTrans = sign;
            }
        }

        public void CloseConnection(object sign)
        {
            if (this.sqlCon != null && this.signCon == sign)
            {
                this.sqlCon.Close();
                this.signCon = null;
            }
        }

        public void CommitTransaction(object sign)
        {
            if (this.signTrans == sign && this.sqlTrans != null)
            {
                this.sqlTrans.Commit();
                this.signTrans = null;
            }
        }

        public DbCommand CreateCommand()
        {
            DbCommand dbCommand = this.sqlCon.CreateCommand();
            if (this.sqlTrans != null)
            {
                dbCommand.Transaction = this.sqlTrans;
            }
            return dbCommand;
        }

        public DbDataAdapter CreateDataAdapter()
        {
            return new SqlDataAdapter();
        }

        public void OpenConnection(object sign)
        {
            if (this.sqlCon.State != ConnectionState.Open)
            {
                this.signCon = sign;
                this.sqlCon.Open();
            }
        }

        public void RollbackTransaction(object sign)
        {
            if (this.signTrans == sign && this.sqlTrans != null)
            {
                this.sqlTrans.Rollback();
                this.signTrans = null;
            }
        }
    }
}
