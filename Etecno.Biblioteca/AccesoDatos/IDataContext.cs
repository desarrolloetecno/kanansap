﻿using System.Data;
using System.Data.Common;

namespace Etecno.Biblioteca.AccesoDatos
{
    public interface IDataContext
    {
        ConnectionState ConnectionState
        {
            get;
        }

        string LikeSymbol
        {
            get;
        }

        string ParameterSymbol
        {
            get;
        }

        void BeginTransaction(object sign);

        void CloseConnection(object sign);

        void CommitTransaction(object sign);

        DbCommand CreateCommand();

        DbDataAdapter CreateDataAdapter();

        void OpenConnection(object sign);

        void RollbackTransaction(object sign);
    }
}
