﻿using System.Configuration;
using System.Data;
using System.Data.Common;
using Etecno.Biblioteca.Errores;

namespace Etecno.Biblioteca.AccesoDatos
{
    public class DatabaseConnection : IDatabaseConnection
    {
        private DbConnection connection;

        private DbProviderFactory factory;

        private object signConnection;

        private object signTransaction;

        private DbTransaction transaction;

        public DbConnection Connection
        {
            get
            {
                return this.connection;
            }
            set
            {
                this.connection = value;
            }
        }

        public DbProviderFactory Factory
        {
            get
            {
                return this.factory;
            }
            set
            {
                this.factory = value;
            }
        }

        public void CloseConnection(object hashSign)
        {
            if (this.Connection == null)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Biblioteca.DataAccess", "DatabaseConnection", "CloseConnection", "No existe una conección para cerrar");
            }
            if (this.Connection.State != ConnectionState.Closed)
            {
                if (this.signConnection != hashSign)
                {
                    throw new ErrorEstandar(TipoError.ERROR, "Etecno.Biblioteca.DataAccess", "DatabaseConnection", "CloseConnection", "No se puede cerrar la conección porque las firmas son distintas.");
                }
                this.Connection.Close();
                this.signConnection = null;
            }
        }

        public void CommitTransaction(object hashSign)
        {
            if (this.Connection == null)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Biblioteca.DataAccess", "DatabaseConnection", "CommitTransaction", "No existe una conección para Activa.");
            }
            if (this.transaction == null)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Biblioteca.DataAccess", "DatabaseConnection", "CommitTransaction", "No existe una conección para Activa.");
            }
            if (this.signTransaction != hashSign)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Biblioteca.DataAccess", "DatabaseConnection", "CommitTransaction", "No es posible cerrar la transacción porque las firmas son distintas.");
            }
            this.transaction.Commit();
            this.transaction = null;
            this.signTransaction = null;
        }

        public DbTransaction CreateTransaction(object hashSign)
        {
            if (this.Connection == null)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Biblioteca.DataAccess", "DatabaseConnection", "CreateTransaction", "No existe una conección para Activa.");
            }
            if (this.transaction != null)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Biblioteca.DataAccess", "DatabaseConnection", "CreateTransaction", "Ya se encuentra abierta una transacción.");
            }
            this.signTransaction = hashSign;
            this.transaction = this.Connection.BeginTransaction();
            return this.transaction;
        }

        public DbConnection GetConnection()
        {
            if (this.Connection == null)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Biblioteca.DataAccess", "DatabaseConnection", "GetConnection", "No existe una conección para Activa.");
            }
            return this.Connection;
        }

        public DbConnection GetConnection(string conectionName)
        {
            ConnectionStringSettings connectionStringSettings = ConfigurationManager.ConnectionStrings[conectionName];
            this.Connection = this.GetDbProviderFactory(connectionStringSettings.ProviderName).CreateConnection();
            this.Connection.ConnectionString = ConfigurationManager.ConnectionStrings[conectionName].ConnectionString;
            return this.Connection;
        }

        public DbProviderFactory GetDbProviderFactory()
        {
            if (this.Factory == null)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Biblioteca.DataAccess", "DatabaseConnection", "GetDbProviderFactory", "No existe un proveedor para generar la Conección.");
            }
            return this.Factory;
        }

        public DbProviderFactory GetDbProviderFactory(string factoryName)
        {
            this.Factory = DbProviderFactories.GetFactory(factoryName);
            return this.Factory;
        }

        public DbTransaction GetTransaction()
        {
            return this.transaction;
        }

        public void OpenConnection(object hashSign)
        {
            if (this.Connection == null)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Biblioteca.DataAccess", "DatabaseConnection", "OpenConnection", "No se ha establecido alguna conección.");
            }
            if (this.Connection.State != ConnectionState.Open)
            {
                this.signConnection = hashSign;
                this.Connection.Open();
            }
        }

        public void RollbackTransaction(object hashSign)
        {
            if (this.Connection == null)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Biblioteca.DataAccess", "DatabaseConnection", "RollbackTransaction", "No se ha establecido alguna conección.");
            }
            if (this.transaction == null)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Biblioteca.DataAccess", "DatabaseConnection", "RollbackTransaction", "No se encontro alguna transacción abierta.");
            }
            if (this.signTransaction != hashSign)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Biblioteca.DataAccess", "DatabaseConnection", "RollbackTransaction", "No se revertir cambios porque las firmas son distintas.");
            }
            this.transaction.Rollback();
            this.transaction = null;
            this.signTransaction = null;
        }
    }
}
