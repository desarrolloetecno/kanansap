﻿using Etecno.Biblioteca.Errores;
using System.Data.Common;

namespace Etecno.Biblioteca.AccesoDatos
{
    public class DataProvider : IDataProvider
    {
        private string connectionName;

        private string connectionString;

        private string databaseName;

        private string likeSymbol;

        private bool namedParameter;

        private string parameterSymbol;

        private string providerName;

        private string userNameParameter;

        private string userPasswordParameter;

        public string ConnectionName
        {
            get
            {
                return this.connectionName;
            }
            set
            {
                this.connectionName = value;
            }
        }

        public string ConnectionString
        {
            get
            {
                return this.connectionString;
            }
            set
            {
                this.connectionString = value;
            }
        }

        public string DatabaseName
        {
            get
            {
                return this.databaseName;
            }
            set
            {
                this.databaseName = value;
            }
        }

        public string LikeSymbol
        {
            get
            {
                return this.likeSymbol;
            }
            set
            {
                this.likeSymbol = value;
            }
        }

        public bool NamedParameter
        {
            get
            {
                return this.namedParameter;
            }
            set
            {
                this.namedParameter = value;
            }
        }

        public string ParameterSymbol
        {
            get
            {
                return this.parameterSymbol;
            }
            set
            {
                this.parameterSymbol = value;
            }
        }

        public string PasswordParameter
        {
            get
            {
                return this.userPasswordParameter;
            }
            set
            {
                this.userPasswordParameter = value;
            }
        }

        public string ProviderName
        {
            get
            {
                return this.providerName;
            }
            set
            {
                this.providerName = value;
            }
        }

        public string UserParameter
        {
            get
            {
                return this.userNameParameter;
            }
            set
            {
                this.userNameParameter = value;
            }
        }

        public DbConnection CreateConnection()
        {
            DbConnection dbConnection = DbProviderFactories.GetFactory(this.ProviderName).CreateConnection();
            dbConnection.ConnectionString = this.ConnectionString;
            return dbConnection;
        }

        public DbConnection CreateConnection(string user, string password)
        {
            DbConnection dbConnection = DbProviderFactories.GetFactory(this.ProviderName).CreateConnection();
            string text = this.ConnectionString;
            if (this.userNameParameter == null || this.userPasswordParameter == null)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Biblioteca.DataAccess", "DataProvider", "CreateConnection", "La configuración Proporcionada no cuenta con los parametros necesarios");
            }
            if (user == null || password == null)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Biblioteca.DataAccess", "DataProvider", "CreateConnection", "El usuario y pasword no puede ser nulo");
            }
            dbConnection.ConnectionString = string.Concat(new string[]
			{
				text,
				";",
				this.userNameParameter,
				"=",
				user,
				";",
				this.userPasswordParameter,
				"=",
				password
			});
            return dbConnection;
        }

        public DbDataAdapter CreateDataAdapter()
        {
            return DbProviderFactories.GetFactory(this.ProviderName).CreateDataAdapter();
        }

        public DbConnection CreateOpenedConnection(string user, string password)
        {
            DbConnection dbConnection = DbProviderFactories.GetFactory(this.ProviderName).CreateConnection();
            string text = this.ConnectionString;
            if (this.userNameParameter == null || this.userPasswordParameter == null)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Biblioteca.DataAccess", "DataProvider", "CreateOpenedConnection", "La configuración Proporcionada no cuenta con los parametros necesarios");
            }
            if (user == null || password == null)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Biblioteca.DataAccess", "DataProvider", "CreateOpenedConnection", "El usuario y password no puede ser nulo");
            }
            dbConnection.ConnectionString = string.Concat(new string[]
			{
				text,
				";",
				this.userNameParameter,
				"=",
				user,
				";",
				this.userPasswordParameter,
				"=",
				password
			});
            dbConnection.Open();
            return dbConnection;
        }

        public DbParameter CreateParameter()
        {
            return DbProviderFactories.GetFactory(this.ProviderName).CreateParameter();
        }
    }
}
