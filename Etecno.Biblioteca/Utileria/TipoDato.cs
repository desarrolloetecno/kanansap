﻿using System;

namespace Etecno.Biblioteca.Utileria
{
    public enum TipoDato
    {
        ENTERO,
        DECIMAL,
        CADENA,
        FECHA,
        BOLEANO,
        HORA,
        BINARIO
    }
}
