﻿using System;
using System.Data;
using System.Data.Common;

namespace Etecno.Biblioteca.Utileria
{
    public class DBHelper
    {
        public string Marca
        {
            get;
            set;
        }

        public DbCommand Comm
        {
            get;
            set;
        }

        public void CrearParametro(TipoDato tipoDato, string nombre, object valor)
        {
            DbParameter dbParameter = this.Comm.CreateParameter();
            dbParameter.ParameterName = this.Marca + nombre;
            switch (tipoDato)
            {
                case TipoDato.ENTERO:
                    dbParameter.DbType = DbType.Int32;
                    break;
                case TipoDato.DECIMAL:
                    dbParameter.DbType = DbType.Decimal;
                    break;
                case TipoDato.CADENA:
                    dbParameter.DbType = DbType.String;
                    break;
                case TipoDato.FECHA:
                    dbParameter.DbType = DbType.Date;
                    break;
                case TipoDato.BOLEANO:
                    dbParameter.DbType = DbType.Boolean;
                    break;
                case TipoDato.HORA:
                    dbParameter.DbType = DbType.Time;
                    break;
                case TipoDato.BINARIO:
                    dbParameter.DbType = DbType.Binary;
                    break;
            }
            if (valor == null)
            {
                dbParameter.Value = DBNull.Value;
            }
            else
            {
                dbParameter.Value = valor;
            }
            this.Comm.Parameters.Add(dbParameter);
        }
    }
}
