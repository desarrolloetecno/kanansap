﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Security.Cryptography;
/*Referencias*/
using Kanan.Llantas.BO2;
using Newtonsoft.Json;

namespace KananWS.Interface
{
    public class IncidenteLlantaWS
    {
        /*Comentarios
         * Clase encargada de consumir el servicio web que visualiza/actualiza
         * los registros de la tabla IncidenteLlanta.
         * 
         * Development by: ing. Rair Santos.
         */
        #region Atributos
        static HttpClient client = new HttpClient();
        private Guid PublicKey;
        private string PrivateKey;
        #endregion Atributos

        #region DTO

        [Serializable]
        [DataContract]
        public class IncidenteDTO
        {
            [DataMember]
            public int? IncidenteID { get; set; }
            [DataMember]
            public string Accion { get; set; }
            [DataMember]
            public string Ubicacion { get; set; }
            [DataMember]
            public string Observaciones { get; set; }
            [DataMember]
            public ILlanta Llantable { get; set; }
            [DataMember]
            public Llanta Neumatico { get; set; }
            [DataMember]
            public DateTime? Fecha { get; set; }
            [DataMember]
            public DateTime? FechaCaptura { get; set; }
            [DataMember]
            public string GrosorCapa { get; set; }
            [DataMember]
            public double? DistanciaRecorrida { get; set; }
            [DataMember]
            public int? ILlantaID { get; set; }
            [DataMember]
            public int? TipoILlanta { get; set; }
            [DataMember]
            public string Referencia { get; set; }
            [DataMember]
            public int? Sincronizado { get; set; }
        }

        #endregion DTO

        #region Constructor
        public IncidenteLlantaWS(Guid publicKey, String privateKey)
        {
            if (client.BaseAddress == null)
                client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.AppSettings.Get("WS_URL"));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            this.PublicKey = publicKey;
            this.PrivateKey = privateKey;
        }
        #endregion Constructor

        #region Acciones
        /*Comentarios
         * Método encargado de traer todas los registros encontrados
         * en IncidenteLlanta filtradas por EmpresaID en Llantas.
         * 
         * Development by: ing. Rair Santos.
         */
        public List<IncidenteLlanta> GetOutOfPhase(int? EmpresaID)
        {
            List<IncidenteLlanta> list = new List<IncidenteLlanta>();
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                HttpResponseMessage resp = client.GetAsync("api/IncidenteLlanta/GetOutOfPhase/" + EmpresaID).Result;
                if (resp.IsSuccessStatusCode)
                {
                    list = resp.Content.ReadAsAsync<List<IncidenteLlanta>>().Result;
                    return list;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("GetOutOfPhase - Error: Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                //Comentado para seguir la estructura de control de excepciones.
                //throw new Exception("Error: " + ex.Message + "Clase: IncidenteLLantaWS.cs");
                throw new Exception(string.Format("GetOutOfPhase - Error: {0}", ex.Message));
            }
        }

        /*Comentarios
         * Clase encargada de actualizar un registro procesado por KananSAP
         * la tabla afectada es IncidenteLlanta.
         * 
         * Development by: ing. Rair Santos.
         */
        public IncidenteLlanta UpdateSAP(IncidenteLlanta inllanta)
        {
            IncidenteLlanta IncidenteLlanta = new IncidenteLlanta();
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                HttpResponseMessage resp = client.PostAsJsonAsync("api/IncidenteLlanta/UpdateSAP/", inllanta).Result;
                if (resp.IsSuccessStatusCode)
                {
                    IncidenteLlanta = resp.Content.ReadAsAsync<IncidenteLlanta>().Result;
                    return IncidenteLlanta;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("UpdateSAP - Error: Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch ( Exception ex)
            {
                //Comentado para seguir la estructura de control de excepciones.
                //throw new Exception("Error: " + ex.Message + "Clase: IncidenteLLantaWS.cs");
                throw new Exception(string.Format("UpdateSAP - Error: {0}", ex.Message));
            }
        }
        #endregion Acciones

        #region Métodos auxiliares
        /* Comentarios
         * Clase encargada de crear el token de seguridad
         * para los webservice.
         */
        private string CreateToken(string message, string secret)
        {
            secret = secret ?? "";
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }
        #endregion Métodos auxiliares
    }
}
