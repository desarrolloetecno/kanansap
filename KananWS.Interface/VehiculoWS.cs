﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Kanan.Vehiculos.BO2;
using Newtonsoft.Json;

namespace KananWS.Interface
{
    public class VehiculoWS
    {

        public class OdometroActivo
        {
            #region Comentarios
            /*-------------------Descripción-------------------
             * No se establecen valores nulables, debido a que
             * al ser una consulta que devuelve la interseccion
             * de IncidenteCaja con HistoricoDesplazamiento en
             * los valores CajaID VehiculosID IncidenteCaja con
             * Vehiculo en DesplazamientoHistorico; por lo tanto
             * no retorna valores null en ningún campo.
             */
            #endregion Comentarios

            public int CajaID { get; set; }
            public int VehiculoID { get; set; }
            public DateTime FechaIncidente { get; set; }
            public String Accion { get; set; }
            public double Distancia { get; set; }
            public DateTime FechaDesplazamiento { get; set; }
        }

        #region Atributos
        static HttpClient client = new HttpClient();
        private Guid PublicKey;
        private string PrivateKey;
        #endregion

        #region Constructor
        public VehiculoWS(Guid publicKey, String privatekey)
        //public VehiculoWS()
        {
            if (client.BaseAddress == null)
                client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.AppSettings.Get("WS_URL"));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            this.PublicKey = publicKey;
            this.PrivateKey = privatekey;
        }
        #endregion

        #region Metodos

        #region Vehiculos

        #region AddVehiculo
        public Vehiculo Add(Vehiculo vehiculo)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/Vehiculo/Add", vehiculo).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Vehiculo>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                //throw new Exception(string.Format("AddVehicle - Error: {0}", ex.Message));
                return null;

            }
        }
        #endregion AddVehiculo

        #region UpdateVehiculo
        public Vehiculo Update(Vehiculo vehiculo)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/Vehiculo/Update", vehiculo).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Vehiculo>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("UpdateVehicle - Error: {0}", ex.Message));
            }
        }
        #endregion UpdateVehiculo

        #region DeleteVehiculo
        public Vehiculo Delete(int id)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.GetAsync("api/Vehiculo/DeleteVehiculo/" + id).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Vehiculo>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("DeleteVehicle - Error: {0}", ex.Message));
            }
        }
        #endregion DeleteVehiculo

        #region GetAllVehiculos
        //public List<Vehiculo> GetAllVehiculos()
        //{
        //    //var customObject = new CustomClass() { Name = "John", Id = 333 };
        //    var serialized = JsonConvert.SerializeObject(string.Empty);
        //    //HttpContent httpContent = new StringContent(serialized, Encoding.UTF8, "application/json");
        //    HttpContent httpContent = new StringContent("", Encoding.UTF8, "application/json");
        //    try
        //    {
        //        //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
        //        HttpResponseMessage resp = client.PostAsync(string.Format("{0}/GetAllVehicles", urlVehiculo), httpContent).Result;
        //        //This method throws an exception if the HTTP response status is an error code.  
        //        //resp.EnsureSuccessStatusCode();
        //        if (resp.IsSuccessStatusCode)
        //        {
        //            //var resultado = resp.Content.ReadAsAsync<string>();
        //            var resultado = resp.Content.ReadAsStringAsync();
        //            var oJson = JsonConvert.DeserializeObject<RootObject>(resultado.Result);
        //            var listavehiculos = JsonConvert.DeserializeObject<List<Vehiculo>>(oJson.d);
        //            return listavehiculos;
        //        }
        //        else
        //        {
        //            var resultado = resp.Content.ReadAsStringAsync().Result;
        //            var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
        //            throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //       throw new Exception(ex.Message);
        //    }
        //}
        #endregion GetAllVehiculos

        #region GetByID
        public Vehiculo GetByID(int vehiculoId)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.GetAsync("api/Vehiculo/GetByID/" + vehiculoId).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Vehiculo>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GetByID - Error: {0}", ex.Message));
            }
        }
        #endregion GetByID

        #region GetByIDTypes
        public List<TipoVehiculo> GetVehicleTypes(int empresaid)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                //HttpResponseMessage resp = client.PostAsync(string.Format("{0}/GetByID", urlVehiculo),httpContent).Result;
                HttpResponseMessage resp = client.GetAsync("api/Vehiculo/GetAllTypeVehicles/" + empresaid).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var ojson = JsonConvert.DeserializeObject(resultado);
                    var tipos = JsonConvert.DeserializeObject<List<TipoVehiculo>>(ojson.ToString());
                    return tipos;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(result.Message + " - " + result.ExceptionMessage);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GetVehiclesTypes - Error: {0}", ex.Message));
            }
        }
        #endregion GetByIDTypes

        #region GetOdometro
        public string GetOdometro(int VehiculoID)
        {
            try
            {
                string var;
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                HttpResponseMessage resp = client.GetAsync("/api/Vehiculo/GetOdometro/" + VehiculoID).Result;
                if (resp.IsSuccessStatusCode)
                {
                    var = resp.Content.ReadAsStringAsync().Result;
                    return var;
                }
                else
                {
                    var = null;
                    throw new Exception("No se pudo obtener el odómetro");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GetOdometroVehiculo - Error: {0}", ex.Message));
            }
        }
        #endregion GetOdometro

        #region GetEstatus
        public string GetEstatus(int VehiculoID)
        {
            try
            {
                string var;
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                HttpResponseMessage resp = client.GetAsync("/api/Vehiculo/GetEstatus/" + VehiculoID).Result;
                if (resp.IsSuccessStatusCode)
                {
                    var = resp.Content.ReadAsStringAsync().Result;
                    var ojson = JsonConvert.DeserializeObject(var);
                    return ojson.ToString();
                }
                else
                {
                    var = null;
                    throw new Exception("No se pudo obtener el estatus del vehículo");
                }

                return var;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GetEstatusVehiculo - Error: {0}", ex.Message));
            }
        }
        #endregion GetEstatus

        #region AsignarSucursal
        public bool AsignarSucursal(Vehiculo vehiculo)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                HttpResponseMessage resp = client.PostAsJsonAsync("api/Vehiculo/SetSucursalToVehicle", vehiculo).Result;
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = Convert.ToBoolean(resp.Content.ReadAsStringAsync().Result);
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("AsignarSucursal - Error: {0}", ex.Message));
            }
        }
        #endregion AsignarSucursal

        #endregion Vehiculos

        #region Activos

        #region GetOdometroActivo
        public string GetOdometroActivo(int CajaID)
        {
            try
            {
                String kilometraje = String.Empty;
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                HttpResponseMessage resp = client.GetAsync("/api/Vehiculo/GetOdometroActivo/" + CajaID).Result;
                if (resp.IsSuccessStatusCode)
                {
                    kilometraje = resp.Content.ReadAsStringAsync().Result;
                    return kilometraje;
                }
                else
                {
                    kilometraje = null;
                    throw new Exception("No se pudo obtener el odómetro");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GetOdometroActivo - Error: {0}", ex.Message));
            }
        }
        #endregion GetOdometroActivo

        #region CalcularOdometro
        /*public double CalcularOdometro()
        {
            OdometroActivo odometro = new OdometroActivo();
            List<OdometroActivo> lista = new List<OdometroActivo>();
            double kilometraje = 0.0;
            int sig;
            for (int i = 0; i < lista.Count; i++)
            {*/
        #region Descripción
        /*--------------------Descripción--------------------
                     * Se verifican los límites para contabilizar el
                     * odómetro en caso de existir ambos límites, en caso
                     * de no existir la fecha final (fecha incial siempre
                     * existe porque las cajas no se mueven solas) entonces
                     * se consulta el rango inicial con el valor más cercano
                     * a la fecha actual en DesplazamientoHistorico y se
                     * suma a los valores actuales para obtener el kilometraje
                     */
        #endregion Descripción

        /*if ((i + 1) < (lista.Count)) //Se valida que "sig" no sea mayor al número de la lista.
                {
                    sig = i + 1;
                    if (lista[i].VehiculoID == lista[sig].VehiculoID) //Se valida los límites pertenescan al mismo vehículo.
                    {
                        if (lista[i].Accion != lista[sig].Accion) //Se valida que no sea la misma acción para identificar que sea el límite inicio y fin.
                            kilometraje += lista[sig].Distancia - lista[i].Distancia;
                        else
                            throw new Exception("Caja se instaló mas de una vez sin desinstalarse antes de otro vehículo. Inconsistencia de datos. ");
                    }
                    else // No existe límite final. Se consulta kilimetraje actual en caso de seguir enganchada la caja con el vehículo.
                    {
                        String DistanciaActual = String.Empty;
                        DistanciaActual = String.Format("select Distancia from DesplazamientoHistorico  where Vehiculo = '{0}' and Fecha between '{1}' and '{2}'", lista[i].VehiculoID, lista[i].FechaIncidente, DateTime.UtcNow);

                        kilometraje += Convert.ToDouble(DistanciaActual);
                    }
                }
            }
            return kilometraje;
        }*/
        #endregion CalcularOdometro

        #region GetEstatus
        public String GetEstatusActivo(int ActivoID)
        {
            try
            {
                //string var;
                //client.DefaultRequestHeaders.Clear();
                //string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                //client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                //client.DefaultRequestHeaders.Add("X_TIME", date);
                //client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                //HttpResponseMessage resp = client.PostAsJsonAsync("/api/Vehiculo/GetEstatusActivo/", ActivoID).Result;
                //if (resp.IsSuccessStatusCode)
                //{
                //    var = resp.Content.ReadAsStringAsync().Result;
                //    var ojson = JsonConvert.DeserializeObject(var);
                //    return ojson.ToString();
                //}
                //else
                //{
                //    var = null;
                //    throw new Exception("No se pudo obtener el estatus del Activo. ");
                //}
                //return var;
                return "Rentado";
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("AddAsset - Error: {0}", ex.Message));
            }
        }
        #endregion GetEstatus

        #region AddActivo
        public Caja AddActivo(Caja activo)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/Vehiculo/AddActivo", activo).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Caja>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("AddAsset - Error: {0}", ex.Message));
            }
        }
        #endregion AddActivo

        #region UpdateActivo
        public Caja UpdateActivo(Caja activo)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/Vehiculo/UpdateActivo", activo).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Caja>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("UpdateAsset - Error: {0}", ex.Message));
            }
        }
        #endregion UpdateActivo

        #region DeleteActivo
        public Caja DeleteActivo(int id)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.GetAsync("api/Vehiculo/DeleteActivo/" + id).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Caja>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("DeleteAsset - Error: {0}", ex.Message));
            }
        }
        #endregion DeleteActivo

        #endregion Activos

        #region Metodos Auxiliares
        private string CreateToken(string message, string secret)
        {
            secret = secret ?? "";
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }
        #endregion

        #endregion
    }
}