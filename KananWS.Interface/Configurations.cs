﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kanan.Comun.BO2;
using Kanan.Costos.BO2;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;

namespace KananWS.Interface
{
    public class Configurations
    {
        public VehicleInfo Info { get; set; }
        /// <summary>
        /// Objeto que contiene la informacion del usuario, empleado, empresa y sucursal
        /// </summary>
        public Empleado UserFull { get; set; }
        /// <summary>
        /// Lista de los proveedores para la recarga de gasolina
        /// </summary>
        public List<Proveedor> Proveedores { get; set; }
        /// <summary>
        /// Lista de tipos de gasolina
        /// </summary>
        public List<TiposCombustible> TiposCombustible { get; set; }
        /// <summary>
        /// Lista de tipos de medicion de longitud (Kms, Millas)
        /// </summary>
        public List<UnidadLongitud> TiposLongitud { get; set; }
        /// <summary>
        /// Lista de tipos de medicion de volumen (Lts, Yrds)
        /// </summary>
        public List<UnidadVolumen> TiposVolumen { get; set; }
        /// <summary>
        /// Lista de los tipos de pagos de la carga de combustible
        /// </summary>
        public List<TipoDePago> TiposPago { get; set; }

        public List<TipoMoneda> TiposMoneda { get; set; }
        /// <summary>
        /// Lista del vehiculo asignado
        /// </summary>
        public List<Vehiculo> ListVehiculos { get; set; }

    }

    public class TiposCombustible
    {
        public int? ID { get; set; }
        public string Nombre { get; set; }
    }

    public class VehicleInfo
    {
        public double? Odometro { get; set; }
        public double? Rendimiento { get; set; }
        public decimal? Costo { get; set; }
    }
}
