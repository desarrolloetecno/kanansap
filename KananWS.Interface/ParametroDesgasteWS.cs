﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using Kanan.Mantenimiento.BO2;
using Newtonsoft.Json;

using Kanan.Operaciones.BO2;
using Kanan.Llantas.BO2;

namespace KananWS.Interface
{
    public class ParametroDesgasteWS
    {
        #region Atributos
        static HttpClient client = new HttpClient();
        private Guid PublicKey;
        private string PrivateKey;
        #endregion

        #region Constructor
        public ParametroDesgasteWS(Guid publicKey, String privatekey)
        {
            if (client.BaseAddress == null)
                client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.AppSettings.Get("WS_URL"));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            this.PublicKey = publicKey;
            this.PrivateKey = privatekey;
        }
        #endregion

        #region DTO
        public class ParametroDesgasteDTO
        {
            public int? PmDesID { get; set; }
            public int? EmpresaID { get; set; }
            public int? SucursalID { get; set; }
            public int? Uso { get; set; }
            public int? LlantaID  { get; set; }
            public double? LimPorc { get; set; }
            public double? LimProf { get; set; }
            public double? AvisoLimPorc { get; set; }
            public double? AvisoLimProf { get; set; }
            public double? TiempActTread { get; set; }
            public int? Sincronizado { get; set; }
        }
        #endregion DTO

        #region Acciones

        #region Add
        public ParametroDesgaste Add(ParametroDesgaste ParametroDesgaste)
        {
            try
            {
                //Se migran los datos a una clase plana.
                ParametroDesgasteDTO data = new ParametroDesgasteDTO();
                data = ParametroDesgasteToDTO(ParametroDesgaste);

                if (data.Uso <= 0 & data.Uso != null)
                    data.Uso = null;
                if (data.LlantaID <= 0 & data.LlantaID != null)
                    data.LlantaID = null;

                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                HttpResponseMessage resp = client.PostAsJsonAsync("api/PMTRODESGASTE/Add", data).Result;

                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ParametroDesgasteDTO>(resultado);
                    ParametroDesgaste PmtroDesg = new ParametroDesgaste();
                    PmtroDesg = DTOToParametroDesgaste(result);
                    return PmtroDesg;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1} ", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("AddParametroDesgaste - Error: {0}", ex.Message));
            }
        }
        #endregion Add

        #region Update
        public ParametroDesgaste Update(ParametroDesgaste ParametroDesgaste)
        {
            try
            {
                //Se migran los datos a una clase plana.
                ParametroDesgasteDTO data = new ParametroDesgasteDTO();
                data = ParametroDesgasteToDTO(ParametroDesgaste);

                if (data.Uso <= 0 & data.Uso != null)
                    data.Uso = null;
                if (data.LlantaID <= 0 & data.LlantaID != null)
                    data.LlantaID = null;

                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                HttpResponseMessage resp = client.PostAsJsonAsync("api/PMTRODESGASTE/Update", data).Result;

                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ParametroDesgasteDTO>(resultado);
                    ParametroDesgaste PmtroDesg = new ParametroDesgaste();
                    PmtroDesg = DTOToParametroDesgaste(result);
                    return PmtroDesg;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1} ", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("UpdateParametroDesgaste - Error: {0}", ex.Message));
            }
        }
        #endregion Update

        #region Delete
        public bool Delete(int ParametroDesgasteID)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                HttpResponseMessage resp = client.PostAsJsonAsync("api/PMTRODESGASTE/Delete", ParametroDesgasteID).Result;

                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<ParametroDesgaste>().Result;
                    return true;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    return false;
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1} ", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("DeleteParametroDesgaste - Error: {0}", ex.Message));
            }
        }
        #endregion Delete

        #region GetByEmpresa
        public List<ParametroDesgaste> GetByEmpresaID(int EmpresaID)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                HttpResponseMessage resp = client.PostAsJsonAsync("api/PMTRODESGASTE/GetByEmpresa", EmpresaID).Result;

                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<IEnumerable<ParametroDesgaste>>().Result;
                    return resultado.ToList();

                    //Terminar en caso de marcar exception con return Resultado.ToList();

                    /*var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<IEnumerable<ParametroDesgasteDTO>>(resultado);
                    List<ParametroDesgaste> PmtroDesg = new List<ParametroDesgaste>();
                    PmtroDesg = DTOToParametroDesgasteList(result);
                    return PmtroDesg;*/
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1} ", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GetByEmpresaID - Error: {0}", ex.Message));
            }
        }
        #endregion GetByEmpresa

        #region GetByLlanta
        public List<ParametroDesgaste> GetByLlantaID(int LlantaID)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                HttpResponseMessage resp = client.PostAsJsonAsync("api/PMTRODESGASTE/GetByLlanta", LlantaID).Result;

                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<IEnumerable<ParametroDesgaste>>().Result;
                    return resultado.ToList();
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1} ", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GetByLlantaID - Error: {0}", ex.Message));
            }
        }
        #endregion GetByEmpresa

        #endregion Acciones

        #region Metodos Auxiliares
        private string CreateToken(string message, string secret)
        {
            secret = secret ?? "";
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }

        private ParametroDesgaste DTOToParametroDesgaste (ParametroDesgasteDTO temporal)
        {
            ParametroDesgaste ParametroDesgasteTemporal = new ParametroDesgaste() 
            { Empresa = new Empresa(), Sucursal = new Sucursal(), Llanta = new Llanta()};

            ParametroDesgasteTemporal.ParametroDesgasteID = temporal.PmDesID;
            ParametroDesgasteTemporal.Empresa.EmpresaID = temporal.EmpresaID;
            ParametroDesgasteTemporal.Sucursal.SucursalID = temporal.SucursalID;
            ParametroDesgasteTemporal.Uso = temporal.Uso;
            ParametroDesgasteTemporal.Llanta.LlantaID = temporal.LlantaID;
            ParametroDesgasteTemporal.LimPorc = temporal.LimPorc;
            ParametroDesgasteTemporal.LimProf = temporal.LimProf;
            ParametroDesgasteTemporal.AvisoLimPorc = temporal.AvisoLimPorc;
            ParametroDesgasteTemporal.AvisoLimProf = temporal.AvisoLimProf;
            ParametroDesgasteTemporal.TiempActTread = temporal.TiempActTread;
            ParametroDesgasteTemporal.Sincronizado = temporal.Sincronizado;

            return ParametroDesgasteTemporal;
        }

        private List<ParametroDesgaste> DTOToParametroDesgasteList (List<ParametroDesgasteDTO> temporal)
        {
            try
            {
                List<ParametroDesgaste> Lista = new List<ParametroDesgaste>();

                for (int i = 0; i < temporal.Count(); i++)
                {
                    Lista.Add(DTOToParametroDesgaste(temporal[i]));
                }

                return Lista;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar migrar información retornada de un servicio a una lista. " + ex.Message);
            }
        }

        private ParametroDesgasteDTO ParametroDesgasteToDTO(ParametroDesgaste Temporal)
        {
            ParametroDesgasteDTO ParametroDesgasteDTO = new ParametroDesgasteDTO();

            ParametroDesgasteDTO.PmDesID = Temporal.ParametroDesgasteID;
            ParametroDesgasteDTO.EmpresaID = Temporal.Empresa.EmpresaID;
            ParametroDesgasteDTO.SucursalID = Temporal.Sucursal.SucursalID;
            ParametroDesgasteDTO.Uso = Temporal.Uso;
            ParametroDesgasteDTO.LlantaID = Temporal.Llanta.LlantaID;
            ParametroDesgasteDTO.LimPorc = Temporal.LimPorc;
            ParametroDesgasteDTO.LimProf = Temporal.LimProf;
            ParametroDesgasteDTO.AvisoLimPorc = Temporal.AvisoLimPorc;
            ParametroDesgasteDTO.AvisoLimProf = Temporal.AvisoLimProf;
            ParametroDesgasteDTO.TiempActTread = Temporal.TiempActTread;
            ParametroDesgasteDTO.Sincronizado = Temporal.Sincronizado == null ? 0 : Temporal.Sincronizado;
            
            return ParametroDesgasteDTO;
        }

        #endregion
    }
}
