﻿using Kanan.Comun.BO2;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace KananWS.Interface
{
    public class NotificacionesWS
    {
        #region NotificacionDto

        public class NotificacionDto
        {
            /// <summary>
            /// Objeto Notificacion para la configuración del envio de alertas
            /// </summary>
            public Notificacion Notification { get; set; }
            /// <summary>
            /// INotificable, puede ser por: Vehiculo = 1, Empresa = 2, Sucursal = 3
            /// </summary>
            public int? TipoNotificableID { get; set; }
            /// <summary>
            /// ID Notificador: VehiculoID, EmpresaID o SucursalID
            /// </summary>
            public int? NotificableID { get; set; }
        }

        #endregion

        #region CONSTANTES
        private static string urlServicio = System.Configuration.ConfigurationManager.AppSettings.Get("WS_URL"); //"http://localhost:51104/";
        #region NOTIFICACION
        private const string GET_NOTIFICATION = "api/Mantenimiento/GetNotificacion";
        private const string INSERT = "api/Mantenimiento/AddNotificacion";
        private const string UPDATE = "api/Mantenimiento/UpdateNotificacion";
        #endregion

        #region VEHICULO
        private const string GET_VEHICLE_BY_EMPRESAID = "api/Vehiculo/GetByEmpresaID/";
        #endregion
        
        #endregion

        #region Variables
        static HttpClient client = new HttpClient();
        private Guid PublicKey;
        private string PrivateKey;
        #endregion

        #region Contructor
        public NotificacionesWS(Guid publicKey, String privatekey)
        {
            if (client.BaseAddress == null)
                client.BaseAddress = new Uri(urlServicio);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            this.PublicKey = publicKey;
            this.PrivateKey = privatekey;
        }
        #endregion

        #region METODOS AUXILIARES
        private string CreateToken(string message, string secret)
        {
            secret = secret ?? "";
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }
        /// <summary>
        /// Agrega la clave publica, la fecha y el token al header
        /// </summary>
        private void AddHeaders()
        {
            string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
            client.DefaultRequestHeaders.Add("X_TIME", date);
            client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
        }

        /// <summary>
        /// Obtiene el ID del Nofiticador: 1 = Vehiculo, 2 = Empresa, 3 = Sucursal
        /// </summary>
        /// <param name="type">Notificacion.GetType()</param>
        /// <returns></returns>
        private int GetTipoNotificadorID(Type type)
        {
            try
            {
                int tipo = 0;
                if (type == typeof(Vehiculo))
                    tipo = 1;
                else if (type == typeof(Empresa))
                    tipo = 2;
                else if (type == typeof(Sucursal))
                    tipo = 3;
                if (tipo == 0)
                    throw new Exception("El Notificador proporcionado no existe. Verifique su información.");
                return tipo;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private Notificable GetTypeNotificador(int? tipoNotificadorID)
        {
            try
            {
                switch(tipoNotificadorID)
                {
                    case 1:
                        return new Vehiculo();
                    case 2:
                        return new Empresa();
                    case 3:
                        return new Sucursal();
                    default:
                        throw new Exception("El tipoNotificadorID proporcionado no existe. Verifique su informacón.");
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        /// <summary>
        /// Obtiene la lista de vehiculos de KananFleet
        /// </summary>
        /// <param name="empresaID"></param>
        /// <returns></returns>
        public List<Vehiculo> GetListaVehiculos(int? empresaID)
        {
            try
            {
                if (!empresaID.HasValue || empresaID <= 0)
                    throw new Exception("Proporcione un identificador válido");
                this.AddHeaders();
                HttpResponseMessage resp = client.GetAsync(GET_VEHICLE_BY_EMPRESAID + empresaID).Result;
                if (resp.IsSuccessStatusCode)
                {
                    var result = resp.Content.ReadAsAsync<List<Vehiculo>>().Result;
                    return result;
                }
                else
                {
                    var result = resp.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", res.Message, res.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GetListaVehiculos - Error: {0}", ex.Message));
            }
        }
        
        /// <summary>
        /// Obtiene los datos de las notificaciones de KananFleet
        /// </summary>
        /// <param name="idNotificador">VehiculoID o SucursalID o EmpresaID</param>
        /// <returns></returns>
        public Notificacion GetNotificacion(Notificacion nt)
        {
            try
            {
                if (nt == null)
                    throw new Exception("Notificacion no puede ser nulo");
                if(nt.Notificador == null)
                    throw new Exception("Notificacion no puede ser nulo");
                if(nt.Notificador.NotificableID == null)
                    throw new Exception("NotificableID no puede ser nulo");
                this.AddHeaders();
                NotificacionDto dto = new NotificacionDto();
                dto.Notification = nt;
                dto.NotificableID = nt.Notificador.NotificableID;
                dto.TipoNotificableID = this.GetTipoNotificadorID(nt.Notificador.GetType());
                dto.Notification.Notificador = null;
                HttpResponseMessage response = client.PostAsJsonAsync(GET_NOTIFICATION, dto).Result;
                if(response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsAsync<NotificacionDto>().Result;
                    result.Notification.Notificador = this.GetTypeNotificador(result.TipoNotificableID);
                    result.Notification.Notificador.NotificableID = result.NotificableID;
                    return result.Notification;
                }
                else
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", res.Message, res.ExceptionMessage));
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Notificacion Insertar(Notificacion nt)
        {
            try
            {
                if (nt == null)
                    throw new Exception("Notificacion no puede ser nulo");
                if(nt.Notificador == null)
                    throw new Exception("Notificador no puede ser nulo");
                if (nt.Notificador.NotificableID == null)
                    throw new Exception("NotificableID no puede ser nulo");
                this.AddHeaders();
                NotificacionDto dto = new NotificacionDto();
                dto.Notification = nt;
                dto.TipoNotificableID = this.GetTipoNotificadorID(nt.Notificador.GetType());
                dto.NotificableID = nt.Notificador.NotificableID;
                dto.Notification.Notificador = null;
                HttpResponseMessage response = client.PostAsJsonAsync(INSERT, dto).Result;
                if(response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsAsync<NotificacionDto>().Result;
                    result.Notification.Notificador = this.GetTypeNotificador(result.TipoNotificableID);
                    result.Notification.Notificador.NotificableID = result.NotificableID;
                    return result.Notification;
                }
                else
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", res.Message, res.ExceptionMessage));
                }

            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Notificacion Actualizar(Notificacion nt)
        {
            try
            {
                if (nt == null)
                    throw new Exception("Notificacion no puede ser nulo");
                if (nt.Notificador == null)
                    throw new Exception("Notificador no puede ser nulo");
                if (nt.Notificador.NotificableID == null)
                    throw new Exception("NotificableID no puede ser nulo");
                this.AddHeaders();
                NotificacionDto dto = new NotificacionDto();
                dto.Notification = nt;
                dto.TipoNotificableID = this.GetTipoNotificadorID(nt.Notificador.GetType());
                dto.NotificableID = nt.Notificador.NotificableID;
                dto.Notification.Notificador = null;
                HttpResponseMessage response = client.PostAsJsonAsync(UPDATE, dto).Result;
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsAsync<NotificacionDto>().Result;
                    result.Notification.Notificador = this.GetTypeNotificador(result.TipoNotificableID);
                    result.Notification.Notificador.NotificableID = result.NotificableID;
                    return result.Notification;
                }
                else
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", res.Message, res.ExceptionMessage));
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
