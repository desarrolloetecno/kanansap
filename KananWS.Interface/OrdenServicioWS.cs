﻿using Kanan.Mantenimiento.BO2;
using Kanan.Vehiculos.BO2;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.Security.Cryptography;

namespace KananWS.Interface
{
    public class OrdenServicioWS
    {
        public class OrdenServicioDto
        {
            public List<serviciocosto> listServicioCosto { get; set; }
            public OrdenServicio ordenServicio { get; set; }
            //public int? AlertaID { get; set; }
            //public int? EmpresaID { get; set; }
            public int? MantenibleID { get; set; }
            public int? TipoMantenibleID { get; set; }
            public List<OrdenServicio> ListaOrdenes { get; set; }
            public List<OrdenesDto> OrdenesDto { get; set; }
            public List<DetalleOrden> DetalleOrden { get; set; }
            public List<DetalleDto> DetalleOrdenDto { get; set; }
            public List<SBOKF_CL_CostosAdicional_INFO> ListaCostosAdicionales { get; set; }
            public OrdenServicioDto()
            {
                this.listServicioCosto = new List<serviciocosto>();
                this.ListaOrdenes = new List<OrdenServicio>();
                this.OrdenesDto = new List<OrdenesDto>();
                this.DetalleOrden = new List<DetalleOrden>();
                this.ordenServicio = new OrdenServicio();
                this.DetalleOrdenDto = new List<DetalleDto>();
            }
        }

        public class OrdenesDto
        {
            public OrdenServicio OrdenServicio { get; set; }
            public int? MantenibleID { get; set; }
            public int? TipoMantenibleID { get; set; }
            public OrdenesDto()
            {
                this.OrdenServicio = new OrdenServicio();
            }
        }

        public class FolioOrden
        {
            public Int32 FolioOrdenID { get; set; }
            public Int32 Folio { get; set; }
            public Int32 AnioFolio { get; set; }

        }
        public class ArrayOfFolioOrden
        {
            [DataMember]
            public string FolioOrdenID {get;set;}
            [DataMember]
            public string Folio {get;set;}
            [DataMember]
            public string AnioFolio {get;set;}
            [DataMember]
            public FolioOrden FolioOrden { get; set; }
        }

        public class DetalleDto
        {
            public int? DetalleOrdenID { get; set; }
            public int? MantenibleID { get; set; }
            public int? ServicioID { get; set; }
            public int? OrdenServicioID { get; set; }
            public int? TipoMantenibleID { get; set; }
            public decimal? Costo { get; set; }
            public int? Sincronizado { get; set; }
        }
        #region Constantes

        private static string urlServicio = System.Configuration.ConfigurationManager.AppSettings.Get("WS_URL"); 
        //"http://localhost:51104/";
        #region Orden de servicio con alerta generada
        private const string INSERT_CON_ALERTA = "api/OrdenServicio/InsertaOrdenServicioConAlerta";
        #endregion
        #region Orden de servicio sin alerta generada
        private const string INSERT_SIN_ALERTA = "api/OrdenServicio/InsertaOrdenServicioSinAlerta";
        private const string UPDATE_SIN_ALERTA = "api/OrdenServicio/ActualizaOrdenServicioSinAlerta";
        #endregion
        #endregion

        #region Variables
        static HttpClient client = new HttpClient();
        private Guid PublicKey;
        private string PrivateKey;
        #endregion

        #region Contructor
        public OrdenServicioWS(Guid publicKey, String privatekey)
        {
            if (client.BaseAddress == null)
                client.BaseAddress = new Uri(urlServicio);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            this.PublicKey = publicKey;
            this.PrivateKey = privatekey;
        }
        #endregion

        #region METODOS AUXILIARES
        private string CreateToken(string message, string secret)
        {
            secret = secret ?? "";
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }
        /// <summary>
        /// Agrega la clave publica, la fecha y el token al header
        /// </summary>
        private void AddHeaders()
        {
            string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
            client.DefaultRequestHeaders.Add("X_TIME", date);
            client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
        }

        private int? GetTipoMantenibleID(Type mantenible)
        {
            try
            {
                if (mantenible == null)
                    return null;
                int tipo = 0;
                if (mantenible == typeof(Kanan.Vehiculos.BO2.Vehiculo))
                    tipo = 1;
                else if (mantenible == typeof(Kanan.Vehiculos.BO2.Caja))
                    tipo = 2;
                else if (mantenible == typeof(Kanan.Vehiculos.BO2.TipoVehiculo))
                    tipo = 3;
                return tipo;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        private Kanan.Vehiculos.BO2.IMantenible GetMantenible(int? tipoMantenibleID)
        {
            try
            {
                Kanan.Vehiculos.BO2.IMantenible mant = null;
                if (tipoMantenibleID == 1)
                    mant = new Kanan.Vehiculos.BO2.Vehiculo();
                else if (tipoMantenibleID == 2)
                    mant = new Kanan.Vehiculos.BO2.Caja();
                else if (tipoMantenibleID == 3)
                    mant = new Kanan.Vehiculos.BO2.TipoVehiculo();
                return mant;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        private List<DetalleDto> ListDetalleOrdenToDTO(List<DetalleOrden> detalle)
        {
            List<DetalleDto> dtoList = new List<DetalleDto>();
            if(detalle != null && detalle.Count > 0)
            {
                foreach(DetalleOrden d in detalle)
                {
                    DetalleDto dto = new DetalleDto();
                    if (d.DetalleOrdenID != null)
                        dto.DetalleOrdenID = d.DetalleOrdenID;
                    dto.Costo = d.Costo;
                    dto.TipoMantenibleID = d.TipoMantenibleID;
                    dto.MantenibleID = d.Mantenible.MantenibleID;
                    dto.ServicioID = d.Servicio.ServicioID;
                    if (d.OrdenServicio != null && d.OrdenServicio.OrdenServicioID != null)
                        dto.OrdenServicioID = d.OrdenServicio.OrdenServicioID;
                    dtoList.Add(dto);
                }
            }
            return dtoList;
        }

        private DetalleDto DetalleOrdenToDTO(DetalleOrden detalle)
        {
            DetalleDto dto = new DetalleDto();
            if (detalle.DetalleOrdenID != null)
                dto.DetalleOrdenID = detalle.DetalleOrdenID;
            dto.Costo = detalle.Costo;
            dto.TipoMantenibleID = detalle.TipoMantenibleID;
            dto.MantenibleID = detalle.Mantenible.MantenibleID;
            dto.ServicioID = detalle.Servicio.ServicioID;
            if (detalle.OrdenServicio != null && detalle.OrdenServicio.OrdenServicioID != null)
                dto.OrdenServicioID = detalle.OrdenServicio.OrdenServicioID;
            return dto;
        }

        private DetalleOrden DetalleOrdenDTO_ToOrdenDetalle(DetalleDto detalleDto)
        {
            DetalleOrden d = new DetalleOrden();
            if (detalleDto.DetalleOrdenID != null)
                d.DetalleOrdenID = detalleDto.DetalleOrdenID;
            d.Servicio.ServicioID = detalleDto.ServicioID;
            d.TipoMantenibleID = detalleDto.TipoMantenibleID;
            d.OrdenServicio.OrdenServicioID = detalleDto.OrdenServicioID;
            d.Costo = Convert.ToDecimal(detalleDto.Costo);
            if (detalleDto.TipoMantenibleID == 1)
                d.Mantenible = new Vehiculo();
            if (detalleDto.TipoMantenibleID == 2)
                d.Mantenible = new Caja();
            d.Mantenible.MantenibleID = detalleDto.MantenibleID;
            return d;
        }

        private List<DetalleOrden> ListDetalleOrdenDTO_ToOrdenDetalle(List<DetalleDto> detalleDto)
        {
            List<DetalleOrden> detalle = new List<DetalleOrden>();
            if(detalleDto != null && detalleDto.Count > 0)
            {
                foreach(DetalleDto dto in detalleDto)
                {
                    DetalleOrden d = new DetalleOrden();
                    if (dto.DetalleOrdenID != null)
                        d.DetalleOrdenID = dto.DetalleOrdenID;
                    d.Servicio.ServicioID = dto.ServicioID;
                    d.TipoMantenibleID = dto.TipoMantenibleID;
                    d.OrdenServicio.OrdenServicioID = dto.OrdenServicioID;
                    d.Costo = Convert.ToDecimal(dto.Costo);
                    if (dto.TipoMantenibleID == 1)
                        d.Mantenible = new Kanan.Vehiculos.BO2.Vehiculo();
                    if (dto.TipoMantenibleID == 2)
                        d.Mantenible = new Kanan.Vehiculos.BO2.Caja();
                    d.Mantenible.MantenibleID = dto.MantenibleID;
                    detalle.Add(d);
                }
            }
            return detalle;
        }
        #endregion

        #region Orden de Servicio con alerta generada
        public OrdenServicio InsertarConAlerta(OrdenServicio orden)
        {
            try
            {
                this.AddHeaders();
                OrdenServicioDto dto = new OrdenServicioDto();
                dto.ordenServicio = orden;
                //dto.listServicioCosto = servicios;}
                dto.DetalleOrdenDto = this.ListDetalleOrdenToDTO(orden.DetalleOrden);
                dto.ordenServicio.DetalleOrden = null;
                //dto.MantenibleID = orden.alertas.Mantenible.MantenibleID;
                //dto.TipoMantenibleID = this.GetTipoMantenibleID(orden.alertas.Mantenible.GetType());
                //dto.ordenServicio.alertas.Mantenible = null;
                HttpResponseMessage resp = client.PostAsJsonAsync(INSERT_CON_ALERTA, dto).Result;
                if (resp.IsSuccessStatusCode)
                {
                    var result = resp.Content.ReadAsAsync<OrdenServicioDto>().Result;
                    OrdenServicio od = result.ordenServicio;
                    od.DetalleOrden = this.ListDetalleOrdenDTO_ToOrdenDetalle(result.DetalleOrdenDto);
                    return od;
                    //if (result != null)
                    //    if (result.Count > 0)
                    //    {
                    //        List<OrdenServicio> lista = new List<OrdenServicio>();
                    //        //result.ListaOrdenes = new List<OrdenServicio>();
                    //        foreach (OrdenesDto odto in result)
                    //        {
                    //            OrdenServicio os = new OrdenServicio();
                    //            os = odto.OrdenServicio;
                    //            //os.alertas.Mantenible = this.GetMantenible(odto.TipoMantenibleID);
                    //            //os.alertas.Mantenible.MantenibleID = odto.MantenibleID;
                    //            lista.Add(os);
                    //        }
                    //        return lista;
                    //    }
                    //    else
                    //        throw new Exception("Es posible que no se haya registrado la informacion de la orden de servicio");
                    //else
                    //    throw new Exception("Es posible que no se haya registrado la informacion de la orden de servicio");
                }
                else
                {
                    var result = resp.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", res.Message, res.ExceptionMessage));
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region Orden de Servicio sin alerta generada
        public List<OrdenServicio> InsertarSinAlerta(OrdenServicio orden, List<serviciocosto> servicios)
        {
            try
            {
                this.AddHeaders();
                OrdenServicioDto dto = new OrdenServicioDto();
                dto.ordenServicio = orden;
                //dto.AlertaID = orden.alertas.AlertaMantenimientoID;
                //dto.EmpresaID = orden.alertas.empresa.EmpresaID;
                dto.listServicioCosto = servicios;
                //dto.MantenibleID = orden.alertas.Mantenible.MantenibleID;
                //dto.TipoMantenibleID = this.GetTipoMantenibleID(orden.alertas.Mantenible.GetType());
                //dto.ordenServicio.alertas.Mantenible = null;
                HttpResponseMessage resp = client.PostAsJsonAsync(INSERT_SIN_ALERTA, dto).Result;
                if (resp.IsSuccessStatusCode)
                {
                    var result = resp.Content.ReadAsAsync<List<OrdenesDto>>().Result;
                    if (result != null)
                        if (result.Count > 0)
                        {
                            List<OrdenServicio> lista = new List<OrdenServicio>();
                            //result.ListaOrdenes = new List<OrdenServicio>();
                            foreach (OrdenesDto odto in result)
                            {
                                OrdenServicio os = new OrdenServicio();
                                os = odto.OrdenServicio;
                                //os.alertas.Mantenible = this.GetMantenible(odto.TipoMantenibleID);
                                //os.alertas.Mantenible.MantenibleID = odto.MantenibleID;
                                lista.Add(os);
                            }
                            return lista;
                        }
                        else
                            throw new Exception("Es posible que no se haya registrado la informacion de la orden de servicio");
                    else
                        throw new Exception("Es posible que no se haya registrado la informacion de la orden de servicio");
                }
                else
                {
                    var result = resp.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", res.Message, res.ExceptionMessage));
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public OrdenServicio InsertarSinAlerta(OrdenServicio orden)//, List<DetalleOrden> detalle)
        {
            try
            {
                this.AddHeaders();
                string json = JsonConvert.SerializeObject(orden);
                OrdenServicioDto dto = new OrdenServicioDto();
                dto.ordenServicio = orden;
                dto.DetalleOrdenDto = this.ListDetalleOrdenToDTO(orden.DetalleOrden);
                dto.ListaCostosAdicionales = orden.CostosAdicionalesDto;
                dto.ordenServicio.DetalleOrden = null;
                HttpResponseMessage resp = client.PostAsJsonAsync(INSERT_SIN_ALERTA, dto).Result;
                if (resp.IsSuccessStatusCode)
                {
                    var result = resp.Content.ReadAsAsync<OrdenServicioDto>().Result;
                    OrdenServicio ordenRet = result.ordenServicio;
                    ordenRet.DetalleOrden = this.ListDetalleOrdenDTO_ToOrdenDetalle(result.DetalleOrdenDto);
                    ordenRet.Code = orden.OrdenServicioID;
                    return ordenRet;
                }
                else
                {
                    var result = resp.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", res.Message, res.ExceptionMessage));
                }
            }
            catch (Exception ex) { throw new Exception(ex.ToString()); }
        }

        public OrdenServicio ActualizarSinAlerta(OrdenServicio orden)//, List<DetalleOrden> detalle)
        {
            try
            {
                
                this.AddHeaders();                
                OrdenServicioDto dto = new OrdenServicioDto();
                dto.ordenServicio = orden;
                dto.DetalleOrdenDto = this.ListDetalleOrdenToDTO(orden.DetalleOrden);                
                dto.ordenServicio.DetalleOrden = null;
                dto.ordenServicio.OrdenServicioID = orden.Code;

                String sJSONData = JsonConvert.SerializeObject(dto);

                HttpResponseMessage resp = client.PostAsJsonAsync(UPDATE_SIN_ALERTA, dto).Result;
                if (resp.IsSuccessStatusCode)
                {
                    var result = resp.Content.ReadAsAsync<OrdenServicioDto>().Result;
                    OrdenServicio ordenRet = result.ordenServicio;
                    ordenRet.DetalleOrden = this.ListDetalleOrdenDTO_ToOrdenDetalle(result.DetalleOrdenDto);
                    return ordenRet;
                }
                else
                {
                    var result = resp.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                   // throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", res.Message, res.ExceptionMessage));
                }
            }
            catch (Exception ex) { throw new Exception(ex.ToString()); }
            return null;
        }
        #endregion

        #region Detalle Orden
        public DetalleOrden AddDetalleOrden(DetalleOrden obj)
        {
            try
            {
                AddHeaders();
                DetalleDto d = this.DetalleOrdenToDTO(obj);
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/OrdenServicio/AddDetalleOrden", d).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<DetalleDto>().Result;
                    var ret = this.DetalleOrdenDTO_ToOrdenDetalle(resultado);
                    return ret;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(String.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error: {0}", ex.Message));
            }
        }

        public DetalleOrden UpdateDetalleOrden(DetalleOrden obj)
        {
            try
            {
                AddHeaders();
                DetalleDto d = this.DetalleOrdenToDTO(obj);
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/OrdenServicio/UpdateDetalleOrden", d).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<DetalleDto>().Result;
                    var ret = this.DetalleOrdenDTO_ToOrdenDetalle(resultado);
                    return ret;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(String.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error: {0}", ex.Message));
            }
        }

        public OrdenServicio recuperaSiguienteFolio(string sEmpresaID)
        {
            OrdenServicio oResponse = new OrdenServicio();
            try
            {
                AddHeaders();
                HttpResponseMessage resp = client.GetAsync(string.Format("api/OrdenServicio/GetFolioOsByEmpresaId/{0}", sEmpresaID)).Result;
                if (resp.IsSuccessStatusCode)
                {
                    try
                    {
                        List<ArrayOfFolioOrden> otemp = resp.Content.ReadAsAsync<List<ArrayOfFolioOrden>>().Result;
                        oResponse.Folio = otemp[0].Folio; 
                        oResponse.AnioFolio = otemp[0].AnioFolio; 
                    }
                    catch { }                    
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(String.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error: {0}", ex.Message));
            }
            return oResponse;
        }

        #endregion
    }
}
