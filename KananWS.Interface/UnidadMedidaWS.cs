﻿#region C#
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion C#

#region Kananfleet
using Newtonsoft.Json;
using Kanan.Comun.BO2;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
#endregion Kananfleet

#region SAP

#endregion SAP

namespace KananWS.Interface
{
    public class UnidadMedidaWS
    {
        #region Objetos

        #endregion Objetos

        #region Atributos

        static HttpClient client = new HttpClient();
        private Guid PublicKey;
        private string PrivateKey;

        #endregion Atributos

        #region Contructor
        public UnidadMedidaWS(Guid publicKey, String privatekey)
        {
            if (client.BaseAddress == null)
                client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.AppSettings.Get("WS_URL"));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            this.PublicKey = publicKey;
            this.PrivateKey = privatekey;
        }
        #endregion Constructor

        #region Métodos

        #region Acciones
        public UnidadMedida Add(UnidadMedida oUnidad)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                HttpResponseMessage resp = client.PostAsJsonAsync("api/Inventario/AddUnidad", oUnidad).Result;

                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<UnidadMedida>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Agregar unidad de medida. - Error: {0}", ex.Message));
            }
        }
        public UnidadMedida Update(UnidadMedida oUnidad)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                HttpResponseMessage resp = client.PostAsJsonAsync("api/Inventario/UpdateUnidad", oUnidad).Result;

                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<UnidadMedida>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Actualizar unidad de medida. - Error: {0}", ex.Message));
            }
        }
        public UnidadMedida Delete(int id)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                HttpResponseMessage resp = client.GetAsync("api/Inventario/DeleteUnidad/" + id).Result;

                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<UnidadMedida>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Eliminar unidad de medida. - Error: {0}", ex.Message));
            }
        }
        #endregion Acciones

        #region Auxiliares
        private string CreateToken(string message, string secret)
        {
            secret = secret ?? "";
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }
        #endregion Auxiliares

        #endregion Métodos
    }
}
