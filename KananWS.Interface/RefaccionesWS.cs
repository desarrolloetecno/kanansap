﻿using Kanan.Mantenimiento.BO2;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace KananWS.Interface
{
    public class RefaccionesWS
    {
        private static string urlServicio = System.Configuration.ConfigurationManager.AppSettings.Get("WS_URL"); //"http://localhost:51104/";
        private const string REFACCIONES_API = "api/Inventario/GetByEmpresaID/";


        #region Variables
        static HttpClient client = new HttpClient();
        private Guid PublicKey;
        private string PrivateKey;
        #endregion
        private string CreateToken(string message, string secret)
        {
            secret = secret ?? "";
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }
        /// <summary>
        /// Agrega la clave publica, la fecha y el token al header
        /// </summary>
        private void AddHeaders()
        {
            string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
            client.DefaultRequestHeaders.Add("X_TIME", date);
            client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
        }

        public List<DetalleRefaccion> SolicitarRefacciones(int? empresaID)
        {
            List<DetalleRefaccion> lstRefacciones = new List<DetalleRefaccion>();
            try
            {
                this.AddHeaders();

                HttpResponseMessage resp = client.GetAsync(string.Format("{0}{1}{2}", urlServicio, REFACCIONES_API, empresaID)).Result;
                if (resp.IsSuccessStatusCode)
                {

                    var result = resp.Content.ReadAsAsync<List<DetalleRefaccion>>().Result;
                    return result;
                }
                else
                {
                    var result = resp.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", res.Message, res.ExceptionMessage));
                }
            }
            catch (Exception ex) { throw new Exception(ex.ToString()); }
            return lstRefacciones;
        }
    }
}
