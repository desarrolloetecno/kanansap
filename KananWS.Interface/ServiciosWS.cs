﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Kanan.Operaciones.BO2;
using System.Runtime.Serialization;
using Kanan.Vehiculos.BO2;
using Kanan.Mantenimiento.BO2;
using System.Security.Cryptography;

namespace KananWS.Interface
{
    public class ServiciosWS
    {
        public class ParametroMantenimientoDto
        {
            public ParametroMantenimiento ParametroMantenimiento { get; set; }
            public int? TipoMantenibleID { get; set; }
            public int? MantenibleID { get; set; }
        }

        #region Constantes
        

        #region URL Parametros de Mantenimiento y Servicio
        private static string urlServicio = System.Configuration.ConfigurationManager.AppSettings.Get("WS_URL"); //"http://localhost:51104/";
        private const string INSERT_MANTENIMIENTOPARAM = "api/Servicios/InsertaParametroMantenimiento";
        private const string INSERT_COMPLETE = "api/Servicios/InsertarCompleto";
        private const string UPDATE_MANTENIMIENTOPARAM = "api/Servicios/ActualizaParametroMantenimiento";
        private const string INSERTORUPDATE_PARAMS = "api/Servicios/InsertaOActualizaParametros";
        private const string DELETE_PARAMETERS = "api/Servicios/EliminarParametros";
        #endregion

        #region URL Tipo de Servicio
        private const string INSERT_TSERVICIO = "api/Servicios/AddTSAP";
        private const string UPDATE_TSERVICIO = "api/Servicios/UpdateTSAP";
        private const string DELETE_TSERVICIO = "api/Servicios/DeleteTSAP/";
        #endregion

        #region URL Servicios
        private const string GET_SERVICIO = "api/Servicios/GetByEmpresaID/";
        private const string INSERT_SERVICIO = "api/Servicios/AddSAP";
        private const string UPDATE_SERVICIO = "api/Servicios/UpdateSAP";
        private const string DELETE_SERVICIO = "api/Servicios/DeleteSAP/";
        #endregion

        #endregion

        #region Variables
        static HttpClient client = new HttpClient();
        private Guid PublicKey;
        private string PrivateKey;
        #endregion

        #region Contructor
        public ServiciosWS(Guid publicKey, String privatekey)
        {
            if (client.BaseAddress == null)
                client.BaseAddress = new Uri(urlServicio);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            this.PublicKey = publicKey;
            this.PrivateKey = privatekey;
        }
        #endregion

        #region METODOS AUXILIARES
        private string CreateToken(string message, string secret)
        {
            secret = secret ?? "";
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }
        /// <summary>
        /// Agrega la clave publica, la fecha y el token al header
        /// </summary>
        private void AddHeaders()
        {
            string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
            client.DefaultRequestHeaders.Add("X_TIME", date);
            client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
        }
        #endregion

        #region PARAMETRO DE MANTENIMIENTO Y DE SERVICIO
        /// <summary>
        /// Guarda unicamente parametro de mantenimiento, se utiliza cuando el mantenible es de TipoVehiculo
        /// </summary>
        /// <param name="pmtroMantenimiento"></param>
        /// <param name="empleado"></param>
        /// <param name="tipoVehiculo"></param>
        /// <returns></returns>
        private ParametroMantenimiento InsertaParametroMantenimiento(ParametroMantenimiento pmtroMantenimiento)
        {
            try
            {
                this.AddHeaders();
                HttpResponseMessage resp = client.PostAsJsonAsync(INSERT_MANTENIMIENTOPARAM, pmtroMantenimiento).Result;
                if(resp.IsSuccessStatusCode)
                {
                    var result = resp.Content.ReadAsAsync<ParametroMantenimiento>().Result;
                    return result;
                }
                else
                {
                    var result = resp.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", res.Message, res.ExceptionMessage));
                }
            }
            catch(Exception ex)
            {
                throw new Exception(string.Format("AddMaintenanceParameters - Error: {0}", ex.Message));
            }
        }

        /// <summary>
        /// Actualiza el parametro de mantenimiento, se utiliza cuando el mantenible es de TipoVehiculo
        /// </summary>
        /// <param name="pmtroMantenimiento"></param>
        /// <returns></returns>
        public ParametroMantenimiento ActualizaPametroMantenimiento(ParametroMantenimiento pmtroMantenimiento)
        {
            try
            {
                this.AddHeaders();
                ParametroMantenimientoDto pmDto = new ParametroMantenimientoDto();
                pmDto.ParametroMantenimiento = pmtroMantenimiento;
                pmDto.MantenibleID = pmtroMantenimiento.Mantenible.MantenibleID;
                if (pmtroMantenimiento.Mantenible.GetType() == typeof(Vehiculo))
                    pmDto.TipoMantenibleID = 1;
                else if (pmtroMantenimiento.Mantenible.GetType() == typeof(Caja))
                    pmDto.TipoMantenibleID = 2;
                pmDto.ParametroMantenimiento.Mantenible = null;
                pmDto.ParametroMantenimiento.ParametroServicio.Mantenible = null;
                var json = JsonConvert.SerializeObject(pmDto);
                HttpResponseMessage resp = client.PostAsJsonAsync(UPDATE_MANTENIMIENTOPARAM, pmDto).Result;
                if (resp.IsSuccessStatusCode)
                {
                    var result = resp.Content.ReadAsAsync<ParametroMantenimientoDto>().Result;
                    return result.ParametroMantenimiento;
                }
                else
                {
                    var result = resp.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", res.Message, res.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("AddMaintenanceParameters - Error: {0}", ex.Message));
            }
        }

        /// <summary>
        /// Guarda parametros de mantenimiento y de servicio, se utiliza cuando el mantenible es de tipo vehiculo o caja
        /// </summary>
        /// <param name="pmtroMantenimiento"></param>
        /// <param name="tipoVehiculo"></param>
        /// <returns></returns>
        public ParametroMantenimiento InsertarCompleto(ParametroMantenimiento pmtroMantenimiento)
        {
            try
            {
                this.AddHeaders();
                ParametroMantenimientoDto pmDto = new ParametroMantenimientoDto();
                pmDto.ParametroMantenimiento = pmtroMantenimiento;
                pmDto.MantenibleID = pmtroMantenimiento.Mantenible.MantenibleID;
                if (pmtroMantenimiento.Mantenible.GetType() == typeof(Vehiculo))
                    pmDto.TipoMantenibleID = 1;
                else if (pmtroMantenimiento.Mantenible.GetType() == typeof(Caja))
                    pmDto.TipoMantenibleID = 2;
                pmDto.ParametroMantenimiento.Mantenible = null;
                pmDto.ParametroMantenimiento.ParametroServicio.Mantenible = null;
                //var json = JsonConvert.SerializeObject(pmtroMantenimiento);

                HttpResponseMessage resp = client.PostAsJsonAsync(INSERT_COMPLETE, pmDto).Result;
                //HttpResponseMessage resp = client.GetAsync(INSERT_COMPLETE + json).Result;
                
                if (resp.IsSuccessStatusCode)
                {
                    var result = resp.Content.ReadAsAsync<ParametroMantenimientoDto>().Result;
                    return result.ParametroMantenimiento;
                }
                else
                {
                    var result = resp.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", res.Message, res.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("AddMaintenanceParameters - Error: {0}", ex.Message));
            }
        }

        /// <summary>
        /// Actualiza los parametros de mantenimiento y de servicio, se utiliza cuando el mantenible es de tipo vehiculo o caja
        /// </summary>
        /// <param name="pmtroMantenimiento"></param>
        /// <returns></returns>
        public ParametroMantenimiento InsertaOActualizarParametro(ParametroMantenimiento pmtroMantenimiento)
        {
            try
            {
                this.AddHeaders();
                //var json = JsonConvert.SerializeObject(pmtroMantenimiento);
                ParametroMantenimientoDto pmDto = new ParametroMantenimientoDto();
                pmDto.ParametroMantenimiento = pmtroMantenimiento;
                pmDto.MantenibleID = pmtroMantenimiento.Mantenible.MantenibleID;
                if (pmtroMantenimiento.Mantenible.GetType() == typeof(Vehiculo))
                    pmDto.TipoMantenibleID = 1;
                else if (pmtroMantenimiento.Mantenible.GetType() == typeof(Caja))
                    pmDto.TipoMantenibleID = 2;
                pmDto.ParametroMantenimiento.Mantenible = null;
                pmDto.ParametroMantenimiento.ParametroServicio.Mantenible = null;
                HttpResponseMessage resp = client.PostAsJsonAsync(INSERTORUPDATE_PARAMS, pmDto).Result;
                if (resp.IsSuccessStatusCode)
                {
                    var result = resp.Content.ReadAsAsync<ParametroMantenimientoDto>().Result;
                    return result.ParametroMantenimiento;
                }
                else
                {
                    var result = resp.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", res.Message, res.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("AddMaintenanceParameters - Error: {0}", ex.Message));
            }
        }

        public bool EliminarParametros(ParametroMantenimiento pmtroMantenimiento)
        {
            try
            {
                this.AddHeaders();
                pmtroMantenimiento.Mantenible = null;
                pmtroMantenimiento.ParametroServicio.Mantenible = null;
                HttpResponseMessage resp = client.PostAsJsonAsync(DELETE_PARAMETERS, pmtroMantenimiento).Result;
                if (resp.IsSuccessStatusCode)
                {
                    var result = resp.Content.ReadAsAsync<string>().Result;
                    return Convert.ToBoolean(result);
                }
                else
                {
                    var result = resp.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", res.Message, res.ExceptionMessage));
                }
            }
            catch (Exception ex) { throw new Exception("KananWS: " + ex.Message); }
        }

        #endregion

        #region TIPOS DE SERVICIO

        public TipoServicio InsertarTipoServicio(TipoServicio tServicio)
        {
            try
            {
                this.AddHeaders();
                HttpResponseMessage resp = client.PostAsJsonAsync(INSERT_TSERVICIO, tServicio).Result;
                if (resp.IsSuccessStatusCode)
                {
                    var result = resp.Content.ReadAsAsync<TipoServicio>().Result;
                    return result;
                }
                else
                {
                    var result = resp.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", res.Message, res.ExceptionMessage));
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool ActualizaTipoServicio(TipoServicio tServicio)
        {
            try
            {
                this.AddHeaders();
                HttpResponseMessage resp = client.PostAsJsonAsync(UPDATE_TSERVICIO, tServicio).Result;
                if (resp.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    var result = resp.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", res.Message, res.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool EliminaTipoServicio(int? id)
        {
            try
            {
                this.AddHeaders();
                HttpResponseMessage resp = client.GetAsync(DELETE_TSERVICIO + id).Result;
                if (resp.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    var result = resp.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", res.Message, res.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region SERVICIOS

        public Servicio InsertarServicio(Servicio sv)
        {
            try
            {
                this.AddHeaders();
                HttpResponseMessage resp = client.PostAsJsonAsync(INSERT_SERVICIO, sv).Result;
                if (resp.IsSuccessStatusCode)
                {
                    var result = resp.Content.ReadAsAsync<Servicio>().Result;
                    return result;
                }
                else
                {
                    var result = resp.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", res.Message, res.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool ActualizaServicio(Servicio sv)
        {
            try
            {
                this.AddHeaders();
                HttpResponseMessage resp = client.PostAsJsonAsync(UPDATE_SERVICIO, sv).Result;
                if (resp.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    var result = resp.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", res.Message, res.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool EliminaServicio(int? id)
        {
            try
            {
                this.AddHeaders();
                HttpResponseMessage resp = client.GetAsync(DELETE_SERVICIO + id).Result;
                if (resp.IsSuccessStatusCode)
                    return true;
                else
                {
                    var result = resp.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", res.Message, res.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Servicio> GetServiciosByID(int empresaid)
        {
            try
            {
                this.AddHeaders();
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                //HttpResponseMessage resp = client.PostAsync(string.Format("{0}/GetByID", urlVehiculo),httpContent).Result;
                HttpResponseMessage resp = client.GetAsync(GET_SERVICIO + empresaid).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var ojson = JsonConvert.DeserializeObject(resultado);
                    var tipos = JsonConvert.DeserializeObject<List<Servicio>>(ojson.ToString());
                    return tipos;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(result.Message + " - " + result.ExceptionMessage);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GetServicios - Error: {0}", ex.Message));
            }
        }
        #endregion
        

    }
}
