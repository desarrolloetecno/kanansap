﻿using Newtonsoft.Json;
using SAPinterface.Data.INFO.Objetos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace KananWS.Interface
{
    public class OperadorWS
    {
         #region Constantes
        static string urlServicio = System.Configuration.ConfigurationManager.AppSettings.Get("WS_URL");
         const string END_POINT_ITINERARIO = "api/EmpleadoUsuario/SaveImage";
      
        #endregion

        #region Variables
        static HttpClient client = new HttpClient();
        private Guid PublicKey;
        private string PrivateKey;
        #endregion

        #region Contructor
        public OperadorWS()
        {
            if (client.BaseAddress == null)
                client.BaseAddress = new Uri(urlServicio);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
         //   this.PublicKey = publicKey;
         //   this.PrivateKey = privatekey;
        }
        #endregion


        #region Orden de Servicio con alerta generada
        public SBO_KF_OPERADOR_INFO ActualizaImagen(SBO_KF_OPERADOR_INFO Operador, out string sresponse)
        {
            SBO_KF_OPERADOR_INFO oResponse = null;
            sresponse = string.Empty;
            try
            {
                
                //oJSON.Itinerario = Oitinerario;
                HttpResponseMessage resp = client.PostAsJsonAsync(END_POINT_ITINERARIO, Operador ).Result;
                //String sJson = JsonConvert.SerializeObject(oJSON);
                if (resp.IsSuccessStatusCode)
                    oResponse = resp.Content.ReadAsAsync<SBO_KF_OPERADOR_INFO>().Result;
                else
                {
                    var result = resp.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                    sresponse = res.Message;
                }
            }
            catch (Exception ex) {

                sresponse = ex.Message;
            }

            return oResponse;
        }
        #endregion

        
    }
}
