﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using Etecno.Security2.BO;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using Newtonsoft.Json;
using System.IO;

namespace KananWS.Interface
{
    public class EmpleadoWS
    {

        public class user
        {
            public string userName { get; set; }
            public string password { get; set; }
        }

        public class EmpleadoDto
        {
            public Empleado Empleado { get; set; }
            public Usuario UserGranted { get; set; }
            public Profile Profile { get; set; }
        }

        public class OprdrObservacion
        {
            public Operador oOperador { get; set; }
            public Observacion oObservacion { get; set; }
        }

        public class OprdrCurso
        {
            public Operador oOperador { get; set; }
            public Curso oCurso { get; set; }
        }

        public class OprdrLicencia
        {
            public Operador oOperador { get; set; }
            public Licencia oLicencia { get; set; }
        }

        #region Atributos
        public Configurations configurations { get; set; }
        static HttpClient client = new HttpClient();
        public Guid PublicKey { get; set; }
        public string PrivateKey { get; set; }
        #endregion

        #region Constructor
        public EmpleadoWS()
        {
            this.configurations = new Configurations();
            if (client.BaseAddress == null)
                client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.AppSettings.Get("WS_URL"));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        #endregion

        #region Metodos
        #region Configurations
        public Configurations GetConfigs(int id)
        {
            try
            {
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.GetAsync("api/EmpleadoUsuario/GetConfiguration/" + id).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                if (resp.IsSuccessStatusCode)
                {
                    var res = resp.Content.ReadAsStringAsync();

                    var resultado = resp.Content.ReadAsAsync<Configurations>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error: {0}", ex.Message));
            }
        }
        #endregion

        #region LogIn Methods
        public Configurations LogginByID(int id)
        {
            try
            {
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.GetAsync("api/EmpleadoUsuario/LoginSAPbyId/" + id).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Configurations>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(String.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error: {0}", ex.Message));
            }
        }

        public Empleado LogginByUser(string usuario, string contraseña)
        {
            var usr = new user
            {
                userName = usuario,
                password = contraseña
            };
            try
            {
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/EmpleadoUsuario/LoginSAPbyUser", usr).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Empleado>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(String.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error: {0}", ex.Message));
            }
        }
        #endregion

        #region Acciones
        public Empleado Add(Empleado empl, Usuario usr)
        {
            //empl.Usuario = new Usuario {Nombre = empl.Email, Estado = EstadoUsuario.Active};
            var empldto = new EmpleadoDto
            {
                Empleado = empl,
                //Este registro es para definir que el ProfileID = 16 será un chofer de FuelData.
                //Se debe tomar dinamicamente el ProfileID del objeto que recibe el método.
                //Rair Santos.
                Profile = new Profile { ProfileID = 16 },
                UserGranted = usr
            };

            string sJSONProfile = JsonConvert.SerializeObject(usr);
            LogError("EmpleadoWS.cs->add()->JSONProfile", sJSONProfile);

            try
            {
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                string sJSONEmpleado = JsonConvert.SerializeObject(empldto);
                LogError("EmpleadoWS.cs->add()->sJSONEmpleado", sJSONEmpleado);
                HttpResponseMessage resp = client.PostAsJsonAsync("api/EmpleadoUsuario/AddEmplSAP", empldto).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Empleado>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(String.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                //throw new Exception(string.Format("Error: {0}", ex.Message));
                return null;
            }
        }
        public static void LogError(string sMetodo, string sTexto)
        {
            string sRuta = "";
            try
            {
                sRuta = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                if (!Directory.Exists(string.Format(@"{0}\Errores", sRuta)))
                    Directory.CreateDirectory(string.Format(@"{0}\Errores", sRuta));
                if (!File.Exists(string.Format(@"{0}\Errores\{1}", sRuta, DateTime.Now.ToString("ddMMyyyy"))))
                {
                    sRuta = string.Format(@"{0}\Errores\{1}.txt", sRuta, DateTime.Now.ToString("ddMMyyyy"));

                    File.AppendAllText(sRuta, string.Format(@"{0}\t{1}:\t{2}\n{3}", DateTime.Now.ToString("HH:mm"), sMetodo, sTexto, Environment.NewLine));
                    File.AppendAllText(sRuta, "\n");
                    File.AppendAllText(sRuta, Environment.NewLine);
                }

            }
            catch { }

        }

        public Operador AddOprdr(Operador operador)
        {
            //var operador = new Operador()
            //{
            //    Nombre = empl.Nombre,
            //    Apellido1 = empl.Apellido1,
            //    Apellido2 = empl.Apellido2,
            //    Telefono = empl.Telefono,
            //    EmpleadoID = empl.EmpleadoID,
            //    Correo = empl.Email,
            //    Direccion = empl.Direccion,
            //    Propietario = empl.Dependencia,
            //    EsActivo = true,
            //    Celular = empl.Celular,
            //    SubPropietario = new Sucursal()
            //};
            try
            {
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/EmpleadoUsuario/AddOpSAP", operador).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Operador>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(String.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error: {0}", ex.Message));
            }
        }

        public string Update(Empleado empl)
        {
            try
            {
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/EmpleadoUsuario/UpdateEmpleadoSAP", empl).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<string>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(String.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error: {0}", ex.Message));
            }
        }

        public Operador UpdateOprdr(Operador operador)
        {
            try
            {
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/EmpleadoUsuario/UpdateOperadorSAP", operador).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Operador>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(String.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error: {0}", ex.Message));
            }
        }

        public OperadorVehiculo AsignaOperador(OperadorVehiculo asignacion)
        {
            //var asignacion = new OperadorVehiculo
            //{
            //    VehiculoAsignado = new Vehiculo { VehiculoID = vehiculoid },
            //    OperadorAsignado = new Operador { EmpleadoID = empleadoid },
            //    FechaAsignacion = DateTime.UtcNow,
            //    ObservacionAsignacion = observ
            //};
            try
            {
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/EmpleadoUsuario/AsignaOperador", asignacion).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<OperadorVehiculo>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(String.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error: {0}", ex.Message));
            }
        }

        public string LiberaOperador(OperadorVehiculo asignacion)
        {

            try
            {
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/EmpleadoUsuario/OperatorReleaseSAP", asignacion).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<string>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(String.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error: {0}", ex.Message));
            }
        }

        public int ConsultaOperadorVehiculo(int empleadoid)
        {
            var asignacion = new OperadorVehiculo
            {
                VehiculoAsignado = new Vehiculo(),
                OperadorAsignado = new Operador { EmpleadoID = empleadoid }
            };
            try
            {
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/EmpleadoUsuario/RecuperarAsignaOperador", asignacion).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<OperadorVehiculo>().Result;
                    if (resultado.VehiculoAsignado.VehiculoID != null)
                        return (int)resultado.VehiculoAsignado.VehiculoID;
                    return 0;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    //throw new Exception(String.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                    return 0;
                }
            }
            catch (Exception ex)
            {
                return 0;
                //throw new Exception(string.Format("Error: {0}", ex.Message));
            }
        }

        #region Operador Complementos
        #region Licencia
        public Licencia AddLicencia(Operador operador, Licencia licencia)
        {
            try
            {
                var obj = new OprdrLicencia
                {
                    oOperador = operador,
                    oLicencia = licencia
                };
                AddHeaders();
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/EmpleadoUsuario/AddLicenciaSAP", obj).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Licencia>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(String.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error: {0}", ex.Message));
            }
        }

        public Licencia UpdateLicencia(Licencia licencia)
        {
            try
            {
                AddHeaders();
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/EmpleadoUsuario/UpdateLicenciaSAP", licencia).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Licencia>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(String.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error: {0}", ex.Message));
            }
        }

        public Licencia DeleteLicencia(int id)
        {
            try
            {

                AddHeaders();
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.GetAsync("api/EmpleadoUsuario/DeleteLicencia/" + id).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Licencia>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(String.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error: {0}", ex.Message));
            }
        }
        #endregion

        #region Curso
        public Curso AddCurso(Operador operador, Curso curso)
        {
            try
            {
                var obj = new OprdrCurso
                {
                    oOperador = operador,
                    oCurso = curso
                };
                AddHeaders();
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/EmpleadoUsuario/AddCursoSAP", obj).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Curso>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(String.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error: {0}", ex.Message));
            }
        }

        public Curso UpdateCurso(Curso curso)
        {
            try
            {
                AddHeaders();
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/EmpleadoUsuario/UpdateCursoSAP", curso).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Curso>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(String.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error: {0}", ex.Message));
            }
        }

        public Curso DeleteCurso(int id)
        {
            try
            {

                AddHeaders();
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.GetAsync("api/EmpleadoUsuario/DeleteCurso/" + id).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Curso>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(String.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error: {0}", ex.Message));
            }
        }
        #endregion

        #region Observacion
        public Observacion AddObservacion(Operador operador, Observacion observacion)
        {
            try
            {
                var obj = new OprdrObservacion
                {
                    oOperador = operador,
                    oObservacion = observacion
                };
                AddHeaders();
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/EmpleadoUsuario/AddObservacionSAP", obj).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Observacion>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(String.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error: {0}", ex.Message));
            }
        }

        public Observacion UpdateObservacion(Observacion observacion)
        {
            try
            {
                AddHeaders();
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/EmpleadoUsuario/UpdateObservacionSAP", observacion).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Observacion>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(String.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error: {0}", ex.Message));
            }
        }

        public Observacion DeleteObservacion(int id)
        {
            try
            {
                AddHeaders();
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.GetAsync("api/EmpleadoUsuario/DeleteObservacion/" + id).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Observacion>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(String.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error: {0}", ex.Message));
            }
        }
        #endregion
        #endregion

        #endregion

        #region Metodos Auxiliares
        private string CreateToken(string message, string secret)
        {
            secret = secret ?? "";
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }

        private void AddHeaders()
        {
            string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
            client.DefaultRequestHeaders.Add("X_TIME", date);
            client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
        }
        #endregion
        #endregion
    }
}