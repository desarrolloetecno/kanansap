﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using Kanan.Costos.BO2;
using Kanan.Vehiculos.BO2;
using Newtonsoft.Json;

namespace KananWS.Interface
{
    public class RecargaCombustibleWS
    {
        #region DTO
        public class CargaCombustibleDto
        {
            public bool? ChangeDesplazamiento { get; set; }
            public int? EmpresaID { get; set; }
            public CargaCombustibleEnum CargaCombustible { get; set; }
            public string TimeZone { get; set; }
            public string uuid { get; set; }
            public int? VehiculoID { get; set; }
            public string id { get; set; }
            public string Sincronizado { get; set; }
        }
        #endregion DTO

        #region LogSAP
        public class LogSAPCargaCombustible
        {
            public bool Status { get; set; }
            public CargaCombustibleEnum Carga { get; set; }
            public string Error { get; set; }
            public int ProveedorID { get; set; }
        }
        #endregion LogSAP

        public class LogCargaCombustible
        {
            public bool Status { get; set; }
            public string UUID { get; set; }
            public string Error { get; set; }
            public int ProveedorID { get; set; }
        }

        public class ResultCarga
        {
            public List<LogCargaCombustible> Log { get; set;} 
            public List<Proveedor> ListProveedores { get; set; }
        }

        #region Filter
        public class Filter
        {
            public DateTime? FechaInicio { get; set; }
            public DateTime? FechaFin { get; set; }
            public int? VehiculoID { get; set; }
        }
        #endregion Filter

        #region Atributos
        static HttpClient client = new HttpClient();
        private Guid PublicKey;
        private string PrivateKey;
        #endregion

        #region Constructor
        public RecargaCombustibleWS(Guid publicKey, String privatekey)
        {
            if (client.BaseAddress == null)
                client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.AppSettings.Get("WS_URL"));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            this.PublicKey = publicKey;
            this.PrivateKey = privatekey; 
        }
        #endregion

        #region Metodos
        #region Carga de Combustible DAO
        public LogSAPCargaCombustible Add(CargaCombustibleEnum oCarga, bool desplazamiento, string timezone, int? empresa, int? vehiculoID)
        {
            oCarga.UID = Guid.NewGuid().ToString();
            var fuel = new CargaCombustibleDto
            {
                ChangeDesplazamiento = desplazamiento,
                TimeZone = timezone,
                CargaCombustible = oCarga,
                EmpresaID = empresa,
                VehiculoID = vehiculoID
            };

            fuel.CargaCombustible.Movil.VehiculoID = fuel.VehiculoID;

            var list = new List<CargaCombustibleDto> {fuel};
            string sJSONFuel = string.Empty;

            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                sJSONFuel = JsonConvert.SerializeObject(list);
                HttpResponseMessage resp = client.PostAsJsonAsync("api/RecargaCombustible/AddSAP", list).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<List<LogSAPCargaCombustible>>().Result;
                    return resultado.FirstOrDefault();
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("AddCargaCombustible - Error: Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("AddCargaCombustible - Error: {0}", ex.Message));
            }
        }

        public CargaCombustible Update(CargaCombustible oCarga, bool desplazamiento, string timezone, int? empresa, int? vehiculoID)
        {
            var fuel = new CargaCombustibleDto
            {
                ChangeDesplazamiento = desplazamiento,
                TimeZone = timezone,
                //Comentado porque no se podrá actualizar la recarga de combustible.
                //CargaCombustible = oCarga,
                EmpresaID = empresa,
                VehiculoID = vehiculoID
            };
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/RecargaCombustible/UpdateSAP", fuel).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<CargaCombustible>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("UpdateCargaCombustible - Error: Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("UpdateCargaCombustible - Error: {0}", ex.Message));
            }
        }

        public CargaCombustible Delete(int id)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.GetAsync("api/RecargaCombustible/DeleteSAP/" + id).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<CargaCombustible>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("UpdateCargaCombustible - Error: Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("UpdateCargaCombustible - Error: {0}", ex.Message));
            }
        }

        public List<CargaCombustibleEnum> GetByVehicleID(int id)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.GetAsync("api/RecargaCombustible/GetByVehicleID/" + id).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<IEnumerable<CargaCombustibleEnum>>().Result;
                    return resultado.ToList();
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("GetByVehicleID - Error: Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GetByVehicleID - Error: {0}", ex.Message));
            }
        }

        public List<CargaCombustible> GetByFilter(int id, DateTime inicio, DateTime fin)
        {
            try
            {
                var filtro = new Filter() {FechaInicio = inicio, FechaFin = fin, VehiculoID = id};
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/RecargaCombustible/GetByFilter", filtro).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<IEnumerable<CargaCombustible>>().Result;
                    return resultado.ToList();
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("GetByVehicleID - Error: Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GetByVehicleID - Error: {0}", ex.Message));
            }
        }

        public List<CargaCombustible> ObtenerUnidadVolumen()
        {
            try
            {
                
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                HttpResponseMessage resp = client.GetAsync("api/ComunService/GetUnidadVolumen").Result;
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    return null;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("GetByVehicleID - Error: Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        } 
        #endregion

        private static List<Proveedor> GetProveedores(int empresaid)
        {
            try
            {
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                //HttpResponseMessage resp = client.PostAsync(string.Format("{0}/GetByID", urlVehiculo),httpContent).Result;
                HttpResponseMessage resp = client.GetAsync("api/ComunService/GetAllProvider/" + empresaid).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<string>().Result;
                    var proveedor = JsonConvert.DeserializeObject<List<Proveedor>>(resultado);
                    return proveedor;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(result.Message + " - " + result.ExceptionMessage);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("GetProveedores - Error:" + ex.Message);
            }
        }

        #region Metodos Auxiliares
        private string CreateToken(string message, string secret)
        {
            secret = secret ?? "";
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }
        #endregion
        #endregion
    }
}
