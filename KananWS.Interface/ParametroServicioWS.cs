﻿using Kanan.Mantenimiento.BO2;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace KananWS.Interface
{
    public class ParametroServicioWS
    {
        public class ParametroServicioDto
        {
            public ParametroServicio ParametroServicio { get; set; }
            public int? TipoMantenibleID { get; set; }
            public int? MantenibleID { get; set; }
        }

        #region URLs
        private static string urlServicio = System.Configuration.ConfigurationManager.AppSettings.Get("WS_URL"); //"http://localhost:51104/";
        private const string INSERT_PSERVICIO = "api/Servicios/InsertParametroServicio";
        private const string UPDATE_PSERVICIO = "api/Servicios/ActualizarParametroServicio";
        private const string UPDATE_ALLPSERVICIO = "api/Servicios/ActualizarTodosParametroServicio";
        private const string DELETE_PSERVICIO = "api/Servicios/EliminarParametroServicio/";
        #endregion

        #region Variables
        static HttpClient client = new HttpClient();
        private Guid PublicKey;
        private string PrivateKey;
        #endregion

        public ParametroServicioWS(Guid publicKey, String privatekey)
        {
            if (client.BaseAddress == null)
                client.BaseAddress = new Uri(urlServicio);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            this.PublicKey = publicKey;
            this.PrivateKey = privatekey;
        }

        #region METODOS AUXILIARES
        private string CreateToken(string message, string secret)
        {
            secret = secret ?? "";
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }
        /// <summary>
        /// Agrega la clave publica, la fecha y el token al header
        /// </summary>
        private void AddHeaders()
        {
            string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
            client.DefaultRequestHeaders.Add("X_TIME", date);
            client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
        }
        #endregion

        public ParametroServicio Insertar(ParametroServicio ps)
        {
            try
            {
                this.AddHeaders();
                ParametroServicioDto psDto = new ParametroServicioDto()
                {
                    ParametroServicio = ps,
                    TipoMantenibleID = 3,
                    MantenibleID = ps.Mantenible.MantenibleID,
                };
                psDto.ParametroServicio.Mantenible = null;
                HttpResponseMessage resp = client.PostAsJsonAsync(INSERT_PSERVICIO, psDto).Result;
                if (resp.IsSuccessStatusCode)
                {
                    var result = resp.Content.ReadAsAsync<ParametroMantenimiento>().Result;
                    return result.ParametroServicio;
                }
                else
                {
                    var result = resp.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", res.Message, res.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("AddMaintenanceParameters - Error: {0}", ex.Message));
            }
        }

        public bool Actualizar(ParametroServicio ps, bool isAll)
        {
            try
            {
                this.AddHeaders();
                ParametroServicioDto psDto = new ParametroServicioDto()
                {
                    ParametroServicio = ps,
                    TipoMantenibleID = 3,
                    MantenibleID = ps.Mantenible.MantenibleID,
                };
                psDto.ParametroServicio.Mantenible = null;
                HttpResponseMessage resp = new HttpResponseMessage();
                if (isAll)
                    resp = client.PostAsJsonAsync(UPDATE_ALLPSERVICIO, psDto).Result;
                else
                    resp = client.PostAsJsonAsync(UPDATE_PSERVICIO, psDto).Result;

                if (resp.IsSuccessStatusCode)
                {
                    var res = resp.Content.ReadAsStringAsync().Result;
                    return Convert.ToBoolean(res);
                }
                else
                {
                    var result = resp.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", res.Message, res.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("AddMaintenanceParameters - Error: {0}", ex.Message));
            }
        }

        public bool Eliminar(int? id)
        {
            try
            {
                this.AddHeaders();
                HttpResponseMessage resp = client.GetAsync(DELETE_PSERVICIO + id).Result;
                if (resp.IsSuccessStatusCode)
                {
                    var result = resp.Content.ReadAsStringAsync().Result;
                    return Convert.ToBoolean(result);
                }
                else
                {
                    var result = resp.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", res.Message, res.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("AddMaintenanceParameters - Error: {0}", ex.Message));
            }
        }

    }
}
