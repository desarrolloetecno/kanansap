﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using Kanan.Llantas.BO2;
using Newtonsoft.Json;

namespace KananWS.Interface
{
    public class LlantasWS
    {
        #region Atributos
        static HttpClient client = new HttpClient();
        private Guid PublicKey;
        private string PrivateKey;
        #endregion

        #region Constructor
        public LlantasWS(Guid publicKey, String privatekey)
        //public VehiculoWS()
        {
            if (client.BaseAddress == null)
                client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.AppSettings.Get("WS_URL"));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            this.PublicKey = publicKey;
            this.PrivateKey = privatekey;
        }
        #endregion

        #region Acciones
        //Corregir nombre de la instancia de llantas por el correspondiente verificando que no existen problemas por el cambio de nombres.
        public Llanta Add(Llanta vehiculo)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/Llantas/Add", vehiculo).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Llanta>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("AddLlanta - Error: {0}", ex.Message));
            }
        }

        public Llanta Update(Llanta vehiculo)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/Llantas/Update", vehiculo).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Llanta>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("UpdateLlanta - Error: {0}", ex.Message));
            }
        }

        public Llanta Get(int? ID)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");

                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));

                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   

                //HttpResponseMessage resp = client.PostAsJsonAsync("api/Llantas/GetByLlantaID/", ID).Result;
                HttpResponseMessage resp = client.GetAsync("api/Llantas/GetByLlantaID/" + ID).Result;

                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Llanta>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GetLlanta - Error: {0}", ex.Message));
            }
        }

        public Llanta Delete(int id)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.GetAsync("api/Llantas/DeleteLlanta/" + id).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<Llanta>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("DeleteVehicle - Error: {0}", ex.Message));
            }
        }

        //public bool AsignarSucursal(Vehiculo vehiculo)
        //{
        //    try
        //    {
        //        client.DefaultRequestHeaders.Clear();
        //        string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
        //        client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
        //        client.DefaultRequestHeaders.Add("X_TIME", date);
        //        client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
        //        HttpResponseMessage resp = client.PostAsJsonAsync("api/Vehiculo/SetSucursalToVehicle", vehiculo).Result;
        //        if (resp.IsSuccessStatusCode)
        //        {
        //            var resultado = Convert.ToBoolean(resp.Content.ReadAsStringAsync().Result);
        //            return resultado;
        //        }
        //        else
        //        {
        //            var resultado = resp.Content.ReadAsStringAsync().Result;
        //            var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
        //            throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("AsignarSucursal - Error: {0}", ex.Message));
        //    }
        //}
        #endregion

        #region Metodos Auxiliares
        private string CreateToken(string message, string secret)
        {
            secret = secret ?? "";
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }
        #endregion
    }
}
