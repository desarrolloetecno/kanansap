﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using Kanan.Costos.BO2;
using Kanan.Vehiculos.BO2;
using Newtonsoft.Json;

namespace KananWS.Interface
{
    public class InciVehiculoWS
    {
        public class InciVehiculoDto: IncidenciaVehiculox
        {
            //public IncidenciaVehiculox incidenciaVehiculox { get; set; }
            public string TimeZone { get; set; }
        }

        public class LogSAPInciVehiculo
        {
            public bool Status { get; set; }
            public IncidenciaVehiculox IncidenciaEnVehiculo { get; set; }
            public string Error { get; set; }
        }

        public class Filter
        {
            public DateTime? FechaInicio { get; set; }
            public DateTime? FechaFin { get; set; }
            public int? VehiculoID { get; set; }
        }

        #region Atributos
        static HttpClient client = new HttpClient();
        private Guid PublicKey;
        private string PrivateKey;
        #endregion

        #region Constructor
        public InciVehiculoWS(Guid publicKey, String privatekey)
        {
            if (client.BaseAddress == null)
                client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.AppSettings.Get("WS_URL"));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            this.PublicKey = publicKey;
            this.PrivateKey = privatekey; 
        }
        #endregion

        #region Métodos
        #region Incidencia Vehiculo DAO
        public LogSAPInciVehiculo Add(IncidenciaVehiculox oIncidenciaEnVehiculo, string timezone)
        {
            string UID = Convert.ToString(oIncidenciaEnVehiculo.IncidenciaID);
            UID = Guid.NewGuid().ToString();

            /*var fuel = new InciVehiculoDto
            {
                TimeZone = timezone
            };*/

            oIncidenciaEnVehiculo.TimeZone = timezone;
            int? CreaOrdenServicio = oIncidenciaEnVehiculo.CreaOrdenServicio;
            //var list = new List<InciVehiculoDto> {fuel};

            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/IncidenciaVehiculo/Add", oIncidenciaEnVehiculo).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                if (resp.IsSuccessStatusCode)
                {
                    //LogSAPInciVehiculo listaIncidencia = new LogSAPInciVehiculo();
                    //return listaIncidencia;

                    //LogSAPInciVehiculo[] salvaxd = new LogSAPInciVehiculo[0];
                    //JsonSerializer js = new JsonSerializer();
                    /*string hf = null;
                    LogSAPInciVehiculo logsapincivehiculo = JsonConvert.DeserializeObject<LogSAPInciVehiculo>(hf);
                    return logsapincivehiculo;*/
                    //IncidenciaVehiculox LogSAPInciVehiculo
                    var resultado = resp.Content.ReadAsAsync<IncidenciaVehiculox>().Result;
                    LogSAPInciVehiculo r = new LogSAPInciVehiculo();
                    r.Status = true;
                    r.IncidenciaEnVehiculo = resultado;
                    r.IncidenciaEnVehiculo.CreaOrdenServicio = CreaOrdenServicio;
                    return r;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("AddIncidenciaVehiculo - Error: Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("AddIncidenciaVehiculo - Error: {0}", ex.Message));
            }
        }

        public IncidenciaVehiculox Update(IncidenciaVehiculox oIncidenciaEnVehiculo, string timezone)
        {
            try
            {
                oIncidenciaEnVehiculo.TimeZone = timezone;
                int? CreaOrdenServicio = oIncidenciaEnVehiculo.CreaOrdenServicio;
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/IncidenciaVehiculo/Update", oIncidenciaEnVehiculo).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<IncidenciaVehiculox>().Result;
                    resultado.CreaOrdenServicio = CreaOrdenServicio;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("UpdateIncidenciaVehiculo - Error: Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("UpdateIncidenciaVehiculo - Error: {0}", ex.Message));
            }
        }

        public IncidenciaVehiculox Delete(int? id)
        {
            
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.GetAsync("api/IncidenciaVehiculo/DeleteIncidenciaVehiculo/" + id).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<IncidenciaVehiculox>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("UpdateIncidenciaVehiculo - Error: Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("UpdateIncidenciaVehiculo - Error: {0}", ex.Message));
            }
        }

        public List<IncidenciaVehiculox> GetByVehicleID(int id)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.GetAsync("api/IncidenciaVehiculo/GetByVehiculoID/" + id).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<IEnumerable<IncidenciaVehiculox>>().Result;
                    return resultado.ToList();
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("GetByVehicleID - Error: Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GetByVehicleID - Error: {0}", ex.Message));
            }
        }

        public List<IncidenciaVehiculox> GetByFilter(int id, DateTime inicio, DateTime fin)
        {
            try
            {
                var filtro = new Filter() { FechaInicio = inicio, FechaFin = fin, VehiculoID = id };
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.PostAsJsonAsync("api/IncidenciaVehiculo/ReporteIncidencias", filtro).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<IEnumerable<IncidenciaVehiculox>>().Result;
                    return resultado.ToList();
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("GetByVehicleID - Error: Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GetByVehicleID - Error: {0}", ex.Message));
            }
        } 
        #endregion

        #region Metodos Auxiliares

        #region CreateToken
        private string CreateToken(string message, string secret)
        {
            secret = secret ?? "";
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }
        #endregion

        #endregion

        #endregion

    }
}
