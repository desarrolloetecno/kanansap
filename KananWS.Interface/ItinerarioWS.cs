﻿using Newtonsoft.Json;
using SAPinterface.Data.INFO.Objetos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace KananWS.Interface
{
    public class ItinerarioWS
    {
         #region Constantes
        static string urlServicio = System.Configuration.ConfigurationManager.AppSettings.Get("WS_URL");
        const string END_POINT_ITINERARIO = "api/Cartaporte/Additinerario";      
        #endregion

        #region Variables
        static HttpClient client = new HttpClient();
        private Guid PublicKey;
        private string PrivateKey;
        #endregion

        #region Contructor
        public ItinerarioWS()
        {
            if (client.BaseAddress == null)
                client.BaseAddress = new Uri(urlServicio);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
         //   this.PublicKey = publicKey;
         //   this.PrivateKey = privatekey;
        }
        #endregion


        #region Orden de Servicio con alerta generada
        public SBO_KF_Itinerario_INFO InsertaItinerario(SBO_KF_Itinerario_INFO Oitinerario, out string sresponse)
        {
            SBO_KF_Itinerario_INFO oResponse = null;
            sresponse = string.Empty;
            try
            {
                SBOKF_CL_oResponseItinerario oJSON = new SBOKF_CL_oResponseItinerario();
                oJSON.Itinerario = Oitinerario;

                HttpResponseMessage resp = client.PostAsJsonAsync(END_POINT_ITINERARIO, oJSON ).Result;
                String sJson = JsonConvert.SerializeObject(oJSON);
                if (resp.IsSuccessStatusCode)
                {
                    SBOKF_CL_oResponseItinerario oRest = resp.Content.ReadAsAsync<SBOKF_CL_oResponseItinerario>().Result;
                    oResponse = oRest.Itinerario;
                }
                else
                {
                    var result = resp.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ResultServer>(result);
                    sresponse = res.Message;                    
                }
            }
            catch (Exception ex) {

                sresponse = ex.Message;
            }

            return oResponse;
        }
        #endregion

        
    }
}
