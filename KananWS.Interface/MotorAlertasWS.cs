﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Referenias
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
//Referencias SAP
using Kanan.Alertas.BO2;
using Kanan.Mantenimiento.BO2;
using Kanan.Vehiculos.BO2;

namespace KananWS.Interface
{
    #region AlertaDTO
        public class AlertaDOT
        {
            public int Total{ get; set;}
            public List<Alerta> ListAlerts {get;set;}
        }
    #endregion 
    
    #region DTO
        public class AlertaMantenimientoDto
        {
            public AlertaMantenimiento AlertaMantenimiento { get; set; }
            public int? MantenibleID { get; set; }
            public int? TipoMantenibleID { get; set; }
            /// <summary>
            /// Indica el tipo de parámetro por el cual se genera la alerta
            /// Distancia = 1, Tiempo = 2;
            /// </summary>
            public int? TypeOfType { get; set; }
        }

        public class AlertaRutinaDTO
        {
            public int? AlertaRutinaID { get; set; }
            public int? ServicioID { get; set; }
            public int? VehiculoID { get; set; }
            public int? EmpresaID { get; set; }
            public int? TipoAlerta { get; set; }
            public int? TypeOfType { get; set; }
            public int? MantenibleID { get; set; }
            public int? TipoMantenibleID { get; set; }
            public int? CajaID { get; set; }
            public int? Sincronizado { get; set; }
        }
    #endregion DTO

    public class MotorAlertasWS
    {
        #region Atributos
        static HttpClient client = new HttpClient();
        private Guid PublicKey;
        private string PrivateKey;
        #endregion

        #region Constructor
        public MotorAlertasWS(Guid publicKey, String privatekey)
        {
            if (client.BaseAddress == null)
                client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.AppSettings.Get("WS_URL"));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            this.PublicKey = publicKey;
            this.PrivateKey = privatekey; 
        }
        #endregion

        #region Métodos

        public AlertaDOT GetAllAlertasByEmpresaID(int EmpresaID)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", "3c9c33b4-d50e-4737-bd0b-eaf3eaf2d34b3c9c33b4-d50e-4737-bd0b-eaf3eaf2d34b");
                client.DefaultRequestHeaders.Add("X_TIME", "15079072631507907263");
                client.DefaultRequestHeaders.Add("X_TOKEN", "IB5aTePKtHGLtvP2VhVuUbNCKtTOyDG+ph2a46bkDp8=IB5aTePKtHGLtvP2VhVuUbNCKtTOyDG+ph2a46bkDp8=");
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.GetAsync("api/Alertas/GetAllAlertasByEmpresaID/" + EmpresaID).Result;
                //This method throws an exception if the HTTP response status is an error code.  
                //var xx = resp.EnsureSuccessStatusCode();
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<AlertaDOT>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("GetAllAlertasByEmpresaID - Error: Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GetAllAlertasByEmpresaID - Error: {0}", ex.Message));
            }
        }

        public AlertaMantenimientoDto GetAlertaMttoByID(int? IAlertaID)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                HttpResponseMessage resp = client.GetAsync("api/Alertas/GetAlertaMttoByID/" + IAlertaID).Result;
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<AlertaMantenimientoDto>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("GetAlertaMttoByID - Error: Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GetAlertaMttoByID - Error: {0}", ex.Message));
            }
        }

        public AlertaRutinaDTO GetAlertaRutinaByID(int? IAlertaID)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                HttpResponseMessage resp = client.GetAsync("api/Alertas/GetAlertaRutinaByID/" + IAlertaID).Result;
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<AlertaRutinaDTO>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("GetAlertaMttoByID - Error: Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GetAlertaMttoByID - Error: {0}", ex.Message));
            }
        }

        public List<DesplazamientoHistorico> GetDesplazamientoHistoricoByEmpresaID(int EmpresaID)
        {
            try
            {
                client.DefaultRequestHeaders.Clear();
                string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
                client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
                client.DefaultRequestHeaders.Add("X_TIME", date);
                client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
                //Call HttpClient.GetAsync to send a GET request to the appropriate URI   
                HttpResponseMessage resp = client.GetAsync("api/Vehiculo/GetDesplazamientoHistorico/" + EmpresaID).Result;
                if (resp.IsSuccessStatusCode)
                {
                    var resultado = resp.Content.ReadAsAsync<List<DesplazamientoHistorico>>().Result;
                    return resultado;
                }
                else
                {
                    var resultado = resp.Content.ReadAsStringAsync().Result;
                    var result = JsonConvert.DeserializeObject<ResultServer>(resultado);
                    throw new Exception(string.Format("GetDesplazamientoHistoricoByEmpresaID - Error: Message:{0}, ExceptionMessage: {1}", result.Message, result.ExceptionMessage));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GetDesplazamientoHistoricoByEmpresaID - Error: {0}", ex.Message));
            }
        }

        #endregion

        #region Meétodos auxiliares
        #region CreateToken
        private string CreateToken(string message, string secret)
        {
            secret = secret ?? "";
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }
        #endregion
        #endregion
    }
}
