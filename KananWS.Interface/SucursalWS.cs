﻿using Kanan.Operaciones.BO2;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace KananWS.Interface
{
    public class SucursalWS
    {
        #region Constantes

        private static string urlServicio = System.Configuration.ConfigurationManager.AppSettings.Get("WS_URL"); //"http://localhost:51104/";
        private const string INSERT_COMPLETE = "api/Sucursal/InsertarCompleto/";
        private const string UPDATE_COMPLETE = "api/Sucursal/ActualizarCompleto/";
        private const string UPDATE = "api/Sucursal/Actualizar/";
        //private const string DELETE = "api/Vehiculo/DeleteTypeVSAP/";
        #endregion

        #region Variables
        static HttpClient client = new HttpClient();
        private Guid PublicKey;
        private string PrivateKey;
        #endregion

        #region Constructor
        public SucursalWS(Guid publicKey, String privateKey)
        {
            if (client.BaseAddress == null)
                client.BaseAddress = new Uri(urlServicio);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            this.PublicKey = publicKey;
            this.PrivateKey = privateKey;
        }
        #endregion

        #region METODOS AUXILIARES
        private string CreateToken(string message, string secret)
        {
            secret = secret ?? "";
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }
        /// <summary>
        /// Agrega la clave publica, la fecha y el token al header
        /// </summary>
        private void AddHeaders()
        {
            string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Add("X_PUBLIC_KEY", PublicKey.ToString());
            client.DefaultRequestHeaders.Add("X_TIME", date);
            client.DefaultRequestHeaders.Add("X_TOKEN", CreateToken(PrivateKey + date, PrivateKey));
        }
        #endregion

        #region Acciones
        public Sucursal InsertarCompleto(Sucursal sucursal)
        {
            try
            {
                this.AddHeaders();
                HttpResponseMessage response = client.PostAsJsonAsync(INSERT_COMPLETE, sucursal).Result;
                var json =  JsonConvert.SerializeObject(sucursal);
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsAsync<Sucursal>().Result;
                    return result;
                }
                else
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    var err = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", err.Message, err.ExceptionMessage));
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public bool ActualizarCompleto(Sucursal sucursal)
        {
            try
            {
                this.AddHeaders();
                HttpResponseMessage response = client.PostAsJsonAsync(UPDATE_COMPLETE, sucursal).Result;
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    return Convert.ToBoolean(result);
                }
                else
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    var err = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", err.Message, err.ExceptionMessage));
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }

        }

        public bool Actualizar(Sucursal sucursal)
        {
            try
            {
                this.AddHeaders();
                string jsonGenerado = JsonConvert.SerializeObject(sucursal);
                HttpResponseMessage response = client.PostAsJsonAsync(UPDATE, sucursal).Result;
                
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    return Convert.ToBoolean(result);
                }
                else
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    var err = JsonConvert.DeserializeObject<ResultServer>(result);
                    throw new Exception(string.Format("Message:{0}, ExceptionMessage: {1}", err.Message, err.ExceptionMessage));
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }

        }

        #endregion

    }
}
