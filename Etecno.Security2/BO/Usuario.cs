﻿namespace Etecno.Security2.BO
{
    using System;
    using System.Runtime.CompilerServices;

    public class Usuario : ICloneable
    {
        public Usuario()
        {
            this.Estado = EstadoUsuario.Active;
        }

        public object Clone()
        {
            return new Usuario { UsuarioID = this.UsuarioID, Nombre = this.Nombre, Clave = this.Clave, Estado = this.Estado, PrivateKey = this.PrivateKey, PublicKey = this.PublicKey };
        }

        public string Clave { get; set; }

        public EstadoUsuario? Estado { get; set; }

        public string Nombre { get; set; }

        public int? UsuarioID { get; set; }

        /*-----Variables del app-----*/
        public string PrivateKey { get; set; }

        public string PublicKey { get; set; }
    
        public string CambioContraseña { get; set; }

    }
}

