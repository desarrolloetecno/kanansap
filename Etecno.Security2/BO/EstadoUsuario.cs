﻿namespace Etecno.Security2.BO
{
    using System;

    public enum EstadoUsuario
    {
        Active = 1,
        Blocked = 3,
        Inactive = 2
    }
}

