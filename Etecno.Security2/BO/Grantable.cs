﻿namespace Etecno.Security2.BO
{
    using System;
    using System.Collections.Generic;

    public interface Grantable
    {
        string GetName();
        List<Permission> GetPermissionList();

        int? GrantableID { get; set; }
    }
}

