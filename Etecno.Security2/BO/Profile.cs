﻿namespace Etecno.Security2.BO
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class Profile : Grantable
    {
        public Profile()
        {
            this.Permissions = new List<Permission>();
        }

        public string GetName()
        {
            return this.Name;
        }

        public List<Permission> GetPermissionList()
        {
            return this.Permissions;
        }

        public string Description { get; set; }

        public int? GrantableID
        {
            get
            {
                return this.ProfileID;
            }
            set
            {
                this.ProfileID = value;
            }
        }

        public string Name { get; set; }

        public List<Permission> Permissions { get; set; }

        public int? ProfileID { get; set; }

        /// <summary>
        /// Company business owner
        /// </summary>
        public int? EmpresaID { get; set; }
    }
}

