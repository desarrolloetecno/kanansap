﻿namespace Etecno.Security2.BO
{
    using System;
    using System.Runtime.CompilerServices;

    public class Privilege
    {
        public DateTime? DateGranted { get; set; }

        public bool? IsActive { get; set; }

        public Grantable PermissionGranted { get; set; }

        public int? PrivilegeID { get; set; }

        /// <summary>
        /// User owner
        /// </summary>
        public Usuario UserGranter { get; set; }

      
    }
}

