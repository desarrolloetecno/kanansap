﻿namespace Etecno.Security2.BO
{
    using System;
    using System.Runtime.CompilerServices;

    public class Action
    {
        public int? ActionID { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }
    }
}

