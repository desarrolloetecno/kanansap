﻿namespace Etecno.Security2.BO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public class UserPrivilege
    {
        public UserPrivilege()
        {
            this.UserPermission = new List<Privilege>();
        }

        public bool HashPermissionByID(Permission permission)
        {
            if (permission == null)
            {
                return false;
            }
            if (!permission.PermissionID.HasValue)
            {
                return false;
            }
            if (this.PermissionList.Find(p => p.PermissionID == permission.PermissionID) == null)
            {
                return false;
            }
            return true;
        }

        public bool HashPermissonByActionAplication(string action, string aplication)
        {
            if (string.IsNullOrEmpty(action) || string.IsNullOrEmpty(aplication))
            {
                return false;
            }
            if (this.PermissionList.Find(p => p.Program.Name.Equals(action) && p.Operation.Name.Equals(aplication)) == null)
            {
                return false;
            }
            return true;
        }

        public Usuario Owner { get; set; }

        public List<Permission> PermissionList
        {
            get
            {
                List<Permission> source = new List<Permission>();
                foreach (Privilege privilege in this.UserPermission)
                {
                    source.AddRange(privilege.PermissionGranted.GetPermissionList());
                }
                return source.Distinct<Permission>().ToList<Permission>();
            }
        }

        public List<Privilege> UserPermission { get; set; }

        public int? UserPrivilegeID { get; set; }
    }
}

