﻿namespace Etecno.Security2.BO
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class Permission : Grantable
    {
        public string GetName()
        {
            return ("Can " + this.Operation.Name + " over " + this.Program.Name);
        }

        public List<Permission> GetPermissionList()
        {
            return new List<Permission> { this };
        }

        public int? GrantableID
        {
            get
            {
                return this.PermissionID;
            }
            set
            {
                this.PermissionID = value;
            }
        }

        public Etecno.Security2.BO.Action Operation { get; set; }

        public int? PermissionID { get; set; }

        public Aplication Program { get; set; }
    }
}

