﻿namespace Etecno.Security2.BO
{
    using System;
    using System.Runtime.CompilerServices;

    public class Aplication
    {
        public int? AplicationID { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }

        public int? Priority { get; set; }

        public string URL { get; set; }
    }
}

