﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Etecno.Security2.BO
{
    [Serializable]
    [DataContract]
    public class Sesion : ICloneable
    {
        #region Atributos
        [DataMember]
        private Guid publickey;
        [DataMember]
        private string privatekey;
        [DataMember]
        private int usuarioid;
        [DataMember]
        private DateTime fechasesion;
        [DataMember]
        private DateTime finsesion;
        #endregion

        #region Constructor
        public Sesion()
        {
            this.publickey = new Guid();
            this.fechasesion = new DateTime();
            this.finsesion = new DateTime();
        }
        #endregion

        #region Propiedades
        public Guid PublicKey
        {
            get { return publickey; }
            set { publickey = value; } 
        }

        public string PrivateKey
        {
            get { return privatekey; }
            set { privatekey = value; }
        }

        public int UsuarioID
        {
            get { return usuarioid; }
            set { usuarioid = value; }
        }

        public DateTime FechaSesion
        {
            get { return fechasesion; }
            set { fechasesion = value; }
        }

        public DateTime FinSesion
        {
            get { return finsesion; }
            set { finsesion = value; }
        }
        #endregion

        #region Clone
        public object Clone()
        {
            return new Sesion
            {
                PrivateKey = this.PrivateKey, 
                PublicKey = this.PublicKey,
                UsuarioID = this.UsuarioID,
                FechaSesion = this.fechasesion,
                FinSesion = this.FinSesion
            };
        }
        #endregion
    }
}
