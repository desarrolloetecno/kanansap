﻿namespace Etecno.Security2.Services
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Security2.BO;
    using Etecno.Security2.Services.DA;
    using Etecno.Security2.Services.DAO;
    using System;
    using System.Data;
    using System.Runtime.InteropServices;

    public class PrivilegeGrantedServices
    {
        public Grantable DataRowToGrantable(DataRow row)
        {
            GrantableType permiso = GrantableType.Permiso;
            Grantable grantable = null;
            if (!row.IsNull("GrantableTipeID"))
            {
                permiso = (GrantableType) Convert.ChangeType(row["GrantableTipeID"], typeof(int));
            }
            if (permiso == GrantableType.Perfil)
            {
                grantable = new Profile();
                if (!row.IsNull("ProfileID"))
                {
                    grantable.GrantableID = null;
                    return grantable;
                }
                grantable.GrantableID = new int?((int) Convert.ChangeType(row["ProfileID"], typeof(int)));
                return grantable;
            }
            if (permiso == GrantableType.Permiso)
            {
                grantable = new Permission();
                if (!row.IsNull("PermissionID"))
                {
                    grantable.GrantableID = null;
                    return grantable;
                }
                grantable.GrantableID = new int?((int) Convert.ChangeType(row["PermissionID"], typeof(int)));
            }
            return grantable;
        }

        public void Delete(IDataContext dctx, int PrivilegeGranted)
        {
            new PrivilegeGrantedDelete().Delete(dctx, PrivilegeGranted);
        }

        public bool HashPrivilegeGranted(DataSet ds)
        {
            if (!ds.Tables.Contains("PrivilegeGranted"))
            {
                return false;
            }
            if (ds.Tables["PrivilegeGranted"].Rows.Count < 1)
            {
                return false;
            }
            return true;
        }

        public void Insert(IDataContext dctx, Grantable grantable)
        {
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                dctx.BeginTransaction(sign);
                new PrivilegeGrantedInsert().Insertar(dctx, grantable);
                dctx.CommitTransaction(sign);
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.RollbackTransaction(sign);
                }
                catch (Exception)
                {
                }
                throw new Exception("MessageServices: Hubo un error al Ejecutar la consulta. " + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
        }

        public Grantable LastDataRowTogGrantable(DataSet ds)
        {
            if (!ds.Tables.Contains("PrivilegeGranted"))
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "PrivilegeGrantedServices", "LastDataRowToPrivilegeGranted", "El DataSet no tiene la tabla PrivilegeGranted");
            }
            int count = ds.Tables["PrivilegeGranted"].Rows.Count;
            if (count < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "PrivilegeGrantedServices", "LastDataRowToPrivilegeGranted", "El DataSet no tiene Filas");
            }
            return this.DataRowToGrantable(ds.Tables["PrivilegeGranted"].Rows[count - 1]);
        }

        public void LastDataRowToPrivilegeGranted(DataSet ds, out GrantableType? grantableTipe, out int? perfilID, out int? permisoID, out int? PrivilegeGrantedID)
        {
            PrivilegeGrantedID = 0;
            grantableTipe = 0;
            perfilID = 0;
            permisoID = 0;
            DataRow row = null;
            if (!ds.Tables.Contains("PrivilegeGranted"))
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "PrivilegeGrantedServices", "LastDataRowToPrivilegeGranted", "El DataSet no tiene la tabla PrivilegeGranted");
            }
            int count = ds.Tables["PrivilegeGranted"].Rows.Count;
            if (count < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "PrivilegeGrantedServices", "LastDataRowToPrivilegeGranted", "El DataSet no tiene Filas");
            }
            row = ds.Tables["PrivilegeGranted"].Rows[count - 1];
            if (!row.IsNull("GrantableTipeID"))
            {
                grantableTipe = new GrantableType?((GrantableType) Convert.ChangeType(row["GrantableTipeID"], typeof(int)));
            }
            if (!row.IsNull("ProfileID"))
            {
                perfilID = new int?((int) Convert.ChangeType(row["ProfileID"], typeof(int)));
            }
            if (!row.IsNull("PermissionID"))
            {
                permisoID = new int?((int) Convert.ChangeType(row["PermissionID"], typeof(int)));
            }
            if (!row.IsNull("PrivilegeGrantedID"))
            {
                PrivilegeGrantedID = new int?((int) Convert.ChangeType(row["PrivilegeGrantedID"], typeof(int)));
            }
        }

        public DataSet Retrieve(IDataContext dctx, int? privileGrantedID, Grantable grantable)
        {
            DataSet set = new DataSet();
            PrivilegeGrantedRetrieve retrieve = new PrivilegeGrantedRetrieve();
            if (grantable != null)
            {
                if (grantable.GetType() == typeof(Profile))
                {
                    set = retrieve.Retrieve(dctx, privileGrantedID, (Profile) grantable, null);
                }
                if (grantable.GetType() == typeof(Permission))
                {
                    set = retrieve.Retrieve(dctx, privileGrantedID, null, (Permission) grantable);
                }
                return set;
            }
            return retrieve.Retrieve(dctx, privileGrantedID, null, null);
        }

        public Grantable RetrieveComplete(IDataContext dctx, Grantable grantable)
        {
            ProfileServices services = new ProfileServices();
            PermissionServices services2 = new PermissionServices();
            Grantable grantable2 = null;
            PrivilegeGrantedRetrieve retrieve = new PrivilegeGrantedRetrieve();
            if (grantable.GetType() == typeof(Profile))
            {
                Profile profile = new Profile();
                grantable2 = services.RetriveComplete(dctx, grantable.GrantableID.Value);
            }
            if (grantable.GetType() == typeof(Permission))
            {
                Permission permission = new Permission();
                grantable2 = services2.RetrieveComplete(dctx, grantable.GrantableID.Value);
            }
            return grantable2;
        }

        public DataSet RetrievePrivileGranted(IDataContext dctx, int? ProfileID, int? UserID)
        {
            PrivilegeGrantedByProfile profile = new PrivilegeGrantedByProfile();
            return profile.Retrieve(dctx, ProfileID, UserID);
        }

        public void Update(IDataContext dctx, int PrivilegeGrantedID, Grantable grantable, Grantable anterior)
        {
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                dctx.BeginTransaction(sign);
                new PrivilegeGrantedUpdate().Actualizar(dctx, PrivilegeGrantedID, grantable, anterior);
                dctx.CommitTransaction(sign);
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.RollbackTransaction(sign);
                }
                catch (Exception)
                {
                }
                throw new Exception("MessageServices: Hubo un error al Ejecutar la consulta. " + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
        }
    }
}

