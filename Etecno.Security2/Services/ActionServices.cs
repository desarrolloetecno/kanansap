﻿namespace Etecno.Security2.Services
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Security2.BO;
    using Etecno.Security2.Services.DAO;
    using System;
    using System.Data;

    public class ActionServices
    {
        public Etecno.Security2.BO.Action DataRowToAplication(DataRow row)
        {
            Etecno.Security2.BO.Action action = new Etecno.Security2.BO.Action();
            if (row.IsNull("ActionID"))
            {
                action.ActionID = null;
            }
            else
            {
                action.ActionID = new int?((int) Convert.ChangeType(row["ActionID"], typeof(int)));
            }
            if (row.IsNull("Name"))
            {
                action.Name = null;
            }
            else
            {
                action.Name = (string) Convert.ChangeType(row["Name"], typeof(string));
            }
            if (row.IsNull("Description"))
            {
                action.Description = null;
                return action;
            }
            action.Description = (string) Convert.ChangeType(row["Description"], typeof(string));
            return action;
        }

        public bool HashAction(DataSet ds)
        {
            if (!ds.Tables.Contains("Action"))
            {
                return false;
            }
            if (ds.Tables["Action"].Rows.Count < 1)
            {
                return false;
            }
            return true;
        }

        public Etecno.Security2.BO.Action LastDataRowToAction(DataSet ds)
        {
            if (!ds.Tables.Contains("Action"))
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "ActionServices", "LastDataRowToAction", "El DataSet no tiene la tabla Action");
            }
            int count = ds.Tables["Action"].Rows.Count;
            if (count < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "ActionServices", "LastDataRowToAction", "El DataSet no tiene Filas");
            }
            return this.DataRowToAplication(ds.Tables["Action"].Rows[count - 1]);
        }

        public DataSet Retrieve(IDataContext dctx, Etecno.Security2.BO.Action action)
        {
            ActionRetrieve retrieve = new ActionRetrieve();
            return retrieve.Retrieve(dctx, action);
        }
    }
}

