﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Etecno.Biblioteca.AccesoDatos;
using Etecno.Biblioteca.Errores;
using Etecno.Security2.BO;
using Etecno.Security2.Services.DAO;

namespace Etecno.Security2.Services
{
    public class SesionServices
    {
        public Sesion DataRowToSesion(DataRow row)
        {
            Sesion sesion = new Sesion();
            if (row.IsNull("UsuarioID"))
            {
                sesion.UsuarioID = 0;
            }
            else
            {
                sesion.UsuarioID = (int)Convert.ChangeType(row["UsuarioID"], typeof(int));
            }
            if (row.IsNull("PublicKey"))
            {
                sesion.PublicKey = Guid.Empty;
            }
            else
            {
                sesion.PublicKey = new Guid(row["PublicKey"].ToString());
            }
            if (row.IsNull("PrivateKey"))
            {
                sesion.PrivateKey = null;
            }
            else
            {
                sesion.PrivateKey = (string)Convert.ChangeType(row["PrivateKey"], typeof(string));
            }
            if (row.IsNull("FechaSesion"))
            {
                sesion.FechaSesion = new DateTime();
                return sesion;
            }
            else
            {
                sesion.FechaSesion = (DateTime)Convert.ChangeType(row["FechaSesion"], typeof(DateTime));
            }
            
            return sesion;
        }

        public bool HashSesion(DataSet ds)
        {
            if (!ds.Tables.Contains("Sesion"))
            {
                return false;
            }
            return ds.Tables["Sesion"].Rows.Count >= 1;
        }

        public Guid Insertar(IDataContext dctx, Sesion sesion)
        {
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                dctx.BeginTransaction(sign);
                var id = new SesionInsertar().Insertar(dctx, sesion);
                dctx.CommitTransaction(sign);
                return id;
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.RollbackTransaction(sign);
                }
                catch (Exception)
                {
                }
                throw new Exception("MessageServices: Hubo un error al Ejecutar la consulta. " + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
        }

        public Sesion LastDataRowToUser(DataSet ds)
        {
            if (!ds.Tables.Contains("Sesion"))
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "SesionServices", "LastDataRowToUser", "El DataSet no tiene la tabla Sesion");
            }
            int count = ds.Tables["Sesion"].Rows.Count;
            if (count < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "SesionServices", "LastDataRowToUser", "El DataSet no tiene Filas");
            }
            return this.DataRowToSesion(ds.Tables["Sesion"].Rows[count - 1]);
        }

        public DataSet Retrieve(IDataContext dctx, Sesion sesion)
        {
            SesionConsultar consultar = new SesionConsultar();
            return consultar.Consultar(dctx, sesion);
        }

        public void Actualizar(IDataContext dctx, Sesion sesion, Sesion anterior)
        {
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                dctx.BeginTransaction(sign);
                new SesionActualizar().Actualizar(dctx, sesion, anterior);
                dctx.CommitTransaction(sign);
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.RollbackTransaction(sign);
                }
                catch (Exception)
                {
                }
                throw new Exception("MessageServices: Hubo un error al Ejecutar la consulta. " + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
        }
    }
}
