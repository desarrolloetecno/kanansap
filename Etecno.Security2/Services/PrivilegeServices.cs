﻿namespace Etecno.Security2.Services
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Security2.BO;
    using Etecno.Security2.Services.DAO;
    using System;
    using System.Data;
    using System.Runtime.InteropServices;

    public class PrivilegeServices
    {
        public Privilege DataRowToPrivilege(DataRow row)
        {
            Privilege privilege = new Privilege {
                UserGranter = new Usuario()
            };
            if (row.IsNull("PrivilegeID"))
            {
                privilege.PrivilegeID = null;
            }
            else
            {
                privilege.PrivilegeID = new int?((int) Convert.ChangeType(row["PrivilegeID"], typeof(int)));
            }
            if (row.IsNull("UserGranterID"))
            {
                privilege.UserGranter.UsuarioID = null;
            }
            else
            {
                privilege.UserGranter.UsuarioID = new int?((int) Convert.ChangeType(row["UserGranterID"], typeof(int)));
            }
            if (row.IsNull("DataGranted"))
            {
                privilege.DateGranted = null;
            }
            else
            {
                privilege.DateGranted = new DateTime?((DateTime) Convert.ChangeType(row["DataGranted"], typeof(DateTime)));
            }
            if (row.IsNull("IsActive"))
            {
                privilege.IsActive = null;
                return privilege;
            }
            privilege.IsActive = new bool?((bool) Convert.ChangeType(row["IsActive"], typeof(bool)));
            return privilege;
        }

        public void Delete(IDataContext dctx, Privilege privilege)
        {
            new PrivilegeDelete().Delete(dctx, privilege);
        }

        public void GetPrivilegeGranted(DataSet ds, out int? PrivilegeGrantedID)
        {
            PrivilegeGrantedID = 0;
            DataRow row = null;
            if (!ds.Tables.Contains("Privilege"))
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "PrivilegeServices", "GetPrivilegeGranted", "El DataSet no tiene la tabla Privilege");
            }
            int count = ds.Tables["Privilege"].Rows.Count;
            if (count < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "PrivilegeServices", "GetPrivilegeGranted", "El DataSet no tiene Filas");
            }
            row = ds.Tables["Privilege"].Rows[count - 1];
            if (!row.IsNull("PrivilegeGrantedID"))
            {
                PrivilegeGrantedID = new int?((int) Convert.ChangeType(row["PrivilegeGrantedID"], typeof(int)));
            }
        }

        public bool HashPrivilege(DataSet ds)
        {
            if (!ds.Tables.Contains("Privilege"))
            {
                return false;
            }
            if (ds.Tables["Privilege"].Rows.Count < 1)
            {
                return false;
            }
            return true;
        }

        public void Insert(IDataContext dctx, Privilege privilege, UserPrivilege userPrivilege, int? PrivilegeGranterID)
        {
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                dctx.BeginTransaction(sign);
                new PrivilegeInsert().Insertar(dctx, privilege, userPrivilege, PrivilegeGranterID);
                dctx.CommitTransaction(sign);
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.RollbackTransaction(sign);
                }
                catch (Exception)
                {
                }
                throw new Exception("MessageServices: Hubo un error al Ejecutar la consulta. " + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
        }

        public void InsertComplete(IDataContext dctx, Privilege privilege, UserPrivilege userPrivilege)
        {
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                dctx.BeginTransaction(sign);
                PrivilegeGrantedServices services = new PrivilegeGrantedServices();
                int? privilegeGrantedID = null;
                GrantableType? grantableTipe = null;
                int? perfilID = null;
                int? permisoID = null;
                services.Insert(dctx, privilege.PermissionGranted);
                DataSet ds = services.Retrieve(dctx, null, privilege.PermissionGranted);
                if (services.HashPrivilegeGranted(ds))
                {
                    services.LastDataRowToPrivilegeGranted(ds, out grantableTipe, out perfilID, out permisoID, out privilegeGrantedID);
                    this.Insert(dctx, privilege, userPrivilege, privilegeGrantedID);
                }
                dctx.CommitTransaction(sign);
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.RollbackTransaction(sign);
                }
                catch (Exception)
                {
                }
                throw new Exception("MessageServices: Hubo un error al Ejecutar la consulta. " + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
        }

        public Privilege LastDataRowToUserPrivilege(DataSet ds)
        {
            if (!ds.Tables.Contains("Privilege"))
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "PrivilegeServices", "LastDataRowToPrivilege", "El DataSet no tiene la tabla Privilege");
            }
            int count = ds.Tables["Privilege"].Rows.Count;
            if (count < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "PrivilegeServices", "LastDataRowToPrivilege", "El DataSet no tiene Filas");
            }
            return this.DataRowToPrivilege(ds.Tables["Privilege"].Rows[count - 1]);
        }

        public DataSet Retrieve(IDataContext dctx, Privilege privelege, UserPrivilege userPrivilege)
        {
            PrivilegeRetrieve retrieve = new PrivilegeRetrieve();
            return retrieve.Retrieve(dctx, privelege, userPrivilege);
        }

        public Privilege RetrieveComplete(IDataContext dctx, int privilegeID)
        {
            UsuarioServicios servicios = new UsuarioServicios();
            PrivilegeGrantedServices services = new PrivilegeGrantedServices();
            Privilege privelege = new Privilege {
                PrivilegeID = new int?(privilegeID)
            };
            DataSet ds = this.Retrieve(dctx, privelege, new UserPrivilege());
            if (this.HashPrivilege(ds))
            {
                privelege = this.LastDataRowToUserPrivilege(ds);
            }
            foreach (DataRow row in ds.Tables["Privilege"].Rows)
            {
                int num = (int) Convert.ChangeType(row["PrivilegeGrantedID"], typeof(int));
                DataSet set2 = services.Retrieve(dctx, new int?(num), null);
                if (services.HashPrivilegeGranted(set2))
                {
                    foreach (DataRow row2 in set2.Tables["PrivilegeGranted"].Rows)
                    {
                        if (!row2.IsNull("ProfileID"))
                        {
                            Profile profile = new Profile {
                                ProfileID = new int?((int) Convert.ChangeType(row2["ProfileID"], typeof(int)))
                            };
                            privelege.PermissionGranted = profile;
                            privelege.PermissionGranted = services.RetrieveComplete(dctx, privelege.PermissionGranted);
                        }
                        if (!row2.IsNull("PermissionID"))
                        {
                            Permission permission = new Permission {
                                PermissionID = new int?((int) Convert.ChangeType(row2["PermissionID"], typeof(int)))
                            };
                            privelege.PermissionGranted = permission;
                            privelege.PermissionGranted = services.RetrieveComplete(dctx, privelege.PermissionGranted);
                        }
                    }
                }
            }
            DataSet set3 = servicios.Retrieve(dctx, privelege.UserGranter);
            if (servicios.HashUsuario(set3))
            {
                privelege.UserGranter = servicios.LastDataRowToUser(set3);
            }
            return privelege;
        }

        public void Update(IDataContext dctx, Privilege privilege, Privilege anterior, int? PrivilegeGranterID, int? anteriorPrivilegeGranterID)
        {
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                dctx.BeginTransaction(sign);
                new PrivilegeUpdate().Actualizar(dctx, privilege, anterior, PrivilegeGranterID, anteriorPrivilegeGranterID);
                dctx.CommitTransaction(sign);
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.RollbackTransaction(sign);
                }
                catch (Exception)
                {
                }
                throw new Exception("MessageServices: Hubo un error al Ejecutar la consulta. " + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
        }
    }
}

