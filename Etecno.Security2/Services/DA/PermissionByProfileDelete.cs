﻿namespace Etecno.Security2.Services.DA
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Biblioteca.Utileria;
    using System;
    using System.Text;

    internal class PermissionByProfileDelete
    {
        public void Delete(IDataContext dctx, int? permissionByProfileID)
        {
            DBHelper helper = new DBHelper();
            object sign = new object();
            if (dctx == null)
            {
                throw new ErrorParametros("Conecci\x00f3n", this);
            }
            string str = "";
            if (!permissionByProfileID.HasValue)
            {
                str = str + ", PermissionByProfileID";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            try
            {
                dctx.OpenConnection(sign);
                helper.Comm = dctx.CreateCommand();
            }
            catch (Exception)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services.DA", "PermissionByProfileDelete", "Delete", "Ha ocurrido un error al tratar de conectarse a la base de datos.");
            }
            StringBuilder builder = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            builder.Append(" delete from PermissionByProfile ");
            builder2.Append(" PermissionByProfileID = @PermissionByProfileID ");
            helper.CrearParametro(TipoDato.ENTERO, "PermissionByProfileID", permissionByProfileID);
            string str2 = builder2.ToString().Trim();
            if (str2.Length > 0)
            {
                if (str2.StartsWith("AND "))
                {
                    str2 = str2.Substring(4);
                }
                else if (str2.StartsWith("OR "))
                {
                    str2 = str2.Substring(3);
                }
                else if (str2.StartsWith(","))
                {
                    str2 = str2.Substring(1);
                }
                builder.Append(" WHERE " + str2);
            }
            dctx.CreateDataAdapter().SelectCommand = helper.Comm;
            int num = 0;
            try
            {
                helper.Comm.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                num = helper.Comm.ExecuteNonQuery();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DA", "PermissionByProfileDelete", "Delete", "Ha ocurrido un error al intentar crear el registro. \n" + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
            if (num < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DA", "PermissionByProfileDelete", "Delete", "Ha ocurrido un error, no se Eliminaron registros.");
            }
        }
    }
}

