﻿namespace Etecno.Security2.Services.DA
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Biblioteca.Utileria;
    using Etecno.Security2.BO;
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Text;

    internal class UserAditionalPermission
    {
        public DataSet Consultar(IDataContext dctx, Usuario usuario, string Action, string Aplication)
        {
            DBHelper helper = new DBHelper();
            object sign = new object();
            if (dctx == null)
            {
                throw new ErrorParametros("Conecci\x00f3n", this);
            }
            string str = "";
            if (usuario == null)
            {
                str = str + ", Usuario";
            }
            if (!usuario.UsuarioID.HasValue)
            {
                str = str + ", UsuarioID";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            try
            {
                dctx.OpenConnection(sign);
                helper.Comm = dctx.CreateCommand();
            }
            catch (Exception)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services.DAO", "UserAditionalPermission", "Consultar", "Ha ocurrido un error al tratar de conectarse a la base de datos.");
            }
            StringBuilder builder = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            builder.Append(" select  up.UserPrivilegeID,us.UsuarioID, us.Nombre, pv.PrivilegeID,pg.GrantableTipeID,gt.Name, ");
            builder.Append(" perf.Name as perfil, pm.PermissionID, ac.Name as Accion, ap.Name as Aplicacion from UserPrivilege up ");
            builder.Append(" inner join Usuario us on us.UsuarioID= up.UserID ");
            builder.Append(" inner join Privilege pv on pv.UserPrivilegeID = up.UserPrivilegeID ");
            builder.Append(" inner join PrivilegeGranted pg on pg.PrivilegeGrantedID= pv.PrivilegeGrantedID ");
            builder.Append(" inner join GrantableTipe gt on gt.GrantableTipeID= pg.GrantableTipeID ");
            builder.Append(" inner join Profile perf on perf.ProfileID= pg.ProfileID ");
            builder.Append(" inner join PermissionByProfile pp on pp.ProfileID = perf.ProfileID ");
            builder.Append(" inner join Permission pm on pm.PermissionID= pp.PermissionID ");
            builder.Append(" inner join Action ac on ac.ActionID= pm.ActionID ");
            builder.Append(" inner join Aplication ap on ap.AplicationID= pm.AplicationID ");
            if (usuario.UsuarioID.HasValue)
            {
                builder2.Append("AND us.UsuarioID = @UserID ");
                helper.CrearParametro(TipoDato.ENTERO, "UserID", usuario.UsuarioID);
            }
            if (!string.IsNullOrEmpty(Action))
            {
                builder2.Append("AND ac.Name = @Action ");
                helper.CrearParametro(TipoDato.CADENA, "Action", Action);
            }
            if (!string.IsNullOrEmpty(Aplication))
            {
                builder2.Append("AND ap.Name = @Aplication ");
                helper.CrearParametro(TipoDato.CADENA, "Aplication", Aplication);
            }
            string str2 = builder2.ToString().Trim();
            if (str2.Length > 0)
            {
                if (str2.StartsWith("AND "))
                {
                    str2 = str2.Substring(4);
                }
                else if (str2.StartsWith("OR "))
                {
                    str2 = str2.Substring(3);
                }
                else if (str2.StartsWith(","))
                {
                    str2 = str2.Substring(1);
                }
                builder.Append(" WHERE " + str2);
            }
            builder.Append(" ORDER BY us.UsuarioID ASC ");
            DataSet dataSet = new DataSet();
            DbDataAdapter adapter = dctx.CreateDataAdapter();
            adapter.SelectCommand = helper.Comm;
            try
            {
                adapter.SelectCommand.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                adapter.Fill(dataSet, "AditionalPermissions");
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services.DAO", "UserAditionalPermission", "Consultar", "Ha ocurrido un error al intentar obtener los registros. \n" + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
            return dataSet;
        }
    }
}

