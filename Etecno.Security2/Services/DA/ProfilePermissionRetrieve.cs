﻿namespace Etecno.Security2.Services.DA
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Biblioteca.Utileria;
    using Etecno.Security2.BO;
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Text;

    internal class ProfilePermissionRetrieve
    {
        public DataSet Retrieve(IDataContext dctx, int? permissionByProfileID, Permission permission, Profile profile)
        {
            DBHelper helper = new DBHelper();
            object sign = new object();
            if (dctx == null)
            {
                throw new ErrorParametros("Conecci\x00f3n", this);
            }
            if (permission == null)
            {
                permission = new Permission();
            }
            if (profile == null)
            {
                profile = new Profile();
            }
            try
            {
                dctx.OpenConnection(sign);
                helper.Comm = dctx.CreateCommand();
            }
            catch (Exception)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services.DAO", "PermissionByProfileRetrieve", "Consultar", "Ha ocurrido un error al tratar de conectarse a la base de datos.");
            }
            StringBuilder builder = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            builder.Append(" select pp.PermissionByProfileID,pp.PermissionID,pp.ProfileID,ac.ActionID,ap.AplicationID ,ap.Name as Aplicacion,ac.Description as Accion from PermissionByProfile pp inner join Profile p on p.ProfileID = pp.ProfileID  inner join Permission pm on pm.PermissionID=pp.PermissionID inner join Action ac on pm.ActionID = ac.ActionID inner join Aplication ap on ap.AplicationID= pm.AplicationID ");
            if (permissionByProfileID.HasValue)
            {
                builder2.Append("AND pp.PermissionByProfileID = @PermissionByProfileID ");
                helper.CrearParametro(TipoDato.ENTERO, "PermissionByProfileID", permissionByProfileID);
            }
            if (permission.PermissionID.HasValue)
            {
                builder2.Append("AND pp.PermissionID = @Permission_PermissionID ");
                helper.CrearParametro(TipoDato.ENTERO, "Permission_PermissionID", permission.PermissionID);
            }
            if (profile.ProfileID.HasValue)
            {
                builder2.Append("AND pp.ProfileID = @Profile_ProfileID ");
                helper.CrearParametro(TipoDato.ENTERO, "Profile_ProfileID", profile.ProfileID);
            }
            string str2 = builder2.ToString().Trim();
            if (str2.Length > 0)
            {
                if (str2.StartsWith("AND "))
                {
                    str2 = str2.Substring(4);
                }
                else if (str2.StartsWith("OR "))
                {
                    str2 = str2.Substring(3);
                }
                else if (str2.StartsWith(","))
                {
                    str2 = str2.Substring(1);
                }
                builder.Append(" WHERE " + str2);
            }
            builder.Append(" ORDER BY pp.PermissionByProfileID ASC ");
            DataSet dataSet = new DataSet();
            DbDataAdapter adapter = dctx.CreateDataAdapter();
            adapter.SelectCommand = helper.Comm;
            try
            {
                adapter.SelectCommand.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                adapter.Fill(dataSet, "ProfilePermission");
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services.DAO", "PermissionByProfileRetrieve", "Consultar", "Ha ocurrido un error al intentar obtener los registros. \n" + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
            return dataSet;
        }
    }
}

