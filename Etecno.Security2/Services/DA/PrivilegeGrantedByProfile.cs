﻿namespace Etecno.Security2.Services.DA
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Biblioteca.Utileria;
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Text;

    internal class PrivilegeGrantedByProfile
    {
        public DataSet Retrieve(IDataContext dctx, int? ProfileID, int? UserID)
        {
            DBHelper helper = new DBHelper();
            object sign = new object();
            if (dctx == null)
            {
                throw new ErrorParametros("Conecci\x00f3n", this);
            }
            try
            {
                dctx.OpenConnection(sign);
                helper.Comm = dctx.CreateCommand();
            }
            catch (Exception)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services.DAO", "PrivilegeGrantedByProfile", "Consultar", "Ha ocurrido un error al tratar de conectarse a la base de datos.");
            }
            StringBuilder builder = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            builder.Append(" select pv.PrivilegeGrantedID,ps.ProfileName from PerfileUsuarioView ps join Privilege pv on pv.PrivilegeID=ps.PrivilegeID ");
            if (ProfileID.HasValue)
            {
                builder2.Append("AND ps.ProfileID = @ProfileID ");
                helper.CrearParametro(TipoDato.ENTERO, "ProfileID", ProfileID);
            }
            if (UserID.HasValue)
            {
                builder2.Append("AND ps.UsuarioID = @UsuarioID ");
                helper.CrearParametro(TipoDato.ENTERO, "UsuarioID", UserID);
            }
            string str = builder2.ToString().Trim();
            if (str.Length > 0)
            {
                if (str.StartsWith("AND "))
                {
                    str = str.Substring(4);
                }
                else if (str.StartsWith("OR "))
                {
                    str = str.Substring(3);
                }
                else if (str.StartsWith(","))
                {
                    str = str.Substring(1);
                }
                builder.Append(" WHERE " + str);
            }
            builder.Append(" ORDER BY  pv.PrivilegeGrantedID ASC ");
            DataSet dataSet = new DataSet();
            DbDataAdapter adapter = dctx.CreateDataAdapter();
            adapter.SelectCommand = helper.Comm;
            try
            {
                adapter.SelectCommand.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                adapter.Fill(dataSet, "PrivilegeGranted");
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services.DAO", "PrivilegeGrantedByProfile", "Consultar", "Ha ocurrido un error al intentar obtener los registros. \n" + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
            return dataSet;
        }
    }
}

