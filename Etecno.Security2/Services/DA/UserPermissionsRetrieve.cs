﻿namespace Etecno.Security2.Services.DA
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Biblioteca.Utileria;
    using Etecno.Security2.BO;
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Text;

    internal class UserPermissionsRetrieve
    {
        public DataSet Consultar(IDataContext dctx, Usuario usuario, Etecno.Security2.BO.Action action, Aplication aplication)
        {
            DBHelper helper = new DBHelper();
            object sign = new object();
            if (dctx == null)
            {
                throw new ErrorParametros("Conecci\x00f3n", this);
            }
            string str = "";
            if (usuario == null)
            {
                str = str + ", Usuario";
            }
            if (!usuario.UsuarioID.HasValue)
            {
                str = str + ", UsuarioID";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            try
            {
                dctx.OpenConnection(sign);
                helper.Comm = dctx.CreateCommand();
            }
            catch (Exception)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services.DAO", "UserPermissionsRetrieve", "Consultar", "Ha ocurrido un error al tratar de conectarse a la base de datos.");
            }
            StringBuilder builder = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            builder.Append("SELECT AplicationID, AplicationName, ActionID, ActionName, UsuarioID, ProfileID, PrivilegeIsActive, UsuarioName, UserIsActive, ProfileName, PermissionID FROM FullPermisoUsuarioView ");
            if (usuario.UsuarioID.HasValue)
            {
                builder2.Append("AND UsuarioID = @UserID ");
                helper.CrearParametro(TipoDato.ENTERO, "UserID", usuario.UsuarioID);
            }
            if (usuario.Estado.HasValue)
            {
                builder2.Append("AND UserIsActive = @UserIsActive ");
                helper.CrearParametro(TipoDato.ENTERO, "UserIsActive", usuario.Estado);
            }
            if (action != null)
            {
                if (action.ActionID.HasValue)
                {
                    builder2.Append("AND ActionID = @ActionID ");
                    helper.CrearParametro(TipoDato.ENTERO, "ActionID", action.ActionID);
                }
                if (!string.IsNullOrEmpty(action.Name))
                {
                    builder2.Append("AND ActionName = @ActionName ");
                    helper.CrearParametro(TipoDato.CADENA, "ActionName", action.Name);
                }
            }
            if (aplication != null)
            {
                if (aplication.AplicationID.HasValue)
                {
                    builder2.Append("AND AplicationID = @AplicationID ");
                    helper.CrearParametro(TipoDato.CADENA, "AplicationID", aplication.AplicationID);
                }
                if (!string.IsNullOrEmpty(aplication.Name))
                {
                    builder2.Append("AND AplicationName = @AplicationName ");
                    helper.CrearParametro(TipoDato.CADENA, "AplicationName", aplication.Name);
                }
            }
            string str2 = builder2.ToString().Trim();
            if (str2.Length > 0)
            {
                if (str2.StartsWith("AND "))
                {
                    str2 = str2.Substring(4);
                }
                else if (str2.StartsWith("OR "))
                {
                    str2 = str2.Substring(3);
                }
                else if (str2.StartsWith(","))
                {
                    str2 = str2.Substring(1);
                }
                builder.Append(" WHERE " + str2);
            }
            builder.Append(" ORDER BY UsuarioID ASC ");
            DataSet dataSet = new DataSet();
            DbDataAdapter adapter = dctx.CreateDataAdapter();
            adapter.SelectCommand = helper.Comm;
            try
            {
                adapter.SelectCommand.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                adapter.Fill(dataSet, "UserPermission");
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services.DAO", "UserPermissionsRetrieve", "Consultar", "Ha ocurrido un error al intentar obtener los registros. \n" + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
            return dataSet;
        }
    }
}

