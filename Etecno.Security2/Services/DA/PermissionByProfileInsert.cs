﻿namespace Etecno.Security2.Services.DA
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Biblioteca.Utileria;
    using Etecno.Security2.BO;
    using System;
    using System.Text;

    internal class PermissionByProfileInsert
    {
        public void Insert(IDataContext dctx, Profile profile, Permission permission)
        {
            DBHelper helper = new DBHelper();
            if (dctx == null)
            {
                throw new ErrorParametros("Conecci\x00f3n", this);
            }
            string str = "";
            if (profile == null)
            {
                str = str + "Profile";
            }
            if (permission == null)
            {
                str = str + ", Permission";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            if (!profile.ProfileID.HasValue)
            {
                str = str + ", ProfileID";
            }
            if (!permission.PermissionID.HasValue)
            {
                str = str + ", PermissionID";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                helper.Comm = dctx.CreateCommand();
            }
            catch (Exception)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DA", "PermissionByProfileInsert", "Insertar", "Ha ocurrido un error al tratar de conectarse a la base de datos.");
            }
            StringBuilder builder = new StringBuilder();
            builder.Append(" INSERT INTO PermissionByProfile ( ProfileID, PermissionID )");
            builder.Append("VALUES (@ProfileID, @PermissionID )");
            helper.CrearParametro(TipoDato.ENTERO, "ProfileID", profile.ProfileID);
            helper.CrearParametro(TipoDato.ENTERO, "PermissionID", permission.PermissionID);
            int num = 0;
            try
            {
                helper.Comm.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                num = helper.Comm.ExecuteNonQuery();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DA", "PermissionByProfileInsert", "Insertar", "Ha ocurrido un error al intentar crear el registro. \n" + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
            if (num < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DA", "PermissionByProfileInsert", "Insertar", "Ha ocurrido un error, no se insertaron registros.");
            }
        }
    }
}

