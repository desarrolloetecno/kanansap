﻿namespace Etecno.Security2.Services
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Security2.BO;
    using Etecno.Security2.Services.DAO;
    using System;
    using System.Data;

    public class UsuarioServicios
    {
        public void Actualizar(IDataContext dctx, Usuario usuario, Usuario anterior)
        {
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                dctx.BeginTransaction(sign);
                new UsuarioActualizar().Actualizar(dctx, usuario, anterior);
                dctx.CommitTransaction(sign);
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.RollbackTransaction(sign);
                }
                catch (Exception)
                {
                }
                throw new Exception("MessageServices: Hubo un error al Ejecutar la consulta. " + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
        }

        public Usuario DataRowToUser(DataRow row)
        {
            Usuario usuario = new Usuario();
            if (row.IsNull("UsuarioID"))
            {
                usuario.UsuarioID = null;
            }
            else
            {
                usuario.UsuarioID = new int?((int)Convert.ChangeType(row["UsuarioID"], typeof(int)));
            }
            if (row.IsNull("Nombre"))
            {
                usuario.Nombre = null;
            }
            else
            {
                usuario.Nombre = (string)Convert.ChangeType(row["Nombre"], typeof(string));
            }
            if (row.IsNull("Clave"))
            {
                usuario.Clave = null;
            }
            else
            {
                usuario.Clave = (string)Convert.ChangeType(row["Clave"], typeof(string));
            }
            if (row.IsNull("EstadoUsuario"))
            {
                usuario.Estado = null;
                return usuario;
            }
            usuario.Estado = new EstadoUsuario?((EstadoUsuario)Convert.ChangeType(row["EstadoUsuario"], typeof(int)));

            if (row.IsNull("PrivateKey"))
                usuario.PrivateKey = null;
            else
                usuario.PrivateKey = (string)Convert.ChangeType(row["PrivateKey"], typeof(string));

            if (row.IsNull("PublicKey"))
                usuario.PublicKey = null;
            else
                usuario.PublicKey = (string)Convert.ChangeType(row["PublicKey"], typeof(string));
            
            if (row.IsNull("CambioContraseña"))
                usuario.CambioContraseña = null;
            else
                usuario.CambioContraseña = (string)Convert.ChangeType(row["CambioContraseña"], typeof(string));

            return usuario;
        }

        public bool HashUsuario(DataSet ds)
        {
            if (!ds.Tables.Contains("Usuario"))
            {
                return false;
            }
            if (ds.Tables["Usuario"].Rows.Count < 1)
            {
                return false;
            }
            return true;
        }

        public void Insertar(IDataContext dctx, Usuario usuario)
        {
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                dctx.BeginTransaction(sign);
                new UsuarioInsertar().Insertar(dctx, usuario);
                dctx.CommitTransaction(sign);
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.RollbackTransaction(sign);
                }
                catch (Exception)
                {
                }
                throw new Exception("MessageServices: Hubo un error al Ejecutar la consulta. " + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
        }

        public Usuario LastDataRowToUser(DataSet ds)
        {
            if (!ds.Tables.Contains("Usuario"))
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "UserServices", "LastDataRowToUser", "El DataSet no tiene la tabla User");
            }
            int count = ds.Tables["Usuario"].Rows.Count;
            if (count < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "UserServices", "LastDataRowToUser", "El DataSet no tiene Filas");
            }
            return this.DataRowToUser(ds.Tables["Usuario"].Rows[count - 1]);
        }

        public DataSet Retrieve(IDataContext dctx, Usuario user)
        {
            UsuarioConsultar consultar = new UsuarioConsultar();
            return consultar.Consultar(dctx, user);
        }
    }
}

