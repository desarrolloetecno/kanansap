﻿namespace Etecno.Security2.Services.DAO
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Biblioteca.Utileria;
    using Etecno.Security2.BO;
    using System;
    using System.Text;

    internal class PermissionInsert
    {
        public void Insert(IDataContext dctx, Permission permission)
        {
            DBHelper helper = new DBHelper();
            if (dctx == null)
            {
                throw new ErrorParametros("Conecci\x00f3n", this);
            }
            string str = "";
            if (permission == null)
            {
                str = str + " Permiso ";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            if (permission.Operation == null)
            {
                str = str + " , Accion ";
            }
            if (permission.Program == null)
            {
                str = str + " , Aplicacion ";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            if (!permission.Operation.ActionID.HasValue)
            {
                str = str + " , AccionID ";
            }
            if (!permission.Program.AplicationID.HasValue)
            {
                str = str + " , AplicationID ";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                helper.Comm = dctx.CreateCommand();
            }
            catch (Exception)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "PermissionInsert", "Insertar", "Ha ocurrido un error al tratar de conectarse a la base de datos.");
            }
            StringBuilder builder = new StringBuilder();
            builder.Append(" INSERT INTO Permission ( AplicationID, ActionID )");
            builder.Append("VALUES (@AplicationID, @ActionID )");
            helper.CrearParametro(TipoDato.ENTERO, "AplicationID", permission.Program.AplicationID);
            helper.CrearParametro(TipoDato.ENTERO, "ActionID", permission.Operation.ActionID);
            int num = 0;
            try
            {
                helper.Comm.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                num = helper.Comm.ExecuteNonQuery();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "PemissionInsert", "Insertar", "Ha ocurrido un error al intentar crear el registro. \n" + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
            if (num < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "PemissionInsert", "Insertar", "Ha ocurrido un error, no se insertaron registros.");
            }
        }
    }
}

