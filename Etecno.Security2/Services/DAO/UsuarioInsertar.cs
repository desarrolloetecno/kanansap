﻿namespace Etecno.Security2.Services.DAO
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Biblioteca.Utileria;
    using Etecno.Security2.BO;
    using System;
    using System.Text;

    internal class UsuarioInsertar
    {
        public void Insertar(IDataContext dctx, Usuario usuario)
        {
            DBHelper helper = new DBHelper();
            if (dctx == null)
            {
                throw new ErrorParametros("Conecci\x00f3n", this);
            }
            string str = "";
            if (usuario == null)
            {
                str = str + ", Usuario";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            if (usuario.Nombre == null)
            {
                str = str + ",Usuario.Nombre";
            }
            if (usuario.Clave == null)
            {
                str = str + ",Usuario.Clave";
            }
            if (!usuario.Estado.HasValue)
            {
                str = str + ",Usuario.Estado";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                helper.Comm = dctx.CreateCommand();
            }
            catch (Exception)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "UsuarioInsertar", "Insertar", "Ha ocurrido un error al tratar de conectarse a la base de datos.");
            }
            StringBuilder builder = new StringBuilder();
            builder.Append(" INSERT INTO Usuario (Nombre, Clave, EstadoUsuario, PrivateKey,PublicKey,CambioContraseña)");
            builder.Append("VALUES (@Usuario_Nombre, @Usuario_Clave, @Usuario_EstadoUsuario, @Usuario_PrivateKey, @Usuario_PublicKey, @Usuario_CambioContraseña)");
            helper.CrearParametro(TipoDato.CADENA, "Usuario_Nombre", usuario.Nombre);
            helper.CrearParametro(TipoDato.CADENA, "Usuario_Clave", usuario.Clave);
            helper.CrearParametro(TipoDato.CADENA, "Usuario_PrivateKey", usuario.PrivateKey);
            helper.CrearParametro(TipoDato.CADENA, "Usuario_PublicKey", usuario.PublicKey);
            helper.CrearParametro(TipoDato.CADENA, "Usuario_CambioContraseña", usuario.CambioContraseña);
            EstadoUsuario? estado = usuario.Estado;
            helper.CrearParametro(TipoDato.ENTERO, "Usuario_EstadoUsuario", estado.HasValue ? estado.GetValueOrDefault() : EstadoUsuario.Active);
            int num = 0;
            try
            {
                helper.Comm.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                num = helper.Comm.ExecuteNonQuery();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "UsuarioInsertar", "Insertar", "Ha ocurrido un error al intentar crear el registro. \n" + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
            if (num < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "UsuarioInsertar", "Insertar", "Ha ocurrido un error, no se insertaron registros.");
            }
        }
    }
}

