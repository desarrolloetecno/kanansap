﻿namespace Etecno.Security2.Services.DAO
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Biblioteca.Utileria;
    using Etecno.Security2.BO;
    using System;
    using System.Text;

    internal class UserPrivilegeInsert
    {
        public void Insertar(IDataContext dctx, UserPrivilege userPrivilegio)
        {
            DBHelper helper = new DBHelper();
            if (dctx == null)
            {
                throw new ErrorParametros("Conecci\x00f3n", this);
            }
            string str = "";
            if (userPrivilegio == null)
            {
                str = str + ", Usuario";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            if (userPrivilegio.Owner == null)
            {
                str = str + ", Usuario";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            if (!userPrivilegio.Owner.UsuarioID.HasValue)
            {
                str = str + ",UsuarioID";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                helper.Comm = dctx.CreateCommand();
            }
            catch (Exception)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "UserPrivilegeInsertar", "Insertar", "Ha ocurrido un error al tratar de conectarse a la base de datos.");
            }
            StringBuilder builder = new StringBuilder();
            builder.Append(" INSERT INTO UserPrivilege (UserID)");
            builder.Append("VALUES (@User_UserID) ");
            helper.CrearParametro(TipoDato.ENTERO, "User_UserID", userPrivilegio.Owner.UsuarioID);
            int num = 0;
            try
            {
                helper.Comm.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                num = helper.Comm.ExecuteNonQuery();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "UserPrivilegeInsertar", "Insertar", "Ha ocurrido un error al intentar crear el registro. \n" + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
            if (num < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "UserPrivilegeInsertar", "Insertar", "Ha ocurrido un error, no se insertaron registros.");
            }
        }
    }
}

