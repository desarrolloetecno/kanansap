﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using Etecno.Biblioteca.AccesoDatos;
using Etecno.Biblioteca.Errores;
using Etecno.Biblioteca.Utileria;
using Etecno.Security2.BO;

namespace Etecno.Security2.Services.DAO
{
    public class SesionConsultar
    {
        public DataSet Consultar(IDataContext dctx, Sesion sesion)
        {
            DBHelper helper = new DBHelper();
            object sign = new object();
            if (dctx == null)
            {
                throw new ErrorParametros("Conecci\x00f3n", this);
            }
            if (sesion == null)
            {
                sesion = new Sesion();
            }
            try
            {
                dctx.OpenConnection(sign);
                helper.Comm = dctx.CreateCommand();
            }
            catch (Exception)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services.DAO", "SesionRetrieve", "Consultar", "Ha ocurrido un error al tratar de conectarse a la base de datos.");
            }
            StringBuilder builder = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            builder.Append(" SELECT PrivateKey, PublicKey, UsuarioID, FechaSesion FROM Sesion ");
            if (!string.IsNullOrEmpty(sesion.PublicKey.ToString()))
            {
                builder2.Append("AND PublicKey = @Sesion_PublicKey ");
                helper.CrearParametro(TipoDato.CADENA, "Sesion_PublicKey", sesion.PublicKey.ToString());
            }
            if (!string.IsNullOrEmpty(sesion.PrivateKey))
            {
                builder2.Append("AND PrivateKey = @Sesion_PrivateKey ");
                helper.CrearParametro(TipoDato.CADENA, "Sesion_PrivateKey", sesion.PrivateKey);
            }
            if (sesion.UsuarioID != null && sesion.UsuarioID > 0)
            {
                builder2.Append("AND UsuarioID = @Sesion_UsuarioID ");
                helper.CrearParametro(TipoDato.CADENA, "Sesion_UsuarioID", sesion.UsuarioID);
            }
            string str2 = builder2.ToString().Trim();
            if (str2.Length > 0)
            {
                if (str2.StartsWith("AND "))
                {
                    str2 = str2.Substring(4);
                }
                else if (str2.StartsWith("OR "))
                {
                    str2 = str2.Substring(3);
                }
                else if (str2.StartsWith(","))
                {
                    str2 = str2.Substring(1);
                }
                builder.Append(" WHERE " + str2);
            }
            builder.Append(" ORDER BY UsuarioID ASC ");
            DataSet dataSet = new DataSet();
            DbDataAdapter adapter = dctx.CreateDataAdapter();
            adapter.SelectCommand = helper.Comm;
            try
            {
                adapter.SelectCommand.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                adapter.Fill(dataSet, "Sesion");
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services.DAO", "SesionRetrieve", "Consultar", "Ha ocurrido un error al intentar obtener los registros. \n" + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
            return dataSet;
        }
    }
}
