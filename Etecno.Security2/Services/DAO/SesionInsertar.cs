﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using Etecno.Biblioteca.AccesoDatos;
using Etecno.Biblioteca.Errores;
using Etecno.Biblioteca.Utileria;
using Etecno.Security2.BO;

namespace Etecno.Security2.Services.DAO
{
    public class SesionInsertar
    {
        public Guid Insertar(IDataContext dctx, Sesion sesion)
        {
            DBHelper helper = new DBHelper();
            if (dctx == null)
            {
                throw new ErrorParametros("Conecci\x00f3n", this);
            }
            string str = "";
            if (sesion == null)
            {
                str = str + ", sesion";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            if (sesion.UsuarioID == null)
            {
                str = str + ",sesion.UsuarioID";
            }
            if (sesion.PrivateKey == null)
            {
                str = str + ",sesion.PrivateKey";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                helper.Comm = dctx.CreateCommand();
            }
            catch (Exception)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "SesionInsertar", "Insertar", "Ha ocurrido un error al tratar de conectarse a la base de datos.");
            }
            StringBuilder builder = new StringBuilder();
            //builder.Append(" declare @OutPublickey [uniqueidentifier] ");
            builder.Append(" declare @returnID table (PublicKey uniqueidentifier) ");
            builder.Append(" INSERT INTO Sesion ( PrivateKey, UsuarioID, FechaSesion) ");
            builder.Append(" output inserted.PublicKey into @returnID "); 
            builder.Append(" VALUES (@Sesion_PrivateKey, @Sesion_UsuarioID, @Sesion_FechaSesion) ");
            builder.Append(" SELECT r.PublicKey from @returnID r "); 
            helper.CrearParametro(TipoDato.CADENA, "Sesion_PrivateKey", sesion.PrivateKey);
            helper.CrearParametro(TipoDato.ENTERO, "Sesion_UsuarioID", sesion.UsuarioID);
            helper.CrearParametro(TipoDato.FECHA, "Sesion_FechaSesion", sesion.FechaSesion);
            var num = new Guid();
            DataSet dataSet = new DataSet();
            DbDataAdapter adapter = dctx.CreateDataAdapter();
            adapter.SelectCommand = helper.Comm;
            try
            {
                //helper.Comm.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                //var temp = helper.Comm.ExecuteScalar();
                adapter.SelectCommand.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                adapter.Fill(dataSet, "Sesion");
                num = new Guid(dataSet.Tables["Sesion"].Rows[0]["PublicKey"].ToString());
                //if (num == Guid.Empty)
                //{
                //    throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "SesionInsertar", "Insertar", "Ha ocurrido un error, no se insertaron registros.");
                //}
                return num;
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "SesionInsertar", "Insertar", "Ha ocurrido un error al intentar crear el registro. \n" + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
            //if (num < 1)
            //{
            //    throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "SesionInsertar", "Insertar", "Ha ocurrido un error, no se insertaron registros.");
            //}
        }
    }
}
