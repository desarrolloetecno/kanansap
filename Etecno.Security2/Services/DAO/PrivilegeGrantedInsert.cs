﻿namespace Etecno.Security2.Services.DAO
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Biblioteca.Utileria;
    using Etecno.Security2.BO;
    using System;
    using System.Text;

    internal class PrivilegeGrantedInsert
    {
        public void Insertar(IDataContext dctx, Grantable grantable)
        {
            DBHelper helper = new DBHelper();
            if (dctx == null)
            {
                throw new ErrorParametros("Conecci\x00f3n", this);
            }
            string str = "";
            if (grantable == null)
            {
                str = str + ", debe de tener Perfil o un Permiso";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            if (grantable == null)
            {
                str = str + "ID";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                helper.Comm = dctx.CreateCommand();
            }
            catch (Exception)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "PrivilegeGrantedInsertar", "Insertar", "Ha ocurrido un error al tratar de conectarse a la base de datos.");
            }
            StringBuilder builder = new StringBuilder();
            builder.Append(" INSERT INTO PrivilegeGranted (GrantableTipeID, ProfileID, PermissionID)");
            builder.Append("VALUES (@Grant_GrantableTipeID, @Grant_ProfileID, @Grant_PermissionID)");
            if (grantable.GetType() == typeof(Permission))
            {
                helper.CrearParametro(TipoDato.ENTERO, "Grant_GrantableTipeID", GrantableType.Permiso);
                helper.CrearParametro(TipoDato.ENTERO, "Grant_ProfileID", null);
                helper.CrearParametro(TipoDato.ENTERO, "Grant_PermissionID", grantable.GrantableID);
            }
            else if (grantable.GetType() == typeof(Profile))
            {
                helper.CrearParametro(TipoDato.ENTERO, "Grant_GrantableTipeID", GrantableType.Perfil);
                helper.CrearParametro(TipoDato.ENTERO, "Grant_ProfileID", grantable.GrantableID);
                helper.CrearParametro(TipoDato.ENTERO, "Grant_PermissionID", null);
            }
            int num = 0;
            try
            {
                helper.Comm.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                num = helper.Comm.ExecuteNonQuery();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "PrivilegeGrantedInsertar", "Insertar", "Ha ocurrido un error al intentar crear el registro. \n" + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
            if (num < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "PrivilegeGrantedInsertar", "Insertar", "Ha ocurrido un error, no se insertaron registros.");
            }
        }
    }
}

