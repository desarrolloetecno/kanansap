﻿namespace Etecno.Security2.Services.DAO
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Biblioteca.Utileria;
    using Etecno.Security2.BO;
    using System;
    using System.Text;

    internal class PrivilegeGrantedUpdate
    {
        public void Actualizar(IDataContext dctx, int PrivilegeGrantedID, Grantable grantable, Grantable anterior)
        {
            DBHelper helper = new DBHelper();
            if (dctx == null)
            {
                throw new ErrorParametros("Conecci\x00f3n", this);
            }
            string str = "";
            if (grantable == null)
            {
                str = str + " Usuario";
            }
            if (anterior == null)
            {
                str = str + ", Usuario Anterior";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            if (!anterior.GrantableID.HasValue)
            {
                str = str + ", GrantableID";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                helper.Comm = dctx.CreateCommand();
            }
            catch (Exception)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "PrivilegeGrantedUpdate", "Actualizar", "Ha ocurrido un error al tratar de conectarse a la base de datos.");
            }
            StringBuilder builder = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            StringBuilder builder3 = new StringBuilder();
            builder.Append(" Update PrivilegeGranted ");
            if ((grantable != null) && (grantable.GetType() == typeof(Permission)))
            {
                builder2.Append(" GrantableTipeID=@Grant_GrantableTipeID ");
                helper.CrearParametro(TipoDato.ENTERO, "Grant_GrantableTipeID", GrantableType.Permiso);
                builder2.Append(" , ProfileID=@Grant_ProfileID ");
                helper.CrearParametro(TipoDato.ENTERO, "Grant_ProfileID", null);
                builder2.Append(", PermissionID=@Grant_PermissionID ");
                helper.CrearParametro(TipoDato.ENTERO, "Grant_PermissionID", grantable.GrantableID);
            }
            else if (grantable.GetType() == typeof(Profile))
            {
                builder2.Append(" GrantableTipeID=@Grant_GrantableTipeID ");
                helper.CrearParametro(TipoDato.ENTERO, "Grant_GrantableTipeID", GrantableType.Perfil);
                builder2.Append(" , ProfileID=@Grant_ProfileID ");
                helper.CrearParametro(TipoDato.ENTERO, "Grant_ProfileID", grantable.GrantableID);
                builder2.Append(", PermissionID=@Grant_PermissionID ");
                helper.CrearParametro(TipoDato.ENTERO, "Grant_PermissionID", null);
            }
            builder3.Append(" PrivilegeGrantedID =@PrivilegeGrantedID ");
            helper.CrearParametro(TipoDato.ENTERO, "PrivilegeGrantedID", PrivilegeGrantedID);
            if ((anterior != null) && (anterior.GetType() == typeof(Permission)))
            {
                builder3.Append("AND GrantableTipeID=@Anterior_GrantableTipeID ");
                helper.CrearParametro(TipoDato.ENTERO, "Anterior_GrantableTipeID", GrantableType.Permiso);
                builder3.Append(" AND ProfileID IS NULL ");
                builder3.Append(" AND PermissionID=@Anterior_PermissionID ");
                helper.CrearParametro(TipoDato.ENTERO, "Anterior_PermissionID", anterior.GrantableID);
            }
            else if (anterior.GetType() == typeof(Profile))
            {
                builder3.Append(" AND GrantableTipeID=@Anterior_GrantableTipeID ");
                helper.CrearParametro(TipoDato.ENTERO, "Anterior_GrantableTipeID", GrantableType.Perfil);
                builder3.Append(" AND ProfileID=@Anterior_ProfileID ");
                helper.CrearParametro(TipoDato.ENTERO, "Anterior_ProfileID", anterior.GrantableID);
                builder3.Append(" AND PermissionID IS NULL ");
            }
            builder.Append(" Set " + builder2.ToString().Trim() + " Where " + builder3.ToString().Trim());
            int num = 0;
            try
            {
                helper.Comm.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                num = helper.Comm.ExecuteNonQuery();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "PrivilegeGrantedUpdate", "Actualizar", "Ha ocurrido un error al intentar crear el registro. \n" + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
            if (num < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "PrivilegeGrantedUpdate", "Actualizar", "Ha ocurrido un error, no se Actualizaron registros.");
            }
        }
    }
}

