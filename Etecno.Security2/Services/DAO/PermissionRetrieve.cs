﻿namespace Etecno.Security2.Services.DAO
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Biblioteca.Utileria;
    using Etecno.Security2.BO;
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Text;

    internal class PermissionRetrieve
    {
        public DataSet Retrieve(IDataContext dctx, Permission permission)
        {
            DBHelper helper = new DBHelper();
            object sign = new object();
            if (dctx == null)
            {
                throw new ErrorParametros("Conecci\x00f3n", this);
            }
            if (permission == null)
            {
                permission = new Permission();
            }
            if (permission.Operation == null)
            {
                permission.Operation = new Etecno.Security2.BO.Action();
            }
            if (permission.Program == null)
            {
                permission.Program = new Aplication();
            }
            try
            {
                dctx.OpenConnection(sign);
                helper.Comm = dctx.CreateCommand();
            }
            catch (Exception)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services.DAO", "PermissionRetrieve", "Consultar", "Ha ocurrido un error al tratar de conectarse a la base de datos.");
            }
            StringBuilder builder = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            builder.Append(" select pm.PermissionID, pm.AplicationID,pm.ActionID,ap.Name Aplicacion, ac.Description as Accion FROM Permission pm inner join Aplication ap on ap.AplicationID= pm.AplicationID inner join Action ac on ac.ActionID= pm.ActionID ");
            if (permission.PermissionID.HasValue)
            {
                builder2.Append("AND pm.PermissionID = @Permission_PermissionID ");
                helper.CrearParametro(TipoDato.ENTERO, "Permission_PermissionID", permission.PermissionID);
            }
            if (permission.Operation.ActionID.HasValue)
            {
                builder2.Append("AND pm.ActionID = @Permission_Action_ActionID ");
                helper.CrearParametro(TipoDato.ENTERO, "Permission_Action_ActionID", permission.Operation.ActionID);
            }
            if (permission.Program.AplicationID.HasValue)
            {
                builder2.Append("AND pm.AplicationID = @Permission_Aplication_AplicationID ");
                helper.CrearParametro(TipoDato.ENTERO, "Permission_Aplication_AplicationID", permission.Program.AplicationID);
            }
            string str2 = builder2.ToString().Trim();
            if (str2.Length > 0)
            {
                if (str2.StartsWith("AND "))
                {
                    str2 = str2.Substring(4);
                }
                else if (str2.StartsWith("OR "))
                {
                    str2 = str2.Substring(3);
                }
                else if (str2.StartsWith(","))
                {
                    str2 = str2.Substring(1);
                }
                builder.Append(" WHERE " + str2);
            }
            builder.Append(" ORDER BY pm.PermissionID ASC ");
            DataSet dataSet = new DataSet();
            DbDataAdapter adapter = dctx.CreateDataAdapter();
            adapter.SelectCommand = helper.Comm;
            try
            {
                adapter.SelectCommand.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                adapter.Fill(dataSet, "Permission");
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services.DAO", "PermissionRetrieve", "Consultar", "Ha ocurrido un error al intentar obtener los registros. \n" + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
            return dataSet;
        }
    }
}

