﻿namespace Etecno.Security2.Services.DAO
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Biblioteca.Utileria;
    using Etecno.Security2.BO;
    using System;
    using System.Text;

    internal class ProfileInsert
    {
        public void Insert(IDataContext dctx, Profile profile)
        {
            DBHelper helper = new DBHelper();
            if (dctx == null)
            {
                throw new ErrorParametros("Conecci\x00f3n", this);
            }
            string str = "";
            if (profile == null)
            {
                str = str + ", Profile";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            if (string.IsNullOrEmpty(profile.Name))
            {
                str = str + ", Name";
            }
            if (string.IsNullOrEmpty(profile.Description))
            {
                str = str + ", Descripccion";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                helper.Comm = dctx.CreateCommand();
            }
            catch (Exception)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "ProfileInsert", "Insertar", "Ha ocurrido un error al tratar de conectarse a la base de datos.");
            }
            StringBuilder builder = new StringBuilder();
            builder.Append(" INSERT INTO Profile ( Name, Description,EmpresaID )");
            builder.Append("VALUES ( @Profile_Name, @Profile_Description,@EmpresaID)");
            helper.CrearParametro(TipoDato.CADENA, "Profile_Name", profile.Name);
            helper.CrearParametro(TipoDato.CADENA, "Profile_Description", profile.Description);
            helper.CrearParametro(TipoDato.ENTERO, "EmpresaID", profile.EmpresaID);
            int num = 0;
            try
            {
                helper.Comm.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                num = helper.Comm.ExecuteNonQuery();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "ProfileInsert", "Insertar", "Ha ocurrido un error al intentar crear el registro. \n" + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
            if (num < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "ProfileInsert", "Insertar", "Ha ocurrido un error, no se insertaron registros.");
            }
        }
    }
}

