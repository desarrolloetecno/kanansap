﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Etecno.Biblioteca.AccesoDatos;
using Etecno.Biblioteca.Errores;
using Etecno.Biblioteca.Utileria;
using Etecno.Security2.BO;

namespace Etecno.Security2.Services.DAO
{
    public class SesionActualizar
    {
        public void Actualizar(IDataContext dctx, Sesion sesion, Sesion anterior)
        {
            DBHelper helper = new DBHelper();
            if (dctx == null)
            {
                throw new ErrorParametros("Conecci\x00f3n", this);
            }
            string str = "";
            if (sesion == null)
            {
                str = str + " Sesion";
            }
            if (anterior == null)
            {
                str = str + ", Sesion Anterior";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            if (sesion.UsuarioID == null)
            {
                str = str + ",sesion.UsuarioID";
            }
            if (sesion.PrivateKey == null)
            {
                str = str + ",sesion.PrivateKey";
            }
            if (anterior.UsuarioID == null)
            {
                str = str + ",sesion.UsuarioID";
            }
            if (anterior.PrivateKey == null)
            {
                str = str + ",sesion.PrivateKey";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                helper.Comm = dctx.CreateCommand();
            }
            catch (Exception)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "UsuarioActualizar", "Actualizar", "Ha ocurrido un error al tratar de conectarse a la base de datos.");
            }
            StringBuilder builder = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            StringBuilder builder3 = new StringBuilder();
            builder.Append(" Update Sesion ");
            if (!string.IsNullOrEmpty(sesion.PrivateKey))
            {
                builder2.Append(" PrivateKey=@PrivateKey ");
                helper.CrearParametro(TipoDato.CADENA, "PrivateKey", sesion.PrivateKey);
            }
            if (!(sesion.UsuarioID == null))
            {
                builder2.Append(", UsuarioID=@UsuarioID ");
                helper.CrearParametro(TipoDato.ENTERO, "UsuarioID", sesion.UsuarioID);
            }
            if (sesion.FechaSesion != new DateTime())
            {
                builder2.Append(", FechaSesion =@FechaSesion ");
                helper.CrearParametro(TipoDato.FECHA, "FechaSesion", sesion.FechaSesion);
            }
            if (sesion.FinSesion != new DateTime())
            {
                builder2.Append(", FinSesion =@FinSesion ");
                helper.CrearParametro(TipoDato.FECHA, "FinSesion", sesion.FinSesion);
            }
            //-----ANTERIOR-----
            if (anterior.PublicKey != Guid.Empty)
            {
                builder3.Append(" PublicKey=@Anterior_PublicKey ");
                helper.CrearParametro(TipoDato.CADENA, "Anterior_PublicKey", anterior.PublicKey.ToString());
            }

            if (!string.IsNullOrEmpty(anterior.PrivateKey))
            {
                builder3.Append(" AND PrivateKey=@Anterior_PrivateKey ");
                helper.CrearParametro(TipoDato.CADENA, "Anterior_PrivateKey", anterior.PrivateKey);
            }
            builder3.Append(" AND UsuarioID =@Anterior_UsuarioID ");
            helper.CrearParametro(TipoDato.ENTERO, "Anterior_UsuarioID", anterior.UsuarioID);

            builder.Append(" Set " + builder2.ToString().Trim() + " Where " + builder3.ToString().Trim());
            int num = 0;
            try
            {
                helper.Comm.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                num = helper.Comm.ExecuteNonQuery();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "UsuarioActualizar", "Actualizar", "Ha ocurrido un error al intentar crear el registro. \n" + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
            if (num < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "UsuarioActualizar", "Actualizar", "Ha ocurrido un error, no se Actualizaron registros.");
            }
        }

    }
}
