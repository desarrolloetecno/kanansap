﻿namespace Etecno.Security2.Services.DAO
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Biblioteca.Utileria;
    using Etecno.Security2.BO;
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Text;

    internal class ProfileRetrieve
    {
        public DataSet Retrieve(IDataContext dctx, Profile profile,bool IsOwnerKanan)
        {
            DBHelper helper = new DBHelper();
            object sign = new object();
            if (dctx == null)
            {
                throw new ErrorParametros("Conecci\x00f3n", this);
            }
            if (profile == null)
            {
                profile = new Profile();
            }
            try
            {
                dctx.OpenConnection(sign);
                helper.Comm = dctx.CreateCommand();
            }
            catch (Exception)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services.DAO", "ProfileRetrieve", "Consultar", "Ha ocurrido un error al tratar de conectarse a la base de datos.");
            }
            StringBuilder builder = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            builder.Append(" SELECT ProfileID, Name, Description,EmpresaID FROM Profile ");
            if (IsOwnerKanan)
            {
                // RETRIEVE PROFILES OF ONLY KANAN OR PRE-CONFIGURATED
                builder2.Append("AND EmpresaID IS NULL ");
            }
            else
            {
                if (profile.ProfileID.HasValue)
                {
                    builder2.Append("AND ProfileID = @Profile_ProfileID ");
                    helper.CrearParametro(TipoDato.ENTERO, "Profile_ProfileID", profile.ProfileID);
                }
                if (!string.IsNullOrEmpty(profile.Name))
                {
                    builder2.Append("AND Name LIKE @Profile_Name ");
                    helper.CrearParametro(TipoDato.CADENA, "Profile_Name", profile.Name);
                }
                if (!string.IsNullOrEmpty(profile.Description))
                {
                    builder2.Append("AND Description LIKE @Profile_Description ");
                    helper.CrearParametro(TipoDato.CADENA, "Profile_Description", profile.Description);
                }
                if (profile.EmpresaID != null)
                {
                    builder2.Append("AND EmpresaID=@EmpresaID ");
                    helper.CrearParametro(TipoDato.ENTERO, "EmpresaID", profile.EmpresaID);
                }
            }
            string str2 = builder2.ToString().Trim();
            if (str2.Length > 0)
            {
                if (str2.StartsWith("AND "))
                {
                    str2 = str2.Substring(4);
                }
                else if (str2.StartsWith("OR "))
                {
                    str2 = str2.Substring(3);
                }
                else if (str2.StartsWith(","))
                {
                    str2 = str2.Substring(1);
                }
                builder.Append(" WHERE " + str2);
            }
            builder.Append(" ORDER BY ProfileID ASC ");
            DataSet dataSet = new DataSet();
            DbDataAdapter adapter = dctx.CreateDataAdapter();
            adapter.SelectCommand = helper.Comm;
            try
            {
                adapter.SelectCommand.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                adapter.Fill(dataSet, "Profile");
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services.DAO", "ProfileRetrieve", "Consultar", "Ha ocurrido un error al intentar obtener los registros. \n" + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
            return dataSet;
        }
    }
}

