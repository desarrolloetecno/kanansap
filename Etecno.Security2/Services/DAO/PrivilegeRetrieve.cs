﻿namespace Etecno.Security2.Services.DAO
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Biblioteca.Utileria;
    using Etecno.Security2.BO;
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Text;

    internal class PrivilegeRetrieve
    {
        public DataSet Retrieve(IDataContext dctx, Privilege privilege, UserPrivilege userPrivilege)
        {
            DBHelper helper = new DBHelper();
            object sign = new object();
            if (dctx == null)
            {
                throw new ErrorParametros("Conecci\x00f3n", this);
            }
            if (privilege.UserGranter == null)
            {
                privilege.UserGranter = new Usuario();
            }
            if (userPrivilege == null)
            {
                userPrivilege = new UserPrivilege();
            }
            try
            {
                dctx.OpenConnection(sign);
                helper.Comm = dctx.CreateCommand();
            }
            catch (Exception)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services.DAO", "PrivilegeRetrieve", "Consultar", "Ha ocurrido un error al tratar de conectarse a la base de datos.");
            }
            StringBuilder builder = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            builder.Append(" SELECT PrivilegeID, UserGranterID, DataGranted, PrivilegeGrantedID, UserPrivilegeID, IsActive FROM Privilege ");
            if (privilege.PrivilegeID.HasValue)
            {
                builder2.Append("AND PrivilegeID = @Privilege_PrivilegeID ");
                helper.CrearParametro(TipoDato.ENTERO, "Privilege_PrivilegeID", privilege.PrivilegeID);
            }
            if (privilege.UserGranter.UsuarioID.HasValue)
            {
                builder2.Append("AND UserGranterID = @Privilege_UserGranter_UsuarioID ");
                helper.CrearParametro(TipoDato.ENTERO, "Privilege_UserGranter_UsuarioID", privilege.UserGranter.UsuarioID);
            }
            if (privilege.DateGranted.HasValue)
            {
                builder2.Append("AND DataGranted = @Privilege_DataGranted ");
                helper.CrearParametro(TipoDato.FECHA, "Privilege_DataGranted", privilege.DateGranted);
            }
            if (userPrivilege.UserPrivilegeID.HasValue)
            {
                builder2.Append("AND UserPrivilegeID = @UserPrivileged_UserPrivilegedID");
                helper.CrearParametro(TipoDato.ENTERO, "UserPrivileged_UserPrivilegedID", userPrivilege.UserPrivilegeID);
            }
            if (privilege.IsActive.HasValue)
            {
                builder2.Append("AND IsActive = @Privilege_IsActive");
                helper.CrearParametro(TipoDato.BOLEANO, "Privilege_IsActive", privilege.IsActive);
            }
            string str2 = builder2.ToString().Trim();
            if (str2.Length > 0)
            {
                if (str2.StartsWith("AND "))
                {
                    str2 = str2.Substring(4);
                }
                else if (str2.StartsWith("OR "))
                {
                    str2 = str2.Substring(3);
                }
                else if (str2.StartsWith(","))
                {
                    str2 = str2.Substring(1);
                }
                builder.Append(" WHERE " + str2);
            }
            builder.Append(" ORDER BY PrivilegeID ASC ");
            DataSet dataSet = new DataSet();
            DbDataAdapter adapter = dctx.CreateDataAdapter();
            adapter.SelectCommand = helper.Comm;
            try
            {
                adapter.SelectCommand.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                adapter.Fill(dataSet, "Privilege");
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services.DAO", "PrivilegeRetrieve", "Consultar", "Ha ocurrido un error al intentar obtener los registros. \n" + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
            return dataSet;
        }
    }
}

