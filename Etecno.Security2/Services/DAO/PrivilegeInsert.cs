﻿namespace Etecno.Security2.Services.DAO
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Biblioteca.Utileria;
    using Etecno.Security2.BO;
    using System;
    using System.Text;

    internal class PrivilegeInsert
    {
        public void Insertar(IDataContext dctx, Privilege privilege, UserPrivilege userPrivilege, int? PrivilegeGrantedID)
        {
            DBHelper helper = new DBHelper();
            if (dctx == null)
            {
                throw new ErrorParametros("Conecci\x00f3n", this);
            }
            string str = "";
            if (privilege == null)
            {
                str = str + ", Privilege";
            }
            if (userPrivilege == null)
            {
                str = str + ", UserPrivilege";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            if (privilege.UserGranter == null)
            {
                str = str + ", UserGranted";
            }
            if (privilege.PermissionGranted == null)
            {
                str = str + ", Granted";
            }
            if (!privilege.DateGranted.HasValue)
            {
                str = str + ",DataGranted";
            }
            if (!userPrivilege.UserPrivilegeID.HasValue)
            {
                str = str + ",UserPrivilegeID";
            }
            if (!PrivilegeGrantedID.HasValue)
            {
                str = str + ", PrivilegeGranted";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                helper.Comm = dctx.CreateCommand();
            }
            catch (Exception)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "PrivilegeInsert", "Insertar", "Ha ocurrido un error al tratar de conectarse a la base de datos.");
            }
            StringBuilder builder = new StringBuilder();
            builder.Append(" INSERT INTO Privilege (UserGranterID, DataGranted, PrivilegeGrantedID, UserPrivilegeID, IsActive )");
            builder.Append("VALUES (@Privilege_UserGrantedID, @Privilege_DataGranted, @PrivilegeGrantedID, @UserP_UserPrivilegeID, @Privilege_IsActive)");
            helper.CrearParametro(TipoDato.ENTERO, "Privilege_UserGrantedID", privilege.UserGranter.UsuarioID);
            helper.CrearParametro(TipoDato.FECHA, "Privilege_DataGranted", privilege.DateGranted);
            helper.CrearParametro(TipoDato.ENTERO, "PrivilegeGrantedID", PrivilegeGrantedID);
            helper.CrearParametro(TipoDato.ENTERO, "UserP_UserPrivilegeID", userPrivilege.UserPrivilegeID);
            helper.CrearParametro(TipoDato.BOLEANO, "Privilege_IsActive", privilege.IsActive);
            int num = 0;
            try
            {
                helper.Comm.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                num = helper.Comm.ExecuteNonQuery();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "PrivilegeInsert", "Insertar", "Ha ocurrido un error al intentar crear el registro. \n" + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
            if (num < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "PrivilegeInsert", "Insertar", "Ha ocurrido un error, no se insertaron registros.");
            }
        }
    }
}

