﻿namespace Etecno.Security2.Services.DAO
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Biblioteca.Utileria;
    using Etecno.Security2.BO;
    using System;
    using System.Text;

    internal class UsuarioActualizar
    {
        public void Actualizar(IDataContext dctx, Usuario usuario, Usuario anterior)
        {
            DBHelper helper = new DBHelper();
            if (dctx == null)
            {
                throw new ErrorParametros("Conecci\x00f3n", this);
            }
            string str = "";
            if (usuario == null)
            {
                str = str + " Usuario";
            }
            if (anterior == null)
            {
                str = str + ", Usuario Anterior";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            if (string.IsNullOrEmpty(usuario.Clave))
            {
                str = str + ", Clave";
            }
            if (!usuario.Estado.HasValue)
            {
                str = str + ", Estado";
            }
            if (string.IsNullOrEmpty(anterior.Clave))
            {
                str = str + ", Clave Anterior";
            }
            if (!anterior.Estado.HasValue)
            {
                str = str + ", Estado Anterior";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                helper.Comm = dctx.CreateCommand();
            }
            catch (Exception)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "UsuarioActualizar", "Actualizar", "Ha ocurrido un error al tratar de conectarse a la base de datos.");
            }
            StringBuilder builder = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            StringBuilder builder3 = new StringBuilder();
            builder.Append(" Update Usuario ");
            if (!string.IsNullOrEmpty(usuario.Nombre))
            {
                builder2.Append(" Nombre=@Nombre ");
                helper.CrearParametro(TipoDato.CADENA, "Nombre", usuario.Nombre);
            }
            if (!string.IsNullOrEmpty(usuario.Clave))
            {
                builder2.Append(", Clave=@Usuario_Clave ");
                helper.CrearParametro(TipoDato.CADENA, "Usuario_Clave", usuario.Clave);
            }
            if (usuario.Estado.HasValue)
            {
                builder2.Append(", EstadoUsuario =@Usuario_EstadoUsuario ");
                helper.CrearParametro(TipoDato.ENTERO, "Usuario_EstadoUsuario", (int) usuario.Estado.Value);
            }
            if (!string.IsNullOrEmpty(usuario.PublicKey))
            {
                builder2.Append(", PublicKey=@Usuario_PublicKey ");
                helper.CrearParametro(TipoDato.CADENA, "Usuario_PublicKey", usuario.PublicKey);
            }
            if (!string.IsNullOrEmpty(usuario.PrivateKey))
            {
                builder2.Append(", PrivateKey=@Usuario_PrivateKey ");
                helper.CrearParametro(TipoDato.CADENA, "Usuario_PrivateKey", usuario.PrivateKey);
            }
            if (!string.IsNullOrEmpty(usuario.CambioContraseña))
            {
                builder2.Append(", CambioContraseña=@Usuario_CambioContraseña ");
                helper.CrearParametro(TipoDato.CADENA, "Usuario_CambioContraseña", usuario.CambioContraseña);
            }
            //-----ANTERIOR-----
            builder3.Append(" UsuarioID =@Anterior_UsuarioID ");
            helper.CrearParametro(TipoDato.ENTERO, "Anterior_UsuarioID", anterior.UsuarioID);

            builder3.Append(" AND Nombre=@Anterior_Nombre ");
            helper.CrearParametro(TipoDato.CADENA, "Anterior_Nombre", anterior.Nombre);

            builder3.Append(" AND Clave=@Anterior_Clave ");
            helper.CrearParametro(TipoDato.CADENA, "Anterior_Clave", anterior.Clave);

            builder3.Append(" AND EstadoUsuario =@Anterior_EstadoUsuario ");
            if (!string.IsNullOrEmpty(anterior.PublicKey))
            {
                builder3.Append(" AND PublicKey=@Anterior_PublicKey ");
                helper.CrearParametro(TipoDato.CADENA, "Anterior_PublicKey", anterior.PublicKey);
            }
            if (!string.IsNullOrEmpty(anterior.PrivateKey))
            {
                builder3.Append(" AND PrivateKey=@Anterior_PrivateKey ");
                helper.CrearParametro(TipoDato.CADENA, "Anterior_PrivateKey", anterior.PrivateKey);
            }
            if (!string.IsNullOrEmpty(anterior.CambioContraseña))
            {
                builder3.Append(" AND CambioContraseña=@Anterior_CambioContraseña ");
                helper.CrearParametro(TipoDato.CADENA, "Anterior_CambioContraseña", anterior.CambioContraseña);
            }
            helper.CrearParametro(TipoDato.ENTERO, "Anterior_EstadoUsuario", (int) anterior.Estado.Value);
            builder.Append(" Set " + builder2.ToString().Trim() + " Where " + builder3.ToString().Trim());
            int num = 0;
            try
            {
                helper.Comm.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                num = helper.Comm.ExecuteNonQuery();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "UsuarioActualizar", "Actualizar", "Ha ocurrido un error al intentar crear el registro. \n" + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
            if (num < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "UsuarioActualizar", "Actualizar", "Ha ocurrido un error, no se Actualizaron registros.");
            }
        }
    }
}

