﻿namespace Etecno.Security2.Services.DAO
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Biblioteca.Utileria;
    using Etecno.Security2.BO;
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Text;

    internal class AplicationRetrieve
    {
        public DataSet Retrieve(IDataContext dctx, Aplication aplication)
        {
            DBHelper helper = new DBHelper();
            object sign = new object();
            if (dctx == null)
            {
                throw new ErrorParametros("Conecci\x00f3n", this);
            }
            if (aplication == null)
            {
                aplication = new Aplication();
            }
            try
            {
                dctx.OpenConnection(sign);
                helper.Comm = dctx.CreateCommand();
            }
            catch (Exception)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services.DAO", "AplicationRetrieve", "Consultar", "Ha ocurrido un error al tratar de conectarse a la base de datos.");
            }
            StringBuilder builder = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            builder.Append(" SELECT AplicationID, Name, Description, URL, Priority FROM Aplication ");
            if (aplication.AplicationID.HasValue)
            {
                builder2.Append("AND AplicationID = @Aplication_AplicationID ");
                helper.CrearParametro(TipoDato.ENTERO, "Aplication_AplicationID", aplication.AplicationID);
            }
            if (!string.IsNullOrEmpty(aplication.Name))
            {
                builder2.Append("AND Name LIKE @Aplication_Name ");
                helper.CrearParametro(TipoDato.CADENA, "Aplication_Name", aplication.Name);
            }
            if (!string.IsNullOrEmpty(aplication.Description))
            {
                builder2.Append("AND Description LIKE @Aplication_Description ");
                helper.CrearParametro(TipoDato.CADENA, "Aplication_Description", aplication.Description);
            }
            if (!string.IsNullOrEmpty(aplication.URL))
            {
                builder2.Append("AND URL LIKE @Aplication_URL ");
                helper.CrearParametro(TipoDato.CADENA, "Aplication_URL", aplication.URL);
            }
            if (aplication.Priority.HasValue)
            {
                builder2.Append("AND Priority = @Aplication_Priority ");
                helper.CrearParametro(TipoDato.ENTERO, "Aplication_Priority", aplication.Priority);
            }
            string str2 = builder2.ToString().Trim();
            if (str2.Length > 0)
            {
                if (str2.StartsWith("AND "))
                {
                    str2 = str2.Substring(4);
                }
                else if (str2.StartsWith("OR "))
                {
                    str2 = str2.Substring(3);
                }
                else if (str2.StartsWith(","))
                {
                    str2 = str2.Substring(1);
                }
                builder.Append(" WHERE " + str2);
            }
            builder.Append(" ORDER BY AplicationID ASC ");
            DataSet dataSet = new DataSet();
            DbDataAdapter adapter = dctx.CreateDataAdapter();
            adapter.SelectCommand = helper.Comm;
            try
            {
                adapter.SelectCommand.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                adapter.Fill(dataSet, "Aplication");
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services.DAO", "AplicationRetrieve", "Consultar", "Ha ocurrido un error al intentar obtener los registros. \n" + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
            return dataSet;
        }
    }
}

