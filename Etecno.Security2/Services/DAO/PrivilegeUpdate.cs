﻿namespace Etecno.Security2.Services.DAO
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Biblioteca.Utileria;
    using Etecno.Security2.BO;
    using System;
    using System.Text;

    internal class PrivilegeUpdate
    {
        public void Actualizar(IDataContext dctx, Privilege privilege, Privilege anterior, int? PrivilegeGrantedID, int? anteriorPrivilegeID)
        {
            DBHelper helper = new DBHelper();
            if (dctx == null)
            {
                throw new ErrorParametros("Conecci\x00f3n", this);
            }
            string str = "";
            if (privilege == null)
            {
                str = str + " Privilege";
            }
            if (anterior == null)
            {
                str = str + ", Privilege Anterior";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            if (privilege.UserGranter == null)
            {
                str = str + " Usuario Otorgador";
            }
            if (anterior.UserGranter == null)
            {
                str = str + ", Usuario AnteriorOtorgador";
            }
            if (!anterior.PrivilegeID.HasValue)
            {
                str = str + ", Anterior.PrivilegeID";
            }
            if (str.Length > 0)
            {
                throw new ErrorParametros(str.Substring(2), this);
            }
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                helper.Comm = dctx.CreateCommand();
            }
            catch (Exception)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "PrivilegeUpdate", "Actualizar", "Ha ocurrido un error al tratar de conectarse a la base de datos.");
            }
            StringBuilder builder = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            StringBuilder builder3 = new StringBuilder();
            builder.Append(" Update Privilege ");
            if (privilege.UserGranter.UsuarioID.HasValue)
            {
                builder2.Append(" UserGranterID=@UserGranterID ");
                helper.CrearParametro(TipoDato.CADENA, "UserGranterID", privilege.UserGranter.UsuarioID);
            }
            if (privilege.DateGranted.HasValue)
            {
                builder2.Append(", DataGranted =@DateGranted ");
                helper.CrearParametro(TipoDato.FECHA, "DateGranted", privilege.DateGranted);
            }
            if (privilege.IsActive.HasValue)
            {
                builder2.Append(", IsActive =@IsActive ");
                helper.CrearParametro(TipoDato.BOLEANO, "IsActive", privilege.IsActive);
            }
            if (PrivilegeGrantedID.HasValue)
            {
                builder2.Append(", PrivilegeGrantedID =@PrivilegeGrantedID ");
                helper.CrearParametro(TipoDato.ENTERO, "PrivilegeGrantedID", PrivilegeGrantedID);
            }
            builder3.Append(" PrivilegeID =@PrivilegeID ");
            helper.CrearParametro(TipoDato.ENTERO, "PrivilegeID", anterior.PrivilegeID);
            builder3.Append(" AND UserGranterID=@Anterior_UserGranterID ");
            helper.CrearParametro(TipoDato.ENTERO, "Anterior_UserGranterID", anterior.UserGranter.UsuarioID);
            builder3.Append(" AND DataGranted =@Anterior_DateGranted ");
            helper.CrearParametro(TipoDato.FECHA, "Anterior_DateGranted", anterior.DateGranted);
            if (anterior.IsActive.HasValue)
            {
                builder3.Append(" AND IsActive =@Anterior_IsActive ");
                helper.CrearParametro(TipoDato.BOLEANO, "Anterior_IsActive", anterior.IsActive);
            }
            else
            {
                builder3.Append(" AND IsActive IS NULL ");
            }
            builder3.Append(" AND PrivilegeGrantedID=@Anterior_PrivilegeGrantedID ");
            helper.CrearParametro(TipoDato.ENTERO, "Anterior_PrivilegeGrantedID", anteriorPrivilegeID);
            builder.Append(" Set " + builder2.ToString().Trim() + " Where " + builder3.ToString().Trim());
            int num = 0;
            try
            {
                helper.Comm.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                num = helper.Comm.ExecuteNonQuery();
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "PrivilegeUpdate", "Actualizar", "Ha ocurrido un error al intentar crear el registro. \n" + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
            if (num < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Servicios.DAO", "PrivilegeUpdate", "Actualizar", "Ha ocurrido un error, no se Actualizaron registros.");
            }
        }
    }
}

