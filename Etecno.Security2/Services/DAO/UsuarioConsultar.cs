﻿namespace Etecno.Security2.Services.DAO
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Biblioteca.Utileria;
    using Etecno.Security2.BO;
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Text;

    internal class UsuarioConsultar
    {
        public DataSet Consultar(IDataContext dctx, Usuario usuario)
        {
            DBHelper helper = new DBHelper();
            object sign = new object();
            if (dctx == null)
            {
                throw new ErrorParametros("Conecci\x00f3n", this);
            }
            if (usuario == null)
            {
                usuario = new Usuario();
            }
            try
            {
                dctx.OpenConnection(sign);
                helper.Comm = dctx.CreateCommand();
            }
            catch (Exception)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services.DAO", "UserRetrieve", "Consultar", "Ha ocurrido un error al tratar de conectarse a la base de datos.");
            }
            StringBuilder builder = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            builder.Append(" SELECT UsuarioID, Nombre, Clave, EstadoUsuario, PrivateKey, PublicKey, CambioContraseña FROM Usuario ");
            if (usuario.UsuarioID.HasValue)
            {
                builder2.Append("AND UsuarioID = @Usuario_UsuarioID ");
                helper.CrearParametro(TipoDato.ENTERO, "Usuario_UsuarioID", usuario.UsuarioID);
            }
            if (!string.IsNullOrEmpty(usuario.Nombre))
            {
                builder2.Append("AND Nombre = @Usuario_Nombre ");
                helper.CrearParametro(TipoDato.CADENA, "Usuario_Nombre", usuario.Nombre);
            }
            if (!string.IsNullOrEmpty(usuario.Clave))
            {
                builder2.Append("AND Clave = @Usuario_Clave ");
                helper.CrearParametro(TipoDato.CADENA, "Usuario_Clave", usuario.Clave);
            }
            if (usuario.Estado.HasValue)
            {
                builder2.Append("AND EstadoUsuario = @Usuario_EstadoUsuario ");
                helper.CrearParametro(TipoDato.ENTERO, "Usuario_EstadoUsuario", usuario.Estado);
            }
            if (!string.IsNullOrEmpty(usuario.PrivateKey))
            {
                builder2.Append("AND PrivateKey = @Usuario_PrivateKey ");
                helper.CrearParametro(TipoDato.CADENA, "Usuario_PrivateKey", usuario.PrivateKey);
            }
            if (!string.IsNullOrEmpty(usuario.PublicKey))
            {
                builder2.Append("AND PublicKey = @Usuario_PublicKey ");
                helper.CrearParametro(TipoDato.CADENA, "Usuario_PublicKey", usuario.PublicKey);
            }
            if (!string.IsNullOrEmpty(usuario.CambioContraseña))
            {
                builder2.Append("AND CambioContraseña = @Usuario_CambioContraseña ");
                helper.CrearParametro(TipoDato.CADENA, "Usuario_CambioContraseña", usuario.CambioContraseña);
            }
            string str2 = builder2.ToString().Trim();
            if (str2.Length > 0)
            {
                if (str2.StartsWith("AND "))
                {
                    str2 = str2.Substring(4);
                }
                else if (str2.StartsWith("OR "))
                {
                    str2 = str2.Substring(3);
                }
                else if (str2.StartsWith(","))
                {
                    str2 = str2.Substring(1);
                }
                builder.Append(" WHERE " + str2);
            }
            builder.Append(" ORDER BY UsuarioID ASC ");
            DataSet dataSet = new DataSet();
            DbDataAdapter adapter = dctx.CreateDataAdapter();
            adapter.SelectCommand = helper.Comm;
            try
            {
                adapter.SelectCommand.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                adapter.Fill(dataSet, "Usuario");
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services.DAO", "UserRetrieve", "Consultar", "Ha ocurrido un error al intentar obtener los registros. \n" + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
            return dataSet;
        }
    }
}

