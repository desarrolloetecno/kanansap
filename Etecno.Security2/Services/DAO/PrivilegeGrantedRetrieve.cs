﻿namespace Etecno.Security2.Services.DAO
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Biblioteca.Utileria;
    using Etecno.Security2.BO;
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Text;

    internal class PrivilegeGrantedRetrieve
    {
        public DataSet Retrieve(IDataContext dctx, int? privilegeGrantedID, Profile profile, Permission permission)
        {
            DBHelper helper = new DBHelper();
            object sign = new object();
            if (dctx == null)
            {
                throw new ErrorParametros("Conecci\x00f3n", this);
            }
            if (profile == null)
            {
                profile = new Profile();
            }
            if (permission == null)
            {
                permission = new Permission();
            }
            try
            {
                dctx.OpenConnection(sign);
                helper.Comm = dctx.CreateCommand();
            }
            catch (Exception)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services.DAO", "PrivilegeGrantedRetrieve", "Consultar", "Ha ocurrido un error al tratar de conectarse a la base de datos.");
            }
            StringBuilder builder = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            builder.Append(" SELECT PrivilegeGrantedID, GrantableTipeID, PermissionID, ProfileID FROM PrivilegeGranted ");
            if (privilegeGrantedID.HasValue)
            {
                builder2.Append("AND PrivilegeGrantedID = @PrivilegeGrantedID");
                helper.CrearParametro(TipoDato.ENTERO, "PrivilegeGrantedID", privilegeGrantedID);
            }
            if (profile.ProfileID.HasValue)
            {
                builder2.Append("AND ProfileID = @Profile_ProfileID ");
                helper.CrearParametro(TipoDato.ENTERO, "Profile_ProfileID", profile.ProfileID);
            }
            if (permission.PermissionID.HasValue)
            {
                builder2.Append("AND PermissionID = @Permission_PermissionID ");
                helper.CrearParametro(TipoDato.ENTERO, "Permission_PermissionID", permission.PermissionID);
            }
            string str2 = builder2.ToString().Trim();
            if (str2.Length > 0)
            {
                if (str2.StartsWith("AND "))
                {
                    str2 = str2.Substring(4);
                }
                else if (str2.StartsWith("OR "))
                {
                    str2 = str2.Substring(3);
                }
                else if (str2.StartsWith(","))
                {
                    str2 = str2.Substring(1);
                }
                builder.Append(" WHERE " + str2);
            }
            builder.Append(" ORDER BY PrivilegeGrantedID ASC ");
            DataSet dataSet = new DataSet();
            DbDataAdapter adapter = dctx.CreateDataAdapter();
            adapter.SelectCommand = helper.Comm;
            try
            {
                adapter.SelectCommand.CommandText = builder.Replace("@", dctx.ParameterSymbol).ToString();
                adapter.Fill(dataSet, "PrivilegeGranted");
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services.DAO", "PrivilegeGrantedRetrieve", "Consultar", "Ha ocurrido un error al intentar obtener los registros. \n" + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
            return dataSet;
        }
    }
}

