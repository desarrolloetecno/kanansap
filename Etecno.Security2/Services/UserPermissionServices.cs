﻿namespace Etecno.Security2.Services
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Security2.BO;
    using Etecno.Security2.Services.DA;
    using Etecno.Security2.Services.DAO;
    using System;
    using System.Data;
    using System.Collections.Generic;

    public class UserPermissionServices
    {
        public static bool HashPermissions(IDataContext dctx, Usuario usuario, string actionName, string aplicationName)
        {
            UserPermissionsRetrieve retrieve = new UserPermissionsRetrieve();
            Usuario usuario2 = new Usuario();
            usuario2 = (Usuario) usuario.Clone();
            usuario2.Estado = EstadoUsuario.Active;
            Aplication aplication = new Aplication();
            Etecno.Security2.BO.Action action = new Etecno.Security2.BO.Action();
            if (!string.IsNullOrEmpty(actionName))
            {
                action.Name = actionName;
            }
            if (!string.IsNullOrEmpty(aplicationName))
            {
                aplication.Name = aplicationName;
            }
            DataSet set = new UserPermissionServices().Retrieve(dctx, usuario2, action, aplication);
            if (!set.Tables.Contains("UserPermission"))
            {
                return false;
            }
            if (set.Tables["UserPermission"].Rows.Count < 1)
            {
                return false;
            }
            return true;
        }

        public bool HashUserPermissions(DataSet ds)
        {
            if (!ds.Tables.Contains("UserPermission"))
            {
                return false;
            }
            if (ds.Tables["UserPermission"].Rows.Count < 1)
            {
                return false;
            }
            return true;
        }

        public DataSet Retrieve(IDataContext dctx, Usuario usuario, Etecno.Security2.BO.Action action, Aplication aplication)
        {
            UserPermissionsRetrieve retrieve = new UserPermissionsRetrieve();
            return retrieve.Consultar(dctx, usuario, action, aplication);
        }

        public DataSet UserPermissions(IDataContext dctx, Usuario usuario)
        {
            Etecno.Security2.Services.DAO.UserPermissions permissions = new Etecno.Security2.Services.DAO.UserPermissions();
            return permissions.Consultar(dctx, usuario);
        }

        public DataSet UserProfiles(IDataContext dctx, Usuario usuario)
        {
            Etecno.Security2.Services.DAO.UserProfiles profiles = new Etecno.Security2.Services.DAO.UserProfiles();
            return profiles.Consultar(dctx, usuario);
        }

        /// <summary>
        /// Obtiene todos los permisos
        /// </summary>
        /// <param name="dctx">base de datos</param>
        /// <param name="userID">id del usuario</param>
        /// <returns></returns>
        public List<Permission> getFullPermissionByUser(IDataContext dctx, int userID)
        {
             List<Permission> permissions= new List<Permission>();
            UserPrivilege uprivilege = new UserPrivilege();
            UserPrivilegeServices uspriv = new UserPrivilegeServices();
            DataSet ds=  uspriv.Retrieve(dctx,new UserPrivilege(){Owner= new Usuario(){UsuarioID=userID}});
            uprivilege=uspriv.LastDataRowToUserPrivilege(ds);
            uprivilege = uspriv.RetrieveComplete(dctx, (int)uprivilege.UserPrivilegeID);
            permissions = uprivilege.PermissionList;
            return permissions;
        }
    }
}

