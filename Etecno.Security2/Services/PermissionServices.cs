﻿namespace Etecno.Security2.Services
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Security2.BO;
    using Etecno.Security2.Services.DAO;
    using System;
    using System.Data;

    public class PermissionServices
    {
        public Permission DataRowToPermission(DataRow row)
        {
            int? nullable;
            Permission permission = new Permission {
                Operation = new Etecno.Security2.BO.Action(),
                Program = new Aplication()
            };
            if (row.IsNull("PermissionID"))
            {
                nullable = null;
                permission.PermissionID = nullable;
            }
            else
            {
                permission.PermissionID = new int?((int) Convert.ChangeType(row["PermissionID"], typeof(int)));
            }
            if (row.IsNull("ActionID"))
            {
                nullable = null;
                permission.Operation.ActionID = nullable;
            }
            else
            {
                permission.Operation.ActionID = new int?((int) Convert.ChangeType(row["ActionID"], typeof(int)));
            }
            if (row.IsNull("AplicationID"))
            {
                permission.Program.AplicationID = null;
                return permission;
            }
            permission.Program.AplicationID = new int?((int) Convert.ChangeType(row["AplicationID"], typeof(int)));
            return permission;
        }

        public bool HashPermission(DataSet ds)
        {
            if (!ds.Tables.Contains("Permission"))
            {
                return false;
            }
            if (ds.Tables["Permission"].Rows.Count < 1)
            {
                return false;
            }
            return true;
        }

        public void Insert(IDataContext dctx, Permission permission)
        {
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                dctx.BeginTransaction(sign);
                new PermissionInsert().Insert(dctx, permission);
                dctx.CommitTransaction(sign);
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.RollbackTransaction(sign);
                }
                catch (Exception)
                {
                }
                throw new Exception("MessageServices: Hubo un error al Ejecutar la consulta. " + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
        }

        public Permission LastDataRowToPermission(DataSet ds)
        {
            if (!ds.Tables.Contains("Permission"))
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "PermissionServices", "LastDataRowToPermission", "El DataSet no tiene la tabla Permission");
            }
            int count = ds.Tables["Permission"].Rows.Count;
            if (count < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "PermissionServices", "LastDataRowToPermission", "El DataSet no tiene Filas");
            }
            return this.DataRowToPermission(ds.Tables["Permission"].Rows[count - 1]);
        }

        public DataSet Retrieve(IDataContext dctx, Permission permission)
        {
            PermissionRetrieve retrieve = new PermissionRetrieve();
            return retrieve.Retrieve(dctx, permission);
        }

        public Permission RetrieveComplete(IDataContext dctx, int permissionID)
        {
            AplicationServices services = new AplicationServices();
            ActionServices services2 = new ActionServices();
            Permission permission = new Permission {
                PermissionID = new int?(permissionID)
            };
            DataSet ds = this.Retrieve(dctx, permission);
            if (this.HashPermission(ds))
            {
                permission = this.LastDataRowToPermission(ds);
            }
            DataSet set2 = services.Retrieve(dctx, permission.Program);
            if (services.HashAplication(set2))
            {
                permission.Program = services.LastDataRowToAplication(set2);
            }
            DataSet set3 = services2.Retrieve(dctx, permission.Operation);
            if (services2.HashAction(set3))
            {
                permission.Operation = services2.LastDataRowToAction(set3);
            }
            return permission;
        }
    }
}

