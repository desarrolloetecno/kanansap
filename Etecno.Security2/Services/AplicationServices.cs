﻿namespace Etecno.Security2.Services
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Security2.BO;
    using Etecno.Security2.Services.DAO;
    using System;
    using System.Data;

    public class AplicationServices
    {
        public Aplication DataRowToAplication(DataRow row)
        {
            Aplication aplication = new Aplication();
            if (row.IsNull("AplicationID"))
            {
                aplication.AplicationID = null;
            }
            else
            {
                aplication.AplicationID = new int?((int) Convert.ChangeType(row["AplicationID"], typeof(int)));
            }
            if (row.IsNull("Name"))
            {
                aplication.Name = null;
            }
            else
            {
                aplication.Name = (string) Convert.ChangeType(row["Name"], typeof(string));
            }
            if (row.IsNull("Description"))
            {
                aplication.Description = null;
            }
            else
            {
                aplication.Description = (string) Convert.ChangeType(row["Description"], typeof(string));
            }
            if (row.IsNull("URL"))
            {
                aplication.URL = null;
            }
            else
            {
                aplication.URL = (string) Convert.ChangeType(row["URL"], typeof(string));
            }
            if (row.IsNull("Priority"))
            {
                aplication.Priority = null;
                return aplication;
            }
            aplication.Priority = new int?((int) Convert.ChangeType(row["Priority"], typeof(int)));
            return aplication;
        }

        public bool HashAplication(DataSet ds)
        {
            if (!ds.Tables.Contains("Aplication"))
            {
                return false;
            }
            if (ds.Tables["Aplication"].Rows.Count < 1)
            {
                return false;
            }
            return true;
        }

        public Aplication LastDataRowToAplication(DataSet ds)
        {
            if (!ds.Tables.Contains("Aplication"))
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "AplicationServices", "LastDataRowToAplication", "El DataSet no tiene la tabla Aplication");
            }
            int count = ds.Tables["Aplication"].Rows.Count;
            if (count < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "AplicationServices", "LastDataRowToAplication", "El DataSet no tiene Filas");
            }
            return this.DataRowToAplication(ds.Tables["Aplication"].Rows[count - 1]);
        }

        public DataSet Retrieve(IDataContext dctx, Aplication aplication)
        {
            AplicationRetrieve retrieve = new AplicationRetrieve();
            return retrieve.Retrieve(dctx, aplication);
        }
    }
}

