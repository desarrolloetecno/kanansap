﻿namespace Etecno.Security2.Services
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Security2.BO;
    using Etecno.Security2.Services.DAO;
    using System;
    using System.Data;

    public class UserPrivilegeServices
    {
        public UserPrivilege DataRowToUserPrivilege(DataRow row)
        {
            UserPrivilege privilege = new UserPrivilege {
                Owner = new Usuario()
            };
            if (row.IsNull("UserPrivilegeID"))
            {
                privilege.UserPrivilegeID = null;
            }
            else
            {
                privilege.UserPrivilegeID = new int?((int) Convert.ChangeType(row["UserPrivilegeID"], typeof(int)));
            }
            if (row.IsNull("UserID"))
            {
                privilege.Owner.UsuarioID = null;
                return privilege;
            }
            privilege.Owner.UsuarioID = new int?((int) Convert.ChangeType(row["UserID"], typeof(int)));
            return privilege;
        }

        public bool HashUserPrivilege(DataSet ds)
        {
            if (!ds.Tables.Contains("UserPrivilege"))
            {
                return false;
            }
            if (ds.Tables["UserPrivilege"].Rows.Count < 1)
            {
                return false;
            }
            return true;
        }

        public void Insertar(IDataContext dctx, UserPrivilege userPrivilege)
        {
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                dctx.BeginTransaction(sign);
                new UserPrivilegeInsert().Insertar(dctx, userPrivilege);
                dctx.CommitTransaction(sign);
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.RollbackTransaction(sign);
                }
                catch (Exception)
                {
                }
                throw new Exception("MessageServices: Hubo un error al Ejecutar la consulta. " + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
        }

        public void InsertComplete(IDataContext dctx, UserPrivilege userPrivilege)
        {
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                dctx.BeginTransaction(sign);
                PrivilegeServices services = new PrivilegeServices();
                UsuarioServicios servicios = new UsuarioServicios();
                servicios.Insertar(dctx, userPrivilege.Owner);
                DataSet ds = servicios.Retrieve(dctx, userPrivilege.Owner);
                if (servicios.HashUsuario(ds))
                {
                    userPrivilege.Owner = servicios.LastDataRowToUser(ds);
                    this.Insertar(dctx, userPrivilege);
                    DataSet set2 = this.Retrieve(dctx, userPrivilege);
                    if (this.HashUserPrivilege(set2))
                    {
                        UserPrivilege privilege = this.LastDataRowToUserPrivilege(set2);
                        if (userPrivilege.UserPermission == null)
                        {
                            throw new Exception("El Usuario debe estar ligado a un Privilegio");
                        }
                        foreach (Privilege privilege2 in userPrivilege.UserPermission)
                        {
                            services.InsertComplete(dctx, privilege2, privilege);
                        }
                    }
                }
                dctx.CommitTransaction(sign);
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.RollbackTransaction(sign);
                }
                catch (Exception)
                {
                }
                throw new Exception("MessageServices: Hubo un error al Ejecutar la consulta. " + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
        }

        public UserPrivilege LastDataRowToUserPrivilege(DataSet ds)
        {
            if (!ds.Tables.Contains("UserPrivilege"))
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "UserPrivilegeServices", "LastDataRowToUserPrivilege", "El DataSet no tiene la tabla UserPrivilege");
            }
            int count = ds.Tables["UserPrivilege"].Rows.Count;
            if (count < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "UserPrivilegeServices", "LastDataRowToUserPrivilege", "El DataSet no tiene Filas");
            }
            return this.DataRowToUserPrivilege(ds.Tables["UserPrivilege"].Rows[count - 1]);
        }

        public DataSet Retrieve(IDataContext dctx, UserPrivilege userPrivilege)
        {
            UserPrivilegeRetrieve retrieve = new UserPrivilegeRetrieve();
            return retrieve.Retrieve(dctx, userPrivilege);
        }

        public UserPrivilege RetrieveComplete(IDataContext dctx, int userPrivilegeID)
        {
            UsuarioServicios servicios = new UsuarioServicios();
            PrivilegeServices services = new PrivilegeServices();
            UserPrivilege userPrivilege = new UserPrivilege();
            Privilege item = new Privilege();
            userPrivilege.UserPrivilegeID = new int?(userPrivilegeID);
            DataSet ds = this.Retrieve(dctx, userPrivilege);
            if (this.HashUserPrivilege(ds))
            {
                userPrivilege = this.LastDataRowToUserPrivilege(ds);
            }
            DataSet set2 = servicios.Retrieve(dctx, userPrivilege.Owner);
            if (servicios.HashUsuario(set2))
            {
                userPrivilege.Owner = servicios.LastDataRowToUser(set2);
            }
            DataSet set3 = services.Retrieve(dctx, new Privilege(), userPrivilege);
            if (services.HashPrivilege(set3))
            {
                foreach (DataRow row in set3.Tables["Privilege"].Rows)
                {
                    item = services.DataRowToPrivilege(row);
                    item = services.RetrieveComplete(dctx, item.PrivilegeID.Value);
                    userPrivilege.UserPermission.Add(item);
                }
            }
            return userPrivilege;
        }
    }
}

