﻿namespace Etecno.Security2.Services
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Security2.BO;
    using Etecno.Security2.Services.DA;
    using System;
    using System.Collections.Generic;
    using System.Data;

    public class ProfilePermissionServices
    {
        public Permission DataRowToPermission(DataRow row)
        {
            int? nullable;
            Permission permission = new Permission {
                Operation = new Etecno.Security2.BO.Action(),
                Program = new Aplication()
            };
            if (row.IsNull("PermissionID"))
            {
                nullable = null;
                permission.PermissionID = nullable;
            }
            else
            {
                permission.PermissionID = new int?((int) Convert.ChangeType(row["PermissionID"], typeof(int)));
            }
            if (row.IsNull("ActionID"))
            {
                nullable = null;
                permission.Operation.ActionID = nullable;
            }
            else
            {
                permission.Operation.ActionID = new int?((int) Convert.ChangeType(row["ActionID"], typeof(int)));
            }
            if (row.IsNull("AplicationID"))
            {
                permission.Program.AplicationID = null;
                return permission;
            }
            permission.Program.AplicationID = new int?((int) Convert.ChangeType(row["AplicationID"], typeof(int)));
            return permission;
        }

        public Profile DataRowToProfile(DataRow row)
        {
            Profile profile = new Profile();
            if (row.IsNull("ProfileID"))
            {
                profile.ProfileID = null;
            }
            else
            {
                profile.ProfileID = new int?((int) Convert.ChangeType(row["ProfileID"], typeof(int)));
            }
            if (row.IsNull("Name"))
            {
                profile.Name = null;
            }
            else
            {
                profile.Name = (string) Convert.ChangeType(row["Name"], typeof(string));
            }
            if (row.IsNull("Description"))
            {
                profile.Description = null;
                return profile;
            }
            profile.Description = (string) Convert.ChangeType(row["Description"], typeof(string));
            return profile;
        }

        public void Delete(IDataContext dctx, int? PermissionByProfileID)
        {
            new PermissionByProfileDelete().Delete(dctx, PermissionByProfileID);
        }

        public bool HashPermission(DataSet ds)
        {
            if (!ds.Tables.Contains("Permission"))
            {
                return false;
            }
            if (ds.Tables["Permission"].Rows.Count < 1)
            {
                return false;
            }
            return true;
        }

        public bool HashPermissionprofile(DataSet ds)
        {
            if (!ds.Tables.Contains("ProfilePermission"))
            {
                return false;
            }
            if (ds.Tables["ProfilePermission"].Rows.Count < 1)
            {
                return false;
            }
            return true;
        }

        public void Insert(IDataContext dctx, Profile profile, Permission permission)
        {
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                dctx.BeginTransaction(sign);
                new PermissionByProfileInsert().Insert(dctx, profile, permission);
                dctx.CommitTransaction(sign);
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.RollbackTransaction(sign);
                }
                catch (Exception)
                {
                }
                throw new Exception("MessageServices: Hubo un error al Ejecutar la consulta. " + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
        }

        public Permission LastDataRowToPermission(DataSet ds)
        {
            if (!ds.Tables.Contains("PermissionProfile"))
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "PermissionProfileServices", "LastDataRowToPermission", "El DataSet no tiene la tabla Action");
            }
            int count = ds.Tables["PermissionProfile"].Rows.Count;
            if (count < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "PermissionProfileServices", "LastDataRowToPermission", "El DataSet no tiene Filas");
            }
            return this.DataRowToPermission(ds.Tables["PermissionProfile"].Rows[count - 1]);
        }

        public Profile LastDataRowToProfile(DataSet ds)
        {
            if (!ds.Tables.Contains("Profile"))
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "PermissionProfileServices", "LastDataRowToProfile", "El DataSet no tiene la tabla ProfilePermission");
            }
            int count = ds.Tables["Profile"].Rows.Count;
            if (count < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "PermissionProfileServices", "LastDataRowToProfile", "El DataSet no tiene Filas");
            }
            return this.DataRowToProfile(ds.Tables["Profile"].Rows[count - 1]);
        }

        public List<Permission> Permissions(IDataContext dctx, Profile profile)
        {
            PermissionServices services = new PermissionServices();
            List<Permission> list = new List<Permission>();
            DataSet ds = this.Retrieve(dctx, null, null, profile);
            if (this.HashPermissionprofile(ds))
            {
                foreach (DataRow row in ds.Tables["ProfilePermission"].Rows)
                {
                    Permission item = new Permission();
                    item = this.DataRowToPermission(row);
                    item = services.RetrieveComplete(dctx, item.PermissionID.Value);
                    list.Add(item);
                }
            }
            return list;
        }

        public List<Profile> Profiles(IDataContext dctx, Permission permission)
        {
            ProfileServices services = new ProfileServices();
            List<Profile> list = new List<Profile>();
            DataSet ds = this.Retrieve(dctx, null, permission, null);
            if (this.HashPermissionprofile(ds))
            {
                foreach (DataRow row in ds.Tables["ProfilePermission"].Rows)
                {
                    Profile profile = new Profile {
                        ProfileID = new int?((int) Convert.ChangeType(row["ProfileID"], typeof(int)))
                    };
                    DataSet set2 = services.Retrieve(dctx, profile);
                    if (services.HashProfile(set2))
                    {
                        profile = this.LastDataRowToProfile(set2);
                    }
                    profile = services.RetriveComplete(dctx, profile.ProfileID.Value);
                    list.Add(profile);
                }
            }
            return list;
        }

        public DataSet Retrieve(IDataContext dctx, int? permissionByProfileID, Permission permission, Profile profile)
        {
            ProfilePermissionRetrieve retrieve = new ProfilePermissionRetrieve();
            return retrieve.Retrieve(dctx, permissionByProfileID, permission, profile);
        }
    }
}

