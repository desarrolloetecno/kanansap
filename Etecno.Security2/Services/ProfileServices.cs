﻿namespace Etecno.Security2.Services
{
    using Etecno.Biblioteca.AccesoDatos;
    using Etecno.Biblioteca.Errores;
    using Etecno.Security2.BO;
    using Etecno.Security2.Services.DAO;
    using System;
    using System.Data;

    public class ProfileServices
    {
        public Profile DataRowToProfile(DataRow row)
        {
            Profile profile = new Profile();
            if (row.IsNull("ProfileID"))
            {
                profile.ProfileID = null;
            }
            else
            {
                profile.ProfileID = new int?((int) Convert.ChangeType(row["ProfileID"], typeof(int)));
            }
            if (row.IsNull("Name"))
            {
                profile.Name = null;
            }
            else
            {
                profile.Name = (string) Convert.ChangeType(row["Name"], typeof(string));
            }
            if (row.IsNull("Description"))
            {
                profile.Description = null;

            }
            else
            {
                profile.Description = (string)Convert.ChangeType(row["Description"], typeof(string));
            }

            if (row.IsNull("EmpresaID"))
                profile.EmpresaID = null;
            else
                profile.EmpresaID = (Int32)Convert.ChangeType(row["EmpresaID"], typeof(Int32));
          
            return profile;
        }

        public bool HashProfile(DataSet ds)
        {
            if (!ds.Tables.Contains("Profile"))
            {
                return false;
            }
            if (ds.Tables["Profile"].Rows.Count < 1)
            {
                return false;
            }
            return true;
        }

        public void Insert(IDataContext dctx, Profile profile)
        {
            object sign = new object();
            try
            {
                dctx.OpenConnection(sign);
                dctx.BeginTransaction(sign);
                new ProfileInsert().Insert(dctx, profile);
                dctx.CommitTransaction(sign);
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                try
                {
                    dctx.RollbackTransaction(sign);
                }
                catch (Exception)
                {
                }
                throw new Exception("MessageServices: Hubo un error al Ejecutar la consulta. " + message);
            }
            finally
            {
                try
                {
                    dctx.CloseConnection(sign);
                }
                catch (Exception)
                {
                }
            }
        }

        public Profile LastDataRowToProfile(DataSet ds)
        {
            if (!ds.Tables.Contains("Profile"))
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "ProfileServices", "LastDataRowToProfile", "El DataSet no tiene la tabla Profile");
            }
            int count = ds.Tables["Profile"].Rows.Count;
            if (count < 1)
            {
                throw new ErrorEstandar(TipoError.ERROR, "Etecno.Security2.Services", "ProfileServices", "LastDataRowToProfile", "El DataSet no tiene Filas");
            }
            return this.DataRowToProfile(ds.Tables["Profile"].Rows[count - 1]);
        }


        /// <summary>
        /// Retrieve all probiles you want
        /// </summary>
        /// <param name="dctx"></param>
        /// <param name="profile"></param>
        /// <returns></returns>
        public DataSet Retrieve(IDataContext dctx, Profile profile)
        {
            ProfileRetrieve retrieve = new ProfileRetrieve();
            return retrieve.Retrieve(dctx, profile,false);
        }

        /// <summary>
        /// Retrieve the profiles by owner to system or kanan
        /// </summary>
        /// <param name="dctx"></param>
        /// <param name="profile"></param>
        /// <param name="IsOwnerKanan"></param>
        /// <returns></returns>
        public DataSet Retrieve(IDataContext dctx, Profile profile,bool IsOwnerKanan)
        {
            ProfileRetrieve retrieve = new ProfileRetrieve();
            return retrieve.Retrieve(dctx, profile, IsOwnerKanan);
        }

        public Profile RetriveComplete(IDataContext dctx, int profileID)
        {
            ProfilePermissionServices services = new ProfilePermissionServices();
            Profile profile = new Profile {
                ProfileID = new int?(profileID)
            };
            DataSet ds = this.Retrieve(dctx, profile);
            if (this.HashProfile(ds))
            {
                profile = this.LastDataRowToProfile(ds);
            }
            profile.Permissions = services.Permissions(dctx, profile);
            return profile;
        }
    }
}

