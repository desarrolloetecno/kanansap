﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kanan.Vehiculos.BO2;
using Kanan.Llantas.BO2;
using SAPbobsCOM;

namespace KananSAP.Llantas.Data
{
    public class ILlantaSAP
    {
        #region Propiedades

        public ILlanta llanta { get; set; }
        public UserTable oUDT { get; set; }
        public Items oItem { get; set; }
        public string Code { get; set; }
        public string ItemCode { get; set; }
        public int? tipo { get; set; }
        public int? Sincronizado { get; set; }
        public Guid? UUID { get; set; }
        private Company company;

        //Migracion Hana
        Recordset Insertar;
        String Parametro;
        private String NombreTabla;
        bool EsSQL;

        //Se agregó para ser visible desde cualquier lugar consulta.
        public String NombreTablaILlantaSAP = String.Empty;

        public bool ExistUDT()
        {
            return oUDT.GetByKey(this.Code);
        }
        #endregion

        #region Constructor
        public ILlantaSAP(SAPbobsCOM.Company company, int _tipo)
        {
            this.company = company;
            this.tipo = _tipo;
            switch (tipo)
            {
                case 1:
                    llanta = new Vehiculo();
                    break;
                case 2:
                    llanta = new Caja();
                    break;
            }
            this.oItem = (SAPbobsCOM.Items)company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
            try
            {
                this.oUDT = company.UserTables.Item("VSKF_ILLANTA");

                //Migración HANA
                this.Insertar = (Recordset)this.company.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.EsSQL = company.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.NombreTabla = EsSQL ? "[@VSKF_ILLANTA]" : @"""@VSKF_ILLANTA""";
                //Se maneja en una variable pública para poder utilizar desde cualquier método.
                this.NombreTablaILlantaSAP = this.NombreTabla;
            }
            catch (Exception)
            {
                this.oUDT = null;
            }
        }
        #endregion

        #region Metodos

        public void LlantaToUDT()
        {
            oUDT.Code = llanta.ILlantaID != null ? this.llanta.ID.ToString() : "0";
            oUDT.Name = this.llanta.ID.ToString();
            this.oUDT.UserFields.Fields.Item("U_KmsRecorridos").Value = llanta.KilometrosRecorridos;
            if (llanta.ubicacion1 != null && llanta.ubicacion1.LlantaID == null)
                this.oUDT.UserFields.Fields.Item("U_Ubicacion1").Value = llanta.ubicacion1.LlantaID;
            if (llanta.ubicacion2 != null && llanta.ubicacion2.LlantaID == null)
                this.oUDT.UserFields.Fields.Item("U_Ubicacion2").Value = llanta.ubicacion2.LlantaID;
            if (llanta.ubicacion3 != null && llanta.ubicacion3.LlantaID == null)
                this.oUDT.UserFields.Fields.Item("U_Ubicacion3").Value = llanta.ubicacion3.LlantaID;
            if (llanta.ubicacion4 != null && llanta.ubicacion4.LlantaID == null)
                this.oUDT.UserFields.Fields.Item("U_Ubicacion4").Value = llanta.ubicacion4.LlantaID;
            if (llanta.ubicacion5 != null && llanta.ubicacion5.LlantaID == null)
                this.oUDT.UserFields.Fields.Item("U_Ubicacion5").Value = llanta.ubicacion5.LlantaID;
            if (llanta.ubicacion6 != null && llanta.ubicacion6.LlantaID == null)
                this.oUDT.UserFields.Fields.Item("U_Ubicacion6").Value = llanta.ubicacion6.LlantaID;
            if (llanta.ubicacion7 != null && llanta.ubicacion7.LlantaID == null)
                this.oUDT.UserFields.Fields.Item("U_Ubicacion7").Value = llanta.ubicacion7.LlantaID;
            if (llanta.ubicacion8 != null && llanta.ubicacion8.LlantaID == null)
                this.oUDT.UserFields.Fields.Item("U_Ubicacion8").Value = llanta.ubicacion8.LlantaID;
            if (llanta.ubicacion9 != null && llanta.ubicacion9.LlantaID == null)
                this.oUDT.UserFields.Fields.Item("U_Ubicacion9").Value = llanta.ubicacion9.LlantaID;
            if (llanta.ubicacion10 != null && llanta.ubicacion10.LlantaID == null)
                this.oUDT.UserFields.Fields.Item("U_Ubicacion10").Value = llanta.ubicacion10.LlantaID;
            if (llanta.ubicacion11 != null && llanta.ubicacion11.LlantaID == null)
                this.oUDT.UserFields.Fields.Item("U_Ubicacion11").Value = llanta.ubicacion11.LlantaID;
            if (llanta.ubicacion12 != null && llanta.ubicacion12.LlantaID == null)
                this.oUDT.UserFields.Fields.Item("U_Ubicacion12").Value = llanta.ubicacion12.LlantaID;
            if (llanta.ubicacion13 != null && llanta.ubicacion13.LlantaID == null)
                this.oUDT.UserFields.Fields.Item("U_Ubicacion13").Value = llanta.ubicacion13.LlantaID;
            if (llanta.ubicacion14 != null && llanta.ubicacion14.LlantaID == null)
                this.oUDT.UserFields.Fields.Item("U_Ubicacion14").Value = llanta.ubicacion14.LlantaID;
            if (llanta.ubicacion15 != null && llanta.ubicacion15.LlantaID == null)
                this.oUDT.UserFields.Fields.Item("U_Ubicacion15").Value = llanta.ubicacion15.LlantaID;
            if (llanta.ubicacion16 != null && llanta.ubicacion16.LlantaID == null)
                this.oUDT.UserFields.Fields.Item("U_Ubicacion16").Value = llanta.ubicacion16.LlantaID;
            if (llanta.ubicacion17 != null && llanta.ubicacion17.LlantaID == null)
                this.oUDT.UserFields.Fields.Item("U_Ubicacion17").Value = llanta.ubicacion17.LlantaID;
            if (llanta.ubicacion18 != null && llanta.ubicacion18.LlantaID == null)
                this.oUDT.UserFields.Fields.Item("U_Ubicacion18").Value = llanta.ubicacion18.LlantaID;
            if (tipo == 1 && llanta.ID != null )
                this.oUDT.UserFields.Fields.Item("U_VehiculoID").Value = this.llanta.ILlantaID;
            if (tipo == 2 && llanta.ID != null)
                this.oUDT.UserFields.Fields.Item("U_ActivoID").Value = this.llanta.ILlantaID;
            if (Sincronizado != null)
                this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Sincronizado;
            if (UUID != null && UUID != new Guid())
                this.oUDT.UserFields.Fields.Item("U_UUID").Value = this.UUID.ToString();
        }

        public void UDTToILlanta()
        {
            

            this.llanta.ID = Convert.ToInt32(this.oUDT.Code);
            this.llanta.KilometrosRecorridos = this.oUDT.UserFields.Fields.Item("U_KmsRecorridos").Value != null  ? null : this.oUDT.UserFields.Fields.Item("U_KmsRecorridos").Value;
            this.llanta.ubicacion1.LlantaID = this.oUDT.UserFields.Fields.Item("U_Ubicacion1").Value;
            this.llanta.ubicacion2.LlantaID = this.oUDT.UserFields.Fields.Item("U_Ubicacion2").Value;
            this.llanta.ubicacion3.LlantaID = this.oUDT.UserFields.Fields.Item("U_Ubicacion3").Value;
            this.llanta.ubicacion4.LlantaID = this.oUDT.UserFields.Fields.Item("U_Ubicacion4").Value;
            this.llanta.ubicacion5.LlantaID = this.oUDT.UserFields.Fields.Item("U_Ubicacion5").Value;
            this.llanta.ubicacion6.LlantaID = this.oUDT.UserFields.Fields.Item("U_Ubicacion6").Value;
            this.llanta.ubicacion7.LlantaID = this.oUDT.UserFields.Fields.Item("U_Ubicacion7").Value;
            this.llanta.ubicacion8.LlantaID = this.oUDT.UserFields.Fields.Item("U_Ubicacion8").Value;
            this.llanta.ubicacion9.LlantaID = this.oUDT.UserFields.Fields.Item("U_Ubicacion9").Value;
            this.llanta.ubicacion10.LlantaID = this.oUDT.UserFields.Fields.Item("U_Ubicacion10").Value;
            this.llanta.ubicacion11.LlantaID = this.oUDT.UserFields.Fields.Item("U_Ubicacion11").Value;
            this.llanta.ubicacion12.LlantaID = this.oUDT.UserFields.Fields.Item("U_Ubicacion12").Value;
            this.llanta.ubicacion13.LlantaID = this.oUDT.UserFields.Fields.Item("U_Ubicacion13").Value;
            this.llanta.ubicacion14.LlantaID = this.oUDT.UserFields.Fields.Item("U_Ubicacion14").Value;
            this.llanta.ubicacion15.LlantaID = this.oUDT.UserFields.Fields.Item("U_Ubicacion15").Value;
            this.llanta.ubicacion16.LlantaID = this.oUDT.UserFields.Fields.Item("U_Ubicacion16").Value;
            this.llanta.ubicacion17.LlantaID = this.oUDT.UserFields.Fields.Item("U_Ubicacion17").Value;
            this.llanta.ubicacion18.LlantaID = this.oUDT.UserFields.Fields.Item("U_Ubicacion18").Value;
            switch (tipo)
            {
                case 1:
                    this.llanta.ILlantaID = this.oUDT.UserFields.Fields.Item("U_VehiculoID").Value;
                    break;
                case 2:
                    this.llanta.ILlantaID = this.oUDT.UserFields.Fields.Item("U_ActivoID").Value;
                    break;
                default:
                    this.llanta.ILlantaID = null;
                    break;
            }
            this.Sincronizado = this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value;
            this.UUID = this.oUDT.UserFields.Fields.Item("U_UUID").Value != null ? Guid.Parse(this.oUDT.UserFields.Fields.Item("U_UUID").Value) : null;
        }
        #endregion

        #region Acciones
        public void Insert()
        {
            //No se utiliza...

            #region SQL || Hana
            /*this.Parametro = String.Empty;
            this.Parametro = String.Format("INSERT INTO {0} VALUES('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', '{19}', '{20}', '{21}', '{22}', '{23}', '{24}', '{25}', '{26}')"this.NombreTabla, this.llanta.ILlantaID, this.llanta.ILlantaID, this.llanta.KilometrosRecorridos, );*/

            this.Insertar.DoQuery(this.Parametro);
            #endregion SQL || Hana

            #region SQL
            /*int flag = oUDT.Add();
            if (flag != 0)
            {
                int errornum;
                string errormsj;
                company.GetLastError(out errornum, out errormsj);
                throw new Exception("Error: " + errornum + " - " + errormsj);
            }*/
            #endregion SQL
        }

        public void Update()
        {
            //No se utiliza.

            var flag = oUDT.Update();
            if (flag != 0)
            {
                int errornum;
                string errormsj;
                company.GetLastError(out errornum, out errormsj);
                throw new Exception("Error: " + errornum + " - " + errormsj);
            }
        }

        public void Delete()
        {
            //No se utiliza.

            string ID = String.Empty;
            if (!String.IsNullOrEmpty(oUDT.Code))
                ID = oUDT.Code;
            else
            {
                throw new Exception("Erron al intentar obtener identificador. ");
            }

            #region SQL || Hana
            this.Parametro = String.Empty;
            this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code"" = '{1}'", this.NombreTabla, ID);

            this.Insertar.DoQuery(this.Parametro);
            #endregion SQL || Hana

            #region SQL
            /*var flag = oUDT.Remove();
            if (flag != 0)
            {
                int errornum;
                string errormsj;
                company.GetLastError(out errornum, out errormsj);
                throw new Exception("Error: " + errornum + " - " + errormsj);
            }*/
            #endregion SQL
        }

        #endregion

        #region Services
        public Recordset Consultar(ILlanta ll, int tipo)
        {
            try
            {
                Recordset rs = this.company.GetBusinessObject(BoObjectTypes.BoRecordset);
                
                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(" SELECT * from " + this.NombreTabla + " as ll ");
                
                if (ll.ID != null && ll.ID > 0)
                    where.Append(string.Format(@" and ll.""Code"" = '{0}' ", ll.ID));
                if (ll.KilometrosRecorridos != null)
                    where.Append(string.Format(" and ll.U_KmsRecorridos = {0} ", ll.KilometrosRecorridos));
                if (ll.ubicacion1 != null && ll.ubicacion1.LlantaID != null)
                    where.Append(string.Format(" and ll.U_Ubicacion1 = {0} ", ll.ubicacion1.LlantaID));
                if (ll.ubicacion2 != null && ll.ubicacion2.LlantaID != null)
                    where.Append(string.Format(" and ll.U_Ubicacion2 = {0} ", ll.ubicacion2.LlantaID));
                if (ll.ubicacion3 != null && ll.ubicacion3.LlantaID != null)
                    where.Append(string.Format(" and ll.U_Ubicacion3 = {0} ", ll.ubicacion3.LlantaID));
                if (ll.ubicacion4 != null && ll.ubicacion4.LlantaID != null)
                    where.Append(string.Format(" and ll.U_Ubicacion4 = {0} ", ll.ubicacion4.LlantaID));
                if (ll.ubicacion5 != null && ll.ubicacion5.LlantaID != null)
                    where.Append(string.Format(" and ll.U_Ubicacion5 = {0} ", ll.ubicacion5.LlantaID));
                if (ll.ubicacion6 != null && ll.ubicacion6.LlantaID != null)
                    where.Append(string.Format(" and ll.U_Ubicacion6 = {0} ", ll.ubicacion6.LlantaID));
                if (ll.ubicacion7 != null && ll.ubicacion7.LlantaID != null)
                    where.Append(string.Format(" and ll.U_Ubicacion7 = {0} ", ll.ubicacion7.LlantaID));
                if (ll.ubicacion8 != null && ll.ubicacion8.LlantaID != null)
                    where.Append(string.Format(" and ll.U_Ubicacion8 = {0} ", ll.ubicacion8.LlantaID));
                if (ll.ubicacion9 != null && ll.ubicacion9.LlantaID != null)
                    where.Append(string.Format(" and ll.U_Ubicacion9 = {0} ", ll.ubicacion9.LlantaID));
                if (ll.ubicacion10 != null && ll.ubicacion10.LlantaID != null)
                    where.Append(string.Format(" and ll.U_Ubicacion10 = {0} ", ll.ubicacion10.LlantaID));
                if (ll.ubicacion11 != null && ll.ubicacion11.LlantaID != null)
                    where.Append(string.Format(" and ll.U_Ubicacion11 = {0} ", ll.ubicacion11.LlantaID));
                if (ll.ubicacion12 != null && ll.ubicacion12.LlantaID != null)
                    where.Append(string.Format(" and ll.U_Ubicacion12 = {0} ", ll.ubicacion12.LlantaID));
                if (ll.ubicacion13 != null && ll.ubicacion13.LlantaID != null)
                    where.Append(string.Format(" and ll.U_Ubicacion13 = {0} ", ll.ubicacion13.LlantaID));
                if (ll.ubicacion14 != null && ll.ubicacion14.LlantaID != null)
                    where.Append(string.Format(" and ll.U_Ubicacion14 = {0} ", ll.ubicacion14.LlantaID));
                if (ll.ubicacion15 != null && ll.ubicacion15.LlantaID != null)
                    where.Append(string.Format(" and ll.U_Ubicacion15 = {0} ", ll.ubicacion15.LlantaID));
                if (ll.ubicacion16 != null && ll.ubicacion16.LlantaID != null)
                    where.Append(string.Format(" and ll.U_Ubicacion16 = {0} ", ll.ubicacion16.LlantaID));
                if (ll.ubicacion17 != null && ll.ubicacion17.LlantaID != null)
                    where.Append(string.Format(" and ll.U_Ubicacion17 = {0} ", ll.ubicacion17.LlantaID));
                if (ll.ubicacion18 != null && ll.ubicacion18.LlantaID != null)
                    where.Append(string.Format(" and ll.U_Ubicacion18 = {0} ", ll.ubicacion18.LlantaID));
                switch (tipo)
                {
                    case 1:
                        where.Append(string.Format(" and ll.U_VehiculoID = {0} ", ll.ILlantaID));
                        break;
                    case 2:
                        where.Append(string.Format(" and ll.U_ActivoID = {0} ", ll.ILlantaID));
                        break;
                }
                if (Sincronizado != null)
                    where.Append(string.Format(" and ll.U_Sincronizado = {0} ", Sincronizado));
                if (UUID != null && UUID != new Guid())
                    where.Append(string.Format(" and ll.U_UUID = '{0}' ", UUID));

                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(3);
                    query.Append(" where " + filtro);
                }
                query.Append(" order by ll.Code desc ");
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<ILlanta> RecordSetToListCargaCombustible(Recordset rs)
        {
            var result = new List<ILlanta>();
            for (int i = 0; i < rs.RecordCount; i++)
            {
                this.ItemCode = rs.Fields.Item("Code").Value;
                this.oUDT.GetByKey(this.ItemCode);
                UDTToILlanta();
                var temp = (ILlanta) this.llanta;
                result.Add(temp);
            }
            return result;
        }

        public Recordset ActualizarCode(ILlanta ll)
        {
            try
            {
                Recordset rs = this.company.GetBusinessObject(BoObjectTypes.BoRecordset);

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(@" UPDATE " + this.NombreTabla + @" SET ""Code"" = '" + ll.ID +
                    "', U_Sincronizado = " + Sincronizado);

                if (UUID != null)
                    where.Append(string.Format(" and U_UUID like '%{0}%' ", UUID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
    }
}
