﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kanan.Costos.BO2;
using Kanan.Llantas.BO2;
using Kanan.Operaciones.BO2;
using SAPbobsCOM;

namespace KananSAP.Llantas.Data
{
    public class LlantaSAP
    {
        #region Propiedades

        public Llanta llanta { get; set; }///
        public UserTable oUDT { get; set; }
        public Items oItem { get; set; }
        public string Code { get; set; }
        public string ItemCode { get; set; }
        public int? Sincronizado { get; set; }
        public Guid? UUID { get; set; }
        private Company company;

        //Migracion Hana
        Recordset Insertar;
        String Parametro;
        private String NombreTabla;
        bool EsSQL;

        //Se agregó para ser visible desde cualquier lugar consulta.
        public String NombreTablaLlantasSAP = String.Empty;


        public bool ExistUDT()
        {
            return oUDT.GetByKey(this.Code);
        }
        #endregion

        #region Constructor
        public LlantaSAP(ref SAPbobsCOM.Company company)
        {
            this.company = company;
            this.llanta = new Llanta
            {
               Propietario = new Empresa(),
               SubPropietario = new Sucursal(),
               proveedorLlanta = new Proveedor()
            };
            
            this.oItem = (SAPbobsCOM.Items)company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
            try
            {
                this.oUDT = company.UserTables.Item("VSKF_LLANTA");

                //Migración HANA
                this.Insertar = (Recordset)this.company.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.EsSQL = company.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.NombreTabla = EsSQL ? "[@VSKF_LLANTA]" : @"""@VSKF_LLANTA""";
                //Se maneja en una variable pública para poder utilizar desde cualquier método.
                this.NombreTablaLlantasSAP = this.NombreTabla;
            }
            catch (Exception)
            {
                this.oUDT = null;
            }
        }
        #endregion

        #region Metodos

        public string AddUpdateUDOLlanta(bool bAdd=true, string sCodigoLLanta ="")
        {
            string sResponse = string.Empty;
            int iCommand = 0, iCode = 0;
            if (bAdd)
            {
                oUDT.Code = this.llanta.ConsecutivoCode; //llanta.LlantaID != null ? this.llanta.LlantaID.ToString() : "0";
                oUDT.Name = this.llanta.ConsecutivoName; //this.llanta.NumeroEconomico == null ? this.llanta.ConsecutivoName : this.llanta.NumeroEconomico;
            }
            else oUDT.GetByKey(sCodigoLLanta);
            this.oUDT.UserFields.Fields.Item("U_ItemCode").Value = this.ItemCode ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_NumEconomico").Value = this.llanta.NumeroEconomico ?? string.Empty;
            if (llanta.NumeroRevestimientos != null)
                this.oUDT.UserFields.Fields.Item("U_NumRevestimientos").Value = this.llanta.NumeroRevestimientos;

            this.oUDT.UserFields.Fields.Item("U_SN").Value = this.llanta.SN ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_Marca").Value = this.llanta.Marca ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_Modelo").Value = this.llanta.Modelo ?? string.Empty;
            if (llanta.NumeroCapas != null)
                this.oUDT.UserFields.Fields.Item("U_NumCapas").Value = this.llanta.NumeroCapas;
            if (llanta.Costo != null)
                this.oUDT.UserFields.Fields.Item("U_Costo").Value = Convert.ToDouble(this.llanta.Costo);
            if (llanta.proveedorLlanta != null && llanta.proveedorLlanta.ProveedorID != null)
                this.oUDT.UserFields.Fields.Item("U_ProveedorLlanta").Value = this.llanta.proveedorLlanta.ProveedorID;
            else this.oUDT.UserFields.Fields.Item("U_ProveedorLlanta").Value = 0;
            this.oUDT.UserFields.Fields.Item("U_NumeroFactura").Value = this.llanta.NumeroFactura ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_FechaCompra").Value = this.llanta.FechaCompra ?? new DateTime();
            this.oUDT.UserFields.Fields.Item("U_EsActivo").Value = this.llanta.EstaActivo != null ? ((bool)this.llanta.EstaActivo ? 1 : 0) : 0;
            this.oUDT.UserFields.Fields.Item("U_Observaciones").Value = this.llanta.Observaciones ?? string.Empty;
            if (llanta.KilometrosRecorridos != null)
                this.oUDT.UserFields.Fields.Item("U_KmsRecorridos").Value = this.llanta.KilometrosRecorridos;
            if (llanta.Propietario != null && llanta.Propietario.EmpresaID != null)
                this.oUDT.UserFields.Fields.Item("U_Propietario").Value = this.llanta.Propietario.EmpresaID;
            this.oUDT.UserFields.Fields.Item("U_EstaInstalado").Value = this.llanta.EstaInstalado != null ? ((bool)this.llanta.EstaInstalado == true ? 1 : 0) : 0;
            if (llanta.SubPropietario != null && llanta.SubPropietario.SucursalID != null)
                this.oUDT.UserFields.Fields.Item("U_SucursalID").Value = this.llanta.SubPropietario.SucursalID;
            else this.oUDT.UserFields.Fields.Item("U_SucursalID").Value = 0;
            if (llanta.Uso != null)
                this.oUDT.UserFields.Fields.Item("U_Uso").Value = this.llanta.Uso;
            this.oUDT.UserFields.Fields.Item("U_Ancho").Value = this.llanta.Ancho ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_Aspecto").Value = this.llanta.Aspecto ?? string.Empty;
            if (llanta.Construccion != null)
                this.oUDT.UserFields.Fields.Item("U_Construccion").Value = this.llanta.Construccion;
            this.oUDT.UserFields.Fields.Item("U_DiametroRin").Value = this.llanta.DiametroRin ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_IndiceCarga").Value = this.llanta.IndiceCarga ?? string.Empty;
            if (llanta.RangoVelocidad != null)
                this.oUDT.UserFields.Fields.Item("U_RangoVelocidad").Value = this.llanta.RangoVelocidad;
            if (llanta.Aplicacion != null)
                this.oUDT.UserFields.Fields.Item("U_Aplicacion").Value = this.llanta.Aplicacion;
            if (llanta.Traccion != null)
                this.oUDT.UserFields.Fields.Item("U_Traccion").Value = this.llanta.Traccion;
            this.oUDT.UserFields.Fields.Item("U_Treadwear").Value = this.llanta.Treadwear ?? string.Empty;
            if (llanta.Temperatura != null)
                this.oUDT.UserFields.Fields.Item("U_Temperatura").Value = this.llanta.Temperatura;
            if (llanta.MaximaPresionInflado != null)
                this.oUDT.UserFields.Fields.Item("U_MaxiPresionInflado").Value = this.llanta.MaximaPresionInflado;
            if (llanta.MinimaPresionInflado != null)
                this.oUDT.UserFields.Fields.Item("U_MinPresionInflado").Value = this.llanta.MinimaPresionInflado;

            if (!bAdd && this.llanta.FechaFabricacion != null)
            {
                if (this.llanta.FechaFabricacion != DateTime.MinValue)
                    this.oUDT.UserFields.Fields.Item("U_FechaFabricacion").Value = this.llanta.FechaFabricacion;
            }
            else this.oUDT.UserFields.Fields.Item("U_FechaFabricacion").Value = this.llanta.FechaFabricacion ?? new DateTime();

            if (!bAdd && this.llanta.FechaCaducidad != null)
            {
                if (this.llanta.FechaCaducidad != DateTime.MinValue)
                    this.oUDT.UserFields.Fields.Item("U_FechaCaducidad").Value = this.llanta.FechaCaducidad;
            }
            else this.oUDT.UserFields.Fields.Item("U_FechaCaducidad").Value = this.llanta.FechaCaducidad != null ? this.llanta.FechaCaducidad : DateTime.Today;

            if (Sincronizado != null)
                this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Sincronizado;
            if (UUID != null && UUID != new Guid())
                this.oUDT.UserFields.Fields.Item("U_UUID").Value = this.UUID.ToString();
            if (this.llanta.KilometrosCaducidad != null & this.llanta.KilometrosCaducidad > 0)
                this.oUDT.UserFields.Fields.Item("U_KmCaducidad").Value = Convert.ToDouble(this.llanta.KilometrosCaducidad);

            iCommand = bAdd ? this.oUDT.Add() : this.oUDT.Update();
            if (iCommand != 0)
                this.company.GetLastError(out iCode, out sResponse);
            else sResponse = "OK";
            return sResponse;
        }

        public void  UDTToLlanta()
        {
            this.llanta = new Llanta
            {
                proveedorLlanta = new Proveedor(),
                Propietario = new Empresa(),
                SubPropietario = new Sucursal()
            };
            this.ItemCode = this.oUDT.UserFields.Fields.Item("U_ItemCode").Value;
            this.llanta.LlantaID = Convert.ToInt32(this.oUDT.Code);
            this.llanta.NumeroEconomico = string.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_NumEconomico").Value) ? null : this.oUDT.UserFields.Fields.Item("U_NumEconomico").Value;
            this.llanta.NumeroRevestimientos = this.oUDT.UserFields.Fields.Item("U_NumRevestimientos").Value == null ? null : (int?)this.oUDT.UserFields.Fields.Item("U_NumRevestimientos").Value;
            this.llanta.SN = string.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_SN").Value) ? null : this.oUDT.UserFields.Fields.Item("U_SN").Value;
            this.llanta.Marca = string.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_Marca").Value) ? null : this.oUDT.UserFields.Fields.Item("U_Marca").Value;
            this.llanta.Modelo = string.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_Modelo").Value) ? null : this.oUDT.UserFields.Fields.Item("U_Modelo").Value;
            this.llanta.NumeroCapas = (int)this.oUDT.UserFields.Fields.Item("U_NumCapas").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_NumCapas").Value;
            this.llanta.Costo = (decimal)this.oUDT.UserFields.Fields.Item("U_Costo").Value == 0 ? null : (decimal?)this.oUDT.UserFields.Fields.Item("U_Costo").Value;
            this.llanta.proveedorLlanta.ProveedorID = (int)this.oUDT.UserFields.Fields.Item("U_ProveedorLlanta").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_ProveedorLlanta").Value;
            this.llanta.NumeroFactura = string.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_NumeroFactura").Value) ? null : this.oUDT.UserFields.Fields.Item("U_NumeroFactura").Value;

            var testc = this.oUDT.UserFields.Fields.Item("U_FechaCompra").Value == null ? null : (DateTime?)this.oUDT.UserFields.Fields.Item("U_FechaCompra").Value;
            this.llanta.FechaCompra = testc == null || testc.Value.Year < 1900 ? null : testc;

            this.llanta.EstaActivo = this.oUDT.UserFields.Fields.Item("U_EsActivo").Value == 1;
            this.llanta.Observaciones = this.oUDT.UserFields.Fields.Item("U_Observaciones").Value;
            this.llanta.KilometrosRecorridos = this.oUDT.UserFields.Fields.Item("U_KmsRecorridos").Value;
            this.llanta.Propietario.EmpresaID = this.oUDT.UserFields.Fields.Item("U_Propietario").Value;
            this.llanta.EstaInstalado = null; //this.oUDT.UserFields.Fields.Item("U_EstaInstalado").Value == 1;
            this.llanta.SubPropietario.SucursalID = this.oUDT.UserFields.Fields.Item("U_SucursalID").Value;
            this.llanta.Uso = this.oUDT.UserFields.Fields.Item("U_Uso").Value;
            this.llanta.Ancho = this.oUDT.UserFields.Fields.Item("U_Ancho").Value;
            this.llanta.Aspecto = this.oUDT.UserFields.Fields.Item("U_Aspecto").Value;
            this.llanta.Construccion = this.oUDT.UserFields.Fields.Item("U_Construccion").Value;
            this.llanta.DiametroRin = this.oUDT.UserFields.Fields.Item("U_DiametroRin").Value;
            this.llanta.IndiceCarga = this.oUDT.UserFields.Fields.Item("U_IndiceCarga").Value;
            this.llanta.RangoVelocidad = this.oUDT.UserFields.Fields.Item("U_RangoVelocidad").Value;
            this.llanta.Aplicacion = this.oUDT.UserFields.Fields.Item("U_Aplicacion").Value;
            this.llanta.Traccion = this.oUDT.UserFields.Fields.Item("U_Traccion").Value;
            this.llanta.Treadwear = this.oUDT.UserFields.Fields.Item("U_Treadwear").Value;
            this.llanta.Temperatura = this.oUDT.UserFields.Fields.Item("U_Temperatura").Value;
            this.llanta.MaximaPresionInflado = this.oUDT.UserFields.Fields.Item("U_MaxiPresionInflado").Value;
            this.llanta.MinimaPresionInflado = this.oUDT.UserFields.Fields.Item("U_MinPresionInflado").Value;
            var testf = this.oUDT.UserFields.Fields.Item("U_FechaFabricacion").Value == null ? null : (DateTime?)this.oUDT.UserFields.Fields.Item("U_FechaFabricacion").Value;
            this.llanta.FechaFabricacion = testf == null || testf.Value.Year < 1900 ? null : testf;            
            this.Sincronizado = this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value;
            this.UUID = this.oUDT.UserFields.Fields.Item("U_UUID").Value != null ? Guid.Parse(this.oUDT.UserFields.Fields.Item("U_UUID").Value) : null;

            this.llanta.KilometrosCaducidad = Convert.ToDecimal(this.oUDT.UserFields.Fields.Item("U_KmCaducidad").Value);

            var testfc = this.oUDT.UserFields.Fields.Item("U_FechaCaducidad").Value == null ? null : (DateTime?)this.oUDT.UserFields.Fields.Item("U_FechaCaducidad").Value;
            this.llanta.FechaCaducidad = testc == null || testc.Value.Year < 1900 ? null : testc;

        }
        #endregion

        #region Acciones
       /* public void Insert()
        {
            try
            {
                #region Fechas
                String FechaCompra = String.Empty;
                String FechaFabricacion = String.Empty;
                String FechaCaducidad = String.Empty;

                FechaCompra = this.llanta.FechaCompra != null ? this.llanta.FechaCompra.Value.ToString("yyyy-MM-dd") : DateTime.Today.ToString("yyyy-MM-dd");
                FechaFabricacion = this.llanta.FechaFabricacion != null ? this.llanta.FechaFabricacion.Value.ToString("yyyy-MM-dd") : DateTime.Today.ToString("yyyy-MM-dd");
                FechaCaducidad = this.llanta.FechaCaducidad != null ? this.llanta.FechaCaducidad.Value.ToString("yyyy-MM-dd") : DateTime.Today.ToString("yyyy-MM-dd");
                #endregion Fechas

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format("INSERT INTO {0} VALUES('{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}','{32}','{33}','{34}','{35}','{36}','{37}','{38}','{39}')", this.NombreTabla, this.llanta.LlantaID, this.llanta.NumeroEconomico, this.ItemCode, this.llanta.NumeroEconomico, this.llanta.NumeroRevestimientos, this.llanta.SN, this.llanta.Marca, this.llanta.Modelo, this.llanta.NumeroCapas, Convert.ToDouble(this.llanta.Costo), this.llanta.Uso, this.llanta.proveedorLlanta.ProveedorID, this.llanta.NumeroFactura, FechaCompra, "1", this.llanta.Observaciones, Convert.ToDouble(this.llanta.KilometrosRecorridos), this.llanta.Propietario.EmpresaID, Convert.ToInt32(this.llanta.EstaInstalado), this.llanta.SubPropietario.SucursalID, this.llanta.Uso, this.llanta.Ancho, this.llanta.Aspecto, this.llanta.Construccion, this.llanta.DiametroRin, this.llanta.IndiceCarga, this.llanta.RangoVelocidad, this.llanta.Aplicacion, this.llanta.Traccion, this.llanta.Treadwear, this.llanta.Temperatura, this.llanta.MaximaPresionInflado, this.llanta.MinimaPresionInflado, FechaFabricacion, "0", this.UUID, Convert.ToDouble(this.llanta.KilometrosCaducidad), FechaCaducidad, this.llanta.CodigoAlmacen);

                Insertar.DoQuery(Parametro);
                #endregion SQL || Hana
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar insertar un registro de tipo Llanta. " + ex.Message);
            }
        }

        public void Update()
        {
            #region Fechas
            String FechaCompra = String.Empty;
            String FechaFabricacion = String.Empty;
            String FechaCaducidad = String.Empty;

            FechaCompra = this.llanta.FechaCompra != null ? this.llanta.FechaCompra.Value.ToString("yyyy-MM-dd") : DateTime.Today.ToString("yyyy-MM-dd");
            FechaFabricacion = this.llanta.FechaFabricacion != null ? this.llanta.FechaFabricacion.Value.ToString("yyyy-MM-dd") : DateTime.Today.ToString("yyyy-MM-dd");
            FechaCaducidad = this.llanta.FechaCaducidad != null ? this.llanta.FechaCaducidad.Value.ToString("yyyy-MM-dd") : DateTime.Today.ToString("yyyy-MM-dd");
            #endregion Fechas

            #region SQL || Hana
            this.Parametro = String.Empty;
            this.Parametro = String.Format("UPDATE {0} SET Code = '{1}', Name = '{2}', U_ItemCode = '{3}', U_NumEconomico = '{4}', U_NumRevestimientos = '{5}', U_SN = '{6}', U_Marca = '{7}', U_Modelo = '{8}', U_NumCapas = '{9}', U_Costo = '{10}', U_ClaseRotacion = '{11}', U_ProveedorLlanta = '{12}', U_NumeroFactura = '{13}', U_FechaCompra = '{14}', U_EsActivo = '{15}', U_Observaciones = '{16}', U_KmsRecorridos = '{17}', U_Propietario = '{18}', U_EstaInstalado = '{19}', U_SucursalID = '{20}', U_Uso = '{21}', U_Ancho = '{22}', U_Aspecto = '{23}', U_Construccion = '{24}', U_DiametroRin = '{25}', U_IndiceCarga = '{26}', U_RangoVelocidad = '{27}', U_Aplicacion = '{28}', U_Traccion = '{29}', U_Treadwear = '{30}', U_Temperatura = '{31}', U_MaxiPresionInflado = '{32}', U_MinPresionInflado = '{33}', U_FechaFabricacion = '{34}', U_Sincronizado = '{35}', U_KmCaducidad = '{36}', U_FechaCaducidad = '{37}' WHERE Code = '{1}'", this.NombreTabla, this.llanta.LlantaID, this.llanta.NumeroEconomico, this.ItemCode, this.llanta.NumeroEconomico, this.llanta.NumeroRevestimientos, this.llanta.SN, this.llanta.Marca, this.llanta.Modelo, this.llanta.NumeroCapas, Convert.ToDouble(this.llanta.Costo), this.llanta.Uso, this.llanta.proveedorLlanta.ProveedorID, this.llanta.NumeroFactura, FechaCompra, Convert.ToInt32(this.llanta.EstaActivo), this.llanta.Observaciones, Convert.ToDouble(this.llanta.KilometrosRecorridos), this.llanta.Propietario.EmpresaID, Convert.ToInt32(this.llanta.EstaInstalado), this.llanta.SubPropietario.SucursalID, this.llanta.Uso, this.llanta.Ancho, this.llanta.Aspecto, this.llanta.Construccion, this.llanta.DiametroRin, this.llanta.IndiceCarga, this.llanta.RangoVelocidad, this.llanta.Aplicacion, this.llanta.Traccion, this.llanta.Treadwear, this.llanta.Temperatura, this.llanta.MaximaPresionInflado, this.llanta.MinimaPresionInflado, FechaFabricacion, this.Sincronizado, Convert.ToDouble(this.llanta.KilometrosCaducidad), FechaCaducidad);

            this.Insertar.DoQuery(this.Parametro);
            #endregion SQL || Hana

            #region SQL
            var flag = oUDT.Update();
            if (flag != 0)
            {
                int errornum;
                string errormsj;
                company.GetLastError(out errornum, out errormsj);
                throw new Exception("Error: " + errornum + " - " + errormsj);
            }
            #endregion SQL
        }*/

        public void Delete()
        {

            string ID = !String.IsNullOrEmpty(oUDT.Code) ? oUDT.Code : String.Empty;
            if (String.IsNullOrEmpty(ID))
                throw new Exception("Erron al intentar obtener identificador. ");

            #region SQL || Hana
            this.Parametro = String.Empty;
            this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code"" = '{1}'", this.NombreTabla, ID);

            this.Insertar.DoQuery(this.Parametro);
            #endregion SQL || Hana

            #region SQL
            /*var flag = oUDT.Remove();
            if (flag != 0)
            {
                int errornum;
                string errormsj;
                company.GetLastError(out errornum, out errormsj);
                throw new Exception("Error: " + errornum + " - " + errormsj);
            }*/
            #endregion SQL
        }

        public void Deactivate()
        {
            try
            {
                Recordset rs = this.company.GetBusinessObject(BoObjectTypes.BoRecordset);

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(" UPDATE " + this.NombreTabla + " SET U_EsActivo = 0 ");

                if (UUID != null)
                    where.Append(string.Format(" and U_UUID like '%{0}%' ", UUID));
                if (llanta.LlantaID != null)
                    where.Append(string.Format(@" and ""Code"" like '%{0}%' ", llanta.LlantaID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                rs.DoQuery(query.ToString());
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar desactivar una llanta. " + ex.Message);
            }
        }

        #endregion

        #region Services

        public Recordset GetAllWarehouse()
        {
            this.Parametro = String.Empty;
            this.Parametro = String.Format("SELECT * FROM OWHS");
            this.Insertar.DoQuery(Parametro);
            this.Insertar.MoveFirst();
            return Insertar;
        }

        public String GetItemCodeByID(int? LlantaID)
        {
            try
            {
                String ItemCode = String.Empty;

                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"select U_ItemCode from {0} where ""Code"" = '{1}'", this.NombreTabla, LlantaID);
                this.Insertar.DoQuery(Parametro);
                ItemCode = this.Insertar.Fields.Item("U_ItemCode").Value;
                if (!String.IsNullOrEmpty(ItemCode))
                    return ItemCode;
                else
                    return String.Empty;
            }
            catch (Exception ex)
            {
                throw new Exception("No fue posible recuperar el código de artículo de la llanta. Error: " + ex.Message);
            }
        }

        public String GetWhsCodeByID(int? LlantaID)
        {
            try
            {
                String WhsCode = String.Empty;

                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"select U_WhsCode from {0} where ""Code"" = '{1}'", this.NombreTabla, LlantaID);
                this.Insertar.DoQuery(Parametro);
                WhsCode = this.Insertar.Fields.Item("U_WhsCode").Value;
                if (!String.IsNullOrEmpty(WhsCode))
                    return WhsCode;
                else
                    return String.Empty;
            }
            catch (Exception ex)
            {
                throw new Exception("No fue posible recuperar el código de artículo de la llanta. Error: " + ex.Message);
            }
        }

        public Recordset Consultar(Llanta ll)
        {
            try
            {
                Recordset rs = this.company.GetBusinessObject(BoObjectTypes.BoRecordset);
                if (ll == null) ll = new Llanta();
                if (ll.proveedorLlanta == null) ll.proveedorLlanta = new Proveedor();
                if (ll.Propietario == null) ll.Propietario = new Empresa();
                if (ll.SubPropietario == null) ll.SubPropietario = new Sucursal();

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(" SELECT * from " + this.NombreTabla + " as ll ");
                
                if (ll.LlantaID != null && ll.LlantaID > 0)
                    where.Append(string.Format(@" and ll.""Code"" = {0} ", ll.LlantaID));
                if (!string.IsNullOrEmpty(ll.NumeroEconomico))
                    where.Append(string.Format(" and ll.U_NumEconomico = '{0}' ", ll.NumeroEconomico));
                if (ll.NumeroRevestimientos != null && ll.NumeroRevestimientos > 0)
                    where.Append(string.Format(" and ll.U_NumRevestimientos = {0} ", ll.NumeroRevestimientos));
                if (!string.IsNullOrEmpty(ll.SN))
                    where.Append(string.Format(" and ll.U_SN = '{0}' ", ll.SN));
                if (!string.IsNullOrEmpty(ll.Marca))
                    where.Append(string.Format(" and ll.U_Marca = '{0}' ", ll.Marca));
                if (!string.IsNullOrEmpty(ll.Modelo))
                    where.Append(string.Format(" and ll.U_Modelo = '{0}' ", ll.Modelo));
                if (ll.NumeroCapas != null && ll.NumeroCapas > 0)
                    where.Append(string.Format(" and ll.U_NumCapas = {0} ", ll.NumeroCapas));
                if (ll.Costo != null && ll.Costo > 0)
                    where.Append(string.Format(" and ll.U_Costo = {0} ", ll.Costo));
                if (ll.proveedorLlanta != null && ll.proveedorLlanta.ProveedorID != null && ll.proveedorLlanta.ProveedorID > 0)
                    where.Append(string.Format(" and ll.U_ProveedorLlanta = {0} ", ll.proveedorLlanta.ProveedorID));
                if (!string.IsNullOrEmpty(ll.NumeroFactura))
                    where.Append(string.Format(" and ll.U_NumeroFactura = '{0}' ", ll.NumeroFactura));
                if (ll.FechaCompra != null)
                    where.Append(string.Format(" and ll.U_FechaCompra = '{0}' ", ll.FechaCompra));
                if (ll.EstaActivo != null)
                    where.Append(string.Format(" and ll.U_EsActivo = {0} ", Convert.ToInt32(ll.EstaActivo)));
                if (!string.IsNullOrEmpty(ll.Observaciones))
                    where.Append(string.Format(" and ll.U_Observaciones = '{0}' ", ll.Observaciones));
                if (ll.KilometrosRecorridos != null && ll.KilometrosRecorridos > 0)
                    where.Append(string.Format(" and ll.U_KmsRecorridos = {0} ", ll.KilometrosRecorridos));
                if (ll.Propietario != null && ll.Propietario.EmpresaID != null && ll.Propietario.EmpresaID > 0)
                    where.Append(string.Format(" and ll.U_Propietario = {0} ", ll.Propietario.EmpresaID));
                if (ll.EstaInstalado != null)
                    where.Append(string.Format(" and ll.U_EstaInstalado = {0} ", Convert.ToInt32(ll.EstaInstalado)));
                if (ll.SubPropietario != null && ll.SubPropietario.SucursalID != null && ll.SubPropietario.SucursalID > 0)
                    where.Append(string.Format(" and ll.U_SucursalID = {0} ", ll.SubPropietario.SucursalID));
                if (ll.Uso != null && ll.Uso > 0)
                    where.Append(string.Format(" and ll.U_Uso = {0} ", ll.Uso));
                if (!string.IsNullOrEmpty(ll.Ancho))
                    where.Append(string.Format(" and ll.U_Ancho = '{0}' ", ll.Ancho));
                if (!string.IsNullOrEmpty(ll.Aspecto))
                    where.Append(string.Format(" and ll.U_Aspecto = '{0}' ", ll.Aspecto));
                if (ll.Construccion != null && ll.Construccion > 0)
                    where.Append(string.Format(" and ll.U_Construccion = {0} ", ll.Construccion));
                if (ll.Uso != null && ll.Uso > 0)
                    where.Append(string.Format(" and ll.U_Uso = {0} ", ll.Uso));
                if (!string.IsNullOrEmpty(ll.DiametroRin))
                    where.Append(string.Format(" and ll.U_DiametroRin = '{0}' ", ll.DiametroRin));
                if (!string.IsNullOrEmpty(ll.IndiceCarga))
                    where.Append(string.Format(" and ll.U_IndiceCarga = '{0}' ", ll.IndiceCarga));
                if (ll.RangoVelocidad != null && ll.RangoVelocidad > 0)
                    where.Append(string.Format(" and ll.U_RangoVelocidad = {0} ", ll.RangoVelocidad));
                if (ll.Aspecto != null && ll.Aplicacion > 0)
                    where.Append(string.Format(" and ll.U_Aplicacion = {0} ", ll.Aplicacion));
                if (ll.Traccion != null && ll.Traccion > 0)
                    where.Append(string.Format(" and ll.U_Traccion = {0} ", ll.Traccion));
                if (!string.IsNullOrEmpty(ll.Treadwear))
                    where.Append(string.Format(" and ll.U_Treadwear = '{0}' ", ll.Treadwear));
                if (ll.Temperatura != null && ll.Temperatura > 0)
                    where.Append(string.Format(" and ll.U_Temperatura = {0} ", ll.Temperatura));
                if (!string.IsNullOrEmpty(ll.MaximaPresionInflado))
                    where.Append(string.Format(" and ll.U_MaxiPresionInflado = '{0}' ", ll.MaximaPresionInflado));
                if (!string.IsNullOrEmpty(ll.MinimaPresionInflado))
                    where.Append(string.Format(" and ll.U_MinPresionInflado = '{0}' ", ll.MinimaPresionInflado));
                if (Sincronizado != null)
                    where.Append(string.Format(" and ll.U_Sincronizado = {0} ", Sincronizado));
                if (UUID != null && UUID != new Guid())
                    where.Append(string.Format(" and ll.U_UUID = '{0}' ", UUID));

                //if (ll.KilometrosCaducidad != null && ll.KilometrosCaducidad > 0)
                //    where.Append(string.Format(" and ll.U_KmCaducidad = '{0}' ", ll.KilometrosCaducidad));

                //if (ll.FechaCaducidad != null)
                //    where.Append(string.Format(" and ll.U_FechaCaducidad = '{0}' ", ll.FechaCaducidad));

                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(3);
                    query.Append(" where " + filtro);
                }
                query.Append(@" order by ll.""Code"" desc ");
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar consultar una llanta. " + ex.Message);
            }
        }

        public List<Llanta> RecordSetToListCargaCombustible(Recordset rs)
        {
            var result = new List<Llanta>();
            for (int i = 0; i < rs.RecordCount; i++)
            {
                this.ItemCode = rs.Fields.Item("Code").Value;
                this.oUDT.GetByKey(this.ItemCode);
                UDTToLlanta();
                var temp = (Llanta)this.llanta.Clone();
                result.Add(temp);
            }
            return result;
        }

        public Recordset ActualizarCode(Llanta ll)
        {
            try
            {
                Recordset rs = this.company.GetBusinessObject(BoObjectTypes.BoRecordset);

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(@" UPDATE " + this.NombreTabla + @" SET ""Code"" = '" + ll.LlantaID +
                    "', U_Sincronizado = " + Sincronizado);

                if (UUID != null)
                    where.Append(string.Format(" and U_UUID like '%{0}%' ", UUID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar actualizar el Code de una llanta. " + ex.Message);
            }
        }
        #endregion
    }
}
