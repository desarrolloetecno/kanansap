﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Etecno.Security2.BO;
namespace Kanan.Alertas.BO2
{
    public class AlertaMostrada
    {
        public int AlertaMostradaID { get; set;}
        /// <summary>
        /// Contenido de la alerta principal
        /// </summary>
        public Alerta CurrentAlert { get; set;}
        /// <summary>
        /// Usuario quien oculta la alerta
        /// </summary>
        public Usuario CurrentUser { get; set;}
        /// <summary>
        /// Si esta oculta o no
        /// </summary>
        public bool Ocultar { get; set;}
        
    }
}
