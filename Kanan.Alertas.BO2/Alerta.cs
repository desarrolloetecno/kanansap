﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Etecno.Security2.BO;
using Kanan.Operaciones.BO2;
using Kanan.Comun.BO2;
namespace Kanan.Alertas.BO2
{
    public class Alerta
    {
        public int? AlertaID { get; set; }
        /// <summary>
        /// Indica el mensaje que contendrá la alerta de cualquier tipo que sea * Idioma español*
        /// </summary>
        public string Descripcion_Es { get; set; }
        /// <summary>
        /// Indica el mensaje que contendrá la alerta de cualquier tipo que sea * Idioma Ingles*
        /// </summary>
        public string Descripcion_En { get; set; }
        /// <summary>
        /// Indica el mensaje que contendrá la alerta de cualquier tipo que sea * Idioma portugues*
        /// </summary>
        public string Descripcion_Pt { get; set; }
        /// <summary>
        /// Indica el mensaje que contendrá la alerta de cualquier tipo que sea * Idioma Frances*
        /// </summary>
        public string Descripcion_Fr { get; set; }
        /// <summary>
        /// Estado actual de la alerta
        /// </summary>
        public bool? Activa { get; set; }
        /// <summary>
        /// Usuario quien realiza una acción en la alerta
        /// </summary>
        public Usuario CurrentUser { get; set; }

        public int? vehiculoID { get; set;}
        public int? licenciaID { get; set; }

        /// <summary>
        /// Empresa de quien pertenece el elemento que genera la alerta
        /// </summary>
        public Empresa Company { get; set; }
        /// <summary>
        /// Sucursal de quien pertenece el elemento que genera la alerta
        /// </summary>
        public Sucursal SubPropietario { get; set; }
        /// <summary>
        /// Indica la clasificación de la alerta, alMtto=1, Poliza=2,Lisencia=3,Cuenta=4
        /// </summary>
        public int? TipoAlerta { get; set; }
        /// <summary>
        /// Representa el id principal de cualquier tipo de alerta generada va de la mano con la propiedad TIPOALERTA
        /// </summary>
        public int? IAlertaID { get; set; }

        /// <summary>
        /// Indica el tipo del tipo para la alerta indica la medicion o el calculo 1=por distancia, 2 =por tiempo o por dias entre otros
        /// </summary>
        public int? TypeOfType { get; set; }

        public int? NumEnviados { get; set; }

        public int? RutinaID { get; set; }

        public object Clone()
        {
            Alerta obj = new Alerta();
            obj.AlertaID = this.AlertaID;
            obj.vehiculoID = this.vehiculoID;
            obj.TypeOfType = this.TypeOfType;
            obj.Descripcion_En = this.Descripcion_En;
            obj.Descripcion_Es = this.Descripcion_Es;
            obj.Descripcion_Fr = this.Descripcion_Fr;
            obj.Descripcion_Pt = this.Descripcion_Pt;
            obj.Activa = this.Activa;
            obj.TipoAlerta = this.TipoAlerta;
            obj.IAlertaID = this.IAlertaID;
            obj.NumEnviados = this.NumEnviados;
            obj.RutinaID = this.RutinaID;
            if (this.CurrentUser != null)
                obj.CurrentUser = (Usuario)this.CurrentUser.Clone();

            if (this.Company != null)
                obj.Company = (Empresa)this.Company.Clone();

            if (this.SubPropietario != null)
                obj.SubPropietario = (Sucursal)this.SubPropietario.Clone();

            return obj;
        }
    }
}
