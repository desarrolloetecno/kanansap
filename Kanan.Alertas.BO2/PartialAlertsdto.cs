﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kanan.Alertas.BO2
{
    public class PartialAlertsdto
    {
        public int? Total { get; set; }
        public List<Alerta> ListAlerts { get; set; }
    }
}
