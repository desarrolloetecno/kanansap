﻿using Kanan.Operaciones.BO2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kanan.Alertas.BO2
{
    public class AlertaPoliza
    {
        /// <summary>
        /// ID principal de la alerta poliza
        /// </summary>
        public int? AlertaPolizaID { get; set; }
        /// <summary>
        /// Empresa
        /// </summary>
        public Empresa Company { get; set; }
        /// <summary>
        /// ID principal del vehiculo 
        /// </summary>
        public int? VehiculoID { get; set; }
        /// <summary>
        /// Nombre del vehiculo
        /// </summary>
        //public string VehiculoName { get; set; }
        /// <summary>
        /// Especifica el tipo de la alerta 1=warning, 2=Danger
        /// </summary>
        //public int? TipoAlerta { get; set; }
        /// <summary>
        /// Dias por vencer la poliza
        /// </summary>
        //public int? Dias { get; set; }
    }
}
