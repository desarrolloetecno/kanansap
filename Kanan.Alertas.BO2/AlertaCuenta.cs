﻿using Kanan.Operaciones.BO2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kanan.Alertas.BO2
{
    public class AlertaCuenta
    {

        public int? AlertaCuentaID { get; set; }
        /// <summary>
        /// id de la empresa
        /// </summary>
        public int? empresaid { get; set;}
        public bool? esactivo { get; set; }
        public bool? es_warning { get; set; }
        /// <summary>
        /// Es solo la etiqueta faltan
        /// </summary>
        public string msjFaltan_es { get; set;}
        public string msjFaltan_fr { get; set; }
        public string msjFaltan_en { get; set; }
        public string msjFaltan_pt { get; set; }

        public int? diasFaltantes { get; set; }
        /// <summary>
        /// Solo la etiqueta que dice dias
        /// </summary>
        public string dias_en { get; set; }
        public string dias_es { get; set; }
        public string dias_pt { get; set; }
        public string dias_fr { get; set; }
        /// <summary>
        /// solo etiqueta renovar
        /// </summary>
        public string prenovar_es { get; set; }
        public string prenovar_en { get; set; }
        public string prenovar_pt { get; set; }
        public string prenovar_fr { get; set; }

        /// <summary>
        /// solo etiqueta para vencimiento
        /// </summary>
        public string pvencimiento_es { get; set; }
        public string pvencimiento_en { get; set; }
        public string pvencimiento_pt { get; set; }
        public string pvencimiento_fr { get; set; }

        public string correos { get; set;}
        public string telefonos { get; set; }

        public object Clone()
        {
            AlertaCuenta obj = new AlertaCuenta();
            return obj;
        }
    }
}
