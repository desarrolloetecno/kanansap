﻿using System;

namespace Kanan.Alertas.BO2
{
    public class AlertaDesgaste
    {
        public int? AlertaDesgasteID { get; set; }
        public int? VehiculoID { get; set; }
        public int? LlantaID { get; set; }
        public int? EmpresaID { get; set; }
        public int? SucursalID { get; set; }
        public int? TipoAlerta { get; set; }

        public object Clone()
        {
            AlertaDesgaste AlertaDesgaste = new AlertaDesgaste
            {
                AlertaDesgasteID = this.AlertaDesgasteID,
                VehiculoID = this.VehiculoID,
                LlantaID = this.LlantaID,
                EmpresaID = this.EmpresaID,
                SucursalID = this.SucursalID,
                TipoAlerta = this.TipoAlerta
            };
            return AlertaDesgaste;
        }
    }
}
