﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kanan.Operaciones.BO2;
namespace Kanan.Alertas.BO2
{
     /// <summary>
    /// 
    /// </summary>
    public class AlertaLicencia
    {
        /// <summary>
        /// ID principal de la alerta de tipo licencia
        /// </summary>
        public int? AlertaLicenciaID { get; set; }
        /// <summary>
        /// Objeto licencia
        /// </summary>
        public Licencia CurrentLicencePlate { get; set; }
        /// <summary>
        /// Empresa
        /// </summary>
        public Empresa Company { get; set; }

        public int? TipoAlerta { get; set;}
        
        /// <summary>
        /// Operador de la licencia
        /// </summary>
        public Operador OwnerLicence { get; set;}

    }
}
