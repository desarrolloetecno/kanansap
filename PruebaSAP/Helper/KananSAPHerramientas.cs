﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbobsCOM;
using SAPbouiCOM;
using System.IO;
using System.Text.RegularExpressions;
namespace KananSAP.Helper
{
    public class KananSAPHerramientas
    {
        #region Comentarios
        /* Clase:           Clase para funciones generales.
         * Fecha:           22/09/17
         */
        #endregion Comentarios

        #region Constructores
        public KananSAPHerramientas(SAPbouiCOM.Application oAplicacionSBO)
        {
            
            this.oAplicacionSBO = oAplicacionSBO;
        }
        #endregion Constructores

        #region Variables
        
        private SAPbouiCOM.Application oAplicacionSBO;
        
        
        #endregion Variables

        #region Propiedades
        private static Regex ALL_MATCH = new Regex("^[zZ]+$");
        public SAPbouiCOM.Application AplicacionSBO
        {
            get { return this.oAplicacionSBO; }
            set { this.oAplicacionSBO = value; }
        }
        #endregion Propiedades

        #region Métodos

        #region Métodos estáticos
        public static void LogError(string sMetodo, string sTexto)
        {
            
            string sRuta = "";
            try
            {
                sRuta = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                if (!Directory.Exists(string.Format(@"{0}\Errores", sRuta)))
                    Directory.CreateDirectory(string.Format(@"{0}\Errores", sRuta));
                if (!File.Exists(string.Format(@"{0}\Errores\{1}", sRuta, DateTime.Now.ToString("ddMMyyyy"))))
                {
                    sRuta =string.Format(@"{0}\Errores\{1}.txt", sRuta, DateTime.Now.ToString("ddMMyyyy"));

                    File.AppendAllText(sRuta, string.Format(@"{0}\t{1}:\t{2}\n{3}",DateTime.Now.ToString("HH:mm"), sMetodo,sTexto, Environment.NewLine));
                    File.AppendAllText(sRuta, "\n");
                    File.AppendAllText(sRuta, Environment.NewLine);
                }

            }
            catch { }

        }
        public static void LogProceso(string sMetodo, string sTexto)
        {

            string sRuta = "";
            try
            {
                sRuta = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                if (!Directory.Exists(string.Format(@"{0}\Proceso", sRuta)))
                    Directory.CreateDirectory(string.Format(@"{0}\Proceso", sRuta));
                if (!File.Exists(string.Format(@"{0}\Proceso\{1}", sRuta, DateTime.Now.ToString("ddMMyyyy"))))
                {
                    sRuta = string.Format(@"{0}\Proceso\{1}.txt", sRuta, DateTime.Now.ToString("ddMMyyyy"));

                    File.AppendAllText(sRuta, string.Format(@"{0}\t{1}:\t{2}\n{3}", DateTime.Now.ToString("HH:mm"), sMetodo, sTexto, Environment.NewLine));
                    File.AppendAllText(sRuta, "\n");
                    File.AppendAllText(sRuta, Environment.NewLine);
                }

            }
            catch { }

        }
        #endregion Métodos estáticos
        public void ColocarMensaje(String sMensaje, BoMessageTime oTiempoMensaje, BoStatusBarMessageType oTipoMensaje, Boolean bDetenerProcesador=false)
        {
          
            try
            {
                if (sMensaje != "Form - Invalid Form")
                    this.oAplicacionSBO.StatusBar.SetText(sMensaje, oTiempoMensaje, oTipoMensaje);

                if (bDetenerProcesador)
                    System.Threading.Thread.Sleep(3000);
            }
            catch (Exception oError)
            {
                KananSAPHerramientas.LogError("KananSAPHerramientas > ColocarMensaje", oError.ToString());
            }
        }
        public static string SiguienteSerie(string cadena)
        {
            char lastCharOpos = cadena[cadena.Length - 1];

            try
            {
                if (ALL_MATCH.IsMatch(cadena))
                {
                    string result = String.Empty;
                    for (int i = 0; i < cadena.Length; i++)
                        result += "A";
                    return result + "A";
                }
                else if (lastCharOpos == '9')
                    return SiguienteSerie(cadena.Remove(cadena.Length - 1, 1)) + "A";
                else
                    if (Char.IsNumber(lastCharOpos))
                    {
                        int iNumerico = Convert.ToInt32(lastCharOpos);
                        iNumerico++;
                        lastCharOpos = Convert.ToChar(iNumerico);
                    }
                    else
                    {
                        if (lastCharOpos == 'Z')
                        {
                            lastCharOpos = '1';
                        }
                        else
                        {
                            ++lastCharOpos;
                        }
                    }
            }
            catch (Exception e)
            {

                return SiguienteSerie(cadena.Remove(cadena.Length - 1, 1)) + "A";

            }

            return cadena.Remove(cadena.Length - 1, 1) + (lastCharOpos).ToString();

        }
        
        #endregion Métodos
    }
}
