﻿using Kanan.Vehiculos.BO2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.Helper
{
    public class UtilMantenimientoHelper
    {
        /// <summary>
        /// Obtiene el TipoMatenibleID
        /// </summary>
        /// <param name="mantenibleType">Tipo de Mantenible</param>
        /// <returns></returns>
        public static int? GetTipoMantenibleID(Type mantenibleType)
        {
            if (mantenibleType == typeof(Vehiculo))
                return 1;
            else if (mantenibleType == typeof(Caja))
                return 2;
            else if (mantenibleType == typeof(TipoVehiculo))
                return 3;
            return null;
        }

    }
}
