﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.Helper
{
    /// <summary>
    /// Contiene los metodos que pueden ser reutilizados en otras clases
    /// </summary>
    public static class KananSAPHelpers
    {
        /// <summary>
        /// Elimina todos los items válidos del ComboBox
        /// </summary>
        /// <param name="combo"></param>
        public static void CleanComboBox(ComboBox combo)
        {
            try
            {
                if (combo != null && combo.ValidValues != null && combo.ValidValues.Count > 0)
                {
                    var f = combo.ValidValues.Count;
                    for (int i = 0; i < f; i++)
                    {
                        combo.ValidValues.Remove(i, BoSearchKey.psk_Index);
                        f--;
                        i--;
                    }
                }

            }
            catch { }
        }

        public static void DeshabilitaItem(Form oForm, string UUID, bool bEnabled= false)
        {
            try
            {
                SAPbouiCOM.Item oItem = oForm.Items.Item(UUID);
                oItem.Enabled = bEnabled;
            }
            catch { }
        }

        public static int GetCodeTemporal()
        {
            int code = (int)DateTime.Now.Ticks;
            return code;
        }
    }
}
