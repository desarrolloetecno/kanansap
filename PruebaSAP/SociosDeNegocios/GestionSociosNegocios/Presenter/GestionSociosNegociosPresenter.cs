﻿#region C#
using System;
using System.Collections.Generic;
using System.Linq;
#endregion C#

#region Kananfleet
using Kanan.Costos.BO2;
using KananSAP.SociosDeNegocios.GestionSociosNegocios.View;
using KananWS.Interface;
#endregion Kananfleet

#region SAP
using SAPbouiCOM;
#endregion SAP

namespace KananSAP.SociosDeNegocios.GestionSociosNegocios.Presenter
{
    class GestionSociosNegociosPresenter
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private string captionbefore;
        private SAPbouiCOM.Item oItem;
        private bool IsMessageBoxElimina = false;
        private bool ModalCarga;
        private bool ModalRep;
        private List<GestionSociosNegociosView> Views;
        private GestionSociosNegociosView view;
        private CostosWS costosWs;
        private BoFormMode mode;
        private bool Cliente;
        private bool Proveedor;
        private bool ejecuta;

        public bool IsLogged { get; set; }
        private Configurations configs;
        private Helper.KananSAPHerramientas oHerramientas;
        #endregion

        #region Constructor
        public GestionSociosNegociosPresenter(SAPbouiCOM.Application application, SAPbobsCOM.Company company, Configurations c)
        {
            this.SBO_Application = application;
            this.Company = company;
            this.configs = c;
            this.Views = new List<GestionSociosNegociosView>();
            this.ejecuta = false;
            this.Cliente = false;
            this.Proveedor = false;
            try
            {
                this.costosWs = new CostosWS(Guid.Parse(this.configs.UserFull.Usuario.PublicKey), this.configs.UserFull.Usuario.PrivateKey);
            }
            catch
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("GestionSociosNegociosPresenter.cs->GestionSociosNegociosPresenter()", "");
                this.costosWs = new CostosWS(new Guid(), string.Empty);
            }

            oHerramientas = new Helper.KananSAPHerramientas(this.SBO_Application);
        }
        #endregion

        #region Metodos

        private void SetActiveView(string FormUID)
        {
            this.view = Views.First(x => x.FormUniqueID == FormUID);
        }

        #region Eventos
        public void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            if (!IsLogged) return;
            string group = string.Empty;
            try
            {
                #region Form Datos Maestros de Socios de Negocios

                if (pVal.FormType == 134)
                {
                    #region Before Action
                    if (pVal.Before_Action)
                    {
                        if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_LOAD)
                        {
                            this.Views.Add(new GestionSociosNegociosView(this.SBO_Application, this.Company, pVal.FormTypeEx, pVal.FormTypeCount));
                            this.SetActiveView(FormUID);
                        }

                        if (pVal.ItemUID == "1" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                        {
                           // ejecuta = this.view.IsProviderKf();
                            if (this.view.IsProviderKf())
                            {
                                Proveedor = true;
                                Cliente = false;
                            }

                            if (this.view.IsClientKf())
                            {
                                Proveedor = false;
                                Cliente = true;
                            }
                            mode = this.view.GetFormMode();
                            
                            oItem = SBO_Application.Forms.GetForm(pVal.FormTypeEx, pVal.FormTypeCount).Items.Item("1");
                            var btn = (SAPbouiCOM.Button)(oItem.Specific);
                            captionbefore = btn.Caption;
                            //if (ejecuta)
                            //{
                            //    switch (mode)
                            //    {
                            //        case BoFormMode.fm_ADD_MODE:
                            //            //this.view.ValidarDatos();
                            //            this.view.FormToEntity();
                            //            break;
                            //        case BoFormMode.fm_UPDATE_MODE:
                            //            //this.view.ValidarDatos();
                            //            this.view.FormToEntity();
                            //            break;
                            //    }
                            //}
                            if (Proveedor)
                            {
                                switch (mode)
                                {
                                    case BoFormMode.fm_ADD_MODE:
                                        //this.view.ValidarDatos();
                                        this.view.FormToEntity();
                                        break;
                                    case BoFormMode.fm_UPDATE_MODE:
                                        //this.view.ValidarDatos();
                                        this.view.FormToEntity();
                                        break;
                                }
                            }
                            if (Cliente)
                            {
                                switch (mode)
                                {
                                    case BoFormMode.fm_ADD_MODE:
                                        this.view.FormToEntityCliente();
                                        break;
                                    case BoFormMode.fm_UPDATE_MODE:
                                        this.view.FormToEntityCliente();
                                        break;
                                }
                            }
                        }
                    }
                    #endregion

                    #region After Action
                    else
                    {
                        if (pVal.EventType == BoEventTypes.et_FORM_UNLOAD)
                        {
                            this.Views.Remove(this.Views.First(x => x.FormUniqueID == FormUID));
                            this.view = null;
                        }

                        if (pVal.ItemUID == "1" & pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            //ejecuta = this.view.IsProviderKf();
                            //switch (mode)
                            //{
                            //    case BoFormMode.fm_ADD_MODE:
                            //        if (ejecuta)
                            //        {
                            //            Insertar();
                            //        }
                            //        break;
                            //    case BoFormMode.fm_UPDATE_MODE:
                            //        if (ejecuta)
                            //        {
                            //            Actualizar();
                            //        }
                            //        break;
                            //}
                            if (this.view.IsProviderKf())
                            {
                                Proveedor = true;
                                Cliente = false;
                            }

                            if (this.view.IsClientKf())
                            {
                                Proveedor = false;
                                Cliente = true;
                            }

                            switch (mode)
                            {
                                case BoFormMode.fm_ADD_MODE:
                                    if (Proveedor)
                                    {
                                        Insertar();
                                    }

                                    if (Cliente)
                                    {
                                        InsertarCliente();
                                    }
                                    break;
                                case BoFormMode.fm_UPDATE_MODE:
                                    if (Proveedor)
                                    {
                                        Actualizar();
                                    }

                                    if (Cliente)
                                    {
                                        ActualizarCliente();
                                    }
                                    break;
                            }
                            mode = BoFormMode.fm_OK_MODE;
                            ejecuta = false;
                            BubbleEvent = false;
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception ex)
            {
                BubbleEvent = false;
                KananSAP.Helper.KananSAPHerramientas.LogError("GestionSociosNegociosPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
                oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            }
        } 
        #endregion

        #region Acciones

        public void Insertar()
        {
            try
            {
                this.view.Entity.SetPropietarioToUDT(this.configs.UserFull.Dependencia.EmpresaID);
                this.view.Entity.proveedor.ProveedorID = null;
                this.view.Entity.proveedor = this.costosWs.Add(this.view.Entity.proveedor,
                    (int) this.configs.UserFull.Dependencia.EmpresaID);
                this.view.Entity.ProveedorToUDT();
                this.view.Entity.InsertUDT();
                this.view.SetFormMode(BoFormMode.fm_OK_MODE);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("GestionSociosNegociosPresenter.cs->Insertar()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public void Actualizar()
        {
            try
            {
                if (view.Entity.ExistUDT())
                {
                    view.Entity.SetPropietarioToUDT(this.configs.UserFull.Dependencia.EmpresaID);
                    var id = Convert.ToInt32(this.view.Entity.oUDT.UserFields.Fields.Item("U_ProveedorID").Value);
                    view.Entity.proveedor.ProveedorID = id;
                    view.Entity.ProveedorToUDT();
                    view.Entity.proveedor = costosWs.Update(view.Entity.proveedor);
                    view.Entity.UpdateUDT();
                }
                else
                {
                    Insertar();
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("GestionSociosNegociosPresenter.cs->Actualizar()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        #region Cliente
        public void InsertarCliente()
        {
            try
            {
                view.EntityCliente.cliente.EmpresaID = this.configs.UserFull.Dependencia.EmpresaID;
                view.EntityCliente.cliente.ClienteID = costosWs.AddCliente(view.EntityCliente.cliente);
                view.EntityCliente.Insert();
                this.view.SetFormMode(BoFormMode.fm_OK_MODE);
            }
            catch (Exception ex)
            {
                throw new Exception("Cliente no registrado. " + ex.Message);
            }
        }
        public void ActualizarCliente()
        {
            try
            {
                if (view.EntityCliente.ExistClient(view.EntityCliente.ItemCode))
                {
                    view.EntityCliente.cliente.EmpresaID = this.configs.UserFull.Dependencia.EmpresaID;
                    view.EntityCliente.cliente.ClienteID = view.EntityCliente.GetClienteIDByCardCore(view.EntityCliente.ItemCode);
                    view.EntityCliente.cliente.ClienteID = costosWs.UpdateCliente(view.EntityCliente.cliente);
                    view.EntityCliente.Update();
                }
                else
                    InsertarCliente();
            }
            catch (Exception ex)
            {
                throw new Exception("Cliente no actualizado. " + ex.Message);
            }
        }
        #endregion Cliente

        #endregion
        
        #endregion
    }
}
