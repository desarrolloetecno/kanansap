﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KananSAP.Costos.Data;
using SAPbouiCOM;

namespace KananSAP.SociosDeNegocios.GestionSociosNegocios.View
{
    class GestionSociosNegociosView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oNewItem;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.Folder oFolderItem;
        private SAPbouiCOM.StaticText oStaticText;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.ComboBox oComboBox;
        private SAPbouiCOM.Button oButton;
        public Costos.Data.ProveedorSAP Entity { get; private set; }
        public Costos.Data.ClienteSAP EntityCliente { get; private set; }
        public string FormUniqueID { get; private set; }
        #endregion

        #region Constructor
        public GestionSociosNegociosView(SAPbouiCOM.Application Application, SAPbobsCOM.Company company, string formtype, int formcount)
        {
            this.Entity = new ProveedorSAP(company);
            this.EntityCliente = new ClienteSAP(company);
            this.SBO_Application = Application;
            this.SetForm(formtype, formcount);
            this.oNewItem = null;
            this.oItem = null;
            this.oFolderItem = null;
            this.oStaticText = null;
            this.oEditText = null;
            this.oComboBox = null;
        }
        #endregion

        #region Métodos
        #region Form Methods
        public Item GetItemFromForm(string ItemID)
        {
            try
            {
                oItem = this.oForm.Items.Item(ItemID);
                return oItem;
            }
            catch
            {
                return null;
            }
        }

        public void SetForm(string type, int count)
        {
            try
            {
                this.oForm = SBO_Application.Forms.GetForm(type, count);
                this.FormUniqueID = this.oForm.UniqueID;
            }
            catch (Exception ex)
            {
                this.oForm = null;
                throw new Exception(ex.Message);
            }
        }

        public void SetFormMode(SAPbouiCOM.BoFormMode mode)
        {
            this.oForm.Mode = mode;
        }

        public bool IsProviderKf()
        {
            string group = string.Empty;
            if (this.oForm == null) return false;
            var item = this.GetItemFromForm("16");
            if (item == null) return false;
            oComboBox = (SAPbouiCOM.ComboBox)(item.Specific);
            @group = oComboBox.Selected != null ? oComboBox.Selected.Description : string.Empty;
            return group.Trim().ToUpper() == "PROVEEDORES KF";
        }
        public bool IsClientKf()
        {
            string group = string.Empty;
            if (this.oForm == null)
                return false;
            var item = this.GetItemFromForm("16");
            if (item == null)
                return false;
            oComboBox = (SAPbouiCOM.ComboBox)(item.Specific);
            @group = oComboBox.Selected != null ? oComboBox.Selected.Description : string.Empty;
            return group.Trim().ToUpper() == "CLIENTES KF";
        }

        public void SetCardCodeCardName()
        {
            oItem = this.GetItemFromForm("5");
            //oItem = oForm.Items.Item("5");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            Entity.CardCode = oEditText.Value;
            Entity.CardCode = oEditText.String;

            oItem = this.GetItemFromForm("7");
            //oItem = oForm.Items.Item("6");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            Entity.CardName = oEditText.Value;
        }

        public BoFormMode GetFormMode()
        {
            return this.oForm != null? this.oForm.Mode : new BoFormMode();
        }

        public void FormToEntity()
        {
            oItem = this.oForm.Items.Item("5");
            oEditText = (SAPbouiCOM.EditText) (oItem.Specific);
            this.Entity.ItemCode = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;

            oItem = this.oForm.Items.Item("7");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.proveedor.Nombre = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;


            oItem = this.oForm.Items.Item("43");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.proveedor.Telefono = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;

            oItem = this.oForm.Items.Item("60");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.proveedor.Correos = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;

            oItem = this.oForm.Items.Item("41");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.proveedor.CUP = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;
        }

        public void FormToEntityCliente()
        {
            oItem = this.oForm.Items.Item("5");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.EntityCliente.ItemCode = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;
            this.EntityCliente.cliente.CardCode = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;

            oItem = this.oForm.Items.Item("7");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.EntityCliente.cliente.Nombre = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;

            oItem = this.oForm.Items.Item("128");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.EntityCliente.cliente.NombreExtranjero = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;

            oItem = this.oForm.Items.Item("41");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.EntityCliente.cliente.RFC = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;

            oItem = this.oForm.Items.Item("43");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.EntityCliente.cliente.Telefono = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;

            oItem = this.oForm.Items.Item("51");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.EntityCliente.cliente.TelefonoMovil = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;

            oItem = this.oForm.Items.Item("60");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.EntityCliente.cliente.Correo = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;
        }

        #endregion
        #endregion

    }
}
