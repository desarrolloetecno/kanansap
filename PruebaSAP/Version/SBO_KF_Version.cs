﻿using Kanan.Vehiculos.BO2;
using KananSAP;
using KananSAP.Configuracion.Configuraciones.Objects;
using KananSAP.Helper;
using SAPbobsCOM;
using SAPbouiCOM;
using SAPinterface.Data.AD;
using SAPinterface.Data.INFO.Tablas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application = System.Windows.Forms.Application;

namespace KananFleet.Version
{
    public class SBO_KF_Version
    {

        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        public Boolean bGuardado = false;
        private XMLPerformance XmlApplication;
        private GestionGlobales oGlobales = null;
        private ConfiguracionView oInterfaz = null;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbobsCOM.Company oCompany;
        private SAPbouiCOM.StaticText oLabel;
        #endregion

        public SBO_KF_Version(GestionGlobales oGlobales,  SAPbobsCOM.Company Company)
        {

            this.oGlobales = oGlobales;
            this.SBO_Application = this.oGlobales.SBO_Application;
            this.XmlApplication = new XMLPerformance(SBO_Application);
            this.oCompany = Company;
            Boolean bContinuar = LoadForm();         
            this.ShowForm();
            
        }

        private Boolean LoadForm()
        {
            string UID = string.Format("UIVERS");
            FormCreationParams oFormCreate = null;
            Boolean bContinuar = false;
            String XmlData = "";
            try
            {

                this.oForm = SBO_Application.Forms.Item(UID);
                if (oForm.UniqueID != UID)
                    throw new Exception("");
                bContinuar = true;
            }
            catch (Exception exp)
            {
                KananSAPHerramientas.LogError("GestionConfig.cs->LoadForm()", exp.Message);
                bContinuar = false;
                try
                {
                    string sPath = Application.StartupPath;
                    XmlData = this.XmlApplication.LoadFromXML(sPath + "\\XML", "", "Version_AddON.xml");
                    bContinuar = true;
                    oFormCreate = (FormCreationParams)this.oGlobales.SBO_Application.CreateObject(BoCreatableObjectType.cot_FormCreationParams);
                    oFormCreate.UniqueID = UID;
                    oFormCreate.FormType = "KFConfing";
                    oFormCreate.XmlData = XmlData;
                    this.oForm = this.oGlobales.SBO_Application.Forms.AddEx(oFormCreate);//SBO_Application.Forms.Item(UID);

                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;

                }
                catch (Exception ex)
                {
                    bContinuar = false;
                    KananSAPHerramientas.LogError("SBO_KF_Version.cs->LoadForm()", ex.Message);
                }
            }
            return bContinuar;
        }

        public void ShowForm()
        {
            try
            {
                oLabel = this.oForm.Items.Item("lblVer").Specific;
                string versionApp = Application.ProductVersion;
                oLabel.Caption = string.Format("V.{0}", versionApp);
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                this.oForm.Select();
                
            }
            catch (Exception ex)
            {

                oLabel = this.oForm.Items.Item("lblVer").Specific;
                string versionApp = Application.ProductVersion;
                oLabel.Caption = string.Format("V.{0}",versionApp);
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                this.oForm.Select();
            }
        }


         
         
       
    }
}