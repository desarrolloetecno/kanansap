﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.Notificaciones.Presenter
{
    public class NotificacionesPresenter
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private SAPbouiCOM.Item oItem;
        public Notificaciones.View.NotificacionesView view;
        private KananSAP.Alertas.Data.NotificacionesSAP notificacionData;
        private KananWS.Interface.NotificacionesWS notificacionWS;
        #endregion

        public NotificacionesPresenter(SAPbouiCOM.Application application, SAPbobsCOM.Company company, KananWS.Interface.Configurations configs)
        {
            this.SBO_Application = application;
            this.Company = company;
            this.view = new View.NotificacionesView(this.SBO_Application, this.Company, configs);
            this.SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
            this.notificacionData = new Alertas.Data.NotificacionesSAP(company);
            this.notificacionWS = new KananWS.Interface.NotificacionesWS(Guid.Parse(configs.UserFull.Usuario.PublicKey), configs.UserFull.Usuario.PrivateKey);
        }

        #region Eventos
        private void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (pVal.FormTypeEx == "FrmConCor")
                {
                    #region beforeaction

                    if (pVal.BeforeAction)
                    {

                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnCanCon")
                        {
                            this.SBO_Application.Forms.ActiveForm.Close();
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ColUID == "Vehiculo" || pVal.ColUID == "Marca" || pVal.ColUID == "Modelo")
                        {
                            this.GetNotificacion(pVal.Row);
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnSave")
                            this.InsertaOActualiza();
                    }

                    #endregion
                }

            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("NotificacionesPresener.cs->SBO_Application_ItemEvent()", ex.Message);
                SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                BubbleEvent = false;
            }
        }

        #endregion

        #region WEB SERVICE
        private void InsertarWS()
        {
            try
            {
                Kanan.Comun.BO2.Notificacion nt = this.notificacionWS.Insertar(this.view.Notificacion);
                if (nt == null)
                    throw new Exception("No fue posible guardar la información");
                if(nt.NotificacionID == null)
                    throw new Exception("No fue posible guardar la información");
                this.view.FormToEntity();
                this.view.Notificacion.NotificacionID = nt.NotificacionID;
            }
            catch(Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("NotificacionesPresener.cs->InsertarWS()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void ActualizarWS()
        {
            try
            {
                //bool update = this.notificacionWS.Actualizar(this.view.Notificacion);
                //if (!update)
                    throw new Exception("No fue posible actualizar la inforación");
                this.view.FormToEntity();
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("NotificacionesPresener.cs->ActualizarWS()", ex.Message);
                throw new Exception(ex.Message);
            }
        }
        
        private void GetNotificacion(int indexRow)
        {
            try
            {
                if(!this.view.EstableceNotificador(indexRow))
                {
                    Kanan.Comun.BO2.Notificacion nt = this.notificacionWS.GetNotificacion(this.view.Notificacion);
                    if (nt != null)
                        this.view.Notificacion = nt;
                    else
                        throw new Exception("No se encontró ningún dato");
                    this.view.EntityToForm();
                }
                
            }
            catch(Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("NotificacionesPresener.cs->GetNotificacion()", ex.Message);
                this.SBO_Application.SetStatusBarMessage("Ocurrio el siguiente error: " + ex.Message, BoMessageTime.bmt_Short, true);
            }
        }
        #endregion

        #region SAP
        private void Insertar()
        {
            try
            {
                this.notificacionData.ItemCode = this.view.Notificacion.NotificacionID.ToString();
                if (this.notificacionData.ExistUDT())
                    throw new Exception("La información proporcionada ya existe");
                this.notificacionData.Notificacion = this.view.Notificacion;
                this.notificacionData.NotificacionToUDT();
                this.notificacionData.InsertUDT();
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("NotificacionesPresener.cs->Insertar()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void Actualizar()
        {
            try
            {
                this.notificacionData.ItemCode = this.view.Notificacion.NotificacionID.ToString();
                if (!this.notificacionData.ExistUDT())
                    throw new Exception("La información que intenta modificar no existe");
                this.notificacionData.Notificacion = this.view.Notificacion;
                this.notificacionData.NotificacionToUDT();
                this.notificacionData.UpdateUDT();
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("NotificacionesPresener.cs->Actualizar()", ex.Message);
                throw new Exception(ex.Message);
            }
        }
        #endregion

        private void InsertaOActualiza()
        {
            try
            {
                string valid = this.view.ValidateFields();
                if (!string.IsNullOrEmpty(valid))
                    throw new Exception(valid);
                this.view.FormToEntity();
                if(this.view.Notificacion.NotificacionID != null)
                {
                    this.ActualizarWS();
                    this.Actualizar();
                }
                else
                {
                    this.InsertarWS();
                    this.Insertar();
                }
                this.SBO_Application.StatusBar.SetText("¡Registro Guardado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            }
            catch(Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("NotificacionesPresener.cs->InsertarOActualizar()", ex.Message);
                this.SBO_Application.SetStatusBarMessage("Ocurrio el siguiente error: " + ex.Message, BoMessageTime.bmt_Short, true);
            }
        }

    }
}
