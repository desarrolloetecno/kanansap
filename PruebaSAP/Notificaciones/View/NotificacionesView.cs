﻿using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using KananWS.Interface;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace KananSAP.Notificaciones.View
{
    public class NotificacionesView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.StaticText oStaticText;
        private SAPbouiCOM.Grid oGrid;
        private SAPbouiCOM.DataTable oDataTable;
        private SAPbouiCOM.DBDataSource oDBDataSource;
        private SAPbouiCOM.OptionBtn oRadioBtn;
        private XMLPerformance XmlApplication;
        string sPath = null;
        
        private KananWS.Interface.ServiciosWS servicioWS;
        private Configurations Configs;
        private NotificacionesWS correos;
        List<Vehiculo> vehiculos { get; set; }
        public Kanan.Comun.BO2.Notificacion Notificacion { get; set; }
        private string currentID { get; set; }
        private string auxCurrentID { get; set; }
        public int TipoNotificadorID { get; set; }
        #endregion

        public NotificacionesView(SAPbouiCOM.Application Application, SAPbobsCOM.Company company, Configurations config)
        {
            oDataTable = null;
            oGrid = null;
            oDBDataSource = null;
            oRadioBtn = null;
            this.Configs = config;
            this.SBO_Application = Application;
            this.correos = new NotificacionesWS(Guid.Parse(this.Configs.UserFull.Usuario.PublicKey), this.Configs.UserFull.Usuario.PrivateKey);
            this.XmlApplication = new XMLPerformance(this.SBO_Application);
            Notificacion = new Kanan.Comun.BO2.Notificacion();
        }

        #region FORM

        public void LoadForm()
        {
            sPath = System.Windows.Forms.Application.StartupPath;
            try
            {
                this.oForm = this.SBO_Application.Forms.Item("FrmConCor");
                this.SetOptionButton();
                this.FillGrid();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Invalid Form"))
                {
                   this.XmlApplication.LoadFromXML(sPath + "\\XML", "Notificaciones.xml");
                    
                    this.oForm = SBO_Application.Forms.Item("FrmConCor");
                    this.SetOptionButton();
                    this.FillGrid();
                    //Establece la ventana centrada
                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;

                }
                else
                {
                    SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                }
            }

        }

        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
            }
            catch (Exception ex)
            {
                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                this.oForm.Select();
            }
        }

        #endregion

        #region Validaciones
        private string ValidateEmail(string emailAddress)
        {
            string validate = string.Empty;
            emailAddress = emailAddress.Trim();
            emailAddress = emailAddress.ToLower();
            string[] correos = emailAddress.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            string patternStrict = @"^(([^<>()[\]\\.,;:\s@\""]+"
                  + @"(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@"
                  + @"((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
                  + @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+"
                  + @"[a-zA-Z]{2,}))$";
            Regex reStrict = new Regex(patternStrict);
            foreach (string email in correos)
            {
                bool isStrictMatch = reStrict.IsMatch(email);
                if (!isStrictMatch)
                {
                    return validate = email;
                }
            }
            return validate;

        }

        public string ValidateFields()
        {
            try
            {
                string err = string.Empty;
                OptionBtn rbtEmpresa, rbtSucusal, rbtVehiculo;
                oItem = oForm.Items.Item("txtCorreos");
                oEditText = oItem.Specific;

                if (string.IsNullOrEmpty(oEditText.String))
                    err += ", Destinatarios";
                rbtEmpresa = oForm.Items.Item("rbtEmpresa").Specific as OptionBtn;
                rbtSucusal = oForm.Items.Item("rbtSucursa").Specific as OptionBtn;
                rbtVehiculo = oForm.Items.Item("rbtVehicle").Specific as OptionBtn;
                if (!rbtEmpresa.Selected && !rbtSucusal.Selected && !rbtVehiculo.Selected)
                    err += ", Notificar por";
                if (err.StartsWith(","))
                    err = "Campos obligatorios: " +  err.Substring(1);
                if(!string.IsNullOrEmpty(oEditText.String.Trim()))
                {
                    string errcr = this.ValidateEmail(oEditText.String.Trim());
                    if (!string.IsNullOrEmpty(errcr))
                        return "Formato de correo  inválido: " + errcr;
                }

                return err;
            }
            catch(Exception ex)
            {
                throw new Exception("ValidateFields: " + ex.Message);
            }
        }

        #endregion

        private void SetOptionButton()
        {
            try
            {
                oItem = oForm.Items.Item("rbtEmpresa");
                oRadioBtn = oItem.Specific;

                oItem = oForm.Items.Item("rbtSucursa");
                oRadioBtn = oItem.Specific;
                oRadioBtn.GroupWith("rbtEmpresa");

                oItem = oForm.Items.Item("rbtVehicle");
                oRadioBtn = oItem.Specific;
                oRadioBtn.GroupWith("rbtEmpresa");
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Genera un SAPbouiCOM.DataTable a partir de una lista de vehículos
        /// </summary>
        private void ListaVehiculosToSAPDataTable()
        {
            try
            {
                if (vehiculos != null)
                {
                    if (vehiculos.Count > 0)
                    {
                        oDataTable = null;
                        SAPbouiCOM.DataTable tab = oForm.DataSources.DataTables.Item(0); //this.CreateTable();
                        int cont = 0;
                        foreach (Vehiculo vh in vehiculos)
                        {
                            tab.Rows.Add();
                            tab.SetValue("Empresa", cont, vh.Propietario.Nombre);
                            tab.SetValue("Sucursal", cont, vh.SubPropietario.Direccion);
                            tab.SetValue("Vehiculo", cont, vh.Nombre);
                            tab.SetValue("Marca", cont, vh.Marca);
                            tab.SetValue("Modelo", cont, vh.Modelo);
                            tab.SetValue("EmpresaID", cont, vh.Propietario.EmpresaID.ToString());
                            tab.SetValue("SucursalID", cont, vh.SubPropietario.SucursalID.ToString());
                            tab.SetValue("VehiculoID", cont, vh.VehiculoID);
                            cont++;
                        }
                        oDataTable = tab;
                    }
                }
            }
            catch(Exception ex)
            {
                throw new Exception("SetTableValues: " + ex.Message);
            }
        }

        /// <summary>
        /// Obtiene la lista de vehiculos de base de Kanan a traves del web service ConfigCorreosWS
        /// </summary>
        /// <param name="empresaID"></param>
        private void GetListaVehiculos()
        {
            try
            {
                vehiculos = correos.GetListaVehiculos(this.Configs.UserFull.Dependencia.EmpresaID);
            }
            catch(Exception ex)
            {
                throw new Exception("GetListaVehiculos: " + ex.Message);
            }
        }

        /// <summary>
        /// Llena el grid con las empresas, sucursales y vehículos
        /// </summary>
        public void FillGrid()
        {
            try
            {
                this.GetListaVehiculos();
                this.ListaVehiculosToSAPDataTable();
                oGrid = null;
                oItem = oForm.Items.Item("gdEmp");
                oGrid = oItem.Specific;
                oGrid.DataTable = oDataTable;
                //Visibles
                oGrid.Columns.Item("Empresa").Width = 150;
                //oGrid.Columns.Item("Empresa").Editable = false;
                oGrid.Columns.Item("Sucursal").Width = 150;
                //oGrid.Columns.Item("Sucursal").Editable = false;
                oGrid.Columns.Item("Vehiculo").Width = 150;
                //oGrid.Columns.Item("Vehiculo").Editable = false;
                oGrid.Columns.Item("Marca").Width = 150;
                //oGrid.Columns.Item("Marca").Editable = false;
                oGrid.Columns.Item("Modelo").Width = 100;
                //oGrid.Columns.Item("Modelo").Editable = false;
                //No visibles
                oGrid.Columns.Item("EmpresaID").Visible = false;
                oGrid.Columns.Item("SucursalID").Visible = false;
                oGrid.Columns.Item("VehiculoID").Visible = false;
                oGrid.CollapseLevel = 2;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void FormToEntity()
        {
            try
            {

                oItem = oForm.Items.Item("lblNTID");
                oStaticText = oItem.Specific;
                if (!string.IsNullOrEmpty(oStaticText.Caption))
                    this.Notificacion.NotificacionID = Int32.Parse(oStaticText.Caption);

                oItem = oForm.Items.Item("rbtEmpresa");
                oRadioBtn = oItem.Specific;
                if(oRadioBtn.Selected)
                {
                    this.Notificacion.Notificador = new Empresa();
                }
                oItem = oForm.Items.Item("rbtSucursa");
                oRadioBtn = oItem.Specific;
                if (oRadioBtn.Selected)
                {
                    this.Notificacion.Notificador = new Sucursal();
                }
                oItem = oForm.Items.Item("rbtVehicle");
                oRadioBtn = oItem.Specific;
                if (oRadioBtn.Selected)
                {
                    this.Notificacion.Notificador = new Vehiculo();
                }
                oItem = oForm.Items.Item("txtCorreos");
                oEditText = oItem.Specific;
                string[] correos = oEditText.String.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                foreach(string correo in correos)
                {
                    this.Notificacion.Correos += ";" + correo;
                }
                if(this.Notificacion.Correos.StartsWith(";"))
                    this.Notificacion.Correos = this.Notificacion.Correos.Substring(1, this.Notificacion.Correos.Length - 1);

            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool EstableceNotificador(int indexRow)
        {
            try
            {
                bool isEqual = false;
                oItem = oForm.Items.Item("gdEmp");
                oGrid = oItem.Specific;
                currentID = oGrid.DataTable.GetValue("VehiculoID", indexRow);
                if (currentID != auxCurrentID)
                {
                    auxCurrentID = currentID;
                    this.Notificacion = new Kanan.Comun.BO2.Notificacion();
                    this.Notificacion.Notificador = new Vehiculo();
                    this.Notificacion.Notificador.NotificableID = Int32.Parse(currentID);
                }
                else
                    isEqual = true;
                return isEqual;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void EntityToForm()
        {
            try
            {
                if (this.Notificacion.NotificacionID != null)
                {
                    oItem = oForm.Items.Item("lblNTID");
                    oStaticText = oItem.Specific;
                    oStaticText.Caption = this.Notificacion.NotificacionID.ToString();
                }
                if (this.Notificacion.Correos != null)
                {
                    oItem = oForm.Items.Item("txtCorreos");
                    oEditText = oItem.Specific;
                    oEditText.String = this.Notificacion.Correos;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
