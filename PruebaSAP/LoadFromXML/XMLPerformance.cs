﻿using System;
using System.Windows.Forms;


namespace KananSAP
{
    public class XMLPerformance
    {
        #region Atributos
        private SAPbouiCOM.Application _SBO_Application; 
        private SAPbouiCOM.Form _oForm;
        #endregion
        #region Propiedades
        public SAPbouiCOM.Application SBO_Application 
        {
            get { return _SBO_Application; }
            set { _SBO_Application = value; }
        }

        public SAPbouiCOM.Form oForm 
        {
            get { return _oForm; }
            set { _oForm = value; }
        }
        #endregion

        #region Constructor

        public XMLPerformance(SAPbouiCOM.Application application)
        {
            this.SBO_Application = application;
        }

        #endregion

        #region Metodos
        public void LoadFromXML( string ruta, string FileName ) { 
        
            System.Xml.XmlDocument oXmlDoc = null; 
        
            oXmlDoc = new System.Xml.XmlDocument(); 
        
            // load the content of the XML File
            oXmlDoc.Load(ruta + "\\" + FileName); 
        
            // load the form to the SBO application in one batch
		    string sXML = oXmlDoc.InnerXml.ToString();
            SBO_Application.LoadBatchActions(ref sXML); 
        }
        public String LoadFromXML(string ruta, string FileName = "", string XMLName = "")
        {

            System.Xml.XmlDocument oXmlDoc = null;
            oXmlDoc = new System.Xml.XmlDocument();
            oXmlDoc.Load(ruta + "\\" + XMLName);
            string sXML = oXmlDoc.InnerXml.ToString();

            return sXML;
        }
        public void SaveAsXML( ref SAPbouiCOM.Form Form ) {         
            System.Xml.XmlDocument oXmlDoc = null; 
            string sXmlString = null; 
        
            oXmlDoc = new System.Xml.XmlDocument(); 
        
            // get the form as an XML string
            sXmlString = Form.GetAsXML(); 
        
            // load the form's XML string to the
            // XML document object
            oXmlDoc.LoadXml( sXmlString ); 
        
            string sPath = null; 
        
            sPath = System.IO.Directory.GetParent( Application.StartupPath ).ToString(); 
        
            // save the XML Document
            oXmlDoc.Save( ( sPath + @"\MySimpleForm.xml" ) );

        }
        #endregion
    }
}
