﻿using Kanan.Vehiculos.BO2;
using KananSAP.Configuracion.Configuraciones.Objects;
using KananSAP.Helper;
using SAPbobsCOM;
using SAPbouiCOM;
using SAPinterface.Data.AD;
using SAPinterface.Data.INFO.Tablas;
using SAPinterface.Data.PD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application = System.Windows.Forms.Application;

namespace KananSAP.Configuracion.Configuraciones.Controller
{
    public class GestionConfig
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        public Boolean bGuardado = false;
        private XMLPerformance XmlApplication;
        private GestionGlobales oGlobales = null;
        private ConfiguracionView oInterfaz = null;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private CheckBox oCheck;
        private SAPbouiCOM.ComboBox oCombo;
        private string iConsecutivo = string.Empty;
        private SAPbobsCOM.Company oCompany;
        #endregion

        public GestionConfig(GestionGlobales oGlobales, string iSiguienteForm, SAPbobsCOM.Company Company)
        {

            this.oGlobales = oGlobales;
            this.SBO_Application = this.oGlobales.SBO_Application;
            this.XmlApplication = new XMLPerformance(SBO_Application);
            this.iConsecutivo = iSiguienteForm;
            this.oCompany = Company;
            Boolean bContinuar = LoadForm();         
            this.ShowForm();
            
        }

        private Boolean LoadForm()
        {
            string UID = string.Format("GConf_{0}", this.iConsecutivo);
            FormCreationParams oFormCreate = null;
            Boolean bContinuar = false;
            String XmlData = "";
            try
            {

                this.oForm = SBO_Application.Forms.Item(UID);
                if (oForm.UniqueID != UID)
                    throw new Exception("");
                bContinuar = true;
            }
            catch (Exception exp)
            {
                KananSAPHerramientas.LogError("GestionConfig.cs->LoadForm()", exp.Message);
                bContinuar = false;
                try
                {
                    string sPath = Application.StartupPath;                    
                    XmlData = this.XmlApplication.LoadFromXML(sPath + "\\XML", "", "GestionConfiguracion.xml");
                    bContinuar = true;
                    oFormCreate = (FormCreationParams)this.oGlobales.SBO_Application.CreateObject(BoCreatableObjectType.cot_FormCreationParams);
                    oFormCreate.UniqueID = UID;
                    oFormCreate.FormType = "GestConf";
                    oFormCreate.XmlData = XmlData;
                    this.oForm = this.oGlobales.SBO_Application.Forms.AddEx(oFormCreate);//SBO_Application.Forms.Item(UID);

                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;

                }
                catch (Exception ex)
                {
                    bContinuar = false;
                    KananSAPHerramientas.LogError("GestionConfig.cs->LoadForm()", ex.Message);
                }
            }
            return bContinuar;
        }

        public void ShowForm()
        {
            try
            {

                this.oForm.Visible = true;
                this.oForm.Select();
                this.InizializarItems();
                this.AddChooseFromList();
                this.Asignaestatus();
                this.LlenarInterfaz();
                
            }
            catch (Exception ex)
            {

                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;                                
                this.InizializarItems();
                this.AddChooseFromList();
                this.Asignaestatus();
                this.LlenarInterfaz();
                this.oForm.Visible = true;
                this.oForm.Select();
                KananSAPHerramientas.LogError("GestionConfig.cs->ShowForm()", ex.Message);

            }
        }



        public void EventoControl(ItemEvent pVal, out bool BubbleEvent, Form oCurrentForm)
        {
            BubbleEvent = true;
            try
            {
                switch (pVal.ItemUID)
                {
                    /*case "cmbFuel":
                        this.BuscarConcepto();
                        break;

                    case "btnSaveFl":
                        this.InsertaActualizaConcepto();
                        break;*/

                    case "cmbAlmcn":
                        if (pVal.Before_Action)
                        {
                            MostrarOcultar("Almacen");
                        }
                        break;

                    case "chkGesS":
                        if (pVal.Before_Action)
                        {
                            MostrarOcultar("SBO");
                        }
                        break;

                    case "chkGesK":
                        if (pVal.Before_Action)
                        {
                            MostrarOcultar("KANAN");
                        }
                        break;

                    case "chkGScl":
                        if (pVal.Before_Action)
                        {
                            MostrarOcultar("sucursal");
                        }
                        break;
                    case "chkGVeh":
                        if (pVal.Before_Action)
                        {
                            MostrarOcultar("vehiculo");
                        }
                        break;

                    case "btnNotif":
                        if (pVal.BeforeAction)
                        {
                            this.ReenviarNotificacion();
                        }
                        break;

                    case "btnSaveCK":
                        if (pVal.Before_Action)
                        {
                            Recordset oCreate = (Recordset)this.oGlobales.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                            try
                            {
                                if (this.Validar())
                                {
                                    this.Insertar();
                                    //oCreate.DoQuery(this.Insertar());
                                    //this.oGlobales.LlenarConfiguracion();
                                    this.oGlobales.Mostrarmensaje("Configuracion guardada", BoMessageTime.bmt_Long, BoStatusBarMessageType.smt_Success);
                                    this.oForm.Close();
                                }
                            }
                            catch (Exception ex)
                            {
                                this.oGlobales.Mostrarmensaje("Configuracion no guardada " + ex.Message, BoMessageTime.bmt_Long, BoStatusBarMessageType.smt_Error, true);
                            }


                        }
                        break;


                    case "btnCanCK":
                        if (pVal.Before_Action)
                        {
                            this.oForm.Close();
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError("GestionConfig.cs->EventoControl()", ex.Message);
            }
        }



        private bool Validar()
        {
            string sMessage = "";
            this.InizializarItems();

            try
            {
                if (!this.oInterfaz.chkSucursal.Checked && !this.oInterfaz.chkVehiculo.Checked)
                    sMessage = "Favor de definir el tipo de gestion Sucursal o Vehículo";
                else
                {
                    if (this.oInterfaz.CmbTipoAnticipo.Value.ToString() == "")
                        sMessage = "Favor de seleccionar el tipo de liquidación para el operador";

                    if (!this.oInterfaz.chkSBO.Checked && !this.oInterfaz.chkKanan.Checked)
                        sMessage = "Favor de definir el tipo de gestion SAP B1 o KananFleet";

                    else
                    {

                        if (this.oInterfaz.chkKanan.Checked)
                        {
                            if (this.oInterfaz.TxtCuentas.String.Trim().ToString() == "")
                                sMessage = "Favor de seleccionar una cuenta de gastos";
                        }

                        if (this.oInterfaz.chkAlmacen.Checked)
                        {
                            if (this.oInterfaz.CmbRecarga.Value.ToString() == "")
                                sMessage = "Favor de seleccionar el almacén de recarga";
                        }
                    }


                    if (this.validaExisteDimensiones())
                    {
                        if (this.oInterfaz.CmbDimension.Value.ToString() == "")
                            sMessage = "Favor de seleccionar la dimension a utilizar";
                    }

                    if (this.oInterfaz.txtServer.String.Trim().ToString() == "")
                        sMessage = "Favor de proporcionar el servidor de correos";
                    if (this.oInterfaz.txtPuerto.String.Trim().ToString() == "")
                        sMessage = "Favor de proporcionar el puerto de salida de correos";
                    if (this.oInterfaz.txtCuentaUsuario.String.Trim().ToString() == "")
                        sMessage = "Favor de proporcionar la cuenta de correo";
                    if (this.oInterfaz.txtPassWord.String.Trim().ToString() == "")
                        sMessage = "Favor de proporcionar la contraseña de la cuenta";

                    if (this.oInterfaz.txtCuentaCP.String.Trim().ToString() == "")
                        sMessage = "Favor de proporcionar la cuenta para servicios de carta de porte";

                    if (this.oInterfaz.txtCuentaAT.String.Trim().ToString() == "")
                        sMessage = "Favor de proporcionar la cuenta de anticipo";

                    if (this.oInterfaz.txtCuentaACP.String.Trim().ToString() == "")
                        sMessage = "Favor de proporcionar la cuenta de contrapartida";

                     ComboBox oCombo = (ComboBox)this.oForm.Items.Item("cmbestatus").Specific;
                     if (oCombo == null)
                         sMessage = "Favor de determinar el estatus para facturacion de la OS";
                     else if (string.IsNullOrEmpty(oCombo.Value))
                         sMessage = "Selecciona el estatus para emitir la factura en la OS";

                     ComboBox oComboSi = (ComboBox)this.oForm.Items.Item("cmbstasi").Specific;
                    if(oComboSi == null)
                        sMessage = "Favor de determinar el estatus para la salida de inventario";
                    else if (string.IsNullOrEmpty(oComboSi.Value))
                        sMessage = "Selecciona el estatus para emitir la salida de inventario";

                }
            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError("GestionConfig.cs->Validar()", ex.Message);
            }
            if (sMessage.Length > 5)
            {
                this.oGlobales.Mostrarmensaje(sMessage, BoMessageTime.bmt_Long, BoStatusBarMessageType.smt_Error);
                return false;
            }
            else
                return true;
        }
       
        private void InizializarItems()
        {
            oInterfaz = new ConfiguracionView();
            try
            {

                //---Llenar items -----/
                oInterfaz.oAlmacenChk = this.oForm.Items.Item("chkARel");
                oInterfaz.oKananChk = this.oForm.Items.Item("chkGesK");
                oInterfaz.oSBOChk = this.oForm.Items.Item("chkGesS");
                oInterfaz.oSucursalChk = this.oForm.Items.Item("chkGScl");
                oInterfaz.oVehiculoChk = this.oForm.Items.Item("chkGVeh");
                oInterfaz.oClienteChk = this.oForm.Items.Item("chkCli");


                oInterfaz.oItemAnticipo = this.oForm.Items.Item("cmbLiq");

                oInterfaz.oDimension = this.oForm.Items.Item("CmbDim");
                oInterfaz.oChkSSL = this.oForm.Items.Item("chkSSL");
                oInterfaz.oChkBorrador = this.oForm.Items.Item("chkBrr");

                oInterfaz.oChkSalidaBorrador = this.oForm.Items.Item("chkdraftSI");

                //oInterfaz.oItemCmbVehiculo = this.oForm.Items.Item("txtVeh");
                oInterfaz.oItemCmbCuentas = this.oForm.Items.Item("txtacctfu");
                oInterfaz.oItemCmbRecarga = this.oForm.Items.Item("cmbAlmcn");
                //oInterfaz.oItemTxtArticulo = this.oForm.Items.Item("txtItemc");

                oInterfaz.oItemServer = this.oForm.Items.Item("txtServ");
                oInterfaz.oItemPuerto = this.oForm.Items.Item("txtPort");
                oInterfaz.oItemCuenta = this.oForm.Items.Item("txtActml");
                oInterfaz.oItemPassWord = this.oForm.Items.Item("txtPass");

                oInterfaz.oItemCuentServicioCP = this.oForm.Items.Item("txtCPacct");
                oInterfaz.oItemCuentaAnticipo = this.oForm.Items.Item("txtAacct");
                oInterfaz.oItemCuentaContraP = this.oForm.Items.Item("txtCAacct");
                //oInterfaz.oItemConcepto = this.oForm.Items.Item("cmbFuel");


                oInterfaz.chkAlmacen = (CheckBox)this.oInterfaz.oAlmacenChk.Specific;
                oInterfaz.chkKanan = (CheckBox)this.oInterfaz.oKananChk.Specific;
                oInterfaz.chkSBO = (CheckBox)this.oInterfaz.oSBOChk.Specific;
                oInterfaz.chkSucursal = (CheckBox)this.oInterfaz.oSucursalChk.Specific;
                oInterfaz.chkVehiculo = (CheckBox)this.oInterfaz.oVehiculoChk.Specific;
                oInterfaz.chkCliente = (CheckBox)this.oInterfaz.oClienteChk.Specific;


                oInterfaz.CmbTipoAnticipo = (ComboBox)this.oInterfaz.oItemAnticipo.Specific;



                oInterfaz.chkSSl = (CheckBox)this.oInterfaz.oChkSSL.Specific;
                oInterfaz.chkBorrador = (CheckBox)this.oInterfaz.oChkBorrador.Specific;
                oInterfaz.chkSalidaBorrador = (CheckBox)this.oInterfaz.oChkSalidaBorrador.Specific;
                oInterfaz.CmbDimension = (ComboBox)this.oInterfaz.oDimension.Specific;
                
                //oInterfaz.CmbConcepto = (ComboBox)this.oInterfaz.oItemConcepto.Specific;

                //oInterfaz.CmbVehiculo = (EditText)this.oInterfaz.oItemCmbVehiculo.Specific;
                oInterfaz.TxtCuentas = (EditText)this.oInterfaz.oItemCmbCuentas.Specific;
                oInterfaz.CmbRecarga = (ComboBox)this.oInterfaz.oItemCmbRecarga.Specific;
                //oInterfaz.TxtArticulo = (EditText)this.oInterfaz.oItemTxtArticulo.Specific;

                oInterfaz.txtServer = (EditText)this.oInterfaz.oItemServer.Specific;
                oInterfaz.txtPuerto = (EditText)this.oInterfaz.oItemPuerto.Specific;
                oInterfaz.txtCuentaUsuario = (EditText)this.oInterfaz.oItemCuenta.Specific;
                oInterfaz.txtPassWord = (EditText)this.oInterfaz.oItemPassWord.Specific;

                oInterfaz.txtCuentaCP = (EditText)this.oInterfaz.oItemCuentServicioCP.Specific;
                oInterfaz.txtCuentaAT = (EditText)this.oInterfaz.oItemCuentaAnticipo.Specific;
                oInterfaz.txtCuentaACP = (EditText)this.oInterfaz.oItemCuentaContraP.Specific;

                //oInterfaz.oItemCmbSucursal = this.oForm.Items.Item("CmbSucl");
                //oInterfaz.CmbSucursal = (ComboBox)this.oInterfaz.oItemCmbSucursal.Specific;
                if (validaExisteDimensiones())
                    oInterfaz.oDimension.Enabled = true;
                else
                    oInterfaz.oDimension.Enabled = false;
                
            }

            catch (Exception ex)
            {
                KananSAPHerramientas.LogError("GestionConfig.cs->InizializarItems()", ex.Message);
                this.oInterfaz = null;
            }

        }
        private void MostrarOcultar(string escenario)
        {
            //this.oForm.Items.Item("tabDataOrd").Click();
            try
            {
                switch (escenario)
                {


                    case "SBO":
                        if (this.oInterfaz.chkSBO.Checked)
                        {
                            if (oInterfaz.chkKanan.Checked)                            
                                oInterfaz.chkKanan.Checked = false;

                            oInterfaz.oItemCmbCuentas.Enabled = false;

                            oInterfaz.oItemCmbRecarga.Enabled = true;
                            oInterfaz.oAlmacenChk.Enabled = true;

                            try
                            {
                                oInterfaz.TxtCuentas.String = "";
                            }
                            catch { }
                            oInterfaz.oItemCmbCuentas.Enabled = false;

                           
                        }
                        break;

                    case "KANAN":
                        if (this.oInterfaz.chkKanan.Checked)
                        {
                            

                            if (oInterfaz.chkAlmacen.Checked)
                                oInterfaz.chkAlmacen.Checked = false;
                            if (oInterfaz.chkSBO.Checked)
                                oInterfaz.chkSBO.Checked = false;

                            oInterfaz.oItemCmbCuentas.Enabled = true;
                            oInterfaz.oItemCmbRecarga.Enabled = false;
                            try
                            {
                                
                                oInterfaz.CmbRecarga.Select(" ", BoSearchKey.psk_ByValue);
                                
                            }catch{ }
                            oInterfaz.oAlmacenChk.Enabled = false;

                            

                        }
                        break;

                    case "vehiculo":
                        if (this.oInterfaz.chkVehiculo.Checked)
                        {

                            oInterfaz.chkSucursal.Checked = false;
                            //oInterfaz.oItemCmbVehiculo.Enabled = true;
                            //oInterfaz.oItemCmbSucursal.Enabled = false;
                            //oInterfaz.CmbSucursal.Select(" ", BoSearchKey.psk_ByValue);

                        }
                        break;

                    case "sucursal":
                        if (this.oInterfaz.chkSucursal.Checked)
                        {

                            oInterfaz.chkVehiculo.Checked = false;
                            //oInterfaz.oItemCmbSucursal.Enabled = true;
                            //oInterfaz.oItemCmbVehiculo.Enabled = false;
                            //oInterfaz.CmbVehiculo.String = "";
                        }
                        break;
                }


            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError("GestionConfig.cs->MostrarOcultar()", ex.Message);
            }
        }

        public void AddChooseFromList()
        {
            try
            {
                
                addChooseAccounts("txtacctfu", "CFLOACT1", "EdtOACT1");
                addChooseAccounts("txtCPacct", "CFLOACT2", "EdtOACT2");
                addChooseAccounts("txtAacct",  "CFLOACT3", "EdtOACT3");
                addChooseAccounts("txtCAacct", "CFLOACT4", "EdtOACT4");
            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError("GestionConfig.cs->AddChooseFromList()", ex.Message);
                //throw new Exception(ex.Message);
            }
        }

        #region Choose from list metodos para mapear cuentas 
        public void addChooseAccounts(string campoUI, string aliasID, string dataSourceID)
        {
            try
            {
                Item oItem = null;
                oForm.DataSources.UserDataSources.Add(dataSourceID /* "EdtODS"*/, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);
                SAPbouiCOM.ChooseFromListCollection oCFLs = null;
                oCFLs = oForm.ChooseFromLists;
                SAPbouiCOM.ChooseFromList oCFL = null;
                SAPbouiCOM.ChooseFromListCreationParams oCFLCreationParams = null;
                oCFLCreationParams = ((SAPbouiCOM.ChooseFromListCreationParams)(SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)));
                oCFLCreationParams.MultiSelection = false;
                oCFLCreationParams.ObjectType = "1";
                oCFLCreationParams.UniqueID = aliasID; //"CFLTOOL";
                oCFL = oCFLs.Add(oCFLCreationParams);
                #region condicionales
                SAPbouiCOM.Conditions oConditions = this.SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_Conditions);
                oConditions = oCFL.GetConditions();
                SAPbouiCOM.Condition oCondition = oConditions.Add();
                oCondition.Alias = "Postable";
                oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
                oCondition.CondVal = "Y";
                oCFL.SetConditions(oConditions);
                /*
                oCondition = oConditions.Add();
                oCondition.Alias = "FrozenFor";
                oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
                //oCondition.Relationship = BoConditionRelationship.cr_AND;
                oCondition.CondVal = "N";
                oCFL.SetConditions(oConditions);*/
                #endregion condicionales
                oItem = oForm.Items.Item(campoUI); // "txttoolid");
                EditText oEditText = (EditText)(oItem.Specific);
                oEditText.DataBind.SetBound(true, "", dataSourceID /*"EdtODS"*/);
                oEditText.ChooseFromListUID = aliasID; //"CFLTOOL";
                oEditText.ChooseFromListAlias = "AcctCode";
            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError("GestionConfig.cs->addChooseAccounts()", ex.Message);
                //throw new Exception(ex.Message);
            }
        }
        #endregion Choose from list metodos para mapear cuentas

        /*public void InsertaActualizaConcepto()
        {
            this.InizializarItems();
            
            try
            {
                if (string.IsNullOrEmpty(oInterfaz.TxtArticulo.String))
                    throw new Exception("Captura el articulo para el concepto");

                String Query = string.Empty;
                Recordset oConfig = (Recordset)this.oGlobales.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                oConfig.DoQuery(GestionGlobales.ConceptoFuel(oInterfaz.CmbConcepto.Value.ToString()));
                if (oConfig.RecordCount > 0)
                {
                    ///Actualizar concepto 
                    Query = string.Format(GestionGlobales.bSqlConnection ? "UPDATE [@VSKF_CONCEPTOFUEL] SET Name = '{0}' WHERE Code = '{1}'" : @"UPDATE ""@VSKF_CONCEPTOFUEL"" SET ""Name"" = '{0}' WHERE Code = '{1}'", oInterfaz.TxtArticulo.String.ToString().Trim(), oInterfaz.CmbConcepto.Value.ToString());
                    oConfig.DoQuery(Query);
                    oGlobales.Mostrarmensaje("Concepto actualizado correctamente", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                }
                else {
                    Query = string.Format(GestionGlobales.bSqlConnection ? "INSERT INTO [@VSKF_CONCEPTOFUEL] VALUES('{0}','{1}')" : @"INSERT INTO ""[@VSKF_CONCEPTOFUEL]"" VALUES('{0}','{1}')", oInterfaz.CmbConcepto.Value.ToString(), oInterfaz.TxtArticulo.String.ToString().Trim());
                    oConfig.DoQuery(Query);

                    oGlobales.Mostrarmensaje("Concepto guardado", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                }
            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError("GestionConfig.cs->InsertaActualizaConcepto()", ex.Message);
                oGlobales.Mostrarmensaje(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            }
        }*/

        /*public void BuscarConcepto()
        {
            this.InizializarItems();
            try
            {                
                Recordset oConfig = (Recordset)this.oGlobales.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                oConfig.DoQuery(GestionGlobales.ConceptoFuel(oInterfaz.CmbConcepto.Value.ToString()));
                if (oConfig.RecordCount > 0)
                {
                    oInterfaz.TxtArticulo.String = Convert.ToString(oConfig.Fields.Item("Name").Value);
                    oInterfaz.TxtArticulo.Value = Convert.ToString(oConfig.Fields.Item("Name").Value);

                }
                else oGlobales.Mostrarmensaje("No existe articulo par el concepto", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError("GestionConfig.cs->BuscarConcepto()", ex.Message);
                oGlobales.Mostrarmensaje(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            }
        }*/
        public void LlenarInterfaz()
        {

            try
            {
                this.oGlobales.FillCombo("Ninguno", "WhsCode", "WhsName", "cmbAlmcn", GestionGlobales.AlmacenRecarga(), this.oForm);
                if (validaExisteDimensiones())
                    this.oGlobales.FillCombo("Ninguno", "DimCode", "DimDesc", "CmbDim", this.BuscaDimActivas(), this.oForm);
            }
            catch { }

            try
            {
                SBO_KF_Configuracion_INFO oSetting = new SBO_KF_Configuracion_INFO();
                string sresponse = string.Empty, sconsultaSql = string.Empty;
                SBO_KF_Configuracion_AD oConfiguracion = new SBO_KF_Configuracion_AD();
                sresponse = oConfiguracion.recuperaConfiguracion(ref oCompany, out sconsultaSql, ref oSetting);
                if (sresponse == "OK")
                {

                    //this.oGlobales.FillCombo("Ninguno","Code", "Name", "CmbVeh", this.VehiculosActivos(), this.oForm);
                    //this.oGlobales.FillCombo("Ninguno", "Code", "Name", "CmbSucl", this.SucursalesActivas(), this.oForm);
                    //this.oGlobales.FillCombo("Ninguno", "AcctCode", "AcctName", "CmbAcctG", this.CuentasGastos(), this.oForm);

                    try
                    {
                        Item oItem = oForm.Items.Item("cmbLiq");
                        ComboBox oCombo = oItem.Specific;
                        KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);
                        oCombo.ValidValues.Add(" ", "Ninguno");
                        oCombo.ValidValues.Add("A", "Liquidación por anticipo");
                        oCombo.ValidValues.Add("F", "Liquidación por fondo fijo");
                        oCombo.Item.DisplayDesc = true;
                    }
                    catch { }


                    //llenar conceptos con datos de tipo de combustible

                    #region Conceptos
                    /* Dictionary<String, String> typeFuel = new Dictionary<string, string>();
            Item oItem = oForm.Items.Item("cmbFuel");
            ComboBox oCombo = oItem.Specific;
            KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);
            oCombo.ValidValues.Add(" ", "Ninguno");
            typeFuel.Add("1","GASOLINA_MAGNA");
            typeFuel.Add("2", "GASOLINA_PREMIUM");
            typeFuel.Add("3", "DIESEL");


            foreach (KeyValuePair<string, string> oFuel in typeFuel)
            {
               string description = string.Format("{0} - {1}", oFuel.Key, oFuel.Value);
               oCombo.ValidValues.Add(oFuel.Key.Trim(), description.Trim());
            }
            oCombo.Item.DisplayDesc = true;*/
                    #endregion

                    if (oSetting != null)
                    {

                        oForm.DataSources.UserDataSources.Item("UD_CHEC10").ValueEx = oSetting.cSSL == 'Y' ? "Y" : "N";
                        oForm.DataSources.UserDataSources.Item("UD_ChkBrr").ValueEx = oSetting.Borrador == "Y" ?"Y":"N" ;
                        SAPbouiCOM.EditText oEditText;
                        oEditText = (EditText)this.oForm.Items.Item("acc_nrc").Specific;
                        oEditText.String = oSetting.NombreCuentaGasto;

                        oEditText = (EditText)this.oForm.Items.Item("acc_ncp").Specific;
                        oEditText.String = oSetting.NombreCuentaServicio;

                        oEditText = (EditText)this.oForm.Items.Item("acc_nop").Specific;
                        oEditText.String = oSetting.NombreCuentaAnticipoOP;

                        oEditText = (EditText)this.oForm.Items.Item("acc_nan").Specific;
                        oEditText.String = oSetting.NombreCuentaContrapartidaOP;

                        
                        if (oSetting.cAlmacen == 'Y')
                            this.oInterfaz.oAlmacenChk.Click();


                        /*if (oSetting.Borrador.Trim() == "Y")
                            this.oInterfaz.chkBorrador.Checked = true;
                        else this.oInterfaz.chkBorrador.Checked = false;*/

                        try
                        {
                            oInterfaz.CmbTipoAnticipo.Select("", BoSearchKey.psk_ByValue);

                        }
                        catch { }

                        if (oSetting.cKANAN == 'Y')
                        {

                            this.oInterfaz.chkKanan.Checked = true;
                            if (oInterfaz.chkAlmacen.Checked)
                                oInterfaz.chkAlmacen.Checked = false;
                            if (oInterfaz.chkSBO.Checked)
                                oInterfaz.chkSBO.Checked = false;

                            oInterfaz.oItemCmbCuentas.Enabled = true;
                            oInterfaz.oItemCmbRecarga.Enabled = false;
                            try
                            {

                                oInterfaz.CmbRecarga.Select(" ", BoSearchKey.psk_ByValue);

                            }
                            catch { }
                            oInterfaz.oAlmacenChk.Enabled = false;
                        }
                        else this.oInterfaz.chkKanan.Checked = false;

                        if (oSetting.cSBO == 'Y')
                        {
                            this.oInterfaz.chkSBO.Checked = true;

                            if (oInterfaz.chkKanan.Checked)
                                oInterfaz.chkKanan.Checked = false;

                            oInterfaz.oItemCmbCuentas.Enabled = false;

                            oInterfaz.oItemCmbRecarga.Enabled = true;
                            oInterfaz.oAlmacenChk.Enabled = true;

                            try
                            {
                                oInterfaz.TxtCuentas.String = "";
                            }
                            catch { }
                            oInterfaz.oItemCmbCuentas.Enabled = false;
                        }
                        else this.oInterfaz.chkSBO.Checked = false;

                        if (oSetting.cSucursal == 'Y')
                            this.oInterfaz.chkSucursal.Checked = true;
                        else this.oInterfaz.chkSucursal.Checked = false;

                        if (oSetting.cVehiculo == 'Y')
                            this.oInterfaz.chkVehiculo.Checked = true;
                        else this.oInterfaz.chkVehiculo.Checked = false;

                        if (oSetting.cCliente == 'Y')
                            this.oInterfaz.chkCliente.Checked = true;
                        else this.oInterfaz.chkCliente.Checked = false;



                        oInterfaz.txtServer.String = oSetting.Server;
                        oInterfaz.txtPuerto.String = oSetting.Puerto.ToString();
                        oInterfaz.txtCuentaUsuario.String = oSetting.CorreoCuenta;
                        oInterfaz.txtPassWord.String = oSetting.PassWord;

                        oInterfaz.txtCuentaCP.String = oSetting.CuentaServicioCP;
                        oInterfaz.txtCuentaAT.String = oSetting.CuentaAnticipo;
                        oInterfaz.txtCuentaACP.String = oSetting.CuentaContrapartida;

                        if (oSetting.DraftOS >0)
                        {
                            try
                            {
                                oForm.DataSources.UserDataSources.Item("UD_CHKD").ValueEx = oSetting.DraftOS == 1 ? "Y" : "N";
                                
                            }
                            catch { }
                        }
                        if (oSetting.EstatusOS > 0)
                        {
                            try {

                                ComboBox oCombo = (ComboBox)this.oForm.Items.Item("cmbestatus").Specific;
                                oCombo.Select(oSetting.EstatusOS.ToString(), BoSearchKey.psk_ByValue);
                            }
                            catch { }                        
                        }

                        if(oSetting.DraftSALIDA>0)
                        {
                            try
                            {
                                oForm.DataSources.UserDataSources.Item("UD_CHKDSI").ValueEx = oSetting.DraftSALIDA == 1 ? "Y" : "N";
                            }
                            catch { }
                        }

                        if (oSetting.EstatusSALIDA > 0)
                        {
                            try
                            {

                                ComboBox oCombo = (ComboBox)this.oForm.Items.Item("cmbstasi").Specific;
                                oCombo.Select(oSetting.EstatusSALIDA.ToString(), BoSearchKey.psk_ByValue);
                            }
                            catch { }
                        }

                        if (oSetting.DimCode != 0)
                        {
                            try
                            {
                                this.oInterfaz.CmbDimension.Select(oSetting.DimCode.ToString(), BoSearchKey.psk_ByValue);
                            }
                            catch { }
                        }

                        try
                        {
                            //this.oForm.DataSources.UserDataSources.Item("UDTxt1").Value = oSetting.Cuenta;
                            oInterfaz.TxtCuentas.String = oSetting.Cuenta;
                        }
                        catch { }



                        try
                        { oInterfaz.CmbTipoAnticipo.Select(oSetting.sTipoAnticipo, BoSearchKey.psk_ByValue); }
                        catch { }


                        try
                        { oInterfaz.CmbRecarga.Select(oSetting.Almacen, BoSearchKey.psk_ByValue); }
                        catch { }

                    }
                }
                else throw new Exception("No se ha podido recuperar la configuración");
            }
            catch (Exception ex)
            {

                KananSAPHerramientas.LogError("GestionConfig.cs->LlenarInterfaz()", ex.Message);
            }
        }
        private void Asignaestatus()
        {
            try
            {
                string query = @"select * from ""@VSKF_ESTATUSORDEN"" where U_Activo = 1";
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    oItem = this.oForm.Items.Item("cmbestatus");
                    oCombo = oItem.Specific;
                    Helper.KananSAPHelpers.CleanComboBox(oCombo);
                    rs.MoveFirst();
                    oCombo.ValidValues.Add(" ", "Selecciona el estatus para facturación");
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("Code").Value);
                        string description = rs.Fields.Item("Name").Value;
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }

                }
                oCombo.Item.DisplayDesc = true;

                //COMBO ESTATUS PARA SALIDAS
                if (rs.RecordCount > 0)
                {
                    oItem = this.oForm.Items.Item("cmbstasi");
                    oCombo = oItem.Specific;
                    Helper.KananSAPHelpers.CleanComboBox(oCombo);
                    rs.MoveFirst();
                    oCombo.ValidValues.Add(" ", "Selecciona el estatus para salida");
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("Code").Value);
                        string description = rs.Fields.Item("Name").Value;
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }

                }
                oCombo.Item.DisplayDesc = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region MétodosAuxiliares
        //public void GetEmailSettings()
        //{
        //    string Servidor = oInterfaz.txtServer.String.Trim().ToString();
        //    string Puerto = oInterfaz.txtPuerto.String.Trim().ToString();
        //    string Mail = oInterfaz.txtCuentaUsuario.String.Trim().ToString();
        //    string PassWord = oInterfaz.txtPassWord.String.Trim().ToString();
        //}
        public void ReenviarNotificacion()
        {
            String Attachment = string.Empty, Asunto = string.Empty, resultado = string.Empty, correoPara = string.Empty, Nombre = string.Empty, sconsultaSql = string.Empty;

            //ConfiguracionKananINFO KnSttings = this.oGlobales.KNSettings;
            SBO_KF_Configuracion_INFO oSetting = new SBO_KF_Configuracion_INFO();
            SBO_KF_Configuracion_AD oConfiguracion = new SBO_KF_Configuracion_AD();

            resultado = oConfiguracion.recuperaConfiguracion(ref oCompany, out sconsultaSql, ref oSetting);
            if (resultado == "OK")
            {
                resultado = "ERROR";
                try
                {
                    string Servidor = oInterfaz.txtServer.String.Trim().ToString();
                    string Puerto = oInterfaz.txtPuerto.String.Trim().ToString();
                    string Mail = oInterfaz.txtCuentaUsuario.String.Trim().ToString();
                    string PassWord = oInterfaz.txtPassWord.String.Trim().ToString();

                    oSetting.Server = Servidor;
                    oSetting.Puerto = Convert.ToInt32(Puerto);
                    oSetting.CorreoCuenta = Mail;
                    oSetting.PassWord = PassWord;
                }
                catch
                {
                    this.SBO_Application.StatusBar.SetText("Validar datos. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                }

                String HTML = @"<html><head><title></title></head><body>" +
                    "<h2>Validación de configuraciones Kananfleet</h2>" +
                    "<p>*Este es un correo enviado a través de un servicio de forma automatica, favor de no responderlo.*</p>" +
                    "</body></html>";

                resultado = GestionGlobales.Notificarcorreo(oSetting, oSetting.CorreoCuenta, "Servicios Kananfleet. NO RESPONDER", HTML, Attachment);

                if (resultado != "OK")
                {
                    this.SBO_Application.StatusBar.SetText(resultado, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                }
                else
                {
                    this.SBO_Application.StatusBar.SetText("Configuración de correo válida. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                }
            }
            else resultado = "No se ha podido reenviar la notificación";
        }
        #endregion MétodosAuxiliares

        #region Consultas


        public String Insertar()
        {
            String sTAbla = "";
            String strQuery = "";
            try
            {

                string chkSucursal = this.oInterfaz.chkSucursal.Checked ? "Y" : "N";
                string chkVehiculo = this.oInterfaz.chkVehiculo.Checked ? "Y" : "N";
                string chkCliente = this.oInterfaz.chkCliente.Checked ? "Y" : "N";
                string cSSL = this.oInterfaz.chkSSl.Checked ? "Y" : "N";
                string CodigoScursal = "-1";//this.oInterfaz.CmbSucursal.Value.ToString().Trim();
                string CodigoVehiculo = "-1";// this.oInterfaz.CmbVehiculo.Value.ToString().Trim();
                string chkSBO = this.oInterfaz.chkSBO.Checked ? "Y" : "N";
                string chkKanan = this.oInterfaz.chkKanan.Checked ? "Y" : "N";
                string chkAlmacen = this.oInterfaz.chkAlmacen.Checked ? "Y" : "N";
                string CodigoAlmacen = this.oInterfaz.CmbRecarga.Value.ToString().Trim();
                string Cuenta = this.oInterfaz.TxtCuentas.String.ToString().Trim();
                string chkBorrador = this.oInterfaz.chkBorrador.Checked ? "Y" : "N";
                string sTypeAnticpo = this.oInterfaz.CmbTipoAnticipo.Value.ToString() == "" ? "" :
                    Convert.ToString(this.oInterfaz.CmbTipoAnticipo.Value.ToString());
                

                //string CodigoArticulo = this.oInterfaz.TxtArticulo.String.ToString().Trim();
                int Dim = this.oInterfaz.CmbDimension.Value.ToString() == "" ? 0 : Convert.ToInt32(this.oInterfaz.CmbDimension.Value.ToString());

                string Servidor = oInterfaz.txtServer.String.Trim().ToString();
                string Puerto = oInterfaz.txtPuerto.String.Trim().ToString();
                string Mail = oInterfaz.txtCuentaUsuario.String.Trim().ToString();
                string PassWord = oInterfaz.txtPassWord.String.Trim().ToString();

                string CuentaCP = oInterfaz.txtCuentaCP.String.Trim().ToString();
                string CuentaAnticipo = oInterfaz.txtCuentaAT.String.Trim().ToString();
                string CuentaContrapartida = oInterfaz.txtCuentaACP.String.Trim().ToString();


                SBO_KF_Configuracion_INFO KNSettings = new SBO_KF_Configuracion_INFO();

                KNSettings.cSucursal = Convert.ToChar(chkSucursal);
                KNSettings.cVehiculo = Convert.ToChar(chkVehiculo);
                KNSettings.cCliente = Convert.ToChar(chkCliente);
                KNSettings.sTipoAnticipo = sTypeAnticpo;

                KNSettings.Sucursal = CodigoScursal;
                KNSettings.Vehiculo = CodigoVehiculo;

                KNSettings.cSBO = Convert.ToChar(chkSBO);
                KNSettings.cSSL = Convert.ToChar(cSSL);
                KNSettings.cKANAN = Convert.ToChar(chkKanan);
                KNSettings.cAlmacen = Convert.ToChar(chkAlmacen);
                KNSettings.Almacen = CodigoAlmacen;
                KNSettings.Articulo = "";
                KNSettings.Cuenta = Cuenta;
                KNSettings.CuentaServicioCP = CuentaCP;
                KNSettings.CuentaAnticipo = CuentaAnticipo;
                KNSettings.CuentaContrapartida = CuentaContrapartida;
                KNSettings.DimCode = Dim;
                KNSettings.Server = Servidor;
                KNSettings.Puerto = Convert.ToInt32(Puerto);
                KNSettings.CorreoCuenta = Mail;
                KNSettings.PassWord = PassWord;
                KNSettings.Borrador = chkBorrador;

                SAPbouiCOM.EditText oEditText;
                oEditText = (EditText)this.oForm.Items.Item("acc_nrc").Specific;
                KNSettings.NombreCuentaGasto = oEditText.String;

                oEditText = (EditText)this.oForm.Items.Item("acc_ncp").Specific;
                KNSettings.NombreCuentaServicio = oEditText.String;

                oEditText = (EditText)this.oForm.Items.Item("acc_nop").Specific;
                KNSettings.NombreCuentaAnticipoOP = oEditText.String;

                oEditText = (EditText)this.oForm.Items.Item("acc_nan").Specific;
                KNSettings.NombreCuentaContrapartidaOP = oEditText.String;

                CheckBox chkDraft = (CheckBox)this.oForm.Items.Item("chkdraft").Specific;
                CheckBox chkdraftSI = (CheckBox)this.oForm.Items.Item("chkdraftSI").Specific;
                ComboBox oCombo = (ComboBox)this.oForm.Items.Item("cmbestatus").Specific;
                KNSettings.DraftOS = chkDraft.Checked ? 1 : 0;
                KNSettings.EstatusOS = Convert.ToInt32(oCombo.Value);

                ComboBox oCombosi = (ComboBox)this.oForm.Items.Item("cmbstasi").Specific;
                KNSettings.EstatusSALIDA = Convert.ToInt32(oCombosi.Value);

                KNSettings.DraftSALIDA = chkdraftSI.Checked ? 1 : 0;
                SBO_KF_Configuracion_AD oConfigAD = new SBO_KF_Configuracion_AD();
                Recordset oConfig = (Recordset)this.oGlobales.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                oConfig.DoQuery(this.Buscar());
                if (oConfig.RecordCount == 0)
                    oConfigAD.InsertaUpdateConfig(ref oCompany, KNSettings);
                else oConfigAD.InsertaUpdateConfig(ref oCompany, KNSettings, true);
            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError("GestionConfig.cs->Insertar()", ex.Message);
            }
            return strQuery;
        }
        private Boolean validaExisteDimensiones()
        {
            Recordset oConfig = (Recordset)this.oGlobales.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            oConfig.DoQuery(this.Dimension());
            return oConfig.RecordCount == 0 ? false : true;
        }

        public void buscaNombreCuenta(string numcuenta, string control)
        {
            try
            {
                List<SBO_KF_OACT_INFO> lstCuentas = new List<SBO_KF_OACT_INFO>();
                SBO_KF_OCRD_PD oCuenta = new SBO_KF_OCRD_PD();
                oCuenta.listaCuentasSAP(ref this.oCompany, ref lstCuentas, numcuenta);
                if (lstCuentas.Count > 0)
                {
                    string NombreCuenta = lstCuentas[0].AcctName;
                    SAPbouiCOM.EditText oEditText = null;
                    switch (control)
                    {


                        case "CFLOACT1":
                            oEditText = (EditText)this.oForm.Items.Item("acc_nrc").Specific;
                            break;
                        case "CFLOACT2":
                            oEditText = (EditText)this.oForm.Items.Item("acc_ncp").Specific;
                            break;

                        case "CFLOACT3":
                            oEditText = (EditText)this.oForm.Items.Item("acc_nop").Specific;
                            break;
                        case "CFLOACT4":
                            oEditText = (EditText)this.oForm.Items.Item("acc_nan").Specific;
                            break;
                    }
                    oEditText.String = NombreCuenta;
                }

            }
            catch { }
        }
        private String iFiltroGrupo()
        {

            String strQuery = "", sTabla = "", iFiltro = "-1";

            if (GestionGlobales.bSqlConnection)
                sTabla = "OITB";
            else
                sTabla = @"""OITB""";

            strQuery = string.Format("SELECT * FROM {0} WHERE {1} = 'VEHICULOS KF'", sTabla, GestionGlobales.bSqlConnection ? "ItmsGrpNam" : @"""ItmsGrpNam""");
            Recordset oGrupo = (Recordset)this.oGlobales.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            oGrupo.DoQuery(strQuery);
            if (oGrupo.RecordCount > 0)
            {
                oGrupo.MoveFirst();
                iFiltro = Convert.ToString(oGrupo.Fields.Item("ItmsGrpCod").Value);

            }

            return iFiltro;
        }


        public String Buscar()
        {
            String strQuery = "";
            strQuery = string.Format(@"SELECT * FROM ""@VSKF_CONFIGURACION"" WHERE ""Code"" = 1");

            return strQuery;
        }

        public String Dimension()
        {
            string strQuery = "";
             strQuery = @"SELECT * FROM ""OADM"" WHERE ""UseMltDims"" = 'Y' ";

            return strQuery;
        }

        public String BuscaDimActivas()
        {
            string strQuery = "";
             strQuery = @"SELECT * FROM ""ODIM"" WHERE ""DimActive"" = 'Y' ";

            return strQuery;
        }

       /* public String CuentasGastos()
        {
            String strQuery = "";
             strQuery = @"SELECT ""AcctCode"", ""AcctName"" FROM OACT WHERE ""Postable"" = 'Y' AND ""FrozenFor"" = 'N'";
            return strQuery;
        }*/

        public String VehiculosActivos()
        {
            String strQuery = "";

            strQuery = string.Format(@"SELECT ""Code"",""Name"" FROM ""@VSKF_VEHICULO"" WHERE ""U_EsActivo"" = 1 ");

            return strQuery;
        }
        public String SucursalesActivas()
        {
            String strQuery = "";

            strQuery = string.Format(@"SELECT ""Code"",""Name"" FROM ""@VSKF_SUCURSAL"" WHERE ""U_EsActivo"" = 1 ");

            return strQuery;
        }
        #endregion

    }
}