﻿﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.Configuracion.Configuraciones.Objects
{
    public class ConfiguracionKananINFO
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public Char cSucursal { get; set; }
        public Char cVehiculo { get; set; }
        public Char cCliente { get; set; }
        public string sTipoAnticipo { get; set; }
        public string Sucursal { get; set; }
        public string Vehiculo { get; set; }
        public string Cliente { get; set; }
        public Char cSBO { get; set; }
        public Char cKANAN { get; set; }
        public Char cAlmacen { get; set; }
        public Char cSSL { get; set; }
        public string Almacen { get; set; }
        public string Cuenta { get; set; }

        public string CuentaServicioCP { get; set; }//guarda la cuenta para servicios de carta de porte
        public string CuentaAnticipo { get; set; }
        public string CuentaContrapartida { get; set; }
        public string Borrador { get; set; }
        public string SalidaBorrador { get; set; }

        public string Articulo { get; set; }
        public Int32 DimCode { get; set; }

        public String Server { get; set; }
        public Int32 Puerto { get; set; }
        public String CorreoCuenta { get; set; }
        public String PassWord { get; set; }
    }

    public class ConfiguracionView
    {
        public CheckBox chkAlmacen { get; set; }
        public CheckBox chkKanan { get; set; }
        public CheckBox chkCliente { get; set; }
        public CheckBox chkSBO { get; set; }
        public CheckBox chkSucursal { get; set; }
        public CheckBox chkVehiculo { get; set; }
        public CheckBox chkSSl { get; set; } 
        public CheckBox chkBorrador { get; set; }
        //INDICA SALIDA DE INVENTARIO EN MODO DRAFT
        public CheckBox chkSalidaBorrador { get; set; }

        //public ComboBox CmbSucursal { get; set; }
        //public EditText CmbVehiculo { get; set; }
        public EditText TxtCuentas { get; set; }
        public ComboBox CmbRecarga { get; set; }
        public ComboBox CmbDimension { get; set; }
        public ComboBox CmbTipoAnticipo { get; set; }

        public EditText txtServer { get; set; }
        public EditText txtPuerto { get; set; }
        public EditText txtCuentaUsuario { get; set; }
        public EditText txtPassWord { get; set; }

        public EditText txtCuentaCP { get; set; }
        public EditText txtCuentaAT { get; set; }
        public EditText txtCuentaACP { get; set; }


        ///----items---//
        public Item oAlmacenChk { get; set; }
        public Item oKananChk { get; set; }
        public Item oSBOChk { get; set; }
        public Item oSucursalChk { get; set; }
        public Item oVehiculoChk { get; set; }
        public Item oClienteChk { get; set; }
        public Item oDimension { get; set; }
        public Item oChkSSL { get; set; }
        public Item oChkBorrador { get; set; }
        //para salida de inventario en modo draft
        public Item oChkSalidaBorrador { get; set; }
        //public Item oItemCmbSucursal { get; set; }
        //public Item oItemCmbVehiculo { get; set; }
        public Item oItemCmbCuentas { get; set; }
        public Item oItemCmbRecarga { get; set; }

        public Item oItemServer { get; set; }
        public Item oItemPuerto { get; set; }

        public Item oItemCuentServicioCP { get; set; }
        public Item oItemCuentaAnticipo { get; set; }
        public Item oItemCuentaContraP { get; set; }

        public Item oItemCuenta { get; set; }
        public Item oItemPassWord { get; set; }

        public Item oItemAnticipo { get; set; }
    }

}