﻿using KananSAP.GestionServicios.CatalogoServicios.Views;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kanan.Vehiculos.BO2;
using KananWS.Interface;

namespace KananSAP.GestionServicios.CatalogoServicios.Presenter
{
    public class CatalogoServiciosPresenter
    {
        #region Atributos

        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private SAPbouiCOM.Item oItem;
        public CatalogoServiciosView view;
        public ConfigurarServicioPresenter configSerPrensenter;
        private bool Exist;
        private bool Modal { get; set; }
        private ServicioView servicioView;
        public bool isConfig { get; set; }
        private Configurations Configs;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="application">SAPbouiCOM.Application</param>
        /// <param name="company">SAPbobsCOM.Company</param>
        /// <param name="mantenibleid">MantenibleID</param>
        /// <param name="manteniblename">Nombre Mantenible</param>
        public CatalogoServiciosPresenter(SAPbouiCOM.Application application, SAPbobsCOM.Company company, Configurations configs)
        {
            this.SBO_Application = application;
            this.Company = company;
            this.Configs = configs;
            this.view = new CatalogoServiciosView(this.SBO_Application, this.Company, configs);
            this.servicioView = new ServicioView(this.SBO_Application, this.Company, configs);
            this.Exist = false;
            this.Modal = false;
            this.SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
        }

        #endregion

        #region Eventos
        /// <summary>
        /// SAPbouiCOM.Application ItemEvent
        /// </summary>
        /// <param name="FormUID">FormUID</param>
        /// <param name="pVal">SAPbouiCOM.ItemEvent</param>
        /// <param name="BubbleEvent">BubbleEvent</param>
        private void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (pVal.FormTypeEx == "ServiceForm")
                {
                    if(this.Modal)
                    {
                        this.servicioView.ShowForm();
                        BubbleEvent = false;
                        return;
                    }
                    #region beforeaction
                    
                    if (pVal.BeforeAction)
                    {
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnSearch")
                        {
                            this.view.SetConditions();
                            this.view.BindDataToForm();
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnAddSv")
                        {
                            //this.servicioPresenter = null;
                            //this.servicioPresenter = new ServicioPresenter(this.SBO_Application, this.Company, this.Configs, this.view);
                            ////this.servicioPresenter.view.EmpresaID = this.view.EmpresaID;
                            this.servicioView.LoadForm();
                            this.servicioView.PrepareNewEntity();
                            this.servicioView.HideDeleteButton();
                            this.servicioView.ShowForm();
                            //this.servicioView.FillComboCcontable();
                            this.servicioView.FillComboTServicio();
                            this.servicioView.ClearFields();
                            this.Modal = true;
                        }
                        
                        if (pVal.ItemUID == "btncancel" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.SBO_Application.Forms.ActiveForm.Close();
                        }
                        if (pVal.EventType == BoEventTypes.et_MATRIX_LINK_PRESSED)
                        {
                            this.view.SetID(pVal.Row);
                            //this.servicioPresenter = null;
                            //this.servicioPresenter = new ServicioPresenter(this.SBO_Application, this.Company, this.Configs, this.view);
                            ////this.servicioPresenter.view.EmpresaID = this.view.EmpresaID;
                            this.servicioView.PrepareNewEntity();
                            this.servicioView.Entity = this.view.oServicio;
                            this.servicioView.LoadForm();
                            this.servicioView.ShowForm();
                            this.servicioView.FillComboTServicio();
                            //this.servicioView.FillComboCcontable();
                            this.servicioView.ClearFields();
                            this.servicioView.Consultar(this.view.oServicio);
                            this.servicioView.DataEntityToForm();
                            this.Modal = true;
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ColUID == "#")
                        {
                            this.view.HasRowValue(pVal.Row);
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnDelLSv")
                        {
                            this.view.Eliminar();
                        }
                    }

                    #endregion
                }

                if (pVal.FormTypeEx == "AddServ")
                {
                    #region beforeaction

                    if (pVal.BeforeAction)
                    {
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnAceptar")
                        {
                            try
                            {
                                this.servicioView.InsertarOActualizar();
                                this.view.BindDataToForm();
                                SBO_Application.StatusBar.SetText("¡Registro guardado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                BubbleEvent = false;
                            }
                            catch (Exception ex)
                            {
                                this.view.BindDataToForm();
                                SBO_Application.StatusBar.SetText("¡Registro guardado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                KananSAP.Helper.KananSAPHerramientas.LogError("CatalogoServiciosPresenter.cs->btnAceptar()", ex.Message);
                                throw new Exception(ex.Message);
                            }

                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnDelSvr")
                        {
                            try
                            {
                                this.servicioView.EliminarCompleto();
                                BubbleEvent = false;
                                this.SBO_Application.Forms.ActiveForm.Close();
                                this.view.BindDataToForm();
                            }
                            catch (Exception ex)
                            {
                                this.SBO_Application.Forms.ActiveForm.Close();
                                this.view.BindDataToForm();
                                KananSAP.Helper.KananSAPHerramientas.LogError("CatalogoServiciosPresenter.cs->btnDelSvr()", ex.Message);
                                throw new Exception(ex.Message);
                            }

                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnCancelS")
                        {
                            this.Modal = false;
                            this.SBO_Application.Forms.ActiveForm.Close();
                        }
                        if (pVal.EventType == BoEventTypes.et_FORM_UNLOAD)
                            this.Modal = false;
                        if (pVal.EventType == BoEventTypes.et_FORM_CLOSE && this.Modal)
                            this.Modal = false;
                    }
                    else
                    {

                        if (pVal.EventType == BoEventTypes.et_CHOOSE_FROM_LIST)
                        {
                            SAPbouiCOM.IChooseFromListEvent oCFLEvento = null;
                            oCFLEvento = ((SAPbouiCOM.IChooseFromListEvent)(pVal));
                            string sCFL_ID = null;
                            sCFL_ID = oCFLEvento.ChooseFromListUID;
                            SAPbouiCOM.Form oForm = null;
                            oForm = SBO_Application.Forms.Item(FormUID);
                            SAPbouiCOM.ChooseFromList oCFL = null;
                            oCFL = oForm.ChooseFromLists.Item(sCFL_ID);
                            if (oCFLEvento.BeforeAction == false)
                            {
                                SAPbouiCOM.DataTable oDataTable = null;
                                oDataTable = oCFLEvento.SelectedObjects;
                                string val = null;
                                try
                                {
                                    val = System.Convert.ToString(oDataTable.GetValue(0, 0));
                                    string sitem = "";
                                    switch (sCFL_ID)
                                    {
                                        case "CFLOITM":
                                            sitem = "EdtOITM";
                                            break;
                                        case "CFLOACT":
                                            sitem = "EdtOACT";
                                            break;
                                        default:
                                            break;
                                    }
                                    oForm.DataSources.UserDataSources.Item(sitem).ValueEx = val;
                                    this.servicioView.cargaNombreCuenta(val);
                                }
                                catch (Exception ex)
                                {

                                }
                            }
                        }
                    }
                    #endregion
                }
                
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("CatalogoServiciosPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
                SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
            }
        }

        #endregion


    }
}
