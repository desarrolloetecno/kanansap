﻿using KananSAP.GestionServicios.CatalogoServicios.Views;
using KananWS.Interface;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.GestionServicios.CatalogoServicios.Presenter
{
    public class ListaTServiciosPresenter
    {
        #region VARIABLES

        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        public ListaTServiciosView view;
        public TipoServicioView viewTServicio;
        private Configurations Configs;
        private bool Modal { get; set; }
        #endregion

        #region Constructor

        public ListaTServiciosPresenter(SAPbouiCOM.Application application, SAPbobsCOM.Company company, Configurations config)
        {
            this.SBO_Application = application;
            this.Company = company;
            this.Configs = config;
            this.Modal = false;
            this.view = new ListaTServiciosView(this.SBO_Application, this.Company, config);
            this.viewTServicio = new TipoServicioView(this.SBO_Application, this.Company, this.Configs);
            this.SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
        }

        #endregion

        #region EVENTOS
        private void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                
                #region Catalogo Tipos de Servicio
                if (pVal.FormTypeEx == "LstTServ")
                {
                    if (this.Modal)
                    {
                        this.viewTServicio.ShowForm();
                        BubbleEvent = false;
                        return;
                    }
                    #region Beforeaction CatalogoTipoVehiculo

                    if (pVal.BeforeAction)
                    {
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnBsqTS")
                        {
                            this.view.SetConditions();
                            this.view.BindData();
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnAddUp")
                        {
                            //this.viewTServicio = null;
                            this.viewTServicio.LoadForm();
                            this.viewTServicio.HideDeleteButton();
                            this.viewTServicio.ShowForm();
                            this.Modal = true;
                            //this.tServicioPresenter = null;
                            //this.tServicioPresenter = new TipoServicioPresenter(SBO_Application, Company,this.Configs, this.view);
                            //this.tServicioPresenter.view.EmpresaID = this.view.EmpresaID;
                            //this.tServicioPresenter.view.LoadForm();
                            //this.tServicioPresenter.view.HideDeleteButton();
                            //this.tServicioPresenter.view.ShowForm();
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnCancel")
                        {
                            this.SBO_Application.Forms.ActiveForm.Close();
                        }
                        if (pVal.EventType == BoEventTypes.et_MATRIX_LINK_PRESSED)
                        {
                            this.view.SetID(pVal.Row);
                            if (this.view.oTipoServicio != null)
                            {
                                if (this.view.oTipoServicio.TipoServicioID != null)
                                {
                                    //this.viewTServicio = null;
                                    //this.viewTServicio = new TipoServicioView(SBO_Application, Company, this.Configs);
                                    this.viewTServicio.PrepareNewEntity();
                                    this.viewTServicio.Entity = this.view.oTipoServicio;
                                    this.viewTServicio.EmpresaID = this.view.EmpresaID;
                                    this.viewTServicio.LoadForm();
                                    this.viewTServicio.ShowForm();
                                    this.viewTServicio.ClearFields();
                                    this.viewTServicio.Consultar(this.view.oTipoServicio);
                                    this.viewTServicio.DataEntityToForm();
                                    this.viewTServicio.isUpdate = true;
                                    this.Modal = true;
                                    //this.tServicioPresenter = null;
                                    //this.tServicioPresenter = new TipoServicioPresenter(SBO_Application, Company,this.Configs, this.view);
                                    //this.tServicioPresenter.view.Entity = this.view.oTipoServicio;
                                    //this.tServicioPresenter.view.EmpresaID = this.view.EmpresaID;
                                    //this.tServicioPresenter.view.LoadForm();
                                    //this.tServicioPresenter.view.ShowForm();
                                    //this.tServicioPresenter.view.ClearFields();
                                    //this.tServicioPresenter.view.Consultar(this.view.oTipoServicio);
                                    //this.tServicioPresenter.view.DataEntityToForm();
                                    //this.tServicioPresenter.view.isUpdate = true;
                                }
                            }
                            //else
                            //{
                            //    //this.viewTServicio = null;
                            //    //this.viewTServicio = new TipoServicioView(SBO_Application, Company, this.Configs);
                            //    this.viewTServicio.EmpresaID = this.view.EmpresaID;
                            //    this.viewTServicio.LoadForm();
                            //    this.viewTServicio.ShowForm();
                            //    this.viewTServicio.ClearFields();
                            //    this.Modal = true;
                            //    //this.tServicioPresenter = null;
                            //    //this.tServicioPresenter = new TipoServicioPresenter(SBO_Application, Company,this.Configs, this.view);
                            //    //this.tServicioPresenter.view.EmpresaID = this.view.EmpresaID;
                            //    //this.tServicioPresenter.view.LoadForm();
                            //    //this.tServicioPresenter.view.ShowForm();
                            //    //this.tServicioPresenter.view.ClearFields();
                            //}
                        }
                        if (pVal.ColUID == "#id" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                            this.view.HasRowValue(pVal.Row);
                        if (pVal.ItemUID == "btnDelLTs" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            if(this.view.TServicioID.HasValue)
                            {
                                this.view.Eliminar();
                                this.view.EliminarDesdeGestion();
                            }
                        }
                    }

                    #endregion
                }
                #endregion

                #region TipoServicio
                if (pVal.FormTypeEx == "TServicio")
                {
                    if (pVal.EventType == BoEventTypes.et_FORM_CLOSE && this.Modal)
                        this.Modal = false;

                    #region BeforeAction
                    if (pVal.BeforeAction)
                    {
                        if (pVal.ItemUID == "btnAceptar" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            try
                            {
                                this.viewTServicio.InsertUpdate();
                                this.view.BindData();
                                SBO_Application.StatusBar.SetText("¡Registro guardado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                            }
                            catch (Exception ex)
                            {
                                this.view.BindData();
                                this.SBO_Application.Forms.ActiveForm.Close();
                                KananSAP.Helper.KananSAPHerramientas.LogError("ListaTServiciosPresenter.cs->btnAceptar()", ex.Message);
                                throw new Exception(ex.Message);
                            }

                        }
                        if (pVal.ItemUID == "btnDelTs" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            try
                            {
                                this.viewTServicio.EliminarCompleto();
                                BubbleEvent = false;
                                this.SBO_Application.Forms.ActiveForm.Close();
                                this.view.BindData();
                            }
                            catch (Exception ex)
                            {
                                this.SBO_Application.Forms.ActiveForm.Close();
                                this.view.BindData();
                                KananSAP.Helper.KananSAPHerramientas.LogError("ListaTServiciosPresenter.cs->btnDelTs()", ex.Message);
                                throw new Exception(ex.Message);
                            }
                                                        //this.Modal = false;
                            
                            //this.view.ClearFields();
                            //this.view.HideDeleteButton();
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnCancTS")
                        {
                            this.Modal = false;
                            this.SBO_Application.Forms.ActiveForm.Close();
                        }
                        if (pVal.EventType == BoEventTypes.et_FORM_UNLOAD)
                        {
                            this.Modal = false;
                        }
                    }
                    #endregion
                }
                #endregion

            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("ListaTServiciosPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
                SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
            }
        }
        #endregion

    }
}
