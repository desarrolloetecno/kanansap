﻿using KananSAP.GestionServicios.CatalogoServicios.Views;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KananSAP.Mantenimiento.Data;
using KananWS.Interface;

namespace KananSAP.GestionServicios.CatalogoServicios.Presenter
{
    public class ConfigurarServicioPresenter
    {
        #region Atributos

        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private SAPbouiCOM.Item oItem;
        public ConfigurarServicioView view;
        private Form oForm;
        private bool Exist;
        private bool Modal;
        private Configurations Configs;
        #endregion

        #region Constructor

        public ConfigurarServicioPresenter(SAPbouiCOM.Application application, SAPbobsCOM.Company company, Configurations config)
        {
            this.SBO_Application = application;
            this.Company = company;
            this.view = new ConfigurarServicioView(this.SBO_Application, this.Company, config);
            this.Exist = false;
            this.Modal = false;
            this.Configs = config;
            this.SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
        }

        #endregion

        #region Eventos

        private void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (pVal.FormTypeEx == "ConfSvForm")
                {
                    if (Modal)
                    {
                        view.ShowForm();
                        BubbleEvent = false;
                        return;
                    }
                    #region beforeaction

                    if (pVal.BeforeAction)
                    {
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "rbtPmtroDt")
                        {
                            this.view.TipoParametro = 1;
                            this.view.ClearTimeParameters();
                            this.view.DisableTimePamameters();
                            this.view.EnableDistancePamameters();
                            //this.view.EnableDisbaleParametersFields();
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "rbtPmtroTm")
                        {
                            this.view.TipoParametro = 2;
                            this.view.ClearDistanceParameters();
                            this.view.DisableDistancePamameters();
                            this.view.EnableTimePamameters();
                            //this.view.EnableDisbaleParametersFields();
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "rbtPmtroAm")
                        {
                            this.view.TipoParametro = 3;
                            this.view.EnableTimePamameters();
                            this.view.EnableDistancePamameters();
                            //this.view.EnableDisbaleParametersFields();
                        }

                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "rbtPmtroHr")
                        {
                            this.view.TipoParametro = 4;
                            this.view.EnableTimePamameters();
                            this.view.EnableDistancePamameters();
                            //this.view.EnableDisbaleParametersFields();
                        }

                        #region Guardar
                        /*
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnAConfSv")
                        {
                            this.view.InsertOrUpdateParameters();
                            this.SBO_Application.StatusBar.SetText("Parámetros de mantenimiento guardados", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                            BubbleEvent = false;
                        }
                        */
                        #endregion Guardar

                        if (pVal.ItemUID == "btnCConfSv" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.view.ClearAllFields();
                            this.view.CloseForm();
                            BubbleEvent = false;
                        }
                        if (pVal.EventType == BoEventTypes.et_KEY_DOWN && pVal.ItemUID == "txtFrcSvDt")
                        {
                            int charPressed = pVal.CharPressed;
                            if (charPressed == 9 || charPressed == 13)
                                this.view.CalcularProximoServicio();
                            //int charPressed = pVal.CharPressed;
                            //this.view.CalcularProximoServicio();
                        }
                        else if (pVal.EventType == BoEventTypes.et_CLICK && pVal.ItemUID == "txtFrcSvDt")
                            this.view.CalcularProximoServicio();

                        if (pVal.EventType == BoEventTypes.et_KEY_DOWN && pVal.ItemUID == "ProxServHr")
                        {
                            int charPressed = pVal.CharPressed;
                            if (charPressed == 9 || charPressed == 13)
                                this.view.CalcularProximoServicio();
                            //int charPressed = pVal.CharPressed;
                            //this.view.CalcularProximoServicio();
                        }

                        if (pVal.EventType == BoEventTypes.et_KEY_DOWN && pVal.ItemUID == "txtUltSvDt")
                        {
                            int charPressed = pVal.CharPressed;
                            if (charPressed == 9 || charPressed == 13)
                                this.view.CalcularProximoServicio();
                            //int charPressed = pVal.CharPressed;
                            //this.view.CalcularProximoServicio();
                        }
                        else if (pVal.EventType == BoEventTypes.et_CLICK && pVal.ItemUID == "txtUltSvDt")
                            this.view.CalcularProximoServicio();
                        if (pVal.EventType == BoEventTypes.et_KEY_DOWN && pVal.ItemUID == "txtPrxSvDt")
                        {
                            int charPressed = pVal.CharPressed;
                            if (charPressed == 9 || charPressed == 13)
                                this.view.CalcularProximoServicio();
                            //int charPressed = pVal.CharPressed;
                            //this.view.CalcularProximoServicio();
                        }
                        else if (pVal.EventType == BoEventTypes.et_CLICK && pVal.ItemUID == "txtPrxSvDt")
                            this.view.CalcularProximoServicio();
                        //if (pVal.ItemUID == "txtFrcSvDt" && pVal.EventType == BoEventTypes.et_CLICK || pVal.CharPressed == 9)
                        //    this.view.CalcularProximoServicio();
                        //if (pVal.ItemUID == "txtUltSvDt" && pVal.EventType == BoEventTypes.et_CLICK || pVal.CharPressed == 9)
                        //    this.view.CalcularProximoServicio();
                        //if (pVal.ItemUID == "txtPrxSvDt" && pVal.EventType == BoEventTypes.et_CLICK || pVal.CharPressed == 9)
                        //    this.view.CalcularProximoServicio();
                    }
                    
                    #endregion
                }

            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("ConfigurarServicioPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
                SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                BubbleEvent = false;
            }
        }

        private BoFormMode GetMode(string uid)
        {
            this.oForm = SBO_Application.Forms.Item(uid);
            return this.oForm.Mode;
        }

        #endregion

    }
}
