﻿using KananSAP.GestionServicios.CatalogoServicios.Views;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kanan.Vehiculos.BO2;
using KananWS.Interface;

namespace KananSAP.GestionServicios.CatalogoServicios.Presenter
{
    public class ListaServicioConfigPresenter
    {
        #region Atributos

        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private SAPbouiCOM.Item oItem;
        public ListaServicioConfigView listaServicioView;
        //public ConfigurarServicioPresenter configSerPrensenter;
        private bool Exist;
        //private bool Modal { get; set; }
        //private ServicioPresenter servicioPresenter;
        public bool isConfig { get; set; }
        private Configurations Configs;
        public ConfigurarServicioView configServicioView;
        //private bool ModalConfigServicio { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="application">SAPbouiCOM.Application</param>
        /// <param name="company">SAPbobsCOM.Company</param>
        /// <param name="mantenibleid">MantenibleID</param>
        /// <param name="manteniblename">Nombre Mantenible</param>
        public ListaServicioConfigPresenter(SAPbouiCOM.Application application, SAPbobsCOM.Company company, Configurations configs)
        {
            this.SBO_Application = application;
            this.Company = company;
            this.Configs = configs;
            this.listaServicioView = new ListaServicioConfigView(this.SBO_Application, this.Company, configs);
            this.Exist = false;
            //this.Modal = false;
            //this.ModalConfigServicio = false;
            this.SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
            configServicioView = new ConfigurarServicioView(this.SBO_Application, this.Company, configs);
        }

        #endregion

        #region Eventos
        /// <summary>
        /// SAPbouiCOM.Application ItemEvent
        /// </summary>
        /// <param name="FormUID">FormUID</param>
        /// <param name="pVal">SAPbouiCOM.ItemEvent</param>
        /// <param name="BubbleEvent">BubbleEvent</param>
        private void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (pVal.FormTypeEx == "LstServ")
                {
                    //Venta de la lista de servicios configurados al vehiculo
                    /*if (Modal)
                    {
                        listaServicioView.ShowForm();
                        BubbleEvent = false;
                        return;
                    }*/
                    //Ventana de configuracion de parámetros de servicios
                    /*if (ModalConfigServicio)
                    {
                        configServicioView.ShowForm();
                        BubbleEvent = false;
                        return;
                    }*/

                    #region beforeaction
                    
                    if (pVal.BeforeAction)
                    {
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnBusq")
                        {
                            this.listaServicioView.SetConditions();
                            this.listaServicioView.ShowParametrosMantenimiento();
                        }

                        #region Comentado
                        //if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnAddSv")
                        //{
                        //    this.servicioPresenter = null;
                        //    this.servicioPresenter = new ServicioPresenter(this.SBO_Application, this.Company, this.Configs);
                        //    this.servicioPresenter.view.EmpresaID = this.listaServicioView.EmpresaID;
                        //    this.servicioPresenter.view.LoadForm();
                        //    this.servicioPresenter.view.ShowForm();
                        //    this.servicioPresenter.view.FillComboTServicio();
                        //    this.servicioPresenter.view.ClearFields();
                        //}
                        #endregion

                        #region EditarParametroServicio
                        /*
                        if (pVal.EventType == BoEventTypes.et_MATRIX_LINK_PRESSED)
                        {
                            this.listaServicioView.SetID(pVal.Row);
                            if (this.listaServicioView.oServicio != null && this.listaServicioView.oVehiculo != null)
                            {
                                if (this.listaServicioView.oServicio.ServicioID != null && this.listaServicioView.oVehiculo.VehiculoID != null)
                                {

                                    #region Comentado
                                    //this.configSerPrensenter = null;
                                    //this.configSerPrensenter = new ConfigurarServicioPresenter(this.SBO_Application, this.Company, this.Configs);
                                    //this.configSerPrensenter.view.oServicio = this.listaServicioView.oServicio;
                                    //this.configSerPrensenter.view.oVehiculo = this.listaServicioView.oVehiculo;
                                    //this.configSerPrensenter.view.LoadForm();
                                    //this.configSerPrensenter.view.FillComboServicio();
                                    //this.configSerPrensenter.view.ShowForm();
                                    //this.configSerPrensenter.view.ClearAllFields();
                                    //this.configSerPrensenter.view.SetServiceVehicleData();
                                    //if (this.configSerPrensenter.view.VerificaExistenciaParametro())
                                    //    this.configSerPrensenter.view.ShowCheckBox();//this.configSerPrensenter.view.SetFormMode(SAPbouiCOM.BoFormMode.fm_UPDATE_MODE);
                                    //else
                                    //    this.configSerPrensenter.view.HideCheckBox();//this.configSerPrensenter.view.SetFormMode(SAPbouiCOM.BoFormMode.fm_ADD_MODE);
                                    #endregion

                                    this.ModalConfigServicio = true;
                                    this.configServicioView.ShowForm();
                                    this.configServicioView.oServicio = this.listaServicioView.oServicio;
                                    this.configServicioView.oVehiculo = this.listaServicioView.oVehiculo;
                                    this.configServicioView.ClearAllFields();
                                    this.configServicioView.PrepareNewEntity();
                                    this.configServicioView.SetServiceVehicleData();
                                    if (this.configServicioView.VerificaExistenciaParametro())
                                        this.configServicioView.ShowCheckBox();
                                    else
                                        this.configServicioView.HideCheckBox();
                                }
                            }
                            BubbleEvent = false;
                        }
                        */
                        #endregion EditarParametroServicio

                        if (pVal.ColUID == "#" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                            this.listaServicioView.HasRowValue(pVal.Row);

                        #region EliminarParametroServicio
                        /*
                        if (pVal.ItemUID == "btnDelLst" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            if (this.listaServicioView.PMantenimientoID.HasValue && this.listaServicioView.PServicioID.HasValue)
                            {
                                if(this.listaServicioView.EliminarParametros())
                                {
                                    this.SBO_Application.StatusBar.SetText("¡Parámetro de Mantenimiento Eliminado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                    this.listaServicioView.ShowParametrosMantenimiento();
                                }
                                else
                                    this.listaServicioView.InitializeIDS();
                            }
                        }
                        */
                        #endregion EliminarParametroServicio

                        #region AgregarParametroServicio
                        /*
                         * Se movió para manejarlo en el presentador de su página encargada de lanzar el formulario.
                        if (pVal.ItemUID == "btnAddPMto" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            #region Comentado
                            //this.configSerPrensenter = null;
                            //this.configSerPrensenter = new ConfigurarServicioPresenter(this.SBO_Application, this.Company, this.Configs);
                            ////this.configSerPrensenter.view.oServicio = this.listaServicioView.oServicio;
                            //this.configSerPrensenter.view.LoadForm();
                            //this.configSerPrensenter.view.oServicio = null;
                            //this.configSerPrensenter.view.oVehiculo = this.listaServicioView.oVehiculo;
                            //this.configSerPrensenter.view.FillComboServicio();
                            //this.configSerPrensenter.view.ShowForm();
                            //this.configSerPrensenter.view.ClearAllFields();
                            //this.configSerPrensenter.view.SetServiceVehicleData();
                            //this.configSerPrensenter.view.HideCheckBox();
                            #endregion Comentado

                            this.listaServicioView.InitializeIDS();
                            this.ModalConfigServicio = true;
                            this.configServicioView.ShowForm();
                            this.configServicioView.oServicio = null;
                            this.configServicioView.oVehiculo = this.listaServicioView.oVehiculo;
                            this.configServicioView.ClearAllFields();
                            this.configServicioView.PrepareNewEntity();
                            this.configServicioView.SetServiceVehicleData();
                            this.configServicioView.HideCheckBox();
                            this.configServicioView.HiddeDeleteButton();
                        }
                        */
                        #endregion AgregarParametroServicio

                        /*if (pVal.EventType == BoEventTypes.et_FORM_CLOSE && this.Modal)
                        {
                            this.Modal = false;
                            this.configServicioView.TipoParametro = 0;
                        }
                        if (pVal.EventType == BoEventTypes.et_FORM_UNLOAD)
                        {
                            this.Modal = false;
                            this.configServicioView.TipoParametro = 0;
                        }*/
                    }

                    #endregion
                }

                if (pVal.FormTypeEx == "ConfSvForm")
                {
                    
                    #region beforeaction

                    if (pVal.BeforeAction)
                    {
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "rbtPmtroDt")
                        {
                            this.configServicioView.TipoParametro = 1;
                            this.configServicioView.ClearTimeParameters();
                            this.configServicioView.DisableTimePamameters();
                            this.configServicioView.EnableDistancePamameters();
                            //this.view.EnableDisbaleParametersFields();
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "rbtPmtroTm")
                        {
                            this.configServicioView.TipoParametro = 2;
                            this.configServicioView.ClearDistanceParameters();
                            this.configServicioView.DisableDistancePamameters();
                            this.configServicioView.EnableTimePamameters();
                            //this.view.EnableDisbaleParametersFields();
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "rbtPmtroAm")
                        {
                            this.configServicioView.TipoParametro = 3;
                            this.configServicioView.EnableTimePamameters();
                            this.configServicioView.EnableDistancePamameters();
                            //this.view.EnableDisbaleParametersFields();
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "rbtPmtroHr")
                        {
                            this.configServicioView.TipoParametro = 4;
                            this.configServicioView.EnableHoursPamameters();
                            
                            //this.configServicioView.EnableDistancePamameters();
                            //this.view.EnableDisbaleParametersFields();
                        }

                        #region GuardarParametroServicio
                        /*
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnAConfSv")
                        {
                            try
                            {
                                this.configServicioView.InsertOrUpdateParameters();
                                this.SBO_Application.StatusBar.SetText("Parámetros de mantenimiento guardados", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                this.listaServicioView.InitializeIDS();
                                this.listaServicioView.ShowParametrosMantenimiento();
                                BubbleEvent = false;
                            }
                            catch (Exception ex) {
                                this.listaServicioView.InitializeIDS();
                                this.listaServicioView.ShowParametrosMantenimiento();
                                //this.SBO_Application.Forms.ActiveForm.Close();
                                throw new Exception(ex.Message); }

                        }
                        */
                        #endregion GuardarParametroServicio

                        #region ConfigurarParametroServicio
                        /*
                        if (pVal.ItemUID == "btnCConfSv" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.ModalConfigServicio = false;
                            this.listaServicioView.InitializeIDS();
                            this.configServicioView.ClearAllFields();
                            this.configServicioView.CloseForm();
                            BubbleEvent = false;
                        }
                        */
                        #endregion ConfigurarParametroServicio

                        if (pVal.EventType == BoEventTypes.et_KEY_DOWN && pVal.ItemUID == "txtFrcSvDt")
                        {
                            int charPressed = pVal.CharPressed;
                            if (charPressed == 9 || charPressed == 13)
                                this.configServicioView.CalcularProximoServicio();
                            //int charPressed = pVal.CharPressed;
                            //this.view.CalcularProximoServicio();
                        }
                        else if (pVal.EventType == BoEventTypes.et_CLICK && pVal.ItemUID == "txtFrcSvDt")
                            this.configServicioView.CalcularProximoServicio();
                        if (pVal.EventType == BoEventTypes.et_KEY_DOWN && pVal.ItemUID == "txtUltSvDt")
                        {
                            int charPressed = pVal.CharPressed;
                            if (charPressed == 9 || charPressed == 13)
                                this.configServicioView.CalcularProximoServicio();
                            //int charPressed = pVal.CharPressed;
                            //this.view.CalcularProximoServicio();
                        }
                        else if (pVal.EventType == BoEventTypes.et_CLICK && pVal.ItemUID == "txtUltSvDt")
                            this.configServicioView.CalcularProximoServicio();
                        if (pVal.EventType == BoEventTypes.et_KEY_DOWN && pVal.ItemUID == "txtPrxSvDt")
                        {
                            int charPressed = pVal.CharPressed;
                            if (charPressed == 9 || charPressed == 13)
                                this.configServicioView.CalcularProximoServicio();
                            //int charPressed = pVal.CharPressed;
                            //this.view.CalcularProximoServicio();
                        }
                        else if (pVal.EventType == BoEventTypes.et_CLICK && pVal.ItemUID == "txtPrxSvDt")
                            this.configServicioView.CalcularProximoServicio();
                        
                        //////////----------------------------
                        if (pVal.EventType == BoEventTypes.et_KEY_DOWN && pVal.ItemUID == "FrecServHr")
                        {
                            int charPressed = pVal.CharPressed;
                            if (charPressed == 9 || charPressed == 13)
                                this.configServicioView.CalcularProximoServicioHora();
                            //int charPressed = pVal.CharPressed;
                            //this.view.CalcularProximoServicio();
                        }
                        else if (pVal.EventType == BoEventTypes.et_CLICK && pVal.ItemUID == "FrecServHr")
                            this.configServicioView.CalcularProximoServicioHora();
                        if (pVal.EventType == BoEventTypes.et_KEY_DOWN && pVal.ItemUID == "UltServHr")
                        {
                            int charPressed = pVal.CharPressed;
                            if (charPressed == 9 || charPressed == 13)
                                this.configServicioView.CalcularProximoServicioHora();
                            //int charPressed = pVal.CharPressed;
                            //this.view.CalcularProximoServicio();
                        }
                        else if (pVal.EventType == BoEventTypes.et_CLICK && pVal.ItemUID == "UltServHr")
                            this.configServicioView.CalcularProximoServicioHora();
                        if (pVal.EventType == BoEventTypes.et_KEY_DOWN && pVal.ItemUID == "ProxServHr")
                        {
                            int charPressed = pVal.CharPressed;
                            if (charPressed == 9 || charPressed == 13)
                                this.configServicioView.CalcularProximoServicioHora();
                            //int charPressed = pVal.CharPressed;
                            //this.view.CalcularProximoServicio();
                        }
                        else if (pVal.EventType == BoEventTypes.et_CLICK && pVal.ItemUID == "ProxServHr")
                            this.configServicioView.CalcularProximoServicioHora();
                        ////// ----------------------------------

                        #region EliminarParametroServicio
                        /*
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnDelPMS")
                        {
                            if(this.listaServicioView.PMantenimientoID.HasValue && this.listaServicioView.PServicioID.HasValue)
                            {
                                if (this.listaServicioView.EliminarParametros())
                                {
                                    this.SBO_Application.StatusBar.SetText("¡Parámetro de Mantenimiento Eliminado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                    this.listaServicioView.ShowParametrosMantenimiento();
                                    this.ModalConfigServicio = false;
                                    this.SBO_Application.Forms.ActiveForm.Close();
                                }
                                else
                                    this.listaServicioView.InitializeIDS();
                            }
                        }
                        */
                        #endregion EliminarParametroServicio

                        /*if (pVal.EventType == BoEventTypes.et_FORM_CLOSE && this.ModalConfigServicio)
                        {
                            this.ModalConfigServicio = false;
                            this.configServicioView.oServicio = null;
                            this.configServicioView.TipoParametro = 0;
                        }
                        if (pVal.EventType == BoEventTypes.et_FORM_UNLOAD)
                        {
                            this.configServicioView.oServicio = null;
                            this.ModalConfigServicio = false;
                            this.configServicioView.TipoParametro = 0;
                        }*/

                        #region Comentado
                        //if (pVal.ItemUID == "txtFrcSvDt" && pVal.EventType == BoEventTypes.et_CLICK || pVal.CharPressed == 9)
                        //    this.view.CalcularProximoServicio();
                        //if (pVal.ItemUID == "txtUltSvDt" && pVal.EventType == BoEventTypes.et_CLICK || pVal.CharPressed == 9)
                        //    this.view.CalcularProximoServicio();
                        //if (pVal.ItemUID == "txtPrxSvDt" && pVal.EventType == BoEventTypes.et_CLICK || pVal.CharPressed == 9)
                        //    this.view.CalcularProximoServicio();
                        #endregion Comentado

                    }

                    #endregion
                }
                
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("ListaServicioConfigPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
                this.listaServicioView.ShowParametrosMantenimiento();
                //SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
            }
        }

        #endregion


    }
}
