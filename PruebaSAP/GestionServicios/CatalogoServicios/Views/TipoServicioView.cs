﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kanan.Mantenimiento.BO2;
using KananWS.Interface;

namespace KananSAP.GestionServicios.CatalogoServicios.Views
{
    public class TipoServicioView
    {
        #region Atributes
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.StaticText oStaticText;
        private SAPbouiCOM.Button oButton;
        public TipoServicioView view;
        private XMLPerformance XmlApplication;
        public TipoServicio Entity { get; set; }
        private KananSAP.Mantenimiento.Data.TipoServicioData tServicioData;
        private KananWS.Interface.ServiciosWS servicioWS;
        public int? EmpresaID { get; set; }
        private Configurations Configs;
        public bool isUpdate { get; set; }
        #endregion
        #region Constructor
        public TipoServicioView(SAPbouiCOM.Application app, SAPbobsCOM.Company company, Configurations config)
        {
            this.Company = company;
            this.SBO_Application = app;
            this.XmlApplication = new XMLPerformance(this.SBO_Application);
            this.PrepareNewEntity();
            this.tServicioData = new KananSAP.Mantenimiento.Data.TipoServicioData(company);
            this.Configs = config;
            //this.EmpresaID = this.Configs.UserFull.Dependencia.EmpresaID;
            this.servicioWS = new KananWS.Interface.ServiciosWS(Guid.Parse(config.UserFull.Usuario.PublicKey), config.UserFull.Usuario.PrivateKey);
            
        }

        #endregion

        #region Form

        public void PrepareNewEntity()
        {
            this.Entity = new Kanan.Mantenimiento.BO2.TipoServicio();
            this.Entity.Propietario = new Kanan.Operaciones.BO2.Empresa();
            this.Entity.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
        }
        /// <summary>
        /// Carga el formulario de configuración de servicios
        /// </summary>
        public void LoadForm()
        {
            try
            {
                this.oForm = this.SBO_Application.Forms.Item("TServicio");
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Invalid Form"))
                {
                    string sPath = System.Windows.Forms.Application.StartupPath;
                    
                    this.XmlApplication.LoadFromXML(sPath + "\\XML",
                            "TipoServicio.xml");
                    
                    this.oForm = SBO_Application.Forms.Item("TServicio");
                    //Establece la ventana centrada
                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;

                }
                else
                {
                    SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                }
            }
        }

        /// <summary>
        /// Muestra el formulario de configuración de servicios
        /// </summary>
        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
            }
            catch (Exception ex)
            {
                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                this.oForm.Select();
            }
        }

        #endregion

        #region Validaciones

        public string ValidatesFields()
        {
            try
            {
                StringBuilder mnsErr = new StringBuilder();
                string err = string.Empty;

                oItem = this.oForm.Items.Item("txtNombre");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                if (string.IsNullOrEmpty(oEditText.String))
                    mnsErr.Append(", nombre del servicio");
                if (this.Configs.UserFull.Dependencia.EmpresaID == null)
                    mnsErr.Append(", empresa");
                oItem = this.oForm.Items.Item("txtDescrip");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                if (string.IsNullOrEmpty(oEditText.String))
                    mnsErr.Append(", Descripción del servicio");
                if (mnsErr.Length > 0)
                {
                    err = mnsErr.ToString();
                    if (err.StartsWith(","))
                        err = "Los siguientes campos son obligatorios: " + err.Substring(1);
                }
                return err;
            }
            catch (Exception ex)
            {
                throw new Exception("Agregar tipo de servicio. ValidatesFields: " + ex.Message);
            }

        }

        #endregion

        #region ABC WEB SERVICE KANAN

        private bool InsertarWS()
        {
            try
            {
                TipoServicio tsTemp = this.servicioWS.InsertarTipoServicio(this.Entity);
                if (tsTemp == null)
                    return false;
                if (tsTemp.TipoServicioID == null)
                    return false;
                this.Entity.TipoServicioID = tsTemp.TipoServicioID;
                return true;
            }
            catch(Exception ex)
            {
                throw new Exception("Kanan-InsertarWS: " + ex.Message);
            }
        }

        private bool ActualizarWS()
        {
            try
            {
                return this.servicioWS.ActualizaTipoServicio(this.Entity);
            }
            catch (Exception ex)
            {
                throw new Exception("Kanan-ActualizarWS: " + ex.Message);
            }
        }

        private bool EliminarWS()
        {
            try
            {
                return this.servicioWS.EliminaTipoServicio(this.Entity.TipoServicioID);
            }
            catch(Exception ex)
            {
                throw new Exception("Kanan-EliminarWS: " + ex.Message);
            }
        }
        #endregion

        #region ABC SAP

        /// <summary>
        /// Actualiza en SAP
        /// </summary>
        private void Actualizar()
        {
            try
            {
                this.tServicioData.oTipoServicio = this.Entity;
                tServicioData.ItemCode = this.Entity.TipoServicioID.ToString();
                if (tServicioData.Existe())
                {
                    this.tServicioData.GetSyncUUID();
                    this.tServicioData.Sincronizado = 2;
                    this.tServicioData.TipoServicioToUDT();
                    this.tServicioData.Actualizar();
                    if(this.ActualizarWS())
                    {
                        this.tServicioData.Existe();
                        this.tServicioData.Sincronizado = 0;
                        this.tServicioData.Sincronizar();
                    }
                }
                this.ClearFields();
            }
            catch (Exception ex)
            {
                throw new Exception("SAP-Actualizar: " + ex.Message);
            }
        }
        /// <summary>
        /// Guarda la informacion en SAP
        /// </summary>
        private void Insertar()
        {
            try
            {
                this.tServicioData.oTipoServicio = this.Entity;
                this.tServicioData.Sincronizado = 1;
                this.tServicioData.UUID = Guid.NewGuid();
                this.tServicioData.ItemCode = "AAA"; //DateTime.Now.Ticks.ToString();
                this.tServicioData.TipoServicioToUDT();
                TipoServicio Existe = new TipoServicio() { Nombre = this.tServicioData.oTipoServicio.Nombre };
                if (tServicioData.Consultar(Existe).RecordCount < 1)
                {
                    this.tServicioData.oTipoServicio.TipoServicioID = null;
                    this.tServicioData.Inserta();
                    if (this.InsertarWS())
                    {
                        this.tServicioData.Sincronizado = 0;
                        this.tServicioData.ActualizarCode(this.Entity);
                    }
                    this.ClearFields();
                }
                else
                {
                    int num = tServicioData.Consultar(Existe).RecordCount;
                    throw new Exception("Error, entrada ya existe " + Convert.ToString(num) + " vez(veces)");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("SAP-Insertar: " + ex.Message);
            }
        }

        #endregion
        

        public void ClearFields()
        {
            try
            {
                this.oForm = this.SBO_Application.Forms.Item("TServicio");
                this.oItem = this.oForm.Items.Item("lblIDTS");
                oStaticText = (StaticText)oItem.Specific;
                oStaticText.Caption = "0";
                this.oItem = this.oForm.Items.Item("txtNombre");
                oEditText = (EditText)oItem.Specific;
                oEditText.String = string.Empty;
                this.oItem = this.oForm.Items.Item("txtDescrip");
                oEditText = (EditText)oItem.Specific;
                oEditText.String = string.Empty;
                this.Entity = new TipoServicio();
                this.Entity.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
                this.Entity.Propietario = new Kanan.Operaciones.BO2.Empresa();
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void FormToEntity()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("TServicio");

                this.oItem = this.oForm.Items.Item("lblIDTS");
                oStaticText = (StaticText)oItem.Specific;
                int id = Convert.ToInt32(oStaticText.Caption);
                if (id != 0)
                    this.Entity.TipoServicioID = id;
                oItem = oForm.Items.Item("txtNombre");
                oEditText = (EditText)oItem.Specific;
                this.Entity.Nombre = oEditText.String;

                oItem = oForm.Items.Item("txtDescrip");
                oEditText = (EditText)oItem.Specific;
                this.Entity.Descripcion = oEditText.String;
                this.Entity.Propietario.EmpresaID = this.Configs.UserFull.Dependencia.EmpresaID;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void DataEntityToForm()
        {
            try
            {
                if (this.Entity.TipoServicioID != null)
                    this.oForm.Title = "Actualizar un tipo de servicio";
                this.oForm = this.SBO_Application.Forms.Item("TServicio");
                this.oItem = this.oForm.Items.Item("lblIDTS");
                oStaticText = (StaticText)oItem.Specific;
                oStaticText.Caption = this.Entity.TipoServicioID.ToString();
                this.oItem = this.oForm.Items.Item("txtNombre");
                oEditText = (EditText)oItem.Specific;
                oEditText.String = this.Entity.Nombre;
                this.oItem = this.oForm.Items.Item("txtDescrip");
                oEditText = (EditText)oItem.Specific;
                oEditText.String = this.Entity.Descripcion;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public void InsertUpdate()
        {
            try
            {
                string isValid = this.ValidatesFields();
                if (!string.IsNullOrEmpty(isValid))
                    throw new Exception(isValid);
                oItem = oForm.Items.Item("lblIDTS");
                oStaticText = oItem.Specific;
                int id = Convert.ToInt32(oStaticText.Caption);
                this.FormToEntity();
                if (id == 0)
                {
                    this.Insertar();
                }
                else
                {
                    this.Actualizar();
                    this.SBO_Application.Forms.ActiveForm.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Registrar tipo servicio. InsertUpdate: " + ex.Message);
            }
        }

        public void Consultar(TipoServicio ts)
        {
            try
            {
                SAPbobsCOM.Recordset rs = this.tServicioData.Consultar(ts);
                if (this.tServicioData.HasTipoServicio(rs))
                    this.Entity = this.tServicioData.LastRecordSetToTipoServicio(rs);
                else
                    throw new Exception("No se encontro ningun registro");
            }
            catch(Exception ex)
            {
                this.SBO_Application.Forms.ActiveForm.Close();
                throw new Exception(ex.Message);
            }
        }

        public void HideDeleteButton()
        {
            try
            {
                this.oForm = this.SBO_Application.Forms.Item("TServicio");
                this.oItem = this.oForm.Items.Item("btnDelTs");
                oItem.Visible = false;
                this.oItem = this.oForm.Items.Item("btnCancTS");
                oItem.Left = 84;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void EliminarCompleto()
        {
            try
            {
                tServicioData.ItemCode = this.Entity.TipoServicioID.ToString();
                if(this.tServicioData.Existe())
                {
                    this.tServicioData.GetSyncUUID();
                    Kanan.Mantenimiento.BO2.TipoServicio ts = new Kanan.Mantenimiento.BO2.TipoServicio();
                    ts.TipoServicioID = this.Entity.TipoServicioID;
                    SAPbobsCOM.Recordset rs = this.tServicioData.Consultar(ts);
                    if (this.tServicioData.HasTipoServicio(rs))
                        ts = this.tServicioData.LastRecordSetToTipoServicio(rs);
                    ts.EsActivo = false;
                    this.tServicioData.oTipoServicio = ts;
                    this.tServicioData.Sincronizado = 3;
                    this.tServicioData.TipoServicioToUDT();
                    this.tServicioData.ActualizarCompleto();//.Eliminar();
                    if(this.EliminarWS())
                    {
                        this.tServicioData.Existe();
                        this.tServicioData.Sincronizado = 0;
                        this.tServicioData.Sincronizar();
                        this.tServicioData.Eliminar();
                    }
                    this.SBO_Application.StatusBar.SetText("¡Registro Eliminado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                }
            }
            catch (Exception ex) { throw new Exception("EliminarCompleto: " + ex.Message); }
        }

    }
}
