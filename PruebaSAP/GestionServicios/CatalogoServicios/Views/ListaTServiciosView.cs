﻿using KananWS.Interface;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.GestionServicios.CatalogoServicios.Views
{
    public class ListaTServiciosView
    {
        #region VARIABLES
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Matrix oMtx;
        private SAPbouiCOM.Conditions oConditions;
        private SAPbouiCOM.Condition oCondition;
        private Button oButton;
        private EditText oEditText;
        private XMLPerformance XmlApplication;
        private KananSAP.Mantenimiento.Data.TipoServicioData tsData;
        private SAPbouiCOM.DBDataSource oDBDataSource;
        public Kanan.Mantenimiento.BO2.TipoServicio oTipoServicio;
        public int? EmpresaID { get; set; }
        private SAPbouiCOM.DataTable oDataTable;
        private KananWS.Interface.ServiciosWS servicioWS;
        string sPath = System.Windows.Forms.Application.StartupPath;
        
        private Configurations Configs;
        public bool isUpdate { get; set; }
        public int? TServicioID { get; set; }
        private string Nombre { get; set; }
        #endregion

        #region CONSTRUCTOR
        public ListaTServiciosView(SAPbouiCOM.Application application, SAPbobsCOM.Company company, Configurations config)
        {
            this.SBO_Application = application;
            this.Company = company;
            this.XmlApplication = new XMLPerformance(this.SBO_Application);
            tsData = new KananSAP.Mantenimiento.Data.TipoServicioData(company);
            oItem = null;
            oEditText = null;
            oButton = null;
            oMtx = null;
            oTipoServicio = new Kanan.Mantenimiento.BO2.TipoServicio();
            oTipoServicio.Propietario = new Kanan.Operaciones.BO2.Empresa();
            oTipoServicio.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
            this.Configs = config;
            servicioWS = new KananWS.Interface.ServiciosWS(Guid.Parse(config.UserFull.Usuario.PublicKey), config.UserFull.Usuario.PrivateKey);
        }
        #endregion

        #region FORM

        public void LoadForm()
        {
            sPath = System.Windows.Forms.Application.StartupPath;
            try
            {
                this.oForm = this.SBO_Application.Forms.Item("LstTServ");
                this.SetConditions();
                this.BindData();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Invalid Form"))
                {
                   // string sPath = System.Windows.Forms.Application.StartupPath;
                   
                    this.XmlApplication.LoadFromXML(sPath + "\\XML",
                            "ListaTServicios.xml");
                    this.oForm = SBO_Application.Forms.Item("LstTServ");
                    this.SetConditions();
                    this.BindData();
                    //Establece la ventana centrada
                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;

                }
                else
                {
                    SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                }
            }

        }

        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
            }
            catch (Exception ex)
            {
                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                this.oForm.Select();
            }
        }

        #endregion

        public void BindData()
        {
            try
            {
                //oConditions = new SAPbouiCOM.Conditions();
                //oCondition = oConditions.Add();
                //oCondition.Alias = "U_EsActivo";
                //oCondition.Operation = BoConditionOperation.co_EQUAL;
                //oCondition.CondVal = "1";
                //oCondition.Relationship = BoConditionRelationship.cr_AND;
                //oCondition = oConditions.Add();
                //oCondition.Alias = "U_EmpresaID";
                //oCondition.Operation = BoConditionOperation.co_EQUAL;
                //oCondition.CondVal = this.Configs.UserFull.Dependencia.EmpresaID.ToString();

                oDBDataSource = oForm.DataSources.DBDataSources.Item("@VSKF_TIPOSERVICIO");
                oDBDataSource.Query(oConditions ?? null);
                oItem = oForm.Items.Item("mtxTServ");//mtxsrv
                oMtx = (SAPbouiCOM.Matrix)oItem.Specific;
                oMtx.LoadFromDataSource();
            }
            catch (Exception ex)
            {
                throw new Exception("BindDataToForm: " + ex.Message);
            }
        }

        public void SetConditions()
        {
            try
            {
                oItem = oForm.Items.Item("txtBusTSV");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                string buscando = oEditText.String.ToString().Trim();
                //oCondition = null;
                //oConditions = null;
                oConditions = new SAPbouiCOM.Conditions();
                oCondition = oConditions.Add();
                oCondition.Alias = "U_ESACTIVO";
                oCondition.Operation = BoConditionOperation.co_EQUAL;
                oCondition.CondVal = "1";
                //oCondition.Relationship = BoConditionRelationship.cr_AND;
                //oCondition = oConditions.Add();
                //oCondition.Alias = "U_EmpresaID";
                //oCondition.Operation = BoConditionOperation.co_EQUAL;
                //oCondition.CondVal = this.Configs.UserFull.Dependencia.EmpresaID.ToString();

                if (!string.IsNullOrEmpty(buscando))
                {
                    oCondition.Relationship = BoConditionRelationship.cr_AND;
                    oCondition = oConditions.Add();
                    oCondition.Alias = "U_NOMBRE";
                    oCondition.Operation = BoConditionOperation.co_CONTAIN;
                    oCondition.CondVal = buscando;
                    //oCondition.Relationship = BoConditionRelationship.cr_OR;

                    //oCondition = oConditions.Add();
                    //oCondition.Alias = "U_Descripcion";
                    //oCondition.Operation = BoConditionOperation.co_CONTAIN;
                    //oCondition.CondVal = buscando;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string GetDataRow(int index)
        {
            try
            {
                oItem = oForm.Items.Item("mtxTServ");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(1, index);
                return oEditText.String;
            }
            catch (Exception ex)
            {
                return "Error en la aplicacion";
            }
        }

        public string GetIDDataRow(int index)
        {
            try
            {
                oItem = oForm.Items.Item("mtxTServ");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(0, index);
                int i = oMtx.RowCount;
                if (index > i)
                    throw new Exception("Seleccione un elemento válido");
                if (string.IsNullOrEmpty(oEditText.String))
                    throw new Exception("Seleccione un elemento válido");
                return oEditText.String;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SetID(int index)
        {
            try
            {
                oItem = oForm.Items.Item("mtxTServ");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(0, index);
                this.oTipoServicio.TipoServicioID = Convert.ToInt32(oEditText.String.Trim());

                oItem = oForm.Items.Item("mtxTServ");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(1, index);
                this.oTipoServicio.Nombre = oEditText.String.ToString();

                oItem = oForm.Items.Item("mtxTServ");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(2, index);
                this.oTipoServicio.Descripcion = oEditText.String.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar()
        {
            try
            {
                int id = (int)this.TServicioID;
                this.tsData.ItemCode = id.ToString();
                if (this.tsData.Existe())
                {
                    this.tsData.GetSyncUUID();
                    Kanan.Mantenimiento.BO2.TipoServicio ts = new Kanan.Mantenimiento.BO2.TipoServicio();
                    ts.TipoServicioID = id;
                    SAPbobsCOM.Recordset rs = this.tsData.Consultar(ts);
                    if (this.tsData.HasTipoServicio(rs))
                        ts = this.tsData.LastRecordSetToTipoServicio(rs);
                    ts.EsActivo = false;
                    this.tsData.oTipoServicio = ts;
                    this.tsData.Sincronizado = 3;
                    this.tsData.TipoServicioToUDT();
                    this.tsData.ActualizarCompleto();//.Eliminar();
                    if(this.servicioWS.EliminaTipoServicio((int)id))
                    {
                        this.tsData.Existe();
                        this.tsData.Sincronizado = 0;
                        this.tsData.Sincronizar();
                        //Comentado debido a que la función eliminar se utiliza en Sincronizar() porque comparten la misma finción en ambos botones de
                        //ambos formularios..
                        //this.tsData.Eliminar();
                    }
                }
                this.SBO_Application.StatusBar.SetText("¡Registro Eliminado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                this.BindData();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void EliminarDesdeGestion()
        {
            tsData.Eliminar();
        }

       
        public bool HasRowValue(int rowIndex)
        {
            try
            {
                this.oForm = this.SBO_Application.Forms.Item("LstTServ");
                oMtx = oForm.Items.Item("mtxTServ").Specific;
                int rows = oMtx.RowCount;
                if (rowIndex <= 0 || rowIndex > rows)
                    return false;
                EditText txtNombre = (SAPbouiCOM.EditText)oMtx.GetCellSpecific("t.nombre", rowIndex);
                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific("#id", rowIndex);
                this.TServicioID = null;
                this.Nombre = null;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()) && !string.IsNullOrEmpty(txtNombre.String.Trim()))
                {
                    this.TServicioID = Convert.ToInt32(oEditText.String.Trim());
                    this.Nombre = txtNombre.String.Trim();
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

    }
}
