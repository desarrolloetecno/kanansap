﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KananSAP.Mantenimiento.Data;
using Kanan.Mantenimiento.BO2;
using Kanan.Vehiculos.BO2;
using KananWS.Interface;
using KananSAP.Helpers;

namespace KananSAP.GestionServicios.CatalogoServicios.Views
{
    public class ConfigurarServicioView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.StaticText oStaticText;
        //private SAPbouiCOM.Button oButton;
        private SAPbouiCOM.OptionBtn oRadioButton;
        private SAPbouiCOM.CheckBox oCheckBox;
        //private SAPbouiCOM.Conditions oConditions;
        //private SAPbouiCOM.Condition oCondition;
        public string FormUniqueID { get; set; }
        //public ConfigurarServicioSAP Entity { get; set; }
        private XMLPerformance XmlApplication;
        public ParametroMantenimiento Entity { get; set; }
        //private ParametroServicio oParametroServicio;
        public Servicio oServicio { get; set; }
        public Vehiculo oVehiculo { get; set; }
        public bool EsActivo { get; set; }
        public Caja oActivo { get; set; }
        public int TipoParametro { get; set; }
        public bool isTypeVehicule { get; set; }
        private ParametroMantenimientoHelperData pmtroMantenomientoHelper;
        private ServicioSAP servicioData;
        private ServiciosWS servicioWS;
        private Configurations Configs;
        private ComboBox oCombo { get; set; }
        private SAPbobsCOM.Company oCompany;
        #endregion

        #region Constructor

        public ConfigurarServicioView(SAPbouiCOM.Application Application, SAPbobsCOM.Company company, Configurations config)
        {
            this.oCompany = company;
            this.SBO_Application = Application;
            this.Entity = new ParametroMantenimiento();
            this.Entity.ParametroServicio = new ParametroServicio();
            this.Entity.ParametroServicio.Servicio = new Servicio();
            //this.Entity.ParametroServicio.Servicio.Propietario = new Kanan.Operaciones.BO2.Empresa();
            //this.Entity.ParametroServicio.Servicio.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
            this.XmlApplication = new XMLPerformance(this.SBO_Application);
            this.oItem = null;
            this.oEditText = null;
            this.oStaticText = null;
            //this.oButton = null;
            //this.oCondition = null;
            //this.oConditions = null;
            this.EsActivo = false;
            this.pmtroMantenomientoHelper = new ParametroMantenimientoHelperData(company);
            this.servicioData = new ServicioSAP(company);
            this.Configs = config;
            this.servicioWS = new ServiciosWS(Guid.Parse(config.UserFull.Usuario.PublicKey), config.UserFull.Usuario.PrivateKey);
        }

        #endregion

        #region Form
        /// <summary>
        /// Carga el formulario de configuración de servicios
        /// </summary>
        public void LoadForm()
        {
            try
            {
                this.oForm = this.SBO_Application.Forms.Item("ConfSvForm");
                this.FillComboServicio();
            }
            catch//(Exception ex)
            {
                //if(ex.Message.Contains("Invalid Form"))
                //{
                    string sPath = System.Windows.Forms.Application.StartupPath;
                     this.XmlApplication.LoadFromXML(sPath + "\\XML", "ConfigurarServicio2.xml");
                    this.oForm = SBO_Application.Forms.Item("ConfSvForm");
                    this.FillComboServicio();
                    //Establece la ventana centrada
                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                    
                //}
                //else
                //{
                    //SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                //}
            }
        }

        /// <summary>
        /// Muestra el formulario de configuración de servicios
        /// </summary>
        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
                this.FillComboServicio();
            }
            catch //(Exception ex)
            {
                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                this.oForm.Select();
            }
        }

        public void CloseForm()
        {
            if (this.oForm.Visible && this.oForm.Selected)
                this.oForm.Close();
        }

        public void FillComboServicio()
        {
            try
            {
                SAPbobsCOM.Recordset rs = servicioData.Consultar(new Servicio());
                if (servicioData.HashServicios(rs))
                {
                    if (oCombo != null && oCombo.ValidValues.Count > 0) return;
                    oCombo = this.oForm.Items.Item("cmbConServ").Specific;
                    KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("", "");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string id = Convert.ToString(rs.Fields.Item("U_SERVICIOID").Value);
                        string des = string.Format("{0} [{1}]", rs.Fields.Item("Servicio").Value, rs.Fields.Item("TipoServicio").Value);
                        oCombo.ValidValues.Add(id, des);
                        rs.MoveNext();
                    }
                    oCombo.Item.DisplayDesc = true;
                }

                oItem = this.oForm.Items.Item("cmbParam");
                oCombo = oItem.Specific;
                oCombo.Item.DisplayDesc = true;
            }
            catch //(Exception ex) 
            {

            }
        }

        #endregion

        #region Validaciones

        /// <summary>
        /// Valida que la informacion proporcionada de acuerdo al tipo de parametro sea correcto
        /// </summary>
        /// <returns></returns>
        public string ValidatesFields()
        {
            try
            {
                StringBuilder mnsErr = new StringBuilder();
                string err = string.Empty;
                this.oForm = this.SBO_Application.Forms.Item("ConfSvForm");
                oItem = this.oForm.Items.Item("cbxUPDTV");
                oCheckBox = (SAPbouiCOM.CheckBox)oItem.Specific;
                if (oCheckBox.Checked)
                    this.isTypeVehicule = true;
                else
                    this.isTypeVehicule = false;
                oCombo = this.oForm.Items.Item("cmbConServ").Specific;
                if (string.IsNullOrEmpty(oCombo.Value))
                    mnsErr.Append(", servicio");
                if (this.TipoParametro > 0)
                {
                    if (this.TipoParametro == 1 || this.TipoParametro == 3)
                    {
                        oItem = this.oForm.Items.Item("txtFrcSvDt");
                        oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                        if (string.IsNullOrEmpty(oEditText.String))
                            mnsErr.Append(", la frecuencia del servicio");

                        oItem = this.oForm.Items.Item("txtUltSvDt");
                        oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                        if (string.IsNullOrEmpty(oEditText.String))
                            mnsErr.Append(", el último servicio efectuado");

                        oItem = this.oForm.Items.Item("txtAltSvDt");
                        oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                        if (string.IsNullOrEmpty(oEditText.String))
                            mnsErr.Append(", la distancia en que se generara la alerta");

                        Entity.AlertaProximoServicio = Convert.ToDouble(oEditText.String);
                    }
                    if (this.TipoParametro == 2 || this.TipoParametro == 3)
                    {
                        oItem = this.oForm.Items.Item("txtUltSvTm");
                        oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                        if (string.IsNullOrEmpty(oEditText.String))
                            mnsErr.Append(", la fecha del último servicio");

                        oItem = this.oForm.Items.Item("txtPrxSvTm");
                        oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                        if (string.IsNullOrEmpty(oEditText.String))
                            mnsErr.Append(", la fecha del próximo servicio");

                        oItem = this.oForm.Items.Item("txtAltSvTm");
                        oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                        if (string.IsNullOrEmpty(oEditText.String))
                            mnsErr.Append(", los dias en que se generara la alerta");
                    }
                    if (mnsErr.Length > 0)
                    {
                        err = mnsErr.ToString();
                        if (err.StartsWith(","))
                            err = "Los siguientes campos son obligatorios:\n" + err.Substring(1);
                    }

                }
                else
                    err = "Seleccione un tipo de parámetro";
                return err;
            }
            catch (Exception ex)
            {
                throw new Exception("ConfigurarServicioView. ValidatesFields: " + ex.Message);
            }

        }
      
        /// <summary>
        /// Verifica si existe algun parámetro de mantenimiento configurado para el mantenible seleccionado
        /// </summary>
        /// <returns></returns>
        public bool VerificaExistenciaParametro()
        {
            try
            {
                bool existe = false;
                ParametroMantenimiento param = new ParametroMantenimiento();
                if (EsActivo == true)
                    param.Mantenible = this.oActivo;
                else
                    param.Mantenible = this.oVehiculo;
                param.ParametroServicio = new ParametroServicio();
                param.ParametroServicio.Servicio = this.oServicio;
                this.pmtroMantenomientoHelper.oParametroMantenimiento = param;
                existe = this.pmtroMantenomientoHelper.ExisteParametro();
                if (existe)
                {
                    this.Entity = this.pmtroMantenomientoHelper.ConsultarCompleto();
                    this.DataEntityToForm();
                }
                return existe;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Web Service
        
        /// <summary>
        /// Guarda la configuracion de parametros de mantenimiento y de servicio
        /// </summary>
        private void InsertarCompletoWS()
        {
            try
            {
                ParametroMantenimiento pmTmp = this.servicioWS.InsertarCompleto(this.Entity);
                if (pmTmp == null)
                    throw new Exception("Es posible que la información no se haya guardado correctamente");
                if (pmTmp.ParametroMantenimientoID == null)
                    throw new Exception("Es posible que la información no se haya guardado correctamente");
                if (pmTmp.ParametroServicio.ParametroServicioID == null)
                    throw new Exception("Es posible que la información no se haya guardado correctamente");
                this.FormToEntity();
                this.Entity.ParametroMantenimientoID = pmTmp.ParametroMantenimientoID;
                this.Entity.ParametroServicio.ParametroServicioID = pmTmp.ParametroServicio.ParametroServicioID;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Actualiza el parametro de mantenimiento
        /// </summary>
        private void ActualizaParametroMantenimientoWS()
        {
            try
            {
                ParametroMantenimiento pmTmp = this.servicioWS.ActualizaPametroMantenimiento(this.Entity);
                if (pmTmp == null)
                    throw new Exception("Es posible que la información no se haya guardado correctamente");
                if (pmTmp.ParametroMantenimientoID == null)
                    throw new Exception("Es posible que la información no se haya guardado correctamente");
                this.FormToEntity();
                if(this.Entity.ParametroMantenimientoID == null)
                    this.Entity.ParametroMantenimientoID = pmTmp.ParametroMantenimientoID;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Guarda o actualiza los parametros de mantenimiento y servicio
        /// </summary>
        private void InsertaOActualizaParametrosWS()
        {
            try
            {
                ParametroMantenimiento pmTmp = this.servicioWS.InsertaOActualizarParametro(this.Entity);
                if (pmTmp == null)
                    throw new Exception("Es posible que la información no se haya guardado correctamente");
                if (pmTmp.ParametroMantenimientoID == null)
                    throw new Exception("Es posible que la información no se haya guardado correctamente");
                if (pmTmp.ParametroServicio.ParametroServicioID == null)
                    throw new Exception("Es posible que la información no se haya guardado correctamente");
                this.FormToEntity();
                if(this.Entity.ParametroMantenimientoID == null)
                    this.Entity.ParametroMantenimientoID = pmTmp.ParametroMantenimientoID;
                if(this.Entity.ParametroServicio.ParametroServicioID == null)
                    this.Entity.ParametroServicio.ParametroServicioID = pmTmp.ParametroServicio.ParametroServicioID;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Actions

        /// <summary>
        /// Establece la informacion del vehiculo y servicio a configurar
        /// </summary>
        public void SetServiceVehicleData()
        {
            try
            {
                #region Estableciendo valores del vehículo, servicio y tipo de servicio

                if (this.oServicio != null)
                {
                    SAPbobsCOM.Recordset rs = servicioData.Consultar(this.oServicio);
                    if (servicioData.HashServicios(rs))
                        this.oServicio = servicioData.LastRecordSetToServicio(rs);
                }

                if (EsActivo == true)
                {
                    //Número económico
                    if (this.oActivo.NumeroEconomico != null)
                    {
                        oItem = this.oForm.Items.Item("lblNEconoV");
                        oStaticText = (SAPbouiCOM.StaticText)oItem.Specific;
                        oStaticText.Caption = this.oActivo.NumeroEconomico;
                    }
                }
                else
                {
                    //Número económico
                    if (this.oVehiculo.Nombre != null)
                    {
                        oItem = this.oForm.Items.Item("lblNEconoV");
                        oStaticText = (SAPbouiCOM.StaticText)oItem.Specific;
                        oStaticText.Caption = this.oVehiculo.Nombre;
                    }
                }
                //Servicio
                oCombo = this.oForm.Items.Item("cmbConServ").Specific;
                if (this.oServicio != null && this.oServicio.ServicioID.HasValue)
                {
                    oCombo.Select(this.oServicio.ServicioID.ToString(), BoSearchKey.psk_ByDescription);
                }
                else
                {
                    oCombo.Select("", BoSearchKey.psk_ByValue);
                }
                //if (this.oServicio.Nombre !=null)
                //{
                //    oItem = this.oForm.Items.Item("lblServV");
                //    oStaticText = (SAPbouiCOM.StaticText)oItem.Specific;
                //    oStaticText.Caption = this.oServicio.Nombre;
                //}

                //Tipo de Servicio
                //if (this.oServicio.TipoServicio.Nombre != null)
                //{
                //    oItem = this.oForm.Items.Item("lblTSerV");
                //    oStaticText = (SAPbouiCOM.StaticText)oItem.Specific;
                //    oStaticText.Caption = this.oServicio.TipoServicio.Nombre;
                //}

                ////Descripcion
                //if (this.oServicio.Descripcion != null)
                //{
                //    oItem = this.oForm.Items.Item("lblDescV");
                //    oStaticText = (SAPbouiCOM.StaticText)oItem.Specific;
                //    oStaticText.Caption = this.oServicio.Descripcion;
                //}

                #endregion
                this.SetOptionButtonGroup();
            }
            catch
            {
                return;
            }
        }
        /// <summary>
        /// Asigna los grupos a los radiobuttons
        /// </summary>
        private void SetOptionButtonGroup()
        {
            try
            {
                ///Parámetro por distancia
                OptionBtn op = this.oForm.Items.Item("rbtPmtroDt").Specific as OptionBtn;

                ///Parámetro por tiempo
                op = this.oForm.Items.Item("rbtPmtroTm").Specific as OptionBtn;
                op.GroupWith("rbtPmtroDt");

                ///Parametro por Horas
                op = this.oForm.Items.Item("rbtPmtroHr").Specific as OptionBtn;
                op.GroupWith("rbtPmtroDt");

                ///Parámetro distancia y tiempo
                op = this.oForm.Items.Item("rbtPmtroAm").Specific as OptionBtn;
                op.GroupWith("rbtPmtroDt");

                EditText ed = this.oForm.Items.Item("txtUltSvTm").Specific as EditText;
                ed.DataBind.SetBound(true, "", "UD_Dates");
                ed = this.oForm.Items.Item("txtPrxSvTm").Specific as EditText;
                ed.DataBind.SetBound(true, "", "UD_Dates");
                ed = this.oForm.Items.Item("txtAltSvTm").Specific as EditText;
                ed.DataBind.SetBound(true, "", "UD_Spin");
            }
            catch
            {
                return;
            }
        }

        /// <summary>
        /// Obtiene los datos capturados por el usuario
        /// </summary>
        public void FormToEntity()
        {
            try
            {
                this.oForm = this.SBO_Application.Forms.Item("ConfSvForm");

                oItem = this.oForm.Items.Item("lblIDPM");
                oStaticText = (SAPbouiCOM.StaticText)oItem.Specific;
                int id = Convert.ToInt32(oStaticText.Caption);
                if (id != 0)
                    this.Entity.ParametroMantenimientoID = id;
                else
                    this.oServicio = new Servicio();

                oItem = this.oForm.Items.Item("cbxUPDTV");
                oCheckBox = (SAPbouiCOM.CheckBox)oItem.Specific;
                if (oCheckBox.Checked)
                    this.isTypeVehicule = true;
                else
                    this.isTypeVehicule = false;

                if (this.TipoParametro > 0)
                {
                    this.Entity.TipoParametro = this.TipoParametro;
                    if (EsActivo == true)
                    {
                        this.Entity.Mantenible = new Caja();
                        this.Entity.Mantenible.MantenibleID = this.oActivo.CajaID;
                        this.Entity.ParametroServicio.Mantenible = new Caja();
                        this.Entity.ParametroServicio.Mantenible.MantenibleID = this.oActivo.CajaID;
                    }
                    else
                    {
                        this.Entity.Mantenible = new Vehiculo();
                        this.Entity.Mantenible.MantenibleID = this.oVehiculo.VehiculoID;
                        this.Entity.ParametroServicio.Mantenible = new Vehiculo();
                        this.Entity.ParametroServicio.Mantenible.MantenibleID = this.oVehiculo.VehiculoID;
                    }
                    int alertaTiempo = 0;
                    if (this.TipoParametro == 1 || this.TipoParametro == 3)
                    {
                        oItem = this.oForm.Items.Item("txtFrcSvDt");
                        oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                        Entity.ParametroServicio.Valor = Convert.ToDouble(oEditText.String);
                        oItem = this.oForm.Items.Item("txtUltSvDt");
                        oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                        Entity.UltimoServicio = Convert.ToDouble(oEditText.String);
                        oItem = this.oForm.Items.Item("txtPrxSvDt");
                        oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                        Entity.ProximoServicio = Convert.ToDouble(oEditText.String);
                        oItem = this.oForm.Items.Item("txtAltSvDt");
                        oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                        Entity.ParametroServicio.Alerta = Convert.ToDouble(oEditText.String);
                        //Entity.AlertaProximoServicio = Convert.ToDouble(oEditText.String);
                    }
                    if (this.TipoParametro == 2 || this.TipoParametro == 3)
                    {
                        oItem = this.oForm.Items.Item("txtUltSvTm");
                        oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                        Entity.UltimoServicioTiempo = Convert.ToDateTime(oEditText.String);

                        oItem = this.oForm.Items.Item("txtPrxSvTm");
                        oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                        Entity.ProximoServicioTiempo = Convert.ToDateTime(oEditText.String);

                        oItem = this.oForm.Items.Item("txtAltSvTm");
                        oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                        alertaTiempo = Convert.ToInt32(oEditText.String);
                    }
                    if (this.Entity.ProximoServicioTiempo != null && this.Entity.UltimoServicioTiempo != null)
                    {
                        if (this.Entity.ParametroServicio.Mantenible.GetType() == typeof(Vehiculo) || this.Entity.ParametroServicio.Mantenible.GetType() == typeof(Caja))
                        {
                            this.Entity.ParametroServicio.AlertaTiempo = alertaTiempo;
                            TimeSpan res = (TimeSpan)(this.Entity.ProximoServicioTiempo - this.Entity.UltimoServicioTiempo);
                            this.Entity.ParametroServicio.ValorTiempo = res.Days;
                        }
                        double alertaTiempoTemp = (double)alertaTiempo;
                        this.Entity.AlertaProximoServicioTiempo = this.Entity.ProximoServicioTiempo.Value.AddDays(-alertaTiempoTemp);
                    }
                    if (this.Entity.ParametroServicio.Valor == null)
                        this.Entity.ParametroServicio.Valor = 0;
                    oCombo = this.oForm.Items.Item("cmbConServ").Specific;
                    this.oServicio.ServicioID = Convert.ToInt32(oCombo.Value);
                    this.Entity.ParametroServicio.Servicio = this.oServicio;
                    if (TipoParametro == 4 || this.TipoParametro == 3)
                    {
                        oItem = this.oForm.Items.Item("FrecServHr");
                        oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                        double FrecServHr = Convert.ToDouble(oEditText.String);
                        this.Entity.ValorHora = FrecServHr;
                        this.Entity.ParametroServicio.ValorHora = FrecServHr;

                        oItem = this.oForm.Items.Item("UltServHr");
                        oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                        double UltServHr = Convert.ToDouble(oEditText.String);
                        this.Entity.UltimoServicioHora = UltServHr;


                        oItem = this.oForm.Items.Item("AlServHr");
                        oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                        double AlServHr = Convert.ToDouble(oEditText.String);
                        this.Entity.AlertaHora = AlServHr;
                        this.Entity.ParametroServicio.AlertaHora = AlServHr;

                        oItem = this.oForm.Items.Item("ProxServHr");
                        oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                        double ProxServHr = Convert.ToDouble(oEditText.String);
                        this.Entity.ProximoServicioHora = ProxServHr;
                        //-Alerta Proximo Serv Hora
                        this.Entity.AlertaProximoServicioHora = ProxServHr - AlServHr;

                        

                        ///---Alora
                       // this.Entity.AlertaHora = 

                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception("ConfigurarServicioView. FormToEntity: " + ex.Message);
            }
        }

        /// <summary>
        /// Establece el o los campos vacios o con datos de configuracion del vehiculo
        /// </summary>
        public void DataEntityToForm()
        {
            try
            {
                if (this.Entity != null)
                {
                    DateTime defaultTime = Convert.ToDateTime("01/01/1990");
                    OptionBtn rbtn = (OptionBtn)this.oForm.Items.Item("rbtPmtroDt").Specific;

                    if (this.Entity.TipoParametro != null)
                    {
                        if (this.Entity.TipoParametro == 1)
                        {
                            rbtn = null;
                            rbtn = (OptionBtn)this.oForm.Items.Item("rbtPmtroDt").Specific;
                            rbtn.Selected = true;
                            this.ClearTimeParameters();
                        }

                        else if (this.Entity.TipoParametro == 2)
                        {
                            rbtn = (OptionBtn)this.oForm.Items.Item("rbtPmtroTm").Specific;
                            rbtn.Selected = true;
                            this.ClearDistanceParameters();
                        }
                        else if (this.Entity.TipoParametro == 3)
                        {
                            rbtn = (OptionBtn)this.oForm.Items.Item("rbtPmtroAm").Specific;
                            rbtn.Selected = true;
                        }
                        else if (this.Entity.TipoParametro == 4)
                        {
                            rbtn = (OptionBtn)this.oForm.Items.Item("rbtPmtroHr").Specific;
                            rbtn.Selected = true;
                        }
                    }

                    if (this.Entity.ParametroMantenimientoID != null)
                    {
                        oItem = this.oForm.Items.Item("lblIDPM");
                        oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                        oStaticText.Caption = this.Entity.ParametroMantenimientoID.ToString();
                    }

                    #region Parámetros de distancia
                    //Frecuencia de servicio
                    if (this.Entity.ParametroServicio.Valor != null)
                    {
                        oItem = this.oForm.Items.Item("txtFrcSvDt");
                        oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                        oEditText.String = this.Entity.ParametroServicio.Valor.ToString();
                    }

                    //Último servicio efectuado
                    if (this.Entity.UltimoServicio != null)
                    {
                        oItem = this.oForm.Items.Item("txtUltSvDt");
                        oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                        oEditText.String = this.Entity.UltimoServicio.ToString();
                    }
                    //Proximo servicio
                    if (this.Entity.ProximoServicio != null)
                    {
                        oItem = this.oForm.Items.Item("txtPrxSvDt");
                        oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                        oEditText.String = this.Entity.ProximoServicio.ToString();
                    }
                    //Alerta proximo servicio
                    if (this.Entity.ParametroServicio.Alerta != null)
                    {
                        oItem = this.oForm.Items.Item("txtAltSvDt");
                        oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                        oEditText.String = this.Entity.ParametroServicio.Alerta.ToString();
                    }

                    #endregion

                    #region Parámetros de tiempo
                    //Ultimo servicio efectuado
                    oItem = this.oForm.Items.Item("txtUltSvTm");
                    oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                    if (this.Entity.UltimoServicioTiempo != null && this.Entity.UltimoServicioTiempo > defaultTime)
                    {
                        string dateu = Convert.ToDateTime(this.Entity.UltimoServicioTiempo).ToShortDateString();
                        oEditText.String = dateu;
                    }
                    else
                        oEditText.String = string.Empty;
                    //Proximo serivio
                    oItem = this.oForm.Items.Item("txtPrxSvTm");
                    oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                    if (this.Entity.ProximoServicioTiempo != null && this.Entity.ProximoServicioTiempo > defaultTime)
                    {
                        string datep = Convert.ToDateTime(this.Entity.ProximoServicioTiempo).ToShortDateString();
                        oEditText.String = datep;
                    }
                    else
                        oEditText.String = string.Empty;
                    //Alerta proximo servicio
                    oItem = this.oForm.Items.Item("txtAltSvTm");
                    oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                    if (this.Entity.ParametroServicio.AlertaTiempo != null)
                        oEditText.String = this.Entity.ParametroServicio.AlertaTiempo.ToString();
                    else
                    {
                        TimeSpan alertaTiempo = (TimeSpan)(this.Entity.ProximoServicioTiempo - this.Entity.AlertaProximoServicioTiempo);
                        if (alertaTiempo.Days > 0)
                            oEditText.String = alertaTiempo.Days.ToString();
                        else
                            oEditText.String = string.Empty;
                    }

                    #endregion

                    #region Parámetros de Hora
                    //Frecuencia de servicio --- if (this.Entity.ParametroServicio.ValorHora != null)
                    if (this.Entity.ValorHora != null)
                    {
                        oItem = this.oForm.Items.Item("FrecServHr");
                        oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                        oEditText.String = this.Entity.ValorHora.ToString();
                    }

                    //Último servicio efectuado
                    if (this.Entity.UltimoServicioHora != null)
                    {
                        oItem = this.oForm.Items.Item("UltServHr");
                        oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                        oEditText.String = this.Entity.UltimoServicioHora.ToString();
                    }
                    //Proximo servicio
                    if (this.Entity.ProximoServicioHora != null)
                    {
                        oItem = this.oForm.Items.Item("ProxServHr");
                        oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                        oEditText.String = this.Entity.ProximoServicioHora.ToString();
                    }
                    //Alerta proximo servicio --- if (this.Entity.ParametroServicio.AlertaHora != null)
                    if (this.Entity.AlertaHora != null)
                    {
                        oItem = this.oForm.Items.Item("AlServHr");
                        oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                        oEditText.String = this.Entity.AlertaHora.ToString();
                    }

                    #endregion

                    if (this.Entity.Mantenible != null)
                    {
                        if (this.Entity.Mantenible.GetType() == typeof(TipoVehiculo))
                        {
                            CheckBox cbx = (CheckBox)this.oForm.Items.Item("cbxUPDTV").Specific;
                            cbx.Checked = true;
                        }
                    }
                }
                else
                {
                    this.ClearDistanceParameters();
                    this.ClearTimeParameters();
                    #region Parámetros de distancia
                    //Frecuencia de servicio
                    //oItem = this.oForm.Items.Item("txtFrcSvDt");
                    //oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                    //oEditText.String = string.Empty;
                    ////Último servicio efectuado
                    //oItem = this.oForm.Items.Item("txtUltSvDt");
                    //oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                    //oEditText.String = string.Empty;
                    ////Proximo servicio
                    //oItem = this.oForm.Items.Item("txtPrxSvDt");
                    //oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                    //oEditText.String = string.Empty;
                    ////Alerta proximo servicio
                    //oItem = this.oForm.Items.Item("txtAltSvDt");
                    //oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                    //oEditText.String = string.Empty;
                    #endregion

                    #region Parámetros de tiempo
                    ////Ultimo servicio efectuado
                    //oItem = this.oForm.Items.Item("txtUltSvTm");
                    //oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                    //oEditText.String = string.Empty;
                    ////Proximo serivio
                    //oItem = this.oForm.Items.Item("txtPrxSvTm");
                    //oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                    //oEditText.String = string.Empty;
                    ////Alerta proximo servicio
                    //oItem = this.oForm.Items.Item("txtAltSvTm");
                    //oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                    //oEditText.String = string.Empty;
                    #endregion
                }
            }
            catch
            {
                return;
            }
        }

        public void ClearDistanceParameters()
        {
            try
            {
                #region Parámetros de distancia
                //Alerta proximo servicio
                oItem = this.oForm.Items.Item("txtAltSvDt");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                oEditText.String = string.Empty;
                //Proximo servicio
                oItem = this.oForm.Items.Item("txtPrxSvDt");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                oEditText.String = string.Empty;
                //Último servicio efectuado
                oItem = this.oForm.Items.Item("txtUltSvDt");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = string.Empty;
                //Frecuencia de servicio
                oItem = this.oForm.Items.Item("txtFrcSvDt");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = string.Empty;
                #endregion
            }
            catch
            {
                return;
            }
        }

        public void ClearHoursParameters()
        {
            try
            {
                #region Parámetros de distancia
                //Alerta proximo servicio
                oItem = this.oForm.Items.Item("FrecServHr");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                oEditText.String = string.Empty;
                //Proximo servicio
                oItem = this.oForm.Items.Item("UltServHr");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                oEditText.String = string.Empty;
                //Último servicio efectuado
                oItem = this.oForm.Items.Item("ProxServHr");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = string.Empty;
                //Frecuencia de servicio
                oItem = this.oForm.Items.Item("AlServHr");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = string.Empty;
                #endregion
            }
            catch
            {
                return;
            }
        }
        public void ClearTimeParameters()
        {
            #region Parámetros de tiempo
            //Alerta proximo servicio
            oItem = this.oForm.Items.Item("txtAltSvTm");
            oEditText = (SAPbouiCOM.EditText)oItem.Specific;
            oEditText.String = string.Empty;
            //Proximo serivio
            oItem = this.oForm.Items.Item("txtPrxSvTm");
            oEditText = (SAPbouiCOM.EditText)oItem.Specific;
            oEditText.String = string.Empty;
            //Ultimo servicio efectuado
            oItem = this.oForm.Items.Item("txtUltSvTm");
            oEditText = (SAPbouiCOM.EditText)oItem.Specific;
            oEditText.String = string.Empty;
            #endregion
        }

        //public void ClearRadioButton()
        //{
        //    try
        //    {
        //        /*Parámetros de distancia*/
        //        OptionBtn rbtnDT = (OptionBtn)this.oForm.Items.Item("rbtPmtroDt").Specific;
        //        rbtnDT.Selected = false;

        //        /*Parámetros de tiempo*/
        //        OptionBtn rbtnTM = (OptionBtn)this.oForm.Items.Item("rbtPmtroTm").Specific;
        //        rbtnTM.Selected = false;

        //        /*Parámetro de horas*/
        //        OptionBtn rbtnHR = (OptionBtn)this.oForm.Items.Item("rbtPmtroHr").Specific;
        //        rbtnHR.Selected = false;

        //        /*Todos los parámetros*/
        //        OptionBtn rbtnAM = (OptionBtn)this.oForm.Items.Item("rbtPmtroAm").Specific;
        //        rbtnAM.Selected = false;
        //    }
        //    catch (Exception ex)
        //    {
        //        SBO_Application.SetStatusBarMessage("Error al limpiar selección de tipo de parámetros. ", BoMessageTime.bmt_Short, false);
        //        return;
        //    }
        //}
        
        public void ClearAllFields()
        {
            oItem = this.oForm.Items.Item("lblIDPM");
            oStaticText = (SAPbouiCOM.StaticText)oItem.Specific;
            oStaticText.Caption = "0";
            this.ClearTimeParameters();
            this.ClearDistanceParameters();
            this.ClearHoursParameters();
            oCombo = this.oForm.Items.Item("cmbConServ").Specific;
            oCombo.Select("", BoSearchKey.psk_ByValue);
        }

        public void PrepareNewEntity()
        {
            this.Entity = new ParametroMantenimiento();
            this.Entity.ParametroServicio = new ParametroServicio();
            this.Entity.ParametroServicio.Servicio = new Servicio();
        }

        public void DisableTimePamameters()
        {
            try
            {
                ///Control parametro distancia
                oItem = this.oForm.Items.Item("txtFrcSvDt");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.Item.Click(BoCellClickType.ct_Regular);
                oItem.Enabled = true; ;
                ///Controles parametro de tiempo
                oItem = this.oForm.Items.Item("txtAltSvTm");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                oEditText.Active = false;
                oItem.Enabled = false;

                oItem = this.oForm.Items.Item("txtPrxSvTm");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                oEditText.Active = false;
                oItem.Enabled = false;

                oItem = this.oForm.Items.Item("txtUltSvTm");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                oEditText.Active = false;
                oItem.Enabled = false;
                
            }
            catch (Exception ex) 
            { 
                throw new Exception(ex.Message);
            }
        }

        public void EnableTimePamameters()
        {
            try
            {
                oItem = this.oForm.Items.Item("txtAltSvTm");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                oItem.Enabled = true;

                oItem = this.oForm.Items.Item("txtPrxSvTm");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                oItem.Enabled = true;

                oItem = this.oForm.Items.Item("txtUltSvTm");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                oItem.Enabled = true;

            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void EnableHoursPamameters()
        {
            try
            {
                
                oItem = this.oForm.Items.Item("FrecServHr");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                oItem.Enabled = true;

                oItem = this.oForm.Items.Item("UltServHr");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                oItem.Enabled = true;

                oItem = this.oForm.Items.Item("ProxServHr");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                oItem.Enabled = true;

                oItem = this.oForm.Items.Item("AlServHr");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                oItem.Enabled = true;

                //oRadioButton = (OptionBtn)this.oForm.Items.Item("rbtPmtroHr").Specific;
                //oRadioButton.Selected = true;


            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void DisableHoursPamameters()
        {
            try
            {
                oItem = this.oForm.Items.Item("FrecServHr");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                oItem.Enabled = false;

                oItem = this.oForm.Items.Item("UltServHr");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                oItem.Enabled = false;

                oItem = this.oForm.Items.Item("ProxServHr");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                oItem.Enabled = false;

                oItem = this.oForm.Items.Item("AlServHr");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                oItem.Enabled = false;

                //oRadioButton = (OptionBtn)this.oForm.Items.Item("rbtPmtroHr").Specific;
                //oRadioButton.Selected = false;

            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void DisableDistancePamameters()
        {
            try
            {
                ///Control parametro por tiempo
                oItem = this.oForm.Items.Item("txtUltSvTm");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                oEditText.Item.Click(BoCellClickType.ct_Regular);
                oItem.Enabled = true;
                ///Controles parametro distancia
                oItem = this.oForm.Items.Item("txtAltSvDt");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                oEditText.Active = false;
                oItem.Enabled = false;

                oItem = this.oForm.Items.Item("txtPrxSvDt");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                oEditText.Active = false;
                oItem.Enabled = false;

                oItem = this.oForm.Items.Item("txtUltSvDt");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.Active = false;
                oItem.Enabled = false;

                oItem = this.oForm.Items.Item("txtFrcSvDt");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.Active = false;
                oItem.Enabled = false;
            }
            catch (Exception ex) { return; }//throw new Exception(ex.Message); }
        }

        public void EnableDistancePamameters()
        {
            try
            {
                oItem = this.oForm.Items.Item("txtAltSvDt");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                oItem.Enabled = true;

                oItem = this.oForm.Items.Item("txtPrxSvDt");
                oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                oItem.Enabled = true;

                oItem = this.oForm.Items.Item("txtUltSvDt");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oItem.Enabled = true;

                oItem = this.oForm.Items.Item("txtFrcSvDt");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oItem.Enabled = true;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        /// <summary>
        /// Guarda o actualizar las configuraciones de los parametros de mantenimiento y
        /// servicio establecidos por el usuario
        /// </summary>
        public void InsertOrUpdateParameters()
        {
            try
            {
                string isValid = this.ValidatesFields();
                if (!string.IsNullOrEmpty(isValid))
                {
                    this.SBO_Application.MessageBox(isValid, 1, "Ok", "", "");
                    return;
                }
                else
                {
                    oItem = this.oForm.Items.Item("lblIDPM");
                    oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                    int id = Convert.ToInt32(oStaticText.Caption);
                    this.FormToEntity();
                    if (this.isTypeVehicule)
                    {
                        if (id != 0)
                        {
                            this.ActualizaParametroMantenimientoWS();
                            this.pmtroMantenomientoHelper.oParametroMantenimiento = this.Entity;
                            this.pmtroMantenomientoHelper.ActualizarParametroMantenimiento();
                            this.ClearAllFields();
                            this.SBO_Application.Forms.ActiveForm.Close();
                        }
                    }
                    else
                    {
                        if (id == 0)
                        {
                            this.InsertarCompletoWS();
                            this.pmtroMantenomientoHelper.oParametroMantenimiento = this.Entity;
                            this.pmtroMantenomientoHelper.InsertaCompleto();
                            this.oServicio = new Servicio();
                            this.ClearAllFields();
                            //this.SBO_Application.Forms.ActiveForm.Close();
                        }
                        else
                        {
                            this.InsertaOActualizaParametrosWS();
                            this.pmtroMantenomientoHelper.oParametroMantenimiento = this.Entity;
                            this.pmtroMantenomientoHelper.InsertaOActualizarParametros();
                            this.ClearAllFields();
                            this.SBO_Application.Forms.ActiveForm.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.oServicio = null;
                this.SBO_Application.Forms.ActiveForm.Close();
                throw new Exception(ex.Message);
                //this.SBO_Application.StatusBar.SetText("ConfigurarServicioPresenter.AddPMantenimiento: " + ex.Message, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                //this.SBO_Application.Forms.ActiveForm.Close();
            }
        }

        public void CalcularProximoServicio()
        {
            double frecServicio = 0, ultServicio = 0;
            oItem = this.oForm.Items.Item("txtFrcSvDt");
            oEditText = (EditText)oItem.Specific;
            string frec = oEditText.String;

            oItem = this.oForm.Items.Item("txtUltSvDt");
            oEditText = (EditText)oItem.Specific;
            string ult = oEditText.String;

            if (!string.IsNullOrEmpty(frec) && !string.IsNullOrEmpty(ult))
            {
                frecServicio = Convert.ToDouble(frec);
                ultServicio = Convert.ToDouble(ult);
                double prox = frecServicio + ultServicio;
                oItem = this.oForm.Items.Item("txtPrxSvDt");
                oEditText = (EditText)oItem.Specific;
                oEditText.String = prox.ToString();
            }
        }

        public void CalcularProximoServicioHora()
        {
            double frecServicio = 0, ultServicio = 0;
            oItem = this.oForm.Items.Item("FrecServHr");
            oEditText = (EditText)oItem.Specific;
            string frec = oEditText.String;

            oItem = this.oForm.Items.Item("UltServHr");
            oEditText = (EditText)oItem.Specific;
            string ult = oEditText.String;

            if (!string.IsNullOrEmpty(frec) && !string.IsNullOrEmpty(ult))
            {
                frecServicio = Convert.ToDouble(frec);
                ultServicio = Convert.ToDouble(ult);
                double prox = frecServicio + ultServicio;
                oItem = this.oForm.Items.Item("ProxServHr");
                oEditText = (EditText)oItem.Specific;
                oEditText.String = prox.ToString();
            }
        }

        public void ShowCheckBox()
        {
            this.oForm = this.SBO_Application.Forms.Item("ConfSvForm");
            this.oForm.Title = "Actualizar servicio";
            oItem = this.oForm.Items.Item("cbxUPDTV");
            oItem.Visible = true;
            oItem = this.oForm.Items.Item("btnAConfSv");
        }

        public void HideCheckBox()
        {
            this.oForm = this.SBO_Application.Forms.Item("ConfSvForm");
            oItem = this.oForm.Items.Item("cbxUPDTV");
            oItem.Visible = false;
        }

        public void HiddeDeleteButton()
        {
            try
            {
                oItem = oForm.Items.Item("btnDelPMS");
                oItem.Visible = false;
                oItem = oForm.Items.Item("btnCConfSv");
                oItem.Left = 783;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void DeterminaParametrizacion()
        {
            try
            {
                oItem = this.oForm.Items.Item("cmbParam");
                oCombo = (ComboBox)this.oItem.Specific;
                string stypeConfig = oCombo.Selected.Value;
                if (!string.IsNullOrEmpty(stypeConfig))
                {
                    oItem = this.oForm.Items.Item("lblServ");
                    oItem.Visible = false;
                    oItem = this.oForm.Items.Item("cmbRutina");
                    oItem.Visible = false;
                    oItem = this.oForm.Items.Item("cmbConServ");
                    oItem.Visible =false;
                    switch (stypeConfig)
                    {
                        case "1":
                            oItem = this.oForm.Items.Item("lblServ");
                            oItem.Visible = true;
                            oStaticText = oItem.Specific;
                            oStaticText.Caption = "Selecciona el servicio:";
                            oItem = this.oForm.Items.Item("cmbConServ");
                            oItem.Visible = true;
                            break;
                        case "2":
                            oItem = this.oForm.Items.Item("lblServ");
                            oItem.Visible = true;
                            oStaticText = oItem.Specific;
                            oStaticText.Caption = "Selecciona la rutina:";
                            oItem = this.oForm.Items.Item("cmbRutina");
                            oItem.Visible = true;
                            new GestionGlobales().FillCombo("Ninguno", "Code", "Name", "cmbRutina", this.sQueryPlantilla(), this.oForm, this.oCompany);
                            oItem = this.oForm.Items.Item("cmbRutina");
                            oCombo = oItem.Specific;
                            oCombo.Item.DisplayDesc = true;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        #endregion

        public String sQueryPlantilla(string sCode = "")
        {
            String strQuery = "";
            if (GestionGlobales.bSqlConnection)
                strQuery = "SELECT * FROM [@VSKF_OSPLANTILLA]";
            else
                strQuery = @"SELECT * FROM ""@VSKF_OSPLANTILLA"" ";

            if (!string.IsNullOrEmpty(sCode))
            {
                strQuery += string.Format(@" WHERE ""Code"" = '{0}' ", sCode);
            }
            return strQuery;
        }

    }
}
