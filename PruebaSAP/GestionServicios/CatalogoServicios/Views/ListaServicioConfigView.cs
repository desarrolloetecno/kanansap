﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KananSAP.Mantenimiento.Data;
using System.Windows.Forms;
using SAPbouiCOM;
using Kanan.Mantenimiento.BO2;
using Kanan.Vehiculos.BO2;
using KananWS.Interface;

namespace KananSAP.GestionServicios.CatalogoServicios.Views
{
    public class ListaServicioConfigView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.OptionBtn oOptionButton;
        private SAPbouiCOM.Button oBoton;
        private SAPbouiCOM.Matrix oMatrix;
        private SAPbouiCOM.DBDataSource oDBDataSource;
        private SAPbouiCOM.DataTable oDataTable;
        private SAPbouiCOM.Conditions oConditions;
        private SAPbouiCOM.Condition oCondition;
        public ServicioSAP Entity { get; set; }
        public string FormUniqueID { get; set; }
        private XMLPerformance XmlApplication;
        public Servicio oServicio { get; set; }
        public int? id;
        public string service_name;
        public Vehiculo oVehiculo { get; set; }
        public Caja oActivo { get; set; }
        public bool EsActivo { get; set; }
        public int? EmpresaID { get; set; }
        string sPath = System.Windows.Forms.Application.StartupPath;        
        private KananWS.Interface.ServiciosWS servicioWS;
        private Configurations Configs;
        public int? ServicioID { get; set; }
        private ParametroMantenimientoData pMantenimientoData;
        private ParametroServicioData pServicioData;
        /// <summary>
        /// Parámetro de MantenimientoID
        /// </summary>
        public int? PMantenimientoID { get; set; }
        /// <summary>
        /// Parámetro de ServicioID
        /// </summary>
        public int? PServicioID { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Contructor del view servicios
        /// </summary>
        /// <param name="Application">SAPbouiCOM.Application</param>
        /// <param name="company">SAPbobsCOM.Company</param>
        /// <param name="mantenibleID">MantenibleID</param>
        /// <param name="mantenibleName">Nombre del mantenible</param>
        public ListaServicioConfigView(SAPbouiCOM.Application Application, SAPbobsCOM.Company company, Configurations config)
        {
            this.oServicio = new Servicio();
            this.SBO_Application = Application;
            this.Entity = new ServicioSAP(company);
            this.Entity.oServicio.Propietario.EmpresaID = 1;
            this.XmlApplication = new XMLPerformance(SBO_Application);
            this.oItem = null;
            this.oEditText = null;
            this.oOptionButton = null;
            this.oBoton = null;
            this.oConditions = null;
            this.oCondition = null;
            this.id = null;
            this.service_name = null;
            this.Configs = config;
            this.oDataTable = null;
            this.servicioWS = new KananWS.Interface.ServiciosWS(Guid.Parse(config.UserFull.Usuario.PublicKey), config.UserFull.Usuario.PrivateKey);
            this.pMantenimientoData = new ParametroMantenimientoData(company);
            this.pServicioData = new ParametroServicioData(company);
            this.EsActivo = false;
        }
        #endregion

        #region Metodos

        #region Form
        /// <summary>
        /// Carga el formulario de la lista de servicios
        /// </summary>
        public void LoadForm()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("LstServ");
                //SetConditions();
                //BindDataToForm();
            }
            catch(Exception ex)
            {
                if(ex.Message.Contains("Invalid Form"))
                {
                   
                    this.XmlApplication.LoadFromXML(sPath + "\\XML", "ConfigurarServicioAVehiculo.xml");//"ListaServiciosConfig.xml");//
                    this.oForm = SBO_Application.Forms.Item("LstServ");
                    //SetConditions();
                    //BindDataToForm();
                    //Estabkece la ventana centrada
                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                }
                else
                {
                    SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                }
            }
        }
        /// <summary>
        /// Muestra el formulario que contiene la lista de servicios disponibles
        /// </summary>
        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
                //BindDataToForm();
            }
            catch(Exception ex)
            {
                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                //BindDataToForm();
                this.oForm.Select();
            }
        }

        public void CloseForm()
        {
            this.oForm = SBO_Application.Forms.Item("ServiceForm");
            this.oForm.Close();
        }

        /// <summary>
        /// Establece el o los campos vacios
        /// </summary>
        public void EmptyEntityToForm()
        {
            oItem = this.oForm.Items.Item("txtBsqSv");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;
            oItem = this.oForm.Items.Item("btnAddSv");
            oBoton = oItem.Specific;
            if (this.oVehiculo == null)
                oBoton.Caption = "Crear";
            if (this.EsActivo)
                if (this.oActivo == null)
                    oBoton.Caption = "Crear";
        }

        #endregion

        #region Matrix
        /// <summary>
        /// Obtiene la lista de servicios de la base de datos
        /// </summary>
        public void BindDataToForm()
        {
            try
            {
                oDBDataSource = oForm.DataSources.DBDataSources.Item("@VSKF_SERVICIOS");
                oDBDataSource.Query(oConditions ?? null);
                oItem = oForm.Items.Item("mtxServ");
                oMatrix = (SAPbouiCOM.Matrix)oItem.Specific;
                oMatrix.LoadFromDataSource();
                
            }
            catch(Exception ex)
            {
                throw new Exception("BindDataToForm: " + ex.Message);
            }
        }

        /// <summary>
        /// Obtiene una fila del control Matrix que contiene la lista de servicios
        /// </summary>
        /// <param name="index">Indice de la fila que se recuperara</param>
        /// <returns></returns>
        public string GetDataRow(int index)
        {
            try
            {
                oItem = oForm.Items.Item("mtxsrv");
                oMatrix = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMatrix.GetCellSpecific(1, index);
                return oEditText.String;
            }
            catch (Exception ex)
            {
                return "Error en la aplicacion";
            }
        }

        public string GetIDDataRow(int index)
        {
            try
            {
                oItem = oForm.Items.Item("mtxsrv");
                oMatrix = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMatrix.GetCellSpecific(0, index);
                int i = oMatrix.RowCount;
                if (index > i)
                    throw new Exception("Seleccione un elemento válido");
                if (!string.IsNullOrEmpty(oEditText.String))
                    throw new Exception("Seleccione un elemento válido");
                return oEditText.String;
            }
            catch (Exception ex)
            {
                return "Error en la aplicacion";
            }
        }

        #endregion

        #region Condiciones
        /// <summary>
        /// Establece el id del item selecionado en el control Matrix de la lista de servicios
        /// </summary>
        /// <param name="index">Indice del cual se obtendra el identificador</param>
        public void SetID(int index)
        {
            try
            {
                this.InitializeIDS();
                //this.oServicio = new Servicio();
                oItem = oForm.Items.Item("mtxServ");
                oMatrix = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMatrix.GetCellSpecific(6, index);
                //this.id = Convert.ToInt32(oEditText.String.Trim());
                this.oServicio.ServicioID = Convert.ToInt32(oEditText.String.Trim());
                this.ServicioID = Convert.ToInt32(oEditText.String.Trim());
                oItem = oForm.Items.Item("mtxServ");
                oMatrix = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMatrix.GetCellSpecific(1, index);
                this.oServicio.Nombre = oEditText.String.ToString();

                //oItem = oForm.Items.Item("mtxServ");
                //oMatrix = (SAPbouiCOM.Matrix)(oItem.Specific);
                //oEditText = (SAPbouiCOM.EditText)oMatrix.GetCellSpecific(2, index);
                ////this.service_name = oEditText.String.ToString();
                //this.oServicio.Descripcion = oEditText.String.ToString();

                oItem = oForm.Items.Item("mtxServ");
                oMatrix = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMatrix.GetCellSpecific(0, index);
                this.PMantenimientoID = Convert.ToInt32(oEditText.String.Trim());
                oItem = oForm.Items.Item("mtxServ");
                oMatrix = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMatrix.GetCellSpecific(7, index);
                this.PServicioID = Convert.ToInt32(oEditText.String.Trim());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Establece las condifiones de filtrado en el control Matrix de la lista de servicios
        /// </summary>
        public void SetConditions()
        {
            try
            {
                oItem = oForm.Items.Item("txtBsqSv");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Filtro = oEditText.String.ToString().Trim();
                //oConditions = new SAPbouiCOM.Conditions();
                //oConditions = new SAPbouiCOM.Conditions();
                //oCondition = oConditions.Add();
                //oCondition.Alias = "U_EsActivo";
                //oCondition.Operation = BoConditionOperation.co_EQUAL;
                //oCondition.CondVal = "1";
                
                //if (!string.IsNullOrEmpty(busqueda))
                //{
                //    oCondition.Relationship = BoConditionRelationship.cr_AND;
                //    oCondition = oConditions.Add();
                //    oCondition.Alias = "U_Nombre";
                //    oCondition.Operation = BoConditionOperation.co_CONTAIN;
                //    oCondition.CondVal = busqueda;//oEditText.String.Trim();
                //    oCondition.Relationship = BoConditionRelationship.cr_OR;
                //    oCondition = oConditions.Add();
                //    oCondition.Alias = "U_Descripcion";
                //    oCondition.Operation = BoConditionOperation.co_CONTAIN;
                //    oCondition.CondVal = busqueda;// oEditText.String.Trim();
                //}
                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        public void InitializeIDS()
        {
            this.ServicioID = null;
            this.PMantenimientoID = null;
            this.PServicioID = null;
        }

        public bool HasRowValue(int rowIndex)
        {
            try
            {
                this.oForm = this.SBO_Application.Forms.Item("LstServ");
                oMatrix = oForm.Items.Item("mtxServ").Specific;
                int rows = oMatrix.RowCount;
                if (rowIndex <= 0 || rowIndex > rows)
                    return false;
                oEditText = (SAPbouiCOM.EditText)oMatrix.GetCellSpecific("#", rowIndex);
                EditText txtServID = (SAPbouiCOM.EditText)oMatrix.GetCellSpecific("clServID", rowIndex);
                EditText txtPsID = (SAPbouiCOM.EditText)oMatrix.GetCellSpecific("clPsID", rowIndex);
                this.InitializeIDS();
                if (!string.IsNullOrEmpty(oEditText.String.Trim()) && !string.IsNullOrEmpty(txtServID.String.Trim()) && !string.IsNullOrEmpty(txtPsID.String.Trim()))
                {
                    this.PMantenimientoID = Convert.ToInt32(oEditText.String.Trim());
                    this.ServicioID = Convert.ToInt32(txtServID.String.Trim());
                    this.PServicioID = Convert.ToInt32(txtPsID.String.Trim());
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        private bool RetrieveForDelete(ParametroMantenimiento parametroMantenimiento)
        {
            try
            {
                bool resultado = false;
                ParametroMantenimiento tmp = new ParametroMantenimiento();
                tmp.ParametroServicio = parametroMantenimiento.ParametroServicio;
                SAPbobsCOM.Recordset rs = pMantenimientoData.Consultar(tmp);
                if(pMantenimientoData.HasParametroMantenimiento(rs))
                {
                    List<ParametroMantenimiento> list = pMantenimientoData.RecordSetToList(rs);
                    foreach(ParametroMantenimiento pm in list)
                    {
                        if (parametroMantenimiento.ParametroMantenimientoID != pm.ParametroMantenimientoID)
                            return true;
                    }
                } 
                return resultado;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        private void CreateTable()
        {
            try
            {
                //this.oDataTable = new DataTable();
                if(this.oForm.DataSources.DataTables.Count <= 0)
                    this.oForm.DataSources.DataTables.Add("tabPmtros");
                this.oForm.DataSources.DataTables.Item("tabPmtros").Clear();
                this.oForm.DataSources.DataTables.Item("tabPmtros").Columns.Add("#", BoFieldsType.ft_Integer, 10);
                this.oForm.DataSources.DataTables.Item("tabPmtros").Columns.Add("clServ", BoFieldsType.ft_AlphaNumeric, 254);
                this.oForm.DataSources.DataTables.Item("tabPmtros").Columns.Add("clPMDist", BoFieldsType.ft_AlphaNumeric, 254);
                this.oForm.DataSources.DataTables.Item("tabPmtros").Columns.Add("clProxSv", BoFieldsType.ft_AlphaNumeric, 254);
                this.oForm.DataSources.DataTables.Item("tabPmtros").Columns.Add("clPmTiem", BoFieldsType.ft_AlphaNumeric, 254);
                this.oForm.DataSources.DataTables.Item("tabPmtros").Columns.Add("clProxSvTi", BoFieldsType.ft_AlphaNumeric, 254);
                //---
                this.oForm.DataSources.DataTables.Item("tabPmtros").Columns.Add("PrmHr", BoFieldsType.ft_Integer, 10);
                this.oForm.DataSources.DataTables.Item("tabPmtros").Columns.Add("PrServHr", BoFieldsType.ft_Integer, 10);
                //---
                this.oForm.DataSources.DataTables.Item("tabPmtros").Columns.Add("clServID", BoFieldsType.ft_Integer, 10);
                this.oForm.DataSources.DataTables.Item("tabPmtros").Columns.Add("clPsID", BoFieldsType.ft_Integer, 10);
            }
            catch (Exception ex) { throw new Exception("CreateTable: " + ex.Message); }
        }

        public string Filtro { get; set; }

        public void ShowParametrosMantenimiento()
        {
            try
            {
                ParametroMantenimiento pm = new ParametroMantenimiento();

                if (EsActivo == true)
                {
                    pm.Mantenible = this.oActivo;
                    if (!String.IsNullOrEmpty(Filtro) & EsActivo == true)
                    {
                        pm.ParametroServicio = new ParametroServicio();
                        pm.ParametroServicio.Servicio = new Servicio();
                        pm.ParametroServicio.Servicio.Nombre = this.Filtro;
                    }
                }
                else
                {
                    pm.Mantenible = this.oVehiculo;
                    if (!string.IsNullOrEmpty(Filtro))
                    {
                        pm.ParametroServicio = new ParametroServicio();
                        pm.ParametroServicio.Servicio = new Servicio();
                        pm.ParametroServicio.Servicio.Nombre = this.Filtro;
                    }
                }

                SAPbobsCOM.Recordset rs = pMantenimientoData.Consultar(pm);
                this.Filtro = null;
                if(pMantenimientoData.HasParametroMantenimiento(rs))
                {
                    this.CreateTable();
                    oMatrix = this.oForm.Items.Item("mtxServ").Specific;
                    rs.MoveFirst();
                    for(int i = 0; i < rs.RecordCount; i++ )
                    {
                        bool isDefaultData = false;
                        DateTime proxSvTpo = Convert.ToDateTime(rs.Fields.Item("U_ProxSvTiem").Value);
                        if (proxSvTpo.CompareTo(Convert.ToDateTime("1990-01-01")) == -1 || proxSvTpo.CompareTo(Convert.ToDateTime("1990-01-01")) == 0)
                            isDefaultData = true;
                        this.oForm.DataSources.DataTables.Item("tabPmtros").Rows.Add();
                        this.oForm.DataSources.DataTables.Item("tabPmtros").SetValue("#", i, rs.Fields.Item("U_PMtoID").Value);
                        this.oForm.DataSources.DataTables.Item("tabPmtros").SetValue("clServ", i, rs.Fields.Item("Servicio").Value);
                        this.oForm.DataSources.DataTables.Item("tabPmtros").SetValue("clPMDist", i, rs.Fields.Item("Parametro").Value <= 0 ? string.Empty : FormatearDecimales(rs.Fields.Item("Parametro").Value.ToString()));
                        this.oForm.DataSources.DataTables.Item("tabPmtros").SetValue("clProxSv", i, rs.Fields.Item("Proximo").Value <= 0 ? string.Empty : FormatearDecimales(rs.Fields.Item("Proximo").Value.ToString()));
                        this.oForm.DataSources.DataTables.Item("tabPmtros").SetValue("clPmTiem", i, rs.Fields.Item("U_ValTiempo").Value <= 0 ? string.Empty : rs.Fields.Item("U_ValTiempo").Value);
                        //---
                        this.oForm.DataSources.DataTables.Item("tabPmtros").SetValue("PrmHr", i, rs.Fields.Item("U_ValHora").Value);

                        this.oForm.DataSources.DataTables.Item("tabPmtros").SetValue("PrServHr", i, rs.Fields.Item("U_ProxServHora").Value);
                        //----
                        if (isDefaultData)
                            this.oForm.DataSources.DataTables.Item("tabPmtros").SetValue("clProxSvTi", i, string.Empty);
                        else
                            this.oForm.DataSources.DataTables.Item("tabPmtros").SetValue("clProxSvTi", i, proxSvTpo.ToString("dd-MM-yyyy"));
                        this.oForm.DataSources.DataTables.Item("tabPmtros").SetValue("clServID", i, rs.Fields.Item("U_ServicioID").Value);
                        this.oForm.DataSources.DataTables.Item("tabPmtros").SetValue("clPsID", i, rs.Fields.Item("U_PServID").Value);
                        
                        rs.MoveNext();
                    }
                    oMatrix.Columns.Item("#").DataBind.Bind("tabPmtros", "#");
                    oMatrix.Columns.Item("clServ").DataBind.Bind("tabPmtros", "clServ");
                    oMatrix.Columns.Item("clPMDist").DataBind.Bind("tabPmtros", "clPMDist");
                    oMatrix.Columns.Item("clProxSv").DataBind.Bind("tabPmtros", "clProxSv");
                    oMatrix.Columns.Item("clPmTiem").DataBind.Bind("tabPmtros", "clPmTiem");
                    oMatrix.Columns.Item("clProxSvTi").DataBind.Bind("tabPmtros", "clProxSvTi");
                    oMatrix.Columns.Item("clServID").DataBind.Bind("tabPmtros", "clServID");
                    oMatrix.Columns.Item("clPsID").DataBind.Bind("tabPmtros", "clPsID");
                    //---
                    oMatrix.Columns.Item("PrmHr").DataBind.Bind("tabPmtros", "PrmHr");
                    oMatrix.Columns.Item("PrServHr").DataBind.Bind("tabPmtros", "PrServHr");
                    //---
                    oMatrix.LoadFromDataSource();
                    oMatrix.Columns.Item("clServID").Visible = false;
                    oMatrix.Columns.Item("clPsID").Visible = false;
                    
                }
                else
                {
                    oMatrix = this.oForm.Items.Item("mtxServ").Specific;
                    if (oMatrix.RowCount > 0)
                    {
                        oMatrix.DeleteRow(oMatrix.RowCount);
                    }
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        
        public bool EliminarParametros()
        {
            try
            {
                bool deleted = false;
                ParametroMantenimiento parametroMtto = new ParametroMantenimiento();
                parametroMtto.ParametroServicio = new ParametroServicio();
                parametroMtto.ParametroMantenimientoID = this.PMantenimientoID;
                parametroMtto.ParametroServicio.ParametroServicioID = this.PServicioID;
                if(this.PMantenimientoID != null && this.PServicioID != null)
                {
                    
                    SAPbobsCOM.Recordset rs = pServicioData.Consultar(parametroMtto.ParametroServicio);
                    if (pServicioData.HashParametroServicio(rs))
                        parametroMtto.ParametroServicio = pServicioData.LastRecordSetToParametroServicio(rs);
                    this.InitializeIDS();
                    if(this.RetrieveForDelete(parametroMtto))
                    {
                        //Se eliminan los parametros de mamtenimiento
                        this.pMantenimientoData.ItemCode = parametroMtto.ParametroMantenimientoID.ToString();
                        if(pMantenimientoData.Existe())
                        {
                            this.pMantenimientoData.Eliminar();
                            deleted = true;
                        }
                    }
                    else
                    {
                        if(parametroMtto.ParametroServicio.Mantenible.GetType() == typeof(Vehiculo))
                        {
                            //Se eliminan los parametros de mantenimiento y de servicio
                            pMantenimientoData.oParametroMantenimiento = parametroMtto;
                            pMantenimientoData.EliminarCompleto();
                            deleted = true;
                        }
                        else
                        {
                            pMantenimientoData.ItemCode = parametroMtto.ParametroMantenimientoID.ToString();
                            if (pMantenimientoData.Existe())
                            {
                                //Se elimina el parametro de mantenimiento
                                pMantenimientoData.Eliminar();
                                deleted = true;
                            }
                        }
                    }
                    if (!this.servicioWS.EliminarParametros(parametroMtto))
                        this.SBO_Application.SetStatusBarMessage("Es posible que los parámetros no se hayan eliminado correctamente");
                }
                return deleted;
            }
            catch (Exception ex) { this.ShowParametrosMantenimiento(); throw new Exception("EliminarParametros: " + ex.Message); }
        }

        private string FormatearDecimales(string value)
        {
            string result = (string) value.Clone();
            if (value.Contains("."))
            {
                var arr = value.Split('.');
                if (arr.Count() >= 2)
                {
                    result = arr[0] + "." + (arr[1].Length >= 2? arr[1].Substring(0, 2) : arr[1]);
                }
            }
            return result;
        }

        #endregion
        
    }
}
