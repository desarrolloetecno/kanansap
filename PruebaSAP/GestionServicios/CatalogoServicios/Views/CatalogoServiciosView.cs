﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KananSAP.Mantenimiento.Data;
using System.Windows.Forms;
using SAPbouiCOM;
using Kanan.Mantenimiento.BO2;
using Kanan.Vehiculos.BO2;
using KananWS.Interface;

namespace KananSAP.GestionServicios.CatalogoServicios.Views
{
    public class CatalogoServiciosView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.Button oBoton;
        private SAPbouiCOM.Matrix oMtx;
        private SAPbouiCOM.DBDataSource oDBDataSource;
        private SAPbouiCOM.DataTable oDataTable;
        private SAPbouiCOM.Conditions oConditions;
        private SAPbouiCOM.Condition oCondition;
        public ServicioSAP Entity { get; set; }
        public string FormUniqueID { get; set; }
        private XMLPerformance XmlApplication;
        public Servicio oServicio;
        public int? id;
        public string service_name;
        public Vehiculo oVehiculo { get; set; }
        public int? EmpresaID { get; set; }
        string sPath = null;//System.Windows.Forms.Application.StartupPath;
       
        private KananWS.Interface.ServiciosWS servicioWS;
        private Configurations Configs;
        public int? ServicioID { get; set; }
        private string Nombre { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Contructor del view servicios
        /// </summary>
        /// <param name="Application">SAPbouiCOM.Application</param>
        /// <param name="company">SAPbobsCOM.Company</param>
        /// <param name="mantenibleID">MantenibleID</param>
        /// <param name="mantenibleName">Nombre del mantenible</param>
        public CatalogoServiciosView(SAPbouiCOM.Application Application, SAPbobsCOM.Company company, Configurations config)
        {
            this.oServicio = new Servicio();
            this.SBO_Application = Application;
            this.Entity = new ServicioSAP(company);
            this.Entity.oServicio.Propietario.EmpresaID = 1;
            this.XmlApplication = new XMLPerformance(SBO_Application);
            this.oItem = null;
            this.oEditText = null;
            this.oBoton = null;
            this.oConditions = null;
            this.oCondition = null;
            this.id = null;
            this.service_name = null;
            this.Configs = config;
            this.servicioWS = new KananWS.Interface.ServiciosWS(Guid.Parse(config.UserFull.Usuario.PublicKey), config.UserFull.Usuario.PrivateKey);
            
        }
        #endregion

        #region Metodos

        #region Form
        /// <summary>
        /// Carga el formulario de la lista de servicios
        /// </summary>
        public void LoadForm()
        {
            sPath = System.Windows.Forms.Application.StartupPath;
            try
            {
                this.oForm = SBO_Application.Forms.Item("ServiceForm");
                this.SetConditions();
                BindDataToForm();
            }
            catch(Exception ex)
            {
                if(ex.Message.Contains("Invalid Form"))
                {
                   
                    this.XmlApplication.LoadFromXML(sPath + "\\XML",  "ListaServicios.xml");
                    this.oForm = SBO_Application.Forms.Item("ServiceForm");
                    this.SetConditions();
                    BindDataToForm();
                    //Estabkece la ventana centrada
                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                }
                else
                {
                    SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                }
            }
        }
        /// <summary>
        /// Muestra el formulario que contiene la lista de servicios disponibles
        /// </summary>
        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
                BindDataToForm();
            }
            catch(Exception ex)
            {
                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                BindDataToForm();
                this.oForm.Select();

            }
        }

        public void CloseForm()
        {
            this.oForm = SBO_Application.Forms.Item("ServiceForm");
            this.oForm.Close();
        }

        /// <summary>
        /// Establece el o los campos vacios
        /// </summary>
        public void EmptyEntityToForm()
        {
            oItem = this.oForm.Items.Item("txtbsq");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;
            //oItem = this.oForm.Items.Item("btnSearch");
            //oBoton = oItem.Specific;
            //if (this.oVehiculo == null)
            //    oBoton.Caption = "Crear";
        }

        #endregion

        #region Matrix
        /// <summary>
        /// Obtiene la lista de servicios de la base de datos
        /// </summary>
        public void BindDataToForm()
        {
            try
            {
                //oConditions = new SAPbouiCOM.Conditions();
                oDBDataSource = oForm.DataSources.DBDataSources.Item("@VSKF_SERVICIOS");
                oDBDataSource.Query(oConditions ?? null);
                oItem = oForm.Items.Item("mtxsrv");
                oMtx = (SAPbouiCOM.Matrix)oItem.Specific;
                oMtx.LoadFromDataSource();
                
            }
            catch(Exception ex)
            {
                throw new Exception("BindDataToForm: " + ex.Message);
            }
        }

        /// <summary>
        /// Obtiene una fila del control Matrix que contiene la lista de servicios
        /// </summary>
        /// <param name="index">Indice de la fila que se recuperara</param>
        /// <returns></returns>
        public string GetDataRow(int index)
        {
            try
            {
                oItem = oForm.Items.Item("mtxsrv");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(1, index);
                return oEditText.String;
            }
            catch (Exception ex)
            {
                return "Error en la aplicacion";
            }
        }

        public string GetIDDataRow(int index)
        {
            try
            {
                oItem = oForm.Items.Item("mtxsrv");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(0, index);
                int i = oMtx.RowCount;
                if (index > i)
                    throw new Exception("Seleccione un elemento válido");
                if (string.IsNullOrEmpty(oEditText.String))
                    throw new Exception("Seleccione un elemento válido");
                return oEditText.String;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Condiciones
        /// <summary>
        /// Establece el id del item selecionado en el control Matrix de la lista de servicios
        /// </summary>
        /// <param name="index">Indice del cual se obtendra el identificador</param>
        public void SetID(int index)
        {
            try
            {
                oItem = oForm.Items.Item("mtxsrv");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(0, index);
                //this.id = Convert.ToInt32(oEditText.String.Trim());
                this.oServicio.ServicioID = Convert.ToInt32(oEditText.String.Trim());

                oItem = oForm.Items.Item("mtxsrv");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(1, index);
                //this.service_name = oEditText.String.ToString();
                this.oServicio.Nombre = oEditText.String.ToString();

                oItem = oForm.Items.Item("mtxsrv");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(2, index);
                //this.service_name = oEditText.String.ToString();
                this.oServicio.Descripcion = oEditText.String.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Establece las condifiones de filtrado en el control Matrix de la lista de servicios
        /// </summary>
        public void SetConditions()
        {
            try
            {
                oItem = oForm.Items.Item("txtbsq");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                string buscando = oEditText.String.ToString().Trim();
                oCondition = null;
                oConditions = null;
                oConditions = new SAPbouiCOM.Conditions();
                oCondition = oConditions.Add();
                oCondition.Alias = "U_EsActivo";
                oCondition.Operation = BoConditionOperation.co_EQUAL;
                oCondition.CondVal = "1";
                //oCondition.Relationship = BoConditionRelationship.cr_AND;
                //oCondition = oConditions.Add();
                //oCondition.Alias = "U_EmpresaID";
                //oCondition.Operation = BoConditionOperation.co_EQUAL;
                //oCondition.CondVal = this.Configs.UserFull.Dependencia.EmpresaID.ToString();
                
                if (!string.IsNullOrEmpty(buscando))
                {
                    oCondition.Relationship = BoConditionRelationship.cr_AND;
                    oCondition = oConditions.Add();
                    oCondition.Alias = "U_Nombre";
                    oCondition.Operation = BoConditionOperation.co_CONTAIN;
                    oCondition.CondVal = buscando;
                    //oCondition.Relationship = BoConditionRelationship.cr_OR;
                    
                    //oCondition = oConditions.Add();
                    //oCondition.Alias = "U_Descripcion";
                    //oCondition.Operation = BoConditionOperation.co_CONTAIN;
                    //oCondition.CondVal = buscando;
                }
                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        
        #endregion
        
        public bool HasRowValue(int rowIndex)
        {
            try
            {
                this.oForm = this.SBO_Application.Forms.Item("ServiceForm");
                oMtx = oForm.Items.Item("mtxsrv").Specific;
                int rows = oMtx.RowCount;
                if (rowIndex <= 0 || rowIndex > rows)
                    return false;
                EditText txtNombre = (SAPbouiCOM.EditText)oMtx.GetCellSpecific("nombresrv", rowIndex);
                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific("#", rowIndex);
                this.ServicioID = null;
                this.Nombre = null;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()) && !string.IsNullOrEmpty(txtNombre.String.Trim()))
                {
                    this.ServicioID = Convert.ToInt32(oEditText.String.Trim());
                    this.Nombre = txtNombre.String.Trim();
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void Eliminar()
        {
            try
            {
                int id = (int)this.ServicioID;
                this.Entity.ItemCode = id.ToString();
                if (this.Entity.Existe())
                {
                    this.Entity.GetSyncUUID();
                    Servicio sv = new Servicio();
                    sv.ServicioID = id;
                    SAPbobsCOM.Recordset rs = this.Entity.ConsultarSoloServicio(sv);
                    if (this.Entity.HashServicios(rs))
                        sv = this.Entity.LastRecordSetToSoloServicio(rs);
                    sv.EsActivo = false;
                    this.Entity.oServicio = sv;
                    this.Entity.Sincronizado = 3;
                    this.Entity.ServicioToUDT();
                    this.Entity.EliminarCompleto();//.Actualizar();
                    if(this.servicioWS.EliminaServicio(id))
                    {
                        this.Entity.Existe();
                        this.Entity.Sincronizado = 0;
                        this.Entity.Sincronizar();
                    }
                    this.SBO_Application.StatusBar.SetText("¡Registro Eliminado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    this.BindDataToForm();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
