﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KananSAP.Mantenimiento.Data;
using Kanan.Mantenimiento.BO2;
using KananWS.Interface;
using SAPinterface.Data.INFO.Tablas;
using SAPinterface.Data.PD;


namespace KananSAP.GestionServicios.CatalogoServicios.Views
{
    public class ServicioView
    {
        #region Variables
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.ComboBox oCombo;
        private SAPbouiCOM.StaticText oStaticText;
        public string FormUniqueID { get; set; }
        private XMLPerformance XmlApplication;
        private TipoServicioData tsData;
        public Servicio Entity { get; set; }
        private ServicioSAP servicioData;
        private KananWS.Interface.ServiciosWS servicioWS;
        private Configurations Configs;
        SAPbobsCOM.Company oCompany;
        #endregion
        public ServicioView(SAPbouiCOM.Application app, SAPbobsCOM.Company company, Configurations config)
        {
            this.SBO_Application = app;
            this.oCompany = company;
            this.tsData = new TipoServicioData(company);
            this.PrepareNewEntity();
            this.XmlApplication = new XMLPerformance(this.SBO_Application);
            servicioData = new ServicioSAP(company);
            this.Configs = config;
            servicioWS = new KananWS.Interface.ServiciosWS(Guid.Parse(config.UserFull.Usuario.PublicKey), config.UserFull.Usuario.PrivateKey);
            oStaticText = null;
            oEditText = null;
            oCombo = null;
        }

        #region FORM

        public void PrepareNewEntity()
        {
            Entity = new Servicio();
            Entity.TipoServicio = new TipoServicio();
            Entity.Propietario = new Kanan.Operaciones.BO2.Empresa();
            Entity.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
        }
        /// <summary>
        /// Carga el formulario de la lista de servicios
        /// </summary>
        public void LoadForm()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("AddServ");
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Invalid Form"))
                {
                    string sPath = System.Windows.Forms.Application.StartupPath;                    
                    this.XmlApplication.LoadFromXML(sPath + "\\XML",  "Servicios.xml");
                    this.oForm = SBO_Application.Forms.Item("AddServ");
                    //Estabkece la ventana centrada
                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                }
                else
                {
                    SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                }
            }
        }
        /// <summary>
        /// Muestra el formulario que contiene la lista de servicios disponibles
        /// </summary>
        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
                this.ChooseFromList();
            }
            catch (Exception ex)
            {
                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                this.oForm.Select();
                this.ChooseFromList();
            }
        }

        #endregion

        #region FILL COMBO

       /* public void FillComboCcontable()
        {
            #region Comentarios
            //Metodo que llena el combo para visualizar las cuentas contables
            #endregion
            try
            {

                oItem = this.oForm.Items.Item("cbxTAcct");
                oCombo = oItem.Specific;
                KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);
                oCombo.ValidValues.Add("", "");
                SAPbobsCOM.Recordset rs = tsData.Cuentascontable(GestionGlobales.bSqlConnection);
                rs.MoveFirst();

                for (int i = 0; i < rs.RecordCount; i++)
                {
                    string value = Convert.ToString(rs.Fields.Item("AcctCode").Value);
                    string description = string.Format("{0} - {1}", rs.Fields.Item("AcctCode").Value, rs.Fields.Item("AcctName").Value);
                    oCombo.ValidValues.Add(value, description);
                    rs.MoveNext();
                }
                oCombo.Item.DisplayDesc = true;

            }
            catch (Exception oError)
            {
                SBO_Application.SetStatusBarMessage("Error al llenar el Combo Cuentas Contable " + oError.ToString(), BoMessageTime.bmt_Short, true);
            }

        }*/

        public void FillComboTServicio()
        {
            try
            {
                Kanan.Mantenimiento.BO2.TipoServicio ts = new Kanan.Mantenimiento.BO2.TipoServicio();
                //ts.Propietario = new Kanan.Operaciones.BO2.Empresa() { EmpresaID = Configs.UserFull.Dependencia.EmpresaID };
                SAPbobsCOM.Recordset rs = tsData.Consultar(ts);
                if(tsData.HasTipoServicio(rs))
                {
                    oItem = this.oForm.Items.Item("cbxTServ");
                    oCombo = oItem.Specific;
                    KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("", "");
                    rs.MoveFirst();
                    for(int i =0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("U_TIPOSERVICIOID").Value);
                        string description = string.Format("{0} - {1}", rs.Fields.Item("U_Nombre").Value, rs.Fields.Item("U_DESCRIPCION").Value);
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }
                    oCombo.Item.DisplayDesc = true;
                }
            }
            catch(Exception ex)
            {
                SBO_Application.SetStatusBarMessage("Error al llenar el Combo TipoVehiculo", BoMessageTime.bmt_Short, true);
            }
        }
        
        #endregion

        void ChooseFromList()
        {
            this.AddChooseItemCode();
            this.AddChooseAccounts();

        }

        void AddChooseAccounts()
        {
            try
            {
                Item oItem = null;
                oForm.DataSources.UserDataSources.Add("EdtOACT", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);
                SAPbouiCOM.ChooseFromListCollection oCFLs = null;
                oCFLs = oForm.ChooseFromLists;
                SAPbouiCOM.ChooseFromList oCFL = null;
                SAPbouiCOM.ChooseFromListCreationParams oCFLCreationParams = null;
                oCFLCreationParams = ((SAPbouiCOM.ChooseFromListCreationParams)(SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)));
                oCFLCreationParams.MultiSelection = false;
                oCFLCreationParams.ObjectType = "1";
                oCFLCreationParams.UniqueID = "CFLOACT";
                oCFL = oCFLs.Add(oCFLCreationParams);
                oItem = oForm.Items.Item("txtacct");
                EditText oEditText = (EditText)(oItem.Specific);
                oEditText.DataBind.SetBound(true, "", "EdtOACT");
                oEditText.ChooseFromListUID = "CFLOACT";
                oEditText.ChooseFromListAlias = "AcctCode";
            }
            catch (Exception ex)
            {

            }
        }
        void AddChooseItemCode()
        {

            try
            {
                Item oItem = null;
                oForm.DataSources.UserDataSources.Add("EdtOITM", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);
                SAPbouiCOM.ChooseFromListCollection oCFLs = null;
                oCFLs = oForm.ChooseFromLists;
                SAPbouiCOM.ChooseFromList oCFL = null;
                SAPbouiCOM.ChooseFromListCreationParams oCFLCreationParams = null;
                oCFLCreationParams = ((SAPbouiCOM.ChooseFromListCreationParams)(SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)));
                oCFLCreationParams.MultiSelection = false;
                oCFLCreationParams.ObjectType = "4";
                oCFLCreationParams.UniqueID = "CFLOITM";
                oCFL = oCFLs.Add(oCFLCreationParams);
                oItem = oForm.Items.Item("txtitcde");
                EditText oEditText = (EditText)(oItem.Specific);
                oEditText.DataBind.SetBound(true, "", "EdtOITM");
                oEditText.ChooseFromListUID = "CFLOITM";
                oEditText.ChooseFromListAlias = "ItemCode";
            }
            catch (Exception ex)
            {

            }
        }

        public void ClearFields()
        {
            try
            {
                this.oForm = this.SBO_Application.Forms.Item("AddServ");
                //oItem = oForm.Items.Item("txtImagen");
                //oEditText = oItem.Specific;
                //oEditText.String = string.Empty;
                oItem = oForm.Items.Item("lblIDSV");
                oStaticText = oItem.Specific;
                oStaticText.Caption = "0";

                oItem = oForm.Items.Item("txtDescrip");
                oEditText = oItem.Specific;
                oEditText.String = string.Empty;

                oItem = oForm.Items.Item("txtNombre");
                oEditText = oItem.Specific;
                oEditText.String = string.Empty;

                oItem = oForm.Items.Item("cbxTServ");
                oCombo = oItem.Specific;
                oCombo.Select("", BoSearchKey.psk_ByValue);

                //oItem = oForm.Items.Item("cbxTAcct");
                //oCombo = oItem.Specific;
                //oCombo.Select("", BoSearchKey.psk_ByValue);

                oItem = oForm.Items.Item("txtacct");
                oEditText = oItem.Specific;
                oEditText.String = string.Empty;

                oItem = oForm.Items.Item("txtitcde");
                oEditText = oItem.Specific;
                oEditText.String = string.Empty;

                oItem = oForm.Items.Item("txtaccnom");
                oEditText = oItem.Specific;
                oEditText.String = string.Empty;

                Entity = new Servicio();
                Entity.TipoServicio = new TipoServicio();
                Entity.Propietario = new Kanan.Operaciones.BO2.Empresa();
                Entity.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();

            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string ValidateFields()
        {
            try
            {
                StringBuilder mns = new StringBuilder();
                string err = string.Empty;
                oForm = SBO_Application.Forms.Item("AddServ");
                oItem = oForm.Items.Item("txtNombre");
                oEditText = oItem.Specific;
                if (string.IsNullOrEmpty(oEditText.String))
                    mns.Append(", nombre");
                oItem = oForm.Items.Item("txtDescrip");
                oEditText = oItem.Specific;
                if (string.IsNullOrEmpty(oEditText.String))
                    mns.Append(", descripcion");


                oItem = oForm.Items.Item("txtacct");
                oEditText = oItem.Specific;
                if (string.IsNullOrEmpty(oEditText.String))
                    mns.Append(", cuenta contable");


                oItem = oForm.Items.Item("txtitcde");
                oEditText = oItem.Specific;
                if (string.IsNullOrEmpty(oEditText.String))
                    mns.Append(", dato maestro de artículo");


                oItem = oForm.Items.Item("cbxTServ");
                oCombo = oItem.Specific;
                if (string.IsNullOrEmpty(oCombo.Value) || oCombo.Value.ToString() == "")
                    mns.Append(", tipo de servicio");
                if (this.Configs.UserFull.Dependencia.EmpresaID == null)
                    mns.Append(", empresa");
                if(mns.Length > 0)
                {
                    err = mns.ToString();
                    if (err.StartsWith(","))
                        err = "Los siguientes campos son obligatorios:\n" + err.Substring(1);
                }
                return err;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void FormToEntity()
        {
            try
            {
                oForm = SBO_Application.Forms.Item("AddServ");
                oItem = oForm.Items.Item("lblIDSV");
                oStaticText = oItem.Specific;
                int id = Convert.ToInt32(oStaticText.Caption);
                if (id != 0)
                    this.Entity.ServicioID = id;

                oItem = oForm.Items.Item("txtNombre");
                oEditText = oItem.Specific;
                Entity.Nombre = oEditText.String;
                oItem = oForm.Items.Item("txtDescrip");
                oEditText = oItem.Specific;
                Entity.Descripcion = oEditText.String;
                //oItem = oForm.Items.Item("txtImagen");
                //oEditText = oItem.Specific;
                //Entity.ImagenUrl = oEditText.String;

                oItem = oForm.Items.Item("txtacct");
                oEditText = oItem.Specific;
                this.Entity.CuentaContable = oEditText.String.Trim().ToString();

                oItem = oForm.Items.Item("txtitcde");
                oEditText = oItem.Specific;
                this.Entity.ItemCode = oEditText.String.Trim().ToString();

                oItem = oForm.Items.Item("txtaccnom");
                oEditText = oItem.Specific;
                this.Entity.NombreCuenta = oEditText.String.Trim().ToString();

                oItem = oForm.Items.Item("cbxTServ");
                oCombo = oItem.Specific;
                Entity.TipoServicio.TipoServicioID = Convert.ToInt32(oCombo.Value);
                this.Entity.Propietario.EmpresaID = this.Configs.UserFull.Dependencia.EmpresaID;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region ABC WEB SERVICE KANAN
        /// <summary>
        /// Guarda la informacion en Kanan
        /// </summary>
        private bool InsertarWS()
        {
            try
            {
                Servicio svTmp = this.servicioWS.InsertarServicio(this.Entity);
                if (svTmp == null)
                    return false;
                if (svTmp.ServicioID == null)
                    return false;
                this.Entity.ServicioID = svTmp.ServicioID;
                return true;
            }
            catch(Exception ex)
            {
                throw new Exception("Kanan-InsertarWS: " + ex.Message);
            }
        }

        /// <summary>
        /// Actualiza la informacion en Kanan
        /// </summary>
        private bool ActualizarWS()
        {
            try
            {
                return this.servicioWS.ActualizaServicio(this.Entity);
            }
            catch (Exception ex)
            {
                throw new Exception("Kanan-ActualizarWS: " + ex.Message);
            }
        }
        /// <summary>
        /// Elimina la informacion en Kanan
        /// </summary>
        private bool EliminarWS()
        {
            try
            {
                return this.servicioWS.EliminaServicio(this.Entity.ServicioID);
            }
            catch (Exception ex)
            {
                throw new Exception("Kanan-EliminarWS: " + ex.Message);
            }
        }
        #endregion

        #region ABC SAP

        /// <summary>
        /// Guarda la informacion en SAP
        /// </summary>
        private void Insertar()
        {
            try
            {
                this.servicioData.oServicio = this.Entity;
                this.servicioData.Sincronizado = 1;
                this.servicioData.UUID = Guid.NewGuid();
                this.servicioData.ItemCode = DateTime.Now.Ticks.ToString();
                this.servicioData.ServicioToUDT();
                this.servicioData.Insertar();
                if(this.InsertarWS())
                {
                    this.servicioData.Sincronizado = 0;
                    this.servicioData.ActualizarCode(this.Entity);
                }
                this.ClearFields();
            }
            catch (Exception ex)
            {
                throw new Exception("SAP-Insertar: " + ex.Message);
            }
        }
        /// <summary>
        /// Actualiza en SAP
        /// </summary>
        private void Actualizar()
        {
            try
            {
                this.servicioData.oServicio = this.Entity;                
                this.servicioData.ItemCode = this.Entity.ServicioID.ToString();
                if (!this.servicioData.Existe())
                    throw new Exception("El regitro que intenta actualizar no existe");
                this.servicioData.GetSyncUUID();
                this.servicioData.Sincronizado = 2;
                this.servicioData.ServicioToUDT();
                this.servicioData.Actualizar();
                if(this.ActualizarWS())
                {
                    this.servicioData.Existe();
                    this.servicioData.Sincronizado = 0;
                    this.servicioData.Sincronizar();
                }

                //this.Entity.Sincronizacion = TipoSync.Sincronizado;
                this.ClearFields();
            }
            catch (Exception ex)
            {
                throw new Exception("SAP-Actualizar: " + ex.Message);
            }
        }

        /// <summary>
        /// Elimina en SAP
        /// </summary>
        public void EliminarCompleto()
        {
            try
            {
                
                this.servicioData.ItemCode = this.Entity.ServicioID.ToString();
                if (this.servicioData.Existe())
                {
                    this.Entity.EsActivo = false;
                    this.servicioData.oServicio = this.Entity;
                    this.servicioData.GetSyncUUID();
                    this.servicioData.Sincronizado = 3;
                    this.servicioData.ServicioToUDT();
                    this.servicioData.EliminarCompleto();//.Actualizar();
                    if(this.EliminarWS())
                    {
                        this.servicioData.Existe();
                        this.servicioData.Sincronizado = 0;
                        this.servicioData.Sincronizar();
                        this.servicioData.Eliminar();
                    }
                    this.SBO_Application.StatusBar.SetText("¡Registro Eliminado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("EliminarCompleto: " +ex.Message);
            }
        }

        #endregion

        public void InsertarOActualizar()
        {
            try
            {
                string err = this.ValidateFields();
                if (!string.IsNullOrEmpty(err))
                    throw new Exception(err);
                oItem = oForm.Items.Item("lblIDSV");
                oStaticText = oItem.Specific;
                int id = Convert.ToInt32(oStaticText.Caption);
                this.FormToEntity();
                if(id == 0)
                {
                    this.Insertar();
                    //this.SBO_Application.Forms.ActiveForm.Close();
                }
                else
                {
                    this.Actualizar();
                    this.SBO_Application.Forms.ActiveForm.Close();
                }
                
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);//SBO_Application.SetStatusBarMessage("InsertarOActualizar: " + ex.Message, BoMessageTime.bmt_Short, true);
            }
        }

        public void cargaNombreCuenta(string numcuenta)
        {
            try
            {
                List<SBO_KF_OACT_INFO> lstCuentas = new List<SBO_KF_OACT_INFO>();
                SBO_KF_OCRD_PD oCuenta = new SBO_KF_OCRD_PD();
                oCuenta.listaCuentasSAP(ref this.oCompany, ref lstCuentas, numcuenta);
                if (lstCuentas.Count > 0)
                {
                    string NombreCuenta = lstCuentas[0].AcctName;
                    SAPbouiCOM.EditText oEditText = (EditText)this.oForm.Items.Item("txtaccnom").Specific;
                    oEditText.String = NombreCuenta;
                }

            }
            catch { }
        }

        public void Consultar(Servicio sv)
        {
            try
            {
                SAPbobsCOM.Recordset rs = this.servicioData.Consultar(sv);
                if (servicioData.HashServicios(rs))
                    this.Entity = servicioData.LastRecordSetToServicio(rs);
                else throw new Exception("No se encontro ningun registro");
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void DataEntityToForm()
        {
            try
            {
                if (this.Entity.ServicioID != null)
                    this.oForm.Title = "Actualizar servicio";
                this.oForm = this.SBO_Application.Forms.Item("AddServ");
                //oItem = oForm.Items.Item("txtImagen");
                //oEditText = oItem.Specific;
                //oEditText.String = string.Empty;
                oItem = oForm.Items.Item("lblIDSV");
                oStaticText = oItem.Specific;
                oStaticText.Caption = this.Entity.ServicioID.ToString();

                oItem = oForm.Items.Item("txtDescrip");
                oEditText = oItem.Specific;
                oEditText.String = this.Entity.Descripcion;

                oItem = oForm.Items.Item("txtNombre");
                oEditText = oItem.Specific;
                oEditText.String = this.Entity.Nombre;

                oItem = oForm.Items.Item("txtacct");
                oEditText = oItem.Specific;
                oEditText.String = this.Entity.CuentaContable;

                oItem = oForm.Items.Item("txtitcde");
                oEditText = oItem.Specific;
                oEditText.String = this.Entity.ItemCode;

                oItem = oForm.Items.Item("txtaccnom");
                oEditText = oItem.Specific;
                oEditText.String = this.Entity.NombreCuenta;

                oItem = oForm.Items.Item("cbxTServ");
                oCombo = oItem.Specific;
                try
                { oCombo.Select(this.Entity.TipoServicio.TipoServicioID.ToString(), BoSearchKey.psk_ByValue); }

                catch { throw new Exception("El tipo de servicio no existe o ha sido eliminado."); }
            }
            catch(Exception ex)
            {
                this.SBO_Application.Forms.ActiveForm.Close();
                throw new Exception(ex.Message);
            }
        }

        public void HideDeleteButton()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("AddServ");
                oItem = oForm.Items.Item("btnDelSvr");
                oItem.Visible = false;
                oItem = oForm.Items.Item("btnCancelS");
                oItem.Left = 84;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }


    }
}
