﻿using KananFleet.GestionCausaRaiz.Control;
using KananFleet.OrdenesServicio.AdministrarOrdenesServicio.Views;
using KananFleet.OrdenesServicio.SolicitarOrdenServicio.Views;
using KananFleet.Version;
using KananSAP.Configuracion.Configuraciones.Controller;
using KananSAP.GestionDatos.MigradorDatos.View;
using KananSAP.GestionFacturaClientes;
using KananSAP.GestionFacturaClientes.Controller;
using KananSAP.Helper;
using KananWS.Interface;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP
{
    public class MainController
    {
        #region Atributos
        private Configurations oConfiguration = null;
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private ItenerarioView oItenerarioView = null;
        private MigradorView oLlantas = null;
        private MigradorView oRecargas = null;
        private SBO_KF_AdministraOSView oGestionOSView = null;
        private SBO_KF_GestionCausaRaiz oCausaRaizMain = null;
        private SBO_KF_SolicitaOrdenesView oSolicitaOrden = null;
        private Dictionary<String, GestionARInvoices> oCollectorInvoices = new Dictionary<string, GestionARInvoices>();
        private Dictionary<int, GestionConfig> oCollectorConfigu = new Dictionary<int, GestionConfig>();
        private GestionGlobales oGlobales = null;
        private string EndPoint = System.Configuration.ConfigurationManager.AppSettings.Get("EndPoint");
        private string EndPointPizarra = System.Configuration.ConfigurationManager.AppSettings.Get("URL_Pizarra");
        #endregion


        public MainController(Configurations oConfig, GestionGlobales oGlobales)
        {
            this.oGlobales = oGlobales;
            this.oConfiguration = oConfig;
            this.Company = oGlobales.Company;
            this.SBO_Application = oGlobales.SBO_Application;
            this.SBO_Application.ItemEvent += new _IApplicationEvents_ItemEventEventHandler(SBO_Application_EventoControl);
            this.SBO_Application.MenuEvent += new _IApplicationEvents_MenuEventEventHandler(SBO_Application_MenuEvent);
            this.SBO_Application.FormDataEvent += new _IApplicationEvents_FormDataEventEventHandler(SBO_Application_DatosForma);
            this.SBO_Application.AppEvent += new _IApplicationEvents_AppEventEventHandler(SBO_Application_AppEvent);
        }

        private void SBO_Application_AppEvent(BoAppEventTypes EventType)
        {
            //BLOQUE DE CÓDIGO PARA CERRAR EL ADDON EN CASO DE SALIRSE DEL CLIENTE O CAMBIAR DE COMPAÑIA
            if (EventType == BoAppEventTypes.aet_ShutDown)
                System.Environment.Exit(0);
            if(EventType == BoAppEventTypes.aet_CompanyChanged)
                System.Environment.Exit(0);

        }

        

        private void SBO_Application_MenuEvent(ref SAPbouiCOM.MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (pVal.BeforeAction)
                {
                    switch (pVal.MenuUID)
                    {
                        case "ConfigLev":
                            int iSiguienteID = NextConfigID();
                            GestionConfig oForma = new GestionConfig(this.oGlobales, iSiguienteID.ToString(), this.Company);
                            oCollectorConfigu.Add(iSiguienteID, oForma);
                            break;

                        case "GesIten":
                            oItenerarioView = new ItenerarioView(this.oGlobales, this.Company);
                            break;
                        case "migrador":
                            oLlantas = new MigradorView(this.oGlobales, this.Company, "llantas", "l", -1);
                            break;

                        case "AdmiOrdSer":
                            oGestionOSView = new SBO_KF_AdministraOSView(this.oGlobales, this.oConfiguration, ref this.Company);
                            break;
                        case "AddOrdSv":
                            oSolicitaOrden = new SBO_KF_SolicitaOrdenesView(this.oGlobales, this.oConfiguration, ref this.Company, "add", "add");
                            break;
                        case "GTCausar":
                            oCausaRaizMain = new SBO_KF_GestionCausaRaiz(this.oGlobales, ref this.Company, this.oConfiguration);
                            break;

                        case "KFVersion":
                            new SBO_KF_Version(this.oGlobales, this.Company);
                            break;

                        case "RptO001":
                            this.StartReport("ConsumoRefacciones");
                            break;
                        case "RptO002":
                            this.StartReport("CostosMantenimiento");
                            break;
                        case "RptO003":
                            this.StartReport("CostoMensual");
                            break;
                        case "RptO004":
                            this.StartReport("DisponibilidadEquipo");
                            break;
                        case "RptO005":
                            this.StartReport("MeanTimeToRepair");
                            break;
                        case "RptO006":
                            this.StartReport("ProximosServicios");
                            break;
                        case "RptO007":
                            this.StartReport("RendimientoCombustible");
                            break;
                        case "RptO008":
                            this.StartReport("TiempoPromedioFallas");
                            break;
                        case "RptO009":
                            this.StartReport("TiempoPromedioReparaciones");
                            break;
                        case "RptO010":
                            this.StartReport("UsoPorHora");
                            break;

                        case "RptPiz001":
                            this.StarReportPizarra();
                            break;
                        case "RptPiz002":
                            this.StarReportPizarra(true);
                            break;
                    }

                }

            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError("MainController.cs->SBO_Application_MenuEvent()", ex.Message);
            }
        }

        private void SBO_Application_DatosForma(ref BusinessObjectInfo pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            String FormID = string.Empty;
            Form oFormulario = null;
            GestionARInvoices InvoiceTemp = null;            
            BubbleEvent = true;

            try
            {
                if (!pVal.BeforeAction)
                {
                    switch (pVal.FormTypeEx)
                    {

                        case "133":
                            if (pVal.EventType == BoEventTypes.et_FORM_DATA_ADD)
                            {
                                FormID = pVal.FormUID;
                                InvoiceTemp = RetriveInvoice(FormID);
                                oFormulario = SBO_Application.Forms.Item(pVal.FormUID);
                                if (InvoiceTemp != null)
                                {
                                    InvoiceTemp.EventoDatosForma(pVal, out BubbleEvent, oFormulario);
                                    if (!BubbleEvent)
                                    {
                                        BubbleEvent = false;
                                        break;
                                    }

                                }
                            }

                            break;

                        default:
                            break;

                    }
                    //KananSAPHerramientas.LogError("MainController.cs->SBO_Application_DatosForma()", "BEFORE" + pVal.EventType.ToString());
                }
                else
                {
                    //KananSAPHerramientas.LogError("MainController.cs->SBO_Application_DatosForma()","AFTER" + pVal.EventType.ToString());
                }

            }
            catch (Exception ex)
            {
                BubbleEvent = false;
                KananSAPHerramientas.LogError("MainController.cs->SBO_Application_DatosForma()", ex.Message);
            }
        }

        private void SBO_Application_EventoControl(String FormUID, ref ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            String FormID = string.Empty;
            Form oFormulario = null;
            GestionARInvoices InvoiceTemp = null;
            if (pVal.FormTypeEx == null)
                return;
            try
            {
                 
                switch (pVal.FormTypeEx)
                {
                    
                    case "GestCarga":
                        if (pVal.ItemUID == "btnRCcsv")
                        { 
                        
                        }

                        break;
                        
                    case "frmAddOS":
                    case "AdminOServ":                    
                        if (oGestionOSView != null)
                            oGestionOSView.EventoControl(pVal, out BubbleEvent);
                    
                        if (oSolicitaOrden != null)
                            oSolicitaOrden.SBO_Application_ItemEvent(pVal.FormUID, ref pVal, out BubbleEvent);
                        break;

                    case "AdmCausaraiz":
                    case "AddUpdateCR":
                        if (oCausaRaizMain != null)
                            oCausaRaizMain.EventoControl(pVal, out BubbleEvent);
                        break;
                    case "migrador":
                        if(oLlantas != null)
                            oLlantas.EventoControl(pVal, out BubbleEvent);

                        if (oRecargas != null)
                            oRecargas.EventoControl(pVal, out BubbleEvent);
                        break;

                    case "GestVieI":
                        if (oItenerarioView != null)
                        {
                            oItenerarioView.EventoControl(pVal, out BubbleEvent);
                        }
                        break;
                    case "GestConf":
                        oFormulario = SBO_Application.Forms.Item(pVal.FormUID);
                        FormID = pVal.FormUID.Substring(pVal.FormUID.IndexOf('_') + 1);
                        if (pVal.EventType == BoEventTypes.et_FORM_CLOSE)
                        {
                            if (pVal.Before_Action)
                            {
                                oCollectorConfigu.Remove(Convert.ToInt32(FormID));
                            }
                        }

                        if (pVal.EventType == BoEventTypes.et_CHOOSE_FROM_LIST)
                        {
                            if (!pVal.Before_Action)
                            {
                                GestionConfig tmp = oCollectorConfigu.FirstOrDefault(x => x.Key == Convert.ToInt32(FormID)).Value;
                                if (tmp != null)
                                {
                                    SAPbouiCOM.IChooseFromListEvent oCFLEvento = null;
                                    oCFLEvento = ((SAPbouiCOM.IChooseFromListEvent)(pVal));
                                    string sCFL_ID = null;
                                    sCFL_ID = oCFLEvento.ChooseFromListUID;
                                    SAPbouiCOM.Form oForm = null;
                                    oForm = SBO_Application.Forms.Item(FormUID);
                                    SAPbouiCOM.ChooseFromList oCFL = null;
                                    oCFL = oForm.ChooseFromLists.Item(sCFL_ID);
                                    string val = string.Empty;
                                    if (oCFLEvento.BeforeAction == false)
                                    {
                                        SAPbouiCOM.DataTable oDataTable = null;
                                        oDataTable = oCFLEvento.SelectedObjects;
                                        val = System.Convert.ToString(oDataTable.GetValue(0, 0));
                                        string sItemCFL = string.Empty;
                                        switch (sCFL_ID)
                                        {
                                            case "CFLOACT1":
                                                sItemCFL = "EdtOACT1";
                                                break;
                                            case "CFLOACT2":
                                                sItemCFL = "EdtOACT2";
                                                break;
                                            case "CFLOACT3":
                                                sItemCFL = "EdtOACT3";
                                                break;
                                            case "CFLOACT4":
                                                sItemCFL = "EdtOACT4";
                                                break;
                                        }
                                        oForm.DataSources.UserDataSources.Item(sItemCFL).ValueEx = val;
                                        tmp.buscaNombreCuenta(val, sCFL_ID);
                                    }
                                }
                            }
                        }

                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            if (pVal.Before_Action)
                            {
                                GestionConfig tmp = oCollectorConfigu.FirstOrDefault(x => x.Key == Convert.ToInt32(FormID)).Value;
                                if (tmp != null)
                                    tmp.EventoControl(pVal, out BubbleEvent, oFormulario);
                            }
                        }
                        break;
                    case "133":
                        oFormulario = SBO_Application.Forms.GetForm(pVal.FormTypeEx, pVal.FormTypeCount);

                        if (pVal.EventType == BoEventTypes.et_FORM_LOAD)
                        {
                            if (pVal.BeforeAction)
                            {
                                FormID = pVal.FormUID;
                                Button oButton = AgregaControlForm(oFormulario);
                                InvoiceTemp = RetriveInvoice(FormID);
                                return;
                            }
                        } 

                        if (pVal.EventType == BoEventTypes.et_FORM_CLOSE)
                        {
                            if (pVal.Before_Action)
                            {
                                FormID = pVal.FormUID;
                                InvoiceTemp = RetriveInvoice(FormID);
                                if (InvoiceTemp != null)
                                {
                                    InvoiceTemp.CartaporteINFO = null;
                                    InvoiceTemp.CartaForm = null;
                                    InvoiceTemp.UIITEmporal = string.Empty;
                                    try { InvoiceTemp.CartaForm.oForm.Close(); }
                                    catch { }
                                    oCollectorInvoices.Remove(FormID);
                                }
                            }
                        }

                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            if (pVal.Before_Action)
                            {
                                FormID = pVal.FormUID;
                                InvoiceTemp = RetriveInvoice(FormID);
                                if (InvoiceTemp != null)
                                    InvoiceTemp.EventoControl(pVal, out BubbleEvent, oFormulario);
                            }
                        }
                        break;

                    default:
                        break;
                }
                //return BubbleEvent;
            }
            catch (Exception ex)
            {
                this.oGlobales.Mostrarmensaje(ex.Message.ToString(), BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                KananSAPHerramientas.LogError("MainController.cs->SBO_Application_EventoControl()", ex.Message);
            }
        }

        //void StarReportPizarra(bool bTodas = false)
        //{
        //    string sURL = sURL = string.Format("{0}&Perfil=0&Empresa={1}", EndPointPizarra, this.oConfiguration.UserFull.Dependencia.EmpresaID);
        //    if (bTodas)
        //        sURL = string.Format("{0}&Perfil=1&Empresa=", EndPointPizarra);
        //    Process.Start(sURL);
        //}

        void StarReportPizarra(bool bTodas = false)
        {
            string sURL = sURL = string.Format("{0}/Modulos/Vectium/vec01PizarraOperadores.aspx?Perfil=0&Empresa={1}", EndPointPizarra, this.Company.CompanyDB.ToString());
            if (bTodas)
                sURL = string.Format("{0}/Modulos/Vectium/vec01PizarraOperadores.aspx?Perfil=1&Empresa=", EndPointPizarra);
            Process.Start(sURL);
        }

        void StartReport(string rutaWebAPI)
        {
            string sURL = string.Format("{0}reportes/{1}/?id={2}", GestionGlobales.sURLReport, rutaWebAPI, this.oConfiguration.UserFull.Dependencia.EmpresaID);
            Process.Start(sURL);
        }
        private int NextConfigID()
        {
            Int32 oFormaID = oCollectorConfigu.Count == 0 ? 1 : (oCollectorConfigu.Keys.Max() + 1);
            GestionConfig FormaConfig = oCollectorConfigu.FirstOrDefault(x => x.Key == oFormaID).Value;
            if (FormaConfig != null)
                return -1;
            else return oFormaID;

        }
        private GestionARInvoices RetriveInvoice(String FormularioID)
        {
            GestionARInvoices Invoice = null;
            try
            {
                Invoice = oCollectorInvoices.FirstOrDefault(x => x.Key == FormularioID).Value;
                if (Invoice == null)
                {
                    Invoice = new GestionARInvoices(this.oGlobales, FormularioID);
                    oCollectorInvoices.Add(FormularioID, Invoice);
                }
            }
            catch (Exception ex)
            {
                this.oGlobales.Mostrarmensaje(ex.Message.ToString(), BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                KananSAPHerramientas.LogError("MainController.cs->RetriveInvoice()", ex.Message);
            }
            return Invoice;
        }

        private Button AgregaControlForm(Form oFormulario)
        {
            Button btnBoton = null;

            try
            {
                oFormulario.Items.Add("btnCartaP", BoFormItemTypes.it_BUTTON);
                Button oCarta = (Button)oFormulario.Items.Item("btnCartaP").Specific;
                oCarta.Caption = "Carta porte";
                oFormulario.Items.Item("btnCartaP").Height = oFormulario.Items.Item("10000330").Height;
                oFormulario.Items.Item("btnCartaP").Width = oFormulario.Items.Item("10000330").Width;
                oFormulario.Items.Item("btnCartaP").Top = oFormulario.Items.Item("10000330").Top;
                oFormulario.Items.Item("btnCartaP").Left = 147;
                oFormulario.Items.Item("btnCartaP").Enabled = true;
                oFormulario.Items.Item("btnCartaP").AffectsFormMode = false;
            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError("MainController.cs->AgregaControlForm()", ex.Message);
            }

            return btnBoton;
        }
    }
}