﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbobsCOM;

//BO
using Etecno.Security2.BO;
using Kanan.Comun.BO2;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using Kanan.Llantas.BO2;
using Kanan.Costos.BO2;
using KananWS.Interface;

//Data
using KananSAP.Operaciones.Data;
using SAPinterface.Data;
using KananSAP.Vehiculos.Data;
using KananSAP.Llantas.Data;
using KananSAP.Costos.Data;

//Library
using KananSAP.GestionDatos.MigradorDatos.Librerias;

namespace KananSAP.GestionDatos.MigradorDatos.Librerias
{
    public class Add
    {
        #region Inicialize
        //public MainKananFleet mainKananFleet = new MainKananFleet();
        private Company Company;
        private Configurations configs;
        public SAPbobsCOM.CompanyService oCompanyService { get; set; }
        public Items item { get; set; }
        public UserTable TablaDeSAP { get; set; }
        public string CodigoDeObjeto { get; set; }
        //private const string TABLE_SERVICE_NAME = "VSKF_OPERADOR";
        int? entero;
        double? partido;
        bool? booleano;
        DateTime? date;
        Imagen imagen;

        public static Boolean EsSQL = true;
        String strQuery = "";
        InsertString Insertar;

        //Objetos
        Operador operador = new Operador { Propietario = new Empresa(), SubPropietario = new Sucursal(), Picture = new Imagen(), Licencias = new List<Licencia>(), Observaciones = new List<Observacion>(), Cursos = new List<Curso>(), Documentos = new List<Documento>()};
        OperadorSAP operadorsap;

        Sucursal sucursal = new Sucursal() { Base = new Empresa() };
        SucursalSAP sucursalsap;
        SucursalWS sucursalWS = new SucursalWS(Guid.Parse("1bb3eb27-b3d4-43d7-828b-9ca6c4e378d0"), "25012018061751378");

        Usuario usuario = new Usuario() { Estado = EstadoUsuario.Active };
        UsuarioSAP usuariosap;

        Empleado empleado = new Empleado() { Usuario = new Usuario(), Dependencia = new Empresa(), EmpleadoSucursal = new Sucursal(), longitud = new UnidadLongitud(),
        volumen = new UnidadVolumen(), Configs = new Dictionary<string,string>()};
        EmpleadoWS empleadoWS = new EmpleadoWS();

        Vehiculo vehiculo = new Vehiculo() { Propietario = new Empresa(), SubPropietario = new Sucursal(), TipoVehiculo = new TipoVehiculo(), EquipoRastreo = new Modem()};
        VehiculoSAP vehiculosap;
        VehiculoWS vehiculoWS = new VehiculoWS(Guid.Parse("1bb3eb27-b3d4-43d7-828b-9ca6c4e378d0"), "25012018061751378");

        Llanta llanta = new Llanta() { proveedorLlanta = new Proveedor(), Propietario = new Empresa(), SubPropietario = new Sucursal() };
        LlantaSAP llantasap;
        LlantasWS llantasWS = new LlantasWS(Guid.Parse("1bb3eb27-b3d4-43d7-828b-9ca6c4e378d0"), "25012018061751378");

        Proveedor proveedor = new Proveedor();
        ProveedorSAP proveedorsap;
        //ProveedorWS proveedorWS = new ProveedorWS();
        #endregion

        #region Existe
        public bool ExisteTablaDeSAP()
        {
            return TablaDeSAP.GetByKey(this.CodigoDeObjeto);
        }
        #endregion

        #region Add
        public Add(ref Company oCom, Configurations conf)
        {
            this.Company = oCom;
            this.configs = conf;
            try
            {
                Insertar = new InsertString(this.Company);
                sucursalsap = new SucursalSAP(oCom);
            }
            catch (Exception)
            {
                //this.TablaDeSAP = null;
            }
        }
        #endregion

        #region LigarEmpresa
        public void LigarEmpresa()
        {
            try
            {
                String RelEmpresa = "[@VSKF_RelEmpresa]";
                String RelEmpleado = "[@VSKF_RelEmpleado]";

                strQuery = string.Format("INSERT INTO {0} VALUES('{1}','{2}','{3}','{4}')", RelEmpresa, "PROMASA-REAL", "PROCESADORA DE METALES S.A", "199", "PROMASA");
                Insertar.Cadena(strQuery);

                strQuery = string.Format("INSERT INTO {0} VALUES('{1}','{2}','{3}','{4}')", RelEmpleado, "245", "Jenie Altamirano Ortega", "245", "364");
                Insertar.Cadena(strQuery);


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " ");
            }
        }
        #endregion

        #region Identificador
        public void Identificador(ArrayList Identificar)
        {
            ArrayList Tabla = new ArrayList();
            Convertidor ObtenerPor= new Convertidor();
            try
            {
                //Método capáz de identificar automaticamente la tabla, si esta existe, en la cual se insertarán los datos dentro del CSV. Rair Santos.
                Tabla = ObtenerPor.Campos(Identificar[1].ToString());
                switch(Tabla[0].ToString())
                {
                    case "OperadorID":
                        break;
                }
            }
            catch ( Exception AddIdentificadorError)
            {
                throw new Exception("Error al identificar lugar de inserción: " + AddIdentificadorError);
            }
        }
        #endregion 

        //Listo
        //Error en insertar EmpleadoWS (Corroborar filtro con Postman)
        #region Operador
        public void Operadores( ArrayList MemoriaTemporal )
        {
            ArrayList CamposDeOperador = null;
            int TotalDeRepeticiones = MemoriaTemporal.Count - 1;
            Convertidor ObtenerPor = new Convertidor();
            String NombreTabla;
            int NumeroDeFila = 0;
            try
            {
                for (int NumeroDeRepeticion = 0; NumeroDeRepeticion <= TotalDeRepeticiones; NumeroDeRepeticion++ )
                {
                    NumeroDeFila = NumeroDeFila + 1;
                    CamposDeOperador = ObtenerPor.Campos(MemoriaTemporal[NumeroDeFila].ToString());
                    this.operador = null;
                    this.operador = new Operador { Propietario = new Empresa(), SubPropietario = new Sucursal(), Picture = new Imagen(), Licencias = new List<Licencia>(), Observaciones = new List<Observacion>(), Cursos = new List<Curso>(), Documentos = new List<Documento>() };

                    //this.operador.OperadorID = CamposDeOperador[0] == "NULL" ? entero : Convert.ToInt32(CamposDeOperador[0]);
                    this.operador.Nombre = CamposDeOperador[1] == "NULL" ? string.Empty : Convert.ToString(CamposDeOperador[1]);
                    this.operador.Apellido1 = CamposDeOperador[2] == "NULL" ? string.Empty : Convert.ToString(CamposDeOperador[2]);
                    this.operador.Apellido2 = CamposDeOperador[3] == "NULL" ? string.Empty : Convert.ToString(CamposDeOperador[3]);
                    this.operador.Direccion = CamposDeOperador[4] == "NULL" ? string.Empty : Convert.ToString(CamposDeOperador[4]);
                    this.operador.Telefono = CamposDeOperador[5] == "NULL" ? string.Empty : Convert.ToString(CamposDeOperador[5]);
                    this.operador.Celular = CamposDeOperador[6] == "NULL" ? string.Empty : Convert.ToString(CamposDeOperador[6]);
                    this.operador.Correo = CamposDeOperador[7] == "NULL" ? string.Empty : Convert.ToString(CamposDeOperador[7]);
                    this.operador.Curp = CamposDeOperador[8] == "NULL" ? string.Empty : Convert.ToString(CamposDeOperador[8]);
                    this.operador.FechaNacimiento = CamposDeOperador[9] == "NULL" ? Convert.ToDateTime(CamposDeOperador[9]) : date;
                    this.operador.LugarNacimiento = CamposDeOperador[10] == "NULL" ? string.Empty : Convert.ToString(CamposDeOperador[10]);
                    this.operador.Alias = CamposDeOperador[11] == "NULL" ? string.Empty : Convert.ToString(CamposDeOperador[11]);
                    this.operador.Propietario.EmpresaID = CamposDeOperador[12] == "NULL" ? Convert.ToInt32(CamposDeOperador[12]) : entero;
                    this.operador.Propietario.NotificableID = this.operador.Propietario.EmpresaID;
                    this.operador.Propietario.EsActivo = true;
                    this.operador.SubPropietario.SucursalID = CamposDeOperador[13] == "NULL" ? Convert.ToInt32(CamposDeOperador[13]) : entero;
                    this.operador.CorreosNotificacion = CamposDeOperador[14] == "NULL" ? string.Empty : Convert.ToString(CamposDeOperador[14]);
                    this.operador.EsActivo = true;
                    this.operador.Picture.ImagenID = (CamposDeOperador[16].ToString() == "NULL") ?
                    this.operador.Picture.ImagenID = null : Convert.ToInt32(CamposDeOperador[16]);
                    this.operador.EmpleadoID = CamposDeOperador[17] != "NULL" ? entero : Convert.ToInt32(CamposDeOperador[17]);
                    //this.operador.Sincronizado = CamposDeOperador[18];
                    //this.UUID = CamposDeOperador[19];

                    OperadorToEmpleado(this.operador, this.empleado);
                    this.EmpleadoToUsuario(this.empleado, this.usuario);
                    this.empleado.Usuario = this.usuario;
                    this.empleado = empleadoWS.Add(this.empleado, this.usuario);
                    EmpleadoToOperador(empleado, operador);
                    this.operador = empleadoWS.AddOprdr(this.operador);

                    //InsertarSAP
                    /*operadorsap.operador = this.operador;
                    operadorsap.OperadorToUDT();
                    operadorsap.Insert();*/
                    if (EsSQL)
                    {
                        NombreTabla = "[@VSKF_CONFIGURACION]";
                    }
                    else
                    {
                        NombreTabla = @"""@VSKF_CONFIGURACION""";
                    }
                    String Parametros = string.Format("INSERT INTO {0} VALUES('{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}')", NombreTabla, this.operador.OperadorID, this.operador.Nombre, this.operador.Nombre, this.operador.Apellido1, this.operador.Apellido2, this.operador.Direccion, this.operador.Telefono, this.operador.Celular, this.operador.Correo, this.operador.Curp, this.operador.FechaNacimiento, this.operador.LugarNacimiento, this.operador.Alias, this.operador.Propietario.EmpresaID, this.operador.SubPropietario.SucursalID, this.operador.CorreosNotificacion, "1", this.operador.Picture.ImagenID, this.operador.EmpleadoID, "0", "");
                    Insertar.Cadena(Parametros);
                }
            }
            catch (Exception AddOperadorError)
            {
                throw new Exception("Error al intentar insertar los operadores: " + AddOperadorError);
            }
        }
        #endregion

        #region Empresa
        public void Empresa (ArrayList MemoriaTemporal)
        {

        }
        #endregion

        //Listo
        //Funcional
        #region Sucursal
        public void Sucursales( ArrayList MemoriaTemporal)
        {
            ArrayList CamposDeSucursal = null;
            int TotalDeRepeticiones = MemoriaTemporal.Count - 1;
            Convertidor ObtenerPor = new Convertidor();
            int NumeroDeFila = 0;
            try
            {
                for (int NumeroDeRepeticion = 0; NumeroDeRepeticion < TotalDeRepeticiones; NumeroDeRepeticion++)
                {
                    NumeroDeFila = NumeroDeFila + 1;
                    CamposDeSucursal = ObtenerPor.Campos(Convert.ToString(MemoriaTemporal[NumeroDeFila]));
                    this.sucursal = null;
                    this.sucursal = new Sucursal() { Base = new Empresa() };

                    //this.sucursal.SucursalID = CamposDeSucursal[0] == "NULL" ? entero : Convert.ToInt32(CamposDeSucursal[0]);
                    this.sucursal.Direccion = CamposDeSucursal[1].ToString() == "NULL" ? string.Empty : Convert.ToString(CamposDeSucursal[1]);
                    this.sucursal.Base.EmpresaID = CamposDeSucursal[2].ToString() == "NULL" ? this.configs.UserFull.EmpleadoID : Convert.ToInt32(this.configs.UserFull.EmpleadoID);
                    this.sucursal.EsActivo = CamposDeSucursal[3].ToString() == "NULL" ? true : Convert.ToBoolean(CamposDeSucursal[3]);
                    this.sucursal.Descripcion = CamposDeSucursal[5].ToString() == "NULL" ? string.Empty : Convert.ToString(CamposDeSucursal[5]);
                    this.sucursal.Contacto = CamposDeSucursal[6].ToString() == "NULL" ? string.Empty : Convert.ToString(CamposDeSucursal[6]);
                    this.sucursal.Telefono = CamposDeSucursal[7].ToString() == "NULL" ? string.Empty : Convert.ToString(CamposDeSucursal[7]);
                    this.sucursal.CodigoPostal = CamposDeSucursal[8].ToString() == "NULL" ? entero : Convert.ToInt32(CamposDeSucursal[8]);
                    this.sucursal.Responsable = CamposDeSucursal[9].ToString() == "NULL" ? string.Empty : Convert.ToString(CamposDeSucursal[9]);
                    this.sucursal.Ciudad = CamposDeSucursal[10].ToString() == "NULL" ? string.Empty : Convert.ToString(CamposDeSucursal[10]);
                    this.sucursal.Estado = CamposDeSucursal[11].ToString() == "NULL" ? string.Empty : Convert.ToString(CamposDeSucursal[11]);
                    this.sucursal.DireccionReal = CamposDeSucursal[12].ToString() == "NULL" ? string.Empty : Convert.ToString(CamposDeSucursal[12]);

                    this.sucursal = sucursalWS.InsertarCompleto(sucursal);

                    //InsertarSAP
                    /*this.sucursalsap = new SucursalSAP(this.Company);
                    this.sucursalsap.Sucursal = new Sucursal();
                    this.sucursalsap.Sucursal = this.sucursal;
                    this.sucursalsap.Sincronizado = 1;
                    this.sucursalsap.UUID = Guid.NewGuid();
                    this.sucursalsap.AddBranch();
                    this.sucursalsap.SetItemCodeBranch(this.sucursal.Direccion);
                    /*this.sucursalsap.SucursalToUDT();
                    this.sucursalsap.Insertar();*/
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        //Pendiente checar WS
        #region Empleado
        public void Empleados( ArrayList MemoriaTemporal )
        {
            ArrayList CamposDeEmpleado = null;
            int TotalDeRepeticiones = MemoriaTemporal.Count - 1;
            Convertidor ObtenerPor = new Convertidor();
            int NumeroDeFila = 0;
            //Enum Enume = "General";
            //Configurations configs = new Configurations();
            try
            {
                for (int NumeroDeRepeticion = 0; NumeroDeRepeticion <= TotalDeRepeticiones; NumeroDeRepeticion++)
                {
                    NumeroDeFila = NumeroDeFila + 1;
                    CamposDeEmpleado = ObtenerPor.Campos(MemoriaTemporal[NumeroDeFila].ToString());
                    this.empleado = null;
                    this.usuario = null;
                    this.empleado = new Empleado() {Usuario = new Usuario(),Dependencia = new Empresa(),EmpleadoSucursal = new Sucursal(),longitud = new UnidadLongitud(),volumen = new UnidadVolumen(),Configs = new Dictionary<string, string>()};
                    this.usuario = new Usuario();

                    //this.empleado.EmpleadoID = CamposDeEmpleado[0] == "NULL" ? entero : Convert.ToInt32(CamposDeEmpleado[0]);
                    this.empleado.Nombre = CamposDeEmpleado[1].ToString() == "NULL" ? string.Empty : Convert.ToString(CamposDeEmpleado[1]);
                    this.empleado.Apellido1 = CamposDeEmpleado[2].ToString() == "NULL" ? string.Empty : Convert.ToString(CamposDeEmpleado[2]);
                    this.empleado.Apellido2 = CamposDeEmpleado[3].ToString() == "NULL" ? string.Empty : Convert.ToString(CamposDeEmpleado[3]);
                    this.empleado.Telefono = CamposDeEmpleado[4].ToString() == "NULL" ? string.Empty : Convert.ToString(CamposDeEmpleado[4]);
                    this.empleado.Direccion = CamposDeEmpleado[5].ToString() == "NULL" ? string.Empty : Convert.ToString(CamposDeEmpleado[5]);
                    this.empleado.Email = CamposDeEmpleado[6].ToString() == "NULL" ? string.Empty : Convert.ToString(CamposDeEmpleado[6]);
                    this.empleado.Celular = CamposDeEmpleado[7].ToString() == "NULL" ? string.Empty : Convert.ToString(CamposDeEmpleado[7]);
                    this.empleado.Tipo = TipoEmpleado.GENERICO; //CamposDeEmpleado[8] == "NULL" ? "2" : Convert.ToString(CamposDeEmpleado[8]);
                    this.empleado.Dependencia.EmpresaID = this.configs.UserFull.EmpleadoID;
                    this.empleado.Dependencia.EsActivo = true;
                    this.empleado.Dependencia.NotificableID = this.configs.UserFull.EmpleadoID;
                    this.empleado.Usuario.UsuarioID = CamposDeEmpleado[10].ToString() == "NULL" ? entero : Convert.ToInt32(CamposDeEmpleado[10]);
                    this.empleado.Usuario.PublicKey = "1bb3eb27-b3d4-43d7-828b-9ca6c4e378d0";
                    this.empleado.Usuario.PrivateKey = "25012018061751378";
                    this.empleado.Configs.Add("America/Tegucigalpa","es-MX");
                    this.empleado.Configs = null;
                    this.empleado.longitud.UnidadLongitudID = CamposDeEmpleado[12].ToString() == "NULL" ? entero : Convert.ToInt32(CamposDeEmpleado[12]);
                    this.empleado.volumen.UnidadVolumenID = CamposDeEmpleado[13].ToString() == "NULL" ? entero : Convert.ToInt32(CamposDeEmpleado[13]);

                    this.empleado.longitud.UnidadLongitudID = CamposDeEmpleado[12].ToString() == "NULL" ? entero : Convert.ToInt32(CamposDeEmpleado[12]);
                    this.empleado.volumen.UnidadVolumenID = CamposDeEmpleado[13].ToString() == "NULL" ? entero : Convert.ToInt32(CamposDeEmpleado[13]);

                    EmpleadoToUsuario(this.empleado, this.usuario);
                    this.empleado.Usuario = this.usuario;
                    empleado = empleadoWS.Add(empleado, usuario);

                    //InsertarSAP
                    /*usuariosap.oEmpleado = this.empleado;

                    if (EsSQL)
                    {
                        usuariosap.AddEmployeeRelationSimple("[@VSKF_RELEMPLEADO]");
                    }
                    else
                    {
                        usuariosap.AddEmployeeRelationSimple(@"""@VSKF_RELEMPLEADO""");
                    }*/
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region TipoVehiculo
        public void TipoVehiculo()
        {

        }
        #endregion

        //Listo.
        //Funcional.
        #region Vehiculo
        public void Vehiculos(ArrayList MemoriaTemporal)
        {
            ArrayList CamposDeVehiculo = null;
            int TotalDeRepeticiones = MemoriaTemporal.Count - 1;
            Convertidor ObtenerPor = new Convertidor();
            int NumeroDeFila = 0;
            String NombreTalba = "";
            String VigenciaPoliza = "";
            DateTime FTemp = new DateTime();
            String TMes = "";
            String TDia = "";
            String EsActivo = "";
            try
            {
                for (int NumeroDeRepeticion = 0; NumeroDeRepeticion < TotalDeRepeticiones; NumeroDeRepeticion++)
                {
                    NumeroDeFila = NumeroDeFila + 1;
                    CamposDeVehiculo = ObtenerPor.Campos(MemoriaTemporal[NumeroDeFila].ToString());
                    this.vehiculo = null;
                    
                    this.vehiculo = new Vehiculo() { Propietario = new Empresa(), SubPropietario = new Sucursal(), TipoVehiculo = new TipoVehiculo(), EquipoRastreo = new Modem(), ImagenVehiculo = new Imagen()};

                    //this.vehiculo.VehiculoID = CamposDeVehiculo[0].ToString() == "NULL" ? entero : Convert.ToInt32(CamposDeVehiculo[0].ToString());
                    this.vehiculo.Placa = CamposDeVehiculo[1].ToString() == "NULL" ? "Placa" : Convert.ToString(CamposDeVehiculo[1].ToString());
                    this.vehiculo.Modelo = CamposDeVehiculo[2].ToString() == "NULL" ? "Modelo" : Convert.ToString(CamposDeVehiculo[2].ToString());
                    this.vehiculo.Descripcion = CamposDeVehiculo[3].ToString() == "NULL" ? "Descripción" : Convert.ToString(CamposDeVehiculo[3].ToString());
                    if (CamposDeVehiculo[4].ToString() != "NULL" & CamposDeVehiculo[4].ToString() != "") { this.vehiculo.TipoVehiculo.TipoVehiculoID = Convert.ToInt32(CamposDeVehiculo[4]); }
                    else { this.vehiculo.TipoVehiculo.TipoVehiculoID = 2652; this.vehiculo.TipoVehiculo.MantenibleID = 2652; this.vehiculo.TipoVehiculo.IAuditableID = 2652; } //Tipo de vehículo generico en caso de no definir el vehículo en el CSV.
                    if (CamposDeVehiculo[5].ToString() != "NULL" & CamposDeVehiculo[5].ToString() != "") { this.vehiculo.ImagenVehiculo.ImagenID = Convert.ToInt32(CamposDeVehiculo[5]); }
                    else { this.vehiculo.ImagenVehiculo = null; this.vehiculo.ListaImagenesVehiculos = null; }
                    //this.vehiculo.Nombre = CamposDeVehiculo[6] == "NULL" ? string.Empty : Convert.ToString(CamposDeVehiculo[6]);
                    if (CamposDeVehiculo[6].ToString() == "NULL" | CamposDeVehiculo[6].ToString() == "") { this.vehiculo.Nombre = "No definido"; }
                    else { this.vehiculo.Nombre = CamposDeVehiculo[6].ToString(); }
                    //if (CamposDeVehiculo[7].ToString() != "NULL" & CamposDeVehiculo[7].ToString() != "") { this.vehiculo.SubPropietario.SucursalID = Convert.ToInt32(CamposDeVehiculo[7]); }
                    //else { this.vehiculo.SubPropietario = null; }
                    //if (CamposDeVehiculo[8].ToString() != "NULL" & CamposDeVehiculo[8].ToString() != "") { this.vehiculo.Propietario.EmpresaID = Convert.ToInt32(CamposDeVehiculo[8]); }
                    

                    //Se define fijamente la empresa a la cual se le agregarán los vehiculos.
                    this.vehiculo.Propietario.EmpresaID = this.configs.UserFull.EmpleadoID;
                    this.vehiculo.Propietario.EsActivo = true;
                    this.vehiculo.Propietario.NotificableID = this.configs.UserFull.EmpleadoID;


                    //this.vehiculo.Nombre = CamposDeVehiculo[9] == "NULL" ? entero : Convert.ToInt32(CamposDeVehiculo[9]);
                    //this.vehiculo.Poliza = CamposDeVehiculo[10] == "NULL" ? string.Empty : Convert.ToString(CamposDeVehiculo[10]);
                    if (CamposDeVehiculo[10].ToString() == "NULL" | CamposDeVehiculo[10].ToString() == "") { this.vehiculo.Poliza = null; }
                    else { this.vehiculo.Poliza = CamposDeVehiculo[10].ToString(); }
                    if (CamposDeVehiculo[11].ToString() != "NULL" & CamposDeVehiculo[11].ToString() != "") 
                    { 
                        //this.vehiculo.VigenciaPoliza = Convert.ToDateTime(CamposDeVehiculo[11]); 
                        //FTemp;
                        //FTemp = Convert.ToDateTime("07/03/2018");
                        if (CamposDeVehiculo[11].ToString() == "7-mar.-18")
                        {
                            FTemp = Convert.ToDateTime("07/03/2018");
                        }
                        else
                        {
                            FTemp = Convert.ToDateTime(CamposDeVehiculo[11].ToString());
                        }
                        
                        if (FTemp.Month.ToString().Length <= 1)
                        {
                            TMes = "0" + FTemp.Month.ToString();
                        }
                        else
                        {
                            TMes = FTemp.Month.ToString();
                        }
                        if (FTemp.Day.ToString().Length <= 1)
                        {
                            TDia = "0" + FTemp.Day.ToString();
                        }
                        else
                        {
                            TDia = FTemp.Day.ToString();
                        }
                        VigenciaPoliza = FTemp.Year.ToString() + "/" + TMes + "/" + TDia; 
                    }
                    else 
                    { 
                        this.vehiculo.VigenciaPoliza = null;
                        if (DateTime.Today.Month.ToString().Length <= 1)
                        {
                            TMes = "0" + DateTime.Today.Month.ToString();
                        }
                        else
                        {
                            TMes = DateTime.Today.Month.ToString();
                        }
                        if (DateTime.Today.Day.ToString().Length <= 1)
                        {
                            TDia = "0" + DateTime.Today.Day.ToString();
                        }
                        else
                        {
                            TDia = DateTime.Today.Day.ToString();
                        }
                        VigenciaPoliza = DateTime.Today.Year.ToString() + "/" + TMes + "/" + TDia; 
                    }
                    if (CamposDeVehiculo[12].ToString() != "NULL" & CamposDeVehiculo[12].ToString() != "") { this.vehiculo.Plazas = Convert.ToInt32(CamposDeVehiculo[12]); }
                    if (CamposDeVehiculo[13].ToString() != "NULL" & CamposDeVehiculo[13].ToString() != "") { this.vehiculo.RendimientoCombustible = Convert.ToDouble(CamposDeVehiculo[13]); }
                    if (CamposDeVehiculo[14].ToString() != "NULL" & CamposDeVehiculo[14].ToString() != "") 
                    { 
                        if (CamposDeVehiculo[14].ToString() == "1" || CamposDeVehiculo[14].ToString() == "true" || CamposDeVehiculo[14].ToString() == "TRUE") 
                        {
                            this.vehiculo.EsActivo = true;
                            EsActivo = "1";
                        }
                        if (CamposDeVehiculo[14].ToString() == "0" || CamposDeVehiculo[14].ToString() == "false" || CamposDeVehiculo[14].ToString() == "FASLE")
                        {
                            this.vehiculo.EsActivo = false;
                            EsActivo = "0";
                        } 
                    }
                    else
                    {
                        this.vehiculo.EsActivo = true;
                        EsActivo = "1";
                    }
                    this.vehiculo.Marca = CamposDeVehiculo[15].ToString() == "NULL" ? "No Definido" : Convert.ToString(CamposDeVehiculo[15].ToString());
                    if (CamposDeVehiculo[16].ToString() != "NULL" & CamposDeVehiculo[16].ToString() != "") { this.vehiculo.Anio = Convert.ToInt32(CamposDeVehiculo[16]); }
                    else { this.vehiculo.Anio = DateTime.Today.Year; }
                    if (CamposDeVehiculo[17].ToString() != "NULL" & CamposDeVehiculo[17].ToString() != "") { this.vehiculo.EquipoRastreo.ModemID = Convert.ToInt32(CamposDeVehiculo[17]); }
                    else { this.vehiculo.EquipoRastreo = null; }
                    if (CamposDeVehiculo[18].ToString() != "NULL" & CamposDeVehiculo[18].ToString() != "") { this.vehiculo.AvisoPoliza = Convert.ToInt32(CamposDeVehiculo[18]); }
                    else { this.vehiculo.AvisoPoliza = null; }
                    if (CamposDeVehiculo[19].ToString() != "NULL" & CamposDeVehiculo[19].ToString() != "") { this.vehiculo.RendimientoCarga = Convert.ToDouble(CamposDeVehiculo[19]); }
                    //this.vehiculo.UnidadLongitudID = CamposDeVehiculo[20] == "NULL" ? entero : Convert.ToInt32(CamposDeVehiculo[20]);
                    //this.vehiculo.UnidadVolumenID = CamposDeVehiculo[21] == "NULL" ? entero : Convert.ToInt32(CamposDeVehiculo[21]);
                    //this.vehiculo.NoSerie = CamposDeVehiculo[22] == "NULL" ? entero : Convert.ToInt32(CamposDeVehiculo[22]);
                    //this.vehiculo.NoMotor = CamposDeVehiculo[23] == "NULL" ? entero : Convert.ToInt32(CamposDeVehiculo[23]);
                    //this.vehiculo.TarjetaIAVE = CamposDeVehiculo[24] == "NULL" ? entero : Convert.ToInt32(CamposDeVehiculo[24]);

                    this.vehiculo = vehiculoWS.Add(vehiculo);

                    //InsertarSAP
                    /*vehiculosap.vehiculo = null;
                    vehiculosap.vehiculo = new Vehiculo() { Propietario = new Empresa(), SubPropietario = new Sucursal(), TipoVehiculo = new TipoVehiculo(), EquipoRastreo = new Modem() };
                    vehiculosap.oUDT.Remove();
                    vehiculosap.vehiculo = this.vehiculo;
                    vehiculosap.VehiculoToUDT();
                    vehiculosap.oUDT.Code = Convert.ToString(this.vehiculo.VehiculoID);
                    vehiculosap.Insert();*/

                   /*
                    if (EsSQL)
                    {
                        NombreTalba = "[@VSKF_VEHICULO]";
                    }
                    //else
                    //{ 
                        //NombreTalba = @"""@VSKF_VEHICULO""";
                    //}
                    String Name = this.vehiculo.Placa;
                    if (this.vehiculo.Placa == "Placa")
                        Name = this.vehiculo.VehiculoID.ToString();

                    strQuery = string.Format("INSERT INTO {0} VALUES('{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}')", NombreTalba, this.vehiculo.VehiculoID, Name, this.vehiculo.VehiculoID, this.vehiculo.Placa, this.vehiculo.Modelo, this.vehiculo.Descripcion, this.vehiculo.TipoVehiculo.TipoVehiculoID, this.vehiculo.ImagenVehiculo.ImagenID, this.vehiculo.Placa, this.vehiculo.SubPropietario.SucursalID, this.vehiculo.Propietario.EmpresaID, this.vehiculo.NumeroEconomico, this.vehiculo.Poliza, VigenciaPoliza.ToString(), this.vehiculo.Plazas, this.vehiculo.RendimientoCombustible, EsActivo.ToString(), this.vehiculo.Marca, this.vehiculo.Anio, this.vehiculo.EquipoRastreo.ModemID, this.vehiculo.AvisoPoliza, this.vehiculo.RendimientoCarga, "", "", "0", "");

                    Insertar.Cadena(strQuery);
                    */
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Proveedor
        public void Proveedor()
        {

        }
        #endregion

        #region CargaCombustible
        public string AddRecargaCombus(CargaCombustible oRecarga, string sCode)
        {           

            string sResponse = "ERROR";
            int iCommand = 0, iCode = 0;
            UserTable oTabla = Company.UserTables.Item("VSKF_CRGCOMBUSTIBLE");
            oTabla.Code = sCode;
            oTabla.Name = sCode;
            oTabla.UserFields.Fields.Item("U_Litros").Value = Convert.ToDouble( oRecarga.Litros);
            oTabla.UserFields.Fields.Item("U_Pesos").Value = Convert.ToDouble(oRecarga.Pesos);
            oTabla.UserFields.Fields.Item("U_VehiculoID").Value = oRecarga.Movil.VehiculoID;
            oTabla.UserFields.Fields.Item("U_Fecha").Value = Convert.ToDateTime( oRecarga.Fecha);
            oTabla.UserFields.Fields.Item("U_HoraMinuto").Value = Convert.ToDateTime(oRecarga.Fecha).ToString("HH:mm");
            oTabla.UserFields.Fields.Item("U_FechaCaptura").Value = DateTime.Now;
            oTabla.UserFields.Fields.Item("U_Ticket").Value = oRecarga.Ticket;
            oTabla.UserFields.Fields.Item("U_TipoCombustible").Value = Convert.ToInt32(oRecarga.TypeFuel.TipoCombustibleID);
            oTabla.UserFields.Fields.Item("U_ProveedorID").Value = Convert.ToInt32( oRecarga.SourceCenter.ProveedorID);
            oTabla.UserFields.Fields.Item("U_Odometro").Value = Convert.ToDouble( oRecarga.Odometro);
            oTabla.UserFields.Fields.Item("U_FormaPago").Value = Convert.ToInt32(oRecarga.TipoPagoID);
            oTabla.UserFields.Fields.Item("U_DetallePago").Value = oRecarga.DetallePago;
            oTabla.UserFields.Fields.Item("U_CentroServicio").Value = oRecarga.CentroServicio;
            oTabla.UserFields.Fields.Item("U_ULongitudID").Value = Convert.ToInt32( oRecarga.Longitud.UnidadLongitudID);
            oTabla.UserFields.Fields.Item("U_UVolumenID").Value = Convert.ToInt32(oRecarga.Volumen.UnidadVolumenID);
            oTabla.UserFields.Fields.Item("U_UUID").Value = Guid.NewGuid().ToString(); 
            oTabla.UserFields.Fields.Item("U_TipoMonedaID").Value = Convert.ToInt32( oRecarga.Moneda.TipoMonedaID);
            oTabla.UserFields.Fields.Item("U_Latitude").Value = Convert.ToDouble( oRecarga.Latitude);
            oTabla.UserFields.Fields.Item("U_Longitude").Value = Convert.ToDouble( oRecarga.Longitude);
            oTabla.UserFields.Fields.Item("U_Facturable").Value = oRecarga.EsFacturableSAP ?? 0;
            iCommand = oTabla.Add();
            if (iCommand != 0)
                Company.GetLastError(out iCode, out sResponse);
            else sResponse = "OK";
            return sResponse;

        }
        #endregion

        //Listo.
        //Funcional.
        #region Llnatas
        public long recuperIDMaximoTabla(string sTabla)
        {
            long iMaximo = -1;
            string sNombreFomatTabla = string.Format(GestionGlobales.bSqlConnection ? "[@{0}]" : "@{0}", sTabla);
            string sql = string.Format(GestionGlobales.bSqlConnection ? @"SELECT MAX(CONVERT(BIGINT,""Code"")) AS ""Code"" FROM {0}" : @"SELECT MAX(TO_INTEGER (""Code"")) AS ""Code"" FROM ""{0}""", sNombreFomatTabla);
            Recordset oRecord = Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            oRecord.DoQuery(sql);
            if (oRecord.RecordCount > 0)
            {
                try
                {
                    iMaximo = Convert.ToInt64(oRecord.Fields.Item("Code").Value);
                    iMaximo++;
                }
                catch {
                    iMaximo = 0;
                }
            }
            else iMaximo = 1;
            return iMaximo;
        }

        public void LimpiaTablaCode(string sTabla, string sCode, string sColumna)
        {
            string sNombreFomatTabla = string.Format(GestionGlobales.bSqlConnection ? "[@{0}]" : "@{0}", sTabla);
            string sql = string.Format(@"DELETE FROM ""@{0}"" WHERE {1} = {2}", sTabla, sColumna, sCode);
            Recordset oRecord = Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            oRecord.DoQuery(sql);
            string sresponse = oRecord.RecordCount > 0 ? "OK" : "SIN RESULTADOS";
        }

        public string AddLlantas(Llanta oLlanta, string sCode, string sName)
        {           

            string sResponse = "ERROR";
            int iCommand = 0, iCode = 0;
            UserTable oTabla = Company.UserTables.Item("VSKF_LLANTA");
            oTabla.Code = sCode;
            oTabla.Name = sName;
            oTabla.UserFields.Fields.Item("U_ItemCode").Value = oLlanta.ItemCode;
            oTabla.UserFields.Fields.Item("U_NumEconomico").Value = oLlanta.NumeroEconomico;
            oTabla.UserFields.Fields.Item("U_NumRevestimientos").Value = oLlanta.NumeroRevestimientos;
            oTabla.UserFields.Fields.Item("U_SN").Value = oLlanta.SN;
            oTabla.UserFields.Fields.Item("U_Marca").Value = oLlanta.Marca;
            oTabla.UserFields.Fields.Item("U_Modelo").Value = oLlanta.Modelo;
            oTabla.UserFields.Fields.Item("U_NumCapas").Value = oLlanta.NumeroCapas;
            oTabla.UserFields.Fields.Item("U_Costo").Value = Convert.ToDouble( oLlanta.Costo);
            oTabla.UserFields.Fields.Item("U_ClaseRotacion").Value = oLlanta.ClaseRotacion;
            oTabla.UserFields.Fields.Item("U_ProveedorLlanta").Value = Convert.ToInt32( oLlanta.proveedorLlanta.ProveedorID);
            oTabla.UserFields.Fields.Item("U_NumeroFactura").Value = oLlanta.NumeroFactura;
            oTabla.UserFields.Fields.Item("U_FechaCompra").Value = Convert.ToDateTime( oLlanta.FechaCompra);
            oTabla.UserFields.Fields.Item("U_EsActivo").Value = 1;
            oTabla.UserFields.Fields.Item("U_Observaciones").Value = oLlanta.Observaciones;
            oTabla.UserFields.Fields.Item("U_KmsRecorridos").Value = Convert.ToDouble(oLlanta.KilometrosRecorridos);
            oTabla.UserFields.Fields.Item("U_Propietario").Value = Convert.ToInt32( oLlanta.Propietario.EmpresaID);
            oTabla.UserFields.Fields.Item("U_EstaInstalado").Value = Convert.ToInt32(oLlanta.EstaInstalado);
            oTabla.UserFields.Fields.Item("U_SucursalID").Value = Convert.ToInt32( oLlanta.SucursalID);
            oTabla.UserFields.Fields.Item("U_Uso").Value = Convert.ToInt32( oLlanta.Uso);
            oTabla.UserFields.Fields.Item("U_Ancho").Value = oLlanta.Ancho;
            oTabla.UserFields.Fields.Item("U_Aspecto").Value = oLlanta.Aspecto;
            oTabla.UserFields.Fields.Item("U_Construccion").Value = Convert.ToInt32( oLlanta.Construccion);
            oTabla.UserFields.Fields.Item("U_DiametroRin").Value = oLlanta.DiametroRin;
            oTabla.UserFields.Fields.Item("U_IndiceCarga").Value = oLlanta.IndiceCarga;
            oTabla.UserFields.Fields.Item("U_RangoVelocidad").Value = Convert.ToInt32( oLlanta.RangoVelocidad);
            oTabla.UserFields.Fields.Item("U_Aplicacion").Value = Convert.ToInt32( oLlanta.Aplicacion);
            oTabla.UserFields.Fields.Item("U_Traccion").Value = Convert.ToInt32( oLlanta.Traccion);
            oTabla.UserFields.Fields.Item("U_Treadwear").Value = oLlanta.Treadwear;
            oTabla.UserFields.Fields.Item("U_Temperatura").Value = Convert.ToInt32( oLlanta.Temperatura);
            oTabla.UserFields.Fields.Item("U_MaxiPresionInflado").Value = oLlanta.MaximaPresionInflado;
            oTabla.UserFields.Fields.Item("U_MinPresionInflado").Value = oLlanta.MinimaPresionInflado;
            oTabla.UserFields.Fields.Item("U_FechaFabricacion").Value = Convert.ToDateTime( oLlanta.FechaFabricacion);
            oTabla.UserFields.Fields.Item("U_Sincronizado").Value = 0;
            oTabla.UserFields.Fields.Item("U_UUID").Value = Guid.NewGuid().ToString();
            oTabla.UserFields.Fields.Item("U_KmCaducidad").Value = Convert.ToDouble( oLlanta.KilometrosCaducidad);
            oTabla.UserFields.Fields.Item("U_FechaCaducidad").Value = Convert.ToDateTime( oLlanta.FechaCaducidad);
            oTabla.UserFields.Fields.Item("U_WhsCode").Value = oLlanta.CodigoAlmacen;
            iCommand = oTabla.Add();
            if (iCommand != 0)
                Company.GetLastError(out iCode, out sResponse);
            else sResponse = "OK";
            return sResponse;

        }
        #endregion

        //Listo
        #region Inventario.Articulo
        public void InventarioArticulos(ArrayList MemoriaTemporal)
        {
            ArrayList CamposDeLlantasArticulo = null;
            String NombreTalba = "[@VSKF_LLANTA]";
            int TotalDeRepeticiones = MemoriaTemporal.Count - 1;
            Convertidor ObtenerPor = new Convertidor();
            int NumeroDeFila = 0;
            String FechaCompra = "";
            String FechaFabricacion = "";
            DateTime FTempCompra = new DateTime();
            DateTime FTempFabricacion = new DateTime();
            String EstaActivo = "1";
            String EstaInstalado = "";
            String Sincronizado = "0";
            String TMes = "";
            String TDia = "";
            try
            {
                for (int NumeroDeRepeticion = 0; NumeroDeRepeticion < TotalDeRepeticiones; NumeroDeRepeticion++)
                {
                    NumeroDeFila = NumeroDeFila + 1;
                    CamposDeLlantasArticulo = ObtenerPor.Campos(MemoriaTemporal[NumeroDeFila].ToString());
                    this.llanta = null;
                    this.llanta = new Llanta() { proveedorLlanta = new Proveedor(), SubPropietario = new Sucursal() };

                    //this.llanta.LlantaID = CamposDeLlantasArticulo[0] == "NULL" ? entero : Convert.ToInt32(CamposDeLlantasArticulo[0]);
                    if (CamposDeLlantasArticulo[16].ToString() != "NULL" & CamposDeLlantasArticulo[16].ToString() != "") { this.llanta.Propietario = (Empresa)CamposDeLlantasArticulo[16]; }


                        //Se insertaestaticamente el valor del la empresala a la cual se la insertaran los registro de las llantas.


                    else { this.llanta.Propietario = new Empresa() { EmpresaID = this.configs.UserFull.EmpleadoID }; }
                    this.llanta.SN = CamposDeLlantasArticulo[2].ToString() == "NULL" ? string.Empty : Convert.ToString(CamposDeLlantasArticulo[2]);
                    this.llanta.NumeroEconomico = CamposDeLlantasArticulo[3].ToString() == "NULL" ? string.Empty : Convert.ToString(CamposDeLlantasArticulo[3]);
                    this.llanta.Observaciones = CamposDeLlantasArticulo[4].ToString() == "NULL" ? string.Empty : Convert.ToString(CamposDeLlantasArticulo[4]);
                    if (CamposDeLlantasArticulo[18].ToString() != "NULL" & CamposDeLlantasArticulo[18].ToString() != "") { this.llanta.SubPropietario.SucursalID = Convert.ToInt32(CamposDeLlantasArticulo[18]); }

                    this.llanta = llantasWS.Add(llanta);

                    //InsertarSAP
                    /*llantasap.llanta = this.llanta;
                    llantasap.LlantaToUDT();
                    llantasap.Insert();*/

                    if (EsSQL)
                    {
                        NombreTalba = "[@VSKF_CONFIGURACION]";
                    }
                    else
                    {
                        NombreTalba = @"""@VSKF_LLANTA""";
                    }

                    strQuery = string.Format("INSERT INTO {0} VALUES('{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}','{32}','{33}','{34}','{35}','{36}')", NombreTalba, this.llanta.LlantaID, this.llanta.NumeroEconomico, this.llanta.LlantaID, this.llanta.NumeroEconomico, this.llanta.NumeroRevestimientos, this.llanta.SN, this.llanta.Marca, this.llanta.Modelo, this.llanta.NumeroCapas, this.llanta.Costo, this.llanta.Traccion, this.llanta.proveedorLlanta.ProveedorID, this.llanta.NumeroFactura, FechaCompra, EstaActivo, this.llanta.Observaciones, "0" /*Se le asigna el numero cero porque no se define el kilometraje recorrido en el CSV.*/, this.llanta.Propietario.EmpresaID, EstaInstalado, this.llanta.SubPropietario.SucursalID, this.llanta.Uso, this.llanta.Ancho, this.llanta.Aspecto, this.llanta.Construccion, this.llanta.DiametroRin, this.llanta.IndiceCarga, this.llanta.RangoVelocidad, this.llanta.Aplicacion, this.llanta.Traccion, this.llanta.Treadwear, this.llanta.Temperatura, this.llanta.MaximaPresionInflado, this.llanta.MinimaPresionInflado, FechaFabricacion, Sincronizado, "");

                    Insertar.Cadena(strQuery);

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Desplazamiento
        public void Desplazamiento()
        {

        }
        #endregion

        #region Activo
        public void Activo()
        {
            
        }
        #endregion

        #region Otros
        public void Otros()
        {

        }

        public string pathOperadores()
        {
            string sPath = string.Empty;
            string sql = @"SELECT ""AttachPath"" FROM OADP";
            Recordset oRecord = Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            oRecord.DoQuery(sql);
            if (oRecord.RecordCount > 0)
                sPath = Convert.ToString(oRecord.Fields.Item("AttachPath").Value);
            return sPath;
        }

        public string pathInfraccion()
        {
            string sPath = string.Empty;
            string sql = @"SELECT ""AttachPath"" FROM OADP";
            Recordset oRecord = Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            oRecord.DoQuery(sql);
            if (oRecord.RecordCount > 0)
                sPath = Convert.ToString(oRecord.Fields.Item("AttachPath").Value);
            return sPath;
        }

        #endregion

        #region Métodos auxiliares
        private void OperadorToEmpleado(Operador op, Empleado oEmpleado)
        {
            oEmpleado.Nombre = op.Nombre;
            oEmpleado.Apellido1 = op.Apellido1;
            oEmpleado.Apellido2 = op.Apellido2;
            oEmpleado.Telefono = op.Telefono;
            oEmpleado.EmpleadoID = op.EmpleadoID;
            oEmpleado.Email = op.Correo;
            oEmpleado.Direccion = op.Direccion;
            oEmpleado.Dependencia = op.Propietario;
            oEmpleado.Tipo = TipoEmpleado.GENERICO;
            op.EsActivo = true;
            oEmpleado.longitud = new UnidadLongitud() { UnidadLongitudID = 1 };
            oEmpleado.volumen = new UnidadVolumen() { UnidadVolumenID = 1 };
            oEmpleado.Celular = op.Celular;
            oEmpleado.EmpleadoSucursal = op.SubPropietario == null ? new Sucursal() : op.SubPropietario;
        }

        private void EmpleadoToOperador(Empleado oempleado, Operador op)
        {
            this.operador.Nombre = oempleado.Nombre;
            this.operador.Apellido1 = oempleado.Apellido1;
            this.operador.Apellido2 = oempleado.Apellido2;
            this.operador.Telefono = oempleado.Telefono;
            this.operador.EmpleadoID = oempleado.EmpleadoID;
            this.operador.Correo = oempleado.Email;
            this.operador.Direccion = oempleado.Direccion;
            this.operador.Propietario = oempleado.Dependencia;
            this.operador.EsActivo = true;
            this.operador.Celular = oempleado.Celular;
            this.operador.SubPropietario = oempleado.EmpleadoSucursal == null ? new Sucursal() : oempleado.EmpleadoSucursal;
            this.operador.Picture = new Imagen();
        }

        private void EmpleadoToUsuario( Empleado em, Usuario us)
        {
            //Se asigna un password como Default para poder registrar el usuario.
            this.usuario.Clave = "Aa123456.";
            this.usuario.Estado = EstadoUsuario.Active;
            this.usuario.Nombre = em.Email;
            this.usuario.UsuarioID = null;
            this.usuario.PrivateKey = "25012018061751378";
            this.usuario.PublicKey = "1bb3eb27-b3d4-43d7-828b-9ca6c4e378d0";
            this.usuario.CambioContraseña = "";
        }
        #endregion
    }
}
