﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SAPbobsCOM;

namespace KananSAP.GestionDatos.MigradorDatos.Librerias
{
    public class InsertString
    {
        Company Company;
        Recordset Insertar;
        public InsertString(Company company)
        {
            try
            {
                this.Company = company;
                Insertar = (Recordset)this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Cadena(String Query)
        {
            try
            {
                Insertar.DoQuery(Query);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
