﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.GestionDatos.MigradorDatos.Librerias
{
    public class Convertidor
    {
        public ArrayList Campos(string fila)
        {
            try
            {
                ArrayList DatosDeOperador = new ArrayList();
                //String[] DatosTemporales = null;
                string Campo = "";
                Char[] CaracteresDeLaFila = fila.ToArray();
                string Caracter = "";
                int ContadorArreglo = 0;

                for (int i = 0; i < CaracteresDeLaFila.Count(); i++)
                {
                    Caracter = Convert.ToString(CaracteresDeLaFila[i]);
                    if (Caracter != ";")
                    {
                        Campo += CaracteresDeLaFila[i];
                    }
                    if (Caracter == ";")
                    {
                        DatosDeOperador.Add(Campo);
                        ContadorArreglo = ContadorArreglo + 1;
                        Campo = "";
                    }
                }
                //ContadorArreglo = ContadorArreglo + 1;
                DatosDeOperador.Add(Campo);
                return DatosDeOperador;
            }
            catch (Exception ConvertidorFilaError)
            {
                throw new Exception("Error al intentar extraer las filas: " + ConvertidorFilaError);
            }
        }
    }
}
