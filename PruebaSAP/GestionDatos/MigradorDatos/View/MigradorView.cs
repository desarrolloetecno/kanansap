﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbouiCOM;
using KananWS.Interface;
using Application = System.Windows.Forms.Application;
using KananSAP.Helper;
using Kanan.Llantas.BO2;
using KananSAP.GestionDatos.MigradorDatos.Librerias;
using System.IO;
using SAPbobsCOM;
using System.Threading;
using Kanan.Vehiculos.BO2;
using SAPinterface.Data.PD;

/*
 * -------Autor-------
 * ----Rair Santos----
 */

namespace KananSAP.GestionDatos.MigradorDatos.View
{
    public class MigradorView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.StaticText oStaticText;
        private SAPbouiCOM.Button oButton;
        private SAPbouiCOM.CheckBox oCheckBox;
        public string FormUniqueID { get; set; }
        private XMLPerformance XmlApplication;
        private GestionGlobales oGlobales = null;
        private List<Llanta> lstLlantas = null;
        private List<CargaCombustible> lstRecargaComb = null;
        private SAPbobsCOM.Company oCompany;
        public int val = 0;
        private string sMoodForm = string.Empty;
        const string CONST_CLASE = "MigradorView.cs";
        private string sFolio = string.Empty;
        private int? VehiculoID = -1;
        #endregion

        #region Constructor
        public MigradorView(GestionGlobales oGlobales, SAPbobsCOM.Company oCompany, string sMoodForm, string UIID, int? VehiculoID)
        {
            this.VehiculoID = VehiculoID;
            this.sFolio = UIID;
            this.sMoodForm = sMoodForm;
            this.oGlobales = oGlobales;
            this.SBO_Application = this.oGlobales.SBO_Application;
            this.XmlApplication = new XMLPerformance(SBO_Application);
            this.oCompany = oCompany;
            this.LoadForm();
            this.ShowForm();
            
        }
        #endregion

        #region Métodos
        public void LoadForm()
        {
            FormCreationParams oFormCreate = null;
            String XmlData = "";
            try
            {
                this.oForm = SBO_Application.Forms.Item(string.Format("migrador{0}", sFolio));
            }
            catch(Exception ex)
            {
                KananSAPHerramientas.LogError("GestionConfig.cs->LoadForm()", ex.Message);
                try
                {
                    string sPath = Application.StartupPath;                   
                    XmlData = this.XmlApplication.LoadFromXML(sPath + "\\XML", "", "MigradorDatos.xml");
                    oFormCreate = (FormCreationParams)this.oGlobales.SBO_Application.CreateObject(BoCreatableObjectType.cot_FormCreationParams);
                    oFormCreate.UniqueID = string.Format("migrador{0}", sFolio);
                    oFormCreate.FormType = "migrador";
                    oFormCreate.XmlData = XmlData;
                    this.oForm = this.oGlobales.SBO_Application.Forms.AddEx(oFormCreate);//SBO_Application.Forms.Item(UID);
                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;

                }
                catch (Exception oex)
                {
                    
                    KananSAPHerramientas.LogError("GestionConfig.cs->LoadForm()", oex.Message);
                }
            }
        }
        
        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
                this.InitializeComponents();
            }
            catch (Exception)
            {
                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 4;
                this.oForm.Visible = true;
                this.InitializeComponents();
                this.oForm.Select();
            }
        }
        
        public void InitializeComponents()
        {
            try
            {
                oItem = oForm.Items.Item("txt");
                oEditText = (EditText)(oItem.Specific);
                oEditText.String = string.Format(@"Para realizar una carga masiva de datos, el archivo adjunto debe contar con las siguientes características:{0}1.- Solo se permiten archivos de tipo csv & txt.{0}2.- Las columnas deben estar delimitadas por coma (,).{0}3.- Los registos (filas) deben estar separados por salto de linea.", Environment.NewLine);

                oItem = oForm.Items.Item("Ruta");
                oEditText = (EditText)(oItem.Specific);

                oItem = oForm.Items.Item("Guardar");
                oButton = (Button)(oItem.Specific);
                oButton.Caption = "Guardar";

                switch (sMoodForm)
                {
                    case "recargas":
                        oForm.Title = "Importar archivo de recargas de combustible";
                        oItem = oForm.Items.Item("MtxReCom");
                        break;

                    case "llantas":
                        oForm.Title = "Importar archivo de llantas";
                        oItem = oForm.Items.Item("MtxFR");
                        break;

                    default:
                        break;
                }
                oItem.Visible = true;
                oCheckBox.Item.Enabled = false;
            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError(string.Format("{0}->InitializeComponents", CONST_CLASE), ex.Message);
            }
        }

        public void EventoControl(ItemEvent pVal, out bool BubbleEvent)
        {
            #region Comentarios
            /* 
             */
            #endregion Comentarios

            BubbleEvent = true;
            try
            {

                if (!pVal.BeforeAction)
                {
                    #region After Action
                    switch (pVal.ItemUID)
                    {
                        case "Buscar":
                            this.BuscarArchivo();
                            break;
                        case "Leer":
                            this.LeerArchivo();
                            break;
                        case "Guardar":
                            this.GuardarArchivo();
                            break;
                    }
                    #endregion After Action
                }
            }
            catch (Exception ex)
            {
                this.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                this.oGlobales.Mostrarmensaje(string.Format("No se ha podido sincronizar la informacion {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                KananSAPHerramientas.LogError(string.Format("{0}->EventoControl()", CONST_CLASE), ex.Message);
            }
        }
        
         
        
        public string GetRuta()
        {
            try
            {
                string ruta;
                oItem = oForm.Items.Item("Ruta");
                oEditText = (EditText)(oItem.Specific);
                ruta = oEditText.Value;
                return ruta;
            }
            catch(Exception ex)
            {
                return null;
                //throw new Exception(ex.Message);
            }
        }
        
        public void SetRuta( string setRuta)
        {
            try
            {
                oItem = oForm.Items.Item("Ruta");
                oEditText = (EditText)(oItem.Specific);
                oEditText.Value = setRuta;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region BuscarArchivo
        public string BuscarArchivo()
        {
            string path = "";
            try
            {
                Thread t = new Thread((ThreadStart)delegate
                {
                    System.Windows.Forms.FileDialog objDialog = new System.Windows.Forms.OpenFileDialog();
                    objDialog.Filter = "csv files (*.csv)|*.csv|txt files (*.txt)|*.txt|All files (*.*)|*.*";

                    //create a Form for ownership (SAP-Window won't work)
                    System.Windows.Forms.Form g = new System.Windows.Forms.Form();
                    g.Width = 1;
                    g.Height = 1;
                    g.Activate();
                    g.BringToFront();
                    g.Visible = true;
                    g.TopMost = true;
                    g.Focus();

                    System.Windows.Forms.DialogResult objResult = objDialog.ShowDialog(g);
                    Thread.Sleep(100);
                    if (objResult == System.Windows.Forms.DialogResult.OK)
                    {
                        path = objDialog.FileName;
                    }
                })
                {
                    IsBackground = false,
                    Priority = ThreadPriority.Highest
                };
                t.SetApartmentState(ApartmentState.STA);
                t.Start();
                while (!t.IsAlive) ;
                Thread.Sleep(1);
                t.Join();

                
                if (!String.IsNullOrEmpty(path))
                {
                    if (File.Exists(path))
                    {
                        this.SetRuta(path);
                        FileInfo oFile = new FileInfo(path);
                        if (oFile.Extension == ".csv" || oFile.Extension == ".txt")
                            this.LeerArchivo();
                        else SBO_Application.MessageBox("El archivo no es de tipo (CSV o TXT) no cumple con los requisitos para la lectura, favor de adjuntar un archivo con las extensiones requeridas.", 1, "OK");
                    }
                    else SBO_Application.MessageBox("No se ha encontrado la información del archivo", 1, "OK");
                }
            }
            catch (Exception SeleccionarArchivoError)
            {

                throw new Exception("Error al seleccionar el archivo: " + SeleccionarArchivoError);
            }
            return path;
        }
        #endregion

        #region LeerArchivo
        public void LeerArchivo()
        {
            try
            {
                
                string sFileName = this.GetRuta();
                if (!string.IsNullOrEmpty(sFileName))
                {
                    if (File.Exists(sFileName))
                    {
                        StreamReader Streamdata = new StreamReader(sFileName);
                        switch (this.sMoodForm)
                        {
                            case "recargas":
                                this.ProcesaLecturaRecargasCombustible(ref Streamdata);
                                break;
                            case "llantas":
                                this.ProcesaLecturaLlantas(ref Streamdata);
                                break;
                        }


                    }
                    else SBO_Application.MessageBox("El archivo elegido no existe favor de agregar un archivo existente", 1, "OK");
                }
                else SBO_Application.MessageBox("El archivo no se encuentra cargado, favor de adjuntarlo y posterior iniciar el proceso de carga", 1, "OK");
            }
            catch (Exception LeerArchivoError)
            {
                this.oGlobales.Mostrarmensaje(string.Format("No se ha podido sincronizar la informacion {0}", LeerArchivoError.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                throw new Exception("Error al leer el archivo: " + LeerArchivoError);
            }
        }
        #endregion



        #region GuardarArchivo
        public void GuardarArchivo()
        {
            try
            {

                Add oConnfigAdd = new Add(ref this.oCompany, null);
                switch (sMoodForm)
                {
                    case "recargas":
                        this.GuardaRecargasaUDT(ref oConnfigAdd);
                        break;
                    case "llantas":
                        this.GuardaLlantaUDT(ref oConnfigAdd);
                        break;
                }
            }
            catch (Exception ex)
            {
                this.oGlobales.Mostrarmensaje(string.Format("No se ha podido sincronizar la informacion {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                this.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                KananSAPHerramientas.LogError("MigradorPresenter.cs->GuardarArchivo()", ex.Message);
            }
        }
        #endregion
        //

        #region Gestiona carga de llantas masivas
        void ProcesaLecturaLlantas(ref StreamReader Steamdata)
        {
            lstLlantas = new List<Llanta>();
            String sLine = string.Empty;
            while (sLine != null)
            {
                sLine = Steamdata.ReadLine();
                if (!string.IsNullOrEmpty(sLine))
                {
                    var oLine = sLine.Split(',');
                    if (oLine.Length < 33)
                        continue;
                    Llanta oTemp = new Llanta();
                    oTemp.ItemCode = Convert.ToString(oLine[0]);
                    oTemp.NumeroEconomico = Convert.ToString(oLine[1]);
                    oTemp.NumeroRevestimientos = Convert.ToInt32(oLine[2]);
                    oTemp.SN = Convert.ToString(oLine[3]);
                    oTemp.Marca = Convert.ToString(oLine[4]);
                    oTemp.Modelo = Convert.ToString(oLine[5]);
                    oTemp.NumeroCapas = Convert.ToInt32(oLine[6]);
                    oTemp.Costo = Convert.ToDecimal(oLine[7]);
                    oTemp.ClaseRotacion = Convert.ToString(oLine[8]);
                    oTemp.proveedorLlanta = new Kanan.Costos.BO2.Proveedor();
                    oTemp.proveedorLlanta.ProveedorID = Convert.ToInt32(oLine[9]);
                    oTemp.NumeroFactura = Convert.ToString(oLine[10]);
                    oTemp.FechaCompra = Convert.ToDateTime(oLine[11]);
                    oTemp.Observaciones = Convert.ToString(oLine[12]);
                    oTemp.KilometrosRecorridos = Convert.ToDouble(oLine[13]);
                    oTemp.Propietario = new Kanan.Operaciones.BO2.Empresa();
                    oTemp.Propietario.EmpresaID = Convert.ToInt32(oLine[14]);
                    oTemp.EstaInstalado = Convert.ToBoolean(Convert.ToUInt32(oLine[15]));
                    oTemp.SucursalID = Convert.ToString(oLine[16]);
                    oTemp.Uso = Convert.ToInt32(oLine[17]);
                    oTemp.Ancho = Convert.ToString(oLine[18]);
                    oTemp.Aspecto = Convert.ToString(oLine[19]);
                    oTemp.Construccion = Convert.ToInt32(oLine[20]);
                    oTemp.DiametroRin = Convert.ToString(oLine[21]);
                    oTemp.IndiceCarga = Convert.ToString(oLine[22]);
                    oTemp.RangoVelocidad = Convert.ToInt32(oLine[23]);
                    oTemp.Aplicacion = Convert.ToInt32(oLine[24]);
                    oTemp.Traccion = Convert.ToInt32(oLine[25]);
                    oTemp.Treadwear = Convert.ToString(oLine[26]);
                    oTemp.Temperatura = Convert.ToInt32(oLine[27]);
                    oTemp.MaximaPresionInflado = Convert.ToString(oLine[28]);
                    oTemp.MinimaPresionInflado = Convert.ToString(oLine[29]);
                    oTemp.FechaFabricacion = Convert.ToDateTime(oLine[30]);
                    oTemp.KilometrosCaducidad = Convert.ToDecimal(oLine[31]);
                    oTemp.FechaCaducidad = Convert.ToDateTime(oLine[32]);
                    oTemp.CodigoAlmacen = Convert.ToString(oLine[33]);
                    oTemp.EstatusSinc = "Pendiente de replicar";
                    lstLlantas.Add(oTemp);
                }
            }
            this.LlenaTablaLlantas(lstLlantas);
        }
        void GuardaLlantaUDT(ref Add oConnfigAdd)
        {
            if (lstLlantas != null)
            {
                if (lstLlantas.Count > 0)
                {
                    LlantasWS llantasWS = new LlantasWS(Guid.Parse("1bb3eb27-b3d4-43d7-828b-9ca6c4e378d0"), "25012018061751378");
                    this.oCompany.StartTransaction();
                    long iCodigomx = oConnfigAdd.recuperIDMaximoTabla("VSKF_LLANTA");
                    if (iCodigomx > 0)
                    {
                        foreach (Llanta odata in lstLlantas)
                        {
                            string sName = string.Format("LP{0}", iCodigomx);
                            try
                            {
                                llantasWS.Add(odata);
                            }
                            catch { }
                            string sresponse = oConnfigAdd.AddLlantas(odata, iCodigomx.ToString(), sName);
                            if (sresponse != "OK")
                                throw new Exception(string.Format("Uno o más registros no se han podido crear, se ha omitido el proceso de carga de llantas. ERROR {0}", sresponse));
                            odata.EstatusSinc = "Registro sincronizado a SAP";
                            iCodigomx++;
                        }
                        this.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                        this.LlenaTablaLlantas(lstLlantas);
                        lstLlantas = new List<Llanta>();
                        this.oGlobales.Mostrarmensaje("La información de llantas se ha sincronizado", BoMessageTime.bmt_Long, BoStatusBarMessageType.smt_Success);
                    }
                    else SBO_Application.MessageBox("No se ha podido recuperar el ultimo registro de llantas", 1, "OK");
                }
                else SBO_Application.MessageBox(string.Format(@"No se ha encontrado información en el archivo, asegúrate que todas las columnas tengan datos y cumpla con los lineamientos solicitados", Environment.NewLine), 1, "OK");
            }
            else SBO_Application.MessageBox(string.Format(@"No se ha encontrado información en el archivo, asegúrate que todas las columnas tengan datos y cumpla con los lineamientos solicitados", Environment.NewLine), 1, "OK");
        }
        void LlenaTablaLlantas(List<Llanta> lstLlantas)
        {
            try
            {
                //this.oForm = SBO_Application.Forms.Item("migrador");
                Matrix oMatrixPorte = (Matrix)this.oForm.Items.Item("MtxFR").Specific;
                Columns oColum = (SAPbouiCOM.Columns)oMatrixPorte.Columns;
                oMatrixPorte.Clear();
                if (lstLlantas != null)
                {
                    int iLinea = 1;
                    foreach (Llanta oLlanta in lstLlantas)
                    {
                        this.oForm.DataSources.UserDataSources.Item("oUMAT01").Value = iLinea.ToString();
                        this.oForm.DataSources.UserDataSources.Item("oUMAT02").Value = oLlanta.NumeroEconomico;
                        this.oForm.DataSources.UserDataSources.Item("oUMAT03").Value = oLlanta.SN;
                        this.oForm.DataSources.UserDataSources.Item("oUMAT04").Value = oLlanta.Marca;
                        this.oForm.DataSources.UserDataSources.Item("oUMAT05").Value = oLlanta.Modelo;
                        this.oForm.DataSources.UserDataSources.Item("oUMAT06").Value = oLlanta.NumeroRevestimientos.ToString();
                        this.oForm.DataSources.UserDataSources.Item("oUMAT07").Value = oLlanta.Costo.ToString();
                        this.oForm.DataSources.UserDataSources.Item("oUMAT08").Value = oLlanta.NumeroFactura;
                        this.oForm.DataSources.UserDataSources.Item("oUMAT09").Value = Convert.ToDateTime(oLlanta.FechaCompra).ToString("dd/MM/yyyy");
                        this.oForm.DataSources.UserDataSources.Item("oUMAT10").Value = oLlanta.EstatusSinc;
                        iLinea++;
                        oMatrixPorte.AddRow(1, -1);
                    }
                }
            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError(string.Format("{0}->LlenaTablaLlantas()", CONST_CLASE), ex.Message);
            }
        }
        #endregion Gestiona carga de llantas masivas

        #region Gestiona carga de combustible masivo
        void GuardaRecargasaUDT(ref Add oConnfigAdd)
        {
            string sresponse = string.Empty;
            if (lstRecargaComb != null)
            {
                if (lstRecargaComb.Count > 0)
                {
                    this.oCompany.StartTransaction();
                    long iCodigomx = oConnfigAdd.recuperIDMaximoTabla("VSKF_CRGCOMBUSTIBLE");
                    if (iCodigomx > 0)
                    {
                        foreach (CargaCombustible odata in lstRecargaComb)
                        {

                            sresponse = oConnfigAdd.AddRecargaCombus(odata, iCodigomx.ToString());
                            if (sresponse != "OK")
                                throw new Exception(string.Format("Uno o más registros no se han podido crear, se ha omitido el proceso de carga de combustibles. ERROR {0}", sresponse));
                            //odata.EstatusSinc = "Registro sincronizado a SAP";
                            iCodigomx++;
                        }
                        ///Crea condicionales y agrupara para crear movimiento en proveedores
                        #region Creacion de documentos SAP 
                        SBO_KF_RecargasVH_PD oRecargaSAP = new SBO_KF_RecargasVH_PD();
                        List<CargaCombustible> lstMovimientoContable = lstRecargaComb.Where(x => x.EsFacturableSAP == 0).ToList();
                        if (lstMovimientoContable.Count() > 0)
                        {
                            SBO_KF_RecargasVH_PD oRecargas = new SBO_KF_RecargasVH_PD();
                            var lstProveedor = lstMovimientoContable.GroupBy(x => Convert.ToInt32(x.SourceCenter.ProveedorID)).ToList();
                            foreach (var oProveedor in lstProveedor)
                            {
                                //agrupamiento por vehiculo
                                List<CargaCombustible> lstVehiculos = lstMovimientoContable.Where(x => Convert.ToInt32(x.SourceCenter.ProveedorID) == Convert.ToInt32(oProveedor.Key)).ToList();
                                var lstGroupVehiculos = lstVehiculos.GroupBy(x => Convert.ToInt32(x.Movil.VehiculoID)).ToList();
                                foreach (var oVehiculo in lstGroupVehiculos)
                                {
                                    List<CargaCombustible> lstRecargaProVehiculos = lstMovimientoContable.Where(x => Convert.ToInt32(x.SourceCenter.ProveedorID) == oProveedor.Key && Convert.ToInt32(x.Movil.VehiculoID) == oVehiculo.Key).ToList();

                                    CargaCombustible oGestionRecarga = lstRecargaProVehiculos.FirstOrDefault();
                                    oGestionRecarga.Litros = lstRecargaProVehiculos.Sum(x => x.Litros);
                                    oGestionRecarga.Pesos = lstRecargaProVehiculos.Sum(x => x.Pesos);
                                    sresponse =  oRecargaSAP.GestionaRecargaSAP(ref oCompany, oGestionRecarga);
                                    if (sresponse != "OK")
                                        throw new Exception(sresponse);
                                }
                            }
                        }
                        #endregion Creacion de documentos SAP
                        ///Fin de creación de condicionales y agrupara para crear movimiento en proveedores
                        this.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                        this.LlenaTablaRecargas(lstRecargaComb);
                        lstRecargaComb = new List<CargaCombustible>();
                        this.oGlobales.Mostrarmensaje("La información de recargas se ha sincronizado", BoMessageTime.bmt_Long, BoStatusBarMessageType.smt_Success);
                    }
                    else SBO_Application.MessageBox("No se ha podido recuperar el ultimo registro de carga de combustibles", 1, "OK");
                }
                else SBO_Application.MessageBox(string.Format(@"No se ha encontrado información en el archivo, asegúrate que todas las columnas tengan datos y cumpla con los lineamientos solicitados", Environment.NewLine), 1, "OK");
            }
            else SBO_Application.MessageBox(string.Format(@"No se ha encontrado información en el archivo, asegúrate que todas las columnas tengan datos y cumpla con los lineamientos solicitados", Environment.NewLine), 1, "OK");
        }
        void ProcesaLecturaRecargasCombustible(ref StreamReader Steamdata)
        {
            String sLine = string.Empty;
            lstRecargaComb = new List<CargaCombustible>();
            while (sLine != null)
            {
                sLine = Steamdata.ReadLine();
                if (!string.IsNullOrEmpty(sLine))
                {
                    var oLine = sLine.Split(',');
                    if (oLine.Length < 16)
                        continue;
                    CargaCombustible oTemp = new CargaCombustible();
                    oTemp.Litros = Convert.ToDouble(oLine[0]);
                    oTemp.Pesos = Convert.ToDecimal(oLine[1]);
                    oTemp.Movil = new Vehiculo();
                    oTemp.Movil.VehiculoID = this.VehiculoID;
                    oTemp.Fecha = Convert.ToDateTime(oLine[3]);
                    oTemp.Ticket = Convert.ToString(oLine[4]);
                    oTemp.TypeFuel = new TipoCombustible();
                    oTemp.TypeFuel.TipoCombustibleID = Convert.ToInt32(oLine[5]);
                    oTemp.SourceCenter = new Kanan.Costos.BO2.Proveedor();
                    oTemp.SourceCenter.ProveedorID = Convert.ToInt32(oLine[6]);
                    oTemp.Odometro = Convert.ToDouble(oLine[7]);
                    oTemp.TipoPagoID = Convert.ToInt32(oLine[8]);
                    oTemp.DetallePago = Convert.ToString(oLine[9]);
                    oTemp.CentroServicio = Convert.ToString(oLine[10]);
                    oTemp.Longitud = new Kanan.Comun.BO2.UnidadLongitud();
                    oTemp.Longitud.UnidadLongitudID = Convert.ToInt32(oLine[11]);
                    oTemp.Volumen = new Kanan.Comun.BO2.UnidadVolumen();
                    oTemp.Volumen.UnidadVolumenID = Convert.ToInt32(oLine[12]);
                    oTemp.Moneda = new Kanan.Comun.BO2.TipoMoneda();
                    oTemp.Moneda.TipoMonedaID = Convert.ToInt32(oLine[13]);
                    oTemp.Latitude = Convert.ToDouble(oLine[14]);
                    oTemp.Longitude = Convert.ToDouble(oLine[15]);
                    oTemp.sMovimientoSAP = Convert.ToString(oLine[16]).Trim();
                    oTemp.EsFacturableSAP = Convert.ToString(oLine[16]).Trim() == "Y" ? 0 : 1;
                    lstRecargaComb.Add(oTemp);
                }
            }
            this.LlenaTablaRecargas(lstRecargaComb);
        }

        void LlenaTablaRecargas(List<CargaCombustible> lstRCombustible)
        {
            try
            {
                Matrix oMatrixPorte = (Matrix)this.oForm.Items.Item("MtxReCom").Specific;
                Columns oColum = (SAPbouiCOM.Columns)oMatrixPorte.Columns;
                oMatrixPorte.Clear();
                if (lstRCombustible != null)
                {
                    int iLinea = 1;
                    foreach (CargaCombustible oRecarga in lstRCombustible)
                    {
                        string sTipoCombustible = string.Empty;
                        switch (Convert.ToInt32( oRecarga.TypeFuel.TipoCombustibleID))
                        { 
                            case 1:
                                sTipoCombustible = "GASOLINA MAGNA";
                                break;
                            case 2:
                                sTipoCombustible = "GASOLINA PREMIUM";
                                break;
                            default :
                                sTipoCombustible = "DIESEL";
                                break;
                        }
                        this.oForm.DataSources.UserDataSources.Item("oUMAT011").Value = iLinea.ToString();
                        this.oForm.DataSources.UserDataSources.Item("oUMAT012").Value = String.Format("{0:n}", Convert.ToDecimal(oRecarga.Litros));
                        this.oForm.DataSources.UserDataSources.Item("oUMAT013").Value = String.Format("{0:n}", oRecarga.Pesos);
                        this.oForm.DataSources.UserDataSources.Item("oUMAT014").Value = Convert.ToDateTime(oRecarga.Fecha).ToString("dd/MM/yyyy");
                        this.oForm.DataSources.UserDataSources.Item("oUMAT015").Value = oRecarga.Ticket;
                        this.oForm.DataSources.UserDataSources.Item("oUMAT016").Value = sTipoCombustible;
                        this.oForm.DataSources.UserDataSources.Item("oUMAT017").Value = Convert.ToDouble(oRecarga.Odometro).ToString();
                        this.oForm.DataSources.UserDataSources.Item("oUMAT018").Value = oRecarga.sMovimientoSAP;
                        iLinea++;
                        oMatrixPorte.AddRow(1, -1);
                    }
                }
            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError(string.Format("{0}->LlenaTablaRecargas()", CONST_CLASE), ex.Message);
            }
        }
        #endregion Gestiona carga de combustible masivo

        #endregion


    }
}
