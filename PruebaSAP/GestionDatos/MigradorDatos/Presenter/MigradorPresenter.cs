﻿using System;
using System.IO;
using System.Web.UI;
using KananSAP.GestionDatos.MigradorDatos.View;
using KananWS.Interface;
using SAPbouiCOM;
using SAPbobsCOM;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

using KananSAP.GestionDatos.MigradorDatos.Librerias;
using Kanan.Llantas.BO2;
using KananSAP.Helper;
using Kanan.Vehiculos.BO2;

namespace KananSAP.GestionDatos.MigradorDatos.Presenter
{
    public class MigradorPresenter
    {
        #region Atributos
        /*-----Form-----*/
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company oCompany;
        private SAPbouiCOM.Item oItem;
        //public MigradorView view { get; set; }
        private Configurations configs;
        //private LlantasWS LlantaService;
        private Helper.KananSAPHerramientas oHerramientas;
        public UserTable TablaDeSAP { get; set; }        
        public string CodigoDeObjeto { get; set; }
        private List<Llanta> lstLlantas = null;
        private List<CargaCombustible> lstRecargasComb = null;
        private string sMoodForm = string.Empty;
        const string CONST_CLASE = "MigradorPresenter.cs";
        /*-----OpenFileDialog-----*/        
        string name;
        #endregion Atributos

        #region Constructor
        public MigradorPresenter(SAPbouiCOM.Application aplication, SAPbobsCOM.Company company, Configurations c, string sMood="")
        {

            this.sMoodForm = sMood;
            this.configs = c;
            this.SBO_Application = aplication;
            this.oCompany = company;
            //this.view = new MigradorView(this.SBO_Application, this.oCompany, this.configs, sMood);
            this.oHerramientas = new Helper.KananSAPHerramientas(this.SBO_Application);
           
        }
        #endregion

        #region Eventos
        public void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {

                if (pVal.FormTypeEx == "migrador" && !pVal.BeforeAction)
                {
                    #region Migrador de datos
                    switch (pVal.ItemUID)
                    {
                        case "Buscar":
                            this.BuscarArchivo();
                            break;
                        case "Leer":
                            this.LeerArchivo();
                            break;
                        case "Guardar":
                            this.GuardarArchivo();
                            break;
                    }
                    BubbleEvent = false;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                BubbleEvent = false;
                oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            }
        }
        #endregion

        #region Métodos

        #region BuscarArchivo
        public string BuscarArchivo()
        {
            string path = "";
            try
            {
                Thread t = new Thread((ThreadStart)delegate
                    {
                        FileDialog objDialog = new OpenFileDialog();
                        objDialog.Filter = "csv files (*.csv)|*.csv|txt files (*.txt)|*.txt|All files (*.*)|*.*";

                        //create a Form for ownership (SAP-Window won't work)
                        System.Windows.Forms.Form g = new System.Windows.Forms.Form();
                        g.Width = 1;
                        g.Height = 1;
                        g.Activate();
                        g.BringToFront();
                        g.Visible = true;
                        g.TopMost = true;
                        g.Focus();

                        DialogResult objResult = objDialog.ShowDialog(g);
                        Thread.Sleep(100);
                        if (objResult == DialogResult.OK)
                        {
                            path = objDialog.FileName;
                        }
                    })
                {
                    IsBackground = false,
                    Priority = ThreadPriority.Highest
                };
                t.SetApartmentState(ApartmentState.STA);
                t.Start();
                while (!t.IsAlive) ;
                Thread.Sleep(1);
                t.Join();

                //SBO_Application.MessageBox("PATTH", 1, "OK");
                if (!String.IsNullOrEmpty(path))
                {
                    if (File.Exists(path))
                    {
                       // view.SetRuta(path);
                        FileInfo oFile = new FileInfo(path);
                        if (oFile.Extension == ".csv" || oFile.Extension == ".txt")
                            this.LeerArchivo();
                        else SBO_Application.MessageBox("El archivo no es de tipo (CSV o TXT) no cumple con los requisitos para la lectura, favor de adjuntar un archivo con las extensiones requeridas.", 1, "OK");
                    }
                    else SBO_Application.MessageBox("No se ha encontrado la información del archivo", 1, "OK");
                }
            }
            catch (Exception SeleccionarArchivoError)
            {
                
                throw new Exception("Error al seleccionar el archivo: " + SeleccionarArchivoError);
            }
            return path;
        }
        #endregion

        #region LeerArchivo
        public void LeerArchivo()
        {
            try
            {
                lstLlantas = new List<Llanta>();
                //name = view.GetRuta();
                if (!string.IsNullOrEmpty(name))
                {
                    if (File.Exists(name))
                    {
                        StreamReader Streamdata = new StreamReader(name);
                        switch (this.sMoodForm)
                        {
                            case "Recargas":
                                this.ProcesaLecturaRecargasCombustible(ref Streamdata);
                                break;
                            default:
                                this.ProcesaLecturaLlantas(ref Streamdata);
                                break;
                        }
                        
                        
                    }
                    else SBO_Application.MessageBox("El archivo elegido no existe favor de agregar un archivo existente", 1, "OK");
                }
                else SBO_Application.MessageBox("El archivo no se encuentra cargado, favor de adjuntarlo y posterior iniciar el proceso de carga", 1, "OK");
            }
            catch (Exception LeerArchivoError)
            {
                throw new Exception("Error al leer el archivo: " + LeerArchivoError);
            }
        }
        #endregion

        

        #region GuardarArchivo
        public void GuardarArchivo()
        {
            try
            {
              
               Add oConnfigAdd = new Add(ref this.oCompany, this.configs);
               switch (sMoodForm)
               {
                   case "Recargas":
                       break;
                   default:
                       this.GuardaLlantaUDT(ref oConnfigAdd);
                    break;
               }
            }
            catch (Exception ex)
            {
                this.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                SBO_Application.MessageBox(ex.Message, 2, "OK");
                KananSAPHerramientas.LogError("MigradorPresenter.cs->GuardarArchivo()", ex.Message);
            }
        }
        #endregion
        //

        #region Gestiona carga de llantas masivas
        void ProcesaLecturaLlantas(ref StreamReader Steamdata)
        {
            String sLine = string.Empty;
            while (sLine != null)
            {
                sLine = Steamdata.ReadLine();
                if (!string.IsNullOrEmpty(sLine))
                {
                    var oLine = sLine.Split(',');
                    if (oLine.Length < 33)
                        continue;
                    Llanta oTemp = new Llanta();
                    oTemp.ItemCode = Convert.ToString(oLine[0]);
                    oTemp.NumeroEconomico = Convert.ToString(oLine[1]);
                    oTemp.NumeroRevestimientos = Convert.ToInt32(oLine[2]);
                    oTemp.SN = Convert.ToString(oLine[3]);
                    oTemp.Marca = Convert.ToString(oLine[4]);
                    oTemp.Modelo = Convert.ToString(oLine[5]);
                    oTemp.NumeroCapas = Convert.ToInt32(oLine[6]);
                    oTemp.Costo = Convert.ToDecimal(oLine[7]);
                    oTemp.ClaseRotacion = Convert.ToString(oLine[8]);
                    oTemp.proveedorLlanta = new Kanan.Costos.BO2.Proveedor();
                    oTemp.proveedorLlanta.ProveedorID = Convert.ToInt32(oLine[9]);
                    oTemp.NumeroFactura = Convert.ToString(oLine[10]);
                    oTemp.FechaCompra = Convert.ToDateTime(oLine[11]);
                    oTemp.Observaciones = Convert.ToString(oLine[12]);
                    oTemp.KilometrosRecorridos = Convert.ToDouble(oLine[13]);
                    oTemp.Propietario = new Kanan.Operaciones.BO2.Empresa();
                    oTemp.Propietario.EmpresaID = Convert.ToInt32(oLine[14]);
                    oTemp.EstaInstalado = Convert.ToBoolean(Convert.ToUInt32(oLine[15]));
                    oTemp.SucursalID = Convert.ToString(oLine[16]);
                    oTemp.Uso = Convert.ToInt32(oLine[17]);
                    oTemp.Ancho = Convert.ToString(oLine[18]);
                    oTemp.Aspecto = Convert.ToString(oLine[19]);
                    oTemp.Construccion = Convert.ToInt32(oLine[20]);
                    oTemp.DiametroRin = Convert.ToString(oLine[21]);
                    oTemp.IndiceCarga = Convert.ToString(oLine[22]);
                    oTemp.RangoVelocidad = Convert.ToInt32(oLine[23]);
                    oTemp.Aplicacion = Convert.ToInt32(oLine[24]);
                    oTemp.Traccion = Convert.ToInt32(oLine[25]);
                    oTemp.Treadwear = Convert.ToString(oLine[26]);
                    oTemp.Temperatura = Convert.ToInt32(oLine[27]);
                    oTemp.MaximaPresionInflado = Convert.ToString(oLine[28]);
                    oTemp.MinimaPresionInflado = Convert.ToString(oLine[29]);
                    oTemp.FechaFabricacion = Convert.ToDateTime(oLine[30]);
                    oTemp.KilometrosCaducidad = Convert.ToDecimal(oLine[31]);
                    oTemp.FechaCaducidad = Convert.ToDateTime(oLine[32]);
                    oTemp.CodigoAlmacen = Convert.ToString(oLine[33]);
                    oTemp.EstatusSinc = "Pendiente de replicar";
                    lstLlantas.Add(oTemp);
                }
            }
            this.LlenaTablaLlantas(lstLlantas);
        }
        void GuardaLlantaUDT(ref Add oConnfigAdd)
        {
            if (lstLlantas != null)
                   {
                       if (lstLlantas.Count > 0)
                       {
                           this.oCompany.StartTransaction();
                           long iCodigomx =  oConnfigAdd.recuperIDMaximoTabla("VSKF_LLANTA");
                           if (iCodigomx > 0)
                           {
                               foreach (Llanta odata in lstLlantas)
                               {
                                   string sName = string.Format("LP{0}", iCodigomx);
                                   string sresponse = oConnfigAdd.AddLlantas(odata, iCodigomx.ToString(), sName);
                                   if (sresponse != "OK")
                                       throw new Exception(string.Format("Uno o más registros no se han podido crear, se ha omitido el proceso de carga de llantas. ERROR {0}", sresponse));
                                   odata.EstatusSinc = "Registro sincronizado a SAP";
                                   iCodigomx++;
                               }
                               this.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                               this.LlenaTablaLlantas(lstLlantas);
                               lstLlantas = new List<Llanta>();
                           }
                           else SBO_Application.MessageBox("No se ha podido recuperar el ultimo registro de llantas", 1, "OK");
                       }
                       else SBO_Application.MessageBox(string.Format(@"No se ha encontrado información en el archivo, asegúrate que todas las columnas tengan datos y cumpla con los lineamientos solicitados", Environment.NewLine), 1, "OK");
                   }
                   else SBO_Application.MessageBox(string.Format(@"No se ha encontrado información en el archivo, asegúrate que todas las columnas tengan datos y cumpla con los lineamientos solicitados", Environment.NewLine), 1, "OK");
        }
        void LlenaTablaLlantas(List<Llanta> lstLlantas)
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("migrador");
                Matrix oMatrixPorte = (Matrix)this.oForm.Items.Item("MtxFR").Specific;
                Columns oColum = (SAPbouiCOM.Columns)oMatrixPorte.Columns;
                oMatrixPorte.Clear();
                if (lstLlantas != null)
                {
                    int iLinea = 1;
                    foreach (Llanta oLlanta in lstLlantas)
                    {
                        this.oForm.DataSources.UserDataSources.Item("oUMAT01").Value = iLinea.ToString();
                        this.oForm.DataSources.UserDataSources.Item("oUMAT02").Value = oLlanta.NumeroEconomico;
                        this.oForm.DataSources.UserDataSources.Item("oUMAT03").Value = oLlanta.SN;
                        this.oForm.DataSources.UserDataSources.Item("oUMAT04").Value = oLlanta.Marca;
                        this.oForm.DataSources.UserDataSources.Item("oUMAT05").Value = oLlanta.Modelo;
                        this.oForm.DataSources.UserDataSources.Item("oUMAT06").Value = oLlanta.NumeroRevestimientos.ToString();
                        this.oForm.DataSources.UserDataSources.Item("oUMAT07").Value = oLlanta.Costo.ToString();
                        this.oForm.DataSources.UserDataSources.Item("oUMAT08").Value = oLlanta.NumeroFactura;
                        this.oForm.DataSources.UserDataSources.Item("oUMAT09").Value = Convert.ToDateTime(oLlanta.FechaCompra).ToString("dd/MM/yyyy");
                        this.oForm.DataSources.UserDataSources.Item("oUMAT10").Value = oLlanta.EstatusSinc;
                        iLinea++;
                        oMatrixPorte.AddRow(1, -1);
                    }
                }
            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError(string.Format("{0}->LlenaTablaLlantas()", CONST_CLASE), ex.Message);
            }
        }
        #endregion Gestiona carga de llantas masivas

        #region Gestiona carga de combustible masivo
        void ProcesaLecturaRecargasCombustible(ref StreamReader Steamdata)
        {
            String sLine = string.Empty;
            while (sLine != null)
            {
                sLine = Steamdata.ReadLine();
                if (!string.IsNullOrEmpty(sLine))
                {
                    var oLine = sLine.Split(',');
                    if (oLine.Length < 15)
                        continue;
                    CargaCombustible oTemp = new CargaCombustible();
                    oTemp.Litros = Convert.ToDouble(oLine[0]);
                    oTemp.Pesos = Convert.ToDecimal(oLine[1]);
                    oTemp.Movil = new Vehiculo();
                    oTemp.Movil.VehiculoID = Convert.ToInt32(oLine[2]);
                    oTemp.Fecha = Convert.ToDateTime(oLine[3]);
                    oTemp.Ticket = Convert.ToString(oLine[4]);
                    oTemp.TypeFuel = new TipoCombustible();
                    oTemp.TypeFuel.TipoCombustibleID = Convert.ToInt32(oLine[5]);
                    oTemp.SourceCenter = new Kanan.Costos.BO2.Proveedor();
                    oTemp.SourceCenter.ProveedorID =  Convert.ToInt32(oLine[6]);
                    oTemp.Odometro = Convert.ToDouble(oLine[7]);
                    oTemp.TipoPagoID = Convert.ToInt32(oLine[8]);
                    oTemp.DetallePago = Convert.ToString(oLine[9]);
                    oTemp.CentroServicio = Convert.ToString(oLine[10]);
                    oTemp.Longitud = new Kanan.Comun.BO2.UnidadLongitud();
                    oTemp.Longitud.UnidadLongitudID = Convert.ToInt32(oLine[11]);
                    oTemp.Volumen = new Kanan.Comun.BO2.UnidadVolumen();
                    oTemp.Volumen.UnidadVolumenID = Convert.ToInt32(oLine[12]);
                    oTemp.Moneda = new Kanan.Comun.BO2.TipoMoneda();
                    oTemp.Moneda.TipoMonedaID = Convert.ToInt32(oLine[13]);
                    oTemp.Latitude = Convert.ToDouble(oLine[14]);
                    oTemp.Longitude = Convert.ToDouble(oLine[15]);
                    lstRecargasComb.Add(oTemp);
                }
            }
            this.LlenaTablaRecargas(lstRecargasComb);
        }

        void LlenaTablaRecargas(List<CargaCombustible> lstRecargas)
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("migrador");
                Matrix oMatrixPorte = (Matrix)this.oForm.Items.Item("MtxReCom").Specific;
                Columns oColum = (SAPbouiCOM.Columns)oMatrixPorte.Columns;
                oMatrixPorte.Clear();
                if (lstLlantas != null)
                {
                    int iLinea = 1;
                    foreach (CargaCombustible oRecarga in lstRecargas)
                    {
                        this.oForm.DataSources.UserDataSources.Item("oUMAT011").Value = iLinea.ToString();
                        this.oForm.DataSources.UserDataSources.Item("oUMAT012").Value = oRecarga.Litros.ToString();
                        this.oForm.DataSources.UserDataSources.Item("oUMAT013").Value = oRecarga.Pesos.ToString();
                        this.oForm.DataSources.UserDataSources.Item("oUMAT014").Value = Convert.ToDateTime(oRecarga.Fecha).ToString("dd/MM/yyyy");
                        this.oForm.DataSources.UserDataSources.Item("oUMAT015").Value = oRecarga.Ticket;
                        this.oForm.DataSources.UserDataSources.Item("oUMAT016").Value = "";
                        this.oForm.DataSources.UserDataSources.Item("oUMAT017").Value = Convert.ToDouble(oRecarga.Odometro).ToString();
                        iLinea++;
                        oMatrixPorte.AddRow(1, -1);
                    }
                }
            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError(string.Format("{0}->LlenaTablaRecargas()", CONST_CLASE), ex.Message);
            }
        }
        #endregion Gestiona carga de combustible masivo

        #endregion

    }
}
