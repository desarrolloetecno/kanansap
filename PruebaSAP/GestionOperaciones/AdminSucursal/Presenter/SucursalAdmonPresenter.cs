﻿using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using KananSAP.GestionOperaciones.AdminSucursal.Views;
using KananSAP.Helpers;
using KananSAP.Operaciones.Data;
using KananWS.Interface;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.GestionOperaciones.AdminSucursal.Presenter
{
    public class SucursalAdmonPresenter
    {
        #region Atributos

        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        public SucursalAdmonView view;
        private SucursalView sucursalView;
        private Configurations Configs;
        private SucursalHelperData helperData;
        private SucursalSAP scData;
        private SucursalWS sucursalWS;
        private SucursalSAPView scSapView;
        public int? VehiculoID { get; set; }
        public Vehiculo Vehiculo { get; set; }
        private bool isSQL;

        /// <summary>
        /// Indica a que o quien se le va asignar sucursal
        /// Vehiculo: 1, Operador: 2, Empleado: 3
        /// </summary>
        public int? SetSucursalTo { get; set; }
        private bool Modal { get; set; }
        private Helper.KananSAPHerramientas oHerramientas;
        #endregion

        #region Constructor
        public SucursalAdmonPresenter(SAPbouiCOM.Application application, SAPbobsCOM.Company company, Configurations configs)
        {
            this.SBO_Application = application;
            this.Company = company;
            this.view = new SucursalAdmonView(this.SBO_Application, this.Company, configs);
            this.SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
            this.Configs = configs;
            helperData = new SucursalHelperData(company);
            scData = new SucursalSAP(company);
            sucursalWS = new SucursalWS(Guid.Parse(configs.UserFull.Usuario.PublicKey), configs.UserFull.Usuario.PrivateKey);
            sucursalView = new SucursalView(this.SBO_Application, this.Company, this.Configs);
            this.Modal = false;
            this.oHerramientas = new Helper.KananSAPHerramientas(application);
            isSQL = Company.DbServerType.ToString().Contains("MSSQL") ? true : false;
        }

        #endregion

        #region Eventos
        private void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                #region AdmonSucursalEmpresaKanan
                if (pVal.FormTypeEx == "frmScAdmon")
                {
                    if(this.Modal)
                    {
                        this.sucursalView.ShowForm();
                        BubbleEvent = false;
                        return;
                    }
                    #region Before

                    if (pVal.BeforeAction)
                    {
                        if (pVal.ItemUID == "btnScAdd" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            sucursalView.LoadForm();
                            sucursalView.PrepareNewSucursal();
                            sucursalView.HiddeDeleteButton();
                            sucursalView.ShowForm();
                            this.Modal = true;
                        }
                        if (pVal.ItemUID == "btnScBus" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.view.SetFiltro();
                        }
                        if (pVal.ColUID == "#" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                            this.view.HasRowValue(pVal.Row);
                        if (pVal.ItemUID == "btnScDel" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            if (this.view.SucursalID.HasValue)
                            {
                                if (this.view.isDelete())
                                    this.EliminarTodo();
                                else
                                    this.AsignarSucursal();
                            }
                            BubbleEvent = false;
                        }

                        if (pVal.ItemUID == "btnScCanc" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.view.Close();
                        }

                        if (pVal.EventType == BoEventTypes.et_MATRIX_LINK_PRESSED)
                        {
                            if (this.view.HasRowValue(pVal.Row))
                            {
                                if (this.Exist(this.view.SucursalID))
                                {
                                    sucursalView.LoadForm();
                                    sucursalView.ShowForm();
                                    sucursalView.Sucursal = this.view.Sucursal;
                                    sucursalView.DataToForm();
                                }
                            }
                            BubbleEvent = false;
                        }
                        
                    }

                    #endregion
                }
                #endregion

                #region SucursalEmpresa Nuevo
                if (pVal.FormTypeEx == "frmScNew")
                {
                    #region Before
                    if(pVal.BeforeAction)
                    {
                        if (pVal.ItemUID == "btnScSave" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.InsertOrUpdate();
                            this.view.SetDataSource();
                            BubbleEvent = false;
                        }
                        if (pVal.ItemUID == "btnDelSc" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.EliminarTodo();
                            this.Modal = false;
                            this.SBO_Application.Forms.ActiveForm.Close();
                        }
                        if (pVal.EventType == BoEventTypes.et_FORM_CLOSE && this.Modal)
                            this.Modal = false;
                        if (pVal.EventType == BoEventTypes.et_FORM_UNLOAD)
                            this.Modal = false;
                        if (pVal.ItemUID == "btnScCan" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.Modal = false;
                            this.SBO_Application.Forms.ActiveForm.Close();
                        }
                    }
                    #endregion
                    else if (!pVal.BeforeAction)
                    {
                        if (pVal.EventType == BoEventTypes.et_CHOOSE_FROM_LIST)
                        {
                            SAPbouiCOM.IChooseFromListEvent oCFLEvento = null;
                            oCFLEvento = ((SAPbouiCOM.IChooseFromListEvent)(pVal));
                            string sCFL_ID = null;
                            sCFL_ID = oCFLEvento.ChooseFromListUID;
                            SAPbouiCOM.Form oForm = null;
                            oForm = SBO_Application.Forms.Item(FormUID);
                            SAPbouiCOM.ChooseFromList oCFL = null;
                            oCFL = oForm.ChooseFromLists.Item(sCFL_ID);
                            if (oCFLEvento.BeforeAction == false)
                            {
                                SAPbouiCOM.DataTable oDataTable = null;
                                oDataTable = oCFLEvento.SelectedObjects;
                                string val = null;
                                try
                                {
                                    val = System.Convert.ToString(oDataTable.GetValue(0, 0));
                                    string sitem = "";
                                    switch (sCFL_ID)
                                    {
                                        case "CFLOWHS":
                                            sitem = "EdtOWHS";
                                            break;
                                        case "CFLOPRC":
                                            sitem = "EdtOPRC";
                                            break;
                                        case "CFLOBRC":
                                            sitem = "EdtOBRC";
                                            break;
                                    }
                                    oForm.DataSources.UserDataSources.Item(sitem).ValueEx = val;
                                }
                                catch { }
                            }
                        }
                    }
                }
                #endregion

                #region SucursalSAP Definir Nuevo Ventana de SAP
                if (pVal.FormType == 943)
                {
                    if (this.scSapView == null)
                        scSapView = new SucursalSAPView(this.SBO_Application, this.Company, this.Configs, pVal.FormTypeEx, pVal.FormTypeCount);
                    if (pVal.ItemUID == "1" && pVal.BeforeAction)//Actualizar
                    {
                        this.scSapView.GetRowsCount();
                    }
                    if (pVal.ItemUID == "1" && !pVal.BeforeAction)//Actualizar
                    {
                        string err = this.scSapView.ValidateData();
                        if (string.IsNullOrEmpty(err))
                        {
                            this.scSapView.FormToData();
                            this.InsertWS_SAPSucursalForm();
                            this.InsertSAP_FormSucursal();
                        }
                    }

                }
                #endregion

            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("SucursalAdmonPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
                this.oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            }
        }

        #endregion

        #region METODOS AdmonSucursal/Empresa
        public bool Exist(int? sucursalID)
        {
            try
            {
                scData.ItemCode = sucursalID.ToString();
                if(scData.ExistUDT())
                {
                    scData.UDTToSucursal();
                    this.view.Sucursal = scData.Sucursal;
                    return true;
                }else
                {
                    if(scData.SetUDTBySucursalID(sucursalID))
                    {
                        scData.UDTToSucursal();
                        this.view.Sucursal = scData.Sucursal;
                        return true;
                    }
                    
                }
                return false;
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("SucursalAdmonPresenter.cs->Exist()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        #region Metodos Web Service WS
        private void InsertWS_SAPSucursalForm()
        {
            try
            {
                Sucursal sc = this.sucursalWS.InsertarCompleto(this.scSapView.Sucursal);
                if (sc == null)
                    throw new Exception("Es posible que la información no se haya almacenado correctamente");
                if (sc.SucursalID == null)
                    throw new Exception("Es posible que la información no se haya almacenado correctamente");
                this.scSapView.Sucursal.SucursalID = sc.SucursalID;
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("SucursalAdmonPresenter.cs->InsertWS_SAPSucursalForm()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private bool EliminarWS()
        {
            try
            {
                this.view.PrepareToDelete();
                this.scData.SetUDTBySucursalID(this.view.Sucursal.SucursalID);
                this.scData.UDTToSucursal();
                return this.sucursalWS.ActualizarCompleto(this.scData.Sucursal);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("SucursalAdmonPresenter.cs->EliminarWS()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Metodos SAP
        private void InsertSAP_FormSucursal()
        {
            try
            {
                scData.SetItemCodeBranch(this.scSapView.Sucursal.Direccion);
                scData.Sucursal = this.scSapView.Sucursal;
                scData.Sucursal.Base = this.Configs.UserFull.Dependencia ?? new Empresa();
                scData.SucursalToUDT();
                scData.Insertar();
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("SucursalAdmonPresenter.cs->InsertarSAP_FormSucursal()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void EliminarSAP()
        {
            try
            {
                this.view.PrepareToDelete();
                this.helperData.ActualizarCompleto(this.view.Sucursal);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("SucursalAdmonPresenter.cs->EliminarSAP()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        private void EliminarTodo()
        {
            try
            {
                this.EliminarSAP();
                if(this.EliminarWS())
                {
                    if (scData.SetUDTBySucursalID(this.view.Sucursal.SucursalID))
                    {
                        scData.GetSyncUUID();
                        scData.Sincronizar(this.view.Sucursal);
                        //scData.RemoveBranch();
                        scData.Eliminar();
                    }
                }
                this.view.SucursalID = null;
                this.oHerramientas.ColocarMensaje("¡Registro eliminado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                this.view.SetDataSource();
                
            }
            catch (Exception ex) {
                this.view.SucursalID = null;
                this.view.SetDataSource();
                this.SBO_Application.Forms.ActiveForm.Close();
                KananSAP.Helper.KananSAPHerramientas.LogError("SucursalAdmonPresenter.cs->EliminarTodo()", ex.Message);
                throw new Exception(ex.Message);
            }
        }
        
        #endregion

        #region METODOS ASIGNAR SUCURSAL

        private void SucursalVehiculoSAP()
        {
            try
            {
                Vehiculos.Data.VehiculoSAP vhData = new Vehiculos.Data.VehiculoSAP(this.Company);
                if (vhData.SetUDTByVehiculoID((int)this.VehiculoID))
                {
                    vhData.SetSucursalToVehiculo(this.view.SucursalID);
                    VehiculoWS vhWS = new VehiculoWS(Guid.Parse(this.Configs.UserFull.Usuario.PublicKey), this.Configs.UserFull.Usuario.PrivateKey);
                    this.Vehiculo.SubPropietario.SucursalID = this.view.SucursalID;
                    vhWS.AsignarSucursal(this.Vehiculo);
                    this.oHerramientas.ColocarMensaje("¡La sucursal se asignó correctamente!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    this.SBO_Application.Forms.ActiveForm.Close();
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("SucursalAdmonPresenter.cs->SucursalVehiculoSAP()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void AsignarSucursal()
        {
            if (this.SetSucursalTo == 1)
            {
                if (this.VehiculoID.HasValue && this.view.SucursalID.HasValue)
                {
                    this.SucursalVehiculoSAP();
                }
            }
            else if (this.SetSucursalTo == 2)
            {
                //Operador
            }
            else if (this.SetSucursalTo == 3)
            {
                //Empleado
            }
        }

        #endregion

        #region METODOS Sucursal/Empresa Nuevo/Actualizar

        #region Metodos SAP
        private void InsertSAP()
        {
            try
            {
                this.sucursalView.FormToData();
                scData.Sucursal = this.sucursalView.Sucursal;                
                scData.Sincronizado = 1;
                scData.UUID = Guid.NewGuid();
                scData.SucursalToUDT();

                scData.Insertar();

                scData.SetItemCodeBranch(this.sucursalView.Sucursal.Name);
                //scData.SucursalToUDT();
                if (SAPinterface.Data.tools.tools.UtilizaBranchs(ref this.Company).Equals("Y", StringComparison.InvariantCultureIgnoreCase))
                    scData.AddBranch();
                //scData.Insertar();

                //scOUBRData.Sucursal = this.view.Sucursal;
                //scOUBRData.SucursalOUBRToUDT();
                //scOUBRData.Insertar();
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("SucursalAdmonPresenter.cs->InsertarSAP()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void UpdateSAP()
        {
            try
            {
                this.sucursalView.FormToData();
                scData.ItemCode = this.view.Sucursal.SucursalID.ToString();
                if (scData.ExistUDT())
                {
                    scData.Sucursal = this.view.Sucursal;
                    scData.GetSyncUUID();
                    scData.Sincronizado = 2;
                    scData.SucursalToUDT();
                    scData.Actualizar();
                }
                else
                {
                    if (scData.SetUDTBySucursalID(this.view.Sucursal.SucursalID))
                    {
                        scData.Sucursal = this.view.Sucursal;
                        scData.GetSyncUUID();
                        scData.Sincronizado = 2;
                        scData.SucursalToUDT();
                        scData.Actualizar();
                    }
                }
                if (scData.ExistBranch(this.view.Sucursal.Direccion))
                {
                    scData.Sucursal.Descripcion = this.view.Sucursal.Descripcion ?? string.Empty;
                    scData.UpdateBranch();
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("SucursalAdmonPresenter.cs->UpdateSAP()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Valida que no exista la sucursal antes de guardar
        /// </summary>
        /// <returns></returns>
        private string ValidateSucursal()
        {
            try
            {
                SAPbobsCOM.Recordset rs = scData.Consultar(this.sucursalView.Sucursal);
                if (scData.HashSucursal(rs))
                {
                    return "Ya existe una sucursal con el nombre de " + this.view.Sucursal.Direccion;
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("SucursalAdmonPresenter.cs->ValidateSucursal()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Metodos WS
        private bool InsertWS()
        {
            bool bExitoso = false;
            try
            {
                this.sucursalView.FormToData();
                Sucursal sc = this.sucursalWS.InsertarCompleto(this.sucursalView.Sucursal);
                if (sc == null)
                    throw new Exception("Es posible que la información no se haya almacenado correctamente");
                if (sc.SucursalID == null)
                    throw new Exception("Es posible que la información no se haya almacenado correctamente");
                this.sucursalView.Sucursal.SucursalID = sc.SucursalID;
                bExitoso = true;
            }
            catch (Exception ex) 
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("SucursalAdmonPresenter.cs->InsertWS()", ex.Message);
                //throw new Exception(ex.Message); 
            }
            return bExitoso;
        }

        private void UpdateWS()
        {
            try
            {
                this.sucursalView.FormToData();
                if (!this.sucursalView.Sucursal.SucursalID.HasValue)
                    throw new Exception("SucursalID no puede ser nulo");
                bool updated = this.sucursalWS.Actualizar(this.sucursalView.Sucursal);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("SucursalAdmonPresenter.cs->UpdateWS()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        public void InsertOrUpdate()
        {
            try
            {
                string err = this.sucursalView.ValidateData();
                this.sucursalView.FormToData();
                
                if (!string.IsNullOrEmpty(err))
                {
                    this.SBO_Application.MessageBox(err, 1, "Ok", "", "");
                }
                else
                {
                    if (this.sucursalView.Sucursal.SucursalID != null)
                    {
                        this.UpdateSAP();
                        this.UpdateWS();
                        if (scData.SetUDTBySucursalID(this.sucursalView.Sucursal.SucursalID))
                        {
                            scData.Sucursal.SucursalID = sucursalView.Sucursal.SucursalID;
                            this.scData.Sincronizar(sucursalView.Sucursal);
                        }
                        //this.view.SetDataSource();
                        this.oHerramientas.ColocarMensaje("¡Registro Guardado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                        this.SBO_Application.Forms.ActiveForm.Close();//.Item("frmScNew").Close();
                    }
                    else
                    {
                        string existe = this.ValidateSucursal();
                        if (!string.IsNullOrEmpty(existe))
                        {
                            this.SBO_Application.MessageBox(existe, 1, "Ok", "", "");
                        }
                        else
                        {
                            
                            if (this.InsertWS())
                            {
                                //if (scData.ExistUDT())//.SetUDTBySucursalID(this.view.Sucursal.SucursalID))
                                //{
                                    scData.Sucursal.SucursalID = this.sucursalView.Sucursal.SucursalID;
                                    //scData.Sincronizar(this.sucursalView.Sucursal);
                                    this.InsertSAP();
                                //}
                            }
                            //this.view.SetDataSource();
                            this.oHerramientas.ColocarMensaje("¡Registro Guardado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                            this.SBO_Application.Forms.Item("frmScNew").Close();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("SucursalAdmonPresenter.cs->InsertOrUpdate()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        #endregion

    }
}
