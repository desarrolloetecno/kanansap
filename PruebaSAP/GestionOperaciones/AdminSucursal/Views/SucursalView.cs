﻿using KananWS.Interface;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kanan.Operaciones.BO2;
using SAPinterface.Data.PD;
using SAPinterface.Data.INFO.Tablas;

namespace KananSAP.GestionOperaciones.AdminSucursal.Views
{
    public class SucursalView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Comp;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.Button oBoton;
        private SAPbouiCOM.ComboBox oComboBox;
        private SAPbouiCOM.StaticText oStaticText;
        public string FormUniqueID { get; set; }
        private XMLPerformance XmlApplication;
        string sPath = null;//System.Windows.Forms.Application.StartupPath;
       
        SAPbobsCOM.Recordset OPRCData;
        private Configurations Configs;
        public Sucursal Sucursal { get; set; }
        public bool isUpdate { get; set; }
        #endregion

        #region Constructor
        public SucursalView(SAPbouiCOM.Application application, SAPbobsCOM.Company company, Configurations configs)
        {
            this.SBO_Application = application;
            this.XmlApplication = new XMLPerformance(SBO_Application);
            this.Comp = company;
            this.oItem = null;
            this.oEditText = null;
            this.oBoton = null;
            this.oComboBox = null;
            this.Configs = configs;
            this.Sucursal = null;
            this.PrepareNewSucursal();

            //Instanciando Recordset
            OPRCData = (SAPbobsCOM.Recordset)this.Comp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
        }
        #endregion

        #region Form

        public void PrepareNewSucursal()
        {
            this.Sucursal = new Sucursal();
            this.Sucursal.Base = new Empresa();
        }

        public void LoadForm()
        {
            sPath = System.Windows.Forms.Application.StartupPath;
            try
            {
                this.oForm = SBO_Application.Forms.Item("frmScNew");
                this.AddChooseCostCenter("EdtOWHS", "CFLOWHS", "64", "txtAlmcen", "WhsCode");
                this.AddChooseCostCenter("EdtOPRC", "CFLOPRC", "61", "txtcostc", "PrcCode");
                this.addChooseBranch("EdtOBRC", "CFLOBRC", "247", "txtbranch", "BPLId");
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Invalid Form"))
                {
                    
                    this.XmlApplication.LoadFromXML(sPath + "\\XML",
                            "Sucursales.xml");
                    this.oForm = SBO_Application.Forms.Item("frmScNew");

                    //Se vuelve editable el Code y Name del formulario.
                    /*oEditText = oForm.Items.Item("txtCode").Specific;
                    oEditText.Item.Enabled = false;

                    oEditText = oForm.Items.Item("txtName").Specific;
                    oEditText.Item.Enabled = false;*/

                    //oComboBox = oForm.Items.Item("cmbOPRC").Specific;
                    //oComboBox.Item.Enabled = true;
                    //oComboBox.ValidValues.Add("0", "--None--");
                    //FillComboSucursal();
                    this.AddChooseCostCenter("EdtOWHS", "CFLOWHS", "64", "txtAlmcen", "WhsCode");
                    this.AddChooseCostCenter("EdtOPRC", "CFLOPRC", "61", "txtcostc", "PrcCode");
                    this.addChooseBranch("EdtOBRC", "CFLOBRC", "247", "txtbranch", "BPLId");
                    //Estabkece la ventana centrada
                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                }
                else
                {
                    SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                }
            }
        }

        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
            }
            catch (Exception ex)
            {
                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                this.oForm.Select();

            }
        }

        #endregion

        #region Auxiliares
        public string ValidateData()
        {
            try
            {
                //List<SBO_KF_SUCURSAL_INFO> lstsucursales = new List<SBO_KF_SUCURSAL_INFO>();
                string err = string.Empty, campos = string.Empty;
                this.oForm = SBO_Application.Forms.Item("frmScNew");

                oItem = this.oForm.Items.Item("txtcostc");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (string.IsNullOrEmpty(oEditText.String))
                    return "Favor de proporcionar el centro de costo para la sucursal";

                //buscar el registros de tablas de usuario 
                oStaticText = oForm.Items.Item("lblScID").Specific;//SucursalID
                /*if (string.IsNullOrEmpty(oStaticText.Caption))
                {
                    new SBO_KF_SUCURSALES_PD().listaSucursales(ref this.Comp, ref lstsucursales, sSucursalID: oEditText.String.Trim());
                    if (lstsucursales.Count > 0)
                        return "El centro de costo ya se ha asignado a una sucursal favor de seleccionar otro centro";
                }*/

                if (SAPinterface.Data.tools.tools.UtilizaBranchs(ref this.Comp).Equals("Y", StringComparison.InvariantCultureIgnoreCase))
                {
                    oItem = this.oForm.Items.Item("txtbranch");
                    oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                    if (string.IsNullOrEmpty(oEditText.String))
                        return "Favor de seleccionar el branch para la sucursal";
                }

                oItem = this.oForm.Items.Item("txtScNomb");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (string.IsNullOrEmpty(oEditText.String))
                    return "Favor de proporcionar el nombre de la sucursal";

                oItem = this.oForm.Items.Item("txtScResp");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (string.IsNullOrEmpty(oEditText.String))
                    return "Favor de proporcionar el responsable para la sucursal";

                oItem = this.oForm.Items.Item("txtScDirec");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (string.IsNullOrEmpty(oEditText.String))
                    return "Favor de proporcionar la dirección para la sucursal";


                oItem = this.oForm.Items.Item("txtAlmcen");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (string.IsNullOrEmpty(oEditText.String))
                    return "Favor de proporcionar el almacén para la sucursal";

                oItem = this.oForm.Items.Item("txtScCiud");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (string.IsNullOrEmpty(oEditText.String))
                    return "Favor de proporcionar la ciudad";

                oItem = this.oForm.Items.Item("txtScEst");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (string.IsNullOrEmpty(oEditText.String))
                    return "Favor de proporcionar el estado";



                oItem = this.oForm.Items.Item("txtScCP");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (string.IsNullOrEmpty(oEditText.String))
                    return "Favor de proporcionar el codigo postal";
                try
                {
                    Convert.ToInt32(oEditText.String);
                }
                catch { err = "El codigo postal debe ser de tipo numerico"; }


                oItem = this.oForm.Items.Item("txtScTel");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (string.IsNullOrEmpty(oEditText.String))
                    return "Favor de proporcionar el télefono";

                if (oEditText.String.Length > 15)
                    return "El campo de teléfono no puede ser mayor a 15 caracteres ";

                //try
                //{
                //    Convert.ToInt32(oEditText.String);
                //}
                //catch { err = "Proporciona un numero de teléfono valido"; }


                if (!this.Configs.UserFull.Dependencia.EmpresaID.HasValue)
                    return "La empresa no se ha definido";

                oStaticText = oForm.Items.Item("lblScID").Specific;//SucursalID
                if (!string.IsNullOrEmpty(oStaticText.Caption))
                    Sucursal.SucursalID = Convert.ToInt32(oStaticText.Caption);

                return err;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void FillComboSucursal()
        {
            try
            {
                String PrcCode = String.Empty;
                String PrcName = String.Empty;
                OPRCData.DoQuery(@"SELECT * FROM ""OPRC""");
                OPRCData.MoveFirst();
                for (int i = 0; i < OPRCData.RecordCount; i++)
                {
                    PrcCode = this.OPRCData.Fields.Item("PrcCode").Value.ToString();
                    PrcName = this.OPRCData.Fields.Item("PrcName").Value.ToString();

                    if (!String.IsNullOrEmpty(PrcCode) & !String.IsNullOrEmpty(PrcName))
                    {
                        oComboBox.ValidValues.Add(PrcCode, PrcName);
                    }
                    OPRCData.MoveNext();
                }
            }
            catch
            {
                throw new Exception("Error al obtener Centros de costo.");
            }
        }
        #endregion

        #region Mapping
        public void addChooseBranch(string EdtKey, string CFLkey, string sType, string sItemTxt, string keymapping)
        {
            try
            {


                Item oItem = null;
                oForm.DataSources.UserDataSources.Add(EdtKey, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);
                SAPbouiCOM.ChooseFromListCollection oCFLs = null;
                oCFLs = oForm.ChooseFromLists;
                SAPbouiCOM.ChooseFromList oCFL = null;
                SAPbouiCOM.ChooseFromListCreationParams oCFLCreationParams = null;
                oCFLCreationParams = ((SAPbouiCOM.ChooseFromListCreationParams)(SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)));
                oCFLCreationParams.MultiSelection = false;
                oCFLCreationParams.ObjectType = sType;
                oCFLCreationParams.UniqueID = CFLkey;
                oCFL = oCFLs.Add(oCFLCreationParams);
                oItem = oForm.Items.Item(sItemTxt);
                EditText oEditText = (EditText)(oItem.Specific);
                oEditText.DataBind.SetBound(true, "", EdtKey);
                oEditText.ChooseFromListUID = CFLkey;
                oEditText.ChooseFromListAlias = keymapping;
            }
            catch { }
        }
        public void AddChooseCostCenter(string EdtKey, string CFLkey, string sType, string sItemTxt, string keymapping)
        {
            try
            {


                Item oItem = null;
                oForm.DataSources.UserDataSources.Add(EdtKey, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);
                SAPbouiCOM.ChooseFromListCollection oCFLs = null;
                oCFLs = oForm.ChooseFromLists;
                SAPbouiCOM.ChooseFromList oCFL = null;
                SAPbouiCOM.ChooseFromListCreationParams oCFLCreationParams = null;
                oCFLCreationParams = ((SAPbouiCOM.ChooseFromListCreationParams)(SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)));
                oCFLCreationParams.MultiSelection = false;
                oCFLCreationParams.ObjectType = sType;
                oCFLCreationParams.UniqueID = CFLkey;
                oCFL = oCFLs.Add(oCFLCreationParams);                
                SAPbouiCOM.Conditions oConditions = this.SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_Conditions);
                oConditions = oCFL.GetConditions();
                SAPbouiCOM.Condition oCondition = oConditions.Add();
                oCondition.Alias = "Locked";
                oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
                oCondition.CondVal = "N";
                oCFL.SetConditions(oConditions);
                oItem = oForm.Items.Item(sItemTxt);
                EditText oEditText = (EditText)(oItem.Specific);
                oEditText.DataBind.SetBound(true, "", EdtKey);
                oEditText.ChooseFromListUID = CFLkey;
                oEditText.ChooseFromListAlias = keymapping;
            }
            catch{}
        }
        public void DataToForm()
        {
            try
            {
                if (this.Sucursal != null)
                {
                    isUpdate = true;
                    this.oForm.Title = "Actualizar sucursal/empresa";

                    /* SE AGREGAN LOS CAMPOS CODE Y NAME PARA REGISTRAR EN VSKF_SUCURSAL */
                    if (!String.IsNullOrEmpty(Convert.ToString(this.Sucursal.Code)))
                    {
                        oEditText = oForm.Items.Item("txtCode").Specific;
                        oEditText.Value = Convert.ToString(Sucursal.Code);
                    }
                    if (!String.IsNullOrEmpty(Convert.ToString(this.Sucursal.Name)))
                    {
                        oEditText = oForm.Items.Item("txtName").Specific;
                        oEditText.Value = Convert.ToString(this.Sucursal.Name);
                    }
                    /* SE AGREGAN LOS CAMPOS CODE Y NAME PARA REGISTRAR EN VSKF_SUCURSAL */

                    if (Sucursal.SucursalID.HasValue)
                    {
                        oStaticText = oForm.Items.Item("lblScID").Specific;
                        oStaticText.Caption = Sucursal.SucursalID.ToString();
                    }
                    if (!string.IsNullOrEmpty(Sucursal.Direccion))
                    {
                        oEditText = oForm.Items.Item("txtScNomb").Specific;
                        oEditText.String = Sucursal.Direccion;
                    }
                    if (!string.IsNullOrEmpty(Sucursal.Responsable))
                    {
                        oEditText = oForm.Items.Item("txtScResp").Specific;
                        oEditText.String = Sucursal.Responsable;
                    }
                    if (!string.IsNullOrEmpty(Sucursal.DireccionReal))
                    {
                        oEditText = oForm.Items.Item("txtScDirec").Specific;
                        oEditText.String = Sucursal.DireccionReal;
                    }
                    if (!string.IsNullOrEmpty(Sucursal.Ciudad))
                    {
                        oEditText = oForm.Items.Item("txtScCiud").Specific;
                        oEditText.String = Sucursal.Ciudad;
                    }
                    if (!string.IsNullOrEmpty(Sucursal.Estado))
                    {
                        oEditText = oForm.Items.Item("txtScEst").Specific;
                        oEditText.String = Sucursal.Estado;
                    }
                    if (Sucursal.CodigoPostal.HasValue)
                    {
                        oEditText = oForm.Items.Item("txtScCP").Specific;
                        oEditText.String = Sucursal.CodigoPostal.ToString();
                    }
                    if (!string.IsNullOrEmpty(Sucursal.Telefono))
                    {
                        oEditText = oForm.Items.Item("txtScTel").Specific;
                        oEditText.String = Sucursal.Telefono;
                    }
                    if (!string.IsNullOrEmpty(Sucursal.Contacto))
                    {
                        oEditText = oForm.Items.Item("txtScCont").Specific;
                        oEditText.String = Sucursal.Contacto;
                    }
                    if (!string.IsNullOrEmpty(Sucursal.Descripcion))
                    {
                        oEditText = oForm.Items.Item("txtScDesc").Specific;
                        oEditText.String = Sucursal.Descripcion;
                    }
                    if (!string.IsNullOrEmpty(Sucursal.Almacen))
                    {
                        oEditText = oForm.Items.Item("txtAlmcen").Specific;
                        oEditText.String = Sucursal.Almacen;
                    }

                    if (!string.IsNullOrEmpty(Sucursal.CostCenter))
                    {
                        oEditText = oForm.Items.Item("txtcostc").Specific;
                        oEditText.String = Sucursal.CostCenter;
                    }
                    //SE AGREGA EL MAIL DEL ALMACENISTA
                    if (!string.IsNullOrEmpty(Sucursal.MailAlmacenista))
                    {
                        oEditText = oForm.Items.Item("txtMailAlm").Specific;
                        oEditText.String = Sucursal.MailAlmacenista;
                    }

                    if (Sucursal.HorasDisponibilidad != null)
                    {
                        oEditText = oForm.Items.Item("txthrsdis").Specific;
                        oEditText.String = Sucursal.HorasDisponibilidad.ToString();
                    }

                    DisableCodeName();

                }
                else
                    isUpdate = false;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void FormToData()
        {
            try
            {
                //oStaticText = oForm.Items.Item("lblScID").Specific;//SucursalID
                //if (!string.IsNullOrEmpty(oStaticText.Caption))
                //    Sucursal.SucursalID = Convert.ToInt32(oStaticText.Caption);

                /* SE AGREGAN LOS CAMPOS CODE Y NAME PARA REGISTRAR EN VSKF_SUCURSAL*/
                //oEditText = oForm.Items.Item("txtCode").Specific;//Code
                //if (!string.IsNullOrEmpty(oEditText.Value))
                //    Sucursal.Code = oEditText.Value;
                

                oEditText = oForm.Items.Item("txtName").Specific;//Nombre
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    Sucursal.Name = oEditText.String.Trim();

                oEditText = oForm.Items.Item("txtScNomb").Specific;//Nombre
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    Sucursal.Direccion = oEditText.String.Trim();
                oEditText = oForm.Items.Item("txtScResp").Specific;//Responsable
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    Sucursal.Responsable = oEditText.String.Trim();
                oEditText = oForm.Items.Item("txtScDirec").Specific;//Direccion
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    Sucursal.DireccionReal = oEditText.String.Trim();
                oEditText = oForm.Items.Item("txtScCiud").Specific;//Ciudad
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    Sucursal.Ciudad = oEditText.String.Trim();
                oEditText = oForm.Items.Item("txtScEst").Specific;//Estado
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    Sucursal.Estado = oEditText.String.Trim();
                oEditText = oForm.Items.Item("txtScCP").Specific;//Código postal
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                     Sucursal.CodigoPostal = Convert.ToInt32(oEditText.String.Trim());
                oEditText = oForm.Items.Item("txtScTel").Specific;//Telefono
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    Sucursal.Telefono = oEditText.String.Trim();
                oEditText = oForm.Items.Item("txtScCont").Specific;//Contacto
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    Sucursal.Contacto = oEditText.String.Trim();
                oEditText = oForm.Items.Item("txtScDesc").Specific;//Descripcion
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    Sucursal.Descripcion = oEditText.String.Trim();
                this.Sucursal.Base = this.Configs.UserFull.Dependencia ?? new Empresa();
                oEditText = oForm.Items.Item("txtAlmcen").Specific;//Almacen
                Sucursal.Almacen = oEditText.String.Trim();

                oEditText = oForm.Items.Item("txtbranch").Specific;//Branch
                if (!string.IsNullOrEmpty(oEditText.String))
                    this.Sucursal.BranchID = Convert.ToInt32(oEditText.String);

                oEditText = oForm.Items.Item("txtcostc").Specific;//Centro de costo
                this.Sucursal.CostCenter = oEditText.String;

                oEditText = oForm.Items.Item("txtMailAlm").Specific;//Correo del Almacenista
                this.Sucursal.MailAlmacenista = oEditText.String;

                oEditText = oForm.Items.Item("txthrsdis").Specific;//Horas de disonibilidad
                this.Sucursal.HorasDisponibilidad = Convert.ToInt32( oEditText.String);
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        #endregion

        public void HiddeDeleteButton()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("frmScNew");
                oItem = oForm.Items.Item("btnDelSc");
                oItem.Visible = false;
                oItem = oForm.Items.Item("btnScCan");
                oItem.Left = 84;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void DisableCodeName()
        {
            try
            {
              //Se intenta los EditText al mostrar unformacion de una Sucursal.
              /*oEditText = oForm.Items.Item("txtCode").Specific;
              oEditText.Item.Enabled = false;
              oEditText = oForm.Items.Item("txtName").Specific;
              oEditText.Item.Enabled = false;*/
            }
            catch (Exception ex)
            {
                throw new Exception ("Error al intentar inabilitar Code, Name. " + ex.Message);
            }
        }
    }
}
