﻿using Kanan.Operaciones.BO2;
using KananSAP.Operaciones.Data;
using KananWS.Interface;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.GestionOperaciones.AdminSucursal.Views
{
    public class SucursalAdmonView
    {
         #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.Button oBoton;
        private SAPbouiCOM.Matrix oMatrix;
        private SAPbouiCOM.Condition oCondition;
        private SAPbouiCOM.Conditions oConditions;
        private XMLPerformance XmlApplication;
        string sPath = null;//System.Windows.Forms.Application.StartupPath;
        
        private Configurations Configs;
        public Sucursal Sucursal { get; set; }
        public SucursalSAP SucursalSAP { get; set; }
        public bool isUpdate { get; set; }
        private string Filtro { get; set; }
        public int? SucursalID { get; set; }
        private string Nombre { get; set; }
        #endregion

        #region Constructor
        public SucursalAdmonView(SAPbouiCOM.Application application, SAPbobsCOM.Company company, Configurations configs)
        {
            this.SBO_Application = application;
            this.XmlApplication = new XMLPerformance(SBO_Application);
            this.oItem = null;
            this.oEditText = null;
            this.oBoton = null;
            oMatrix = null;
            this.Configs = configs;
            this.Sucursal = new Sucursal();
            this.SucursalSAP = new SucursalSAP(company);
            this.Sucursal.Base = new Empresa();
            isUpdate = false;
            this.SucursalID = null;
            this.Nombre = null;
        }
        #endregion

        #region Form
        public void LoadForm()
        {
            sPath = System.Windows.Forms.Application.StartupPath;
            try
            {
                this.oForm = SBO_Application.Forms.Item("frmScAdmon");
                this.SetDataSource();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Invalid Form"))
                {
                    
                    this.XmlApplication.LoadFromXML(sPath + "\\XML",
                           "SucursalAdmon.xml");
                    this.oForm = SBO_Application.Forms.Item("frmScAdmon");
                    this.SetDataSource();
                    //Estabkece la ventana centrada
                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                }
                else
                {
                    SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                }
            }
        }
       
        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
            }
            catch (Exception ex)
            {
                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                this.oForm.Select();

            }
        }

        public void Close()
        {
            if (this.oForm.Visible & this.oForm.Selected)
                this.oForm.Close();
        }

        public void SetDataSource()
        {
            try
            {
                string query = string.Format(@"select * from " + this.SucursalSAP.NombreTablaSucursalSAP + @" where ""U_EMPRESAID"" = {0} and ""U_ESACTIVO"" = 1 {1}", this.Configs.UserFull.Dependencia.EmpresaID, this.Filtro);
                if(oForm.DataSources.DataTables.Count <= 0)
                    oForm.DataSources.DataTables.Add("tabSucursal");
                oForm.DataSources.DataTables.Item("tabSucursal").Clear();
                oForm.DataSources.DataTables.Item("tabSucursal").ExecuteQuery(query);
                oMatrix = oForm.Items.Item("mtxScles").Specific;
                oMatrix.Columns.Item("#").DataBind.Bind("tabSucursal", "U_SUCURSALID");
                oMatrix.Columns.Item("clScNom").DataBind.Bind("tabSucursal", "U_DIRECCION");
                oMatrix.Columns.Item("clScDirec").DataBind.Bind("tabSucursal", "U_DIRECCIONREAL");
                oMatrix.Columns.Item("clScResp").DataBind.Bind("tabSucursal", "U_RESPONSABLE");
                oMatrix.LoadFromDataSource();
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        #endregion

        #region Auxiliares

        public void SetFiltro()
        {
            try
            {
                this.Filtro = string.Empty;
                oEditText = oForm.Items.Item("txtScBus").Specific;
                string filtro = oEditText.String;
                if(!string.IsNullOrEmpty(filtro))
                {
                    this.Filtro += string.Format(@"and ""U_DIRECCION"" like '%{0}%' ", filtro);
                    this.Filtro += string.Format(@"or ""U_DIRECCIONREAL"" like '%{0}%' ", filtro);
                    this.Filtro += string.Format(@"or ""U_RESPONSABLE"" like '%{0}%' ", filtro);
                    this.SetDataSource();
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public bool HasRowValue(int rowIndex)
        {
            try
            {
                this.oForm = this.SBO_Application.Forms.Item("frmScAdmon");
                oMatrix = oForm.Items.Item("mtxScles").Specific;
                int rows = oMatrix.RowCount;
                if (rowIndex > rows)
                    return false;
                EditText txtNombre = (SAPbouiCOM.EditText)oMatrix.GetCellSpecific("clScNom", rowIndex);
                oEditText = (SAPbouiCOM.EditText)oMatrix.GetCellSpecific("#", rowIndex);
                if(!string.IsNullOrEmpty(oEditText.String.Trim()) && !string.IsNullOrEmpty(txtNombre.String.Trim()))
                {
                    //this.Sucursal = new Sucursal();
                    //this.Sucursal.SucursalID = Convert.ToInt32(oEditText.String.Trim());
                    this.SucursalID = null;
                    this.Nombre = null;
                    this.SucursalID = Convert.ToInt32(oEditText.String.Trim());
                    this.Nombre = txtNombre.String.Trim();
                    return true;
                }
                this.SucursalID = null;
                this.Nombre = null;
                return false;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void PrepareToDelete()
        {
            try
            {
                if(this.SucursalID != null)
                {
                    this.Sucursal = new Sucursal();
                    this.Sucursal.SucursalID = this.SucursalID;
                    this.Sucursal.Direccion = this.Nombre;
                    this.Sucursal.Base = this.Configs.UserFull.Dependencia ?? new Empresa();
                    this.Sucursal.EsActivo = false;
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void ChangeForm()
        {
            try
            {
                this.oForm.Title = "Elegir Sucursal";
                Button btnSelect = oForm.Items.Item("btnScDel").Specific;
                btnSelect.Caption = "Seleccionar";
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public bool isDelete()
        {
            try
            {
                this.oForm = this.SBO_Application.Forms.Item("frmScAdmon");
                Button btnSelect = oForm.Items.Item("btnScDel").Specific;
                if (btnSelect.Caption.Contains("Borrar"))
                    return true;
                return false;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public string GetSucursalName()
        {
            try
            {
                return this.Nombre == null ? "Desconocido" : this.Nombre;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        #endregion
    }
}
