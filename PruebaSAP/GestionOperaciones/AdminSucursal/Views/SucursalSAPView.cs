﻿using Kanan.Operaciones.BO2;
using KananWS.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.GestionOperaciones.AdminSucursal.Views
{
    public class SucursalSAPView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.Matrix oMatrix;
        public string FormUniqueID { get; private set; }
        private SAPbouiCOM.DBDataSource oDBDataSource;
        private Configurations Configs;
        public Sucursal Sucursal { get; set; }
        private SAPbobsCOM.Recordset SucursalesRs { get; set; }
        private SAPbobsCOM.Company Company;
        private List<Sucursal> lstSucursales { get; set; }
        private int RowsCount { get; set; }
        #endregion

        #region Constructor
        public SucursalSAPView(SAPbouiCOM.Application Application, SAPbobsCOM.Company company, Configurations config, string formtype, int formcount) 
        {
            try
            {
                this.SBO_Application = Application;
                this.Configs = config;
                this.Company = company;
                this.oItem = null;
                this.oMatrix = null;
                this.SetForm(formtype, formcount);
                oDBDataSource = null;
                this.Sucursal = new Sucursal();
                this.Sucursal.Base = new Empresa();
                this.SucursalesRs = this.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                //this.GetAll();
                //this.lstSucursales = new List<Sucursal>();
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        private void GetAll()
        {
            try
            {
                if(this.SucursalesRs == null)
                    this.SucursalesRs = this.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                this.SucursalesRs.DoQuery(@"select * from ""OUBR""");
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void SetForm(string type, int count)
        {
            try
            {
                this.oForm = SBO_Application.Forms.GetForm(type, count);
                this.FormUniqueID = this.oForm.UniqueID;
            }
            catch (Exception ex)
            {
                this.oForm = null;
                throw new Exception(ex.Message);
            }
        }

        public int GetDataID(int rowIndex)
        {
            try
            {
                int id = 0;
                this.oMatrix = oForm.Items.Item("3").Specific;
                SAPbouiCOM.EditText txt = oMatrix.Columns.Item("#").Cells.Item(rowIndex).Specific;
                if (!string.IsNullOrEmpty(txt.String))
                    id = Convert.ToInt32(txt.String);
                return id;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public string ValidateData()
        {
            try
            {
                string err = string.Empty, mns = string.Empty;
                this.oMatrix = oForm.Items.Item("3").Specific;
                int rows = rows = oMatrix.RowCount;
                if(this.RowsCount < rows)
                {
                    this.RowsCount = 0;
                    SAPbouiCOM.EditText txtName = (SAPbouiCOM.EditText)oMatrix.GetCellSpecific("Name", rows - 1);
                    if (this.Configs.UserFull.Dependencia.EmpresaID == null)
                        mns += ", Empresa";
                    if (string.IsNullOrEmpty(txtName.String.Trim()))
                        mns += ", Nombre";
                    if (!string.IsNullOrEmpty(mns))
                    {
                        if (mns.StartsWith(","))
                            mns = mns.Substring(1);
                        err = "Los siguientes datos son requeridos: " + mns;
                    }
                    return err;
                }
                return "Iguales";
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void FormToData()
        {
            try
            {
                this.oMatrix = oForm.Items.Item("3").Specific;
                int rows = oMatrix.RowCount - 1;
                SAPbouiCOM.EditText txtName = (SAPbouiCOM.EditText)oMatrix.GetCellSpecific("Name", rows);
                SAPbouiCOM.EditText txtDescrip = (SAPbouiCOM.EditText)oMatrix.GetCellSpecific("Remarks", rows);
                if (!string.IsNullOrEmpty(txtName.String.Trim()))
                    this.Sucursal.Direccion = txtName.String.Trim();
                if (!string.IsNullOrEmpty(txtDescrip.String.Trim()))
                    this.Sucursal.Descripcion = txtDescrip.String.Trim();
                this.Sucursal.Base = this.Configs.UserFull.Dependencia ?? new Empresa();
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void GetRowsCount()
        {
            try
            {
                this.oMatrix = oForm.Items.Item("3").Specific;
                int rows = oMatrix.RowCount;
                this.RowsCount = rows;
                //return rows;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

    }
}
