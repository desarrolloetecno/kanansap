﻿using System;
using System.Collections.Generic;
using KananSAP.Vehiculos.Data;
using SAPbouiCOM;
using Application = System.Windows.Forms.Application;
using Kanan.Vehiculos.BO2;
using KananSAP.GestionVehiculos.IncidenciaVehiculo.Presenter;

namespace KananSAP.GestionVehiculos.IncidenciaVehiculo.Views
{
    public class InciVehiculoViews
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.StaticText oStaticText;
        private SAPbouiCOM.Matrix oMtx;
        private SAPbouiCOM.DBDataSource oDBDataSource;
        private SAPbouiCOM.Button oButton;
        private SAPbouiCOM.Conditions oConditions;
        private SAPbouiCOM.Condition oCondition;
        public InciVehiculoSAP Entity { get; set; }
        public string FormUniqueID { get; set; }
        private XMLPerformance XmlApplication;
        public int? id;
        public string name;
        public int? vehiculoid;
        public DateTime FechaInicio;
        public DateTime FechaFinal;
        #endregion

        #region Constructor
        public InciVehiculoViews(SAPbouiCOM.Application Application, SAPbobsCOM.Company company, string Name, int? id)
        {
            this.SBO_Application = Application;
            this.Entity = new InciVehiculoSAP(company);
            this.XmlApplication = new XMLPerformance(SBO_Application);
            this.oItem = null;
            this.oStaticText = null;
            this.oEditText = null;
            this.oConditions = null;
            this.oCondition = null;
            this.id = null;
            this.name = Name;
            this.vehiculoid = id;
        }
        #endregion

        #region Metodos
        #region Metodos Form
        public void LoadForm()
        {
            try
            {
                //this.oForm = SBO_Application.Forms.Item("GestInci");
                //BindDataToForm();
                try
                {
                    this.oForm = SBO_Application.Forms.Item("GestInci");
                }
                catch
                {
                    string sPath = Application.StartupPath;
                    this.XmlApplication.LoadFromXML(sPath + "\\XML", "GestionInci.xml");
                    
                }
                
                this.oForm = SBO_Application.Forms.Item("GestInci");
                
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;

                oItem = oForm.Items.Item("FInicio");
                oEditText = (EditText)(oItem.Specific);
                //oEditText.Value = Convert.ToString(DateTime.Today);
                //FechaInicio = "";
                //oEditText.Value = Convert.ToString(FechaInicio);
                //FInicio = Convert.ToString(FechaInicio);
                
                oItem = oForm.Items.Item("FFinal");
                oEditText = (EditText)(oItem.Specific);
                //oEditText.Value = Convert.ToString(DateTime.Today);
                //FechaFinal = "";
                //oEditText.Value = Convert.ToString(FechaFin);
                //FFin = Convert.ToString(FechaFin);

                oItem = oForm.Items.Item("lblvname");
                oStaticText = (StaticText)(oItem.Specific);
                oStaticText.Caption = this.name;

                oItem = oForm.Items.Item("lblvid");
                oStaticText = (StaticText)(oItem.Specific);
                string mItem = Convert.ToString(oItem.UniqueID);
                oStaticText.Caption = this.vehiculoid.ToString();
                string vehiid = vehiculoid.ToString();
                
                if (mItem == "lblvid" & vehiid != null)
                {
                    oItem.Visible = true;
                }
                else
                {
                    oItem.Visible = false;
                }
                
            }
            catch(Exception e)
            {
                throw new Exception("Algunos campos del formulario no se cargaron correctamente " + e.Message);
            }
        }

        public void ShowForm()
        {
            try
            {
                //this.oForm.Visible = true;
                //this.oForm.Select();
                //BindDataToForm();
                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                BindDataToForm();
                this.oForm.Select();
            }
            catch (Exception e)
            {
                throw new Exception("Ocurrio un problema al cargar mostrar el formulario " + e.Message);
            }
        }

        #endregion

        #region Metodos Matrix
        public void BindDataToForm()
        {
            try
            {
                //Genera consulta para llenar la Matriz con todos los datos R. Santos
                SetConditions();
                oDBDataSource = oForm.DataSources.DBDataSources.Item("@VSKF_INVEHICULO");
                oDBDataSource.Query(oConditions ?? null);
                oItem = oForm.Items.Item("MtxIN");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oMtx.LoadFromDataSource();
            }
            catch(Exception ex)
            {
                throw new Exception("Error al llenar la matriz " + ex);
            }
        }

        public string GetDataRow(int index)
        {
            try
            {
                oItem = oForm.Items.Item("MtxIN");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(1, index);
                return oEditText.String;
            }
            catch (Exception ex)
            {
                return "Error en la aplicacion " + ex;
            }
        }
        
        //Limpiar datos de la Mtx
        public void CleanoMtx()
        {
            try
            {
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oMtx.Clear();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar limpiar la matriz " + ex.Message);
            }
        }

        #endregion

        public DateTime GetFechaInicio()
        {
            try
            {
                oItem = oForm.Items.Item("FInicio");
                oEditText = (EditText)(oItem.Specific); ;
                FechaInicio = SplitFechas(oEditText.Value);
                return FechaInicio;
            }
            catch(Exception ex)
            {
                throw new Exception("Error al obtener fecha de inicio en el filtro, verifique los campos " + ex.Message);
            }
        }

        public DateTime GetFechaFinal()
        {
            try
            {
                oItem = oForm.Items.Item("FFinal");
                oEditText = (EditText)(oItem.Specific);
                FechaFinal = SplitFechas(oEditText.Value);
                return FechaFinal;
            }
            catch(Exception ex)
            {
                throw new Exception("Error al obtener fecha final en el filtro, verifique loos campos" + ex.Message);
            }
        }

        //Se creó este método para convertir fechas de DateTimePicker en fechas reconocibles para la consulta en SAP
        public DateTime SplitFechas( string TextoFecha)
        {
            try
            {
                var splitFechas = TextoFecha.ToCharArray();
                string tempSplit = Convert.ToString(splitFechas[0]) + Convert.ToString(splitFechas[1]) + Convert.ToString(splitFechas[2]) + Convert.ToString(splitFechas[3])
                    + "-" + Convert.ToString(splitFechas[4]) + Convert.ToString(splitFechas[5]) + "-" + Convert.ToString(splitFechas[6]) + Convert.ToString(splitFechas[7]);
                DateTime resultadoSplit = Convert.ToDateTime(tempSplit);
                return resultadoSplit;
            }
            catch (Exception ex)
            {
                throw new Exception("Error en la aplicacion. SplitFechas() " + ex.Message);
            }
        }

        public void SetID(int index)
        {
            try
            {
                oItem = oForm.Items.Item("MtxIN");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(1, index);
                try
                {
                    this.id = Convert.ToInt32(oEditText.String.Trim());
                }
                catch
                {
                    this.SBO_Application.StatusBar.SetText("Seleccione una incidencia", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int GetID(int index)
        {
            try
            {
                int xd = 0;
                oItem = oForm.Items.Item("MtxIN");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(1, index);
                try
                {
                    xd = Convert.ToInt32(oEditText.String.Trim());
                }
                catch
                {
                    this.SBO_Application.StatusBar.SetText("No se ha seleccionado una incidencia. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                }

                return xd;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SetConditions()
        {
            try
            {
                oConditions = new SAPbouiCOM.Conditions();
                oItem = oForm.Items.Item("lblvid");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                if (!string.IsNullOrEmpty(oStaticText.Caption))
                {
                    oCondition = oConditions.Add();
                    oCondition.Alias = "U_VehiculoID";
                    oCondition.Operation = BoConditionOperation.co_EQUAL;
                    oCondition.CondVal = oStaticText.Caption.Trim();
                    //oCondition.Relationship = BoConditionRelationship.cr_OR;
                }
                else
                {
                    oConditions = null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void LlenaroMtxFiltroFechas()
        {
            try
            {
                //Genera consulta para llenar la Matriz con todos los datos R. Santos
                CondicionesFiltroFechas();
                oDBDataSource = oForm.DataSources.DBDataSources.Item("@VSKF_INVEHICULO");
                //oDBDataSource.Query(oConditions ?? null);
                oDBDataSource.Query(oConditions ?? null);
                //SAPbobsCOM.Recordset rec = new SAPbobsCOM.Recordset();
                oItem = oForm.Items.Item("MtxIN");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oMtx.LoadFromDataSource();
            }
            catch(Exception ex)
            {
                throw new Exception("Error al llenar la matriz por filtro de fechas " + ex.Message);
            }
        }

        public void CondicionesFiltroFechas()
        {
            try
            {
                GetFechaInicio();
                GetFechaFinal();
                oConditions = new SAPbouiCOM.Conditions();  
                oItem = oForm.Items.Item("lblvid");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                if (!string.IsNullOrEmpty(oStaticText.Caption))
                {
                    //Las Fechas se manejan de la sig. manera: 20171231
                    //Lo cual indica el 31 de diciembre del 2017. R. Santos

                    //Se verifica que el mes sea de 2 digitos. R. Santos
                    string tempMonthInicio = Convert.ToString(FechaInicio.Month);
                    if (FechaInicio.Month <= 9 & tempMonthInicio.Length == 1)
                    {
                        tempMonthInicio = "0" + Convert.ToString(FechaInicio.Month);
                    }

                    //Se verifica que el mes sea de 2 digitos. R. Santos
                    string tempMonthFinal = Convert.ToString(FechaFinal.Month);
                    if (FechaFinal.Month <= 9 & tempMonthFinal.Length == 1)
                    {
                        tempMonthFinal = "0" + Convert.ToString(FechaFinal.Month);
                    }

                    //Se adquiere la fecha de inicio en el formato debido
                    string tempDayInicio = Convert.ToString(FechaInicio.Day);
                    if (FechaInicio.Day <= 9 & tempDayInicio.Length == 1)
                    {
                        tempDayInicio = "0" + Convert.ToString(FechaInicio.Day);
                    }
                    string tempFechaIncio = Convert.ToString(FechaInicio.Year) + tempMonthInicio + tempDayInicio;
                    //Se adquiere la fecha de fin en el formato debido
                    string tempDayFinal = Convert.ToString(FechaFinal.Day);
                    if (FechaFinal.Day <= 9 & tempDayFinal.Length == 1)
                    {
                        tempDayFinal = "0" + Convert.ToString(FechaFinal.Day);
                    }
                    string tempFechaFinal = Convert.ToString(FechaFinal.Year) + tempMonthFinal + tempDayFinal;
                    oCondition = oConditions.Add();
                    oCondition.Alias = "U_FechaIncidencia";
                    oCondition.Operation = BoConditionOperation.co_BETWEEN;
                    oCondition.CondVal = tempFechaIncio;
                    oCondition.CondEndVal = tempFechaFinal;
                }
                else
                {
                    oConditions = null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al filtrar fechas " + ex.Message);
            }
        }

        #endregion
    }
}
