﻿using System;
using System.Linq;
using Kanan.Costos.BO2;
using Kanan.Comun.BO2;
using Kanan.Vehiculos.BO2;
using KananSAP.Vehiculos.Data;
using KananWS.Interface;
using SAPbouiCOM;
using Application = System.Windows.Forms.Application;

namespace KananSAP.GestionVehiculos.IncidenciaVehiculo.Views
{
    public class DetalleIncidenciaVehiculoViews
    {

        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.StaticText oStaticText;
        private SAPbouiCOM.ComboBox oComboBox;
        private SAPbouiCOM.Button oButton;
        private SAPbouiCOM.DBDataSource oDBDataSource;
        private SAPbouiCOM.Conditions oConditions;
        private SAPbouiCOM.Condition oCondition;
        private string name;
        public int? vehiculoid;
        public int? Ccid;
        public InciVehiculoSAP Entity { get; set; }
        public CargaCombustibleSAP EntityCarga { get; set; }
        public string FormUniqueID { get; set; }
        private XMLPerformance XmlApplication;
        public Configurations configs;
        #endregion

        #region Constructor
        public DetalleIncidenciaVehiculoViews(SAPbouiCOM.Application Application, SAPbobsCOM.Company company, Configurations c, string Name, int? id, int? ccid) 
        {
            this.Entity = new InciVehiculoSAP(company);
            this.XmlApplication = new XMLPerformance(Application);
            this.SBO_Application = Application;
            this.oItem = null;
            this.oEditText = null;
            this.oStaticText = null;
            this.oComboBox = null;
            this.oForm = null;
            this.configs = c;
            this.name = Name;
            this.vehiculoid = id;
            this.Ccid = ccid;
        }
        #endregion

        #region Métodos

        #region FormMethods
        public void SetForm(string type, int count)
        {
            try
            {
                if (count > 1)
                {
                    this.oForm = SBO_Application.Forms.GetForm(type, count);
                    this.oForm.Close();
                    this.oForm = SBO_Application.Forms.GetForm(type, 1);
                    this.oForm.Select();
                    this.FormUniqueID = this.oForm.UniqueID;
                }
                else
                {
                    this.oForm = SBO_Application.Forms.GetForm(type, count);
                    this.FormUniqueID = this.oForm.UniqueID;
                }
            }
            catch (Exception ex)
            {
                this.oForm = null;
                throw new Exception(ex.Message);
                SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }
        }

        public void LoadForm()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("EInci");
            }
            catch
            {
                string sPath = Application.StartupPath;
                this.XmlApplication.LoadFromXML(sPath + "\\XML","EditarIncidencia.xml");
                
                //this.AddDataSources();
                this.oForm = SBO_Application.Forms.Item("EInci");
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                if (configs != null)
                    //FillComboBoxes();

                oItem = oForm.Items.Item("lblvehi");
                oStaticText = (StaticText) (oItem.Specific);
                oStaticText.Caption = this.name;

                oItem = oForm.Items.Item("lblid");
                oStaticText = (StaticText)(oItem.Specific);
                //Se Salva el nombre del item para comprar
                string nItem = Convert.ToString(oStaticText.Item.UniqueID);
                //Se salva el id del vehículo
                string idvehi = this.vehiculoid.ToString();
                if (nItem == "lblid" & idvehi != null)
                {
                    //Se hace visible el elemento y se le asigana el id del vehículo
                    oItem = this.oForm.Items.Item("txtvehiid");
                    oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                    oStaticText.Caption = Convert.ToString(idvehi);
                    oItem.Visible = true;

                    oItem = this.oForm.Items.Item("txtinid");
                    oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                    oStaticText.Caption = Convert.ToString(GetInciID());
                    oItem.Visible = true;

                    oItem = this.oForm.Items.Item("lblid");
                    oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                    oStaticText.Caption = Convert.ToString(idvehi);
                    oItem.Visible = true;
                }
                else
                {
                    oItem.Visible = false;
                }

                oItem = oForm.Items.Item("lblccid");
                oStaticText = (StaticText)(oItem.Specific);
                oStaticText.Caption = this.Ccid != null ? this.Entity.oUDT.Code.ToString() : string.Empty;
                oItem.Visible = true;
               
                if (Ccid == null)
                {
                    HideDeleteButton();
                }
                //else
                //    ShowDeleteButton();

                //Pendiente de agregar... R. Satos
                /*oItem = this.oForm.Items.Item("txthr");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = "00";

                oItem = this.oForm.Items.Item("txtmin");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = "00";*/
            }
        }

        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
            }
            catch (Exception)
            {
                this.LoadForm();
                this.oForm.Visible = true;
                this.oForm.Select();
            }
        }

        public void CloseForm()
        {
            if (this.oForm.Visible & this.oForm.Selected)
                this.oForm.Close();
        }

        public void HideDeleteButton()
        {
            oItem = oForm.Items.Item("btnelimina");
            oItem.Visible = false;

            oItem = oForm.Items.Item("btnphoto");
            oItem.Visible = false;
            Item hided = oItem;

            oItem = oForm.Items.Item("btnguardar");
            oItem.Left = hided.Left;

            oItem = oForm.Items.Item("btngeo");
            oItem.Visible = false;
            hided = oItem;

            oItem = oForm.Items.Item("2");
            oItem.Left = hided.Left;

            oItem = oForm.Items.Item("btnOS");
            oItem.Visible = false;

        }

        public void HidePhotoButtons()
        {
            oItem = oForm.Items.Item("btnphoto");
            oItem.Visible = false;

            oItem = oForm.Items.Item("btngeo");
            oItem.Visible = false;

            oItem = oForm.Items.Item("btnOS");
            oItem.Visible = false;
        }

        public void ShowDeleteButton()
        {
            oItem = oForm.Items.Item("btnelimina");
            oItem.Visible = true;

            oItem = oForm.Items.Item("btnphoto");
            oItem.Visible = true;
            Item hided = oItem;

            oItem = oForm.Items.Item("btnguardar");
            oItem.Left = hided.Left;

            oItem = oForm.Items.Item("btngeo");
            oItem.Visible = true;
            hided = oItem;

            oItem = oForm.Items.Item("2");
            oItem.Left = hided.Left;


            oItem = oForm.Items.Item("btnelimina");
            oItem.Visible = true;
            hided = oItem;

            oItem = oForm.Items.Item("2");
            oItem.Left = hided.Left + hided.Width + 3;
        }

        public bool LanzaFormulario()
        {
            CheckBox oChek = (CheckBox)this.oForm.Items.Item("chkos").Specific;
            return oChek.Checked;
        }

        public void FormToEntity()
        {
            DateTime? ShortDate = new DateTime();
            string fecha = "";
            string hh = "";
            string mm = "";
            this.oForm = SBO_Application.Forms.Item("EInci");

            try
            {
                //Se movió al final para evitar que al lanzar la ventana inicie apuntando al txtlon y elimine los datos que contiene.
                //De esta forma iniciará apuntando a la fecha. R.Santos
                /*oItem = this.oForm.Items.Item("txtfecha");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Entity.IncidenciaEnVehiculo.FechaIncidencia = String.IsNullOrEmpty(oEditText.String.Trim()) ? (DateTime?)null : DateTime.Parse(oEditText.String);*/

                oItem = this.oForm.Items.Item("txtinid");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                //Para agregar una nueva incidencia se verifica que el valor code sea "" y para editar se verifica que el valor Entity.oUDT.Code exista.
                this.Entity.IncidenciaEnVehiculo.IncidenciaID = string.IsNullOrEmpty(Entity.oUDT.Code) ? 0 : Convert.ToInt32(Entity.oUDT.Code);

                oItem = this.oForm.Items.Item("txtvehiid");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                this.Entity.IncidenciaEnVehiculo.Vehiculo.VehiculoID = Convert.ToInt32(oStaticText.Caption);

                oItem = this.oForm.Items.Item("txtdes");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Entity.IncidenciaEnVehiculo.Descripcion = Convert.ToString(oEditText.Value);

                oItem = this.oForm.Items.Item("txtlat");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Entity.IncidenciaEnVehiculo.Latitude = Convert.ToDouble(oEditText.Value);

                oItem = this.oForm.Items.Item("txtlon");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Entity.IncidenciaEnVehiculo.Longitude = Convert.ToDouble(oEditText.Value);

                oItem = this.oForm.Items.Item("hh");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                hh = oEditText.Value.Length < 2 ? "0" + oEditText.Value : oEditText.Value;
                //hh = oEditText.Value;
                //hh += "0"+hh;

                oItem = this.oForm.Items.Item("mm");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                mm = oEditText.Value.Length < 2 ? "0" + oEditText.Value : oEditText.Value;
                //mm = oEditText.Value;
                //mm += "0" + mm;

                oItem = this.oForm.Items.Item("txtfecha");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                ShortDate = String.IsNullOrEmpty(oEditText.String.Trim()) ? (DateTime?)null : DateTime.Parse(oEditText.String);
                fecha = ShortDate.Value.ToShortDateString();
                fecha = fecha + " " + hh + ":" + mm + ":00";

                this.Entity.IncidenciaEnVehiculo.FechaIncidencia = String.IsNullOrEmpty(oEditText.String.Trim()) ? (DateTime?)null : DateTime.Parse(fecha);

                oItem = this.oForm.Items.Item("chkos");
                CheckBox oChkOS = (SAPbouiCOM.CheckBox)(oItem.Specific);
                this.Entity.IncidenciaEnVehiculo.CreaOrdenServicio = oChkOS.Checked ? 1 : 0;
            }
            catch(Exception ex)
            {
                //throw new Exception(ex.Message);
                SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }
        }

        public void EntityToForm()
        {
            this.oForm = SBO_Application.Forms.Item("EInci");

            DateTime Fecha = new DateTime();
            //Se movió al final para evitar que al lanzar la ventana inicie apuntando al txtlon y elimine los datos que contiene.
            //De esta forma iniciará apuntando a la fecha. R.Santos
            /*oItem = this.oForm.Items.Item("txtfecha");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = this.Entity.IncidenciaEnVehiculo.FechaIncidencia == null ? string.Empty : Entity.IncidenciaEnVehiculo.FechaIncidencia.Value.ToShortDateString();*/

            oItem = this.oForm.Items.Item("txtinid");
            oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            //Comentano para probrobar solución de campo U_IncidenciaID (null) R. Santos.
            //oStaticText.Caption = Convert.ToString(this.Entity.IncidenciaEnVehiculo.IncidenciaID);
            oStaticText.Caption = Convert.ToString(this.Entity.IncidenciaEnVehiculo.IncidenciaID) == null 
            ? Convert.ToString(this.Entity.oUDT.Code) : Convert.ToString(this.Entity.IncidenciaEnVehiculo.IncidenciaID);

            oItem = this.oForm.Items.Item("txtvehiid");
            oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            oStaticText.Caption = Convert.ToString(this.Entity.IncidenciaEnVehiculo.Vehiculo.VehiculoID);
            //oStaticText.Caption = this.Entity.IncidenciaEnVehiculo.Vehiculo.VehiculoID == null ? "No definido" : Convert.ToString(this.Entity.IncidenciaEnVehiculo.Vehiculo.VehiculoID);

            oItem = this.oForm.Items.Item("txtdes");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = Convert.ToString(this.Entity.IncidenciaEnVehiculo.Descripcion);

            oItem = this.oForm.Items.Item("txtlat");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            //Se agregó al final la asignación del valor 0 para evitar errores en la validación por estar el campo oculto en "Editar incidencia" R. Santos.
            oEditText.String = Convert.ToString(this.Entity.IncidenciaEnVehiculo.Latitude = 0);

            oItem = this.oForm.Items.Item("txtlon");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            //Se agregó al final la asignación del valor 0 para evitar errores en la validación por estar el campo oculto en "Editar incidencia" R. Santos.
            oEditText.String = Convert.ToString(this.Entity.IncidenciaEnVehiculo.Longitude = 0);

            Fecha = Convert.ToDateTime(this.Entity.IncidenciaEnVehiculo.FechaIncidencia);

            oItem = this.oForm.Items.Item("hh");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.Value = Convert.ToString(Fecha.Hour);

            oItem = this.oForm.Items.Item("mm");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.Value = Convert.ToString(Fecha.Minute);

            oItem = this.oForm.Items.Item("txtfecha");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = this.Entity.IncidenciaEnVehiculo.FechaIncidencia == null ? string.Empty : Entity.IncidenciaEnVehiculo.FechaIncidencia.Value.ToShortDateString();

            oItem = this.oForm.Items.Item("chkos");
            CheckBox oChkOS = (SAPbouiCOM.CheckBox)(oItem.Specific);
            oChkOS.Checked = this.Entity.IncidenciaEnVehiculo.CreaOrdenServicio == 1;
        }

        public void EmptyEntityToForm()
        {
            /*oItem = this.oForm.Items.Item("txtfecha");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = DateTime.Now.ToShortDateString();*/

            oItem = this.oForm.Items.Item("txtinid");
            oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            oStaticText.Caption = Convert.ToString(this.Entity.IncidenciaEnVehiculo.IncidenciaID) == null
            ? Convert.ToString(this.Entity.oUDT.Code) : Convert.ToString(this.Entity.IncidenciaEnVehiculo.IncidenciaID);

            oItem = this.oForm.Items.Item("txtvehiid");
            oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            oStaticText.Caption = string.Empty;

            oItem = this.oForm.Items.Item("txtdes");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtlat");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtlon");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("hh");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.Value = string.Empty;

            oItem = this.oForm.Items.Item("mm");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.Value = string.Empty;

            //Se movió al final para evitar que al lanzar la ventana inicie apuntando al txtlon y elimine los datos que contiene.
            //De esta forma iniciará apuntando a la fecha. R.Santos
            oItem = this.oForm.Items.Item("txtfecha");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = DateTime.Now.ToShortDateString();
        }

        public void ValidateData()
        {
            try
            {
                this.FormToEntity();
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
                SBO_Application.StatusBar.SetText(ex.Message.ToString(), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }

            if (this.Entity.IncidenciaEnVehiculo.FechaIncidencia == null || this.Entity.IncidenciaEnVehiculo.FechaIncidencia > DateTime.Now)
            {
                SBO_Application.StatusBar.SetText("El valor 'Fecha de la incidencia' es obligatorio y no debe ser mayor a la fecha actual.", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }

            //La validación se realizó en el momento de la captura de los valores del Form.

            /*oItem = this.oForm.Items.Item("hh");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (string.IsNullOrEmpty(oEditText.Value) || oEditText.Value.Length < 2)
            {
                throw new Exception("El formato de horas es incorrecto");
            }

            oItem = this.oForm.Items.Item("mm");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (string.IsNullOrEmpty(oEditText.Value) || oEditText.Value.Length < 2)
            {
                throw new Exception("El formato de minutos es incorrecto");
            }*/

            if (this.Entity.IncidenciaEnVehiculo.Descripcion == null || this.Entity.IncidenciaEnVehiculo.Descripcion.Length <= 0)
            {
                //throw new Exception("El valor 'Descripcion' es obligatorio y debe tener al menos un carácter.");
                SBO_Application.StatusBar.SetText("El valor 'Descripcion' es obligatorio y debe tener al menos un carácter.", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }
            //Se igualaron a para evitar capturar los valores.
            if (this.Entity.IncidenciaEnVehiculo.Latitude == null || this.Entity.IncidenciaEnVehiculo.Latitude != 0)
            {
                //throw new Exception("El valor 'Latitud' es obligatorio y debe ser mayor a cero.");
                SBO_Application.StatusBar.SetText("El valor 'Latitud' es obligatorio y debe ser mayor a cero.", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }
            //Se igualaron a para evitar capturar los valores.
            if (this.Entity.IncidenciaEnVehiculo.Longitude == null || this.Entity.IncidenciaEnVehiculo.Longitude != 0)
            {
                //throw new Exception("El valor 'Longitud' es obligatorio y debe ser mayor a cero.");
                SBO_Application.StatusBar.SetText("El valor 'Longitud' es obligatorio y debe ser mayor a cero.", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }
            if (this.Entity.IncidenciaEnVehiculo.Vehiculo.VehiculoID == null || this.Entity.IncidenciaEnVehiculo.Vehiculo.VehiculoID <= 0)
            {
                //throw new Exception("El valor 'Incidencia Vehiculo ID' es obligatorio y debe ser mayor a cero.");
                SBO_Application.StatusBar.SetText("El valor 'Incidencia Vehiculo ID' es obligatorio y debe ser mayor a cero.", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }
        }
        #endregion

        #region ItemsMethods
        //Region
        #endregion

        #region Métodos Auxiliares

        private bool IsNumeric(string value)
        {
            try
            {
                Convert.ToInt32(value);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string GetID()
        {
            oItem = oForm.Items.Item("lblid");
            oStaticText = (StaticText)(oItem.Specific);
            return oStaticText.Caption;
        }

        public string GetInciID()
        {
            //Se verifica que se tomen los valores de la tabla. R.Santos
            //this.FormToEntity();
            /*string incidenid = Convert.ToString(this.Entity.oUDT.Code);
            return incidenid;*/

            //Este método influye en el momendo de agregar una incidencia nueva.
            //Se recomienda no cambiar. R. Santos.
            oItem = oForm.Items.Item("lblccid");
            oStaticText = (StaticText)(oItem.Specific);
            return oStaticText.Caption;
        }

        public void CargarIncidencia()
        {
            //SetConditions();
            oDBDataSource = oForm.DataSources.DBDataSources.Item("@VSKF_INVEHICULO");
            oDBDataSource.Query(oConditions ?? null);

        }

        public void SetConditions()
        {
            try
            {
                oConditions = new SAPbouiCOM.Conditions();
                oItem = oForm.Items.Item("lblvid");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                if (!string.IsNullOrEmpty(oStaticText.Caption))
                {
                    oCondition = oConditions.Add();
                    oCondition.Alias = "U_VehiculoID";
                    oCondition.Operation = BoConditionOperation.co_EQUAL;
                    oCondition.CondVal = oStaticText.Caption.Trim();
                    //oCondition.Relationship = BoConditionRelationship.cr_OR;
                }
                else
                {
                    oConditions = null;
                }
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
                SBO_Application.StatusBar.SetText("Error en condiciones" + ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }
        }

        #endregion

        #endregion

    }
}
