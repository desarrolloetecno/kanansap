﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kanan.Vehiculos.BO2;
using KananSAP.GestionVehiculos.IncidenciaVehiculo.Views;
 using KananSAP.GestionVehiculos.IncidenciaVehiculo.Presenter;
using KananSAP.Reportes.Presenter;
using KananWS.Interface;
using KananSAP.Helper;
using SAPbouiCOM;
using SAPbobsCOM;
using KananFleet.OrdenesServicio.SolicitarOrdenServicio.Views;

namespace KananSAP.GestionVehiculos.IncidenciaVehiculo.Presenter
{
    public class InciVehiculoPresenter
    {
        #region Atributo
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        public InciVehiculoViews view { get; set; }
        private SBO_KF_SolicitaOrdenesView SoliciudOrden = null;
        public DetalleIncidenciaVehiculoViews detalleview { get; set; }
        private InciVehiculoWS.LogSAPInciVehiculo log;
        private Configurations configs;
        private InciVehiculoWS incidenciaws;
        private GestionGlobales oGlobales = null;
        bool EsSQL;
        private bool Modal;
        public int a;
        private bool ModalEditarIncidencia;
        private string Link;
        private int VehiculoID = -1;

        public Reportes.Presenter.ReportePresenter ReportesPresenter { get; set; }
        //private string EndPoint;
        #endregion

        #region Constructor
        public InciVehiculoPresenter(SAPbouiCOM.Application aplication, SAPbobsCOM.Company company, int id, string vehiclename, Configurations c, GestionGlobales oGlobales)
        {
            this.VehiculoID = id;
            this.oGlobales = oGlobales;
            this.configs = c;
            this.SBO_Application = aplication;
            this.Company = company;
            this.view = new InciVehiculoViews(this.SBO_Application, this.Company, vehiclename, id);
            //this.cargaview = new RecargaView(this.SBO_Application, this.Company, this.configs, vehiclename, id, null);
            this.incidenciaws = new InciVehiculoWS(Guid.Parse(this.configs.UserFull.Usuario.PublicKey), this.configs.UserFull.Usuario.PrivateKey);
            this.log = new InciVehiculoWS.LogSAPInciVehiculo();
            this.Modal = false;
            this.Link = "Vehiculos/ImagenesIncidenciaVehiculo.aspx?_modal=true&IncidenciaId=";
            this.ModalEditarIncidencia = false;
            this.a = 0;

            //this.EndPoint = System.Configuration.ConfigurationManager.AppSettings.Get("EndPoint");
            SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
            EsSQL = Company.DbServerType.ToString().Contains("MSSQL") ? true : false;
        }
        #endregion

        #region Eventos
        private void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (pVal.FormTypeEx == "frmAddOS")
                {
                    if (this.SoliciudOrden != null)
                        this.SoliciudOrden.SBO_Application_ItemEvent(FormUID, ref pVal, out BubbleEvent);
                }

                if (pVal.FormTypeEx == "GestInci")
                {
                    if (Modal)
                    {
                        Modal = false;
                        detalleview.ShowForm();
                        BubbleEvent = false;
                        return;
                    }

                    #region beforeaction
                    if (pVal.BeforeAction)
                    {
                        //if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnSearch")
                        //{
                        //    this.view.BindDataToForm();
                        //}

                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            if (pVal.Row == 1)
                            {
                                //this.a = this.view.GetID(pVal.Row);
                                if (this.view.GetID(pVal.Row) > 0)
                                    this.a = this.view.GetID(pVal.Row);
                            }
                        }
                        if (pVal.EventType == BoEventTypes.et_MATRIX_LINK_PRESSED)
                        {
                            this.view.SetID(pVal.Row);
                            if (this.view.id != null)
                            {
                                this.detalleview = new DetalleIncidenciaVehiculoViews(this.SBO_Application, this.Company, configs, view.name, view.vehiculoid, view.id);
                                var code = ((int)this.view.id).ToString().Trim();
                                var flag = this.detalleview.Entity.oUDT.GetByKey(code);
                                if (!flag)
                                {
                                    int errornum;
                                    string errormsj;
                                    Company.GetLastError(out errornum, out errormsj);
                                    throw new Exception("Error: " + errornum + " - " + errormsj);
                                }
                                this.detalleview.LoadForm();
                                PrepareUserForm();
                                this.detalleview.ShowForm();
                            }
                            this.Modal = true;
                            BubbleEvent = false;
                        }

                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnnew")
                        {
                            this.detalleview = null;
                            this.detalleview = new DetalleIncidenciaVehiculoViews(this.SBO_Application, this.Company, configs, view.name, view.vehiculoid, null);
                            this.detalleview.LoadForm();
                            this.detalleview.ShowForm();
                            this.Modal = true;
                            BubbleEvent = false;
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btncon")
                        {
                            //this.view.FillGrid();
                            //this.ExtraerIncidenciaKanan();
                            this.view.CleanoMtx();
                            this.view.LlenaroMtxFiltroFechas();
                            BubbleEvent = false;

                        }

                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnext")
                        {
                            this.view.CleanoMtx();
                            this.ExtraerIncidenciaKanan();
                            //this.ActualizarCampos();
                            this.view.BindDataToForm();
                            BubbleEvent = false;

                        }
                    }
                    #endregion

                    #region After Action
                    else
                    {
                        //Se Agrego el BubbleEvent
                        //BubbleEvent = true;
                        //if (Modal && pVal.EventType == BoEventTypes.et_FORM_ACTIVATE)
                        //{
                        //    userview.ShowForm();
                        //    BubbleEvent = false;
                        //    return;
                        //}
                    }
                    #endregion
                }

                if (pVal.FormTypeEx == "EInci")
                {
                    #region Modales

                    #endregion

                    #region beforeaction
                    //  If the modal form is closed...
                    //if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_CLOSE & Modal)
                    //{
                    //    Modal = false;
                    //}
                    if (pVal.BeforeAction)
                    {
                        #region
                      
                        if (pVal.ItemUID == "btnguardar" && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED && Modal)
                        {
                            this.detalleview.ValidateData();
                            //this.Update();
                            this.GuardarIncidenciaVehiculo();
                            this.detalleview.CloseForm();
                            this.view.BindDataToForm();
                            BubbleEvent = false;
                        }
                        if (pVal.ItemUID == "btnelimina" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED && Modal)
                        {
                            this.Delete();
                            BubbleEvent = false;
                            this.detalleview.CloseForm();
                            this.view.BindDataToForm();
                        }

                        /*if (pVal.ItemUID == "btnphoto" && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED && Modal)
                        {
                            this.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepFotos", @EndPoint + "Vehiculos/ImagenesIncidenciaVehiculo.aspx?_modal=true&IncidenciaId=" + this.a);
                            //Link += this.detalleview.Entity.IncidenciaEnVehiculo.IncidenciaID;
                            BubbleEvent = false;
                            GestionGlobales.ventanaAbierta = false;
                            this.ReportesPresenter.view.ShowForm();
                        }*/

                        /*if (pVal.ItemUID == "btngeo" && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED && Modal)
                        {
                            this.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepGeopos", @EndPoint + "Vehiculos/GeoposicionRecarga.aspx?_modal=true&incive=1&IncidenciaVehiculoID=" + this.a);
                            this.ReportesPresenter.view.ShowForm();
                            ModalGeo = true;
                            BubbleEvent = false;
                        }*/
                        if (pVal.EventType == BoEventTypes.et_FORM_UNLOAD)
                        {
                            this.Modal = false;
                            BubbleEvent = false;
                        }
                        #endregion
                    }
                    #endregion

                    #region After Action
                    else
                    {
                        if (pVal.ItemUID == "btnOS" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED && Modal)
                        {
                            if (this.detalleview.LanzaFormulario())
                                this.SoliciudOrden = new SBO_KF_SolicitaOrdenesView(this.oGlobales, this.configs, ref this.Company, "inc", "incidencia", IncidenciaID: this.detalleview.Entity.IncidenciaEnVehiculo.IncidenciaID);
                        }
                        //Se agrego el BubbleEvent
                        //BubbleEvent = true;
                        //if (pVal.EventType == BoEventTypes.et_FORM_LOAD)
                        //{
                        //    if (this.view.id != null)
                        //    {
                        //        Exist = this.userview.Entity.SetEmployeeRelationById((int)this.view.id);
                        //        PrepareUserForm();
                        //    }
                        //}
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Modal = false;
                BubbleEvent = false;
                SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                KananSAPHerramientas.LogError("GestionIncidenciaPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
            }
        }
        #endregion

        #region Métodos

        #region Acciones
        private void GuardarIncidenciaVehiculo()
        {
            try
            {
                if (string.IsNullOrEmpty(detalleview.GetInciID().Trim()))
                {
                    Insertar();
                }
                else
                {
                    Update();
                }
            }
            catch (Exception ex)
            {
                SBO_Application.StatusBar.SetText("Error al insertar. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoVehiculoPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
                return;
            }
        }

        private void Update()
        {
            try
            {
                this.detalleview.FormToEntity();
                this.detalleview.Entity.IncidenciaEnVehiculo = 
                    this.incidenciaws.Update(this.detalleview.Entity.IncidenciaEnVehiculo, this.configs.UserFull.Configs["TimeZone"]);
                this.detalleview.Entity.ItemCode = this.detalleview.Entity.IncidenciaEnVehiculo.IncidenciaID.ToString();
                this.detalleview.Entity.IncidenciaVehiculoToSAP();
                this.detalleview.Entity.Update();
                this.detalleview.EmptyEntityToForm();
                this.SBO_Application.StatusBar.SetText("Operación exitosa", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoVehiculoPresenter.cs->Update()", ex.Message);
                SBO_Application.StatusBar.SetText("Error al actualizar . " + ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }
        }

        private void Insertar()
        {
            try
            {
                this.Company.StartTransaction();

                this.detalleview.FormToEntity();
                this.log = this.incidenciaws.Add(this.detalleview.Entity.IncidenciaEnVehiculo, this.configs.UserFull.Configs["TimeZone"]);
                if (log.Status)
                {
                    this.detalleview.Entity.IncidenciaEnVehiculo = log.IncidenciaEnVehiculo;
                    this.detalleview.Entity.ItemCode = log.IncidenciaEnVehiculo.IncidenciaID.ToString();
                    this.detalleview.Entity.IncidenciaVehiculoToSAP();
                    this.detalleview.Entity.Insert();
                    if (this.Company.InTransaction)
                        this.Company.EndTransaction(BoWfTransOpt.wf_Commit);
                    if(log.IncidenciaEnVehiculo.CreaOrdenServicio == 1)
                        this.SoliciudOrden = new SBO_KF_SolicitaOrdenesView(this.oGlobales, this.configs, ref this.Company, "inc", "incidencia", IncidenciaID: this.detalleview.Entity.IncidenciaEnVehiculo.IncidenciaID);
                    this.detalleview.EmptyEntityToForm();
                    this.SBO_Application.StatusBar.SetText("Operación exitosa", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                }
                else
                {
                    throw new Exception(log.Error);
                }
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
                
                if (this.Company.InTransaction)
                    this.Company.EndTransaction(BoWfTransOpt.wf_RollBack);

                SBO_Application.StatusBar.SetText("Error al insertar. " + ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoVehiculoPresenter.cs->Insert()", ex.Message);
                return;
            }
        }

        private void Delete()
        {
            try
            {
                this.detalleview.Entity.IncidenciaEnVehiculo = this.incidenciaws.Delete(this.detalleview.Entity.IncidenciaEnVehiculo.IncidenciaID);
                this.detalleview.Entity.ItemCode = this.detalleview.Entity.IncidenciaEnVehiculo.IncidenciaID.ToString();
                this.detalleview.Entity.Delete();
                this.detalleview.EmptyEntityToForm();
                this.SBO_Application.StatusBar.SetText("Operación exitosa", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoVehiculoPresenter.cs->Delete()", ex.Message);
                SBO_Application.StatusBar.SetText("Error al eliminar. " + ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }
        }
        #endregion

        #region Métodos Auxiliares
        private void PrepareUserForm()
        {
            try
            {
                var id = this.detalleview.Entity.oUDT.Code;
                var i = Convert.ToInt32(id);
                this.detalleview.Entity.ItemCode = i.ToString();
                this.detalleview.Entity.SAPToIncidenciaVehiculo();
                this.detalleview.EntityToForm();
                if (!this.detalleview.Entity.IncidenciaEnVehiculo.Longitude.HasValue )
                {
                    this.detalleview.HidePhotoButtons();
                }

            }
            catch (Exception ex)
            {
                this.detalleview.EmptyEntityToForm();
                this.detalleview.CloseForm();
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoVehiculoPresenter.cs->PrepareUserForm()", ex.Message);
                SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }
        }

        private void ExtraerIncidenciaKanan()
        {
            var lstSAP = new List<IncidenciaVehiculox>();
            var lstKF = new List<IncidenciaVehiculox>();
            this.detalleview = new DetalleIncidenciaVehiculoViews(this.SBO_Application, this.Company, configs, view.name, view.vehiculoid, null);
            DateTime? date = new DateTime();

            try
            {
                Recordset rs = this.detalleview.Entity.GetByVehiculoID(view.vehiculoid);

                if (rs.RecordCount > 0)
                {
                    lstSAP = detalleview.Entity.RecordSetToListIncidenciaVehiculo(rs);
                    date = lstSAP.Max(x => x.FechaIncidencia);
                    if (date != null)
                    {
                        lstKF = incidenciaws.GetByVehicleID((int)detalleview.vehiculoid);
                        //lstKF = UtcToObj(lstKF);
                        InsertarIncidenciaKF(lstSAP, lstKF);
                    }
                }
                else
                {
                    lstKF = incidenciaws.GetByVehicleID((int)detalleview.vehiculoid);
                    //lstKF = UtcToObj(lstKF);
                    InsertarIncidenciaKF(lstSAP, lstKF);
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoVehiculoPresenter.cs->ExtraerIncidenciaKanan()", ex.Message);
                SBO_Application.StatusBar.SetText("Error al extraer. " + ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }
        }

        private void InsertarIncidenciaKF(IEnumerable<IncidenciaVehiculox> sap, IEnumerable<IncidenciaVehiculox> kf)
        {
            try
            {
                var lst = (from IncidenciaEnVehiculo in kf where sap.All(x => x.IncidenciaID != IncidenciaEnVehiculo.IncidenciaID) select (IncidenciaVehiculox)IncidenciaEnVehiculo.Clone()).ToList();
                foreach (IncidenciaVehiculox IncidenciaEnVehiculo in lst)
                {
                    this.detalleview.Entity.IncidenciaEnVehiculo = IncidenciaEnVehiculo;
                    this.detalleview.Entity.ItemCode = IncidenciaEnVehiculo.IncidenciaID.ToString();
                    this.detalleview.Entity.IncidenciaVehiculoToSAP();
                    this.detalleview.Entity.Insert();

                    //Se insertan los valores en la base de datos local.
                    //this.Insertar();
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoVehiculoPresenter.cs->InsertarIncidenciasKF()", ex.Message);
                SBO_Application.StatusBar.SetText("Error al insertar, Metodo extraer. " + ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }
        }

        private void ActualizarCampos()
        {
            var lstSAP = new List<IncidenciaVehiculox>();
            var lstKF = new List<IncidenciaVehiculox>();
            this.detalleview = new DetalleIncidenciaVehiculoViews(this.SBO_Application, this.Company, configs, view.name, view.vehiculoid, null);
            DateTime? date = new DateTime();

            try
            {
                Recordset rs = this.detalleview.Entity.GetByVehiculoID(view.vehiculoid);

                if (rs.RecordCount > 0)
                {
                    lstSAP = detalleview.Entity.RecordSetToListIncidenciaVehiculo(rs);
                    date = lstSAP.Max(x => x.FechaIncidencia);
                    if (date != null)
                    {
                        lstKF = incidenciaws.GetByVehicleID((int)detalleview.vehiculoid);

                        //Se transforma lstKF a UTC
                        lstKF = UtcToObj(lstKF);
                        
                        int tLista = 0;
                        bool bKF = false;
                        bool bSAP = false;
                        List<IncidenciaVehiculox> interseccion = new List<IncidenciaVehiculox>();

                        if (lstSAP.Count > lstKF.Count)
                        {
                            tLista = lstSAP.Count;
                            bSAP = true;
                            bKF = false;
                        }
                        else
                        {
                            tLista = lstKF.Count;
                            bKF = true;
                            bSAP = false;
                        }

                        for (int i = 0; i < tLista; i++)
                        {
                            if (bSAP == true)
                            {
                                for (int j = 0; j < lstKF.Count; j++ )
                                {
                                    if (lstSAP[i].IncidenciaID == lstKF[j].IncidenciaID)
                                    {
                                        if (lstKF[j].ImagenesIncidenciaAntes != null)
                                            if (lstKF[j].ImagenesIncidenciaAntes.ImagenID == null)
                                                lstKF[j].ImagenesIncidenciaAntes = null;
                                        if (lstKF[j].ImagenesIncidenciaDespues != null)
                                            if(lstKF[j].ImagenesIncidenciaDespues.ImagenID == null)
                                                lstKF[j].ImagenesIncidenciaDespues = null;

                                        if (lstSAP[i].ImagenesIncidenciaAntes != null)
                                            if (lstSAP[i].ImagenesIncidenciaAntes.ImagenID == null)
                                                lstSAP[i].ImagenesIncidenciaAntes = null;
                                        if (lstSAP[i].ImagenesIncidenciaDespues != null)
                                            if(lstSAP[i].ImagenesIncidenciaDespues.ImagenID == null)
                                                lstSAP[i].ImagenesIncidenciaDespues = null;

                                        if (!lstSAP[i].Equals(lstKF[j]))
                                        {
                                            interseccion.Add(lstSAP[i]);
                                        }
                                    }
                                }
                                bKF = false;
                            }
                            if (bKF == true)
                            {
                                for (int k = 0; k < lstSAP.Count; k++)
                                {
                                    if (lstKF[i].IncidenciaID == lstSAP[k].IncidenciaID)
                                    {
                                        if (lstKF[k].ImagenesIncidenciaAntes != null)
                                            if (lstKF[k].ImagenesIncidenciaAntes.ImagenID == null)
                                                lstKF[k].ImagenesIncidenciaAntes = null;
                                        if (lstKF[k].ImagenesIncidenciaDespues != null)
                                            if (lstKF[k].ImagenesIncidenciaDespues.ImagenID == null)
                                                lstKF[k].ImagenesIncidenciaDespues = null;

                                        if (lstSAP[i].ImagenesIncidenciaAntes != null)
                                            if (lstSAP[i].ImagenesIncidenciaAntes.ImagenID == null)
                                                lstSAP[i].ImagenesIncidenciaAntes = null;
                                        if (lstSAP[i].ImagenesIncidenciaDespues != null)
                                            if (lstSAP[i].ImagenesIncidenciaDespues.ImagenID == null)
                                                lstSAP[i].ImagenesIncidenciaDespues = null;

                                        if (!lstKF[i].Equals(lstSAP[k]))
                                        {
                                            interseccion.Add(lstKF[i]);
                                        }
                                    }
                                }
                                bSAP = false;
                            }
                        }

                        if (interseccion.Count > 0)
                        {
                            for (int l = 0; l < interseccion.Count; l++ )
                            {
                                this.detalleview.Entity.IncidenciaEnVehiculo = null;
                                this.detalleview.Entity.IncidenciaEnVehiculo = new IncidenciaVehiculox() { Vehiculo = new Vehiculo() };
                                this.detalleview.Entity.IncidenciaEnVehiculo = interseccion[l];
                                this.detalleview.Entity.ItemCode = interseccion[l].IncidenciaID.ToString();
                                this.detalleview.Entity.IncidenciaEnVehiculo = this.incidenciaws.Update(interseccion[l], this.configs.UserFull.Configs["TimeZone"]);
                                //this.detalleview.Entity.ItemCode = this.detalleview.Entity.IncidenciaEnVehiculo.IncidenciaID.ToString();
                                this.detalleview.Entity.IncidenciaVehiculoToSAP();
                                this.detalleview.Entity.Update();

                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoVehiculoPresenter.cs->ActualizarCampos()", ex.Message);
                SBO_Application.StatusBar.SetText("Error al sincronizar campos con fueldata. " + ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }
        }

        private void InsertarIncidenciaKF(IEnumerable<IncidenciaVehiculox> kf)
        {
            try
            {
                foreach (IncidenciaVehiculox IncidenciaEnVehiculo in kf)
                {
                    this.detalleview.Entity.IncidenciaEnVehiculo = IncidenciaEnVehiculo;
                    this.detalleview.Entity.ItemCode = IncidenciaEnVehiculo.IncidenciaID.ToString();
                    this.detalleview.Entity.IncidenciaVehiculoToSAP();
                    this.detalleview.Entity.Insert();
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoVehiculoPresenter.cs->InsertarIncidenciaKF()", ex.Message);
                SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }
        }

        #region UtcToObj
        public List<IncidenciaVehiculox> UtcToObj(List<IncidenciaVehiculox> FormaBase)
        {
            try
            {
                List<IncidenciaVehiculox> Lista = FormaBase;
                NodaTimeHelper.Services.DateTimeHelper Utc = new NodaTimeHelper.Services.DateTimeHelper(configs.UserFull.Configs["TimeZone"]);

                for (int NoCiclo = 0; NoCiclo < FormaBase.Count; NoCiclo++)
                {
                    Lista[NoCiclo].FechaIncidencia = Utc.ToUserTime(Convert.ToDateTime(FormaBase[NoCiclo].FechaIncidencia));
                }
                return Lista;
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoVehiculoPresenter.cs->UtcToObj()", ex.Message);
                throw new Exception(ex.Message);
                return null;
            }
        }
        #endregion

        #endregion

        #endregion
    }
}