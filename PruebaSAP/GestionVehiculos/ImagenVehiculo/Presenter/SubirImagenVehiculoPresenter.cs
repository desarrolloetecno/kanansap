﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using KananSAP.GestionVehiculos.ImagenVehiculo.View;
using KananSAP.Helper;
using KananWS.Interface;
using SAPbouiCOM;

namespace KananSAP.GestionVehiculos.ImagenVehiculo.Presenter
{
   public class SubirImagenVehiculoPresenter
    {
       #region Atributo
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private SAPbouiCOM.Item oItem;
        public SubirImagenVehiculoView view { get; set; }
        private Configurations configs;
        bool EsSQL;
        private bool Modal;
        public int a;
        private bool ModalEditarIncidencia;
        private string Link;

        public Reportes.Presenter.ReportePresenter ReportesPresenter { get; set; }
        private string EndPoint;
        #endregion

        #region Constructor
        public SubirImagenVehiculoPresenter(SAPbouiCOM.Application aplication, SAPbobsCOM.Company company, int id, Configurations c)
        {
            this.configs = c;
            this.SBO_Application = aplication;
            this.Company = company;
            this.Modal = false;
            this.Link = "Vehiculos/ImagenesIncidenciaVehiculo.aspx?_modal=true&IncidenciaId=";
            this.ModalEditarIncidencia = false;
            this.a = 0;

            this.EndPoint = System.Configuration.ConfigurationManager.AppSettings.Get("EndPoint");
            SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
            EsSQL = Company.DbServerType.ToString().Contains("MSSQL") ? true : false;
        }
        #endregion

        #region Eventos
        private void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (pVal.FormTypeEx == "ImgVh")
                {
                    if (Modal)
                    {
                        Modal = false;
                        BubbleEvent = false;
                        return;
                    }

                    #region beforeaction
                    if (pVal.BeforeAction)
                    {

                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            if (pVal.Row == 1)
                            {
                            }
                        }
                        if (pVal.EventType == BoEventTypes.et_MATRIX_LINK_PRESSED)
                        {
                            this.Modal = true;
                            BubbleEvent = false;
                        }

                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnFre")
                        {
                            BubbleEvent = false;

                        }
                    }
                    #endregion
                    #region After Action
                    else
                    {

                    }
                    #endregion
                }

                if (pVal.FormTypeEx == "ImgVh")
                {
                    #region Modales

                    #endregion

                    #region beforeaction
                    //  If the modal form is closed...
                    //if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_CLOSE & Modal)
                    //{
                    //    Modal = false;
                    //}
                    if (pVal.BeforeAction)
                    {
                            #region

                            if (pVal.EventType == BoEventTypes.et_FORM_UNLOAD)
                            {
                                this.Modal = false;
                                BubbleEvent = false;
                            }
                            #endregion
                    }
                    #endregion
                    #region After Action
                    else
                    {
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Modal = false;
                BubbleEvent = false;
                SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                KananSAPHerramientas.LogError("GestionCargasPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
            }
        }
        #endregion

        #region Métodos

        #region Acciones

        #endregion


        #endregion
    }
}
