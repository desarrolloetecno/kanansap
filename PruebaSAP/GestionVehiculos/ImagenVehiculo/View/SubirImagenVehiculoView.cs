﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Etecno.Security2.BO;
using SAPbouiCOM;
using KananWS.Interface;
using System.Windows.Forms;
using Application = System.Windows.Forms.Application;

namespace KananSAP.GestionVehiculos.ImagenVehiculo.View
{
    public class SubirImagenVehiculoView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.Button oButton;
        private SAPbouiCOM.StaticText oStaticText;
        private XMLPerformance XmlApplication;
        public Configurations configs;
        public int? VehiculoID;
        #endregion Atributos

        #region Constructor

        public SubirImagenVehiculoView(SAPbouiCOM.Application Application, SAPbobsCOM.Company company,
            Configurations c)
        {
            this.SBO_Application = Application;
            this.XmlApplication = new XMLPerformance(SBO_Application);
        }

        #endregion Constructor

        #region Metodos

        public void LoadForm()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("ImgVh");
            }
            catch
            {
                string sPath = Application.StartupPath;
                this.XmlApplication.LoadFromXML(sPath + "\\XML",  "SubirImagenVhiculo.xml");
                
                //this.AddDataSources();
                this.oForm = SBO_Application.Forms.Item("ImgVh");
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
            }
        }

        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
            }
            catch (Exception)
            {
                this.LoadForm();
                this.oForm.Visible = true;
                this.oForm.Select();
            }
        }

        public void CloseForm()
        {
            if (this.oForm.Visible & this.oForm.Selected)
                this.oForm.Close();
        }

        #endregion Metodos
    }
}
