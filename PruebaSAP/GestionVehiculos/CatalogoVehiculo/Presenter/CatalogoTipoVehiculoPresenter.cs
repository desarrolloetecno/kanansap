﻿using Kanan.Vehiculos.BO2;
using KananSAP.GestionVehiculos.CatalogoVehiculo.Views;
using KananSAP.Vehiculos.Data;
using KananWS.Interface;
using SAPbouiCOM;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
namespace KananSAP.GestionVehiculos.CatalogoVehiculo.Presenter
{
    public class CatalogoTipoVehiculoPresenter
    {
        #region Variables
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        public CatalogoTipoVehiculoView view;
        private Configurations Configs;
        //private TipoVehiculoPresenter tVehiculoPresenter;
        private TipoVehiculoView tVehiculoView;
        private TipoVehiculosWS tVehiculoWS;
        private TipoVehiculoSAP tVehiculoData;
        private bool Modal { get; set; }
        #endregion

        #region Constructor
        public CatalogoTipoVehiculoPresenter(SAPbouiCOM.Application Application, SAPbobsCOM.Company company, Configurations config)
        {
            this.Configs = config;
            this.SBO_Application = Application;
            this.Company = company;
            this.view = new CatalogoTipoVehiculoView(this.SBO_Application, this.Company);
            this.view.Configs = config;
            this.tVehiculoWS = new TipoVehiculosWS(Guid.Parse(config.UserFull.Usuario.PublicKey), config.UserFull.Usuario.PrivateKey);
            this.tVehiculoData = new TipoVehiculoSAP(company);
            this.tVehiculoView = new TipoVehiculoView(this.SBO_Application, this.Company, this.Configs);
            this.Modal = false;
            this.SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
        }
        #endregion Constructor

        private void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (pVal.FormTypeEx == "FormLstTVh")
                {
                    if(this.Modal)
                    {
                        this.tVehiculoView.ShowForm();
                        BubbleEvent = false;
                        return;
                    }
                    #region beforeaction

                    if (pVal.BeforeAction)
                    {
                        if (pVal.ColUID == "#" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.view.HasRowValue(pVal.Row);
                        }
                        if (pVal.EventType == BoEventTypes.et_MATRIX_LINK_PRESSED)
                        {
                            try
                            {
                                int id = Convert.ToInt32(this.view.GetIDDataRow(pVal.Row));
                                if(id != 0)
                                {
                                    //this.tVehiculoPresenter = null;
                                    //this.tVehiculoPresenter = new TipoVehiculoPresenter(this.SBO_Application, this.Company, this.Configs, this.view);
                                    this.tVehiculoView.LoadForm();
                                    this.tVehiculoView.ShowForm();
                                    this.tVehiculoView.PrepareNewEntity();
                                    this.tVehiculoView.oTipoVehiculo.TipoVehiculoID = id;
                                    this.Consultar();
                                    this.Modal = true;
                                }
                                
                                //this.tVehiculoPresenter.view.EntityToForm();
                            }
                            catch (Exception ex)
                            {
                                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoTipoVehiculoPresenter.cs->SBO_Application_ItemEvent()->et_Matrix_Link_Pressed", ex.Message);
                                SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                                BubbleEvent = false;
                            }
                        }
                        if(pVal.ItemUID == "btnNewTVh" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            //this.tVehiculoPresenter = null;
                            //this.tVehiculoPresenter = new TipoVehiculoPresenter(this.SBO_Application, this.Company, this.Configs, this.view);
                            this.tVehiculoView.LoadForm();
                            this.tVehiculoView.ShowForm();
                            this.tVehiculoView.PrepareNewEntity();
                            this.tVehiculoView.HiddeDeleteButton();
                            
                            this.Modal = true;
                        }
                        if (pVal.ItemUID == "btnBsqTVh" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.view.SetConditions();
                            this.view.BindDataToForm();
                        }
                        if (pVal.ItemUID == "btnCanLTVh" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.SBO_Application.Forms.ActiveForm.Close();
                        }
                        if (pVal.ItemUID == "btnDelTV" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            //Se valida antes de eliminar un ID con un "HashValue"
                            if (this.view.TVehiculoID.HasValue)
                            {
                                this.Eliminar((int)this.view.TVehiculoID);
                            }
                            else
                            {
                                SBO_Application.StatusBar.SetText("¡Seleccione un registro!");
                            }
                        }
                    }

                    #endregion
                }

                if (pVal.FormTypeEx == "FormTVh")
                {
                    if (pVal.BeforeAction)
                    {
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnAddTVh")
                        {
                            try
                            {
                                this.InsertarOActualizar();
                                this.view.BindDataToForm();
                                SBO_Application.StatusBar.SetText("¡Registro guardado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                BubbleEvent = false;
                            }
                            catch (Exception ex)
                            {
                                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoTipoVehiculoPresenter.cs->btnAddTVh()", ex.Message);
                                SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                                BubbleEvent = false;
                            }
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnDelTvh")
                        {
                            this.Eliminar();
                            BubbleEvent = false;
                            this.SBO_Application.Forms.ActiveForm.Close();
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnCanTVh")
                        {
                            this.Modal = false;
                            this.SBO_Application.Forms.ActiveForm.Close();
                        }
                        if (pVal.EventType == BoEventTypes.et_FORM_CLOSE && this.Modal)
                            this.Modal = false;
                        if (pVal.EventType == BoEventTypes.et_FORM_UNLOAD)
                            this.Modal = false;
                    }
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoTipoVehiculoPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
                SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                BubbleEvent = false;
            }
        }

        private void Eliminar(int id)
        {
            try
            {
                this.tVehiculoData.ItemCode = id.ToString();
                if (this.tVehiculoData.ExistUDT())
                {
                    this.tVehiculoData.Sincronizado = 3;
                    this.tVehiculoData.Deactivate();
                    if(this.tVehiculoWS.Eliminar(id))
                    {
                        this.tVehiculoData.ExistUDT();
                        this.tVehiculoData.Sincronizado = 0;
                        this.tVehiculoData.Sincronizar();
                        this.tVehiculoData.Eliminar();
                    }
                    SBO_Application.StatusBar.SetText("¡Registro eliminado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    this.view.SetConditions();
                    this.view.BindDataToForm();
                }
            }
            catch(Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoTipoVehiculoPresenter.cs->Eliminar()", ex.Message);
                SBO_Application.StatusBar.SetText("Error: " + ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            }
        }

        #region Metodos Tipo de Vehiculos
        #region ABC WEB SERVICE
        private bool InsertarWS()
        {
            try
            {

                TipoVehiculo tipoVehiculo = this.tVehiculoWS.Insertar(this.tVehiculoView.oTipoVehiculo);
                if (tipoVehiculo == null)
                    return false;
                if (tipoVehiculo.TipoVehiculoID == null)
                    return false;
                this.tVehiculoView.FormToEntity();
                this.tVehiculoView.oTipoVehiculo.TipoVehiculoID = tipoVehiculo.TipoVehiculoID;
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private bool ActualizarWS()
        {
            try
            {
                //this.tVehiculoView.FormToEntity();
                return this.tVehiculoWS.Actualizar(this.tVehiculoView.oTipoVehiculo);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoTipoVehiculoPresenter.cs->ActualizarWS()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private bool EliminarWS()
        {
            try
            {
                return this.tVehiculoWS.Eliminar((int)this.tVehiculoView.oTipoVehiculo.TipoVehiculoID);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoTipoVehiculoPresenter.cs->EliminarWS()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region ABC SAP
        /// <summary>
        /// Realiza una consulta con los datos del item seleccionado y muestra los datos en caso de encontrarlos
        /// </summary>
        public void Consultar()
        {
            try
            {
                SAPbobsCOM.Recordset rs = this.tVehiculoData.Consultar(this.tVehiculoView.oTipoVehiculo);
                if (this.tVehiculoData.HashTipoVehiculo(rs))
                {
                    this.tVehiculoView.oTipoVehiculo = tVehiculoData.LastRecordSetToTipoVehiculo(rs);
                    this.tVehiculoView.EntityToForm();
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoTipoVehiculoPresenter.cs->Consultar()", ex.Message);
                throw new Exception("Consultar: " + ex.Message);
            }
        }

        private void Insertar()
        {
            try
            {
                //this.tVehiculoView.FormToEntity();
                this.tVehiculoData.TipoVehiculo = this.tVehiculoView.oTipoVehiculo;
                this.tVehiculoData.Sincronizado = 1;
                this.tVehiculoData.UUID = Guid.NewGuid();
                this.tVehiculoData.ItemCode = DateTime.Now.Ticks.ToString();
                this.tVehiculoData.TipoVehiculoToUDT();
                this.tVehiculoData.Insertar();

                if (this.InsertarWS())
                {
                    this.tVehiculoData.Sincronizado = 0;
                    this.tVehiculoData.ActualizarCode(this.tVehiculoView.oTipoVehiculo);
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoTipoVehiculoPresenter.cs->Insertar()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void Actualizar()
        {
            try
            {
                //this.tVehiculoView.FormToEntity();
                this.tVehiculoData.ItemCode = this.tVehiculoView.oTipoVehiculo.TipoVehiculoID.ToString();
                if (this.tVehiculoData.ExistUDT())
                {
                    this.tVehiculoData.GetSyncUUID();
                    this.tVehiculoData.TipoVehiculo = this.tVehiculoView.oTipoVehiculo;
                    this.tVehiculoData.Sincronizado = 2;
                    this.tVehiculoData.TipoVehiculoToUDT();
                    this.tVehiculoData.Actualizar();
                    if (this.ActualizarWS())
                    {
                        this.tVehiculoData.ExistUDT();
                        this.tVehiculoData.Sincronizado = 0;
                        this.tVehiculoData.Sincronizar();
                    }
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoTipoVehiculoPresenter.cs->Actualizar()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void EliminarSAP()
        {
            try
            {
                this.tVehiculoData.ItemCode = this.tVehiculoView.oTipoVehiculo.TipoVehiculoID.ToString();
                if (this.tVehiculoData.ExistUDT())
                {
                    this.tVehiculoData.Sincronizado = 3;
                    this.tVehiculoData.Deactivate();
                    if (this.EliminarWS())
                    {
                        this.tVehiculoData.ExistUDT();
                        this.tVehiculoData.Sincronizado = 0;
                        this.tVehiculoData.Sincronizar();
                    }
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoTipoVehiculoPresenter.cs->EliminarSAP()", ex.Message);
                throw new Exception(ex.Message);
            }
        }
        #endregion

        private void InsertarOActualizar()
        {
            try
            {
                string err = this.tVehiculoView.ValidateFields();
                if (!string.IsNullOrEmpty(err))
                    throw new Exception(err);
                this.tVehiculoView.FormToEntity();
                if (this.tVehiculoView.oTipoVehiculo.TipoVehiculoID != null)
                {
                    //this.ActualizarWS();
                    this.Actualizar();
                    this.tVehiculoView.ClearFields();
                    this.SBO_Application.Forms.ActiveForm.Close();
                }
                else
                {
                    //this.InsertarWS();
                    this.Insertar();
                    this.tVehiculoView.ClearFields();
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoTipoVehiculoPresenter.cs->InsertarOActualizar()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void Eliminar()
        {
            try
            {
                //this.EliminarWS();
                this.EliminarSAP();
                SBO_Application.StatusBar.SetText("¡Registro eliminado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                this.view.BindDataToForm();
                this.tVehiculoView.ClearFields();
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoTipoVehiculoPresenter.cs->Eliminar()", ex.Message);
                throw new Exception(ex.Message);
            }
        }
        #endregion

    }
}
