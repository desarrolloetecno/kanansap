﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using KananSAP.GestionVehiculos.RecargaCombustible;
using KananSAP.GestionVehiculos.RecargaCombustible.Presenter;
using KananSAP.Reportes.Presenter;
using KananWS.Interface;
using SAPbouiCOM;
using KananSAP.GestionServicios.CatalogoServicios.Presenter;
using KananSAP.GestionVehiculos.CatalogoVehiculo.Views;
using KananSAP.GestionVehiculos.IncidenciaVehiculo.Presenter;
using KananSAP.GestionVehiculos.GestoriaVehiculo.Presenter;
using KananSAP.GestionIncidencias.IncidenciasVehiculo.Presenter;
using KananSAP.GestionDatos.MigradorDatos.Presenter;
using KananSAP.GestionDatos.MigradorDatos.View;
using System.Diagnostics;
using SAPinterface.Data.AD;
using SAPinterface.Data.INFO.Tablas;
using SAPinterface.Data.WS;

namespace KananSAP.GestionVehiculos.CatalogoVehiculo.Presenter
{    
    public class CatalogoVehiculosPresenter
    {
        #region Atributos
        public static bool ventanaAbierta = false;
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private SAPbouiCOM.Item oItem;
        private bool IsMessageBoxElimina = false;
        private GestionGlobales oGlobales = null;
        private bool Modal { get; set; }
        private bool ModalGestoria { get; set; }
        private bool ModalCarga;
        private bool ModalIncidencia;
        private bool ModalEditarIncidencia;
        private bool ModalConfigServicio { get; set; }
        private bool ModalRegInd;
        private bool ModalRep;
        private bool ModalDisp;
        private bool ModalPictureIncidencia;
        private bool ModalGeolocalizacionIncidencia;
        private bool ModalPictureCarga;
        private bool ModalGeolocalizacionCarga;
        private bool ModalServicio;
        private bool bModuloDatosMaestroArticulo = false; //Bandera para indicar si el formulario es Datos maestros de artículo
        private bool bAgregaVehiculo = false; //Bandera para indicar si el formulario es Datos maestros de artículo y se está agregando un nuevo registro
        private bool ShowConfigServVehi { get; set; }
        private bool GuardConfigServVehi { get; set; }
        //private bool ShowConfigServVehi { get; set; }
        //private bool ShowConfigServVehi { get; set; }
        private List<CatalogoVahiculosView> Views;
        public CatalogoVahiculosView view;
        private VehiculoWS VehiculoService;
        public GestoriaVehiculoPresenter gestoria;
        private GestionCargasPresenter recarga;
        private InciVehiculoPresenter incidencia;
        private ListaServicioConfigPresenter lstServiciosConfigPresenter;
        private CatalogoServiciosPresenter lstServiciosPresenter;
        private Configurations configs;
        public Reportes.Presenter.ReportePresenter ReportesPresenter { get; set; }
        private string EndPoint;

        //Se agregó una clase estática para manejar el evento "Guardar" de ConfigurarServicioPresenter
        private ConfigurarServicioPresenter confServiciosPresenter;

        private bool ejecuta;
        private BoFormMode mode;

        public bool IsLogged { get; set; }
        public bool Foco;

        //private bool ModalTServicio;
        //private ServicioPresenter servicioPresenter;
        private ListaTServiciosPresenter lstTServicioPresenter;
        private bool ModalSucursal { get; set; }
        private GestionOperaciones.AdminSucursal.Presenter.SucursalAdmonPresenter admonSucursalPresenter;
        private KananSAP.Helper.KananSAPHerramientas oHerramientas;
        #endregion

        public CatalogoVehiculosPresenter(GestionGlobales oGlobales, SAPbouiCOM.Application sApplication, SAPbobsCOM.Company Cmpny, Configurations c)
        {
            this.oGlobales = oGlobales;
            this.SBO_Application = sApplication;
            this.Company = Cmpny;
            this.configs = c;
            this.ejecuta = false;
            this.Views = new List<CatalogoVahiculosView>();
            this.EndPoint = System.Configuration.ConfigurationManager.AppSettings.Get("EndPoint");
            //this.recarga = new RecargaPresenter(this.SBO_Application, this.Company, this.view.Entity.vehiculo.VehiculoID, this.configs);
            try
            {
                this.VehiculoService = new VehiculoWS(Guid.Parse(this.configs.UserFull.Usuario.PublicKey), this.configs.UserFull.Usuario.PrivateKey);
            }
            catch
            {
                this.VehiculoService = new VehiculoWS(new Guid(), string.Empty);
            }
            this.Modal = false;
            this.ModalRep = false;
            this.ModalDisp = false;
            this.ModalPictureIncidencia = false;
            this.ModalGeolocalizacionIncidencia = false;
            this.ModalGeolocalizacionCarga = false;
            this.ModalPictureCarga = false;
            this.ModalGestoria = false;
            this.ModalCarga = false;
            this.ModalIncidencia = false;
            this.ModalEditarIncidencia = false;
            this.ModalConfigServicio = false;
            this.ModalServicio = false;
            this.ModalSucursal = false;
            this.ShowConfigServVehi = false;
            this.GuardConfigServVehi = false;

            this.Foco = false;

            #region Comentado. La aplicación principal maneja los eventos
            //SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
            //SBO_Application.MenuEvent += new SAPbouiCOM._IApplicationEvents_MenuEventEventHandler(SBO_Application_MenuEvent);
            #endregion Comentado. La aplicación principal maneja los eventos

            // SBO_Application.FormDataEvent += new SAPbouiCOM._IApplicationEvents_FormDataEventEventHandler(SBO_Application_FormDataEvent);
            this.oHerramientas = new Helper.KananSAPHerramientas(this.SBO_Application);

        }

        public string GetFormUniqueID()
        {
            return this.view.FormUniqueID;
        }

        private void SetActiveView(string FormUID)
        {
            this.view = Views.First(x => x.FormUniqueID == FormUID);
        }

        public void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            if (!IsLogged) return;
            string group = string.Empty;
            try
            {
                #region FotoGeo EditarIncidencia en GestionIncidencia
                if(ModalEditarIncidencia)
                {
                    BubbleEvent = false;
                    ModalEditarIncidencia = false;
                    return;
                }

                if(ModalPictureIncidencia)
                {
                    BubbleEvent = false;
                    ModalPictureIncidencia = false;
                    this.incidencia.ReportesPresenter.view.ShowForm();
                    return;
                }

                if (ModalGeolocalizacionIncidencia)
                {
                    BubbleEvent = false;
                    ModalGeolocalizacionIncidencia = false;
                    this.incidencia.ReportesPresenter.view.ShowForm();
                    return;
                }

                if (pVal.FormTypeEx == "EInci")
                {
                    #region BeforeAction
                    if (pVal.BeforeAction)
                    {
                        if (pVal.ItemUID == "btnphoto" && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.incidencia.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepFotos", @EndPoint + "Vehiculos/ImagenesIncidenciaVehiculo.aspx?_modal=true&sap=true&IncidenciaId=" + this.incidencia.view.id);
                            BubbleEvent = false;
                            this.ModalPictureIncidencia = true;
                        }

                        if (pVal.ItemUID == "btngeo" && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.incidencia.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepGeopos", @EndPoint + "Vehiculos/GeoposicionRecarga.aspx?_modal=true&sap=true&incive=1&IncidenciaVehiculoID=" + this.incidencia.view.id);
                            BubbleEvent = false;
                            this.ModalGeolocalizacionIncidencia = true;
                        }
                    }
                    #endregion

                    #region AfterAction
                    else
                    {

                    }
                    #endregion
                }
                #endregion FotoGeo EditarIncidencia en GestionIncidencia

                #region GestionCargas
                if (ModalPictureCarga)
                {
                    BubbleEvent = false;
                    ModalPictureCarga = false;
                    this.recarga.ReportesPresenter.view.ShowForm();
                    return;
                }
                if (ModalGeolocalizacionCarga)
                {
                    BubbleEvent = false;
                    ModalGeolocalizacionCarga = false;
                    this.recarga.ReportesPresenter.view.ShowForm();
                    return;
                }
                if (pVal.FormTypeEx == "CargaCom")
                {
                    #region BeforeAction
                    if (pVal.BeforeAction)
                    {
                        if (pVal.ItemUID == "btnphoto" && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.recarga.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepFotos", @EndPoint + "Vehiculos/ImagenesRecarga.aspx?_modal=true&sap=true&CargaCombustibleID=" + this.recarga.cargaview.Entity.carga.CargaCombustibleID);
                            BubbleEvent = false;
                            this.ModalPictureCarga = true;
                        }
                        if (pVal.ItemUID == "btngeo" && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.recarga.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepGeopos", @EndPoint + "Vehiculos/GeoposicionRecarga.aspx?_modal=true&sap=true&CargaCombustibleID=" + this.recarga.cargaview.Entity.carga.CargaCombustibleID);
                            //this.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepGeopos", @"http://w.administraflotilla.com/Vehiculos/GeoposicionRecarga.aspx?_modal=true&CargaCombustibleID=6757");
                            //Modal = false;
                            BubbleEvent = false;
                            this.ModalGeolocalizacionCarga = true;
                        }
                    }
                    #endregion BeforeAction

                    #region AfterAction
                    else
                    {

                    }
                    #endregion AfterAction
                }
                #endregion GestionCargas

                #region Servicios

                #region ListaServicioConfig

                #region LstServ
                if (pVal.FormTypeEx == "LstServ")
                {
                    if (Modal)
                    {
                        lstServiciosConfigPresenter.listaServicioView.ShowForm();
                        BubbleEvent = false;
                        return;
                    }

                    if (ModalConfigServicio)
                    {
                        lstServiciosConfigPresenter.configServicioView.ShowForm();
                        ModalConfigServicio = false;
                        BubbleEvent = false;
                        return;
                    }

                    if (ShowConfigServVehi)
                    {
                        //BubbleEvent = false;
                        ShowConfigServVehi = false;
                        //Modal encargado de mostrar el formulario.
                        lstServiciosConfigPresenter.listaServicioView.InitializeIDS();
                        lstServiciosConfigPresenter.configServicioView.ShowForm();
                        lstServiciosConfigPresenter.configServicioView.oServicio = null;
                        lstServiciosConfigPresenter.configServicioView.EsActivo = false;
                        lstServiciosConfigPresenter.configServicioView.oVehiculo = lstServiciosConfigPresenter.listaServicioView.oVehiculo;
                        lstServiciosConfigPresenter.configServicioView.ClearAllFields();
                        lstServiciosConfigPresenter.configServicioView.PrepareNewEntity();
                        lstServiciosConfigPresenter.configServicioView.SetServiceVehicleData();
                        lstServiciosConfigPresenter.configServicioView.HideCheckBox();
                        lstServiciosConfigPresenter.configServicioView.HiddeDeleteButton();
                        return;
                    }

                    if (pVal.BeforeAction)
                    {
                        #region EditarParametroServicio
                        if (pVal.EventType == BoEventTypes.et_MATRIX_LINK_PRESSED)
                        {
                            lstServiciosConfigPresenter.listaServicioView.SetID(pVal.Row);
                            if (lstServiciosConfigPresenter.listaServicioView.oServicio != null && lstServiciosConfigPresenter.listaServicioView.oVehiculo != null)
                            {
                                if (lstServiciosConfigPresenter.listaServicioView.oServicio.ServicioID != null && lstServiciosConfigPresenter.listaServicioView.oVehiculo.VehiculoID != null)
                                {
                                    ModalConfigServicio = true;
                                    lstServiciosConfigPresenter.configServicioView.ShowForm();
                                    lstServiciosConfigPresenter.configServicioView.oServicio = lstServiciosConfigPresenter.listaServicioView.oServicio;
                                    lstServiciosConfigPresenter.configServicioView.EsActivo = false;
                                    lstServiciosConfigPresenter.configServicioView.oVehiculo = lstServiciosConfigPresenter.listaServicioView.oVehiculo;
                                    lstServiciosConfigPresenter.configServicioView.ClearAllFields();
                                    lstServiciosConfigPresenter.configServicioView.PrepareNewEntity();
                                    lstServiciosConfigPresenter.configServicioView.SetServiceVehicleData();
                                    if (lstServiciosConfigPresenter.configServicioView.VerificaExistenciaParametro())
                                        lstServiciosConfigPresenter.configServicioView.ShowCheckBox();
                                    else
                                        lstServiciosConfigPresenter.configServicioView.HideCheckBox();
                                }
                            }
                            BubbleEvent = false;
                        }
                        #endregion EditarParametroServicio

                        #region EliminarParametroServicio
                        if (pVal.ItemUID == "btnDelLst" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            if (lstServiciosConfigPresenter.listaServicioView.PMantenimientoID.HasValue && lstServiciosConfigPresenter.listaServicioView.PServicioID.HasValue)
                            {
                                if (lstServiciosConfigPresenter.listaServicioView.EliminarParametros())
                                {
                                    this.SBO_Application.StatusBar.SetText("¡Parámetro de Mantenimiento Eliminado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                    lstServiciosConfigPresenter.listaServicioView.ShowParametrosMantenimiento();
                                }
                                else
                                    lstServiciosConfigPresenter.listaServicioView.InitializeIDS();
                            }
                        }
                        #endregion EliminarParametroServicio

                        #region AgregarParametroServicio
                        if (pVal.ItemUID == "btnAddPMto" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            ShowConfigServVehi = true;
                            return;
                            /*lstServiciosConfigPresenter.configServicioView.ShowForm();
                            lstServiciosConfigPresenter.configServicioView.oServicio = null;
                            lstServiciosConfigPresenter.configServicioView.oVehiculo = lstServiciosConfigPresenter.listaServicioView.oVehiculo;
                            lstServiciosConfigPresenter.configServicioView.ClearAllFields();
                            lstServiciosConfigPresenter.configServicioView.PrepareNewEntity();
                            lstServiciosConfigPresenter.configServicioView.SetServiceVehicleData();
                            lstServiciosConfigPresenter.configServicioView.HideCheckBox();
                            lstServiciosConfigPresenter.configServicioView.HiddeDeleteButton();*/
                        }
                        #endregion AgregarParametroServicio
                    }
                    if (pVal.EventType == BoEventTypes.et_FORM_CLOSE && this.Modal)
                    {
                        this.Modal = false;
                        lstServiciosConfigPresenter.configServicioView.TipoParametro = 0;
                    }
                    if (pVal.EventType == BoEventTypes.et_FORM_UNLOAD)
                    {
                        this.Modal = false;
                        lstServiciosConfigPresenter.configServicioView.TipoParametro = 0;
                    }
                }
                #endregion LstServ

                #region ConfSvForm
                if (pVal.FormTypeEx == "ConfSvForm")
                {
                    if (pVal.BeforeAction)
                    {
                        #region GuardarParametroServicio
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnAConfSv")
                        {
                            try
                            {
                                lstServiciosConfigPresenter.configServicioView.InsertOrUpdateParameters();
                                //Se utiliza el SBO_Application para generar los mensajes.
                                this.SBO_Application.StatusBar.SetText("Parámetros de mantenimiento guardados", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                lstServiciosConfigPresenter.listaServicioView.InitializeIDS();
                                lstServiciosConfigPresenter.listaServicioView.ShowParametrosMantenimiento();
                                this.SBO_Application.Forms.ActiveForm.Close();
                                BubbleEvent = false;
                                return;
                            }
                            catch (Exception ex)
                            {
                                lstServiciosConfigPresenter.listaServicioView.InitializeIDS();
                                lstServiciosConfigPresenter.listaServicioView.ShowParametrosMantenimiento();
                                //this.SBO_Application.Forms.ActiveForm.Close();
                                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoVehiculoPresenter.cs->btnAConfSv()", ex.Message);
                                throw new Exception(ex.Message);
                            }

                        }
                        #endregion GuardarParametroServicio

                        #region ConfigurarParametroServicio
                        if (pVal.ItemUID == "btnCConfSv" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            ModalConfigServicio = false;
                            lstServiciosConfigPresenter.listaServicioView.InitializeIDS();
                            lstServiciosConfigPresenter.configServicioView.ClearAllFields();
                            this.SBO_Application.Forms.ActiveForm.Close();
                        }
                        #endregion ConfigurarParametroServicio

                        #region EliminarParametroServicio
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnDelPMS")
                        {
                            if (lstServiciosConfigPresenter.listaServicioView.PMantenimientoID.HasValue && lstServiciosConfigPresenter.listaServicioView.PServicioID.HasValue)
                            {
                                if (lstServiciosConfigPresenter.listaServicioView.EliminarParametros())
                                {
                                    this.SBO_Application.StatusBar.SetText("¡Parámetro de Mantenimiento Eliminado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                    lstServiciosConfigPresenter.listaServicioView.ShowParametrosMantenimiento();
                                    ModalConfigServicio = false;
                                    this.SBO_Application.Forms.ActiveForm.Close();
                                }
                                else
                                    lstServiciosConfigPresenter.listaServicioView.InitializeIDS();
                            }
                        }
                        #endregion EliminarParametroServicio

                        if (pVal.EventType == BoEventTypes.et_FORM_CLOSE && this.ModalConfigServicio)
                        {
                            this.ModalConfigServicio = false;
                            lstServiciosConfigPresenter.configServicioView.oServicio = null;
                            lstServiciosConfigPresenter.configServicioView.TipoParametro = 0;
                        }
                        if (pVal.EventType == BoEventTypes.et_FORM_UNLOAD)
                        {
                            lstServiciosConfigPresenter.configServicioView.oServicio = null;
                            this.ModalConfigServicio = false;
                            lstServiciosConfigPresenter.configServicioView.TipoParametro = 0;
                        }

                    }
                    else
                    {
                       // if (pVal.ItemUID == "cmbParam")
                       //     lstServiciosConfigPresenter.configServicioView.DeterminaParametrizacion();
                    }
                }
                #endregion

                #endregion

                #region ConfigurarServicio
                //if (pVal.FormTypeEx == "ConfSvForm")
                //{
                //    if (pVal.BeforeAction)
                //    {
                //        #region Guardar
                //        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnAConfSv")
                //        {
                //            confServiciosPresenter = new ConfigurarServicioPresenter(SBO_Application, Company, configs);
                //            confServiciosPresenter.view.InsertOrUpdateParameters();
                //            this.SBO_Application.StatusBar.SetText("Parámetros de mantenimiento guardados", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                //            //Probar destruir instancia del presentador para evitar dos instancias de la misma.
                //            //Revisar variables dependientes de la isntancia parcial.
                //            //confServiciosPresenter = null;
                //            BubbleEvent = false;
                //            return;
                //        }
                //        #endregion Guardar
                //    }
                //}
                #endregion ConfigurarServicio

                #endregion Servicios

                #region Form Datos Maestros de Articulos
                if (pVal.FormType == 150)
                {
                    this.bModuloDatosMaestroArticulo = true;
                    if (this.view != null)
                    {
                        SAPbouiCOM.ComboBox oCombo;
                        var item = this.view.GetItemFromForm("39");
                        if (item != null)
                        {
                            oCombo = (SAPbouiCOM.ComboBox)(item.Specific);
                            if (oCombo.Selected != null)
                            {
                                @group = oCombo.Selected != null ? oCombo.Selected.Description : string.Empty;
                            }
                        }
                    }

                    if (ModalGestoria)
                    {
                        BubbleEvent = false;
                        ModalGestoria = false;
                        this.gestoria.view.LoadForm();
                        this.gestoria.view.ShowForm();
                        this.gestoria.view.SetFocus();
                    }

                    if (ModalIncidencia)
                    {
                        BubbleEvent = false;
                        ModalIncidencia = false;
                        this.incidencia.view.LoadForm();
                        this.incidencia.view.ShowForm();
                        return;
                    }

                    if (ModalCarga)
                    {
                        BubbleEvent = false;
                        ModalCarga = false;
                        this.recarga.view.LoadForm();
                        this.recarga.view.ShowForm();
                        return;
                    }

                    if (ModalRep)
                    {
                        BubbleEvent = false;
                        ModalRep = false;
                        ReportesPresenter.view.ShowForm();
                        return;
                    }

                    if (ModalDisp)
                    {
                        BubbleEvent = false;
                        ModalDisp = false;
                        ReportesPresenter.view.ShowForm();
                        return;
                    }

                    if (ModalServicio)
                    {
                        ModalServicio = false;
                        BubbleEvent = false;
                        lstServiciosConfigPresenter.listaServicioView.ShowForm();
                        return;
                    }

                    if (ModalSucursal)
                    {
                        ModalSucursal = false;
                        BubbleEvent = false;
                        this.admonSucursalPresenter.view.ChangeForm();
                        this.admonSucursalPresenter.view.ShowForm();
                        return;
                    }

                    #region Before Action

                    if (pVal.BeforeAction)
                    {
                        try
                        {
                            #region LoadForm
                            if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_LOAD)
                            {
                                if (this.configs.UserFull == null)
                                {
                                    KananSAP.Helper.KananSAPHerramientas.LogError("MenuPresenter.cs->SBO_Application_MenuEvent()", "No se obtuvieron valores de inicio de sesión. Es posible que la conexión a Internet sea mala o que no sea una instalación oficial del addonkananfleet.");
                                    oHerramientas.ColocarMensaje("No se obtuvieron valores de inicio de sesión, valide su conexión de Internet. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                                    return;
                                }

                                this.Views.Add(new CatalogoVahiculosView(this.SBO_Application, this.Company, pVal.FormTypeEx, pVal.FormTypeCount));
                                this.SetActiveView(FormUID);
                                this.view.InsertFolder();
                                this.Foco = true;
                                var empresaId = this.configs.UserFull.Dependencia.EmpresaID;
                                if (empresaId != null)
                                {
                                    this.view.AddItemsToForm();
                                }
                                else
                                {
                                    throw new Exception("KananFleet Configurations not found");
                                }
                                this.view.ChangeFolder(6);
                            }
                            #endregion LoadForm

                            #region InformacionVehiculo
                            if (pVal.ItemUID == "vehinfo" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                            {
                                this.view.ChangeFolder(14);
                            }
                            #endregion InformacionVehiculo

                            #region GestoríaVehículo
                            if (pVal.ItemUID == "btngest" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                            {
                                if (view.IsVehicleKf())
                                {
                                    if (this.view.Entity.vehiculo.VehiculoID != null && this.view.Entity.vehiculo.VehiculoID > 0)
                                    {
                                        BubbleEvent = false;
                                        this.gestoria = null;
                                        this.gestoria = new GestoriaVehiculoPresenter(this.SBO_Application, this.Company, this.configs, this.view.Entity.vehiculo);
                                        this.ModalGestoria = true;
                                    }
                                    else
                                    {
                                        oHerramientas.ColocarMensaje("Seleccione un Articulo de Tipo Vehiculo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                    }
                                }
                                else
                                {
                                    oHerramientas.ColocarMensaje("Seleccione un Articulo de Tipo Vehiculo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                }
                            }
                            #endregion GestoríaVehículo

                            #region RecargaCombustible
                            if (pVal.ItemUID == "btnrcomb" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                            {
                                //this.view.FreezeForm(true);
                                if (view.IsVehicleKf())
                                {
                                    //this.view.FreezeForm(true);
                                    if (this.view.Entity.vehiculo.VehiculoID != null && this.view.Entity.vehiculo.VehiculoID > 0)
                                    {
                                        this.recarga = null;
                                        GestionGlobales.CurrentVehiculoName = this.view.Entity.vehiculo.Nombre;
                                        GestionGlobales.CurrentVehiculoID = this.view.Entity.vehiculo.VehiculoID;
                                        this.recarga = new GestionCargasPresenter(this.SBO_Application, this.Company, (int)this.view.Entity.vehiculo.VehiculoID, this.view.Entity.vehiculo.Nombre, this.configs, this.view.Entity.vehiculo.SubPropietario.SucursalID);
                                        this.ModalCarga = true;
                                        BubbleEvent = false;
                                    }
                                    else
                                    {
                                        oHerramientas.ColocarMensaje("Seleccione un Articulo de Tipo Vehiculo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                    }
                                }
                                else
                                {
                                    oHerramientas.ColocarMensaje("Seleccione un Articulo de Tipo Vehiculo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                }
                            }
                            #endregion RecargaCombustible

                            #region Importar recargas masivas
                            if (view.IsVehicleKf())
                            {
                                if (pVal.ItemUID == "btnRCcsv" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                                {
                                    MigradorView oRecargas = new MigradorView(this.oGlobales, this.Company, "recargas", "2", this.view.Entity.vehiculo.VehiculoID);
                                }
                            }
                            //else
                            //{
                            //    oHerramientas.ColocarMensaje("Seleccione un Articulo de Tipo Vehiculo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                            //}                            
                            #endregion Importar recargas masivas

                            #region importar recargas de combustible
                            if (pVal.ItemUID == "btnRCcsv" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                            {
                                //if (view.IsVehicleKf())
                                //    oRecargas = new MigradorView(this.oGlobales, this.Company, "recargas", "2");
                            }
                            #endregion importar recargas de combustible

                            #region Servicio
                            //Configurar Servicio a Vehículo
                            if (pVal.ItemUID == "btnaddsvc" && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                            {
                                ejecuta = @group.Trim().ToUpper() == "VEHICULOS KF";
                                if (ejecuta)
                                {
                                    if (this.view.Entity.vehiculo.VehiculoID != null && this.view.Entity.vehiculo.VehiculoID > 0)
                                    {
                                        this.lstServiciosConfigPresenter = null;
                                        this.lstServiciosConfigPresenter = new ListaServicioConfigPresenter(SBO_Application, Company, this.configs);
                                        this.lstServiciosConfigPresenter.listaServicioView.EsActivo = false;
                                        this.lstServiciosConfigPresenter.listaServicioView.LoadForm();
                                        this.lstServiciosConfigPresenter.listaServicioView.oVehiculo = this.view.Entity.vehiculo;
                                        this.lstServiciosConfigPresenter.listaServicioView.ShowForm();
                                        this.lstServiciosConfigPresenter.listaServicioView.ShowParametrosMantenimiento();
                                        //this.lstServiciosConfigPresenter.listaServicioView.oVehiculo = this.view.Entity.vehiculo;
                                        this.lstServiciosConfigPresenter.isConfig = true;
                                        this.ModalServicio = true;
                                        BubbleEvent = false;
                                    }
                                    else
                                    {
                                        oHerramientas.ColocarMensaje("Seleccione un Articulo de Tipo Vehiculo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                    }
                                }
                                else
                                {
                                    oHerramientas.ColocarMensaje("Seleccione un Articulo de Tipo Vehiculo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                }
                            }
                            #endregion Servicio

                            #region AsignarLlantas
                            if (pVal.ItemUID == "btnLlantas" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                            {
                                //this.view.FreezeForm(true);
                                if (view.IsVehicleKf())
                                {
                                    if (this.view.Entity.vehiculo.VehiculoID != null && this.view.Entity.vehiculo.VehiculoID > 0)
                                    {
                                        if (this.view.Entity.vehiculo.SubPropietario != null && this.view.Entity.vehiculo.SubPropietario.SucursalID != null)
                                            this.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepDet", @EndPoint + "PaginasSAP/ConfiguracionLLantas.aspx?id=" + this.view.Entity.vehiculo.VehiculoID + "&tipo=1&sap=true&empid=" + configs.UserFull.Dependencia.EmpresaID + "&sucID=" + this.view.Entity.vehiculo.SubPropietario.SucursalID);
                                        else
                                            this.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepDet", @EndPoint + "PaginasSAP/ConfiguracionLLantas.aspx?id=" + this.view.Entity.vehiculo.VehiculoID + "&tipo=1&sap=true&empid=" + configs.UserFull.Dependencia.EmpresaID + "&sucID=");
                                        //this.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepDet", @"http://test.administraflotilla.com/reportesSAP/DetalleRecargaVehiculos.aspx?lang=" + this.configs.UserFull.Configs["Lang"] + "&Tmz=" + "&long=Kms&vol=Lts&vh=" + this.view.Entity.vehiculo.VehiculoID);
                                        //this.ReportesPresenter.view.LoadForm();
                                        //this.ReportesPresenter.view.ShowForm();
                                        this.ModalRep = true;
                                        BubbleEvent = false;
                                    }
                                    else
                                    {
                                        oHerramientas.ColocarMensaje("Seleccione un Articulo de Tipo Vehiculo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                    }
                                }
                                else
                                {
                                    oHerramientas.ColocarMensaje("Seleccione un Articulo de Tipo Vehiculo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                }
                            }
                            #endregion AsignarLlantas

                            #region Incidencia
                            //Presionar botón "Incidencia en Vehículo"
                            if (pVal.ItemUID == "btnInci" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                            {
                                //this.view.FreezeForm(true);
                                if (view.IsVehicleKf())
                                {
                                    //this.view.FreezeForm(true);
                                    if (this.view.Entity.vehiculo.VehiculoID != null && this.view.Entity.vehiculo.VehiculoID > 0)
                                    {
                                        this.incidencia = null;
                                        this.incidencia = new InciVehiculoPresenter(this.SBO_Application, this.Company, (int)this.view.Entity.vehiculo.VehiculoID, this.view.Entity.vehiculo.Nombre, this.configs, this.oGlobales);
                                        this.ModalIncidencia = true;
                                        BubbleEvent = false;
                                    }
                                    else
                                    {
                                        SBO_Application.StatusBar.SetText("Seleccione un Articulo de Tipo Vehiculo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                    }
                                }
                                else
                                {
                                    SBO_Application.StatusBar.SetText("Seleccione un Articulo de Tipo Vehiculo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                }
                            }
                            #endregion Incidencia

                            #region ReporteDetalleRecargaVehiculo
                            if (pVal.ItemUID == "btnreport" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                            {
                                //this.view.FreezeForm(true);
                                if (view.IsVehicleKf())
                                {
                                    if (this.view.Entity.vehiculo.VehiculoID != null && this.view.Entity.vehiculo.VehiculoID > 0)
                                    {
                                        this.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepDet", @EndPoint + "reportesSAP/DetalleRecargaVehiculos.aspx?lang=es-MX&sap=true&long=Kms&vol=Lts&vh=" + this.view.Entity.vehiculo.VehiculoID);
                                        //this.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepDet", @"http://test.administraflotilla.com/reportesSAP/DetalleRecargaVehiculos.aspx?lang=" + this.configs.UserFull.Configs["Lang"] + "&Tmz=" + "&long=Kms&vol=Lts&vh=" + this.view.Entity.vehiculo.VehiculoID);
                                        //this.ReportesPresenter.view.LoadForm();
                                        //this.ReportesPresenter.view.ShowForm();
                                        this.ModalRep = true;
                                        BubbleEvent = false;
                                    }
                                    else
                                    {
                                        oHerramientas.ColocarMensaje("Seleccione un Articulo de Tipo Vehiculo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                    }
                                }
                                else
                                {
                                    oHerramientas.ColocarMensaje("Seleccione un Articulo de Tipo Vehiculo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                }
                            }
                            #endregion ReporteDetalleRecargaVehiculo


                            #region Lanzar reporte de hoja de inspección
                            if (pVal.ItemUID == "btnhojai" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                            {
                                if (view.IsVehicleKf())
                                {
                                    if (this.view.Entity.vehiculo.VehiculoID != null && this.view.Entity.vehiculo.VehiculoID > 0)
                                    {
                                        Process.Start(string.Format(@"{0}kananfleet/Inspeccion/?id={1}", GestionGlobales.sURLReport, this.configs.UserFull.Dependencia.EmpresaID));
                                        BubbleEvent = false;
                                    }
                                    else oHerramientas.ColocarMensaje("Seleccione un Articulo de Tipo Vehiculo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                }
                                else oHerramientas.ColocarMensaje("Seleccione un Articulo de Tipo Vehiculo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                            }
                            #endregion Lanzar reporte de hoja de inspección

                            #region DisponibilidadVehiculos
                            if (pVal.ItemUID == "btnDisp" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                            {
                                oHerramientas.ColocarMensaje("Sólo informativo. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                            }
                            #endregion DisponibilidadVehiculo

                            #region Sucursal
                            //Elegir Susucursal
                            if (pVal.ItemUID == "btnAddSuc" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                            {
                                ejecuta = @group.Trim().ToUpper() == "VEHICULOS KF";
                                if (ejecuta)
                                {
                                    if (this.view.Entity.vehiculo.VehiculoID != null && this.view.Entity.vehiculo.VehiculoID > 0)
                                    {
                                        //if(!this.view.Entity.vehiculo.SubPropietario.SucursalID.HasValue)
                                        //{
                                        //int result = this.SBO_Application.MessageBox("Este Vehículo esta asignado a una sucursal.\n¿Desea reemplazarlo?", 1, "Si", "No", "");
                                        //if(result == 1)
                                        //{
                                        this.admonSucursalPresenter = null;
                                        this.admonSucursalPresenter = new GestionOperaciones.AdminSucursal.Presenter.SucursalAdmonPresenter(this.SBO_Application, this.Company, this.configs);
                                        this.admonSucursalPresenter.SetSucursalTo = 1;
                                        this.admonSucursalPresenter.VehiculoID = this.view.Entity.vehiculo.VehiculoID;
                                        this.admonSucursalPresenter.Vehiculo = this.view.Entity.vehiculo;
                                        this.admonSucursalPresenter.view.LoadForm();
                                        this.admonSucursalPresenter.view.ChangeForm();
                                        this.admonSucursalPresenter.view.ShowForm();
                                        this.ModalSucursal = true;
                                        BubbleEvent = false;
                                        //}
                                        //}
                                    }
                                    else
                                    {
                                        oHerramientas.ColocarMensaje("Seleccione un Articulo de Tipo Vehiculo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                    }
                                }
                                else
                                {
                                    oHerramientas.ColocarMensaje("Seleccione un Articulo de Tipo Vehiculo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                }
                            }
                            #endregion Sucursal

                            #region GuardarDMA
                            if (pVal.ItemUID == "1" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                            {
                                //oHerramientas.ColocarMensaje("Se guardará un artículo", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                //if (this.view != null)
                                {
                                    SAPbouiCOM.ComboBox oCombo;
                                    var item = this.view.GetItemFromForm("39");
                                    if (item != null)
                                    {
                                        oCombo = (SAPbouiCOM.ComboBox)(item.Specific);
                                        @group = oCombo.Selected != null ? oCombo.Selected.Description : string.Empty;
                                    }
                                    //this.view.ValidarDatos();
                                    ejecuta = this.view.IsVehicleKf();
                                    mode = this.view.GetFormMode(pVal.FormUID);
                                }
                                ejecuta = @group.Trim().ToUpper() == "VEHICULOS KF";

                                if (ejecuta )
                                {
                                    switch (mode)
                                    {
                                        case BoFormMode.fm_ADD_MODE:
                                            this.bModuloDatosMaestroArticulo = true;
                                            Helper.KananSAPHerramientas.LogProceso("SBO_Application_ItemEvent", string.Format("Before Registrados: {0} Límite: {1}",
                                                   this.view.Entity.GetTotaVehiculos(), this.configs.UserFull.Dependencia.EKPermitidos));
                                            if (this.view.Entity.GetTotaVehiculos() > this.configs.UserFull.Dependencia.EKPermitidos)
                                            {
                                                oHerramientas.ColocarMensaje("Ya ha excedido el máximo de vehículos permitidos para su cuenta", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                                BubbleEvent = false;

                                                return;
                                                //throw new Exception("Ya ha excedido el máximo de vehículos permitidos para su cuenta");
                                            }
                                            else
                                            {
                                                try
                                                {
                                                    if (ejecuta)
                                                        this.view.ValidarDatos();
                                                    Insertar();
                                                }
                                                catch
                                                {
                                                    ejecuta = false;
                                                    KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoVehiculoPresenter.cs->ValidarDatos()", "");
                                                    return;
                                                }
                                                this.view.FormToEntity();
                                            }
                                            break;
                                        case BoFormMode.fm_UPDATE_MODE:
                                            try
                                            {
                                                if (ejecuta)
                                                    this.view.ValidarDatos();
                                                //Actualizar();
                                            }
                                            catch (Exception ex)
                                            {
                                                ejecuta = false;
                                                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoVehiculoPresenter.cs->ValidarDatos()", ex.Message);
                                                throw new Exception(ex.Message);
                                                //return;
                                            }
                                            this.view.FormToEntity();
                                            break;
                                    }
                                }
                                else if (this.view.ObjetoKananfleet.Equals("HERRAMIENTAS KF"))   //this.view.IsVehicleKf("HERRAMIENTAS KF")
                                    this.ProcesaArticuloTipoHerramienta("HERRAMIENTAS KF");
                                else if (this.view.ObjetoKananfleet.Equals("REFACCIONES KF")) //this.view.IsVehicleKf("REFACCIONES KF")
                                    this.ProcesaArticuloTipoHerramienta("REFACCIONES KF");
                                        
                            }
                            #endregion GuardarDMA

                            if (pVal.EventType == BoEventTypes.et_FORM_CLOSE)
                            {
                                this.bModuloDatosMaestroArticulo = false;
                                this.bAgregaVehiculo = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            ejecuta = false;
                            KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoVehiculoPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
                            throw new Exception(ex.Message);
                            
                        }
                    }
                    #endregion

                    #region After Action
                    else
                    {
                        if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE)
                        {
                            this.SetActiveView(FormUID);
                        }

                        if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
                        {
                            this.Views.Remove(this.Views.First(x => x.FormUniqueID == FormUID));
                            this.view = null;
                        }

                        #region Form Unload
                        if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
                        {
                            this.Foco = false;
                        }

                        if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_CLOSE)
                        {
                            this.Foco = false;
                        }
                        #endregion Form Unload

                        #region Acciones
                        if (pVal.ItemUID == "1" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                        {

                            switch (mode)
                            {
                                case BoFormMode.fm_ADD_MODE:
                                    if (!this.view.ObjetoKFID.Equals("0") &&  this.view.ObjetoKananfleet.Equals("REFACCIONES KF")) 
                                    {
                                        SBO_KF_OITM_INFO oitmtemp = new SBO_KF_OITM_INFO();
                                        SBO_KF_OITM_AD oArticulo = new SBO_KF_OITM_AD();
                                        oitmtemp.U_SINCRONIZABLE = "Y";
                                        oitmtemp.ItemCode = this.view.CodigoArticuloRefaccion.Trim();
                                        oitmtemp.iIdArticulo = Convert.ToInt32(this.view.ObjetoKFID); // FolioArticuloID;
                                        string response = oArticulo.actualizaHerramienta(ref this.Company, oitmtemp);
                                        this.view.CodigoArticuloRefaccion = string.Empty;
                                        this.view.ObjetoKFID = "0";
                                        this.view.ObjetoSincronizable = "";
                                        this.view.ObjetoKananfleet = "";
                                    }
                                    if (!this.view.ObjetoKFID.Equals("0") && this.view.ObjetoKananfleet.Equals("HERRAMIENTAS KF"))
                                    {
                                        SBO_KF_OITM_INFO oitmtemp = new SBO_KF_OITM_INFO();
                                        SBO_KF_OITM_AD oArticulo = new SBO_KF_OITM_AD();
                                        oitmtemp.U_SINCRONIZABLE = "Y";
                                        oitmtemp.ItemCode = this.view.CodigoArticuloRefaccion.Trim();
                                        oitmtemp.iIdArticulo = Convert.ToInt32(this.view.ObjetoKFID); // FolioArticuloID;
                                        string response = oArticulo.actualizaHerramienta(ref this.Company, oitmtemp);
                                        this.view.CodigoArticuloRefaccion = string.Empty;
                                        this.view.ObjetoKFID = "0";
                                        this.view.ObjetoSincronizable = "";
                                        this.view.ObjetoKananfleet = "";
                                    }
                        
                                    //if (this.view.ObjetoKananfleet.Equals("HERRAMIENTAS KF"))   //this.view.IsVehicleKf("HERRAMIENTAS KF")
                                    //{
                                    //    switch (mode)
                                    //    {
                                    //        case BoFormMode.fm_ADD_MODE:
                                    //            this.ProcesaArticuloTipoHerramienta("HERRAMIENTAS KF");
                                    //            break;
                                    //        case BoFormMode.fm_UPDATE_MODE:
                                    //            //try
                                    //            //{
                                    //            //    if (ejecuta)
                                    //            //        this.view.ValidarDatos();
                                    //            //    //Actualizar();
                                    //            //}
                                    //            //catch (Exception ex)
                                    //            //{
                                    //            //    ejecuta = false;
                                    //            //    KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoVehiculoPresenter.cs->ValidarDatos()", ex.Message);
                                    //            //    throw new Exception(ex.Message);
                                    //            //    //return;
                                    //            //}
                                    //            //this.view.FormToEntity();
                                    //            break;
                                    //    }
                                    //}
                                    //else if (this.view.ObjetoKananfleet.Equals("REFACCIONES KF")) //this.view.IsVehicleKf("REFACCIONES KF")
                                    //{
                                    //    switch (mode)
                                    //    {
                                    //        case BoFormMode.fm_ADD_MODE:
                                    //            this.ProcesaArticuloTipoHerramienta("REFACCIONES KF");
                                    //            break;
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    this.view.EmptyEntityToForm();
                                    //}
                                    break;
                                case BoFormMode.fm_UPDATE_MODE:
                                    var id = this.view.ObjetoKFID;
                                    if (ejecuta || this.view.IsVehicleKf("HERRAMIENTAS KF") || this.view.IsVehicleKf("REFACCIONES KF"))
                                    {
                                        
                                        Actualizar();
                                        
                                    }
                                    this.view.ObjetoKFID = id;
                                    if (!this.view.ObjetoKFID.Equals("0") &&  this.view.ObjetoKananfleet.Equals("REFACCIONES KF")) 
                                    {
                                        SBO_KF_OITM_INFO oitmtemp = new SBO_KF_OITM_INFO();
                                        SBO_KF_OITM_AD oArticulo = new SBO_KF_OITM_AD();
                                        oitmtemp.U_SINCRONIZABLE = "Y";
                                        oitmtemp.ItemCode = this.view.CodigoArticuloRefaccion.Trim();
                                        oitmtemp.iIdArticulo = Convert.ToInt32(this.view.ObjetoKFID); // FolioArticuloID;
                                        string response = oArticulo.actualizaHerramienta(ref this.Company, oitmtemp);
                                        this.view.CodigoArticuloRefaccion = string.Empty;
                                        this.view.ObjetoKFID = "0";
                                        this.view.ObjetoSincronizable = "";
                                        this.view.ObjetoKananfleet = "";
                                    }
                                    if (!this.view.ObjetoKFID.Equals("0") && this.view.ObjetoKananfleet.Equals("HERRAMIENTAS KF"))
                                    {
                                        SBO_KF_OITM_INFO oitmtemp = new SBO_KF_OITM_INFO();
                                        SBO_KF_OITM_AD oArticulo = new SBO_KF_OITM_AD();
                                        oitmtemp.U_SINCRONIZABLE = "Y";
                                        oitmtemp.ItemCode = this.view.CodigoArticuloRefaccion.Trim();
                                        oitmtemp.iIdArticulo = Convert.ToInt32(this.view.ObjetoKFID); // FolioArticuloID;
                                        string response = oArticulo.actualizaHerramienta(ref this.Company, oitmtemp);
                                        this.view.CodigoArticuloRefaccion = string.Empty;
                                        this.view.ObjetoKFID = "0";
                                        this.view.ObjetoSincronizable = "";
                                        this.view.ObjetoKananfleet = "";
                                    }
                                    Buscar();
                                    break;
                                case BoFormMode.fm_FIND_MODE:
                                    Buscar();
                                    break;
                            }
                            mode = BoFormMode.fm_OK_MODE;
                            ejecuta = false;
                            BubbleEvent = false;
                        }
                        #endregion Acciones

                        
                    }
                    #endregion
                }
                #endregion

                #region Form Lista de Articulos
                if (pVal.FormType == 10003 && this.view != null)
                {
                    if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD & !pVal.Before_Action)
                    {
                        Buscar();
                    }
                }
                #endregion

                #region Form Confirmar Eliminar
                if (pVal.FormType == 0)
                {
                    if (pVal.ItemUID == "1" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED & pVal.Before_Action)
                    {
                        if (IsMessageBoxElimina)
                        {
                            if (this.view != null && this.view.IsVehicleKf())
                            {
                                Eliminar();
                                BubbleEvent = false;
                                SBO_Application.Forms.ActiveForm.Close();
                                this.view.SetFormMode(BoFormMode.fm_FIND_MODE);
                            }
                            IsMessageBoxElimina = false;
                        }
                        if (bModuloDatosMaestroArticulo && bAgregaVehiculo)
                        {
                            if (this.view.Entity.GetTotaVehiculos() > this.configs.UserFull.Dependencia.EKPermitidos)
                            {
                                oHerramientas.ColocarMensaje("Ya ha excedido el máximo de vehículos permitidos para su cuenta", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                BubbleEvent = false;
                                return;
                            }
                        }
                    }

                }
                #endregion

                #region NUEVA RECARGA
                if (pVal.FormTypeEx == "GestCarga")
                {
                    if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_CLOSE && ModalCarga)
                    {
                        this.ModalCarga = false;
                    }
                }
                #endregion

                #region Admin incidencia
                if (pVal.FormTypeEx == "GestInci")
                {
                    if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_CLOSE && ModalIncidencia)
                    {
                        this.ModalIncidencia = false;
                    }
                }
                #endregion

                #region REPORTE DETALLE DE RECARGAS
                if (pVal.FormUID == "RepDet")
                {
                    if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_CLOSE && ModalRep)
                    {
                        this.ModalRep = false;
                    }
                }
                #endregion

                #region DisponibilidadVehiculos
                if (pVal.FormUID == "DVehiculos")
                {
                    if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_CLOSE && ModalDisp)
                    {
                        this.ModalDisp = false;
                    }
                }
                #endregion DisponibilidadVehiculo

                #region FORMULARIO LISTA DE SERVICIOS
                if (pVal.FormUID == "LstServ")
                {
                    if (pVal.BeforeAction)
                    {
                        if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_CLOSE && ModalServicio)
                        {
                            this.ModalServicio = false;
                        }
                    }
                }
                #endregion

                #region BOTON CANCELAR LISTA CONFIGURAR SERVICIO
                if (pVal.ItemUID == "btnCanSv")
                {
                    if (pVal.FormTypeEx == "LstServ")
                    {
                        try
                        {
                            SAPbouiCOM.Form TForm = this.SBO_Application.Forms.Item("LstServ");
                            if (TForm.Visible && TForm.Selected)
                            {
                                TForm.Close();
                            }
                            BubbleEvent = false;
                        }
                        catch
                        {
                            return;
                        }
                    }
                    else
                    {
                        this.SBO_Application.Forms.ActiveForm.Close();
                    }
                }
                #endregion

                #region CANCELAR CONFIG ORDEN SERVICIO
                if (pVal.ItemUID == "btnCConfSv")
                {
                    this.SBO_Application.Forms.ActiveForm.Close();
                }
                #endregion

                #region Sucursal
                if (pVal.FormTypeEx == "frmScAdmon")
                {
                    if (pVal.ItemUID == "btnScCanc" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                    {
                        this.ModalSucursal = false;
                        /*Comentado porque cierra el menú nativo de SAP*/
                        //this.SBO_Application.Forms.ActiveForm.Close();

                        /*Comentado porque no debemanejar este evento en esta clase.*/
                        //this.admonSucursalPresenter.view.Close();
                    }
                    if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_CLOSE && ModalSucursal)
                        this.ModalSucursal = false;
                    if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
                        this.ModalSucursal = false;
                }
                #endregion
            }
            catch (Exception ex)
            {
                BubbleEvent = false;
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoVehículoPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
                oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Warning);
            }
        }

        #region MenuEvent
        public void SBO_Application_MenuEvent(ref SAPbouiCOM.MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            //if (!IsLogged) return;
            try
            {
                if (SBO_Application.Forms.ActiveForm.Type == 150)
                {
                    if (pVal.BeforeAction)
                    {
                        if (pVal.MenuUID == "1283") //Menu Eliminar Item
                        {
                            IsMessageBoxElimina = pVal.BeforeAction;
                        }
                    }
                    else
                    {
                        //Flechas de navegación de items: 1290 - Primer Item, 1289 - Item Anterior, 1288 - item siguiente, 1291 - Ultimo item
                        if (pVal.MenuUID == "1290" || pVal.MenuUID == "1289" || pVal.MenuUID == "1288" ||
                            pVal.MenuUID == "1291")
                        {
                            Buscar();

                        }
                        if (pVal.MenuUID == "1282") //Menu Crear Item
                        {
                            this.view.EmptyEntityToForm();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                BubbleEvent = false;
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoVehiculoPresenter.cs->SBO_Application_MenuEvent()", ex.Message);
                oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                ejecuta = false;
            }
            //BubbleEvent = false;
        }
        #endregion MenuEvent

        #region Acciones
        public void Buscar()
        {
            try
            {
                if (this.view == null)
                    return;
                oItem = this.view.GetItemFromForm("5");
                var edittext = (SAPbouiCOM.EditText)(oItem.Specific);

                //Se envía el ItemCode del articulo de SAP y devuelve Objeto articulo SAP y Objeto vehículo Kananfleet.
                if (this.view.Entity.SAPToVehiculo(edittext.String.Trim()))
                {
                    this.view.DisableButtons(true);
                    this.view.EntityToForm();
                    this.GetOdometro();
                    this.GetEstatus();
                    this.view.SetFormMode(BoFormMode.fm_OK_MODE);
                    
                }
                else
                {                    
                    this.view.EmptyEntityToForm();
                    this.view.SetFormMode(BoFormMode.fm_OK_MODE);
                    this.view.DisableButtons();
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoVehiculoPresenter.cs->Buscar()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public void GetOdometro()
        {
            try
            {
                int VHID = Convert.ToInt32(this.view.Entity.vehiculo.VehiculoID);
                string odometro = VehiculoService.GetOdometro(VHID);
                this.view.SetOdometro(odometro);
            }
            catch(Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoVehiculoPresenter.cs->GetOdometro()", ex.Message);
                oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Warning);
            }
        }

        public void GetEstatus()
        {
            try
            {
                int VehiculoID = Convert.ToInt32(this.view.Entity.vehiculo.VehiculoID);
                string Estatus = VehiculoService.GetEstatus(VehiculoID);
                this.view.SetEstatus(Estatus);
            }
            catch(Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoVehiculoPresenter.cs->GetEstatus()", ex.Message);
                oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Warning);
            }
        }

        public void Insertar()
        {
            bool bContinuar = true;
            try
            {
                if (view.IsVehicleKf())
                {
                    this.view.Entity.vehiculo.VehiculoID = null;
                    //Se valida si ya excedió el límite de vehiculos permitidos
                    if (this.view.Entity.GetTotaVehiculos() > this.configs.UserFull.Dependencia.EKPermitidos)
                        bContinuar = false;
                    if (bContinuar)
                    {
                        this.view.Entity.vehiculo.Propietario.EmpresaID = this.configs.UserFull.Dependencia.EmpresaID;
                        this.view.Entity.VehiculoToSAP();
                        this.view.Entity.Sincronizado = 0;
                        this.view.Entity.UUID = Guid.NewGuid();
                        Kanan.Vehiculos.BO2.Vehiculo oTemp = this.VehiculoService.Add(this.view.Entity.vehiculo);
                        this.view.Entity.vehiculo = oTemp;
                        this.view.Entity.Insert();
                        if (oTemp == null)
                            oHerramientas.ColocarMensaje("No se ha sincronizado el vehículo con la nube. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                        else
                        {
                            this.view.Entity.vehiculo.VehiculoID = oTemp.VehiculoID;
                            //this.view.Entity.Update();
                            this.view.Entity.ActualizarVehicleID();
                        }
                        //SE QUITA PARA PODER INSERTARSE
                        //this.view.EmptyItemToForm();
                        //this.view.EmptyEntityToForm();
                        //this.view.SetFormMode(BoFormMode.fm_OK_MODE);
                        oHerramientas.ColocarMensaje("Registro de vehículo KananFleet Exitoso", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
                    }
                    else oHerramientas.ColocarMensaje("Ya ha excedido el máximo de vehículos permitidos para su cuenta", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Warning);
                }
                //else if (this.view.IsVehicleKf(groupSBO: "HERRAMIENTAS KF"))
                //    this.ProcesaArticuloTipoHerramienta("HERRAMIENTAS KF");
                //else if (this.view.IsVehicleKf("REFACCIONES KF"))
                //    this.ProcesaArticuloTipoHerramienta("REFACCIONES KF");
            }
            catch (Exception ex)
            {
                this.view.Entity.Remove();
                var vehiculoId = this.view.Entity.vehiculo.VehiculoID;
                if (vehiculoId != null && vehiculoId > 0)
                    this.VehiculoService.Delete((int)vehiculoId);

                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoVehiculoPresenter.cs->Insertar()", ex.Message);
                oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
            }
        }

        public void Actualizar()
        {
            try
            {
                if (this.view.IsVehicleKf())
                {
                    if (view.Entity.ExistUDT())
                    {
                        this.view.Entity.vehiculo.Propietario.EmpresaID = this.configs.UserFull.Dependencia.EmpresaID;
                        this.view.Entity.VehiculoToSAP();
                        var id = Convert.ToInt32(this.view.Entity.oUDT.UserFields.Fields.Item("U_VehiculoID").Value);
                        this.view.Entity.vehiculo.VehiculoID = id;
                        /*Primero se realiza el update en web, posteriormente en SAP.*/
                        this.view.Entity.vehiculo.FechaAlta = this.view.FormFrozen();
                        this.view.Entity.vehiculo = this.VehiculoService.Update(this.view.Entity.vehiculo);
                        this.view.Entity.Update();
                    }
                    else
                    {
                        Insertar();
                    }
                    this.view.SetFormMode(BoFormMode.fm_OK_MODE);
                }
                //else if (this.view.IsVehicleKf("HERRAMIENTAS KF"))
                //    this.ProcesaArticuloTipoHerramienta("HERRAMIENTAS KF");
                //else if(this.view.IsVehicleKf("REFACCIONES KF"))
                //    this.ProcesaArticuloTipoHerramienta("REFACCIONES KF");

                oHerramientas.ColocarMensaje("Información actualizada correctamente.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoVehiculoPresenter.cs->Actualizar()", ex.Message);
                oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
            }
        }

        void ProcesaArticuloTipoHerramienta(string CategoriaKF)
        {
            try
            {
                //oHerramientas.ColocarMensaje(string.Format("Se registrará la refacción: {0}", this.view.objMarca), BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);

                Int32 FolioArticuloID = -1;
                oItem = this.view.GetItemFromForm("5");                
                EditText oTxtItemcode = (SAPbouiCOM.EditText)(oItem.Specific);

                oItem = this.view.GetItemFromForm("7");
                EditText oTxtItemName = (SAPbouiCOM.EditText)(oItem.Specific);
                string sMarca = "";
                string sModelo = "";
                Form oPanelLateral = null;
                //oHerramientas.ColocarMensaje("Obtener marca y modelo.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);

                //oHerramientas.ColocarMensaje(string.Format("Marca: {0}",this.view.objMarca), BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
                //oHerramientas.ColocarMensaje(string.Format("Modelo: {0}", this.view.objModelo), BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);

                try
                {
                    oPanelLateral = this.view.GetPanelLateral();
                     
                    EditText oCampousuario = (EditText)oPanelLateral.Items.Item(oPanelLateral.Items.Item("U_Marca").UniqueID).Specific;
                    sMarca = oCampousuario.String;

                    EditText oCampousuario1 = (EditText)oPanelLateral.Items.Item(oPanelLateral.Items.Item("U_Modelo").UniqueID).Specific;
                    sModelo = oCampousuario1.String;  
                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                //oHerramientas.ColocarMensaje(string.Format("Marca: {0}", sMarca), BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
                //oHerramientas.ColocarMensaje(string.Format("Modelo: {0}", this.view.objModelo), BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success); 

                string ItemCode = oTxtItemcode.String.Trim();
                string ItemName = oTxtItemName.String.Trim();
                 

                SBO_KF_ARTICULOS_WS oHerramientaWeb = new SBO_KF_ARTICULOS_WS();
                SBO_KF_OITM_INFO oHerramientaSBO = new SBO_KF_OITM_INFO();
                SBO_KF_OITM_AD oArticulo = new SBO_KF_OITM_AD();
                List<SBO_KF_OITM_INFO> lstArticulos = new List<SBO_KF_OITM_INFO>();
                string response = oArticulo.buscaArticulo(ref this.Company, ref lstArticulos, ItemCode: ItemCode);
                //if (response.Equals("OK", StringComparison.InvariantCultureIgnoreCase) && lstArticulos.Count > 0)
                //{
                //    bool bUpdate =  lstArticulos[0].iIdArticulo > 0;
                //    oHerramientaSBO = lstArticulos[0];
                //    response = oHerramientaWeb.CreaEditaHerramientasWebApi(GestionGlobales.sURLWs, oHerramientaSBO, this.configs.UserFull.Dependencia.EmpresaID, out FolioArticuloID, bUpdate, CategoriaKF);
                //    if (response == "OK" && FolioArticuloID > 0)
                //    {
                //        SBO_KF_OITM_INFO oitmtemp = new SBO_KF_OITM_INFO();
                //        oitmtemp.U_SINCRONIZABLE = "Y";
                //        oitmtemp.ItemCode = oTxtItemcode.String.Trim();
                //        oitmtemp.iIdArticulo = FolioArticuloID;
                //        response = oArticulo.actualizaHerramienta(ref this.Company, oitmtemp);
                //    }
                //}
                //else
                //{
                bool bUpdate = false;
                oHerramientaSBO.U_Marca = this.view.objMarca;
                oHerramientaSBO.U_Modelo = this.view.objModelo;
                oHerramientaSBO.iIdArticulo = 0;
                    if (response.Equals("OK", StringComparison.InvariantCultureIgnoreCase) && lstArticulos.Count > 0)
                    {
                        oHerramientaSBO.iIdArticulo = lstArticulos[0].iIdArticulo;
                        //oHerramientaSBO.U_Marca = lstArticulos[0].U_Marca;
                        //oHerramientaSBO.U_Modelo = lstArticulos[0].U_Modelo;
                        bUpdate = lstArticulos[0].iIdArticulo > 0;
                    }
                        

                    
                    //oHerramientaSBO = lstArticulos[0];
                    
                    oHerramientaSBO.ItemCode = ItemCode;
                    oHerramientaSBO.ItemName = ItemName;
                    oHerramientaSBO.FrgnName = ItemName;
                    

                    response = oHerramientaWeb.CreaEditaHerramientasWebApi(GestionGlobales.sURLWs, oHerramientaSBO, this.configs.UserFull.Dependencia.EmpresaID, out FolioArticuloID, bUpdate, CategoriaKF);
                    this.view.ObjetoKFID = FolioArticuloID.ToString();
                    this.view.CodigoArticuloRefaccion = ItemCode;
                    //if (response == "OK" && FolioArticuloID > 0)
                    //{
                    //    SBO_KF_OITM_INFO oitmtemp = new SBO_KF_OITM_INFO();
                    //    oitmtemp.U_SINCRONIZABLE = "Y";
                    //    oitmtemp.ItemCode = oTxtItemcode.String.Trim();
                    //    oitmtemp.iIdArticulo = FolioArticuloID;
                    //    response = oArticulo.actualizaHerramienta(ref this.Company, oitmtemp);
                    //}
                //}
            }
            catch { }
        }

        public void Eliminar()
        {
            try
            {
                this.view.FormToEntity();
                if (this.view.Entity.SetUDTByItemCode())
                {
                    var id = Convert.ToInt32(this.view.Entity.oUDT.UserFields.Fields.Item("U_VehiculoID").Value);
                    this.view.Entity.vehiculo = this.VehiculoService.GetByID(id);
                    var vehiculoId = this.view.Entity.vehiculo.VehiculoID;
                    if (vehiculoId != null && vehiculoId > 0)
                        this.VehiculoService.Delete((int)vehiculoId);
                }
                this.view.Entity.Remove();
                this.view.EmptyItemToForm();
                this.view.EmptyEntityToForm();
                oHerramientas.ColocarMensaje("Eliminación exitosa.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);

            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("CatálogoVehiculoPresenter.cs->Eliminiar()", ex.Message);
                oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
            }
        }
        #endregion

    }
}