﻿using Kanan.Vehiculos.BO2;
using KananSAP.GestionVehiculos.CatalogoVehiculo.Views;
using KananWS.Interface;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KananSAP.Vehiculos.Data;
using Kanan.Mantenimiento.BO2;
using SAPinterface.Data.PD;
using SAPinterface.Data.INFO.Objetos;
using KananSAP.Mantenimiento.Data;
using SAPbobsCOM;
using SAPinterface.Data.INFO.Tablas;
using SAPinterface.Data.WS;

namespace KananSAP.GestionVehiculos.CatalogoVehiculo.Presenter
{
    public class ListaConfigTipoVehiculoPresenter
    {
        #region Variables
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private SAPbouiCOM.Item oItem;
        public ListaConfigTipoVehiculoView view;
        private Configurations Configs;
        //private ConfigPmtroServicioTVehiculoPresenter cParametroPresenter;
        private TipoVehiculosWS tVehiculoWS;
        private TipoVehiculoSAP tVehiculoData;
        private bool Modal { get; set; }
        private int? IDParametroWs = -1;
        private ConfigPmtroServicioTVehiculoView cParamTServicioView;
        private ParametroServicioWS pmtroServicioWS;
        private KananSAP.Mantenimiento.Data.ParametroServicioData pmtroServicioData;
        private int OSRowIndex = -1;
        #endregion

        public ListaConfigTipoVehiculoPresenter(SAPbouiCOM.Application Application, SAPbobsCOM.Company company, Configurations config)
        {
            this.Modal = false;
            this.Configs = config;
            this.SBO_Application = Application;
            this.Company = company;
            this.view = new ListaConfigTipoVehiculoView(this.SBO_Application, this.Company);
            this.view.Configs = config;
            this.tVehiculoWS = new TipoVehiculosWS(Guid.Parse(config.UserFull.Usuario.PublicKey), config.UserFull.Usuario.PrivateKey);
            this.tVehiculoData = new TipoVehiculoSAP(company);
            this.cParamTServicioView = new ConfigPmtroServicioTVehiculoView(this.SBO_Application, this.Company, this.Configs);
            this.pmtroServicioData = new KananSAP.Mantenimiento.Data.ParametroServicioData(company);
            pmtroServicioWS = new ParametroServicioWS(Guid.Parse(config.UserFull.Usuario.PublicKey), config.UserFull.Usuario.PrivateKey);
            this.SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
            
        }

        private void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (pVal.FormTypeEx == "FrmCLstTVh")
                {
                    if(this.Modal)
                    {
                        this.cParamTServicioView.ShowForm();
                        BubbleEvent = false;
                        return;
                    }
                    #region beforeaction

                    if (pVal.BeforeAction)
                    {
                        if (pVal.EventType == BoEventTypes.et_MATRIX_LINK_PRESSED)
                        {
                            try
                            {
                                this.view.SetTipoVehiculoMtxID(pVal.Row);
                                //this.cParamTServicioView.ServicioID = this.view.ServicioID;
                                if(this.view.TipoVehiculo.TipoVehiculoID != null)
                                {
                                    //this.cParametroPresenter = null;
                                    //this.cParametroPresenter = new ConfigPmtroServicioTVehiculoPresenter(this.SBO_Application, this.Company, this.Configs);
                                    this.cParamTServicioView.LoadForm();
                                    this.cParamTServicioView.ShowForm();
                                    this.cParamTServicioView.TipoVehiculo = this.view.TipoVehiculo;
                                    this.Consultar();
                                    this.Modal = true;
                                }
                            }
                            catch (Exception ex)
                            {
                                KananSAP.Helper.KananSAPHerramientas.LogError("ListaConfigTipoVehiculoPresenter.cs->MatrixLinkPressed()", ex.Message);
                                SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                                BubbleEvent = false;
                                this.Modal = false;
                            }
                        }

                        
                        if (pVal.ItemUID == "btnBsqTVh" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.view.SetConditions();
                            this.view.BindDataToForm();
                        }
                        if (pVal.ItemUID == "btnCanCTVh" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.SBO_Application.Forms.ActiveForm.Close();
                        }
                    }

                    #endregion
                }
                
                if (pVal.FormTypeEx == "FormCPSTVh")
                {
                    #region beforeaction

                    if (pVal.BeforeAction)
                    {
                        if (pVal.ItemUID == "btnAddCTVh" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.InsertaOActualiza();
                            BubbleEvent = false;
                        }

                        if (pVal.ItemUID == "btnEdt" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.DeterminaRowSelected();
                            BubbleEvent = false;
                        }


                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnCanCTVh")
                        {
                            this.Modal = false;
                            this.SBO_Application.Forms.ActiveForm.Close();
                        }
                        if (pVal.EventType == BoEventTypes.et_FORM_CLOSE && this.Modal)
                            this.Modal = false;
                        if (pVal.EventType == BoEventTypes.et_FORM_UNLOAD)
                            this.Modal = false;
                        if (pVal.ItemUID == "GvCsto" && (pVal.EventType == BoEventTypes.et_ITEM_PRESSED  || pVal.EventType == BoEventTypes.et_CLICK))
                            this.OSRowIndex = pVal.Row;
                    }
                    else {
                        if (pVal.ItemUID == "cmbtype")
                            this.cParamTServicioView.DeterminaParametrizacion();

                        if (pVal.ItemUID == "cmbparam")
                            this.cParamTServicioView.ocultaParametros();
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("ListaConfigTipoVehiculoPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
                SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                BubbleEvent = false;
            }
        }

        #region Metodos Configuracion de Parámetros de Mantenimiento

        #region ABC WEB SERVICE

        private void InsertarWS()
        {
           /* try
            {
                Kanan.Mantenimiento.BO2.ParametroServicio ps = this.pmtroServicioWS.Insertar(this.cParamTServicioView.ParametroServicio);
                if (ps == null)
                    throw new Exception("Es posible que la información no se haya guardado correctamente");
                if (ps.ParametroServicioID == null)
                    throw new Exception("Es posible que la información no se haya guardado correctamente");
                this.cParamTServicioView.FormToEntity();
                this.cParamTServicioView.ParametroServicio.ParametroServicioID = ps.ParametroServicioID;
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("ListaConfigTipoVehiculoPresenter.cs->InsertarWs()", ex.Message);
                throw new Exception(ex.Message);
            }*/
        }

        private void ActualizarWS()
        {
            try
            {
                bool update = this.pmtroServicioWS.Actualizar(this.cParamTServicioView.ParametroServicio, false);
                if (!update)
                    throw new Exception("No fue posible modificar la información.");

            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("ListaConfigTipoVehiculoPresenter.cs->ActualizarWS()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void ActualizarTodosWS()
        {
            try
            {
                bool update = this.pmtroServicioWS.Actualizar(this.cParamTServicioView.ParametroServicio, true);
                if (!update)
                    throw new Exception("No fue posible modificar la información.");

            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("ListaConfigTipoVehiculoPresenter.cs->ActualizarTodosWS()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region ABC SAP
        private void Insertar()
        {
            try
            {
                this.pmtroServicioData.oParametroServicio = this.cParamTServicioView.ParametroServicio;
                this.pmtroServicioData.ParametroServicioToUDT();
                this.pmtroServicioData.Insert();
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("ListaConfigTipoVehiculoPresenter.cs->Insertar()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void Actualizar()
        {
            /*try
            {
                this.pmtroServicioData.ItemCode = this.cParamTServicioView.ParametroServicio.ParametroServicioID.ToString();
                if (!this.pmtroServicioData.Existe())
                    throw new Exception("El registro que intenta actualizar no existe");
                this.cParamTServicioView.FormToEntity();
                this.pmtroServicioData.oParametroServicio = this.cParamTServicioView.ParametroServicio;
                this.pmtroServicioData.ParametroServicioToUDT();
                this.pmtroServicioData.Actualizar();
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("ListaConfigTipoVehiculoPresenter.cs->Actualizar()", ex.Message);
                throw new Exception(ex.Message);
            }*/
        }

        private void ActualizarTodos()
        {
            /*try
            {
                this.pmtroServicioData.ItemCode = this.cParamTServicioView.ParametroServicio.ParametroServicioID.ToString();
                if (!this.pmtroServicioData.Existe())
                    throw new Exception("El registro que intenta actualizar no existe");
                this.cParamTServicioView.FormToEntity();
                this.pmtroServicioData.oParametroServicio = this.cParamTServicioView.ParametroServicio;
                this.pmtroServicioData.ParametroServicioToUDT();
                this.pmtroServicioData.ActualizarTodos();

            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("ListaConfigTipoVehiculoPresenter.cs->ActualizarTodos()", ex.Message);
                throw new Exception(ex.Message);
            }*/
        }

        public void Consultar()
        {
            try
            {
                this.cParamTServicioView.ParametroServicio.Mantenible = this.cParamTServicioView.TipoVehiculo;
                this.cParamTServicioView.ParametroServicio.Servicio.Nombre = String.Empty;
                
                #region LimpiaParametros
                this.cParamTServicioView.ParametroServicio.ParametroServicioID = null;
                this.cParamTServicioView.ParametroServicio.Servicio.ServicioID = null;
                this.cParamTServicioView.ParametroServicio.Alerta = null;
                this.cParamTServicioView.ParametroServicio.Valor = null;
                #endregion LimpiaParametros

                SAPbobsCOM.Recordset rs = this.pmtroServicioData.Consultar(this.cParamTServicioView.ParametroServicio);
                if (this.pmtroServicioData.HashParametroServicio(rs))
                {
                    this.cParamTServicioView.ParametroServicio = pmtroServicioData.LastRecordSetToParametroServicio(rs);
                    this.cParamTServicioView.UpdateForm();
                }
                else
                {
                    this.cParamTServicioView.ParametroServicio.ParametroServicioID = null;
                    this.cParamTServicioView.ParametroServicio.Servicio.ServicioID = null;
                    this.cParamTServicioView.ParametroServicio.Alerta = null;
                    this.cParamTServicioView.ParametroServicio.Valor = null;
                }
                this.cParamTServicioView.EntityToForm();
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("ListaConfigTipoVehiculoPresenter.cs->Consultar()", ex.Message);
                throw new Exception(ex.Message);
            }
        }
        #endregion

        /// <summary>
        /// Ejecuta los metodos del web service y de sap Insertar o Actualizar
        /// </summary>
        /// 
        private void DeterminaRowSelected()
        {
            try
            {
                if (this.OSRowIndex >= 0)
                {

                    SBO_KF_PARAMTROMANTEN_INFO oParametro = this.cParamTServicioView.lstparms[this.OSRowIndex];
                    this.cParamTServicioView.CargaParamFromGrid(oParametro);
                    IDParametroWs = oParametro.Identificador;
                }
                else new KananSAP.Helper.KananSAPHerramientas(this.SBO_Application).ColocarMensaje("Favor de seleccionar el registro para poder editar", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
            catch
            {
                new KananSAP.Helper.KananSAPHerramientas(this.SBO_Application).ColocarMensaje("El valor seleccionado no es válido", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
        
        }
        private void InsertaOActualiza()
        {
            try
            {
                SBO_KF_PARAMETROMNTB_WS ocontrolws = new SBO_KF_PARAMETROMNTB_WS();
                SBO_KF_PARAMTROMANTEN_INFO oParam = new SBO_KF_PARAMTROMANTEN_INFO();
                SBO_KF_SERVPARAMETRO_PD control = new SBO_KF_SERVPARAMETRO_PD();
                string response = this.cParamTServicioView.FormToEntity(ref oParam);
                if (response == "OK")
                { 
                    SBO_KF_PARAMTROMANTEN_INFO oParamws = new SBO_KF_PARAMTROMANTEN_INFO();
                    if (this.OSRowIndex >= 0)
                        oParam.Identificador = (int)IDParametroWs;

                    response = ocontrolws.GestionParametroTipoVehiculo(GestionGlobales.sURLWs, ref oParamws, oParam, this.OSRowIndex >= 0);
                    if (response == "OK")
                    {
                        oParam.Identificador = oParamws.Identificador;
                        response = this.OSRowIndex >= 0 ? control.UpdateParametro(ref this.Company, oParam) : control.InsertaParametro(ref this.Company, oParam);

                        if (response == "OK")
                        {
                            this.cParamTServicioView.llenaMatrix();
                            new KananSAP.Helper.KananSAPHerramientas(this.SBO_Application).ColocarMensaje(OSRowIndex >= 0 ? "El registro se ha actualizado con exito" : "El parametro se ha registrado con éxito", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                            this.OSRowIndex = -1;
                        }
                        else
                            new KananSAP.Helper.KananSAPHerramientas(this.SBO_Application).ColocarMensaje(string.Format("No se ha podido crear el parametro de mantenimietno. ERROR {0}", response), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                    }
                    else new KananSAP.Helper.KananSAPHerramientas(this.SBO_Application).ColocarMensaje(string.Format("No se ha podido crear el parametro de mantenimietno. ERROR {0}", response), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                
                }
                else new KananSAP.Helper.KananSAPHerramientas(this.SBO_Application).ColocarMensaje(string.Format("No se ha podido crear el parametro de mantenimietno. ERROR {0}", response), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);

                this.OSRowIndex = -1;

               /* int aplicados = 0, fallos = 0 ; 
                ServiciosWS servicioWS = new ServiciosWS(Guid.Parse(this.Configs.UserFull.Usuario.PublicKey), this.Configs.UserFull.Usuario.PrivateKey);
                List<SBO_KF_VEHICULOS_INFO> lstVehiculos = new List<SBO_KF_VEHICULOS_INFO>();
                SBO_KF_VEHICULOS_PD ocontrolVehiculo = new SBO_KF_VEHICULOS_PD();
                ParametroServicio oParam = new ParametroServicio();
                ParametroMantenimiento oParammto = new ParametroMantenimiento();
                string response = this.cParamTServicioView.FormToEntity(ref oParam, ref oParammto);
                if (response == "OK")
                {
                    response = ocontrolVehiculo.listaVehiculos(ref this.Company, ref lstVehiculos, TipoID: Convert.ToInt32(this.cParamTServicioView.TipoVehiculo.TipoVehiculoID));
                    if (response == "OK")
                    {
                        foreach (SBO_KF_VEHICULOS_INFO oVehiculo in lstVehiculos)
                        {
                            try
                            {
                                this.Company.StartTransaction();
                                oParammto.Mantenible.MantenibleID = oVehiculo.U_VEHICULOID;
                                oParammto.ParametroServicio.Servicio = oParammto.ParametroServicio.Servicio;
                                oParammto.ParametroServicio.Servicio = oParam.Servicio;
                                ParametroMantenimiento pmTmp = servicioWS.InsertarCompleto(oParammto);
                                oParam.ParametroServicioID = pmTmp.ParametroMantenimientoID;

                                ParametroMantenimientoData paraMantenimientoData = new ParametroMantenimientoData(this.Company);
                                ParametroServicioData paraServicioData = new ParametroServicioData(this.Company);
                                paraServicioData.oParametroServicio = new ParametroServicio();
                                paraServicioData.oParametroServicio.Mantenible = new Vehiculo();
                                paraServicioData.oParametroServicio.Mantenible.MantenibleID = oVehiculo.U_VEHICULOID;
                                paraServicioData.oParametroServicio.Servicio = new Servicio();
                                paraServicioData.oParametroServicio.Servicio = oParam.Servicio;
                                paraServicioData.oParametroServicio.ParametroServicioID = pmTmp.ParametroMantenimientoID;
                                if (this.cParamTServicioView.bTypeRutina)
                                    paraServicioData.oParametroServicio.PlantillaID = oParam.Servicio.ServicioID;
                                //paraServicioData.oParametroServicio = oParammto.ParametroServicio;
                                paraServicioData.ParametroServicioToUDT();
                                paraServicioData.Insert();
                                
                                //Recordset rs = paraServicioData.Consultar(oParammto.ParametroServicio);
                                //if (paraServicioData.HashParametroServicio(rs))
                                //    oParammto.ParametroServicio = paraServicioData.LastRecordSetToParametroServicio(rs);

                                oParammto.Mantenible = paraServicioData.oParametroServicio.Mantenible;
                                oParammto.ParametroServicio = new ParametroServicio();
                                oParammto.ParametroServicio.Servicio = paraServicioData.oParametroServicio.Servicio;
                                oParammto.ParametroMantenimientoID = pmTmp.ParametroMantenimientoID;

                                if (this.cParamTServicioView.bTypeRutina)
                                    oParammto.PlantillaID = oParam.Servicio.ServicioID;

                                paraMantenimientoData.oParametroMantenimiento = oParammto;
                                paraMantenimientoData.ParametroMantenimientoToUDT();
                                paraMantenimientoData.Insert();

                                this.Company.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                aplicados++;
                            }
                            catch (Exception ex)
                            {
                                fallos++;
                                new KananSAP.Helper.KananSAPHerramientas(this.SBO_Application).ColocarMensaje(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                if (this.Company.InTransaction)
                                    this.Company.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            }
                        }
                        new KananSAP.Helper.KananSAPHerramientas(this.SBO_Application).ColocarMensaje(string.Format("Se han aplicado un total de {0} parametrizaciones de un total de {1} vehículos. Se registraron un total de {2} fallos", aplicados, lstVehiculos.Count, fallos), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                        this.SBO_Application.Forms.ActiveForm.Close();
                    }
                    else new KananSAP.Helper.KananSAPHerramientas(this.SBO_Application).ColocarMensaje("No se han encontrado vehículos registrados con la parametrización seleccionada", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);

                }
                else new KananSAP.Helper.KananSAPHerramientas(this.SBO_Application).ColocarMensaje(string.Format("No se ha podido crear el parametro de mantenimietno. ERROR {0}", response), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                
                */

                #region  version anterior
                /*string err = this.cParamTServicioView.ValidateFieds();
                if (!string.IsNullOrEmpty(err))
                    throw new Exception(err);
                this.cParamTServicioView.FormToEntity();
                if (this.cParamTServicioView.ParametroServicio.ParametroServicioID == null)
                {
                    this.InsertarWS();
                    this.Insertar();
                }
                else
                {
                    if (this.cParamTServicioView.updateAll)
                    {
                        this.ActualizarTodosWS();
                        this.ActualizarTodos();
                    }
                    else
                    {
                        this.ActualizarWS();
                        this.Actualizar();
                    }
                }
                this.cParamTServicioView.ClearFields();
                this.SBO_Application.StatusBar.SetText("¡Registro guardado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                this.SBO_Application.Forms.Item("FormCPSTVh").Close();//.ActiveForm.Close();*/
                #endregion version anterior
            }
            catch (Exception ex)
            {
                new KananSAP.Helper.KananSAPHerramientas(this.SBO_Application).ColocarMensaje(string.Format("No se ha podido crear el parametro de mantenimietno. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                KananSAP.Helper.KananSAPHerramientas.LogError("ListaConfigTipoVehiculoPresenter.cs->InsertaOActualiza()", ex.Message);
            }
        }
        #endregion

        //private void Eliminar(int id)
        //{
        //    try
        //    {
        //        bool eliminado = this.tVehiculoWS.Eliminar(id);
        //        if (!eliminado)
        //            throw new Exception("El registro no existe");//SBO_Application.StatusBar.SetText("No fue posible eliminar el resgistro", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
        //        else
        //        {
        //            this.tVehiculoData.ItemCode = id.ToString();
        //            if (!this.tVehiculoData.ExistUDT())
        //                throw new Exception("El registro no existe");
        //            TipoVehiculo tvh = new TipoVehiculo();
        //            tvh.TipoVehiculoID = id;
        //            SAPbobsCOM.Recordset rs = this.tVehiculoData.Consultar(tvh);
        //            if(tVehiculoData.HashTipoVehiculo(rs))
        //            {
        //                tvh = this.tVehiculoData.LastRecordSetToTipoVehiculo(rs);
        //                tvh.EsActivo = false;
        //                this.tVehiculoData.TipoVehiculo = tvh;
        //                this.tVehiculoData.TipoVehiculoToUDT();
        //                this.tVehiculoData.Actualizar();
        //                SBO_Application.StatusBar.SetText("¡Registro eliminado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
        //                this.view.SetConditions();
        //                this.view.BindDataToForm();
        //            }
        //        }
                
        //    }
        //    catch(Exception ex)
        //    {
        //        SBO_Application.StatusBar.SetText("Error: " + ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
        //    }
        //}

        

    }
}
