﻿using System.Windows.Forms;
using Kanan.Mantenimiento.BO2;
using Kanan.Vehiculos.BO2;
using KananWS.Interface;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CheckBox = SAPbouiCOM.CheckBox;
using SAPinterface.Data.PD;
using SAPinterface.Data.INFO.Objetos;
using SAPinterface.Data.INFO.Tablas;

namespace KananSAP.GestionVehiculos.CatalogoVehiculo.Views
{
    public class ConfigPmtroServicioTVehiculoView
    {
        #region Variables
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.StaticText oStaticText;
        private SAPbouiCOM.ComboBox oCombo;
        private SAPbouiCOM.CheckBox oCheck;
        private Configurations Configs;
        private XMLPerformance XmlApplication;
        public List<SBO_KF_PARAMTROMANTEN_INFO> lstparms = null;
        public bool bTypeRutina { get; set; }
        string sPath = System.Windows.Forms.Application.StartupPath;
        
        public TipoVehiculo TipoVehiculo { get; set; }
        public ParametroServicio ParametroServicio { get; set; }
        public bool updateAll { get; set; }
        public int ServicioID { get; set; }
        private bool isTypeVehicule { get; set; }
        string[] uidistancia = { "txtFrecS", "txtAltSvDt" };
        //string[] uitiempo = { "txtUltSvTm", "txtPrxSvTm", "txtAltSvTm" };
        string[] uihoras = { "FrecServHr", "AlServHr" };
        string[] tabs = { "tabD",  "tabH" };
        int TipoParametro = -1;
        #endregion

        #region Constructor
        public ConfigPmtroServicioTVehiculoView(SAPbouiCOM.Application application, SAPbobsCOM.Company company, Configurations configs)
        {
            this.SBO_Application = application;
            this.Company = company;
            this.XmlApplication = new XMLPerformance(this.SBO_Application);
            TipoVehiculo = new TipoVehiculo() { Propietario = new Kanan.Operaciones.BO2.Empresa(), SubPropietario = new Kanan.Operaciones.BO2.Sucursal(),};
            ParametroServicio = new ParametroServicio() { Servicio = new Servicio(), };
            this.Configs = configs;
            lstparms = new List<SBO_KF_PARAMTROMANTEN_INFO>();
        }
        #endregion

        #region Form
        /// <summary>
        /// Carga el formulario de la lista de servicios
        /// </summary>
        public void LoadForm()
        {
            sPath = System.Windows.Forms.Application.StartupPath;
            try
            {
                this.oForm = SBO_Application.Forms.Item("FormCPSTVh");
                //this.FillComboServicios();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Invalid Form"))
                {
                    this.XmlApplication.LoadFromXML(sPath + "\\XML",
                            "ConfigPmtroServicioTVehiculo.xml");
                    
                    this.oForm = SBO_Application.Forms.Item("FormCPSTVh");
                    //this.FillComboServicios();
                    //Estabkece la ventana centrada
                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                }
                else
                {
                    SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                }
            }
        }
        /// <summary>
        /// Muestra el formulario que contiene la lista de servicios disponibles
        /// </summary>
        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
                this.Paramtriza();
                this.llenaMatrix();
            }
            catch (Exception ex)
            {
                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                this.oForm.Select();
                this.Paramtriza();
                this.llenaMatrix();
            }
        }

        public void Paramtriza(bool bInicial = true)
        {
            try
            {

                TipoParametro = 0;
                string[] combos = { "cmbtype", "cmbparam" };

                if (bInicial)
                {
                    foreach (string cmbo in combos)
                    {
                        try
                        {
                            oItem = this.oForm.Items.Item(cmbo);
                            oCombo = oItem.Specific;
                            oCombo.Item.DisplayDesc = true;
                            oCombo.Select("0", BoSearchKey.psk_ByValue);
                        }
                        catch { continue; }
                    }
                }

                foreach (string tab in tabs)
                {
                    try
                    {
                        oItem = this.oForm.Items.Item(tab);
                        oItem.Enabled = false;
                    }
                    catch { continue; }
                }

                foreach (string ui in uidistancia)
                {
                    try
                    {
                        oItem = this.oForm.Items.Item(ui);
                        oItem.Enabled = false;
                        oEditText = oItem.Specific;
                        oEditText.String = "0";
                    }
                    catch { continue; }
                }

                foreach (string ui in uihoras)
                {
                    try
                    {
                        oItem = this.oForm.Items.Item(ui);
                        oEditText = oItem.Specific;
                        oEditText.String = "0";
                    }
                    catch { continue; }
                }
            }
            catch { }
        }

        /// <summary>
        /// Establece el o los campos vacios
        /// </summary>
        public void ClearFields()
        {
            oItem = this.oForm.Items.Item("lblIDPS");
            oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            oStaticText.Caption = "0";
            oItem = this.oForm.Items.Item("lblDescTVh");
            oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            oStaticText.Caption = string.Empty;
            oItem = this.oForm.Items.Item("lblNomTVh");
            oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            oStaticText.Caption = string.Empty;
            oItem = this.oForm.Items.Item("txtAlertS");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;
            oItem = this.oForm.Items.Item("txtFrecS");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;
            
            PrepareNewEntity();
        }

        public void PrepareNewEntity()
        {
            TipoVehiculo = new TipoVehiculo() { Propietario = new Kanan.Operaciones.BO2.Empresa(), SubPropietario = new Kanan.Operaciones.BO2.Sucursal(), };
            ParametroServicio = new ParametroServicio() { Servicio = new Servicio(), };


        }

        public void llenaMatrix()
        {
            SBO_KF_SERVPARAMETRO_PD oParams = new SBO_KF_SERVPARAMETRO_PD();
            try
            {
                lstparms = new List<SBO_KF_PARAMTROMANTEN_INFO>();
                oParams.ListaParametros(ref this.Company, ref lstparms, Convert.ToInt32(this.TipoVehiculo.TipoVehiculoID));
                if (lstparms.Count > 0)
                {
                    DataTable tab = oForm.DataSources.DataTables.Item("mtxPlacas");
                    tab.Rows.Clear();
                        int index = 0;
                        foreach (SBO_KF_PARAMTROMANTEN_INFO o in lstparms)
                        {
                            tab.Rows.Add();
                            tab.SetValue("#", index, (index + 1).ToString());
                            tab.SetValue("id", index, o.Identificador.ToString());
                            tab.SetValue("basado", index, o.TipoParametro == 0 ? "Rutina" : "Servicio");
                            tab.SetValue("KMServicio", index, o.FrecuenciaDistancia.ToString());
                            tab.SetValue("KMAlerta", index, o.FrecuenciaAlertaDistancia.ToString());
                            tab.SetValue("HoraServ", index, o.FrecuenciaHora.ToString());
                            tab.SetValue("HoraAlerta", index, o.FrecuenciaAlertaHora.ToString());
                            index++;
                        }
                }
            }
            catch { }
        }

        #endregion

        #region Validaciones
        public string ValidateFieds()
        {
            try
            {
                int AlertSVal;
                int FrecSVal;
                string err = string.Empty;
                oItem = oForm.Items.Item("txtAlertS");
                oEditText = oItem.Specific;
                AlertSVal = Convert.ToInt32(oEditText.Value);
                if (string.IsNullOrEmpty(oEditText.String))
                {
                    err += ", alerta para el servicio";
                }
                    
                else if (AlertSVal < 1)
                {
                    err += ", alerta para el servicio";
                }
                oItem = oForm.Items.Item("txtFrecS");
                oEditText = oItem.Specific;
                FrecSVal = Convert.ToInt32(oEditText.Value);
                if (string.IsNullOrEmpty(oEditText.String))
                {
                    err += ", frecuencia del servicio";
                }
                else if (FrecSVal < 1)
                {
                    err += ", frecuencia del servicio";
                }
                oItem = oForm.Items.Item("cbxServ");
                oCombo = oItem.Specific;
                if (string.IsNullOrEmpty(oCombo.Value))
                {
                    err += ", servicio";
                }
                if(!string.IsNullOrEmpty(err))
                {
                    if(err.StartsWith(","))
                        err = err.Substring(1);
                    err = "Los siguientes campos son obligatorios: " + err;
                }
                return err;
            }
            catch(Exception ex)
            {
                throw new Exception("Error de validación: " + ex.Message);
            }
        }
        #endregion
       /* public void FillComboServicios()
        {
            try
            {
                KananSAP.Mantenimiento.Data.ServicioSAP svData = new KananSAP.Mantenimiento.Data.ServicioSAP(this.Company);
                Servicio sv = new Servicio();
                sv.Propietario = new Kanan.Operaciones.BO2.Empresa();
                sv.Propietario.EmpresaID = this.Configs.UserFull.Dependencia.EmpresaID;
                //sv.ServicioID = this.ServicioID;
                SAPbobsCOM.Recordset rs = svData.Consultar(sv);
                if(svData.HashServicios(rs))
                {
                    if (oCombo != null)
                        oCombo = null;
                    oItem = oForm.Items.Item("cbxServ");
                    oCombo = oItem.Specific;
                    for(int i = 0; i < rs.RecordCount; i ++)
                    {
                        int value = rs.Fields.Item("U_ServicioID").Value;
                        string description = rs.Fields.Item("Servicio").Value;
                        oCombo.ValidValues.Add(value.ToString(), description);
                        rs.MoveNext();
                    }
                    oCombo.Item.DisplayDesc = true;
                }
                oItem = oForm.Items.Item("cmbtype");
                oCombo = oItem.Specific;
                oCombo.Item.DisplayDesc = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }*/

        public void CargaParamFromGrid(SBO_KF_PARAMTROMANTEN_INFO oParam)
        {

            
            
            
            oItem = this.oForm.Items.Item("cmbparam");
            oCombo = oItem.Specific;
            oCombo.Select(oParam.ModoParametrizacion.ToString(), BoSearchKey.psk_ByValue);
            ocultaParametros();

             oItem = this.oForm.Items.Item("cmbtype");
             oCombo = oItem.Specific;
             

            if( oParam.RutinaID > 0 )
                oCombo.Select("2", BoSearchKey.psk_ByValue);
            else
                oCombo.Select("1", BoSearchKey.psk_ByValue);

            DeterminaParametrizacion();
            oItem = this.oForm.Items.Item("cmbmood");
            oCombo = oItem.Specific;

            if (oParam.RutinaID > 0)
                oCombo.Select(oParam.RutinaID.ToString(), BoSearchKey.psk_ByValue);
            else
                oCombo.Select(oParam.ServicioID.ToString(), BoSearchKey.psk_ByValue);

            oItem = this.oForm.Items.Item("txtFrecS");
            oEditText = (SAPbouiCOM.EditText)oItem.Specific;
            oEditText.String = oParam.FrecuenciaDistancia.ToString();

            oItem = this.oForm.Items.Item("txtAltSvDt");
            oEditText = (SAPbouiCOM.EditText)oItem.Specific;
            oEditText.String = oParam.FrecuenciaAlertaDistancia.ToString();

            oItem = this.oForm.Items.Item("FrecServHr");
            oEditText = (SAPbouiCOM.EditText)oItem.Specific;
            oEditText.String = oParam.FrecuenciaHora.ToString();

            oItem = this.oForm.Items.Item("AlServHr");
            oEditText = (SAPbouiCOM.EditText)oItem.Specific;
            oEditText.String = oParam.FrecuenciaAlertaHora.ToString();
            

        }
        public void EntityToForm()
        {
            try
            {
                if(this.ParametroServicio.ParametroServicioID != null)
                {
                    oItem = oForm.Items.Item("lblIDPS");
                    oStaticText = oItem.Specific;
                    oStaticText.Caption = this.ParametroServicio.ParametroServicioID.ToString();
                }
                if (this.TipoVehiculo != null)
                {
                    //this.TipoVehiculo = (TipoVehiculo)this.ParametroServicio.Mantenible;
                    if (this.TipoVehiculo.Nombre != null)
                    {
                        oItem = oForm.Items.Item("lblNomTVh");
                        oStaticText = oItem.Specific;
                        oStaticText.Caption = this.TipoVehiculo.Nombre;
                    }
                    if (this.TipoVehiculo.Descripcion != null)
                    {
                        oItem = oForm.Items.Item("lblDescTVh");
                        oStaticText = oItem.Specific;
                        oStaticText.Caption = this.TipoVehiculo.Descripcion;
                    }
                }
                if(this.ParametroServicio.Alerta != null)
                {
                    oItem = oForm.Items.Item("txtAlertS");
                    oEditText = oItem.Specific;
                    oEditText.String = this.ParametroServicio.Alerta.ToString();
                }
                if (this.ParametroServicio.Valor != null)
                {
                    oItem = oForm.Items.Item("txtFrecS");
                    oEditText = oItem.Specific;
                    oEditText.String = this.ParametroServicio.Valor.ToString();
                }
                if(this.ParametroServicio.Servicio != null)
                {
                    if(this.ParametroServicio.Servicio.ServicioID != null)
                    {
                        oItem = oForm.Items.Item("cbxServ");
                        oCombo = oItem.Specific;
                        oCombo.Select(this.ParametroServicio.Servicio.ServicioID.ToString(), BoSearchKey.psk_ByValue);
                        oCombo.Item.DisplayDesc = true;
                    }
                }
                
                
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public string FormToEntity(ref SBO_KF_PARAMTROMANTEN_INFO oParam)
        {
            string response = "OK";
            try
            {
                if (this.TipoParametro > 0)
                {
                    oItem = this.oForm.Items.Item("cmbparam");
                    oCombo = (SAPbouiCOM.ComboBox)this.oItem.Specific;
                    if (oCombo.Selected.Value != "0")
                    {


                        oParam.TipoVehiculoID = this.TipoVehiculo.TipoVehiculoID;
                        oParam.RutinaID = 0;
                        oParam.ServicioID = 0;
                        oParam.EsRutina = this.bTypeRutina;
                        oParam.EsServicio = this.bTypeRutina ? false : true;
                        oParam.TipoParametro = this.bTypeRutina ? 0 : 1;
                        oParam.ModoParametrizacion = this.TipoParametro;
                        oParam.TipoMantenibleID = 1;
                        oParam.Sincronizado = 1;
                        oParam.FrecuenciaDistancia = 0;
                        oParam.FrecuenciaAlertaDistancia = 0;
                        oParam.FrecuenciaHora = 0;
                        oParam.FrecuenciaAlertaHora = 0;

                        oItem = this.oForm.Items.Item("cmbmood");
                        oCombo = (SAPbouiCOM.ComboBox)this.oItem.Specific;
                        if (this.bTypeRutina)
                            oParam.RutinaID = Convert.ToInt32(oCombo.Selected.Value);
                        else
                            oParam.ServicioID = Convert.ToInt32(oCombo.Selected.Value);


                        if (this.TipoParametro == 1)
                        {
                            oItem = this.oForm.Items.Item("txtFrecS");
                            oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                            if (string.IsNullOrEmpty(oEditText.String))
                                throw new Exception("Determina los kilometros para la distancia del parametro");
                            oParam.FrecuenciaDistancia = Convert.ToDouble(oEditText.String);
                            if (oParam.FrecuenciaDistancia == 0)
                                throw new Exception("El valor de frecuencia de servicio debe ser mayor a cero");


                            oItem = this.oForm.Items.Item("txtAltSvDt");
                            oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                            if (string.IsNullOrEmpty(oEditText.String))
                                throw new Exception("Determina la alerta en kilometros para el parametro");
                            oParam.FrecuenciaAlertaDistancia = Convert.ToDouble(oEditText.String);
                            if (oParam.FrecuenciaAlertaDistancia == 0)
                                throw new Exception("El valor de kilometros para la alerta debe ser mayor a cero");
                        }
                        else if (this.TipoParametro == 2)
                        {

                            oItem = this.oForm.Items.Item("FrecServHr");
                            oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                            if (string.IsNullOrEmpty(oEditText.String))
                                throw new Exception("Determina la frecuencia del servicio en horas");
                            oParam.FrecuenciaHora = Convert.ToDouble(oEditText.String);
                            if (oParam.FrecuenciaHora == 0)
                                throw new Exception("El valor de la frecuencia del servicio en horas debe ser mayor a cero");


                            oItem = this.oForm.Items.Item("AlServHr");
                            oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                            if (string.IsNullOrEmpty(oEditText.String))
                                throw new Exception("Determina la alerta de horas para el parámetro");
                            oParam.FrecuenciaAlertaHora = Convert.ToDouble(oEditText.String);
                            if (oParam.FrecuenciaAlertaHora == 0)
                                throw new Exception("El valor de la alerta de horas para el parámetro debe ser mayor a cero");

                        }
                        else if (this.TipoParametro == 3)
                        {
                            oItem = this.oForm.Items.Item("txtFrecS");
                            oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                            if (string.IsNullOrEmpty(oEditText.String))
                                throw new Exception("Determina los kilometros para la distancia del parametro");
                            oParam.FrecuenciaDistancia = Convert.ToDouble(oEditText.String);
                            if (oParam.FrecuenciaDistancia == 0)
                                throw new Exception("El valor de frecuencia de servicio debe ser mayor a cero");


                            oItem = this.oForm.Items.Item("txtAltSvDt");
                            oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                            if (string.IsNullOrEmpty(oEditText.String))
                                throw new Exception("Determina la alerta en kilometros para el parametro");
                            oParam.FrecuenciaAlertaDistancia = Convert.ToDouble(oEditText.String);
                            if (oParam.FrecuenciaAlertaDistancia == 0)
                                throw new Exception("El valor de kilometros para la alerta debe ser mayor a cero");

                            oItem = this.oForm.Items.Item("FrecServHr");
                            oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                            if (string.IsNullOrEmpty(oEditText.String))
                                throw new Exception("Determina la frecuencia del servicio en horas");
                            oParam.FrecuenciaHora = Convert.ToDouble(oEditText.String);
                            if (oParam.FrecuenciaHora == 0)
                                throw new Exception("El valor de la frecuencia del servicio en horas debe ser mayor a cero");


                            oItem = this.oForm.Items.Item("AlServHr");
                            oEditText = (SAPbouiCOM.EditText)oItem.Specific;
                            if (string.IsNullOrEmpty(oEditText.String))
                                throw new Exception("Determina la alerta de horas para el parámetro");
                            oParam.FrecuenciaAlertaHora = Convert.ToDouble(oEditText.String);
                            if (oParam.FrecuenciaAlertaHora == 0)
                                throw new Exception("El valor de la alerta de horas para el parámetro debe ser mayor a cero");
                        }
                        else throw new Exception("Proporciona el tipo de parametrización");
                    }
                    else throw new Exception("Proporciona el modo rutina o servicio");
                }
                else throw new Exception("Proporciona el tipo de parametrización");

            }
            catch (Exception ex)
            {
                response = ex.Message;
            }
            return response;
        }

        public void ocultaParametros()
        {
            oItem = this.oForm.Items.Item("cmbparam");
            oCombo = (SAPbouiCOM.ComboBox)this.oItem.Specific;
            SAPbouiCOM.Folder oFolder;
            string stypeConfig = oCombo.Selected.Value;
            TipoParametro = 0;
            if (!string.IsNullOrEmpty(stypeConfig))
            {
                this.Paramtriza(false);
                switch (stypeConfig)
                {
                    case "1":
                        TipoParametro = 1;
                        oItem = this.oForm.Items.Item("tabD");
                        oItem.Enabled = true;
                        oFolder =  oItem.Specific;
                        oFolder.Select();
                        foreach (string ui in uidistancia)
                        {
                            try
                            {
                                oItem = this.oForm.Items.Item(ui);
                                oItem.Enabled = true;
                            }
                            catch { continue; }
                        }
                        break;
                         

                    case "2":
                        TipoParametro = 2;
                        oItem = this.oForm.Items.Item("tabH");
                        oItem.Enabled = true;
                        oFolder =  oItem.Specific;
                        oFolder.Select();
                        break;
                    case "3":
                        TipoParametro = 3;
                        oItem = this.oForm.Items.Item("tabD");
                        oItem.Enabled = true;
                        oFolder =  oItem.Specific;
                        oFolder.Select();
                        foreach (string tab in tabs)
                        {
                            try
                            {
                                oItem = this.oForm.Items.Item(tab);
                                oItem.Enabled = true;
                            }
                            catch { continue; }
                        }
                        foreach (string ui in uidistancia)
                        {
                            try
                            {
                                oItem = this.oForm.Items.Item(ui);
                                oItem.Enabled = true;
                            }
                            catch { continue; }
                        }
                        break;
                }
            }
        }
        public void DeterminaParametrizacion()
        {
            try
            {
                bTypeRutina = false;
                oItem = this.oForm.Items.Item("cmbtype");
                oCombo = (SAPbouiCOM.ComboBox)this.oItem.Specific;
                string stypeConfig = oCombo.Selected.Value;
                if (!string.IsNullOrEmpty(stypeConfig))
                {
                    oItem = this.oForm.Items.Item("lblmood");
                    oItem.Visible = false;
                    oItem = this.oForm.Items.Item("cmbmood");
                    oItem.Visible = false;
                    switch (stypeConfig)
                    {
                        case "1":
                            oItem = this.oForm.Items.Item("lblmood");
                            oItem.Visible = true;
                            oStaticText = oItem.Specific;
                            oStaticText.Caption = "Selecciona el servicio:";
                            oItem = this.oForm.Items.Item("cmbmood");
                            oItem.Visible = true;
                            oCombo = oItem.Specific;
                            this.FillComboServicios();
                            oCombo.Item.DisplayDesc = true;
                            oCombo.Select("0", BoSearchKey.psk_ByValue);
                            break;
                        case "2":
                            bTypeRutina = true;
                            oItem = this.oForm.Items.Item("lblmood");
                            oItem.Visible = true;
                            oStaticText = oItem.Specific;
                            oStaticText.Caption = "Selecciona la rutina:";
                            oStaticText.Item.Left = 10;
                            oItem = this.oForm.Items.Item("cmbmood");
                            oItem.Visible = true;
                            new GestionGlobales().FillCombo("Ninguno", "Code", "Name", "cmbmood", this.sQueryPlantilla(), this.oForm, this.Company);
                            oCombo = oItem.Specific;
                            oCombo.Item.DisplayDesc = true;
                            oCombo.Select("0", BoSearchKey.psk_ByValue);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void FillComboServicios()
        {
            try
            {

                SBO_KF_ORDENSERVICIO_PD controlOS = new SBO_KF_ORDENSERVICIO_PD();
                List<DetalleOrden> lstServicioAuto = new List<DetalleOrden>();
                SBO_KF_DOCUMENTS_LINE_INFO oDocLine = new SBO_KF_DOCUMENTS_LINE_INFO();
                string response = controlOS.listaServiciosAutomotriz(ref this.Company, ref lstServicioAuto);

                if (response == "OK")
                {
                    oItem = this.oForm.Items.Item("cmbmood");
                    oCombo = oItem.Specific;
                    KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Ninguno");
                    foreach (DetalleOrden oServicio in lstServicioAuto)
                        oCombo.ValidValues.Add(oServicio.Servicio.ServicioID.ToString(), oServicio.Servicio.Nombre);
                }
                else throw new Exception("No se han registrado servicos"); 
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        public String sQueryPlantilla(string sCode = "")
        {
            String strQuery = "";
            if (GestionGlobales.bSqlConnection)
                strQuery = "SELECT * FROM [@VSKF_OSPLANTILLA]";
            else
                strQuery = @"SELECT * FROM ""@VSKF_OSPLANTILLA"" ";

            if (!string.IsNullOrEmpty(sCode))
            {
                strQuery += string.Format(@" WHERE ""Code"" = '{0}' ", sCode);
            }
            return strQuery;
        }
        public void UpdateForm()
        {
            try
            {
                this.oForm.Title = "Actualizar parámetro de servicio a tipo de vehículo";
                oItem = oForm.Items.Item("cbxAllTVh");
                oItem.Visible = true;
                oItem = oForm.Items.Item("lblActAll");
                oItem.Visible = true;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

    }
}
