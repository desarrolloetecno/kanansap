﻿using Kanan.Vehiculos.BO2;
using KananWS.Interface;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.GestionVehiculos.CatalogoVehiculo.Views
{
    public class TipoVehiculoView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.StaticText oStaticText;
        public string FormUniqueID { get; set; }
        private XMLPerformance XmlApplication;
        public TipoVehiculo oTipoVehiculo { get; set; }
        public Configurations Configs;
        #endregion

        #region Constructor
        public TipoVehiculoView(SAPbouiCOM.Application Application, SAPbobsCOM.Company company, Configurations configs)
        {
            this.oTipoVehiculo = new TipoVehiculo();
            this.oTipoVehiculo.Propietario = new Kanan.Operaciones.BO2.Empresa();
            this.oTipoVehiculo.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
            this.oEditText = null;
            this.SBO_Application = Application;
            this.Configs = configs;
            this.XmlApplication = new XMLPerformance(this.SBO_Application);
        }
        #endregion

        #region Form
        /// <summary>
        /// Carga el formulario de configuración de servicios
        /// </summary>
        public void LoadForm()
        {
            try
            {
                this.oForm = this.SBO_Application.Forms.Item("FormTVh");
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Invalid Form"))
                {
                    string sPath = System.Windows.Forms.Application.StartupPath;
                    this.XmlApplication.LoadFromXML(sPath + "\\XML", "TipoVehiculo.xml");
                    
                    this.oForm = SBO_Application.Forms.Item("FormTVh");
                    //Establece la ventana centrada
                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;

                }
                else
                {
                    SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                }
            }
        }

        /// <summary>
        /// Muestra el formulario de configuración de servicios
        /// </summary>
        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
            }
            catch (Exception ex)
            {
                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                this.oForm.Select();
            }
        }

        #endregion

        #region Validaciones
        public string ValidateFields()
        {
            try
            {
                string err = string.Empty;
                oItem = oForm.Items.Item("txtNombTVh");
                oEditText = oItem.Specific;
                if (string.IsNullOrEmpty(oEditText.String.Trim()))
                    err += ", nombre del vehículo";
                if (this.Configs.UserFull.Dependencia.EmpresaID == null)
                    err += ", EmpresaID";
                if (!string.IsNullOrEmpty(err))
                    if (err.StartsWith(","))
                        err = "Los siguientes campos son obligatorios: " + err.Substring(1);
                oItem = oForm.Items.Item("txtDesTVh");
                oEditText = oItem.Specific;
                if (oEditText.Value.LongCount() < 4)
                    err = "El campo descripción debe ser igual o mayor a 5 carácteres";
                return err;
            }
            catch(Exception ex)
            {
                throw new Exception("Ocurrió un error al validar los campos: " + ex.Message);
            }
        }
        #endregion

        /// <summary>
        /// Borra los datos del objeto principal TipoVehiculo y lo prepara para asignarle nuevos valores
        /// </summary>
        public void PrepareNewEntity()
        {
            this.oTipoVehiculo = null;
            this.oTipoVehiculo = new TipoVehiculo();
            this.oTipoVehiculo.Propietario = new Kanan.Operaciones.BO2.Empresa();
            this.oTipoVehiculo.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
        }

        public void FormToEntity()
        {
            try
            {
                oItem = oForm.Items.Item("lblIDTVh");
                oStaticText = oItem.Specific;
                if(oStaticText.Caption != "0")
                    this.oTipoVehiculo.TipoVehiculoID = Convert.ToInt32(oStaticText.Caption.ToString());
                oItem = oForm.Items.Item("txtNombTVh");
                oEditText = oItem.Specific;
                this.oTipoVehiculo.Nombre = oEditText.String;
                oItem = oForm.Items.Item("txtDesTVh");
                oEditText = oItem.Specific;
                this.oTipoVehiculo.Descripcion = oEditText.String;
                this.oTipoVehiculo.Propietario.EmpresaID = this.Configs.UserFull.Dependencia.EmpresaID;
            }
            catch(Exception ex)
            {
                throw new Exception("Error al obtener los datos de la interfaz: " + ex.Message);
            }
        }

        public void EntityToForm()
        {
            try
            {
                if (this.oTipoVehiculo.TipoVehiculoID != null)
                {
                    this.oForm.Title = "Actualizar tipo de vehículo";
                    oItem = oForm.Items.Item("lblIDTVh");
                    oStaticText = oItem.Specific;
                    oStaticText.Caption = this.oTipoVehiculo.TipoVehiculoID.ToString();
                }
                if (this.oTipoVehiculo.Nombre != null)
                {
                    oItem = oForm.Items.Item("txtNombTVh");
                    oEditText = oItem.Specific;
                    oEditText.String = this.oTipoVehiculo.Nombre;
                }
                if (this.oTipoVehiculo.Descripcion != null)
                {
                    oItem = oForm.Items.Item("txtDesTVh");
                    oEditText = oItem.Specific;
                    oEditText.String = this.oTipoVehiculo.Descripcion;
                }
            }
            catch(Exception ex)
            {

            }
        }

        public void ClearFields()
        {
            try
            {
                oItem = oForm.Items.Item("lblIDTVh");
                oStaticText = oItem.Specific;
                oStaticText.Caption = "0";

                oItem = oForm.Items.Item("txtNombTVh");
                oEditText = oItem.Specific;
                oEditText.String = string.Empty;

                oItem = oForm.Items.Item("txtDesTVh");
                oEditText = oItem.Specific;
                oEditText.String = string.Empty;
                PrepareNewEntity();
            }
            catch(Exception ex)
            {
                throw new Exception("ClearFields: " + ex.Message);
            }
        }

        public void HiddeDeleteButton()
        {
            try
            {
                this.oForm = this.SBO_Application.Forms.Item("FormTVh");
                //this.oForm = oItem.Specific;
                this.oItem = oForm.Items.Item("btnDelTvh");
                //this.oForm = oItem.Specific;
                this.oItem.Visible = false;
                this.oItem = oForm.Items.Item("btnCanTVh");
                //this.oForm = oItem.Specific;
                this.oItem.Left = 84;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void Close()
        {
            this.SBO_Application.Forms.ActiveForm.Close();
        }
    }
}
