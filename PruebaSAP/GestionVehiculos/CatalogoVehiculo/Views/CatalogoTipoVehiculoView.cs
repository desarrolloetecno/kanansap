﻿using KananWS.Interface;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.GestionVehiculos.CatalogoVehiculo.Views
{
    public class CatalogoTipoVehiculoView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.Button oBoton;
        private SAPbouiCOM.Matrix oMtx;
        private SAPbouiCOM.DBDataSource oDBDataSource;
        private SAPbouiCOM.DataTable oDataTable;
        private SAPbouiCOM.Conditions oConditions;
        private SAPbouiCOM.Condition oCondition;
        private XMLPerformance XmlApplication;
        string sPath = System.Windows.Forms.Application.StartupPath;
       
        public Configurations Configs { get; set; }
        public int? TVehiculoID { get; set; }
        private string Nombre { get; set; }
        #endregion
        
        public CatalogoTipoVehiculoView(SAPbouiCOM.Application Application, SAPbobsCOM.Company company)
        {
            this.SBO_Application = Application;
            this.XmlApplication = new XMLPerformance(SBO_Application);
            this.oItem = null;
            this.oEditText = null;
            this.oBoton = null;
            this.oConditions = null;
            this.oCondition = null;
        }

        #region Form
        /// <summary>
        /// Carga el formulario de la lista de servicios
        /// </summary>
        public void LoadForm()
        {
            sPath = System.Windows.Forms.Application.StartupPath;
            try
            {
                this.oForm = SBO_Application.Forms.Item("FormLstTVh");
                this.SetConditions();
                BindDataToForm();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Invalid Form"))
                {
                    this.XmlApplication.LoadFromXML(sPath + "\\XML",
                            "ListaTipoVehiculos.xml");
                    
                    this.oForm = SBO_Application.Forms.Item("FormLstTVh");
                    this.SetConditions();
                    BindDataToForm();
                    //Estabkece la ventana centrada
                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                }
                else
                {
                    SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                }
            }
        }
        /// <summary>
        /// Muestra el formulario que contiene la lista de servicios disponibles
        /// </summary>
        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
                BindDataToForm();
            }
            catch (Exception ex)
            {
                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                BindDataToForm();
                this.oForm.Select();

            }
        }

        /// <summary>
        /// Establece el o los campos vacios
        /// </summary>
        public void ClearFields()
        {
            oItem = this.oForm.Items.Item("txtBsqTVh");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;
        }

        #endregion

        #region Matrix
        /// <summary>
        /// Obtiene la lista de servicios de la base de datos
        /// </summary>
        public void BindDataToForm()
        {
            try
            {
                //oConditions = new SAPbouiCOM.Conditions();
                oDBDataSource = oForm.DataSources.DBDataSources.Item("@VSKF_TIPOVEHICULO");
                //oConditions = new SAPbouiCOM.Conditions();
                //oCondition = oConditions.Add();
                //oCondition.Alias = "U_EsActivo";
                //oCondition.Operation = BoConditionOperation.co_EQUAL;
                //oCondition.CondVal = "1";
                //oCondition.Relationship = BoConditionRelationship.cr_AND;
                //oCondition = oConditions.Add();
                //oCondition.Alias = "U_EmpresaID";
                //oCondition.Operation = BoConditionOperation.co_EQUAL;
                //oCondition.CondVal = this.Configs.UserFull.Dependencia.EmpresaID.ToString();
                oDBDataSource.Query(oConditions ?? null);
                oMtx = null;
                oItem = oForm.Items.Item("MtxTVh");
                oMtx = (SAPbouiCOM.Matrix)oItem.Specific;
                oMtx.LoadFromDataSource();
                //this.SetDeleteImage();

            }
            catch (Exception ex)
            {
                throw new Exception("BindDataToForm: " + ex.Message);
            }
        }

        /// <summary>
        /// Obtiene una fila del control Matrix que contiene la lista de servicios
        /// </summary>
        /// <param name="index">Indice de la fila que se recuperara</param>
        /// <returns></returns>
        public string GetDataRow(int index)
        {
            try
            {
                oItem = oForm.Items.Item("MtxTVh");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(1, index);
                return oEditText.String;
            }
            catch (Exception ex)
            {
                return "Error en la aplicacion";
            }
        }

        public string GetIDDataRow(int index)
        {
            try
            {
                oItem = oForm.Items.Item("MtxTVh");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(0, index);
                int i = oMtx.RowCount;
                if (index > i)
                    throw new Exception("Seleccione un elemento válido");
                if (string.IsNullOrEmpty(oEditText.String))
                    throw new Exception("Seleccione un elemento válido");
                return oEditText.String;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Condiciones
        /// <summary>
        /// Establece el id del item selecionado en el control Matrix de la lista de tipo de vehiculos
        /// </summary>
        /// <param name="index">Indice del cual se obtendra el identificador</param>
        public void SetID(int index)
        {
            try
            {
                oItem = oForm.Items.Item("MtxTVh");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(0, index);
                //this.id = Convert.ToInt32(oEditText.String.Trim());
                //this.oServicio.ServicioID = Convert.ToInt32(oEditText.String.Trim());

                oItem = oForm.Items.Item("MtxTVh");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(1, index);
                //this.service_name = oEditText.String.ToString();
                //this.oServicio.Nombre = oEditText.String.ToString();

                oItem = oForm.Items.Item("MtxTVh");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(2, index);
                //this.service_name = oEditText.String.ToString();
                //this.oServicio.Descripcion = oEditText.String.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Establece las condiciones de filtrado en el control Matrix de la lista de tipo de vehiculos
        /// </summary>
        public void SetConditions()
        {
            try
            {
                oItem = oForm.Items.Item("txtBsqTVh");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                string buscando = oEditText.String.ToString().Trim();
                oCondition = null;
                oConditions = null;
                oConditions = new SAPbouiCOM.Conditions();
                oCondition = oConditions.Add();
                oCondition.Alias = "U_EsActivo";
                oCondition.Operation = BoConditionOperation.co_EQUAL;
                oCondition.CondVal = "1";
                //oCondition.Relationship = BoConditionRelationship.cr_AND;

                //oCondition = oConditions.Add();
                //oCondition.Alias = "U_EmpresaID";
                //oCondition.Operation = BoConditionOperation.co_EQUAL;
                //oCondition.CondVal = this.Configs.UserFull.Dependencia.EmpresaID.ToString();

                if (!string.IsNullOrEmpty(buscando))
                {
                    oCondition.Relationship = BoConditionRelationship.cr_AND;
                    oCondition = oConditions.Add();
                    oCondition.Alias = "U_Nombre";
                    oCondition.Operation = BoConditionOperation.co_CONTAIN;
                    oCondition.CondVal = buscando;
                    oCondition.Relationship = BoConditionRelationship.cr_OR;

                    oCondition = oConditions.Add();
                    oCondition.Alias = "U_Descrip";
                    oCondition.Operation = BoConditionOperation.co_CONTAIN;
                    oCondition.CondVal = buscando;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        
        public bool HasRowValue(int rowIndex)
        {
            try
            {
                this.oForm = this.SBO_Application.Forms.Item("FormLstTVh");
                oMtx = oForm.Items.Item("MtxTVh").Specific;
                int rows = oMtx.RowCount;
                if (rowIndex <= 0 || rowIndex > rows)
                    return false;
                EditText txtNombre = (SAPbouiCOM.EditText)oMtx.GetCellSpecific("NomTVh", rowIndex);
                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific("#", rowIndex);
                this.TVehiculoID = null;
                this.Nombre = null;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()) && !string.IsNullOrEmpty(txtNombre.String.Trim()))
                {
                    this.TVehiculoID = Convert.ToInt32(oEditText.String.Trim());
                    this.Nombre = txtNombre.String.Trim();
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

    }
}
