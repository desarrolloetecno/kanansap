﻿using System;
using System.Collections.Generic;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using KananSAP.GestionVehiculos.RecargaCombustible;
using KananSAP.Operaciones.Data;
using SAPbouiCOM;
using KananSAP.Vehiculos.Data;
using SAPinterface.Data;
//using KananSAP.Vehiculos.Data;

//Se agregó para obtener el odómetro del vehículo.
using KananWS.Interface;
using System.Globalization;

// Comentario
namespace KananSAP.GestionVehiculos.CatalogoVehiculo.Views
{
    public class CatalogoVahiculosView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Form oFormOITM;
        private SAPbouiCOM.Item oNewItem;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.Folder oFolderItem;
        private SAPbouiCOM.StaticText oStaticText;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.ComboBox oComboBox;
        private SAPbouiCOM.Button oButton;
        public  VehiculoSAP Entity { get; private set; }
        public SucursalSAP ESucursal;
        private TipoVehiculoSAP ETipov;
        public string FormUniqueID { get; private set; }
        public string ObjetoKananfleet;
        public string ObjetoSincronizable;
        public string ObjetoKFID;
        public string CodigoArticuloRefaccion;
        //CAMOS INMSO JBASTO 20200907
        public string objMarca;
        public string objModelo;
        #endregion

        #region Constructor
        public CatalogoVahiculosView(SAPbouiCOM.Application Application, SAPbobsCOM.Company company, string formtype, int formcount) 
        {
            this.Entity = new VehiculoSAP(company);
            this.ESucursal = new SucursalSAP(company);
            this.ETipov = new TipoVehiculoSAP(company);
            this.SBO_Application = Application;
            this.SetForm(formtype, formcount);
            this.oNewItem = null;
            this.oItem = null;
            this.oFolderItem = null;
            this.oStaticText = null;
            this.oEditText = null;
            this.oComboBox = null;
            this.ObjetoKananfleet = "";
            this.ObjetoSincronizable = "";
            this.ObjetoKFID = "";
        }
        #endregion

        #region Metodos

        #region Metodos Form
        public void InsertFolder()
        {
            oNewItem = oForm.Items.Add("vehinfo", SAPbouiCOM.BoFormItemTypes.it_FOLDER);

            // use an existing folder item for grouping and setting the
            // items properties (such as location properties)
            // use the 'Display Debug Information' option (under 'Tools')
            // in the application to acquire the UID of the desired folder
            oItem = oForm.Items.Item("27");

            oNewItem.Top = oItem.Top;
            oNewItem.Height = oItem.Height;
            oNewItem.Width = oItem.Width;
            oNewItem.Left = oItem.Left + oItem.Width;

            oFolderItem = ((SAPbouiCOM.Folder)(oNewItem.Specific));

            oFolderItem.Caption = "Información de vehículo";

            // group the folder with the desired folder item
            oFolderItem.GroupWith("27");
            //oFolderItem.Pane = 14;

            // add your own items to the form
        }

        public void AddItemsToForm()
        {
            const int pane = 14;

            #region ComponentePadre

            oForm.DataSources.UserDataSources.Add("CombComp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);

            oItem = oForm.Items.Item("95");
            oNewItem = oForm.Items.Add("lblcomp", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "cmbcomp";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Componente padre";

            oItem = oForm.Items.Item("lblcomp");
            oNewItem = oForm.Items.Add("cmbcomp", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 250;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.DisplayDesc = true;

            oComboBox = ((SAPbouiCOM.ComboBox)(oNewItem.Specific));

            // bind the Combo Box item to the defined used data source
            oComboBox.DataBind.SetBound(true, "", "CombComp");
            this.FillComponente();
            #endregion ComponentePadre

            #region Sucursal

            oForm.DataSources.UserDataSources.Add("CombSuc", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);

            oItem = oForm.Items.Item("lblcomp");
            oNewItem = oForm.Items.Add("lblsuc", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "cmbsuc";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Sucursal";

            oItem = oForm.Items.Item("lblsuc");
            oNewItem = oForm.Items.Add("cmbsuc", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 250;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.DisplayDesc = true;

            oComboBox = ((SAPbouiCOM.ComboBox)(oNewItem.Specific));

            // bind the Combo Box item to the defined used data source
            oComboBox.DataBind.SetBound(true, "", "CombSuc");
            this.FillSucursal();
            #endregion

            #region Tipo Vehiculo

            oForm.DataSources.UserDataSources.Add("CombSource", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);

            oItem = oForm.Items.Item("lblsuc");
            oNewItem = oForm.Items.Add("lbltipo", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "cmbtipo";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Tipo de vehículo";

            oItem = oForm.Items.Item("lbltipo");
            oNewItem = oForm.Items.Add("cmbtipo", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 250;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.DisplayDesc = true;

            oComboBox = ((SAPbouiCOM.ComboBox)(oNewItem.Specific));

            // bind the Combo Box item to the defined used data source
            oComboBox.DataBind.SetBound(true, "", "CombSource");
            FillTipoVehiculo();
            #endregion

            #region TipoChasis
            oForm.DataSources.UserDataSources.Add("cmbtipo", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);

            oItem = oForm.Items.Item("lbltipo");
            oNewItem = oForm.Items.Add("lbltpcss", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "cmbtpchss";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Tipo de chasís";

            oItem = oForm.Items.Item("lbltpcss");
            oNewItem = oForm.Items.Add("cmbtpchss", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 250;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.DisplayDesc = true;

            oComboBox = ((SAPbouiCOM.ComboBox)(oNewItem.Specific));

            // bind the Combo Box item to the defined used data source
            oComboBox.DataBind.SetBound(true, "", "cmbtipo");
            FillTipoChasis();
            #endregion TipoChasis

            #region No Economico
            oItem = oForm.Items.Item("lbltpcss");
            oNewItem = oForm.Items.Add("lblnoecon", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtnoecon";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "No. Económinco";

            oItem = oForm.Items.Item("lblnoecon");
            oNewItem = oForm.Items.Add("txtnoecon", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            #endregion

            #region Odometro
            oItem = oForm.Items.Item("lblnoecon");
            oNewItem = oForm.Items.Add("lblodom", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtodom";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Odómetro";

            oItem = oForm.Items.Item("lblodom");
            oNewItem = oForm.Items.Add("txtodom", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));
            #endregion

            #region Marca
            oItem = oForm.Items.Item("lblodom");
            oNewItem = oForm.Items.Add("lblmarca", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtmarca";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Marca";

            oItem = oForm.Items.Item("lblmarca");
            oNewItem = oForm.Items.Add("txtmarca", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            #endregion

            #region Modelo
            oItem = oForm.Items.Item("lblmarca");
            oNewItem = oForm.Items.Add("lblmodelo", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtmodelo";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Modelo";

            oItem = oForm.Items.Item("lblmodelo");
            oNewItem = oForm.Items.Add("txtmodelo", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            #endregion

            #region Año
            oItem = oForm.Items.Item("lblmodelo");
            oNewItem = oForm.Items.Add("lblanio", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtanio";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Año";

            oItem = oForm.Items.Item("lblanio");
            oNewItem = oForm.Items.Add("txtanio", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            #endregion

            #region Placa
            oItem = oForm.Items.Item("lblanio");
            oNewItem = oForm.Items.Add("lblplaca", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtplaca";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Placa";

            oItem = oForm.Items.Item("lblplaca");
            oNewItem = oForm.Items.Add("txtplaca", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            #endregion

            #region Póliza
            oItem = oForm.Items.Item("lblplaca");
            oNewItem = oForm.Items.Add("lblpoliza", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtpoliza";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Póliza";

            oItem = oForm.Items.Item("lblpoliza");
            oNewItem = oForm.Items.Add("txtpoliza", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            #endregion

            #region Vigencia de Póliza
            oForm.DataSources.UserDataSources.Add("UDDate", SAPbouiCOM.BoDataType.dt_DATE, 254);
            oItem = oForm.Items.Item("lblpoliza");
            oNewItem = oForm.Items.Add("lblvigpol", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtvigepol";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Vigencia de la póliza";

            oItem = oForm.Items.Item("lblvigpol");
            oNewItem = oForm.Items.Add("txtvigepol", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            oEditText.DataBind.SetBound(true, "", "UDDate");
            #endregion

            #region Avisar Vencimiento antes
            oItem = oForm.Items.Item("lblvigpol");
            oNewItem = oForm.Items.Add("lblaviso", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtaviso";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Avisar vencimiento antes de";

            oItem = oForm.Items.Item("lblaviso");
            oNewItem = oForm.Items.Add("txtaviso", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));

            oItem = oForm.Items.Item("txtaviso");
            oNewItem = oForm.Items.Add("lblaviso1", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left + oItem.Width + 10;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "días";

            #endregion

            #region Plazas
            oItem = oForm.Items.Item("lblaviso");
            oNewItem = oForm.Items.Add("lblplazas", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtplazas";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Plazas";

            oItem = oForm.Items.Item("lblplazas");
            oNewItem = oForm.Items.Add("txtplazas", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            //oEditText.String = SBO_Application.Company.DatabaseName;

            #endregion

            #region Rendimiento Combustible
            oItem = oForm.Items.Item("lblplazas");
            oNewItem = oForm.Items.Add("lblrcomb", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtrcomb";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Rendimiento de combustible";

            oItem = oForm.Items.Item("lblrcomb");
            oNewItem = oForm.Items.Add("txtrcomb", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));

            oItem = oForm.Items.Item("txtrcomb");
            oNewItem = oForm.Items.Add("lblrcomb1", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left + oItem.Width + 10;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtrcomb";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Kms/Ltr";


            #endregion

            #region Rendimiento de Carga
            oItem = oForm.Items.Item("lblrcomb");
            oNewItem = oForm.Items.Add("lblrcarga", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtrcarga";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Rendimiento con carga";

            oItem = oForm.Items.Item("lblrcarga");
            oNewItem = oForm.Items.Add("txtrcarga", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));

            oItem = oForm.Items.Item("txtrcarga");
            oNewItem = oForm.Items.Add("lblrcarga1", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left + oItem.Width + 10;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtrcomb";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Kms/Ltr";
            #endregion

            #region CostoCompra
            oItem = oForm.Items.Item("lblrcarga");
            oNewItem = oForm.Items.Add("lblcoscom", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtcoscom";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Costo de compra";

            oItem = oForm.Items.Item("lblcoscom");
            oNewItem = oForm.Items.Add("txtcoscom", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            #endregion CostoCompra

            #region IAVE
            oItem = oForm.Items.Item("lblcoscom");
            oNewItem = oForm.Items.Add("lblIAVE", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtIAVE";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "IAVE";

            oItem = oForm.Items.Item("lblIAVE");
            oNewItem = oForm.Items.Add("txtIAVE", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            #endregion IAVE

            #region Descripción
            oItem = oForm.Items.Item("lblIAVE");
            oNewItem = oForm.Items.Add("lbldescr", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtdescr";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Descripción";

            oItem = oForm.Items.Item("lbldescr");
            oNewItem = oForm.Items.Add("txtdescr", SAPbouiCOM.BoFormItemTypes.it_EXTEDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 160;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 42;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            #endregion Descripcion

            #region ComberturaSeguro
            oItem = oForm.Items.Item("lbldescr");
            oNewItem = oForm.Items.Add("lblcober", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 44;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtcober";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Cobertura de seguro";

            oItem = oForm.Items.Item("lblcober");
            oNewItem = oForm.Items.Add("txtcober", SAPbouiCOM.BoFormItemTypes.it_EXTEDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 160;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 42;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            #endregion CoberturaSeguro

            #region Boton Recarga de Combustible
            oItem = oForm.Items.Item("130");
            oNewItem = oForm.Items.Add("btngest", SAPbouiCOM.BoFormItemTypes.it_BUTTON);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 200;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 20;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oButton = ((SAPbouiCOM.Button)(oNewItem.Specific));
            oButton.Caption = "Gestoría de vehículo";
            #endregion

            #region Boton Recarga de Combustible
            oItem = oForm.Items.Item("btngest");
            oNewItem = oForm.Items.Add("btnrcomb", SAPbouiCOM.BoFormItemTypes.it_BUTTON);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 200;
            oNewItem.Top = oItem.Top + 25;
            oNewItem.Height = 20;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oButton = ((SAPbouiCOM.Button)(oNewItem.Specific));
            oButton.Caption = "Agregar recarga de combustible";
            #endregion

            #region Boton importa recarga combustible CSV
            oItem = oForm.Items.Item("btnrcomb");
            oNewItem = oForm.Items.Add("btnRCcsv", SAPbouiCOM.BoFormItemTypes.it_BUTTON);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 200;
            oNewItem.Top = oItem.Top + 25;
            oNewItem.Height = 20;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;
            oButton = ((SAPbouiCOM.Button)(oNewItem.Specific));
            oButton.Caption = "Importa archivo CSV";
            #endregion Boton importa recarga combustible CSV


            #region Boton Para Configurar Servicio
            oItem = oForm.Items.Item("btnRCcsv");
            oNewItem = oForm.Items.Add("btnaddsvc", SAPbouiCOM.BoFormItemTypes.it_BUTTON);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 200;
            oNewItem.Top = oItem.Top + 25;
            oNewItem.Height = 20;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;
            oButton = (SAPbouiCOM.Button)oNewItem.Specific;
            oButton.Caption = "Configurar servicio a vehículo";
            #endregion

            #region Boton Para Asignar Sucursal a Vehiculo
            //oItem = oForm.Items.Item("btnaddsvc");
            //oNewItem = oForm.Items.Add("btnAddSuc", SAPbouiCOM.BoFormItemTypes.it_BUTTON);
            //oNewItem.Left = oItem.Left;
            //oNewItem.Width = 200;
            //oNewItem.Top = oItem.Top + 25;
            //oNewItem.Height = 20;
            //oNewItem.FromPane = pane;
            //oNewItem.ToPane = pane;
            //oButton = (SAPbouiCOM.Button)oNewItem.Specific;
            //oButton.Caption = "Asignar Sucursal";
            #endregion

            #region Boton Para Asignar Llantas a Vehiculo
            oItem = oForm.Items.Item("btnaddsvc");
            oNewItem = oForm.Items.Add("btnLlantas", SAPbouiCOM.BoFormItemTypes.it_BUTTON);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 200;
            oNewItem.Top = oItem.Top + 25;
            oNewItem.Height = 20;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;
            oButton = (SAPbouiCOM.Button)oNewItem.Specific;
            oButton.Caption = "Asignar llantas";
            #endregion

            #region Boton Para Incidencia de vehiculos
            oItem = oForm.Items.Item("btnLlantas");
            oNewItem = oForm.Items.Add("btnInci", SAPbouiCOM.BoFormItemTypes.it_BUTTON);
            oNewItem.Visible = true;
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 200;
            oNewItem.Top = oItem.Top + 25;
            oNewItem.Height = 20;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oButton = (SAPbouiCOM.Button)oNewItem.Specific;
            oButton.Caption = "Incidencia en vehículo";
            //oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));
            #endregion

            #region Boton Reporte Detalle de Recarga de Combustible
            oItem = oForm.Items.Item("btnInci");
            oNewItem = oForm.Items.Add("btnreport", SAPbouiCOM.BoFormItemTypes.it_BUTTON);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 200;
            oNewItem.Top = oItem.Top + 25;
            oNewItem.Height = 20;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oButton = ((SAPbouiCOM.Button)(oNewItem.Specific));
            oButton.Caption = "Reporte detalle de carga de combustible";
            #endregion

            #region Boton de hoja de inspeccion
            oItem = oForm.Items.Item("btnreport");
            oNewItem = oForm.Items.Add("btnhojai", SAPbouiCOM.BoFormItemTypes.it_BUTTON);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 200;
            oNewItem.Top = oItem.Top + 25;
            oNewItem.Height = 20;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;
            oButton = ((SAPbouiCOM.Button)(oNewItem.Specific));
            oButton.Caption = "Abrir una hoja de inspección";
            #endregion Boton de hoja de inspeccion

            #region PanelDisponibilidadVehiculos
            oItem = oForm.Items.Item("btnhojai");
            oNewItem = oForm.Items.Add("btnDisp", SAPbouiCOM.BoFormItemTypes.it_EXTEDIT);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 200;
            oNewItem.Top = oItem.Top + 25;
            oNewItem.Height = 35;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            oEditText.String = "Estatus de vehículo";
            oEditText.Item.Enabled = false; //Se establece como no editable.
            #endregion

            #region Simbologia
            oItem = oForm.Items.Item("btnDisp");
            oNewItem = oForm.Items.Add("lblSimb", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 200;
            oNewItem.Top = oItem.Top + 40;
            oNewItem.Height = 20;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));
            oStaticText.Caption = "Verde = Disponible";
            oStaticText.Item.Enabled = false; //Se establece como no editable.

            oItem = oForm.Items.Item("lblSimb");
            oNewItem = oForm.Items.Add("lblSimb2", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 200;
            oNewItem.Top = oItem.Top + 15;
            oNewItem.Height = 20;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));
            oStaticText.Caption = "Rojo = Incidencia";
            oStaticText.Item.Enabled = false; //Se establece como no editable.

            oItem = oForm.Items.Item("lblSimb2");
            oNewItem = oForm.Items.Add("lblSimb3", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 200;
            oNewItem.Top = oItem.Top + 15;
            oNewItem.Height = 20;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));
            oStaticText.Caption = "Amarillo = Mantenimiento";
            oStaticText.Item.Enabled = false; //Se establece como no editable.
            #endregion Simbologia
        }

        public Item GetItemFromForm(string ItemID)
        {
            try
            {
                oItem = this.oForm.Items.Item(ItemID);
                return oItem;
            }
            catch
            {
                return null;
            }
        }

        public Form GetPanelLateral()
        {
            return (Form)this.SBO_Application.Forms.GetForm("-150", this.oForm.TypeCount);
        }

        public void ChangeFolder(int panelview)
        {
            this.oForm.PaneLevel = panelview;
        }

        public void SetForm(string type, int count)
        {
            try
            {
                this.oForm = SBO_Application.Forms.GetForm(type, count);
                this.FormUniqueID = this.oForm.UniqueID;
            }
            catch (Exception ex)
            {
                this.oForm = null;
                throw new Exception(ex.Message);
            }
        }

        public void SetFormMode(SAPbouiCOM.BoFormMode mode)
        {
            this.oForm.Mode = mode;
        }

        public void FreezeForm(bool f)
        {
            this.oForm.Freeze(f);
        }

        public BoFormMode GetFormMode(string FormUID)
        {
            this.oForm = SBO_Application.Forms.Item(FormUID);
            //oForm.Mode = 
            return this.oForm.Mode;
        }

        private void FillComponente()
        {
            var rs = this.Entity.GetComponentes();
            oComboBox = oForm.Items.Item("cmbcomp").Specific;
            this.CleanComboBox(oComboBox);
            oComboBox.ValidValues.Add("0", "--Ninguno--");
            rs.MoveFirst();
            for (int i = 0; i < rs.RecordCount; i++)
            {
                string nombre = rs.Fields.Item("U_Nombre").Value.ToString();
                string value = (string)Convert.ChangeType(rs.Fields.Item("U_VehiculoID").Value, typeof(string));
                oComboBox.ValidValues.Add(value, nombre);
                rs.MoveNext();
            }
            oComboBox.Item.DisplayDesc = true;
        }

        private void FillSucursal()
        {
            var rs = this.ESucursal.Consultar(new Sucursal{EsActivo = true});
            oComboBox = oForm.Items.Item("cmbsuc").Specific;
            this.CleanComboBox(oComboBox);
            oComboBox.ValidValues.Add("0", "--Ninguno--");
            rs.MoveFirst();
            for (int i = 0; i < rs.RecordCount; i++)
            {
                string nombre = rs.Fields.Item("U_Direccion").Value.ToString();
                string value = (string)Convert.ChangeType(rs.Fields.Item("U_SucursalID").Value, typeof(string));
                oComboBox.ValidValues.Add(value, nombre);
                rs.MoveNext();
            }
            oComboBox.Item.DisplayDesc = true;
        }

        private void FillTipoVehiculo()
        {
            var rs = this.ETipov.Consultar(new TipoVehiculo{EsActivo = true});
            oComboBox = oForm.Items.Item("cmbtipo").Specific;
            this.CleanComboBox(oComboBox);
            //oComboBox.ValidValues.Add("0", "--None--");
            rs.MoveFirst();
            for (int i = 0; i < rs.RecordCount; i++)
            {
                string nombre = rs.Fields.Item("U_Nombre").Value.ToString();
                string value = (string)Convert.ChangeType(rs.Fields.Item("U_TVeiculoID").Value, typeof(string));
                oComboBox.ValidValues.Add(value, nombre);
                rs.MoveNext();
            }
            oComboBox.Item.DisplayDesc = true;
        }

        private void FillTipoChasis()
        {
            oComboBox = oForm.Items.Item("cmbtpchss").Specific;
            this.CleanComboBox(oComboBox);
            oComboBox.ValidValues.Add("0", "--None--");
            oComboBox.ValidValues.Add("1", "Auto");
            oComboBox.ValidValues.Add("2", "Moto");
            oComboBox.ValidValues.Add("4", "Monta Carga");
            oComboBox.ValidValues.Add("6", "Tracto / Camión");
            oComboBox.ValidValues.Add("7", "Caja 1");
            oComboBox.ValidValues.Add("8", "Caja 2");
            oComboBox.ValidValues.Add("9", "Doly");
            oComboBox.Item.DisplayDesc = true;
        }

        private void CleanComboBox(ComboBox combo)
        {
            if (combo == null || combo.ValidValues == null || combo.ValidValues.Count <= 0) return;
            var f = combo.ValidValues.Count;
            for (int i = 0; i < f; i++)
            {
                combo.ValidValues.Remove(i, BoSearchKey.psk_Index);
                f--;
                i--;
            }
        }
        #endregion

        #region Metodos Entity
        public void FormToEntity()
        {
            try
            {
                oItem = this.oForm.Items.Item("cmbcomp");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                this.Entity.vehiculo.ComponenteID = oComboBox.Selected == null ? (int?)null : Convert.ToInt32(oComboBox.Selected.Value);
                if (this.Entity.vehiculo.ComponenteID == 0)
                    this.Entity.vehiculo.ComponenteID = null;
            }
            catch (Exception)
            {
                this.Entity.vehiculo.ComponenteID = null;
            }

            try
            {
                oItem = this.oForm.Items.Item("cmbsuc");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                this.Entity.vehiculo.SubPropietario.SucursalID = oComboBox.Selected == null ? (int?)null : Convert.ToInt32(oComboBox.Selected.Value);
                if (this.Entity.vehiculo.SubPropietario.SucursalID == 0)
                    this.Entity.vehiculo.SubPropietario.SucursalID = null;
            }
            catch (Exception)
            {
                this.Entity.vehiculo.SubPropietario.SucursalID = null;
            }

            try
            {
                oItem = this.oForm.Items.Item("cmbtipo");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                this.Entity.vehiculo.TipoVehiculo.TipoVehiculoID = oComboBox.Selected == null ? (int?)null : Convert.ToInt32(oComboBox.Selected.Value);
            }
            catch (Exception)
            {
                this.Entity.vehiculo.TipoVehiculo.TipoVehiculoID = null;
            }

            try
            {
                oItem = this.oForm.Items.Item("cmbtpchss");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                this.Entity.vehiculo.TipoChasis = oComboBox.Selected == null ? (int?)null : Convert.ToInt32(oComboBox.Selected.Value);
            }
            catch (Exception)
            {
                this.Entity.vehiculo.TipoChasis = null;
            }

            oItem = this.oForm.Items.Item("5");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.ItemCode = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;

            oItem = this.oForm.Items.Item("txtnoecon");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.vehiculo.Nombre = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;

            oItem = this.oForm.Items.Item("txtmarca");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.vehiculo.Marca = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;

            oItem = this.oForm.Items.Item("txtmodelo");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.vehiculo.Modelo = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;

            try
            {
                oItem = this.oForm.Items.Item("txtanio");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Entity.vehiculo.Anio = String.IsNullOrEmpty(oEditText.String) ? (int?)null : (Convert.ToInt32(oEditText.String));
            }
            catch (Exception)
            {
                this.Entity.vehiculo.Anio = null;
            }

            oItem = this.oForm.Items.Item("txtplaca");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.vehiculo.Placa = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;

            oItem = this.oForm.Items.Item("txtpoliza");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.vehiculo.Poliza = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;

            try
            {
                oItem = this.oForm.Items.Item("txtvigepol");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Entity.vehiculo.VigenciaPoliza = String.IsNullOrEmpty(oEditText.String) ? (DateTime?)null : DateTime.Parse(oEditText.String);
            }
            catch (Exception)
            {
                this.Entity.vehiculo.VigenciaPoliza = null;
            }

            try
            {
                oItem = this.oForm.Items.Item("txtaviso");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Entity.vehiculo.AvisoPoliza = String.IsNullOrEmpty(oEditText.String) ? (int?)null : (Convert.ToInt32(oEditText.String));
            }
            catch (Exception)
            {
                this.Entity.vehiculo.AvisoPoliza = null;
            }

            try
            {
                oItem = this.oForm.Items.Item("txtplazas");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Entity.vehiculo.Plazas = String.IsNullOrEmpty(oEditText.String) ? (int?)null : (Convert.ToInt32(oEditText.String));
            }
            catch (Exception)
            {
                this.Entity.vehiculo.Plazas = null;
            }

            try
            {
                oItem = this.oForm.Items.Item("txtrcomb");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Entity.vehiculo.RendimientoCombustible = String.IsNullOrEmpty(oEditText.String) ? (double?)null : (Convert.ToDouble(oEditText.String));
            }
            catch (Exception)
            {
                this.Entity.vehiculo.RendimientoCombustible = null;
            }

            try
            {
                oItem = this.oForm.Items.Item("txtrcarga");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Entity.vehiculo.RendimientoCarga = String.IsNullOrEmpty(oEditText.String) ? (double?)null : (Convert.ToDouble(oEditText.String));
            }
            catch (Exception)
            {
                this.Entity.vehiculo.RendimientoCarga = null;
            }

            try
            {
                oItem = this.oForm.Items.Item("txtcoscom");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Entity.vehiculo.costo = String.IsNullOrEmpty(oEditText.String) ? (double?)null : (Convert.ToDouble(oEditText.String));
            }
            catch (Exception)
            {
                this.Entity.vehiculo.costo = null;
            }

            try
            {
                oItem = this.oForm.Items.Item("txtIAVE");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Entity.vehiculo.TarjetaIAVE = String.IsNullOrEmpty(oEditText.String) ? (String)null : oEditText.String;
            }
            catch (Exception)
            {
                this.Entity.vehiculo.TarjetaIAVE = null;
            }

            oItem = this.oForm.Items.Item("txtdescr");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.vehiculo.Descripcion = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;

            oItem = this.oForm.Items.Item("txtcober");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.vehiculo.Cobertura = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;
        }

        public string FormFrozen()
        {
            string strFrozendate = string.Empty;
            try
            {
               // SAPbouiCOM.Form oMasterCode = SBO_Application.Forms.Item("150");
                oItem = this.GetItemFromForm("10002045");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (oEditText.Value != "")
                {
                    strFrozendate = DateTime.ParseExact(oEditText.Value, "yyyyMMdd", CultureInfo.InvariantCulture,
                                       DateTimeStyles.None).ToString("yyyy-MM-ddTHH:mm:ss");
                    //strFrozendate = Convert.ToDateTime(oEditText.Value).ToString("yyyy-MM-ddTHH:mm:ss");
                }

            }
            catch (Exception ex)
            {
            }
            return strFrozendate;
        }
        public void EntityToForm()
        {
            try
            {
                oItem = this.oForm.Items.Item("cmbcomp");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                oComboBox.Select("0");
                oComboBox.Select(this.Entity.vehiculo.ComponenteID.ToString(), BoSearchKey.psk_ByDescription);
            }
            catch { }

            try
            {
                oItem = this.oForm.Items.Item("cmbsuc");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                oComboBox.Select("0");
                oComboBox.Select(this.Entity.vehiculo.SubPropietario.SucursalID.ToString());
            }
            catch { }

            try
            {
                oItem = this.oForm.Items.Item("cmbtipo");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                //oComboBox.Select("0");
                oComboBox.Select(this.Entity.vehiculo.TipoVehiculo.TipoVehiculoID.Value.ToString());
            }
            catch {}

            try
            {
                oItem = this.oForm.Items.Item("cmbtpchss");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                oComboBox.Select("0");
                if (this.Entity.vehiculo.TipoChasis != null & this.Entity.vehiculo.TipoChasis > 0)
                    oComboBox.Select(this.Entity.vehiculo.TipoChasis.Value.ToString());
                else
                    oComboBox.Select("0");
            }
            catch { }

            oItem = this.oForm.Items.Item("txtnoecon");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = !string.IsNullOrEmpty(Entity.vehiculo.Nombre) ? this.Entity.vehiculo.Nombre : string.Empty;

            oItem = this.oForm.Items.Item("txtodom");
            oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            oStaticText.Caption = string.Empty;

            //oItem = this.oForm.Items.Item("7");
            //oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            //oEditText.String = this.Entity.vehiculo.Nombre;

            oItem = this.oForm.Items.Item("txtmarca");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = !string.IsNullOrEmpty(Entity.vehiculo.Marca) ? this.Entity.vehiculo.Marca : string.Empty;

            oItem = this.oForm.Items.Item("txtmodelo");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = !string.IsNullOrEmpty(Entity.vehiculo.Modelo) ? this.Entity.vehiculo.Modelo : string.Empty;

            oItem = this.oForm.Items.Item("txtanio");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = Entity.vehiculo.Anio != null ? Entity.vehiculo.Anio.ToString() : string.Empty;

            oItem = this.oForm.Items.Item("txtplaca");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = !string.IsNullOrEmpty(Entity.vehiculo.Placa) ? this.Entity.vehiculo.Placa : string.Empty;

            oItem = this.oForm.Items.Item("txtpoliza");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = !string.IsNullOrEmpty(Entity.vehiculo.Poliza) ? this.Entity.vehiculo.Poliza : string.Empty;

            oItem = this.oForm.Items.Item("txtvigepol");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = this.Entity.vehiculo.VigenciaPoliza != null ? this.Entity.vehiculo.VigenciaPoliza.Value.ToShortDateString() : string.Empty;

            oItem = this.oForm.Items.Item("txtaviso");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = Entity.vehiculo.AvisoPoliza != null ? this.Entity.vehiculo.AvisoPoliza.ToString() : string.Empty;

            oItem = this.oForm.Items.Item("txtplazas");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = Entity.vehiculo.Plazas != null ? this.Entity.vehiculo.Plazas.ToString() : string.Empty;

            oItem = this.oForm.Items.Item("txtrcomb");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = Entity.vehiculo.RendimientoCombustible != null ? this.Entity.vehiculo.RendimientoCombustible.ToString() : string.Empty; ;

            oItem = this.oForm.Items.Item("txtrcarga");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = Entity.vehiculo.RendimientoCarga != null ? this.Entity.vehiculo.RendimientoCarga.ToString() : string.Empty; ;

            oItem = this.oForm.Items.Item("txtcoscom");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = Entity.vehiculo.costo != null ? Convert.ToString(this.Entity.vehiculo.costo) : string.Empty;

            oItem = this.oForm.Items.Item("txtdescr");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = !string.IsNullOrEmpty(this.Entity.vehiculo.Descripcion) ? this.Entity.vehiculo.Descripcion : string.Empty;

            oItem = this.oForm.Items.Item("txtcober");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = !string.IsNullOrEmpty(this.Entity.vehiculo.Cobertura) ? this.Entity.vehiculo.Cobertura : string.Empty;

            oItem = this.oForm.Items.Item("txtIAVE");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = Entity.vehiculo.TarjetaIAVE != null ? Convert.ToString(this.Entity.vehiculo.TarjetaIAVE) : string.Empty;

            //oItem = this.oForm.Items.Item("44");
            //oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            //oEditText.String = this.Entity.vehiculo.Descripcion;
        }

        public void DisableButtons(bool bDisable=false)
        {
            try
            {
                string[] lstButtons = { "btngest", "btnrcomb", "btnRCcsv", "btnaddsvc", "btnLlantas", "btnInci", "btnreport" };
                /*"txtnoecon", "txtodom", "txtmarca", "txtmodelo", "txtanio", "txtplaca", "txtpoliza", "txtvigepol", "txtaviso", "txtplazas", "txtrcomb", "txtrcarga", "txtcoscom", "txtdescr", "txtcober", "txtIAVE", "cmbcomp", "cmbsuc", "cmbtipo", "cmbtpchss" };*/
                foreach (string sbutton in lstButtons)
                {
                    oItem = this.oForm.Items.Item(sbutton);
                    oItem.Enabled = bDisable;                    
                }
            }
            catch {  }
            /*try
            {
                oItem = this.oForm.Items.Item("txtcober");
                oItem.Enabled = bDisable;
                oItem = this.oForm.Items.Item("txtIAVE");
                oItem.Enabled = bDisable;
            }
            catch { }*/
        
        }
        public void EmptyEntityToForm()
        {
            //oItem = this.oForm.Items.Item("5");
            //oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            //oEditText.String = string.Empty;

            //oItem = this.oForm.Items.Item("7");
            //oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            //oEditText.String = string.Empty;
            
            oItem = this.oForm.Items.Item("cmbcomp");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select("0");
            oComboBox.Select("0");

            oItem = this.oForm.Items.Item("cmbsuc");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select("0");
            oComboBox.Select("0");
            //oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_ByValue);

            oItem = this.oForm.Items.Item("cmbtipo");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            //oComboBox.Select("0");
            //oComboBox.Select("0");
            //oComboBox.Select(1, SAPbouiCOM.BoSearchKey.psk_Index);

            oItem = this.oForm.Items.Item("cmbtpchss");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select("0");
            oComboBox.Select("0");

            oItem = this.oForm.Items.Item("txtnoecon");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtodom");
            oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            oStaticText.Caption = string.Empty;

            oItem = this.oForm.Items.Item("txtmarca");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtmodelo");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtanio");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtplaca");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtpoliza");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtvigepol");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtaviso");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtplazas");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtrcomb");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtrcarga");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtcoscom");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtIAVE");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtdescr");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtcober");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;
        }

        public void EmptyItemToForm()
        {
            //oItem = this.oForm.Items.Item("5");
            //oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            //oEditText.String = string.Empty;

            //oItem = this.oForm.Items.Item("7");
            //oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            //oEditText.String = string.Empty;

            //oItem = this.oForm.Items.Item("39");
            //oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            //oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index);
        }

        public void ValidarDatos()
        {
            this.FormToEntity();

            oItem = this.oForm.Items.Item("txtnoecon");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (string.IsNullOrEmpty(oEditText.String))
            {
                throw new Exception("El dato 'No. Economico' es obligatorio para el registro de vehiculos KananFleet");
            }

            //Marca
            oItem = this.oForm.Items.Item("txtmarca");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (string.IsNullOrEmpty(oEditText.String))
            {
                throw new Exception("El dato 'Marca' es obligatorio para el registro de vehiculos KananFleet");
            }

            //Modelo
            oItem = this.oForm.Items.Item("txtmodelo");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (string.IsNullOrEmpty(oEditText.String))
            {
                throw new Exception("El dato 'Modelo' es obligatorio para el registro de vehiculos KananFleet");
            }

            oItem = this.oForm.Items.Item("cmbtipo");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            if (oComboBox.Selected == null)
            {
                throw new Exception("Seleccione un 'Tipo de Vehículo'");
            }

            oItem = this.oForm.Items.Item("txtanio");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (!string.IsNullOrEmpty(oEditText.String))
            {
                try
                {
                    Convert.ToInt32(oEditText.String);
                }
                catch (Exception)
                {
                    throw new Exception("El valor del campo 'Año' de ser númerico");
                }
            }

            oItem = this.oForm.Items.Item("txtvigepol");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (!string.IsNullOrEmpty(oEditText.String))
            {
                try
                {
                    DateTime.Parse(oEditText.String);
                }
                catch (Exception)
                {
                    throw new Exception("El valor del campo 'Vigencia de la Póliza' debe darse en el formato 'DDMMAAAA' ");
                }
            }

            oItem = this.oForm.Items.Item("txtcoscom");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (!string.IsNullOrEmpty(oEditText.String))
            {
                try
                {
                    Double.Parse(oEditText.String);
                }
                catch (Exception ex)
                {
                    throw new Exception("El valor del campo 'Costo de compra' debe ser de tipo numérico. " + ex.Message);
                    //SBO_Application.StatusBar.SetText("El valor del campo 'Costo de compra' debe ser de tipo numérico. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                    //return;
                }
            }

            //return result;
        }

        public bool IsVehicleKf(string groupSBO = "VEHICULOS KF")
        {
            string group = string.Empty;
            if (this.oForm == null) return false;
            var item = this.GetItemFromForm("39");
            if (item == null) return false;
            oComboBox = (SAPbouiCOM.ComboBox)(item.Specific);
            @group = oComboBox.Selected != null ? oComboBox.Selected.Description : string.Empty;
            bool bMathc = group.Trim().ToUpper().Equals(groupSBO, StringComparison.InvariantCultureIgnoreCase);
             
            Form oPanelLateral = null;
            try
            {
                oPanelLateral = (Form)this.SBO_Application.Forms.GetForm("-150", this.oForm.TypeCount);
               // for(int i =0 ; i<oPanelLateral.Items.Count; i++)
                EditText oCampousuario = (EditText)oPanelLateral.Items.Item(oPanelLateral.Items.Item("U_KF_SINCRONIZABLE").UniqueID).Specific;
                ObjetoSincronizable = oCampousuario.String; // este seria tu resultado
                EditText oCampousuario1 = (EditText)oPanelLateral.Items.Item(oPanelLateral.Items.Item("U_KF_ARTICULOID").UniqueID).Specific;
                ObjetoKFID = oCampousuario1.String; // este seria tu resultado
                EditText oCampousuario2 = (EditText)oPanelLateral.Items.Item(oPanelLateral.Items.Item("U_KF_TIPART").UniqueID).Specific;
                var tipo =  oCampousuario2.String; // este seria tu resultado
                
                
                //SE JALAN LAS CADENAS PARA MARCA Y MODELO
                try
                {
                    EditText oCampousuario3 = (EditText)oPanelLateral.Items.Item(oPanelLateral.Items.Item("U_Marca").UniqueID).Specific;
                    objMarca = oCampousuario3.String;

                    EditText oCampousuario4 = (EditText)oPanelLateral.Items.Item(oPanelLateral.Items.Item("U_Modelo").UniqueID).Specific;
                    objModelo = oCampousuario4.String;
                }
                catch(Exception ex)
                {
                    throw new Exception("Error al intentar obtener marca y modelo. Error: " + ex.Message);
                }



                switch(tipo)
                {
                    case "1":
                        ObjetoKananfleet ="VEHICULOS KF";
                        break;
                    case "2":
                        ObjetoKananfleet = "REFACCIONES KF";
                        break;
                    case "3":
                        ObjetoKananfleet = "HERRAMIENTAS KF";
                        break;
                }

                /*
                Console.WriteLine(ObjetoSincronizable);
                Console.WriteLine(ObjetoKananfleet);
                Console.WriteLine(ObjetoKFID);*/
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            

            return bMathc;
        }

       

        public void SetOdometro(string kilometro)
        {
            try
            {
                oItem = this.oForm.Items.Item("txtodom");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                oStaticText.Caption = kilometro + " kms";
                //oItem.Enabled = false;
            }
            catch(Exception ex)
            {
                oItem = this.oForm.Items.Item("txtodom");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                oStaticText.Caption = "Error";
                throw new Exception("Error al intentar cargar el odómetro del vehículo. Error: " + ex.Message);
            }
        }

        public void SetEstatus(string EstatusVehiculo)
        {
            try
            {
                //this.oForm.Items.Item("vehinfo").Click();
                
                oItem = this.oForm.Items.Item("btnDisp");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);

                switch (EstatusVehiculo)
                {
                    case "Disponible":
                        oEditText.String = "Disponible";
                        oEditText.FontSize = 25;
                        oEditText.ForeColor = 65280;
                        //oEditText.BackColor = 65280; //verde
                        break;
                    case "Incidencia":
                        oEditText.String = "Incidencia";
                        oEditText.FontSize = 25;
                        oEditText.ForeColor = 255;
                        //oEditText.BackColor = 255; //rojo
                        break;
                    case "Mantenimiento":
                        oEditText.String = "Mantenimiento";
                        oEditText.FontSize = 25;
                        oEditText.ForeColor = 65535;
                        //oEditText.BackColor = 65535; //amarillo
                        break;
                    case "Rentado":
                        oEditText.String = "Rentado";
                        oEditText.BackColor = 16776960;
                        break;

                    default:
                        oEditText.String = "Undefined";
                        break;
                }
            }
            catch (Exception ex)
            {
                oItem = this.oForm.Items.Item("btnDisp");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                oStaticText.Caption = "Error";
                throw new Exception("Error al intentar cargar el estatus del vehículo. Error: " + ex.Message);
            }
        }

        #endregion

       #endregion
    }
}
