﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kanan.Comun.BO2;
using Kanan.Costos.BO2;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using KananSAP.GestionVehiculos.RecargaCombustible.View;
using KananSAP.Reportes.Presenter;
using KananWS.Interface;
using SAPbouiCOM;
using KananSAP.Helper;
using SAPbobsCOM;
using Form = SAPbouiCOM.Form;

namespace KananSAP.GestionVehiculos.RecargaCombustible.Presenter
{
    public class GestionCargasPresenter
    {
        #region Atributo
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private SAPbouiCOM.Item oItem;
        public GestionCargasView view { get; set; }
        public RecargaView cargaview { get; set; }
        private RecargaCombustibleWS.LogSAPCargaCombustible log;
        private Configurations configs;
        private RecargaCombustibleWS cargaws;
        private bool Modal;
        private int? iSucursalID;
        private string VehiculoName { get; set; }
        private int? VehiculoID { get; set; }

        public Reportes.Presenter.ReportePresenter ReportesPresenter { get; set; }
        private string EndPoint;
        #endregion

        #region Constructor
        public GestionCargasPresenter(SAPbouiCOM.Application aplication, SAPbobsCOM.Company company, int id, string vehiclename, Configurations c, int? iSucursal)
        {
            this.configs = c;
            this.SBO_Application = aplication;
            this.Company = company;
            this.VehiculoID = id;
            this.VehiculoName = vehiclename;
            this.view = new GestionCargasView(this.SBO_Application, this.Company, this.VehiculoName, this.VehiculoID);
            //this.cargaview = new RecargaView(this.SBO_Application, this.Company, this.configs, vehiclename, id, null);
            this.cargaws = new RecargaCombustibleWS(Guid.Parse(this.configs.UserFull.Usuario.PublicKey), this.configs.UserFull.Usuario.PrivateKey);
            this.log = new RecargaCombustibleWS.LogSAPCargaCombustible();
            this.Modal = false;
            this.EndPoint = System.Configuration.ConfigurationManager.AppSettings.Get("EndPoint");
            SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
            cargaws.ObtenerUnidadVolumen();
            this.iSucursalID = iSucursal;
        }
        #endregion

        #region Eventos
        private void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (pVal.FormTypeEx == "GestCarga")
                {
                    if (Modal)
                    {
                        Modal = false;
                        BubbleEvent = false;
                        cargaview.ShowForm();
                        return;
                    }

                    #region Modales

                    #endregion

                    #region beforeaction
                    if (!pVal.BeforeAction)
                    {
                        //if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnSearch")
                        //{
                        //    this.view.BindDataToForm();
                        //}
                        if (pVal.EventType == BoEventTypes.et_FORM_LOAD)
                        {
                            
                        }
                        if (pVal.EventType == BoEventTypes.et_MATRIX_LINK_PRESSED)
                        {
                            this.view.SetID(pVal.Row);
                            if (this.view.id != null)
                            {
                                this.cargaview = new RecargaView(this.SBO_Application, ref this.Company, configs, view.name, view.vehiculoid, view.id, iSucursalID);
                                var code = ((int) this.view.id).ToString().Trim();
                                var flag = this.cargaview.Entity.oUDT.GetByKey(code);
                                if (!flag)
                                {
                                    int errornum;
                                    string errormsj;
                                    Company.GetLastError(out errornum, out errormsj);
                                    //throw new Exception("Error: " + errornum + " - " + errormsj);
                                    SBO_Application.StatusBar.SetText("Error: " + errornum + " - " + errormsj, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                    return;
                                }
                                this.cargaview.LoadForm();
                                PrepareUserForm();
                                this.cargaview.ShowForm();
                            }
                            this.Modal = true;
                            BubbleEvent = false;
                        }

                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnnew")
                        {
                            this.cargaview = null;
                            
                            this.cargaview = new RecargaView(this.SBO_Application, ref this.Company, configs, this.VehiculoName, this.VehiculoID, null, iSucursalID);
                            this.cargaview.LoadForm();
                            this.cargaview.ShowForm();
                            this.Modal = true;
                            BubbleEvent = false;
                        }

                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnextraer")
                        {
                            this.ExtraerCargasKanan();
                            this.view.BindDataToForm();
                            BubbleEvent = false;
                        }
                    
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "MtxFR")
                        {
                            this.view.SetID(pVal.Row);
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnEdCarga")
                        {
                            this.view.GetCargaFromGrid();
                           if (this.view.id != null)
                            {
                                this.cargaview = new RecargaView(this.SBO_Application, ref this.Company, configs, this.VehiculoName, this.VehiculoID, view.id, iSucursalID);
                                var code = ((int) this.view.id).ToString().Trim();
                                var flag = this.cargaview.Entity.oUDT.GetByKey(code);
                                if (!flag)
                                {
                                    int errornum;
                                    string errormsj;
                                    Company.GetLastError(out errornum, out errormsj);
                                    //throw new Exception("Error: " + errornum + " - " + errormsj);
                                    SBO_Application.StatusBar.SetText("Error: " + errornum + " - " + errormsj, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                    return;
                                }
                                this.cargaview.LoadForm();
                                PrepareUserForm();
                                this.cargaview.ShowForm();
                            }
                            this.Modal = true;
                            BubbleEvent = false;

                        }

                    }
                    #endregion

                    #region After Action
                    else
                    {
                        //if (Modal && pVal.EventType == BoEventTypes.et_FORM_ACTIVATE)
                        //{
                        //    userview.ShowForm();
                        //    BubbleEvent = false;
                        //    return;
                        //}
                    }
                    #endregion

                    //BubbleEvent = false;
                }

                if (pVal.FormTypeEx == "CargaCom")
                {
                    #region beforeaction
                    //  If the modal form is closed...
                    //if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_CLOSE & Modal)
                    //{
                    //    Modal = false;
                    //}
                    if (!pVal.BeforeAction)
                    {
                        //if (Modal)
                        //{
                        #region
                        if (pVal.ItemUID == "btnguardar" && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED && Modal)
                        {
                            try
                            {
                                string sresponse = "OK";
                                this.Company.StartTransaction();
                                this.cargaview.ValidateData();                                
                                //solo si es facturable se afecta movimiento inventario factura servicio o proveedor
                                if (this.cargaview.Entity.carga.EsFacturableSAP == 1)
                                {
                                    this.cargaview.FormToEntity();
                                    sresponse = this.cargaview.GestionarCarga(this.cargaview.Entity.carga);
                                }
                                if (sresponse == "OK")
                                {
                                    this.GuardarCargaCombustible();
                                    this.cargaview.EmptyEntityToForm();
                                    this.SBO_Application.StatusBar.SetText("Operación exitosa", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                    BubbleEvent = false;
                                    this.cargaview.CloseForm();
                                    this.Company.EndTransaction(BoWfTransOpt.wf_Commit);
                                }
                                else
                                {
                                    this.SBO_Application.StatusBar.SetText(sresponse, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                    //this.cargaview.EmptyEntityToForm();
                                    this.Company.EndTransaction(BoWfTransOpt.wf_RollBack);
                                }
                                this.view.BindDataToForm();
                            }
                            catch (Exception ex)
                            {
                                this.cargaview.EmptyEntityToForm(); 
                                this.cargaview.CloseForm();
                                this.Company.EndTransaction(BoWfTransOpt.wf_RollBack);
                                this.SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);                                                               
                                throw new Exception(ex.Message);
                            }
                            
                        }
                        if (pVal.ItemUID == "btnelimina" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED && Modal)
                        {
                            this.Delete();
                            //Modal = false;
                            BubbleEvent = false;
                            this.cargaview.CloseForm();
                            this.view.BindDataToForm();
                        }

                        //this.GestionaFacturas();
                        if (pVal.ItemUID == "oInvoice" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED && Modal)
                        {
                            //Modal = false;
                            // BubbleEvent = false;
                            //this.cargaview.GestionaFacturas();
                        }

                        if (pVal.ItemUID == "btnphoto" && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED && Modal)
                        {
                            this.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepFotos", @EndPoint + "Vehiculos/ImagenesRecarga.aspx?_modal=true&CargaCombustibleID=" + this.cargaview.Entity.carga.CargaCombustibleID);
                            this.ReportesPresenter.view.ShowForm();
                            BubbleEvent = false;
                        }
                        if (pVal.ItemUID == "btngeo" && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED && Modal)
                        {
                            this.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepGeopos", @EndPoint + "Vehiculos/GeoposicionRecarga.aspx?_modal=true&CargaCombustibleID=" + this.cargaview.Entity.carga.CargaCombustibleID);
                            //this.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepGeopos", @"http://w.administraflotilla.com/Vehiculos/GeoposicionRecarga.aspx?_modal=true&CargaCombustibleID=6757");
                            //Modal = false;
                            this.ReportesPresenter.view.ShowForm();
                            BubbleEvent = false;
                        }
                        if (pVal.EventType == BoEventTypes.et_FORM_UNLOAD)
                        {
                            this.Modal = false;
                            BubbleEvent = false;
                        }
                        #endregion
                        //}
                    }
                    #endregion

                    #region After Action
                    else
                    {
                        //if (pVal.EventType == BoEventTypes.et_FORM_LOAD)
                        //{
                        //    if (this.view.id != null)
                        //    {
                        //        Exist = this.userview.Entity.SetEmployeeRelationById((int)this.view.id);
                        //        PrepareUserForm();
                        //    }
                        //}
                    }
                    #endregion
                }
                //BubbleEvent = false;
            }

            catch (Exception ex)
            {
                Modal = false;
                BubbleEvent = false;
                if (!string.IsNullOrEmpty(ex.Message))
                    SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                KananSAPHerramientas.LogError("GestionCargasPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
            }
        }
        #endregion

        #region Metodos

        #region Acciones
        private void GuardarCargaCombustible()
        {
            try
            {
                if (string.IsNullOrEmpty(cargaview.GetCargaID().Trim()))
                {
                     Insertar();
                }
                else
                {
                    Update();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
               // KananSAP.Helper.KananSAPHerramientas.LogError("GestionCargasPresenter.cs->GuardarCargaCombustible()", ex.Message);
               // SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
               // return;
            }
        }

        private void Update()
        {
            try
            {
                var id_t = this.cargaview.GetID();
                var id = Convert.ToInt32(id_t);
                this.cargaview.FormToEntity();
                this.cargaview.Entity.carga = this.cargaws.Update(this.cargaview.Entity.carga, true,
                    this.configs.UserFull.Configs["TimeZone"],
                    this.configs.UserFull.Dependencia.EmpresaID, id);
                this.cargaview.Entity.ItemCode = this.cargaview.Entity.carga.CargaCombustibleID.ToString();
                this.cargaview.Entity.CargaToSAP();
                this.cargaview.Entity.Update();
                //this.cargaview.EmptyEntityToForm();
                //this.SBO_Application.StatusBar.SetText("Operación exitosa", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("GestionCargasPresenter.cs->Update()", ex.Message);
                SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }
        }

        private void Insertar()
        {
            try
            {
                var id_t = this.cargaview.GetID();
                var id = Convert.ToInt32(id_t);
                this.cargaview.FormToEntity();
                CargaCombustibleEnum enumcarga = NewCargaToOldCarga(this.cargaview.Entity.carga);
                this.log = this.cargaws.Add(enumcarga, true,
                    this.configs.UserFull.Configs["TimeZone"],
                    this.configs.UserFull.Dependencia.EmpresaID, id);
                if (log.Status)
                {
                    //this.cargaview.Entity.carga = log.Carga;
                    this.cargaview.Entity.carga = this.OldCargaToNewCarga(log.Carga);
                    this.cargaview.Entity.ItemCode = log.Carga.CargaCombustibleID.ToString();
                    this.cargaview.Entity.CargaToSAP();
                    this.cargaview.Entity.Insert();                    
                }
                else
                {
                    throw new Exception(log.Error);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                //KananSAP.Helper.KananSAPHerramientas.LogError("GestionCargasPresenter.cs->Insertar()", ex.Message);
                //SBO_Application.StatusBar.SetText("Error al insertar. " + ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                //return;
            }
        }

        private void Delete()
        {
            try
            {
                this.cargaview.Entity.carga = this.cargaws.Delete(this.cargaview.Entity.carga.CargaCombustibleID);
                this.cargaview.Entity.ItemCode = this.cargaview.Entity.carga.CargaCombustibleID.ToString();
                this.cargaview.Entity.Delete();
                this.cargaview.EmptyEntityToForm();
                this.SBO_Application.StatusBar.SetText("Operación exitosa", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("GestionCargasPresenter.cs->Delete()", ex.Message);
                SBO_Application.StatusBar.SetText("Error al eliminar" + ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }
        }
        #endregion

        #region Metodos Auxiliares
        private CargaCombustible OldCargaToNewCarga(CargaCombustibleEnum oldCarga)
        {
            CargaCombustible newCarga = new CargaCombustible
            {
                Movil = new Vehiculo(),
                SourceCenter = new Proveedor(),
                TipoPago = new TipoDePago(),
                Longitud = new UnidadLongitud(),
                Volumen = new UnidadVolumen(),
                TypeFuel = new TipoCombustible(),
                Moneda = new TipoMoneda(),
                ImagenRecarga = new Imagen(),
                ListImagenesRecarga = new List<ImagenCargaCombustible>()
            };

            newCarga.CargaCombustibleID = oldCarga.CargaCombustibleID;
            newCarga.Litros = oldCarga.Litros;
            newCarga.Pesos = oldCarga.Pesos;
            newCarga.Movil = oldCarga.Movil;
            newCarga.Fecha = oldCarga.Fecha;
            newCarga.SourceCenter = oldCarga.SourceCenter;
            newCarga.Odometro = oldCarga.Odometro;
            newCarga.FechaCaptura = oldCarga.FechaCaptura;
            newCarga.Ticket = oldCarga.Ticket;
            newCarga.TypeFuel.TipoCombustibleID = Convert.ToInt32(oldCarga.TypeFuel);
            newCarga.CentroServicio = oldCarga.CentroServicio;
            newCarga.TipoPago = oldCarga.TipoPago;
            newCarga.DetallePago = oldCarga.DetallePago;
            newCarga.Longitud = oldCarga.Longitud;
            newCarga.Volumen = oldCarga.Volumen;
            newCarga.UID = oldCarga.UID;
            newCarga.Latitude = oldCarga.Latitude;
            newCarga.Longitude = oldCarga.Longitude;
            newCarga.Moneda = oldCarga.Moneda;
            newCarga.ImagenRecarga = oldCarga.ImagenRecarga;
            newCarga.UrlServerImagen = oldCarga.UrlServerImagen;
            newCarga.TipoPagoID = oldCarga.TipoPagoID;
            newCarga.TipoMonedaID = oldCarga.TipoMonedaID;
            newCarga.ListImagenesRecarga = oldCarga.ListImagenesRecarga;

            return newCarga;
        }

        private CargaCombustibleEnum NewCargaToOldCarga(CargaCombustible oldCarga)
        {
            CargaCombustibleEnum newCarga = new CargaCombustibleEnum
            {
                Movil = new Vehiculo(),
                SourceCenter = new Proveedor(),
                TipoPago = new TipoDePago(),
                Longitud = new UnidadLongitud(),
                Volumen = new UnidadVolumen(),
                TypeFuel = new TipoCombustibleEnum(),
                Moneda = new TipoMoneda(),
                ImagenRecarga = new Imagen(),
                ListImagenesRecarga = new List<ImagenCargaCombustible>()
            };

            newCarga.CargaCombustibleID = oldCarga.CargaCombustibleID;
            newCarga.Litros = oldCarga.Litros;
            newCarga.Pesos = oldCarga.Pesos;
            newCarga.Movil = oldCarga.Movil;
            newCarga.Fecha = oldCarga.Fecha;
            newCarga.SourceCenter = oldCarga.SourceCenter;
            newCarga.Odometro = oldCarga.Odometro;
            newCarga.FechaCaptura = oldCarga.FechaCaptura;
            newCarga.Ticket = oldCarga.Ticket;
            int fuel = Convert.ToInt32(oldCarga.TypeFuel.TipoCombustibleID);
            newCarga.TypeFuel = (TipoCombustibleEnum)fuel;
            newCarga.CentroServicio = oldCarga.CentroServicio;
            newCarga.TipoPago = oldCarga.TipoPago;
            newCarga.DetallePago = oldCarga.DetallePago;
            newCarga.Longitud = oldCarga.Longitud;
            newCarga.Volumen = oldCarga.Volumen;
            newCarga.UID = oldCarga.UID;
            newCarga.Latitude = oldCarga.Latitude;
            newCarga.Longitude = oldCarga.Longitude;
            newCarga.Moneda = oldCarga.Moneda;
            newCarga.ImagenRecarga = oldCarga.ImagenRecarga;
            newCarga.UrlServerImagen = oldCarga.UrlServerImagen;
            newCarga.TipoPagoID = oldCarga.TipoPagoID;
            newCarga.TipoMonedaID = oldCarga.TipoMonedaID;
            newCarga.ListImagenesRecarga = oldCarga.ListImagenesRecarga;

            return newCarga;
        }
        private void PrepareUserForm()
        {
            try
            {
                var id = this.cargaview.Entity.oUDT.Code;
                var i = Convert.ToInt32(id);
                this.cargaview.Entity.ItemCode = i.ToString();
                this.cargaview.Entity.SAPToCarga();
                this.cargaview.EntityToForm();
                if (!this.cargaview.Entity.carga.Longitude.HasValue )
                {
                    this.cargaview.HidePhotoButtons();
                }

                
            }
            catch (Exception ex)
            {
                this.cargaview.EmptyEntityToForm();
                this.cargaview.CloseForm();
                KananSAP.Helper.KananSAPHerramientas.LogError("GestionCargasPresenter.cs->PrepareUserForm()", ex.Message);
                SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }
        }

        private void ExtraerCargasKanan()
        {
            var lstSAP = new List<CargaCombustible>();
            var lstKF = new List<CargaCombustibleEnum>();
            DateTime? date = new DateTime();
            DateTime? temp = new DateTime();
            this.cargaview = new RecargaView(this.SBO_Application, ref this.Company, configs, this.VehiculoName, this.VehiculoID, null, iSucursalID);

            try
            {
                var rs =
                        this.cargaview.Entity.Consultar(new CargaCombustible
                        {
                            Movil = new Vehiculo { VehiculoID = cargaview.vehiculoid }
                        });
                if (rs.RecordCount > 0)
                {
                    lstSAP = cargaview.Entity.RecordSetToListCargaCombustible(rs);
                    date = lstSAP.Max(x => x.Fecha);// filtra por todos aquellos donde la fecha sea
                    if (date != null)
                    {
                        temp = DateTime.UtcNow;
                        temp.Value.AddDays(1);
                        lstKF = cargaws.GetByVehicleID((int) cargaview.vehiculoid);
                        InsertarCargasKF(lstSAP, lstKF);
                    }
                }
                else
                {
                    lstKF = cargaws.GetByVehicleID((int)cargaview.vehiculoid);
                    InsertarCargasKF(lstKF);
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("GestionCargasPresenter.cs->ExtraerCargas()", ex.Message);
                SBO_Application.StatusBar.SetText("Error al extraer cargas. " + ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }
        }

        private void InsertarCargasKF(IEnumerable<CargaCombustible> sap, IEnumerable<CargaCombustibleEnum> kf)
        {
            try
            {
                var lst = (from carga in kf where sap.All(x => x.CargaCombustibleID != carga.CargaCombustibleID) select (CargaCombustibleEnum)carga.Clone()).ToList();
                foreach (CargaCombustibleEnum carga in lst)
                {
                    this.cargaview.Entity.carga = OldCargaToNewCarga(carga); ;
                    this.cargaview.Entity.ItemCode = carga.CargaCombustibleID.ToString();
                    this.cargaview.Entity.CargaToSAP();
                    this.cargaview.Entity.Insert();
                    try
                    {
                        this.cargaview.GestionarCarga(OldCargaToNewCarga(carga));
                    }
                    catch(Exception ex) {
                        KananSAPHerramientas.LogError("this.cargaview.GestionarCarga(carga)", ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("GestionCargasPresenter.cs->InsertarCargasKF()", ex.Message);
                SBO_Application.StatusBar.SetText("Error al insertar cargas en ExtraerCargas(). " + ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }
        }

        private void InsertarCargasKF(IEnumerable<CargaCombustibleEnum> kf)
        {
            try
            {
                foreach (CargaCombustibleEnum carga in kf)
                {
                    //this.cargaview.Entity.carga = carga;
                    this.cargaview.Entity.carga = this.OldCargaToNewCarga(carga);
                    this.cargaview.Entity.ItemCode = carga.CargaCombustibleID.ToString();
                    this.cargaview.Entity.CargaToSAP();
                    this.cargaview.Entity.Insert();
                    try
                    {
                        this.cargaview.GestionarCarga(this.OldCargaToNewCarga(carga));
                    }
                    catch(Exception ex) {

                        KananSAPHerramientas.LogError("GestionCargasPresenter.cs->InsertarCargasKF()", ex.Message);
                        
                    }
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("GestionCargasPresenter.cs->InsertarCargasKF()", ex.Message);
                SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }
        }
        #endregion

        #endregion
    }
}
