﻿using System;
using System.Web.UI;
using Kanan.Vehiculos.BO2;
using KananSAP.GestionVehiculos.CatalogoVehiculo;
using KananSAP.GestionVehiculos.RecargaCombustible;
using KananWS.Interface;
using SAPbouiCOM;

namespace KananSAP.GestionVehiculos.RecargaCombustible
{
    public class RecargaPresenter
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private RecargaCombustibleWS.LogSAPCargaCombustible log;
        private Configurations configs;
        private RecargaCombustibleWS RecargaService;
        public int? vehiculoid;
        private string name;
        public RecargaView view;

        

        #endregion

        public RecargaPresenter(SAPbouiCOM.Application sApplication, SAPbobsCOM.Company Cmpny, int? _parent, string Name, Configurations c)
        {
            this.vehiculoid = _parent;
            this.name = Name;
            this.SBO_Application = sApplication;
            this.Company = Cmpny;
            this.configs = c;
            this.view = new RecargaView(sApplication,Cmpny,configs, this.name, this.vehiculoid);
            this.RecargaService = new RecargaCombustibleWS();
            this.log = new RecargaCombustibleWS.LogSAPCargaCombustible();
            //this.view.LoadForm();
            SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);

            //SBO_Application.MenuEvent += new SAPbouiCOM._IApplicationEvents_MenuEventEventHandler(SBO_Application_MenuEvent); 

            //view = new CatalogoVahiculosView(SBO_Application, "150", 1);
            //this.view.InsertFolder();
            //this.view.AddVehicleItemsToForm();
        }

        private void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (pVal.FormTypeEx == "CargaCom")
                {
                    #region Before Action

                    if (pVal.Before_Action)
                    {
                        //if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
                        //{
                        //    this.parent.FreezeForm(false);
                        //}

                        //if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_LOAD)
                        //{
                        //    this.parent.FreezeForm(true);
                        //}

                        if ( pVal.ItemUID == "btnguardar" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.view.ValidateData();
                            this.GuardarCargaCombustible();
                            this.view.CloseForm();
                            BubbleEvent = false;
                        }
                    }
                    #endregion
                    #region After Action
                    //else
                    //{
                    //    //if (pVal.ItemUID == "btnguardar" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                    //    //{
                            
                    //    //}
                    //}
                    #endregion
                }
            }
            catch (Exception ex)
            {
                if (!ex.Message.Contains("Invalid Form") )
                {
                    this.SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                }
            }
        }

        private void GuardarCargaCombustible()
        {
            try
            {
                var id_t = this.view.GetID();
                var id = Convert.ToInt32(id_t);
                this.view.FormToEntity();
                this.log = this.RecargaService.Add(this.view.Entity.carga, true,
                    this.configs.UserFull.Configs["TimeZone"],
                    this.configs.UserFull.Dependencia.EmpresaID, id);
                this.view.EmptyEntityToForm();
                if (log.Status)
                {
                    this.SBO_Application.StatusBar.SetText("Carga de combustible registrada correctamente", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                }
                else
                {
                    throw new Exception(log.Error);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
