using System;
using System.Linq;
using Kanan.Costos.BO2;
using Kanan.Comun.BO2;
using Kanan.Vehiculos.BO2;
using KananSAP.Vehiculos.Data;
using KananWS.Interface;
using SAPbouiCOM;
using Application = System.Windows.Forms.Application;
using KananSAP.Configuracion.Configuraciones.Objects;
using SAPbobsCOM;
using SAPinterface.Data.AD;
using SAPinterface.Data.INFO.Tablas;

namespace KananSAP.GestionVehiculos.RecargaCombustible
{
    public class RecargaView
    {
        //Comentario prueba 2017/09/18

        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.StaticText oStaticText;
        private SAPbouiCOM.ComboBox oComboBox;
        private SAPbouiCOM.Button oButton;
        private SAPbobsCOM.Company oCompany;
        public string name;
        public int? vehiculoid;
        public int? Ccid;
        public int? sucursalID;
        public CargaCombustibleSAP Entity { get; set; }
        public string FormUniqueID { get; set; }
        private XMLPerformance XmlApplication;
        public Configurations configs;
        #endregion

        #region Constructor
        public RecargaView(SAPbouiCOM.Application Application, ref SAPbobsCOM.Company company, Configurations c, string Name, int? id, int? ccid, int? iCodigoSucursal)
        {
            this.configs = c;
            this.name = Name;
            this.vehiculoid = id;
            this.Ccid = ccid;
            this.sucursalID = iCodigoSucursal;
            this.oCompany = company;
            this.Entity = new CargaCombustibleSAP(company);
            this.XmlApplication = new XMLPerformance(Application);
            this.SBO_Application = Application;
            this.oItem = null;
            this.oEditText = null;
            this.oStaticText = null;
            this.oComboBox = null;
            this.oForm = null;
            
        }
        #endregion

        #region Metodos

        #region FormMethods
        public void SetForm(string type, int count)
        {
            try
            {
                if (count > 1)
                {
                    this.oForm = SBO_Application.Forms.GetForm(type, count);
                    this.oForm.Close();
                    this.oForm = SBO_Application.Forms.GetForm(type, 1);
                    this.oForm.Select();
                    this.FormUniqueID = this.oForm.UniqueID;
                }
                else
                {
                    this.oForm = SBO_Application.Forms.GetForm(type, count);
                    this.FormUniqueID = this.oForm.UniqueID;
                }
            }
            catch (Exception ex)
            {
                this.oForm = null;
                //throw new Exception(ex.Message);
                SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }
        }

        public void LoadForm()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("CargaCom");
                this.EntityFields();
            }
            catch
            {
                string sPath = Application.StartupPath;
                this.XmlApplication.LoadFromXML(sPath + "\\XML","RecargaCombustible.xml");
                
                //this.AddDataSources();
                this.oForm = SBO_Application.Forms.Item("CargaCom");
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.EntityFields();
            }
        }

        void EntityFields()
        {
            try
            {
                this.name = GestionGlobales.CurrentVehiculoName;
                this.vehiculoid = GestionGlobales.CurrentVehiculoID;
                if (configs != null)
                    FillComboBoxes();

                oItem = oForm.Items.Item("lblvehi");
                oStaticText = (StaticText)(oItem.Specific);
                oStaticText.Caption = this.name;

                oItem = oForm.Items.Item("lblid");
                oStaticText = (StaticText)(oItem.Specific);
                oStaticText.Caption = this.vehiculoid.ToString();
                oItem.Visible = false;

                oItem = oForm.Items.Item("lblccid");
                oStaticText = (StaticText)(oItem.Specific);
                oStaticText.Caption = this.Ccid != null ? this.Ccid.ToString() : string.Empty;
                oItem.Visible = false;

                if (Ccid == null)
                {
                    HideDeleteButton();
                }
                //else
                //    ShowDeleteButton();

                oItem = this.oForm.Items.Item("txthr");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = "00";

                oItem = this.oForm.Items.Item("txtmin");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = "00";
            }
            catch { }
        }

        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
                this.EntityFields();
            }
            catch (Exception)
            {
                this.LoadForm();
                this.oForm.Visible = true;
                this.oForm.Select();
            }
        }

        public void CloseForm()
        {
            if (this.oForm.Visible & this.oForm.Selected)
                this.oForm.Close();
        }

        public void FillComboBoxes()
        {
            oItem = oForm.Items.Item("cmbtipogas");
            oComboBox = ((SAPbouiCOM.ComboBox)(oItem.Specific));
            oComboBox.DataBind.SetBound(true, "", "dstipocom");
            if (this.configs.TiposCombustible != null && this.configs.TiposCombustible.Count > 0)
            {
                foreach (var tipo in this.configs.TiposCombustible)
                {
                    oComboBox.ValidValues.Add(tipo.ID.ToString(), tipo.Nombre);
                }
                oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index);
            }

            oItem = oForm.Items.Item("cmbmoneda");
            oComboBox = ((SAPbouiCOM.ComboBox)(oItem.Specific));
            oComboBox.DataBind.SetBound(true, "", "dsmoneda");
            if (this.configs.TiposMoneda != null && this.configs.TiposMoneda.Count > 0)
            {
                foreach (var tipo in this.configs.TiposMoneda)
                {
                    oComboBox.ValidValues.Add(tipo.TipoMonedaID.ToString(), tipo.Descripcion);
                }
                oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index);
            }

            oItem = oForm.Items.Item("cmbprov");
            oComboBox = ((SAPbouiCOM.ComboBox)(oItem.Specific));
            oComboBox.DataBind.SetBound(true, "", "dsprov");
            if (this.configs.Proveedores != null && this.configs.Proveedores.Count > 0)
            {
                foreach (var tipo in this.configs.Proveedores)
                {
                    oComboBox.ValidValues.Add(tipo.ProveedorID.ToString(), tipo.Nombre);
                }
                oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index);
            }

            oItem = oForm.Items.Item("cmbuvol");
            oComboBox = ((SAPbouiCOM.ComboBox)(oItem.Specific));
            oComboBox.DataBind.SetBound(true, "", "dsuvol");
            if (this.configs.TiposVolumen != null && this.configs.TiposVolumen.Count > 0)
            {
                foreach (var tipo in this.configs.TiposVolumen)
                {
                    oComboBox.ValidValues.Add(tipo.UnidadVolumenID.ToString(), tipo.Nomenclatura);
                }
                oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index);
            }

            oItem = oForm.Items.Item("cmbmpago");
            oComboBox = ((SAPbouiCOM.ComboBox)(oItem.Specific));
            oComboBox.DataBind.SetBound(true, "", "dsmpago");
            if (this.configs.TiposPago != null && this.configs.TiposPago.Count > 0)
            {
                foreach (var tipo in this.configs.TiposPago)
                {
                    oComboBox.ValidValues.Add(tipo.TipoPagoID.ToString(), tipo.TipoPago);
                }
                oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index);
            }

            oItem = oForm.Items.Item("cmbudist");
            oComboBox = ((SAPbouiCOM.ComboBox)(oItem.Specific));
            oComboBox.DataBind.SetBound(true, "", "dsudist");
            if (this.configs.TiposLongitud != null && this.configs.TiposLongitud.Count > 0)
            {
                foreach (var tipo in this.configs.TiposLongitud)
                {
                    oComboBox.ValidValues.Add(tipo.UnidadLongitudID.ToString(), tipo.Nomenclatura);
                }
                oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index);
            }
        }

        public void HideDeleteButton()
        {
            oItem = oForm.Items.Item("btnelimina");
            oItem.Visible = false;

            oItem = oForm.Items.Item("btnphoto");
            oItem.Visible = false;
            Item hided = oItem;

            oItem = oForm.Items.Item("btnguardar");
            oItem.Left = hided.Left;

            oItem = oForm.Items.Item("btngeo");
            oItem.Visible = false;
            hided = oItem;

            oItem = oForm.Items.Item("2");
            oItem.Left = hided.Left;
        }

        public void HidePhotoButtons()
        {
            oItem = oForm.Items.Item("btnphoto");
            oItem.Visible = false;

            oItem = oForm.Items.Item("btngeo");
            oItem.Visible = false;
        }

        public void ShowDeleteButton()
        {
            oItem = oForm.Items.Item("btnelimina");
            oItem.Visible = true;

            oItem = oForm.Items.Item("btnphoto");
            oItem.Visible = true;
            Item hided = oItem;

            oItem = oForm.Items.Item("btnguardar");
            oItem.Left = hided.Left;

            oItem = oForm.Items.Item("btngeo");
            oItem.Visible = true;
            hided = oItem;

            oItem = oForm.Items.Item("2");
            oItem.Left = hided.Left;


            oItem = oForm.Items.Item("btnelimina");
            oItem.Visible = true;
            hided = oItem;

            oItem = oForm.Items.Item("2");
            oItem.Left = hided.Left + hided.Width + 3;
        }

        public void FormToEntity()
        {
            this.oForm = SBO_Application.Forms.Item("CargaCom");

            oItem = this.oForm.Items.Item("txtfecha");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.carga.Fecha = String.IsNullOrEmpty(oEditText.String.Trim()) ? (DateTime?)null : DateTime.Parse(oEditText.String);

            oItem = this.oForm.Items.Item("txthr");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (!string.IsNullOrEmpty(oEditText.String.Trim()) && !oEditText.String.Trim().Equals("00") && Entity.carga.Fecha != null)
            {
                try
                {
                    var hr = Convert.ToInt32(oEditText.String);
                    this.Entity.carga.Fecha = Entity.carga.Fecha.Value.AddHours(hr);
                    //if(hr <= 0 || hr > 12)
                    //    this.Entity.carga.Fecha = Entity.carga.Fecha.Value.AddHours(hr);
                    //else
                    //{
                    //    throw new Exception("Valor 'Horas' invalido en campo 'Fecha'");
                    //}

                }
                catch
                {
                    //throw new Exception("Valor 'Horas' invalido en campo 'Fecha'");
                    SBO_Application.StatusBar.SetText("Valor 'Hora' inválido en campo 'Fecha'", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                    return;
                }
            }

            oItem = this.oForm.Items.Item("txtmin");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (!string.IsNullOrEmpty(oEditText.String.Trim()) && !oEditText.String.Trim().Equals("00") && Entity.carga.Fecha != null)
            {
                try
                {
                    var min = Convert.ToInt32(oEditText.String);
                    this.Entity.carga.Fecha = Entity.carga.Fecha.Value.AddMinutes(min);

                    //if (min <= 0 || min > 12)
                    //    this.Entity.carga.Fecha = Entity.carga.Fecha.Value.AddMinutes(min);
                    //else
                    //{
                    //    throw new Exception("Valor 'Minutos' invalido en campo 'Fecha'");
                    //}
                }
                catch
                {
                    //throw new Exception("Valor 'Minutos' invalido en campo 'Fecha'");
                    SBO_Application.StatusBar.SetText("Valor 'Minutos' invalido en campo 'Fecha'", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                    return;
                }
            }

            oItem = this.oForm.Items.Item("cmbtipogas");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            this.Entity.carga.TypeFuel.TipoCombustibleID = oComboBox.Selected == null ? null : (int?)(Convert.ToInt32(oComboBox.Selected.Value));

            oItem = this.oForm.Items.Item("cmbprov");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            this.Entity.carga.SourceCenter = oComboBox.Selected == null ? null : configs.Proveedores.First(x => x.ProveedorID == Convert.ToInt32(oComboBox.Selected.Value));

            oItem = this.oForm.Items.Item("txtpos");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.carga.CentroServicio = oEditText.String;

            oItem = this.oForm.Items.Item("txtrecarga");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.carga.Litros = String.IsNullOrEmpty(oEditText.String) ? 0.0 : Convert.ToDouble(oEditText.String);

            oItem = this.oForm.Items.Item("cmbuvol");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            this.Entity.carga.Volumen = oComboBox.Selected == null ? null : configs.TiposVolumen.First(x => x.UnidadVolumenID == Convert.ToInt32(oComboBox.Selected.Value));

            oItem = this.oForm.Items.Item("cmbmpago");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            this.Entity.carga.TipoPago.TipoPagoID = oComboBox.Selected == null ? null : configs.TiposPago.First(x => x.TipoPagoID == Convert.ToInt32(oComboBox.Selected.Value)).TipoPagoID;

            oItem = this.oForm.Items.Item("txtdpago");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.carga.TipoPago.DetallePago = oEditText.String;

            oItem = this.oForm.Items.Item("txtcosto");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.carga.Pesos = string.IsNullOrEmpty(oEditText.String) && !this.IsNumeric(oEditText.String) ? 0 : Convert.ToDecimal(oEditText.String);

            oItem = this.oForm.Items.Item("cmbmoneda");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            this.Entity.carga.Moneda = oComboBox.Selected == null ? null : configs.TiposMoneda.First(x => x.TipoMonedaID == Convert.ToInt32(oComboBox.Selected.Value));


            oItem = this.oForm.Items.Item("txtticket");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.carga.Ticket = oEditText.String.Trim();

            oItem = this.oForm.Items.Item("txtodo");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.carga.Odometro = String.IsNullOrEmpty(oEditText.String.Trim()) ? (double?)null : (Convert.ToDouble(oEditText.String));

            oItem = this.oForm.Items.Item("cmbudist");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            this.Entity.carga.Longitud = oComboBox.Selected == null ? null : configs.TiposLongitud.First(x => x.UnidadLongitudID == Convert.ToInt32(oComboBox.Selected.Value));
            try
            {
                oItem = this.oForm.Items.Item("chkSAP");
                CheckBox chkRecarga = (CheckBox)oItem.Specific;
                if (chkRecarga.Checked)
                    this.Entity.carga.EsFacturableSAP = 1;
                else
                    this.Entity.carga.EsFacturableSAP = 0;
            }
            catch
            {
                this.Entity.carga.EsFacturableSAP = 1;
            }
        }

        public void EntityToForm()
        {
            this.oForm = SBO_Application.Forms.Item("CargaCom");

            oItem = this.oForm.Items.Item("txtfecha");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = this.Entity.carga.Fecha == null ? string.Empty : Entity.carga.Fecha.Value.ToShortDateString();

            oItem = this.oForm.Items.Item("txthr");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = this.Entity.carga.Fecha == null ? "00" : string.Format("{0:00}", Entity.carga.Fecha.Value.Hour);

            oItem = this.oForm.Items.Item("txtmin");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = this.Entity.carga.Fecha == null ? "00" : string.Format("{0:00}", Entity.carga.Fecha.Value.Minute);

            oItem = this.oForm.Items.Item("cmbtipogas");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select(((int)this.Entity.carga.TypeFuel.TipoCombustibleID).ToString());

            oItem = this.oForm.Items.Item("cmbprov");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select(this.Entity.carga.SourceCenter.ProveedorID.ToString());

            oItem = this.oForm.Items.Item("txtpos");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = this.Entity.carga.CentroServicio;

            oItem = this.oForm.Items.Item("txtrecarga");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = this.Entity.carga.Litros.ToString();

            oItem = this.oForm.Items.Item("cmbuvol");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select(this.Entity.carga.Volumen.UnidadVolumenID.ToString());

            oItem = this.oForm.Items.Item("cmbmpago");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            //oComboBox.Select(this.Entity.carga.TipoPago.TipoPagoID.ToString());
            if (this.Entity.carga.TipoPago.TipoPagoID != null)
            {
                oComboBox.Select(this.Entity.carga.TipoPago.TipoPagoID.ToString());
            }
            else
            {
                oComboBox.Select("4");
            }

            oItem = this.oForm.Items.Item("txtdpago");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = this.Entity.carga.DetallePago;

            oItem = this.oForm.Items.Item("txtcosto");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = this.Entity.carga.Pesos.ToString();

            oItem = this.oForm.Items.Item("txtticket");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = this.Entity.carga.Ticket;

            oItem = this.oForm.Items.Item("txtodo");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = this.Entity.carga.Odometro.ToString();

            oItem = this.oForm.Items.Item("cmbudist");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select(this.Entity.carga.Longitud.UnidadLongitudID.ToString());

            try
            {
                oItem = this.oForm.Items.Item("chkSAP");
                CheckBox chkRecarga = (CheckBox)oItem.Specific;
                chkRecarga.Checked = this.Entity.carga.EsFacturableSAP == 1;
                
            }
            catch
            {
                
            }

        }

        public void EmptyEntityToForm()
        {
            oItem = this.oForm.Items.Item("txtfecha");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = DateTime.Now.ToShortDateString();

            oItem = this.oForm.Items.Item("txthr");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = "00";

            oItem = this.oForm.Items.Item("txtmin");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = "00";

            oItem = this.oForm.Items.Item("cmbtipogas");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index);

            oItem = this.oForm.Items.Item("cmbprov");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index);

            oItem = this.oForm.Items.Item("txtpos");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtrecarga");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("cmbuvol");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index);

            oItem = this.oForm.Items.Item("cmbmpago");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index);

            oItem = this.oForm.Items.Item("txtdpago");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtcosto");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtticket");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtodo");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("cmbudist");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index);

        }

        public void ValidateData()
        {
            try
            {
                this.FormToEntity();                
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
                SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }

            if (this.Entity.carga.Litros == null || this.Entity.carga.Litros <= 0)
            {
                //throw new Exception("El valor 'Recarga' es obligatorio y debe ser mayor a cero");
                SBO_Application.StatusBar.SetText("El valor 'Recarga' es obligatorio y debe ser mayor a cero", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }

            if (this.Entity.carga.TipoPago.TipoPagoID == 1)
            {
                if (this.Entity.carga.TipoPago.DetallePago.Length < 4 || Entity.carga.TipoPago.DetallePago.Length > 5)
                {
                    //throw new Exception("El valor 'Detalle de Pago' es obligatorio y debe tener los 4 o 5 ultimos digitos del número de tarjeta");
                    SBO_Application.StatusBar.SetText("El valor 'Detalle de Pago' es obligatorio y debe tener los 4 o 5 ultimos digitos del número de tarjeta", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                    return;
                }
                if (!this.IsNumeric(this.Entity.carga.TipoPago.DetallePago))
                {
                    //throw new Exception("El valor 'Detalle de Pago' debe ser numerico");
                    SBO_Application.StatusBar.SetText("El valor 'Detalle de Pago' debe ser numerico", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                    return;
                }
            }
            if (this.Entity.carga.Pesos <= 0)
            {
                //throw new Exception("El valor 'Costo' es obligatorio y debe ser mayor a cero");
                SBO_Application.StatusBar.SetText("El valor 'Costo' es obligatorio y debe ser mayor a cero", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }
            if (this.Entity.carga.Odometro == null || this.Entity.carga.Odometro <= 0)
            {
                //throw new Exception("El valor 'Odometro' es obligatorio y debe ser mayor a cero");
                SBO_Application.StatusBar.SetText("El valor 'Odometro' es obligatorio y debe ser mayor a cero", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                return;
            }

            //this.GestionaFacturas();

        }

        /*public void GestionaFacturas()
        {

            //Validación de no haberse facturando antes.
            String sQuery = string.Format(@"SELECT ""Code"" FROM  ""@VSKF_PROVEEDOR"" WHERE U_ProveedorID = {0}", this.Entity.carga.SourceCenter.ProveedorID);
            this.GestionarCarga(this.Entity.carga);
        }*/


        public string GestionarCarga(CargaCombustible carga)
        {


            string sresponse = string.Empty, sconsultaSql = string.Empty;
            SBO_KF_Configuracion_INFO KNSttings = new SBO_KF_Configuracion_INFO();
            SBO_KF_Configuracion_AD oConfiguracion = new SBO_KF_Configuracion_AD();
            sresponse = oConfiguracion.recuperaConfiguracion(ref oCompany, out sconsultaSql, ref KNSttings);
            if (sresponse == "OK")
            {
                sresponse = string.Empty;

                bool bContinuar = false;
                string sDocEntryDocumento = string.Empty;
                int BranchID = -1, SucursalID = -1;
                Recordset oRecordQuery = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                if (!string.IsNullOrEmpty(carga.Ticket))
                {
                    if (KNSttings != null)
                    {
                        Recordset oCardCode = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                        string CardCode = "";
                        String sQuery = string.Format(@"SELECT ""Code"" FROM  ""@VSKF_PROVEEDOR"" WHERE U_ProveedorID = {0}", carga.SourceCenter.ProveedorID);

                        oCardCode.DoQuery(sQuery);
                        if (oCardCode.RecordCount > 0)
                        {
                            oCardCode.MoveFirst();
                            CardCode = Convert.ToString(oCardCode.Fields.Item("Code").Value);
                        }
                        string sFecha = Convert.ToDateTime(carga.Fecha.ToString()).ToString("dd/MM/yyyy");
                        string CentroCostoAlmacen = string.Empty;
                        if (KNSttings.cSucursal == 'Y')
                        //CentroCostoAlmacen = KNSttings.Sucursal;
                        {
                            //sucursalID
                            if (!string.IsNullOrEmpty(this.sucursalID.ToString()))
                            {
                                sQuery = string.Format(@"SELECT ""Name"", U_BranchID,U_CENTRO FROM ""@VSKF_SUCURSAL"" WHERE U_SucursalID = {0}", this.sucursalID.ToString());
                                oRecordQuery.DoQuery(sQuery);
                                if (oRecordQuery.RecordCount > 0)
                                {
                                    oRecordQuery.MoveFirst();
                                    CentroCostoAlmacen = Convert.ToString(oRecordQuery.Fields.Item("U_CENTRO").Value);
                                    try
                                    {
                                        BranchID = Convert.ToInt32(oRecordQuery.Fields.Item("U_BranchID").Value);
                                    }
                                    catch { }
                                }
                                else
                                    CentroCostoAlmacen = KNSttings.Sucursal;
                            }
                            else
                                throw new Exception("No se puede crear la recarga. El vehiculo no tiene asignado una sucursal y la configuración de centro de costo es por sucursal");
                        }
                        else
                        {
                            //? KNSttings.Sucursal : vehiculoid.ToString();
                            // CentroCostoAlmacen = ve
                            sQuery = string.Format(@"SELECT ""Name"",U_SUCURSALID FROM ""@VSKF_VEHICULO"" WHERE U_VehiculoID = {0}", vehiculoid);
                            oRecordQuery.DoQuery(sQuery);
                            if (oRecordQuery.RecordCount > 0)
                            {
                                oRecordQuery.MoveFirst();
                                CentroCostoAlmacen = Convert.ToString(oRecordQuery.Fields.Item("Name").Value);
                                try
                                {
                                    SucursalID = Convert.ToInt32(oRecordQuery.Fields.Item("U_SUCURSALID").Value);
                                }
                                catch { }
                            }
                            else CentroCostoAlmacen = KNSttings.Vehiculo;
                            #region se valida si la empresa utiliza branchs para recuperar el branch de la sucursal asociada al vehiculo
                            if (SAPinterface.Data.tools.tools.UtilizaBranchs(ref this.oCompany).Equals("Y", StringComparison.InvariantCultureIgnoreCase))
                            {
                                sQuery = string.Format(@"SELECT ""Name"",U_BranchID FROM ""@VSKF_SUCURSAL"" WHERE U_SucursalID = {0} ", SucursalID);
                                oRecordQuery.DoQuery(sQuery);
                                if (oRecordQuery.RecordCount > 0)
                                {
                                    try
                                    {
                                        BranchID = Convert.ToInt32(oRecordQuery.Fields.Item("U_BranchID").Value);
                                    }
                                    catch { }
                                }
                            }
                            #endregion se valida si la empresa utiliza branchs para recuperar el branch de la sucursal asociada al vehiculo
                        }

                        #region validación para recuperar centro de costo en cliente
                        if (KNSttings.cCliente == 'Y')
                        {
                            try
                            {
                                string sql = string.Format(GestionGlobales.bSqlConnection ? @"SELECT U_KF_CostCentro FROM OCRD WHERE CardCode = '{0}'" : @"SELECT U_KF_CostCentro FROM ""OCRD"" WHERE ""CardCode"" = '{0}' ", CardCode);
                                oRecordQuery.DoQuery(sql);
                                if (oRecordQuery.RecordCount == 0)
                                {
                                    oRecordQuery.MoveFirst();
                                    string sCostCenter = Convert.ToString(oRecordQuery.Fields.Item("U_KF_CostCentro").Value);
                                    if (!string.IsNullOrEmpty(sCostCenter))
                                        CentroCostoAlmacen = sCostCenter;
                                }
                            }
                            catch { }
                        }
                        #endregion validación para recuperar centro de costo en cliente

                        double dLitros = Convert.ToDouble(carga.Litros);
                        string Combustible = ((carga.TypeFuel).ToString()).Replace("_", " ").ToString();
                        string ArticuloCombustible = DevolverArticuloInventario(Convert.ToInt32(carga.TypeFuel.TipoCombustibleID).ToString());

                        if (KNSttings.cAlmacen == 'N' && KNSttings.cSBO == 'Y')
                        {
                            bContinuar = this.ValidarFacturacion(carga.Ticket, out sDocEntryDocumento);
                            if (!bContinuar)
                            {
                                if (!string.IsNullOrEmpty(CardCode))
                                    sresponse = this.FacturaProveedor(CardCode, sFecha, carga.Ticket, CentroCostoAlmacen, carga.Pesos, dLitros, ArticuloCombustible, KNSttings.DimCode, BranchID);
                                else sresponse = "No se ha encontrado el registro del proveedor";
                            }
                            else sresponse = "OK";
                        }
                        if (KNSttings.cAlmacen == 'Y' && KNSttings.cSBO == 'Y')
                        {
                            bContinuar = this.ValidaSalidaMercancia(carga.Ticket, out sDocEntryDocumento);
                            if (!bContinuar)
                            {
                                sresponse = this.SalidaMercancia(sFecha, carga.Ticket, CentroCostoAlmacen, ArticuloCombustible, dLitros, KNSttings.Almacen, KNSttings.DimCode, BranchID);
                            }
                            else sresponse = "OK";
                        }

                        if (KNSttings.cSBO == 'N' && KNSttings.cKANAN == 'Y')
                        {
                            bContinuar = this.ValidarFacturacion(carga.Ticket, out sDocEntryDocumento);
                            if (!bContinuar)
                            {
                                if (!string.IsNullOrEmpty(CardCode))
                                    sresponse = this.Servicio(KNSttings.Cuenta, Combustible, CardCode, carga.Ticket, sFecha, carga.Pesos, CentroCostoAlmacen, Combustible, BranchID);
                                else sresponse = "No se encuentran los datos del proveedor";
                            }
                            else sresponse = "OK";
                        }
                    }
                    else sresponse = "No se encuentra la configuración para gestionar las facturas";
                }
                else sresponse = "No se ha determinado el folio de ticket para la recarga";
            }
            else sresponse = "No se ha encontrado la configuracion";
            return sresponse;
        }

        #region FACTURAS_INVENTARIO_SERVICIO_PROVEEDOR

        private bool ValidarFacturacion(string sFolioTicket, out string sDocEntry)
        {
            sDocEntry = string.Empty;
            bool bContinuar = false;
            string sql = string.Format(@"SELECT ""DocEntry"" FROM OPCH WHERE ""NumAtCard"" = '{0}' ", sFolioTicket);
            Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            rs.DoQuery(sql);
            if (rs.RecordCount > 0)
            {
                bContinuar = true;
                try
                {
                    sDocEntry = Convert.ToString(rs.Fields.Item("DocEntry").Value);
                }
                catch { }
            }
            else bContinuar = false;
            return bContinuar;
        }

        private bool ValidaSalidaMercancia(string sFolioTicket, out string sDocEntry)
        {
            sDocEntry = string.Empty;
            bool bContinuar = false;
            string sql = string.Format(@"SELECT ""DocEntry"" FROM OIGE WHERE ""Ref2"" = '{0}' ", sFolioTicket);
            Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            rs.DoQuery(sql);
            if (rs.RecordCount > 0)
            {
                bContinuar = true;
                try
                {
                    sDocEntry = Convert.ToString(rs.Fields.Item("DocEntry").Value);
                }
                catch { }
            }
            else bContinuar = false;
            return bContinuar;
        }


        private string FacturaProveedor(string CardCode, string sFecha, string sTicket, string sCentroCosto, Decimal dCosto, Double dLitros, string sTipoCombustible, int icCosto, int BranchID)///, Double dImpuesto)
        {
            string sresponse = string.Empty;
            Documents oInvoices = (SAPbobsCOM.Documents)this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseInvoices);
            oInvoices.DocType = BoDocumentTypes.dDocument_Items;
            oInvoices.CardCode = CardCode;
            if (SAPinterface.Data.tools.tools.UtilizaBranchs(ref this.oCompany).Equals("Y", StringComparison.InvariantCultureIgnoreCase))
                oInvoices.BPL_IDAssignedToInvoice = BranchID;
            oInvoices.Comments = string.Format("Recarga de combustibele emitida la fecha {0}", DateTime.Now.ToString("dd/MM/yyyy"));
            oInvoices.DocDueDate = Convert.ToDateTime(sFecha);
            oInvoices.TaxDate = Convert.ToDateTime(sFecha);
            oInvoices.DocDate = Convert.ToDateTime(sFecha);
            oInvoices.NumAtCard = sTicket;
            oInvoices.UserFields.Fields.Item("U_KF_VEHICULOFK").Value = vehiculoid.ToString();

            //detalle
            oInvoices.Lines.ItemCode = sTipoCombustible;

            switch (icCosto)
            {
                case 0:
                case 1:
                    oInvoices.Lines.CostingCode = sCentroCosto;
                    break;
                case 2:
                    oInvoices.Lines.CostingCode2 = sCentroCosto;
                    break;

                case 3:
                    oInvoices.Lines.CostingCode3 = sCentroCosto;
                    break;

                case 4:
                    oInvoices.Lines.CostingCode4 = sCentroCosto;
                    break;
                case 5:
                    oInvoices.Lines.CostingCode5 = sCentroCosto;
                    break;

            }
            //oInvoices.Lines.WarehouseCode = sAlmace;
            oInvoices.Lines.Quantity = dLitros;            
            oInvoices.Lines.LineTotal = Convert.ToDouble(dCosto) ;
            //fin detalle
            oInvoices.Lines.Add();


            try
            {
                int iSave = oInvoices.Add();
                if (iSave != 0)
                {
                    this.oCompany.GetLastError(out iSave, out sresponse);
                    sresponse = string.Format("Factura de proveedor no creada. ERROR {0}", sresponse);
                }
                else sresponse = "OK";
            }
            catch (Exception ex)
            {
                sresponse = ex.Message;
                //throw new Exception("Factura de proveedor no creada, " + ex.Message.ToString());
            }
            return sresponse;

        }
        private string Servicio(string sAcctNum, string sTipoCombustible, string CardCode, string sTicket, string sFecha, decimal dCostoSIVA, string CentroCosto, string sCombustible, int BranchID)
        {
            string sresponse = string.Empty;
            Documents oInvoices = (SAPbobsCOM.Documents)this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseInvoices);
            oInvoices.DocType = BoDocumentTypes.dDocument_Service;
            oInvoices.CardCode = CardCode;
            oInvoices.Comments = string.Format("Recarga de combustibele emitida la fecha {0}", DateTime.Now.ToString("dd/MM/yyyy"));
            oInvoices.DocDueDate = Convert.ToDateTime(sFecha);
            oInvoices.TaxDate = Convert.ToDateTime(sFecha);
            oInvoices.DocDate = Convert.ToDateTime(sFecha);
            oInvoices.NumAtCard = sTicket;
            if (SAPinterface.Data.tools.tools.UtilizaBranchs(ref this.oCompany).Equals("Y", StringComparison.InvariantCultureIgnoreCase))
                oInvoices.BPL_IDAssignedToInvoice = BranchID;
            oInvoices.UserFields.Fields.Item("U_KF_VEHICULOFK").Value = vehiculoid.ToString();

            ///detalle lineas
            oInvoices.Lines.AccountCode = sAcctNum;
            oInvoices.Lines.ItemDescription = string.Format("Recarga de combustible del tipo {0}", sTipoCombustible);
            oInvoices.Lines.Quantity = 1;
            //oInvoices.Lines.LineTotal = Convert.ToDouble( dCostoSIVA);
            oInvoices.Lines.LineTotal = Convert.ToDouble(dCostoSIVA);

            //fin detalle
            oInvoices.Lines.Add();
            try
            {
                int iSave = oInvoices.Add();
                if (iSave != 0)
                {
                    this.oCompany.GetLastError(out iSave, out sresponse);
                    sresponse = string.Format("Factura de servicio no creada. ERROR {0}", sresponse);
                }
                else sresponse = "OK";
            }
            catch (Exception ex)
            {
                sresponse = ex.Message;
            }
            return sresponse;
        }

        private string SalidaMercancia(string sFecha, string sTicket, string CentroCosto, string sCombustible,
            Double dLitros, string Almacen, int icCosto, int BranchID)
        {

            string sresponse = string.Empty;
            Documents GIssue = (SAPbobsCOM.Documents)this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenExit);
            GIssue.DocDate = Convert.ToDateTime(sFecha);
            GIssue.TaxDate = Convert.ToDateTime(sFecha);
            GIssue.Reference2 = sTicket;
            if (SAPinterface.Data.tools.tools.UtilizaBranchs(ref this.oCompany).Equals("Y", StringComparison.InvariantCultureIgnoreCase))
                GIssue.BPL_IDAssignedToInvoice = BranchID;
            GIssue.UserFields.Fields.Item("U_KF_VEHICULOFK").Value = vehiculoid.ToString();
            GIssue.Comments = string.Format(@"Salida de mercancia emitida la fecha {0} Vehiculo encargado de realizarla ""{1}""", DateTime.Now.ToString("dd/MM/yyyy"), this.vehiculoid);
            GIssue.Lines.ItemCode = sCombustible;
            GIssue.Lines.Quantity = dLitros;
            GIssue.Lines.WarehouseCode = Almacen;
            switch (icCosto)
            {
                case 0:
                case 1:
                    GIssue.Lines.CostingCode = CentroCosto;
                    break;
                case 2:
                    GIssue.Lines.CostingCode2 = CentroCosto;
                    break;

                case 3:
                    GIssue.Lines.CostingCode3 = CentroCosto;
                    break;

                case 4:
                    GIssue.Lines.CostingCode4 = CentroCosto;
                    break;
                case 5:
                    GIssue.Lines.CostingCode5 = CentroCosto;
                    break;

            }
            GIssue.Lines.Add();

            try
            {
                int iSave = GIssue.Add();
                if (iSave != 0)
                {
                    this.oCompany.GetLastError(out iSave, out sresponse);
                    sresponse = string.Format("Salida de mercancía no creada. ERROR {0}", sresponse);
                }
                else sresponse = "OK";
            }
            catch (Exception ex)
            {
                sresponse = ex.Message;
            }
            return sresponse;
        }
        #endregion

        private String DevolverArticuloInventario(string Concepto)
        {
            String response = "";            
            try
            {
                Recordset oRecords = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                oRecords.DoQuery(GestionGlobales.ConceptoFuel(Concepto));
                if (oRecords.RecordCount > 0)
                {
                    oRecords.MoveFirst();
                    response = Convert.ToString(oRecords.Fields.Item("Name").Value);
                }
                else
                    throw new Exception("");

            }
            catch (Exception ex)
            {
                throw new Exception("No se ha podido consultar el concepto de combustible " + ex.Message.ToString());
            }

            return response;
        }
        
        #endregion

        #region ItemsMethods

        //public void FillComboBoxesAlt()
        //{
        //    #region Tipo Combustible
        //    this.oItem = this.oForm.Items.Item("cmbtipogas");
        //    this.oComboBox = (SAPbouiCOM.ComboBox)(this.oItem.Specific);
        //    if (oComboBox != null && oComboBox.ValidValues != null && oComboBox.ValidValues.Count > 0)
        //    {
        //        foreach (var value in oComboBox.ValidValues)
        //        {
        //            oComboBox.ValidValues.Remove(value);
        //        }
        //    }
        //    oComboBox.Item.DisplayDesc = true;
        //    oComboBox.ValidValues.Add("1", "Magna");
        //    oComboBox.ValidValues.Add("2", "Premium");
        //    oComboBox.ValidValues.Add("3", "Diesel");
        //    #endregion

        //    #region Proveedor
        //    this.oItem = this.oForm.Items.Item("cmbprov");
        //    this.oComboBox = (SAPbouiCOM.ComboBox)(this.oItem.Specific);
        //    if (oComboBox != null && oComboBox.ValidValues != null && oComboBox.ValidValues.Count > 0)
        //    {
        //        foreach (var value in oComboBox.ValidValues)
        //        {
        //            oComboBox.ValidValues.Remove(value);
        //        }
        //    }
        //    oComboBox.Item.DisplayDesc = true;
        //    oComboBox.ValidValues.Add("70", "CONTROL INTEGRAL DE COMBUSTIBLES");
        //    oComboBox.ValidValues.Add("81", "CLUTCH Y FRENOS LOPEZ");
        //    oComboBox.ValidValues.Add("83", "SERVICIO DIESEL DE MERIDA SA DE CV");
        //    #endregion

        //    #region Volumen
        //    this.oItem = this.oForm.Items.Item("cmbuvol");
        //    this.oComboBox = (SAPbouiCOM.ComboBox)(this.oItem.Specific);
        //    if (oComboBox != null && oComboBox.ValidValues != null && oComboBox.ValidValues.Count > 0)
        //    {
        //        foreach (var value in oComboBox.ValidValues)
        //        {
        //            oComboBox.ValidValues.Remove(value);
        //        }
        //    }
        //    oComboBox.Item.DisplayDesc = true;
        //    oComboBox.ValidValues.Add("1", "Lts");
        //    oComboBox.ValidValues.Add("2", "Gal");
        //    #endregion

        //    #region tipo pago
        //    this.oItem = this.oForm.Items.Item("cmbmpago");
        //    this.oComboBox = (SAPbouiCOM.ComboBox)(this.oItem.Specific);
        //    if (oComboBox != null && oComboBox.ValidValues != null && oComboBox.ValidValues.Count > 0)
        //    {
        //        foreach (var value in oComboBox.ValidValues)
        //        {
        //            oComboBox.ValidValues.Remove(value);
        //        }
        //    }
        //    oComboBox.Item.DisplayDesc = true;
        //    oComboBox.ValidValues.Add("1", "Tarjeta (Crédito/Debito)");
        //    oComboBox.ValidValues.Add("2", "Vale (Electrónico)");
        //    oComboBox.ValidValues.Add("3", "Vale (Papel)");
        //    oComboBox.ValidValues.Add("4", "Efectivo");
        //    oComboBox.ValidValues.Add("5", "Otro (Especifique)");
        //    oComboBox.ValidValues.Add("6", "Cheque");

        //    #endregion

        //    #region tipo pago
        //    this.oItem = this.oForm.Items.Item("cmbudist");
        //    this.oComboBox = (SAPbouiCOM.ComboBox)(this.oItem.Specific);
        //    if (oComboBox != null && oComboBox.ValidValues != null && oComboBox.ValidValues.Count > 0)
        //    {
        //        foreach (var value in oComboBox.ValidValues)
        //        {
        //            oComboBox.ValidValues.Remove(value);
        //        }
        //    }
        //    oComboBox.Item.DisplayDesc = true;
        //    oComboBox.ValidValues.Add("1", "Kms");
        //    oComboBox.ValidValues.Add("2", "Mi");
        //    oComboBox.ValidValues.Add("3", "Yd");
        //    #endregion
        //}


        #endregion

        #region Metodos Auxiliares

        private bool IsNumeric(string value)
        {
            try
            {
                Convert.ToInt32(value);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string GetID()
        {
            oItem = oForm.Items.Item("lblid");
            oStaticText = (StaticText)(oItem.Specific);
            return oStaticText.Caption;
        }

        public string GetCargaID()
        {
            oItem = oForm.Items.Item("lblccid");
            oStaticText = (StaticText)(oItem.Specific);
            return oStaticText.Caption;
        }
        #endregion

        #endregion
    }
}
