﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#region Referencias
using SAPbobsCOM;
using SAPbouiCOM;
using Kanan.Vehiculos.BO2;
using KananSAP.Vehiculos.Data;
using KananSAP.Helper;
using KananSAP.GestionDatos.MigradorDatos.Librerias;
using System.IO;
using System.Threading;
using System.Diagnostics;
#endregion Referencias

namespace KananSAP.GestionVehiculos.GestoriaVehiculo.View
{
    public class GestoriaVehiculoView
    {
        #region Atributos
        private SAPbouiCOM.EditTextColumn oEditTextColumn;
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.DBDataSource oDBDataSource;
        private SAPbouiCOM.StaticText oStaticText;
        private SAPbouiCOM.Conditions oConditions;
        private SAPbouiCOM.Condition oCondition;
        private SAPbouiCOM.ComboBox oComboBox;
        private SAPbouiCOM.EditText oEditText;
        private SAPbobsCOM.Company Company;
        private SAPbouiCOM.Folder oFolder;
        private SAPbouiCOM.Matrix oMtx;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private Vehiculo vehiculo;
        private String sPathGestoria { get; set; }
        KananSAP.XMLPerformance XmlApplication;
        public GestoriaVehiculoSAP Entity { get; set; }
        private KananSAPHerramientas oHerramientas;

        public bool Foco;
        public int? RegistroID;
        #endregion Atributos

        #region Constructor
        public GestoriaVehiculoView(SAPbouiCOM.Application application, SAPbobsCOM.Company company, Vehiculo v)
        {
            this.SBO_Application = application;
            this.Company = company;
            Add oConnfigAdd = new Add(ref company, null);
            try
            {
                sPathGestoria = string.Format(@"{0}KFGestoria", oConnfigAdd.pathOperadores());
                if (!Directory.Exists(sPathGestoria))
                    Directory.CreateDirectory(sPathGestoria);
            }
            catch { }
            this.oHerramientas = new KananSAPHerramientas(this.SBO_Application);
            this.XmlApplication = new XMLPerformance(SBO_Application);
            this.Entity = new GestoriaVehiculoSAP(this.Company);
            this.vehiculo = v;

            this.oConditions = null;
            this.oCondition = null;

            this.RegistroID = null;

            this.oEditTextColumn = null;
            this.oStaticText = null;
            this.oEditText = null;
            this.oComboBox = null;
            this.oFolder = null;
            this.oItem = null;
            this.oMtx = null;
        }
        #endregion Constructor

        #region Métodos
        public void LoadForm()
        {
            try
            {
                try
                {
                    this.oForm = SBO_Application.Forms.Item("Gestoria");
                }
                catch
                {
                    string sPath = System.Windows.Forms.Application.StartupPath;
                    this.XmlApplication.LoadFromXML(sPath + "\\XML", "ModuloGestoria.xml");

                }

                this.oForm = SBO_Application.Forms.Item("Gestoria");

                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;

                oItem = oForm.Items.Item("bajMot");
                oComboBox = (ComboBox)(oItem.Specific);
                KananSAP.Helper.KananSAPHelpers.CleanComboBox(oComboBox);
                oComboBox.ValidValues.Add("0", "Ninguna");
                oComboBox.ValidValues.Add("1", "Administrativa");
                oComboBox.ValidValues.Add("2", "Observaciones");
                oComboBox.ValidValues.Add("3", "Siniestro");
                oComboBox.ValidValues.Add("4", "Venta");
                oComboBox.ValidValues.Add("5", "Otro");
                oComboBox.Item.DisplayDesc = true;

                this.Foco = true;

                oItem = oForm.Items.Item("vh");
                oStaticText = (StaticText)(oItem.Specific);
                oStaticText.Caption = this.vehiculo.Nombre;

                oItem = oForm.Items.Item("mtxPlacas");
                oMtx = (Matrix)(oItem.Specific);
                oMtx.Item.Enabled = true;

                oItem = oForm.Items.Item("mtxProp");
                oMtx = (Matrix)(oItem.Specific);
                oMtx.Item.Enabled = true;

                oItem = oForm.Items.Item("mtxTarjeta");
                oMtx = (Matrix)(oItem.Specific);
                oMtx.Item.Enabled = true;

                oItem = oForm.Items.Item("mtxAlta");
                oMtx = (Matrix)(oItem.Specific);
                oMtx.Item.Enabled = true;

                oItem = oForm.Items.Item("mtxBaja");
                oMtx = (Matrix)(oItem.Specific);
                oMtx.Item.Enabled = true;

                //oItem = oForm.Items.Item("FInicio");
                //oEditText = (EditText)(oItem.Specific);
            }
            catch (Exception e)
            {
                SBO_Application.StatusBar.SetText("Error al cargar el formulario. " + e.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                return;
            }
        }

        public void ShowForm()
        {
            try
            {
                //this.LoadForm(); /*Comentado debido a que se llaman a las funciones independientemente. */
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 3;
                this.oForm.Visible = true;
                BindDataToForm();
                this.oForm.Select();
            }
            catch (Exception e)
            {
                SBO_Application.StatusBar.SetText("Error al mostar el formulario. " + e.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                return;
            }
        }

        public void CloseForm()
        {
            if (this.oForm.Visible & this.oForm.Selected & this.Foco == true)
            {
                this.Foco = false;
                this.oForm.Close();
                return;
            }
        }

        private void CleanComboBox(ComboBox combo)
        {
            if (combo == null || combo.ValidValues == null || combo.ValidValues.Count <= 0)
                return;
            var f = combo.ValidValues.Count;
            combo.Value.Remove(0, 5);
            for (int i = 0; i < f; i++)
            {
                combo.ValidValues.Remove(i, BoSearchKey.psk_Index);
            }
        }

        public bool IsOtroCombo()
        {
            bool run = false;

            oItem = oForm.Items.Item("bajMot");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            if (oComboBox.Selected.Value == "5")
                run = true;

            return run;
        }

        public void EnableItem(String item)
        {
            if (String.IsNullOrEmpty(item))
                return;

            oItem = oForm.Items.Item(item);
            oItem.Enabled = true;
        }

        public String ActiveFolder()
        {
            String Folder = String.Empty;
            try
            {
                if (oForm != null & this.Foco == true)
                {
                    this.oFolder = this.oForm.Items.Item("TabPlaca").Specific;
                    if (oFolder.Selected)
                    {
                        Folder = "TabPlaca";
                    }

                    this.oFolder = this.oForm.Items.Item("TabProp").Specific;
                    if (oFolder.Selected)
                    {
                        Folder = "TabProp";
                    }

                    this.oFolder = this.oForm.Items.Item("TabTarje").Specific;
                    if (oFolder.Selected)
                    {
                        Folder = "TabTarje";
                    }

                    this.oFolder = this.oForm.Items.Item("Alta").Specific;
                    if (oFolder.Selected)
                    {
                        Folder = "Alta";
                    }

                    this.oFolder = this.oForm.Items.Item("Baja").Specific;
                    if (oFolder.Selected)
                    {
                        Folder = "Baja";
                    }
                }
                return Folder;
            }
            catch (Exception ex)
            {
                oHerramientas.AplicacionSBO.StatusBar.SetText(". " + ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                KananSAPHerramientas.LogError("GestoriaVehiculoView.cs->ActiveFolder()", ex.Message);
                return null;
            }
        }

        public void SetFocus()
        {
            try
            {
                this.oForm.Items.Item("TabPlaca").Click();
            }
            catch (Exception e)
            {
                throw new Exception("SetFocus() -> " + e.Message);
            }
        }

        public void BindDataToForm()
        {
            try
            {
                SetConditionsPlaca(vehiculo.VehiculoID.ToString());
                oDBDataSource = oForm.DataSources.DBDataSources.Item("@VSKF_GESPLA");
                oDBDataSource.Query(oConditions);
                oItem = oForm.Items.Item("mtxPlacas");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oMtx.LoadFromDataSource();

                SetConditionsPropietario(vehiculo.VehiculoID.ToString());
                oDBDataSource = oForm.DataSources.DBDataSources.Item("@VSKF_GESPRO");
                oDBDataSource.Query(oConditions);
                oItem = oForm.Items.Item("mtxProp");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oMtx.LoadFromDataSource();

                SetConditionsTarjeta(vehiculo.VehiculoID.ToString());
                oDBDataSource = oForm.DataSources.DBDataSources.Item("@VSKF_GESTAR");
                oDBDataSource.Query(oConditions);
                oItem = oForm.Items.Item("mtxTarjeta");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oMtx.LoadFromDataSource();

                SetConditionsAlta(vehiculo.VehiculoID.ToString());
                oDBDataSource = oForm.DataSources.DBDataSources.Item("@VSKF_GESALTA");
                oDBDataSource.Query(oConditions);
                oItem = oForm.Items.Item("mtxAlta");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oMtx.LoadFromDataSource();

                SetConditionsBaja(vehiculo.VehiculoID.ToString());
                oDBDataSource = oForm.DataSources.DBDataSources.Item("@VSKF_GESBAJA");
                oDBDataSource.Query(oConditions);
                oItem = oForm.Items.Item("mtxBaja");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oMtx.LoadFromDataSource();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al llenar la matriz " + ex);
            }
        }

        public void SetID(int index, String Matrix)
        {
            try
            {
                oItem = oForm.Items.Item(Matrix);
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = oMtx.GetCellSpecific(1, index);
                try
                {
                    this.RegistroID = Convert.ToInt32(oEditText.String.Trim());
                }
                catch
                {
                    this.SBO_Application.StatusBar.SetText("Seleccione una registro. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                    return;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region Condiciones
        public void SetConditionsPlaca(String vh)
        {
            try
            {
                #region Code
                oConditions = new SAPbouiCOM.Conditions();

                oCondition = oConditions.Add();
                oCondition.Alias = "U_VehiculoID";
                oCondition.Operation = BoConditionOperation.co_EQUAL;
                oCondition.CondVal = vh;
                #endregion Code
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SetConditionsPropietario(String vh)
        {
            try
            {
                #region Code
                oConditions = new SAPbouiCOM.Conditions();

                oCondition = oConditions.Add();
                oCondition.Alias = "U_VehiculoID";
                oCondition.Operation = BoConditionOperation.co_EQUAL;
                oCondition.CondVal = vh;
                #endregion Code
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SetConditionsTarjeta(String vh)
        {
            try
            {
                #region Code
                oConditions = new SAPbouiCOM.Conditions();

                oCondition = oConditions.Add();
                oCondition.Alias = "U_VehiculoID";
                oCondition.Operation = BoConditionOperation.co_EQUAL;
                oCondition.CondVal = vh;
                #endregion Code
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SetConditionsAlta(String vh)
        {
            try
            {
                #region Code
                oConditions = new SAPbouiCOM.Conditions();

                oCondition = oConditions.Add();
                oCondition.Alias = "U_VehiculoID";
                oCondition.Operation = BoConditionOperation.co_EQUAL;
                oCondition.CondVal = vh;
                #endregion Code
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SetConditionsBaja(String vh)
        {
            try
            {
                #region Code
                oConditions = new SAPbouiCOM.Conditions();

                oCondition = oConditions.Add();
                oCondition.Alias = "U_VehiculoID";
                oCondition.Operation = BoConditionOperation.co_EQUAL;
                oCondition.CondVal = vh;
                #endregion Code
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion Condiciones

        #endregion Métodos

        #region MetodosForm

        public void FormToEntity()
        {
            if (!ValidarTipoGestion())
                return;
            switch (Entity.gestoria.TipoGestion)
            {
                case 1:
                    DefaultVehiculoToGestoria();
                    oItem = oForm.Items.Item("plaFecha");
                    oEditText = (EditText)(oItem.Specific);
                    Entity.gestoria.Fecha = Convert.ToDateTime(oEditText.String);
                    Entity.gestoria.FechaRegistro = DateTime.Now;

                    oItem = oForm.Items.Item("plaNum");
                    oEditText = (EditText)(oItem.Specific);
                    Entity.gestoria.Numero = oEditText.String;

                    oItem = oForm.Items.Item("plaObs");
                    oEditText = (EditText)(oItem.Specific);
                    Entity.gestoria.Observacion = oEditText.String;
                    this.CargaMueveEvidencia("txtdplaca");
                    break;
                case 2:
                    DefaultVehiculoToGestoria();
                    oItem = oForm.Items.Item("proFecha");
                    oEditText = (EditText)(oItem.Specific);
                    Entity.gestoria.Fecha = Convert.ToDateTime(oEditText.String);
                    Entity.gestoria.FechaRegistro = DateTime.Now;

                    oItem = oForm.Items.Item("proObs");
                    oEditText = (EditText)(oItem.Specific);
                    Entity.gestoria.Observacion = oEditText.String;
                    this.CargaMueveEvidencia("txtpropd");
                    break;
                case 3:
                    DefaultVehiculoToGestoria();
                    oItem = oForm.Items.Item("tarFecha");
                    oEditText = (EditText)(oItem.Specific);
                    Entity.gestoria.Fecha = Convert.ToDateTime(oEditText.String);
                    Entity.gestoria.FechaRegistro = DateTime.Now;

                    oItem = oForm.Items.Item("tarNum");
                    oEditText = (EditText)(oItem.Specific);
                    Entity.gestoria.Numero = oEditText.String;

                    oItem = oForm.Items.Item("tarObs");
                    oEditText = (EditText)(oItem.Specific);
                    Entity.gestoria.Observacion = oEditText.String;

                    this.CargaMueveEvidencia("txttarjd");
                    break;
                case 4:
                    DefaultVehiculoToGestoria();
                    oItem = oForm.Items.Item("altFecha");
                    oEditText = (EditText)(oItem.Specific);
                    Entity.gestoria.Fecha = Convert.ToDateTime(oEditText.String);
                    Entity.gestoria.FechaRegistro = DateTime.Now;

                    oItem = oForm.Items.Item("altObs");
                    oEditText = (EditText)(oItem.Specific);
                    Entity.gestoria.Observacion = oEditText.String;

                    this.CargaMueveEvidencia("txtupd");
                    break;
                case 5:
                    DefaultVehiculoToGestoria();
                    oItem = oForm.Items.Item("bajFecha");
                    oEditText = (EditText)(oItem.Specific);
                    Entity.gestoria.Fecha = Convert.ToDateTime(oEditText.String);
                    Entity.gestoria.FechaRegistro = DateTime.Now;

                    oItem = oForm.Items.Item("bajMot");
                    oComboBox = (ComboBox)(oItem.Specific);
                    Entity.gestoria.MotivoBajaID = Convert.ToInt32(oComboBox.Selected.Value);
                    Entity.gestoria.MotivoBaja = oComboBox.Selected.Description;

                    oItem = oForm.Items.Item("bajFecha");
                    oEditText = (EditText)(oItem.Specific);
                    Entity.gestoria.Especifique = oEditText.String;

                    oItem = oForm.Items.Item("bajObs");
                    oEditText = (EditText)(oItem.Specific);
                    Entity.gestoria.Observacion = oEditText.String;

                    this.CargaMueveEvidencia("txtdownd");
                    break;
            }
        }
        void CargaMueveEvidencia(string scajaui)
        {
            try
            {

                Entity.gestoria.rutaevidencia = string.Empty;
                this.oItem = this.oForm.Items.Item(scajaui);
                this.oEditText = (EditText)this.oItem.Specific;
                if (!string.IsNullOrEmpty(this.oEditText.String))
                {
                    if (File.Exists(this.oEditText.String))
                    {
                        FileInfo oInfo = new FileInfo(this.oEditText.String);
                        Entity.gestoria.rutaevidencia = string.Format(@"{0}_{1}", Guid.NewGuid().ToString(), oInfo.Name);
                        string sOrigen = oInfo.FullName;
                        string sDestino = string.Format(@"{0}\{1}", this.sPathGestoria, Entity.gestoria.rutaevidencia);
                        File.Copy(sOrigen, sDestino);

                    }
                }
                else throw new Exception("Favor de seleccionar un documento de evidencia");
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        public void EntityToForm()
        {
            switch (Entity.gestoria.TipoGestion)
            {
                case 1:
                    #region Placas
                    if (Entity.gestoria.Fecha != null)
                    {
                        oItem = oForm.Items.Item("plaFecha");
                        oEditText = (EditText)(oItem.Specific);
                        oEditText.String = Entity.gestoria.Fecha.Value.ToString("dd/MM/yyyy");
                    }

                    if (Entity.gestoria.Numero != null)
                    {
                        oItem = oForm.Items.Item("plaNum");
                        oEditText = (EditText)(oItem.Specific);
                        oEditText.String = Entity.gestoria.Numero;
                    }

                    if (Entity.gestoria.Observacion != null)
                    {
                        oItem = oForm.Items.Item("plaObs");
                        oEditText = (EditText)(oItem.Specific);
                        oEditText.String = Entity.gestoria.Observacion;
                    }

                    if (!string.IsNullOrEmpty(Entity.gestoria.rutaevidencia))
                    {
                        oItem = oForm.Items.Item("txtdplaca");
                        oEditText = (EditText)(oItem.Specific);
                        oEditText.String = Entity.gestoria.rutaevidencia;
                    }
                    #endregion Placas
                    break;

                case 2:
                    #region Propietario
                    if (Entity.gestoria.Fecha != null)
                    {
                        oItem = oForm.Items.Item("proFecha");
                        oEditText = (EditText)(oItem.Specific);
                        oEditText.String = Entity.gestoria.Fecha.Value.ToString("dd/MM/yyyy");
                    }

                    if (Entity.gestoria.Observacion != null)
                    {
                        oItem = oForm.Items.Item("proObs");
                        oEditText = (EditText)(oItem.Specific);
                        oEditText.String = Entity.gestoria.Observacion;
                    }

                    if (!string.IsNullOrEmpty(Entity.gestoria.rutaevidencia))
                    {
                        oItem = oForm.Items.Item("txtpropd");
                        oEditText = (EditText)(oItem.Specific);
                        oEditText.String = Entity.gestoria.rutaevidencia;
                    }
                    #endregion Propietario
                    break;

                case 3:
                    #region Tarjeta
                    if (Entity.gestoria.Fecha != null)
                    {
                        oItem = oForm.Items.Item("tarFecha");
                        oEditText = (EditText)(oItem.Specific);
                        oEditText.String = Entity.gestoria.Fecha.Value.ToString("dd/MM/yyyy");
                    }

                    if (Entity.gestoria.Numero != null)
                    {
                        oItem = oForm.Items.Item("tarNum");
                        oEditText = (EditText)(oItem.Specific);
                        oEditText.String = Entity.gestoria.Numero;
                    }

                    if (Entity.gestoria.Observacion != null)
                    {
                        oItem = oForm.Items.Item("tarObs");
                        oEditText = (EditText)(oItem.Specific);
                        oEditText.String = Entity.gestoria.Observacion;
                    }

                    if (!string.IsNullOrEmpty(Entity.gestoria.rutaevidencia))
                    {
                        oItem = oForm.Items.Item("txttarjd");
                        oEditText = (EditText)(oItem.Specific);
                        oEditText.String = Entity.gestoria.rutaevidencia;
                    }
                    #endregion Tarjeta
                    break;

                case 4:
                    #region Alta
                    if (Entity.gestoria.Fecha != null)
                    {
                        oItem = oForm.Items.Item("altFecha");
                        oEditText = (EditText)(oItem.Specific);
                        oEditText.String = Entity.gestoria.Fecha.Value.ToString("dd/MM/yyyy");
                    }

                    if (Entity.gestoria.Observacion != null)
                    {
                        oItem = oForm.Items.Item("altObs");
                        oEditText = (EditText)(oItem.Specific);
                        oEditText.String = Entity.gestoria.Observacion;
                    }

                    if (!string.IsNullOrEmpty(Entity.gestoria.rutaevidencia))
                    {
                        oItem = oForm.Items.Item("txtupd");
                        oEditText = (EditText)(oItem.Specific);
                        oEditText.String = Entity.gestoria.rutaevidencia;
                    }
                    #endregion Alta
                    break;

                case 5:
                    #region Baja
                    if (Entity.gestoria.Fecha != null)
                    {
                        oItem = oForm.Items.Item("bajFecha");
                        oEditText = (EditText)(oItem.Specific);
                        oEditText.String = Entity.gestoria.Fecha.Value.ToString("dd/MM/yyyy");
                    }

                    if (Entity.gestoria.MotivoBajaID != null)
                    {
                        try
                        {
                            oItem = oForm.Items.Item("bajMot");
                            oComboBox = (ComboBox)(oItem.Specific);
                            oComboBox.Select(Entity.gestoria.MotivoBajaID.ToString(), BoSearchKey.psk_ByValue);
                        }
                        catch { }
                    }

                    if (Entity.gestoria.Especifique != null)
                    {
                        oItem = oForm.Items.Item("bajFecha");
                        oEditText = (EditText)(oItem.Specific);
                        oEditText.String = Entity.gestoria.Especifique;
                    }

                    if (Entity.gestoria.Observacion != null)
                    {
                        oItem = oForm.Items.Item("bajObs");
                        oEditText = (EditText)(oItem.Specific);
                        oEditText.String = Entity.gestoria.Observacion;
                    }

                    if (!string.IsNullOrEmpty(Entity.gestoria.rutaevidencia))
                    {
                        oItem = oForm.Items.Item("txtdownd");
                        oEditText = (EditText)(oItem.Specific);
                        oEditText.String = Entity.gestoria.rutaevidencia;
                    }
                    #endregion Baja
                    break;
            }
        }
        public void ViewEvidencia(string uicaja)
        {
            try
            {
                this.oItem = this.oForm.Items.Item(uicaja);
                this.oEditText = (EditText)this.oItem.Specific;
                if (!string.IsNullOrEmpty(this.oEditText.String))
                {
                    string sPathimage = string.Format(@"{0}\{1}", sPathGestoria, this.oEditText.String);
                    if (File.Exists(sPathimage))
                        Process.Start(sPathimage);
                }
            }
            catch (Exception) { }
        }
        public void CargaFileTexBox(string uicaja)
        {
            string path = "";
            try
            {
                Thread t = new Thread((ThreadStart)delegate
                {
                    System.Windows.Forms.FileDialog objDialog = new System.Windows.Forms.OpenFileDialog();
                    objDialog.Filter = "PDF files (*.pdf)|*.pdf|WORD files (*.doc)|*.doc|Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.png";
                    System.Windows.Forms.Form g = new System.Windows.Forms.Form();
                    g.Width = 1;
                    g.Height = 1;
                    g.Activate();
                    g.BringToFront();
                    g.Visible = true;
                    g.TopMost = true;
                    g.Focus();

                    System.Windows.Forms.DialogResult objResult = objDialog.ShowDialog(g);
                    Thread.Sleep(100);
                    if (objResult == System.Windows.Forms.DialogResult.OK)
                    {
                        path = objDialog.FileName;
                    }
                })
                {
                    IsBackground = false,
                    Priority = ThreadPriority.Highest
                };
                t.SetApartmentState(ApartmentState.STA);
                t.Start();
                while (!t.IsAlive) ;
                Thread.Sleep(1);
                t.Join();
                if (!String.IsNullOrEmpty(path))
                {
                    if (File.Exists(path))
                    {
                        this.oItem = this.oForm.Items.Item(uicaja);
                        this.oEditText = (EditText)this.oItem.Specific;
                        this.oEditText.String = path;
                    }
                }
            }
            catch (Exception ex)
            {
                this.SBO_Application.MessageBox("Error al seleccionar el archivo: " + ex.Message, 1, "OK");
            }

        }
        public void EmptyEntityToForm()
        {
            #region Placas
            oItem = oForm.Items.Item("plaFecha");
            oEditText = (EditText)(oItem.Specific);
            oEditText.String = String.Empty;

            oItem = oForm.Items.Item("plaNum");
            oEditText = (EditText)(oItem.Specific);
            oEditText.String = String.Empty;

            oItem = oForm.Items.Item("plaObs");
            oEditText = (EditText)(oItem.Specific);
            oEditText.String = String.Empty;

            oItem = oForm.Items.Item("txtdplaca");
            oEditText = (EditText)(oItem.Specific);
            oEditText.String = String.Empty;
            #endregion Placas

            #region Propietario
            oItem = oForm.Items.Item("proFecha");
            oEditText = (EditText)(oItem.Specific);
            oEditText.String = String.Empty;

            oItem = oForm.Items.Item("proObs");
            oEditText = (EditText)(oItem.Specific);
            oEditText.String = String.Empty;

            oItem = oForm.Items.Item("txtpropd");
            oEditText = (EditText)(oItem.Specific);
            oEditText.String = String.Empty;
            #endregion Propietario

            #region Tarjeta

            oItem = oForm.Items.Item("tarFecha");
            oEditText = (EditText)(oItem.Specific);
            oEditText.String = String.Empty;

            oItem = oForm.Items.Item("tarNum");
            oEditText = (EditText)(oItem.Specific);
            oEditText.String = String.Empty;

            oItem = oForm.Items.Item("tarObs");
            oEditText = (EditText)(oItem.Specific);
            oEditText.String = String.Empty;

            oItem = oForm.Items.Item("txttarjd");
            oEditText = (EditText)(oItem.Specific);
            oEditText.String = String.Empty;
            #endregion Tarjeta

            #region Alta
            oItem = oForm.Items.Item("altFecha");
            oEditText = (EditText)(oItem.Specific);
            oEditText.String = String.Empty;

            oItem = oForm.Items.Item("altObs");
            oEditText = (EditText)(oItem.Specific);
            oEditText.String = String.Empty;

            oItem = oForm.Items.Item("txtupd");
            oEditText = (EditText)(oItem.Specific);
            oEditText.String = String.Empty;
            #endregion Alta

            #region Baja
            oItem = oForm.Items.Item("bajFecha");
            oEditText = (EditText)(oItem.Specific);
            oEditText.String = String.Empty;

            oItem = oForm.Items.Item("bajMot");
            oComboBox = (ComboBox)(oItem.Specific);
            oComboBox.Select("0");

            oItem = oForm.Items.Item("bajEsp");
            oEditText = (EditText)(oItem.Specific);
            oEditText.String = String.Empty;

            oItem = oForm.Items.Item("bajFecha");
            oEditText = (EditText)(oItem.Specific);
            oEditText.String = String.Empty;

            oItem = oForm.Items.Item("bajObs");
            oEditText = (EditText)(oItem.Specific);
            oEditText.String = String.Empty;

            oItem = oForm.Items.Item("txtdownd");
            oEditText = (EditText)(oItem.Specific);
            oEditText.String = String.Empty;
            #endregion Baja
        }

        public void DefaultVehiculoToGestoria()
        {
            Entity.gestoria.VehiculoID = vehiculo.VehiculoID;
            Entity.gestoria.EmpresaID = vehiculo.Propietario.EmpresaID;
            Entity.gestoria.SucursalID = vehiculo.SubPropietario.SucursalID;
        }

        #endregion MetodosForm

        #region Validaciones
        public bool ValidarTipoGestion()
        {
            bool run = false;

            if (Entity != null)
                if (Entity.gestoria != null)
                    if (Entity.gestoria.TipoGestion != null)
                        if (Entity.gestoria.TipoGestion > 0)
                            run = true;
            return run;
        }
        #endregion Validaciones
    }
}
