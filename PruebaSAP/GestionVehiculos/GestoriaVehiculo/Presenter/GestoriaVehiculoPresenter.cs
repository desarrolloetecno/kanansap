﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#region Referencias
using SAPbobsCOM;
using SAPbouiCOM;
using KananSAP.GestionVehiculos.GestoriaVehiculo.View;
using KananWS.Interface;
using Kanan.Vehiculos.BO2;
using KananSAP.Helper;
using System.Threading;
using System.IO;
#endregion Referencias

namespace KananSAP.GestionVehiculos.GestoriaVehiculo.Presenter
{
    /*Comentarios
     * Creada 16/08/2018
     * Rair Santos
     */
    public class GestoriaVehiculoPresenter
    {
        #region Atibutos

        private SAPbouiCOM.Application SBO_Applicaction;
        private SAPbobsCOM.Company Company;
        public GestoriaVehiculoView view;
        private Configurations configs;
        private Vehiculo vehiculo;
        private GestoriaVehiculoWS GestoriaVehiculoWS;
        private Matrix mtx;

        public bool IsLogged { get; set; }

        #endregion Atributos

        #region Constructor
        public GestoriaVehiculoPresenter(SAPbouiCOM.Application application, SAPbobsCOM.Company company, Configurations c, Vehiculo v)
        {
            this.SBO_Applicaction = application;
            this.Company = company;
            this.vehiculo = v;
            this.configs = c;
            this.view = new GestoriaVehiculoView(this.SBO_Applicaction, this.Company, this.vehiculo);
        }
        #endregion Constructor

        #region Eventos
        public void SBO_Application_ItemEvent(String FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = false;
            /*Se valida desde DMA*/
            //if (!IsLogged)
            //    return;
            try
            {
                #region Gestoria
                if (pVal.FormTypeEx == "Gestoria" & view.Foco == true)
                {
                    String Folder = String.Empty;
                    if (!pVal.BeforeAction)
                        return;
                    if (pVal.BeforeAction)
                        if (pVal.ItemUID == "Close")
                        {
                            BubbleEvent = false;
                            this.view.CloseForm();
                        }

                    Folder = this.view.ActiveFolder();
                    if (!String.IsNullOrEmpty(Folder))
                    {
                        if (pVal.BeforeAction)
                        {
                            switch (Folder)
                            {
                                case "TabPlaca":
                                    switch (pVal.ItemUID)
                                    {
                                        case "Save":
                                            view.Entity.gestoria.TipoGestion = 1;
                                            view.FormToEntity();
                                            if (view.RegistroID != null & view.RegistroID > 0)
                                                view.Entity.Update();
                                            else
                                                view.Entity.Insert();
                                            view.EmptyEntityToForm();
                                            view.RegistroID = null;
                                            SBO_Applicaction.StatusBar.SetText("Registro de placa guardado. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                            this.view.BindDataToForm();
                                            break;
                                        case "mtxPlacas":
                                            if (pVal.EventType == BoEventTypes.et_MATRIX_LINK_PRESSED)
                                            {
                                                this.view.SetID(pVal.Row, "mtxPlacas");
                                                this.view.Entity.GetEntityByID(this.view.RegistroID, 1);
                                                this.view.Entity.gestoria.TipoGestion = 1;
                                                this.view.EntityToForm();
                                            }
                                            break;
                                        case "btnplacad":
                                            this.view.CargaFileTexBox("txtdplaca");
                                            break;
                                        case "viewpla":
                                            this.view.ViewEvidencia("txtdplaca");
                                            break;
                                    }
                                    break;

                                case "TabProp":
                                    switch (pVal.ItemUID)
                                    {
                                        case "Save":
                                            view.Entity.gestoria.TipoGestion = 2;
                                            view.FormToEntity();
                                            if (view.RegistroID != null & view.RegistroID > 0)
                                                view.Entity.Update();
                                            else
                                                view.Entity.Insert();
                                            view.EmptyEntityToForm();
                                            view.RegistroID = null;
                                            SBO_Applicaction.StatusBar.SetText("Registro de propietario guardado. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                            this.view.BindDataToForm();
                                            break;
                                        case "mtxProp":
                                            if (pVal.EventType == BoEventTypes.et_MATRIX_LINK_PRESSED)
                                            {
                                                this.view.SetID(pVal.Row, "mtxProp");
                                                this.view.Entity.GetEntityByID(this.view.RegistroID, 2);
                                                this.view.Entity.gestoria.TipoGestion = 2;
                                                this.view.EntityToForm();
                                            }
                                            break;
                                        case "btnpropd":
                                            this.view.CargaFileTexBox("txtpropd");
                                            break;
                                        case "viewprop":
                                            this.view.ViewEvidencia("txtpropd");
                                            break;
                                    }
                                    break;

                                case "TabTarje":
                                    switch (pVal.ItemUID)
                                    {
                                        case "Save":
                                            view.Entity.gestoria.TipoGestion = 3;
                                            view.FormToEntity();
                                            if (view.RegistroID != null & view.RegistroID > 0)
                                                view.Entity.Update();
                                            else
                                                view.Entity.Insert();
                                            view.EmptyEntityToForm();
                                            view.RegistroID = null;
                                            SBO_Applicaction.StatusBar.SetText("Registro de tarjeta guardado. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                            this.view.BindDataToForm();
                                            break;
                                        case "mtxTarjeta":
                                            if (pVal.EventType == BoEventTypes.et_MATRIX_LINK_PRESSED)
                                            {
                                                this.view.SetID(pVal.Row, "mtxTarjeta");
                                                this.view.Entity.GetEntityByID(this.view.RegistroID, 3);
                                                this.view.Entity.gestoria.TipoGestion = 3;
                                                this.view.EntityToForm();
                                            }
                                            break;
                                        case "btntarjd":
                                            this.view.CargaFileTexBox("txttarjd");
                                            break;
                                        case "viewtarj":
                                            this.view.ViewEvidencia("txttarjd");
                                            break;
                                    }
                                    break;

                                case "Alta":
                                    switch (pVal.ItemUID)
                                    {
                                        case "Save":
                                            view.Entity.gestoria.TipoGestion = 4;
                                            view.FormToEntity();
                                            if (view.RegistroID != null & view.RegistroID > 0)
                                                view.Entity.Update();
                                            else
                                                view.Entity.Insert();
                                            view.EmptyEntityToForm();
                                            view.RegistroID = null;
                                            SBO_Applicaction.StatusBar.SetText("Registro de alta guardado. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                            this.view.BindDataToForm();
                                            break;
                                        case "mtxAlta":
                                            if (pVal.EventType == BoEventTypes.et_MATRIX_LINK_PRESSED)
                                            {
                                                this.view.SetID(pVal.Row, "mtxAlta");
                                                this.view.Entity.GetEntityByID(this.view.RegistroID, 4);
                                                this.view.Entity.gestoria.TipoGestion = 4;
                                                this.view.EntityToForm();
                                            }
                                            break;
                                        case "btnupd":
                                            this.view.CargaFileTexBox("txtupd");
                                            break;
                                        case "viewup":
                                            this.view.ViewEvidencia("txtupd");
                                            break;
                                    }
                                    break;

                                case "Baja":
                                    switch (pVal.ItemUID)
                                    {
                                        case "bajMot":
                                            if (view.IsOtroCombo())
                                                view.EnableItem("bajEsp");
                                            break;

                                        case "Save":
                                            view.Entity.gestoria.TipoGestion = 5;
                                            view.FormToEntity();
                                            if (view.RegistroID != null & view.RegistroID > 0)
                                                view.Entity.Update();
                                            else
                                                view.Entity.Insert();
                                            view.EmptyEntityToForm();
                                            view.RegistroID = null;
                                            SBO_Applicaction.StatusBar.SetText("Registro de baja guardado. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                            this.view.BindDataToForm();
                                            break;
                                        case "mtxBaja":
                                            if (pVal.EventType == BoEventTypes.et_MATRIX_LINK_PRESSED)
                                            {
                                                this.view.SetID(pVal.Row, "mtxBaja");
                                                this.view.Entity.GetEntityByID(this.view.RegistroID, 5);
                                                this.view.Entity.gestoria.TipoGestion = 5;
                                                this.view.EntityToForm();
                                            }
                                            break;
                                        case "btndownd":
                                            this.view.CargaFileTexBox("txtdownd");
                                            break;
                                        case "viewbaja":
                                            this.view.ViewEvidencia("txtdownd");
                                            break;
                                    }
                                    break;
                            }
                        }
                        return;
                    }
                }

                if (pVal.FormTypeEx == "Gestoria" & pVal.EventType == BoEventTypes.et_FORM_CLOSE)
                    this.view.Foco = false;
                if (pVal.FormTypeEx == "Gestoria" & pVal.EventType == BoEventTypes.et_FORM_UNLOAD)
                    this.view.Foco = false;


                #endregion Gestoria
            }
            catch (Exception ex)
            {
                SBO_Applicaction.StatusBar.SetText("" + ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                KananSAPHerramientas.LogError("GestoriaPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
                return;
            }
        }
        #endregion Eventos

        #region Métodos

        #endregion Métodos
    }
}
