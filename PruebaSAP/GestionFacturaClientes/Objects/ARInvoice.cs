﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.GestionFacturaClientes.Objects
{
    public class ARInvoice
    {
        public string CardCode { get; set; }
        public string CardName { get; set; }
        public string NumAtCard { get; set; }
        public int NumDocumento { get; set; }
        public string DocuDueDate { get; set; }
        public string DocDate { get; set; }
        public string TaxDate { get; set; }
        public string PrefixFolio { get; set; }
        public string Folio { get; set; }
        public String Comentarios { get; set; }
        public List<ARInvoiceLines> Lineas {get; set;}
    }

    public class ARInvoiceLines
    {
        public String ItemCode { get; set; }
        public String Description { get; set; }
        public Double Quantity { get; set; }
        public Double Bultos { get; set; }
        public Double Peso { get; set; }
    
    }
}
