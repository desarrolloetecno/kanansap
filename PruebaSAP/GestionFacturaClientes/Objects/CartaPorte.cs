﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.GestionFacturaClientes.Objects
{
    public class CartaPorte
    {
        public String RutaOrigen { get; set; }
        public String RutaDestino { get; set; }
        public String Vehiculo { get; set; }
        public String Operador { get; set; }
        public String NumeroViaje { get; set; }
        public String Bultos { get; set; }
        public String Carga { get; set; }
        public String Peso { get; set; }
        public String Comentarios { get; set; }
        public ARInvoiceLines LineasEntrega { get; set; }


    }
}
