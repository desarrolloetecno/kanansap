﻿using SAPbouiCOM; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.GestionFacturaClientes.Objects
{

    public class CartaPorteINFO
    {

        public String RutaOrigen { get; set; }
        public String RutaDestino { get; set; }
        public String Vehiculo { get; set; }
        public String Operador { get; set; }
        public string NumViaje { get; set; }
        public string Bulto { get; set; }
        public string Carga { get; set; }
        public string Peso { get; set; }
        public string DocNum { get; set; }
        public String Comentario { get; set; }
        public List<DataCartaPorte> lstRegistros { get; set; }
    }
    public class DataCartaPorte
    {
        public int iLineNum { get; set; }
        public String Articulo { get; set; }
        public String Descripcion { get; set; }
        public Decimal Cantidad { get; set; }
        public String Bulto { get; set; }
        public String Carga { get; set; }
        public String Peso { get; set; }
    }

    public class RefaccionesView
    {
        public EditText txtRefSAP { get; set; }
        public EditText txtQntyRf { get; set; }
        public EditText txtCostoRf { get; set; }
        public ComboBox cmbAlmacn { get; set; }
        public ComboBox cmbRefaccion { get; set; }
        public Item ocmbAlmacen { get; set; }
        public Item ocmbReffcion { get; set; }


    }

    public class ARInvoiceINFO
    {
        #region Comentarios Clase
        /*
         Clase que gestiona las propiedades a nivel vista de la carta de porte
         */
        #endregion
        public EditText txtRutaOrigen { get; set; }
        public EditText txtRutaDestino { get; set; }
        public EditText cmbVehiculo { get; set; }
        public ComboBox cmbOperador { get; set; }
        public EditText txtNumViaje { get; set; }
        public EditText txtBulto { get; set; }
        public EditText txtCarga { get; set; }
        public EditText txtPeso { get; set; }
        public EditText txtComentario { get; set; }

        public Item oitemRutaOrigen { get; set; }
        public Item oitemRutaDestino { get; set; }
        public Item oitemVehiculo { get; set; }
        public Item oitemOperador { get; set; }
        public Item oitemNumViaje { get; set; }
        public Item oitemBulto { get; set; }
        public Item oitemCarga { get; set; }
        public Item oitemPeso { get; set; }
        public Item oitemComentario { get; set; }

    }
}
