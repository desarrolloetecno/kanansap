﻿﻿using KananSAP.GestionFacturaClientes.Objects;
using KananSAP.GestionVehiculos.RecargaCombustible.View;
using KananSAP.Helper;
using SAPbobsCOM;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.GestionFacturaClientes.Controller
{
    public class GestionARInvoices
    {
        #region Atributos
        public GestionCartaPorte CartaForm = null;
        public Form oForm = null;
        private SAPbouiCOM.Application SBO_Application;
        private GestionGlobales oGlobales = null;
        private String UIIDFormpadre = string.Empty;
        private Matrix oArticulos = null;
        public String UIITEmporal = string.Empty;
        public CartaPorteINFO CartaporteINFO = null;
        #endregion

        #region Comentarios_Clase
        /**
         * Clase encargada de gestionar los eventos del formularios de SAP 
         * 
         * **/
        #endregion


        public GestionARInvoices(GestionGlobales oGlobales, String FormUIDPadre)
        {
            this.oGlobales = oGlobales;
            this.SBO_Application = oGlobales.SBO_Application;
            this.UIIDFormpadre = FormUIDPadre;
        }

        public void EventoDatosForma(BusinessObjectInfo pVal, out bool BubbleEvent, Form oCurrentForm)
        {
            BubbleEvent = false;

            try
            {

                if (!pVal.BeforeAction)
                {
                    switch (pVal.EventType)
                    {


                        case BoEventTypes.et_FORM_DATA_ADD:
                            BubbleEvent = false;
                            if (!pVal.BeforeAction)
                            {

                                string DocNum = oForm.DataSources.DBDataSources.Item(0).GetValue("DocNum", 0);

                                if (CartaporteINFO == null)
                                {
                                    //this.oGlobales.Mostrarmensaje("Captura la información de la carta de porte", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                                    this.SBO_Application.MessageBox("Captura la información de la carta de porte", 1, "Aceptar");
                                    this.oForm.Items.Item("btnCartaP").Click();
                                    BubbleEvent = false;
                                    break;
                                }
                                else
                                {
                                    if (CartaporteINFO.lstRegistros.Count == 0)
                                    {
                                        //this.oGlobales.Mostrarmensaje("No se encuentran articulos para la carta de porte", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                                        this.SBO_Application.MessageBox("No se encuentran articulos para la carta de porte", 1, "Aceptar");
                                        this.oForm.Items.Item("btnCartaP").Click();
                                        BubbleEvent = false;
                                        break;
                                    }
                                    else
                                    {
                                        BubbleEvent = this.CreaCartaPorte(DocNum, CartaporteINFO);
                                        if (!BubbleEvent)
                                            break;
                                        CartaporteINFO = null;
                                        try { CartaForm.oForm.Close(); }
                                        catch { }
                                    }

                                }

                                break;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void EventoControl(ItemEvent pVal, out bool BubbleEvent, Form oCurrentForm)
        {
            this.oForm = oCurrentForm;
            BubbleEvent = true;
            try
            {


                switch (pVal.ItemUID)
                {
                   

                    case "btnCartaP":
                        if (pVal.Before_Action)
                        {
                            if (oCurrentForm.Mode == BoFormMode.fm_ADD_MODE)
                            {
                                BubbleEvent = ValidarArticulos(oCurrentForm);
                                if (BubbleEvent)
                                {
                                    string DocNum = oForm.DataSources.DBDataSources.Item(0).GetValue("DocNum", 0);
                                    if (CartaporteINFO != null)
                                    {
                                        if (DocNum != CartaporteINFO.DocNum)
                                            CartaporteINFO = null;
                                    }

                                    if (string.IsNullOrEmpty(this.UIITEmporal))
                                        this.UIITEmporal = this.oGlobales.GenerarUUID(DocNum, "");
                                    if (oArticulos.RowCount >= 1)
                                        CartaForm = new GestionCartaPorte(this.oGlobales, oArticulos, CartaporteINFO, this.UIIDFormpadre, DocNum);
                                    else
                                        this.oGlobales.Mostrarmensaje("Favor de agregar articulos a la factura", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error, true);



                                }
                            }
                            else if (oCurrentForm.Mode == BoFormMode.fm_OK_MODE)
                            {
                                BubbleEvent = ValidarArticulos(oCurrentForm);
                                this.UIITEmporal = oCurrentForm.DataSources.DBDataSources.Item(0).GetValue("DocNum", 0);
                                this.ValidarExisteCarta();
                            }

                        }
                        break;
                }

            }
            catch (Exception ex)
            {

                KananSAPHerramientas.LogError("GestionARInvoices.cs->EventoControl()", ex.Message);
            }
        }


        private Boolean CreaCartaPorte(String docNum, CartaPorteINFO data)
        {
            bool bContinuar = false;
            try
            {
                String message = string.Empty;
                GestionCartaPorte oCarta = new GestionCartaPorte();
                bContinuar = oCarta.Validar(data, out message);
                if (bContinuar)
                {
                    this.oGlobales.Company.StartTransaction();

                    Recordset oContarRegistros = (Recordset)this.oGlobales.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                    oContarRegistros.DoQuery(oCarta.EliminaCabeceraByID(docNum));
                    oContarRegistros.DoQuery(oCarta.EliminaDetalleByID(docNum));
                    oContarRegistros.DoQuery(oCarta.iCantidadRegistros());
                    Int32 iTotales = (oContarRegistros.RecordCount + 1);

                    oContarRegistros.DoQuery(oCarta.InsertarCarta(data, docNum));
                    foreach (DataCartaPorte concepto in data.lstRegistros)
                    {
                        oContarRegistros.DoQuery(oCarta.InsertarDetalle(iTotales, concepto, docNum));
                        iTotales++;
                    }

                    if (this.oGlobales.Company.InTransaction)
                        this.oGlobales.Company.EndTransaction(BoWfTransOpt.wf_Commit);
                }
                else { this.SBO_Application.MessageBox(message, 1, "Aceptar"); this.oForm.Items.Item("btnCartaP").Click(); }
            }
            catch (Exception ex)
            {
                bContinuar = false;
                if (this.oGlobales.Company.InTransaction)
                    this.oGlobales.Company.EndTransaction(BoWfTransOpt.wf_RollBack);
            }


            return bContinuar;
        }

        private void ValidarExisteCarta()
        {

            Recordset CPorte = (Recordset)this.oGlobales.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            CPorte.DoQuery(BuscarCarta());
            if (CPorte.RecordCount > 0)
            {
                this.CartaporteINFO = new CartaPorteINFO();

                CPorte.MoveFirst();
                this.CartaporteINFO.RutaDestino = Convert.ToString(CPorte.Fields.Item("U_RUTADESTINO").Value);
                this.CartaporteINFO.RutaOrigen = Convert.ToString(CPorte.Fields.Item("U_RUTAORIGEN").Value);
                this.CartaporteINFO.Vehiculo = Convert.ToString(CPorte.Fields.Item("U_VEHICULO").Value);
                this.CartaporteINFO.Operador = Convert.ToString(CPorte.Fields.Item("U_OPERADOR").Value);
                this.CartaporteINFO.Comentario = Convert.ToString(CPorte.Fields.Item("U_OBSERVACIONES").Value);
                this.CartaporteINFO.DocNum = Convert.ToString(CPorte.Fields.Item("Code").Value);
                this.CartaporteINFO.NumViaje = Convert.ToString(CPorte.Fields.Item("U_NUMVIAJE").Value);
                this.CartaporteINFO.lstRegistros = new List<DataCartaPorte>();
                while (!CPorte.EoF)
                {
                    DataCartaPorte otemp = new DataCartaPorte();
                    otemp.Articulo = Convert.ToString(CPorte.Fields.Item("U_ITEMCODE").Value);
                    otemp.Descripcion = Convert.ToString(CPorte.Fields.Item("U_DESCRIPCION").Value);
                    otemp.Cantidad = Convert.ToDecimal(CPorte.Fields.Item("U_CANTIDAD").Value);
                    otemp.Bulto = Convert.ToString(CPorte.Fields.Item("U_BULTO").Value);
                    otemp.Carga = Convert.ToString(CPorte.Fields.Item("U_CARGA").Value);
                    otemp.Peso = Convert.ToString(CPorte.Fields.Item("U_PESO").Value);
                    this.CartaporteINFO.lstRegistros.Add(otemp);
                    CPorte.MoveNext();
                }


                CartaForm = new GestionCartaPorte(this.oGlobales, oArticulos, CartaporteINFO, this.UIIDFormpadre, UIITEmporal);
            }
            else { this.oGlobales.Mostrarmensaje("No existen registro para carta de porte", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error, true); }
        }

        private Boolean ValidarArticulos(Form oCurrentForm)
        {
            Boolean bContinuar = true;
            try
            {
                ComboBox oDocType = (ComboBox)oCurrentForm.Items.Item(oCurrentForm.Items.Item("3").UniqueID).Specific;


                if (oDocType.Value.ToString().Trim() == "I")
                {
                    oArticulos = (Matrix)oCurrentForm.Items.Item(oCurrentForm.Items.Item("38").UniqueID).Specific;
                    if (oArticulos.RowCount <= 1 && oCurrentForm.Mode != BoFormMode.fm_OK_MODE)
                    {
                        this.oGlobales.Mostrarmensaje("Agrega artículos para poder gestionar la carta porte", BoMessageTime.bmt_Long, BoStatusBarMessageType.smt_Warning);
                        bContinuar = false;
                    }
                }
                else bContinuar = false;
            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError("GestionARInvoices.cs->ValidarArticulos()", ex.Message);
            }
            return bContinuar;
        }

        #region Consultas
        private String BuscarCarta()
        {
            String strQuery = "";

            if (GestionGlobales.bSqlConnection)

                strQuery = string.Format(@"SELECT T1.Code, T1.U_RutaOrigen, T1.U_RutaDestino, T1.U_Vehiculo,
                            T1.U_Operador, T1.U_Observaciones, T1.U_DocFactura,T1.U_NumViaje,
                            T2.U_ItemCode, T2.U_Descripcion, T2.U_Cantidad, T2.U_Bulto,
                            T2.U_Carga, T2.U_Peso FROM ""@VSKF_CARTADEPORTE"" T1 INNER JOIN [@VSKF_CARTADETALLE] T2
                            ON T1.Code = T2.U_IDCarta WHERE T1.U_DocFactura = '{0}'", this.UIITEmporal);
            else
                strQuery = string.Format(@"SELECT ""T1"".""Code"", ""T1"".""U_RUTAORIGEN"", ""T1"".""U_RUTADESTINO"", ""T1"".""U_VEHICULO"",
                            ""T1"".""U_OPERADOR"", ""T1"".""U_OBSERVACIONES"", ""T1"".""U_DOCFACTURA"",""T1"".""U_NUMVIAJE"",
                            ""T2"".""U_ITEMCODE"", ""T2"".""U_DESCRIPCION"", ""T2"".""U_CANTIDAD"", ""T2"".""U_BULTO"",
                            ""T2"".""U_CARGA"", ""T2"".""U_PESO"" FROM ""@VSKF_CARTADEPORTE"" ""T1"" INNER JOIN ""@VSKF_CARTADETALLE"" ""T2""
                            ON ""T1"".""Code"" = ""T2"".""U_IDCARTA"" WHERE ""T1"".""U_DOCFATURA"" = '{0}'", this.UIITEmporal);

            return strQuery;
        }

        #endregion

    }
}