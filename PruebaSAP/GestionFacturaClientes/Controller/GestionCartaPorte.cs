﻿using System;
using SAPbouiCOM;
using SAPinterface.Data;
using Application = System.Windows.Forms.Application;
using KananSAP.GestionFacturaClientes.Objects;
using SAPbobsCOM;
using KananSAP.Helper;
using System.Collections.Generic;
using System.Linq;

namespace KananSAP.GestionFacturaClientes.Controller
{
    public class GestionCartaPorte
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        public SAPbouiCOM.Form oForm;
        //public String UIIDTemporal = string.Empty;
        //public Boolean bGuardado = false;
        private XMLPerformance XmlApplication;
        private String UIDFormpadre = string.Empty;
        private GestionGlobales oGlobales = null;
        private Matrix oMatrixData = null;
        private ARInvoiceINFO oInfoPorte = null;
        private Matrix oMatrixPorte;
        private String DocNum = string.Empty;
        //private string FolioFacura = "";
        #endregion

        #region Comentarios Clase
        /*
         * Clase encargada de gestionar los eventos del Formulario 
         * Carta de porte
         */
        #endregion

        public GestionCartaPorte(GestionGlobales oGlobales, Matrix oMatrixArticulos, CartaPorteINFO dataINFO, String UIFPadre, string DocNum)
        {
            //this.FolioFacura = DocFactura;
            //this.UIIDTemporal = UITemporal;
            this.oGlobales = oGlobales;
            this.SBO_Application = this.oGlobales.SBO_Application;
            this.XmlApplication = new XMLPerformance(SBO_Application);
            this.UIDFormpadre = UIFPadre;
            Boolean bContinuar = LoadForm();
            this.ShowForm();
            this.oMatrixData = oMatrixArticulos;
            this.Inicializar();
            this.DocNum = DocNum;
            this.LlenarObjeto(dataINFO, oMatrixArticulos);
            //this.LlenarComponentes();
        }

        public GestionCartaPorte()
        {

        }

        private Boolean LoadForm()
        {
            string UID = string.Format("GP_{0}", UIDFormpadre);
            FormCreationParams oFormCreate = null;
            Boolean bContinuar = false;
            String XmlData = "";
            try
            {

                this.oForm = SBO_Application.Forms.Item(UID);
                if (oForm.UniqueID != UID)
                    throw new Exception("");
                bContinuar = true;
            }
            catch (Exception e)
            {
                //this.oGlobales.Mostrarmensaje(e.Message.ToString(), BoMessageTime.bmt_Long, BoStatusBarMessageType.smt_Error, true);
                bContinuar = false;
                try
                {
                    string sPath = Application.StartupPath;                    
                    XmlData = this.XmlApplication.LoadFromXML(sPath + "\\XML", "", "GestionCartaPorte.xml");
                    bContinuar = true;
                    oFormCreate = (FormCreationParams)this.oGlobales.SBO_Application.CreateObject(BoCreatableObjectType.cot_FormCreationParams);
                    oFormCreate.UniqueID = UID;
                    oFormCreate.FormType = "GesCPorte";
                    oFormCreate.XmlData = XmlData;
                    this.oForm = this.oGlobales.SBO_Application.Forms.AddEx(oFormCreate); //SBO_Application.Forms.Item(UID);
                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;

                }
                catch (Exception ex)
                {
                    bContinuar = false;
                    KananSAPHerramientas.LogError("GestionCartaPorte.cs->LoadForm()", ex.Message);
                }
            }
            return bContinuar;
        }

        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
                this.AddItemsCFL();
            }
            catch (Exception ex)
            {
                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                this.AddItemsCFL();
                this.oForm.Select();
                KananSAPHerramientas.LogError("GestionCartaPorte.cs->ShowForm()", ex.Message);
            }
        }



        public void AddItemsCFL()
        {
            try
            {

                Item oItem = null;
                oForm.DataSources.UserDataSources.Add("EditDS", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);
                SAPbouiCOM.ChooseFromListCollection oCFLs = null;
                oCFLs = oForm.ChooseFromLists;
                SAPbouiCOM.ChooseFromList oCFL = null;
                SAPbouiCOM.ChooseFromListCreationParams oCFLCreationParams = null;
                oCFLCreationParams = ((SAPbouiCOM.ChooseFromListCreationParams)(SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)));
                oCFLCreationParams.MultiSelection = false;
                oCFLCreationParams.ObjectType = "4";
                oCFLCreationParams.UniqueID = "CFL10";

                oCFL = oCFLs.Add(oCFLCreationParams);
                SAPbouiCOM.Conditions oConditions = this.SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_Conditions);
                oConditions = oCFL.GetConditions();
                SAPbouiCOM.Condition oCondition = oConditions.Add();
                oCondition.Alias = "ItmsGrpCod";
                oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
                oCondition.CondVal = new GestionGlobales().iFiltroGrupo(this.oGlobales.Company, "VEHICULOS KF");
                oCFL.SetConditions(oConditions);
                oItem = oForm.Items.Item("txtVeh");
                EditText oEditText = (EditText)(oItem.Specific);
                oEditText.DataBind.SetBound(true, "", "EditDS");
                oEditText.ChooseFromListUID = "CFL10";
                oEditText.ChooseFromListAlias = "ItemCode";

            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError("GestionConfig.cs->AddItemsCFL()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public Matrix DevolverMatrix()
        {
            return this.oMatrixPorte;
        }
        private void LlenarObjeto(CartaPorteINFO data, Matrix mdtArticulos)
        {
            try
            {
                Boolean bAddMatrix = true;
                this.oMatrixPorte = (Matrix)this.oForm.Items.Item("matCPorte").Specific;
                Columns oColum = (SAPbouiCOM.Columns)oMatrixData.Columns;
                this.oMatrixPorte.Clear();
                int iLinea = 0;
                this.oGlobales.FillCombo("Ninguno", "Code", "Name", "cmbCPOper", Operadores(), this.oForm);
                if (data != null)
                {
                    this.oInfoPorte.txtRutaOrigen.String = data.RutaOrigen;
                    this.oInfoPorte.txtRutaDestino.String = data.RutaDestino;
                    this.oInfoPorte.txtNumViaje.String = data.NumViaje;
                    try { this.oInfoPorte.cmbVehiculo.String = data.Vehiculo; }
                    catch { }
                    try
                    { this.oInfoPorte.cmbOperador.Select(data.Operador, BoSearchKey.psk_ByValue); }
                    catch { }
                    this.oInfoPorte.txtComentario.String = data.Comentario;

                    if (data.lstRegistros.Count > 0)
                    {
                        bAddMatrix = false;
                        for (int i = mdtArticulos.RowCount; i > 0; i--)
                        {
                            Boolean bAdd = false;
                            EditText txtCode = (EditText)oMatrixData.Columns.Item("1").Cells.Item(i).Specific;
                            EditText txtCantidad = (EditText)oMatrixData.Columns.Item("11").Cells.Item(i).Specific;
                            EditText txtPeso = (EditText)oMatrixData.Columns.Item("58").Cells.Item(i).Specific;
                            EditText txtDescripcion = (EditText)oMatrixData.Columns.Item("3").Cells.Item(i).Specific;


                            DataCartaPorte oTemp = data.lstRegistros.FirstOrDefault(x => x.Articulo == txtCode.String.ToString().Trim());
                            if (oTemp != null)
                            {

                                this.oForm.DataSources.UserDataSources.Item("oUMAT01").Value = oTemp.iLineNum.ToString();
                                this.oForm.DataSources.UserDataSources.Item("oUMAT02").Value = oTemp.Articulo;
                                this.oForm.DataSources.UserDataSources.Item("oUMAT03").Value = oTemp.Descripcion;
                                this.oForm.DataSources.UserDataSources.Item("oUMAT04").Value = oTemp.Cantidad.ToString();
                                this.oForm.DataSources.UserDataSources.Item("oUMAT05").Value = oTemp.Bulto;
                                this.oForm.DataSources.UserDataSources.Item("oUMAT06").Value = oTemp.Carga;
                                this.oForm.DataSources.UserDataSources.Item("oUMAT07").Value = oTemp.Peso;


                                bAdd = true;
                            }
                            else
                            {

                                if (!string.IsNullOrEmpty(txtCode.String.ToString()))
                                {
                                    //this.oForm.DataSources.UserDataSources.Item("oUMAT01").Value = oTemp.iLineNum.ToString();
                                    this.oForm.DataSources.UserDataSources.Item("oUMAT02").Value = txtCode.String.ToString().Trim();
                                    this.oForm.DataSources.UserDataSources.Item("oUMAT03").Value = txtDescripcion.String.ToString().Trim();
                                    this.oForm.DataSources.UserDataSources.Item("oUMAT04").Value = txtCantidad.String.ToString().Trim();
                                    bAdd = true;
                                }
                            }
                            if (bAdd)
                                this.oMatrixPorte.AddRow(1, -1);
                        }
                    }
                }


                if (bAddMatrix)
                {
                    if (mdtArticulos.RowCount >= 1)
                    {
                        for (int i = 1; i <= oMatrixData.RowCount; i++)
                        {
                            EditText txtCode = (EditText)oMatrixData.Columns.Item("1").Cells.Item(i).Specific;
                            EditText txtCantidad = (EditText)oMatrixData.Columns.Item("11").Cells.Item(i).Specific;
                            EditText txtPeso = (EditText)oMatrixData.Columns.Item("58").Cells.Item(i).Specific;
                            EditText txtDescripcion = (EditText)oMatrixData.Columns.Item("3").Cells.Item(i).Specific;

                            String ItemCode = txtCode.String.ToString().Trim();
                            String Quantity = txtCantidad.String.ToString().Trim();
                            String Description = txtDescripcion.String.ToString().Trim();
                            String Peso = txtPeso.String.ToString().Trim();
                            if (!string.IsNullOrEmpty(ItemCode))
                            {

                                this.oForm.DataSources.UserDataSources.Item("oUMAT01").Value = iLinea.ToString();
                                this.oForm.DataSources.UserDataSources.Item("oUMAT02").Value = ItemCode;
                                this.oForm.DataSources.UserDataSources.Item("oUMAT03").Value = Description;
                                this.oForm.DataSources.UserDataSources.Item("oUMAT04").Value = Quantity;
                                this.oForm.DataSources.UserDataSources.Item("oUMAT07").Value = Peso;

                                this.oMatrixPorte.AddRow(1, -1);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public CartaPorteINFO FormToEntity()
        {
            CartaPorteINFO INFO = new CartaPorteINFO();

            INFO.RutaOrigen = this.oInfoPorte.txtRutaOrigen.String;
            INFO.RutaDestino = this.oInfoPorte.txtRutaDestino.String;
            INFO.NumViaje = this.oInfoPorte.txtNumViaje.String;
            INFO.Vehiculo = this.oInfoPorte.cmbVehiculo.String;
            INFO.Comentario = this.oInfoPorte.txtComentario.String;
            INFO.DocNum = this.DocNum;
            INFO.Operador = this.oInfoPorte.cmbOperador.Value.ToString();
            List<DataCartaPorte> datalist = new List<DataCartaPorte>();
            if (oMatrixPorte != null)
            {
                int iLinea = 1;
                for (int i = oMatrixPorte.RowCount; i > 0; i--)
                {
                    DataCartaPorte otemp = new DataCartaPorte();
                    EditText txtItem = (EditText)oMatrixPorte.Columns.Item("cNItem").Cells.Item(i).Specific;
                    EditText txtDescrip = (EditText)oMatrixPorte.Columns.Item("colDesc").Cells.Item(i).Specific;
                    EditText txtBulto = (EditText)oMatrixPorte.Columns.Item("colBul").Cells.Item(i).Specific;
                    EditText txtCarga = (EditText)oMatrixPorte.Columns.Item("colCar").Cells.Item(i).Specific;
                    EditText txtPeso = (EditText)oMatrixPorte.Columns.Item("colPe").Cells.Item(i).Specific;
                    EditText txtLine = (EditText)oMatrixPorte.Columns.Item("cNumCP").Cells.Item(i).Specific;
                    EditText txtCantidad = (EditText)oMatrixPorte.Columns.Item("colCant").Cells.Item(i).Specific;

                    otemp.iLineNum = iLinea;//Convert.ToInt32(txtLine.String.Trim());
                    otemp.Descripcion = txtDescrip.String.Trim();
                    otemp.Articulo = txtItem.String.Trim();
                    otemp.Bulto = txtBulto.String.Trim();
                    otemp.Carga = txtCarga.String.Trim();
                    otemp.Peso = txtPeso.String.Trim();
                    otemp.Cantidad = txtCantidad.String == "" ? 0 : Convert.ToDecimal(txtCantidad.String.Trim());
                    datalist.Add(otemp);
                    iLinea++;
                }
            }
            INFO.lstRegistros = datalist;
            return INFO;
        }


        public void EventoControl(ItemEvent pVal, out bool BubbleEvent, Form oCurrentForm)
        {
            BubbleEvent = true;
            try
            {
                switch (pVal.ItemUID)
                {
                    case "btnSveCP":
                        break;

                    case "matCPorte":

                        break;

                    case "btnCnclCP":
                        if (pVal.Before_Action)
                            this.oForm.Close();
                        break;
                }
            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError("GestionCartaPorte.cs->EventoControl()", ex.Message);
            }
        }
        private void Inicializar()
        {
            oInfoPorte = new ARInvoiceINFO();
            try
            {
                oInfoPorte.oitemRutaOrigen = this.oForm.Items.Item("txtOrigen");
                oInfoPorte.txtRutaOrigen = (EditText)this.oInfoPorte.oitemRutaOrigen.Specific;
                oInfoPorte.oitemRutaDestino = this.oForm.Items.Item("txtDestin");
                oInfoPorte.txtRutaDestino = (EditText)this.oInfoPorte.oitemRutaDestino.Specific;
                oInfoPorte.oitemVehiculo = this.oForm.Items.Item("txtVeh");
                oInfoPorte.cmbVehiculo = (EditText)this.oInfoPorte.oitemVehiculo.Specific;
                oInfoPorte.oitemOperador = this.oForm.Items.Item("cmbCPOper");
                oInfoPorte.cmbOperador = (ComboBox)this.oInfoPorte.oitemOperador.Specific;
                oInfoPorte.oitemNumViaje = this.oForm.Items.Item("txtNmViaj");
                oInfoPorte.txtNumViaje = (EditText)this.oInfoPorte.oitemNumViaje.Specific;
                oInfoPorte.oitemComentario = this.oForm.Items.Item("txtCmnts");
                oInfoPorte.txtComentario = (EditText)this.oInfoPorte.oitemComentario.Specific;

            }
            catch (Exception ex)
            {

                KananSAPHerramientas.LogError("GestionCartaPorte.cs->Inicializar()", ex.Message);
                oInfoPorte = null;
            }

        }
        public Boolean Validar(CartaPorteINFO data, out String Message)
        {

            Boolean bContinuar = true;
            Message = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(data.RutaOrigen))
                    Message = "Captura la ruta de origen en carta de porte";

                if (string.IsNullOrEmpty(data.RutaDestino))
                    Message = "Captura la ruta de Destino en carta de porte";

                if (string.IsNullOrEmpty(data.NumViaje))
                    Message = "Captura el numero de viaje en carta de porte";

                if (string.IsNullOrEmpty(data.Operador))
                    Message = "Selecciona el operador en carta de porte";

                if (string.IsNullOrEmpty(data.Vehiculo))
                    Message = "Selecciona el vehículo en carta de porte";

                foreach (DataCartaPorte carta in data.lstRegistros)
                {
                    if (string.IsNullOrEmpty(carta.Bulto))
                        Message = string.Format(@"Captura el valor de bulto para ""{0}"", en carta de porte", carta.Articulo);

                    if (string.IsNullOrEmpty(carta.Carga))
                        Message = string.Format(@"Captura el valor de carga para ""{0}"", en carta de porte", carta.Articulo);

                    if (string.IsNullOrEmpty(carta.Peso))
                        Message = string.Format(@"Captura el valor de peso para ""{0}"", en carta de porte", carta.Articulo);
                }

                if (!string.IsNullOrEmpty(Message))
                {
                    //this.oGlobales.Mostrarmensaje(Message, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                    //this.SBO_Application.MessageBox(Message, 1, "Aceptar");
                    bContinuar = false;
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                //this.oGlobales.Mostrarmensaje(ex.Message, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                KananSAPHerramientas.LogError("GestionCartaPorte.cs->Validar()", ex.Message);
                bContinuar = false;
            }

            return bContinuar;
        }

        #region Consultas

        private String BuscarCarta()
        {
            String strQuery = "";

            //  if (GestionGlobales.bSqlConnection)
            //     strQuery = string.Format(@"SELECT * FROM [@VSKF_CARTADEPORTE] WHERE Code = '{0}') ", this.UIIDTemporal);
            // else
            //    strQuery = string.Format(@"SELECT * FROM ""@VSKF_CARTADEPORTE"" WHERE ""Code"" = '{0}') ", this.UIIDTemporal);

            return strQuery;
        }
        public String InsertarCarta(CartaPorteINFO data, string DocNum)
        {
            String strQuery = "";
            String sTAbla = "";
            try
            {
                if (GestionGlobales.bSqlConnection)
                    sTAbla = "[@VSKF_CARTADEPORTE]";
                else
                    sTAbla = @"""@VSKF_CARTADEPORTE""";
                strQuery = string.Format(@"INSERT INTO {6} VALUES ('{0}','{0}','{1}','{2}','{3}','{4}','{5}',{0},'{7}') ", DocNum, data.RutaOrigen.Trim(), data.RutaDestino.Trim(), data.Vehiculo.Trim(), data.Operador.Trim(), data.Comentario.Trim(), sTAbla, data.NumViaje.Trim());

            }
            catch { }

            return strQuery;
        }

        public String InsertarDetalle(int iLinea, DataCartaPorte concepto, string DocNum)
        {
            String strQuery = "";
            String sTAbla = "";

            if (GestionGlobales.bSqlConnection)
                sTAbla = "[@VSKF_CARTADETALLE]";
            else
                sTAbla = @"""@VSKF_CARTADETALLE""";
            try
            {
                strQuery = string.Format(@"INSERT INTO {8} VALUES ('{0}','{0}','{1}','{2}',{3},'{4}','{5}','{6}','{7}') ",
                    iLinea,
                    concepto.Articulo.Trim(),
                    concepto.Descripcion.Trim(),
                    concepto.Cantidad,
                    concepto.Bulto,
                    concepto.Carga,
                    concepto.Peso,
                    DocNum,
                    sTAbla);
            }
            catch { }
            return strQuery;
        }

        public String EliminaCabeceraByID(string DocNum)
        {
            String strQuery = "";
            String sTAbla = "";
            try
            {
                if (GestionGlobales.bSqlConnection)
                    sTAbla = "[@VSKF_CARTADEPORTE]";
                else
                    sTAbla = @"""@VSKF_CARTADEPORTE""";
                strQuery = string.Format(@"DELETE FROM  {1} WHERE ""U_DOCFACTURA"" = '{0}' ", DocNum,sTAbla);

            }
            catch { }

            return strQuery;
        }

        public String EliminaDetalleByID(string DocNum)
        {
            String strQuery = "";
            String sTAbla = "";

            if (GestionGlobales.bSqlConnection)
                sTAbla = "[@VSKF_CARTADETALLE]";
            else
                sTAbla = @"""@VSKF_CARTADETALLE""";
            try
            {
                strQuery = string.Format(@"DELETE FROM  {1} WHERE {2} = '{0}'", DocNum, sTAbla, GestionGlobales.bSqlConnection ? "U_IDCARTA" : @"""U_IDCARTA""");
            }
            catch { }
            return strQuery;
        }


        private String Vehiculos()
        {
            String strQuery = "";

            if (GestionGlobales.bSqlConnection)
                strQuery = string.Format(@"SELECT Code, Name FROM [@VSKF_VEHICULO] WHERE U_EsActivo = 1");
            else
                strQuery = string.Format(@"SELECT ""Code"",""Name"" FROM ""@VSKF_VEHICULO"" WHERE ""U_ESACTIVO"" = 1 ");

            return strQuery;
        }
        public String iCantidadRegistros()
        {
            int iRegistros = -1;
            String strQuery = "", sTabla = "";

            if (GestionGlobales.bSqlConnection)
                sTabla = "[@VSKF_CARTADETALLE]";
            else
                sTabla = @"""@VSKF_CARTADETALLE""";

            return strQuery = string.Format("SELECT * FROM {0} ", sTabla);
        }


        private String Operadores()
        {
            String strQuery = string.Empty;

            if (GestionGlobales.bSqlConnection)
                strQuery = "SELECT Code, Name FROM [@VSKF_OPERADOR]";
            else
                strQuery = @"SELECT ""Code"", ""Name"" FROM ""@VSKF_OPERADOR""";

            return strQuery;
        }

        #endregion
    }
}
