﻿
using Kanan.Vehiculos.BO2;
using KananSAP.Configuracion.Configuraciones.Objects;
using KananSAP.GestionUsuarios.Autentificacion;
using KananSAP.Helper;
using KananWS.Interface;
using SAPbobsCOM;
using SAPbouiCOM;
using SAPinterface.Data.INFO.Objetos;
using SAPinterface.Data.PD;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application = System.Windows.Forms.Application;


namespace KananSAP.GestionFacturaClientes
{
    public class ItenerarioView
    {
          #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private const string CONST_CLASE = "ItenerarioView.cs";
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.IChooseFromListEvent oEvtChoose;
        private DataTable oDocsBase, oTableOCRD;        
        private Grid oMatDocs;
        private XMLPerformance XmlApplication;
        private GestionGlobales oGlobales = null;
        private SAPbobsCOM.Company oCompany;
        private AutentificacionPresenter oAut = null;
        private Configurations configurations = null;
        private SBO_KF_ODLN_PD oItinerarios = null;
        private const string CONST_ENDPOINT = "PaginasSAP/CartaPorte/ItinerarioExplosivos.aspx?_modal=true";
        #endregion

        public ItenerarioView(GestionGlobales oGlobales,SAPbobsCOM.Company Company)
        {

            this.oGlobales = oGlobales;
            this.SBO_Application = this.oGlobales.SBO_Application;
            this.XmlApplication = new XMLPerformance(SBO_Application);
            //this.iConsecutivo = iSiguienteForm;
            this.oCompany = Company;
            Boolean bContinuar = LoadForm();         
            this.ShowForm();
            oAut = new  AutentificacionPresenter(this.SBO_Application, this.oCompany);
            
        }

        private Boolean LoadForm()
        {
            string UID = "GestVieI1";
            FormCreationParams oFormCreate = null;
            Boolean bContinuar = false;
            String XmlData = "";
            oItinerarios = new SBO_KF_ODLN_PD();
            try
            {

                this.oForm = SBO_Application.Forms.Item(UID);
                if (oForm.UniqueID != UID)
                    throw new Exception("");
                bContinuar = true;
            }
            catch (Exception exp)
            {
                KananSAPHerramientas.LogError("GestionConfig.cs->LoadForm()", exp.Message);
                bContinuar = false;
                try
                {
                    string sPath = Application.StartupPath;                   
                    XmlData = this.XmlApplication.LoadFromXML(sPath + "\\XML", "", "GestionDeItinerario.xml");
                    bContinuar = true;
                    oFormCreate = (FormCreationParams)this.oGlobales.SBO_Application.CreateObject(BoCreatableObjectType.cot_FormCreationParams);
                    oFormCreate.UniqueID = UID;
                    oFormCreate.FormType =   "GestVieI";                    
                    oFormCreate.XmlData = XmlData;
                    this.oForm = this.oGlobales.SBO_Application.Forms.AddEx(oFormCreate);//SBO_Application.Forms.Item(UID);
                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                }
                catch (Exception ex)
                {
                    bContinuar = false;
                    KananSAPHerramientas.LogError("GestionConfig.cs->LoadForm()", ex.Message);
                }
            }
            return bContinuar;
        }

        public void ShowForm()
        {
            try
            {

                this.oForm.Visible = true;
                //this.oForm.Select();
                /*this.InizializarItems();
                this.LlenarInterfaz(this.oGlobales.KNSettings);*/
                this.AgregarChooseFromList();
            }
            catch (Exception ex)
            {

                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;                                
                /*this.InizializarItems();
                this.LlenarInterfaz(this.oGlobales.KNSettings);*/
                this.oForm.Visible = true;
                this.AgregarChooseFromList();
                this.oForm.Select();
                KananSAPHerramientas.LogError("GestionConfig.cs->ShowForm()", ex.Message);

            }
        }
        private void AgregarChooseFromList()
        {

            try
            {
                this.oForm.DataSources.UserDataSources.Add("oUDSSN", BoDataType.dt_SHORT_TEXT, 254);
                EditText oCliente = (EditText)oForm.Items.Item("oUETClient").Specific;
                oCliente.DataBind.SetBound(true, "", "oUDSSN");
                oCliente.ChooseFromListUID = "oUCFLSocios";
                oCliente.ChooseFromListAlias = "CardCode";
                oDocsBase = this.oForm.DataSources.DataTables.Item("KFDocs");
                this.oMatDocs = (Grid)this.oForm.Items.Item("matCIte").Specific;
                this.BuscaDocumentos(true);
            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError(string.Format("{}->AgregarChooseFromList()", CONST_CLASE), ex.Message);
            }
        }


        public void EventoControl(ItemEvent pVal, out bool BubbleEvent)
        {
            #region Comentarios
            /* 
             */
            #endregion Comentarios

            BubbleEvent = true;
            try
            {

                if (!pVal.BeforeAction)
                {
                    #region After Action

                    if (pVal.ColUID == "Carta de porte PDF" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        this.LanzarInformeWEB(pVal.Row);
                        
                    
                    switch (pVal.EventType)
                    {
                        case BoEventTypes.et_ITEM_PRESSED:
                            switch (pVal.ItemUID)
                            {
                                case "btnBsca":
                                    this.BuscaDocumentos();
                                    break;
                            }
                            break;                            
                           
                        case BoEventTypes.et_CHOOSE_FROM_LIST:
                            switch (pVal.ItemUID)
                            {

                                case "oUETClient":
                                    this.oEvtChoose = ((SAPbouiCOM.IChooseFromListEvent)(pVal));
                                    if (!oEvtChoose.BeforeAction)
                                    {
                                        this.oTableOCRD = null;
                                        this.oTableOCRD = oEvtChoose.SelectedObjects;
                                        try
                                        {
                                            string sCardCode = Convert.ToString(oTableOCRD.GetValue(0, 0));
                                            this.oForm.DataSources.UserDataSources.Item("oUDSSN").ValueEx = sCardCode;
                                        }
                                        catch { }
                                    }
                                    break;

                            }
                            break;
                    }
                    #endregion After Action
                }
            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError(string.Format("{}->EventoControl()", CONST_CLASE), ex.Message);
            }
        }
        void LanzarInformeWEB(Int32 iRowindex)
        {
            if (iRowindex >= 0 && iRowindex <= this.oDocsBase.Rows.Count)
            {
                try
                {
                    SBOKF_CL_Empresa_INFO oEmpresa = new SBOKF_CL_Empresa_INFO();
                    if (this.configurations == null)
                        configurations = this.oAut.LogInCurrentSAPUserToKF();
                    string DocEntry = Convert.ToString( this.oDocsBase.GetValue(1, iRowindex));
                    SBO_KF_Itinerario_INFO oItinerario = new SBO_KF_Itinerario_INFO();
                    oEmpresa.EmpresaID = Convert.ToInt32( configurations.UserFull.Dependencia.EmpresaID);
                    string sresponse = oItinerarios.listaEncabezadoItinerario(ref oCompany, ref oItinerario, DocEntry);
                    if (sresponse == "OK")
                    {
                        List<SBOKF_CL_ItinerarioDetalle_INFO> lstDetalle = new List<SBOKF_CL_ItinerarioDetalle_INFO>();
                        sresponse = oItinerarios.listaDetalleItinerario(ref oCompany, ref lstDetalle, DocEntry);
                        if (sresponse == "OK")
                        {
                            oItinerario.lstDetalle = lstDetalle;
                            oItinerario.Empresa = oEmpresa;
                            ItinerarioWS oWSEndpoint = new ItinerarioWS();
                            SBO_KF_Itinerario_INFO oResponse = null;

                            if (!string.IsNullOrEmpty(oItinerario.ItinerarioId))
                            {
                                oResponse = new SBO_KF_Itinerario_INFO();
                                oResponse.ItinerarioId = oItinerario.ItinerarioId;
                            }
                            else
                            {
                                oResponse = oWSEndpoint.InsertaItinerario(oItinerario, out sresponse);
                                if (oResponse != null)
                                {
                                    sresponse = oItinerarios.ActualizaEntrega(ref oCompany, oItinerario.DocEntry, oResponse.ItinerarioId);
                                    if(sresponse != "OK")
                                        throw new Exception("No se ha podido actualizar el itinerario");
                                }
                                else this.SBO_Application.StatusBar.SetText(string.Format("No se ha podido enviar la información del itinerario. ERROR {0}", sresponse), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                            }

                            //lanzar link cuando el objeto no es nulo
                            if (oResponse != null)
                            {
                                string sEndPoint = string.Format("{0}{1}&ItinerarioID={2}&EntrgaID={3}&empid={4}", GestionGlobales.sURLWs, CONST_ENDPOINT, oResponse.ItinerarioId, oItinerario.sDocNum, configurations.UserFull.Dependencia.EmpresaID);
                                Process.Start(sEndPoint);
                            }
                        }
                        else this.SBO_Application.StatusBar.SetText("No se ha encontrado el detalle del itinerario", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                    }
                    else this.SBO_Application.StatusBar.SetText("No se ha encontrado el itinerario", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                }
                catch (Exception ex)
                {
                    this.SBO_Application.StatusBar.SetText(string.Format("No se ha encontrado la información del itinerario. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                }
            }
        }
        void BuscaDocumentos(bool bTop20=false)
        {
            try
            {
                oItem = this.oForm.Items.Item("oUETClient");
                EditText txtOCRD = (EditText)this.oItem.Specific;
                oItem = this.oForm.Items.Item("txtIni");
                EditText txtFinicio= (EditText)this.oItem.Specific;
                oItem = this.oForm.Items.Item("txtFin");
                EditText txtFin = (EditText)this.oItem.Specific;
                string sFechaInicio = txtFinicio.Value;
                string sFechaFin = txtFin.Value;

                this.oDocsBase.Clear();
                List<SBO_KF_Itinerario_INFO> lstItinerarios = new List<SBO_KF_Itinerario_INFO>();
                string sResponse = oItinerarios.listaItinerarioAbiertos(ref oCompany, ref lstItinerarios, "O", txtOCRD.Value, bTop20, sFechaInicio, sFechaFin);
                if (sResponse != "OK")
                {
                    this.HeaderDocuments();
                    this.oDocsBase.Rows.Add(lstItinerarios.Count);
                    for (int i = 0; i < this.oMatDocs.Columns.Count; i++)
                        oMatDocs.Columns.Item(i).Editable = i == 6;
                    SBO_Application.MessageBox("No se han encontrado resultados", 1, "OK");
                }
                else
                {
                    this.oForm.Freeze(true);
                    this.HeaderDocuments();
                    this.oDocsBase.Rows.Add(lstItinerarios.Count);
                    int iLine = 0;
                    foreach (SBO_KF_Itinerario_INFO oRecordSet in lstItinerarios)
                    {

                        oDocsBase.SetValue(0, iLine, (iLine + 1).ToString());
                        oDocsBase.SetValue(1, iLine, oRecordSet.DocEntry);
                        oDocsBase.SetValue(2, iLine, oRecordSet.CardCode);
                        oDocsBase.SetValue(3, iLine, oRecordSet.CardName);
                        oDocsBase.SetValue(4, iLine, oRecordSet.DocDate.ToString("dd/MM/yyyy"));
                        oDocsBase.SetValue(5, iLine, String.Format("{0:n} MXN", Convert.ToDecimal(oRecordSet.DocTotal)));
                        oDocsBase.SetValue(6, iLine, "");
                        oDocsBase.SetValue(7, iLine, "Visualizar");
                        iLine++;
                    }
                    this.oMatDocs.DataTable = this.oDocsBase;
                    this.oMatDocs.Columns.Item(1).Type = BoGridColumnType.gct_EditText;
                    this.oMatDocs.Columns.Item(6).Type = BoGridColumnType.gct_CheckBox;

                    EditTextColumn oEditTextColumn = (EditTextColumn)this.oMatDocs.Columns.Item(1);
                    oEditTextColumn.LinkedObjectType = "17";
                    for (int i = 0; i < this.oMatDocs.Columns.Count; i++)
                        oMatDocs.Columns.Item(i).Editable = i == 6;
                    this.oForm.Freeze(false);
                }             

            }
            catch (Exception ex)
            {
                this.oForm.Freeze(false);
                KananSAPHerramientas.LogError(string.Format("{0}->BuscaDocumentos()", CONST_CLASE), ex.Message);
            }

        }
        void HeaderDocuments()
        {
            this.oDocsBase.Columns.Add("#", BoFieldsType.ft_Integer, 150);
            this.oDocsBase.Columns.Add("No Documento", BoFieldsType.ft_Integer, 100);
            this.oDocsBase.Columns.Add("Codigo cliente", BoFieldsType.ft_Text, 105);
            this.oDocsBase.Columns.Add("Nombre Cliente", BoFieldsType.ft_Text, 180);
            this.oDocsBase.Columns.Add("Fecha", BoFieldsType.ft_Text, 80);
            this.oDocsBase.Columns.Add("Total", BoFieldsType.ft_Text, 80);
            this.oDocsBase.Columns.Add("Visualizar con IVA", BoFieldsType.ft_Text, 50);
            //this.oDocsBase.Columns.Add("Formato 560", BoFieldsType.ft_Text, 80);
            this.oDocsBase.Columns.Add("Carta de porte PDF", BoFieldsType.ft_Text, 80);
        }

    }
}
