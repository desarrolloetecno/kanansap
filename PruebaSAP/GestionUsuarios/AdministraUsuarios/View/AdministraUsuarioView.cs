﻿using System;
using SAPbouiCOM;
using SAPinterface.Data;
using Application = System.Windows.Forms.Application;

namespace KananSAP.GestionUsuarios.AdministraUsuarios
{
    public class AdministraUsuarioView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.StaticText oStaticText;
        private SAPbouiCOM.LinkedButton oLinkedButton;
        private SAPbouiCOM.Matrix oMtx;
        private SAPbouiCOM.DBDataSource oDBDataSource;
        private SAPbouiCOM.Columns oColumns;
        private SAPbouiCOM.Column oColumn;
        private SAPbouiCOM.Button oButton;
        private SAPbouiCOM.Conditions oConditions;
        private SAPbouiCOM.Condition oCondition;
        public UsuarioSAP Entity { get; set; }
        public string FormUniqueID { get; set; }
        private XMLPerformance XmlApplication;
        public int? id;
        #endregion

        #region Constructor
        public AdministraUsuarioView(SAPbouiCOM.Application Application, SAPbobsCOM.Company company)
        {
            this.SBO_Application = Application;
            this.Entity = new UsuarioSAP(company);
            this.XmlApplication = new XMLPerformance(SBO_Application);
            this.LoadForm();
            this.oItem = null;
            this.oLinkedButton = null;
            this.oStaticText = null;
            this.oEditText = null;
            this.oConditions = null;
            this.oCondition = null;
            this.oColumns = null;
            this.oColumn = null;
            this.id = null;
        }
        #endregion

        #region Metodos
        #region Metodos Form
        public void LoadForm()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("UsersForm");
                BindDataToForm();
            }
            catch
            {
                string sPath = Application.StartupPath;
                this.XmlApplication.LoadFromXML(sPath + "\\XML","GestionUsuariosKF.xml");
                
                this.oForm = SBO_Application.Forms.Item("UsersForm");
                BindDataToForm();

                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width)/2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height)/2;
            }
        }

        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
                BindDataToForm();
            }
            catch (Exception)
            {
                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width)/2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height)/2;
                this.oForm.Visible = true;
                BindDataToForm();
                this.oForm.Select();
            }
        }

        #endregion
        #region Metodos Matrix
        public void BindDataToForm()
        {
            //oForm.DataSources.DataTables.Item(0)
            //    .ExecuteQuery("select empID, firstName, email FROM OHEM");

            //"OHEM" es la tabla de empleados perteneciente a SAP.
            oDBDataSource = oForm.DataSources.DBDataSources.Item("OHEM");
            oDBDataSource.Query(oConditions ?? null);
            oItem = oForm.Items.Item("MtxEmpl");
            oMtx = (SAPbouiCOM.Matrix) (oItem.Specific);
            oMtx.LoadFromDataSource();
        }

        public string GetDataRow(int index)
        {
            try
            {
                oItem = oForm.Items.Item("MtxEmpl");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(1, index);
                return oEditText.String;
            }
            catch (Exception ex)
            {
                return "Error en la aplicacion";
            }
        }

        #endregion

        public void SetID(int index)
        {
            try
            {
                oItem = oForm.Items.Item("MtxEmpl");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(1, index);
                //this.oItem = this.oForm.Items.Item("txtempl");
                //this.oEditText = (SAPbouiCOM.EditText)(this.oItem.Specific);
                this.id = Convert.ToInt32(oEditText.String.Trim());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        
        public void SetConditions()
        {
            try
            {
                oConditions = new SAPbouiCOM.Conditions();
                oItem = oForm.Items.Item("txtempl");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (!string.IsNullOrEmpty(oEditText.String))
                {
                    oCondition = oConditions.Add();
                    oCondition.Alias = "firstName";
                    oCondition.Operation = BoConditionOperation.co_CONTAIN;
                    oCondition.CondVal = oEditText.String.Trim();
                    oCondition.Relationship = BoConditionRelationship.cr_OR;

                    oCondition = oConditions.Add();
                    oCondition.Alias = "lastName";
                    oCondition.Operation = BoConditionOperation.co_CONTAIN;
                    oCondition.CondVal = oEditText.String.Trim();
                }
                else
                {
                    oConditions = null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion
    }
}
