﻿using System;
using System.Collections.Generic;
using System.Linq;
using Etecno.Security2.BO;
using Kanan.Comun.BO2;
using Kanan.Operaciones.BO2;
using KananSAP.Operaciones.Data;
using KananSAP.Vehiculos.Data;
using KananWS.Interface;
using SAPbouiCOM;
using SAPinterface.Data;
using Application = System.Windows.Forms.Application;
using VehiculoSAP = KananSAP.Vehiculos.Data.VehiculoSAP;
using KananSAP.GestionDatos.MigradorDatos.Librerias;
using System.Threading;
using SAPinterface.Data.INFO.Objetos;
using SAPinterface.Data.PD;
using System.IO;
using System.Diagnostics;

//using VehiculoSAP = SAPinterface.Data.VehiculoSAP;

namespace KananSAP.GestionUsuarios.AdministraUsuarios
{
    public class UsuarioView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.ComboBox oComboBox;
        private SAPbouiCOM.StaticText oStaticText;
        private SAPbouiCOM.Grid oGrid;
        private string pass;
        private string pass2;
        public UsuarioSAP Entity { get; set; }
        public OperadorSAP EntityOperador { get; set; }
        public OperadorVehiculoSAP Asignacion { get; set; }
        public VehiculoSAP EntityVehicle { get; set; }
        public LicenciaSAP ELicencia { get; set; }
        public SBO_KF_INFRACCION_INFO oInfraccion { get; set; }
        public CursoSAP ECurso { get; set; }
        public ObservacionSAP EObservacion { get; set; }
        public string FormUniqueID { get; set; }
        private XMLPerformance XmlApplication;
        public Configurations configs;
        public SucursalSAP scData;

        public List<Licencia> listlicencia;
        public List<Curso> listcurso;
        public List<Observacion> listobservacion;
        public List<Licencia> dellicencia;
        public List<Curso> delcurso;
        public List<Observacion> delobservacion;
        public List<Licencia> edtlicencia;
        public List<Curso> edtcurso;
        public List<Observacion> edtobservacion;
        public List<SBO_KF_INFRACCION_INFO> listInfraccionesIns;
        public List<SBO_KF_INFRACCION_INFO> listInfracciones;
        public List<SBO_KF_INFRACCION_INFO> listInfraccionesDel;

        public int? LicRowIndex { get; set; }
        public int? CurRowIndex { get; set; }
        public int? ObsRowIndex { get; set; }
        public int? InfracRowIndex { get; set; }
        public string sPathOperadores { get; set; }
        private string sPathImgDefault { get; set; }
        private string sPathTemp { get; set; }
        private string sOldImage { get; set; }
        private string sNewImage { get; set; }

        private String sPathInfraccion { get; set; } //JBASTO20200716

        private SAPbobsCOM.Company oCompany;
        #endregion

        #region Constructor
        public UsuarioView(SAPbouiCOM.Application Application, SAPbobsCOM.Company company, Configurations c)
        {
            Add oConnfigAddInfrac = new Add(ref company, null);//JBASTO20200716
            try//JBASTO20200716
            {
                sPathInfraccion = string.Format(@"{0}KFInfraccion", oConnfigAddInfrac.pathInfraccion());//JBASTO20200716
                if (!Directory.Exists(sPathInfraccion))//JBASTO20200716
                    Directory.CreateDirectory(sPathInfraccion);//JBASTO20200716
            }
            catch { }//JBASTO20200716
            this.SBO_Application = Application;
            this.Entity = new UsuarioSAP(company);
            this.EntityVehicle = new VehiculoSAP(company);
            this.EntityOperador = new OperadorSAP(company);
            this.Asignacion = new OperadorVehiculoSAP(company);
            this.ELicencia = new LicenciaSAP(company);
            this.ECurso = new CursoSAP(company);
            this.oInfraccion = new SBO_KF_INFRACCION_INFO();
            this.EObservacion = new ObservacionSAP(company);
            this.XmlApplication = new XMLPerformance(SBO_Application);
            this.configs = c;
            this.oItem = null;
            this.oEditText = null;
            this.oComboBox = null;
            this.oStaticText = null;
            this.oForm = null;
            this.pass = string.Empty;
            this.pass2 = string.Empty;
            this.scData = new SucursalSAP(company);
            this.listlicencia = new List<Licencia>();
            this.listcurso = new List<Curso>();
            this.listobservacion = new List<Observacion>();
            this.dellicencia = new List<Licencia>();
            this.delcurso = new List<Curso>();
            this.delobservacion = new List<Observacion>();
            this.edtlicencia = new List<Licencia>();
            this.edtcurso = new List<Curso>();
            this.edtobservacion = new List<Observacion>();
            this.oCompany = company;
            Add oConnfigAdd = new Add(ref company, this.configs);
            sPathOperadores = string.Format(@"{0}KFOperadores", oConnfigAdd.pathOperadores());
            sPathImgDefault = string.Format(@"{0}KF_Default.png", oConnfigAdd.pathOperadores());
            sPathInfraccion = string.Format(@"{0}KFInfraccion", oConnfigAdd.pathInfraccion());
        }
        #endregion

        #region Metodos
        #region Metodos Form
        public void LoadForm()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("KFUser");

                FillVehicles();
                FillSucursal();
                
            }
            catch (Exception ex)
            {
                if (oForm == null || ex.Message.Contains("Invalid Form"))
                {
                    string sPath = Application.StartupPath;
                    this.XmlApplication.LoadFromXML(sPath + "\\XML", "oUsuarioKF.xml");
                    
                    this.oForm = SBO_Application.Forms.Item("KFUser");

                    //oItem = this.oForm.Items.Item("txtdirecc");
                    //oItem.Visible = false;
                    //oItem = this.oForm.Items.Item("Item_8");
                    //oItem.Visible = false;
                    oGrid = this.oForm.Items.Item("grdobsrv").Specific;
                    oGrid.Columns.Item("colobsid").Visible = false;

                    oGrid = this.oForm.Items.Item("grdcurs").Specific;
                    oGrid.Columns.Item("colid").Visible = false;

                    oGrid = this.oForm.Items.Item("grdlic").Specific;
                    oGrid.Columns.Item("colid").Visible = false;

                    oItem = this.oForm.Items.Item("btnelimina");
                    oItem.Visible = false;

                    FillVehicles();
                    FillVehiclesInfrac();
                    FillTiposInfraccion(); 
                    FillSucursal();
                    this.oForm.PaneLevel = 1;

                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width)/2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height)/2;
                }
                else
                {
                    SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                }
            }
        }

        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
                ChooseListCardCode();
            }
            catch (Exception)
            {
                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width)/2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height)/2;
                this.oForm.Visible = true;
                this.oForm.Select();
                ChooseListCardCode();
            }
        }

        public void CloseForm()
        {
            if (this.oForm.Visible & this.oForm.Selected)
                this.oForm.Close();
        }

        public void ChangeFolder(int panelview)
        {
            this.oForm.PaneLevel = panelview;
        }

        private void ChooseListCardCode()
        {

            try
            {

                Item oItem = null;
                oForm.DataSources.UserDataSources.Add("ChoCRD", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);
                SAPbouiCOM.ChooseFromListCollection oCFLs = null;
                oCFLs = oForm.ChooseFromLists;
                SAPbouiCOM.ChooseFromList oCFL = null;
                SAPbouiCOM.ChooseFromListCreationParams oCFLCreationParams = null;
                oCFLCreationParams = ((SAPbouiCOM.ChooseFromListCreationParams)(SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)));
                oCFLCreationParams.MultiSelection = false;
                oCFLCreationParams.ObjectType = "2";
                oCFLCreationParams.UniqueID = "CLOCRD";
                oCFL = oCFLs.Add(oCFLCreationParams);
                //SAPbouiCOM.Conditions oConditions = this.SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_Conditions);
                //oConditions = oCFL.GetConditions();
                //SAPbouiCOM.Condition oCondition = oConditions.Add();
                //oCondition.Alias = "CardType";
                //oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
                //oCondition.CondVal = "C";
                //oCFL.SetConditions(oConditions);
                oItem = oForm.Items.Item("txtOCrd");
                EditText oEditText = (EditText)(oItem.Specific);
                oEditText.DataBind.SetBound(true, "", "ChoCRD");
                oEditText.ChooseFromListUID = "CLOCRD";
                oEditText.ChooseFromListAlias = "CardCode";
            }
            catch (Exception ex)
            {
               // KananSAPHerramientas.LogError(string.Format("{}->AgregarChooseFromList()", CONST_CLASE), ex.Message);
            }
        }

        public void SetLicRowIndex(int rowIndex)
        {
            try
            {
                this.LicRowIndex = rowIndex;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void SetCurRowIndex(int rowIndex)
        {
            try
            {
                this.CurRowIndex = rowIndex;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        public void SetInfrRowIndex(int rowIndex)
        {
            try
            {
                this.InfracRowIndex = rowIndex;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        public void SetObsRowIndex(int rowIndex)
        {
            try
            {
                this.ObsRowIndex = rowIndex;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        public void FillCardCode(string CardCode)
        {
            oItem = oForm.Items.Item("txtOCrd");
            oEditText = (EditText)(oItem.Specific);
            oEditText.Value = CardCode;
        }
        private void FillSucursal()
        {
            var rs = this.scData.Consultar(new Sucursal());
            oComboBox = oForm.Items.Item("cmbxSucsal").Specific;
            this.CleanComboBox(oComboBox);
            oComboBox.ValidValues.Add("0", "--None--");
            rs.MoveFirst();
            for(int i = 0; i< rs.RecordCount; i++)
            {
                string nombre = rs.Fields.Item("U_Direccion").Value.ToString();
                string value = (string)Convert.ChangeType(rs.Fields.Item("U_SucursalID").Value, typeof(string));
                oComboBox.ValidValues.Add(value, nombre);
                rs.MoveNext();
            }
            oComboBox.Item.DisplayDesc = true;
        }
        public void FillVehicles()
        {
            try
            {
                int? iIDSuc = 0;
                if (this.Entity.oEmpleado.EmpleadoSucursal != null)
                    iIDSuc = this.Entity.oEmpleado.EmpleadoSucursal.SucursalID;
                if (iIDSuc == null) iIDSuc = 0;

                //var rs = this.EntityVehicle.GetAllVehicles(iIDSuc);

                var rs = this.EntityVehicle.ObtenerVehiculosRegistrados(iIDSuc, this.Entity.oEmpleado.EmpleadoID);
                rs.MoveFirst();

                oItem = this.oForm.Items.Item("cmbvehi");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                CleanComboBox(oComboBox);
                oComboBox.ValidValues.Add("0", "Sin asignar");

                for (int i = 0; i < rs.RecordCount; i++)
                {
                    oComboBox.ValidValues.Add(rs.Fields.Item("U_VehiculoID").Value.ToString(), rs.Fields.Item("U_Nombre").Value.ToString() + " - " + rs.Fields.Item("U_Modelo").Value.ToString() +
                        " - " + rs.Fields.Item("U_Marca").Value.ToString() + " - " + rs.Fields.Item("U_Anio").Value.ToString());
                    rs.MoveNext();
                }
            }
            catch { }
        }

        public void FillVehiclesInfrac()
        {
            try
            {
                int? iIDSuc = 0;
                if (this.Entity.oEmpleado.EmpleadoSucursal != null)
                    iIDSuc = this.Entity.oEmpleado.EmpleadoSucursal.SucursalID;
                if (iIDSuc == null) iIDSuc = 0;

                //var rs = this.EntityVehicle.GetAllVehicles(iIDSuc);

                var rs = this.EntityVehicle.ObtenerVehiculosRegistrados(iIDSuc, this.Entity.oEmpleado.EmpleadoID);
                rs.MoveFirst();

                oItem = this.oForm.Items.Item("cmbNEU");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                CleanComboBox(oComboBox);
                oComboBox.ValidValues.Add("0", "Sin asignar");

                for (int i = 0; i < rs.RecordCount; i++)
                {
                    oComboBox.ValidValues.Add(rs.Fields.Item("U_VehiculoID").Value.ToString(), rs.Fields.Item("U_Nombre").Value.ToString() + " - " + rs.Fields.Item("U_Modelo").Value.ToString() +
                        " - " + rs.Fields.Item("U_Marca").Value.ToString() + " - " + rs.Fields.Item("U_Anio").Value.ToString());
                    rs.MoveNext();
                }
            }
            catch { }
        }

        public void FillTiposInfraccion()
        {
            try
            {
                SBO_KF_TIPOSINFRACCION_PD ctrTipos = new SBO_KF_TIPOSINFRACCION_PD();
                SBO_KF_TIPOSINFRACCION_INFO oTipo = new SBO_KF_TIPOSINFRACCION_INFO();
                List<SBO_KF_TIPOSINFRACCION_INFO> tiposInfraccion = new List<SBO_KF_TIPOSINFRACCION_INFO>();

                ctrTipos.listaTiposInfracciones(ref this.oCompany, ref tiposInfraccion, oTipo, false);


               

                oItem = this.oForm.Items.Item("cmbTIn");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                CleanComboBox(oComboBox);
                oComboBox.ValidValues.Add("0", "Sin asignar");

                for (int i = 0; i < tiposInfraccion.Count; i++)
                {
                    oComboBox.ValidValues.Add(tiposInfraccion[i].U_TIPOSINFRACCIONID.ToString(), tiposInfraccion[i].U_NOMBRE );
                     
                }
            }
            catch { }
        }

        private void CleanComboBox(ComboBox combo)
        {
            if (combo == null || combo.ValidValues == null || combo.ValidValues.Count <= 0) return;
            var f = combo.ValidValues.Count;
            for (int i = 0; i < f; i++)
            {
                combo.ValidValues.Remove(i, BoSearchKey.psk_Index);
                f--;
                i--;
            }
        }

        public void HideDeleteButton()
        {
            oItem = oForm.Items.Item("btnelimina");
            oItem.Visible = false;
            Item hided = oItem;

            /*Comentado para mantener posición default*/
            /*oItem = oForm.Items.Item("2");
            oItem.Left = hided.Left;*/
        }

        public void ShowDeleteButton()
        {
            oItem = oForm.Items.Item("btnelimina");
            oItem.Visible = true;
            Item hided = oItem;

            /*Comentado para mantener posición defacult*/
            /*oItem = oForm.Items.Item("2");
            oItem.Left = hided.Left + hided.Width + 3;*/
        }

        #endregion

        #region Entity
        public void FormToEntity()
        {
            if (this.EntityOperador.operador == null)
                this.EntityOperador.operador = new Operador();

            oItem = this.oForm.Items.Item("cmbvehi");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            this.EntityVehicle.ItemCode = oComboBox.Selected == null ? null : oComboBox.Selected.Value.ToString();

            oItem = this.oForm.Items.Item("cmbxSucsal");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            this.Entity.oEmpleado.EmpleadoSucursal.SucursalID = oComboBox.Selected == null ? null : (int?)Convert.ChangeType(oComboBox.Selected.Value.ToString(), typeof(int));

            oItem = this.oForm.Items.Item("txtnombre");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.oEmpleado.Nombre = oEditText.String == string.Empty ? null : oEditText.String;

            oItem = this.oForm.Items.Item("txtpaterno");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.oEmpleado.Apellido1 = oEditText.String == string.Empty ? null : oEditText.String;

            oItem = this.oForm.Items.Item("txtmaterno");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.oEmpleado.Apellido2 = oEditText.String == string.Empty ? null : oEditText.String;

            oItem = this.oForm.Items.Item("txttel");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.oEmpleado.Telefono = oEditText.String == string.Empty ? null : oEditText.String;

            oItem = this.oForm.Items.Item("txtcel");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.oEmpleado.Celular = oEditText.String == string.Empty ? null : oEditText.String;

            oItem = this.oForm.Items.Item("txtdirec");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.oEmpleado.Direccion = oEditText.String == string.Empty ? null : oEditText.String;

            oItem = this.oForm.Items.Item("txtalias");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.EntityOperador.operador.Alias = oEditText.String == string.Empty ? null : oEditText.String;

            oItem = this.oForm.Items.Item("txtcurp");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.EntityOperador.operador.Curp = oEditText.String == string.Empty ? null : oEditText.String;

            oItem = this.oForm.Items.Item("txtlugnac");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.EntityOperador.operador.LugarNacimiento = oEditText.String == string.Empty ? null : oEditText.String;

            oItem = this.oForm.Items.Item("txtcalerta");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.EntityOperador.operador.CorreosNotificacion = oEditText.String == string.Empty ? null : oEditText.String;

            oItem = this.oForm.Items.Item("txFijo");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.EntityOperador.operador.FondoFijo = oEditText.String == string.Empty ? (Double?)null : Convert.ToDouble(oEditText.String);

            oItem = this.oForm.Items.Item("txtOCrd");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.EntityOperador.operador.sCardCode = oEditText.String == string.Empty ? "" : Convert.ToString(oEditText.String);

            try
            {
                oItem = this.oForm.Items.Item("txtfecnac");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.EntityOperador.operador.FechaNacimiento = String.IsNullOrEmpty(oEditText.String) ? (DateTime?)null : DateTime.Parse(oEditText.String);
            }
            catch (Exception)
            {
                this.EntityOperador.operador.FechaNacimiento = null;
            }
            ///
            if (!string.IsNullOrEmpty(sNewImage))
            {
                try
                {
                    if (!System.IO.Directory.Exists(sPathOperadores))
                        System.IO.Directory.CreateDirectory(sPathOperadores);
                    string sOldFile = string.Format(@"{0}\{1}", sPathOperadores, sOldImage);
                    if (System.IO.File.Exists(sOldFile))
                        System.IO.File.Delete(sOldFile);
                    string sTempFile = string.Format(@"{0}\{1}", sPathTemp, sNewImage);
                    string sDestinoFile = string.Format(@"{0}\{1}", sPathOperadores, sNewImage);
                    System.IO.File.Move(sTempFile, sDestinoFile);
                    this.EntityOperador.operador.sCadenaImagen = sNewImage;
                }
                catch { }
            }
            this.Entity.oEmpleado.Configs = new Dictionary<string, string>
                {
                    {"TimeZone", "America/Mexico_City"},
                    {"Lang", "es-MX"}
                };

            this.Entity.oEmpleado.Dependencia = new Empresa { EmpresaID = this.configs.UserFull.Dependencia.EmpresaID };
            if (this.Entity.oEmpleado.Usuario == null)
            {
                this.Entity.oEmpleado.Usuario = new Usuario { Estado = EstadoUsuario.Active, Nombre = this.Entity.oEmpleado.Email, Clave = pass };
            }
            else
            {
                this.Entity.oEmpleado.Usuario.Estado = EstadoUsuario.Active;
                this.Entity.oEmpleado.Usuario.Nombre = this.Entity.oEmpleado.Email;
                this.Entity.oEmpleado.Usuario.Clave = pass;
            }
            this.Entity.oEmpleado.volumen = new UnidadVolumen { UnidadVolumenID = 1 };
            this.Entity.oEmpleado.longitud = new UnidadLongitud { UnidadLongitudID = 1 };
            this.Entity.oEmpleado.Tipo = TipoEmpleado.GENERICO;
        }

        public void EmptyEntityToForm()
        {
            oItem = this.oForm.Items.Item("cmbxSucsal"); 
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index);

            oItem = this.oForm.Items.Item("txtnombre");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = this.Entity.oEmployee.FirstName + " " + this.Entity.oEmployee.MiddleName;

            oItem = this.oForm.Items.Item("txtpaterno");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = this.Entity.oEmployee.LastName;

            oItem = this.oForm.Items.Item("txtmaterno");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txttel");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = this.Entity.oEmployee.HomePhone;

            oItem = this.oForm.Items.Item("txtcel");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = this.Entity.oEmployee.MobilePhone;

            oItem = this.oForm.Items.Item("txtdirec");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtalias");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtcurp");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtfecnac");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtlugnac");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtcalerta");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;
        }

        public void EntityToForm()
        {
            try
            {
                //oItem = this.oForm.Items.Item("cmbvehi");
                //oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                //if (!string.IsNullOrEmpty(this.EntityVehicle.oUDT.Code.Trim()))
                //{
                //    this.EntityVehicle.oUDT.GetByKey(this.EntityVehicle.oUDT.Code);
                //    var id = this.EntityVehicle.oUDT.UserFields.Fields.Item("U_VehiculoID").ToString();
                //    oComboBox.Select(this.EntityVehicle.oUDT.UserFields.Fields.Item("U_VehiculoID").ToString(), BoSearchKey.psk_ByValue); 
                //}

                oItem = this.oForm.Items.Item("cmbxSucsal");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                if (this.EntityOperador.operador.SubPropietario.SucursalID.HasValue)
                    oComboBox.Select(this.EntityOperador.operador.SubPropietario.SucursalID.ToString(), BoSearchKey.psk_ByValue);

                oItem = this.oForm.Items.Item("txtnombre");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.oEmpleado.Nombre;

                oItem = this.oForm.Items.Item("txtpaterno");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.oEmpleado.Apellido1;

                oItem = this.oForm.Items.Item("txtmaterno");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.oEmpleado.Apellido2;

                oItem = this.oForm.Items.Item("txttel");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.oEmpleado.Telefono;

                oItem = this.oForm.Items.Item("txtcel");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.oEmpleado.Celular;

                oItem = this.oForm.Items.Item("txtdirec");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.oEmpleado.Direccion;

                oItem = this.oForm.Items.Item("txtalias");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.EntityOperador.operador.Alias;

                oItem = this.oForm.Items.Item("txtcurp");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.EntityOperador.operador.Curp;

                oItem = this.oForm.Items.Item("txtlugnac");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.EntityOperador.operador.LugarNacimiento;

                oItem = this.oForm.Items.Item("txtcalerta");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.EntityOperador.operador.CorreosNotificacion;

                oItem = this.oForm.Items.Item("txFijo");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = Convert.ToDouble(this.EntityOperador.operador.FondoFijo).ToString();

                oItem = this.oForm.Items.Item("txtOCrd");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.EntityOperador.operador.sCardCode;

                oItem = this.oForm.Items.Item("txtfecnac");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.EntityOperador.operador.FechaNacimiento != null ?
                    this.EntityOperador.operador.FechaNacimiento.Value.ToString("dd-MM-yyyy") : string.Empty;

                oItem = oForm.Items.Item("imgPicture");
                SAPbouiCOM.PictureBox oPicbox = (SAPbouiCOM.PictureBox)(oItem.Specific);
                if (!string.IsNullOrEmpty(this.EntityOperador.operador.sCadenaImagen))
                {
                    sOldImage = this.EntityOperador.operador.sCadenaImagen;
                    oPicbox.Picture = string.Format(@"{0}\{1}", sPathOperadores, sOldImage);
                }
                else
                    oPicbox.Picture = sPathImgDefault;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void UploadPhoto()
        {
            string path = "";
            try
            {
                try
                {
                    if (!System.IO.Directory.Exists(sPathOperadores))
                        System.IO.Directory.CreateDirectory(sPathOperadores);
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("No se ha podido acceder al repositorio de imagenes. ERROR {0}", ex.Message));
                }


                Thread t = new Thread((ThreadStart)delegate
                {
                    System.Windows.Forms.FileDialog objDialog = new System.Windows.Forms.OpenFileDialog();
                    objDialog.Filter = "Image files (*.jpg, *.jpeg, *.png) | *.jpg; *.jpeg; *.png";
                    System.Windows.Forms.Form g = new System.Windows.Forms.Form();
                    g.Width = 1;
                    g.Height = 1;
                    g.Activate();
                    g.BringToFront();
                    g.Visible = true;
                    g.TopMost = true;
                    g.Focus();

                    System.Windows.Forms.DialogResult objResult = objDialog.ShowDialog(g);
                    Thread.Sleep(100);
                    if (objResult == System.Windows.Forms.DialogResult.OK)
                    {
                        path = objDialog.FileName;
                    }
                })
                {
                    IsBackground = false,
                    Priority = ThreadPriority.Highest
                };
                t.SetApartmentState(ApartmentState.STA);
                t.Start();
                while (!t.IsAlive) ;
                Thread.Sleep(1);
                t.Join();
                if (!String.IsNullOrEmpty(path))
                {
                    System.IO.FileInfo oFile = new System.IO.FileInfo(path);
                    sPathTemp = string.Format(@"{0}\temp", sPathOperadores);
                    sNewImage = string.Format("{0}_{1}",DateTime.Now.ToString("ddMMyyyyHHmmss"), oFile.Name);
                    if (!System.IO.Directory.Exists(sPathTemp))
                        System.IO.Directory.CreateDirectory(sPathTemp);
                    string sFiletemp = string.Format(@"{0}\{1}", sPathTemp, sNewImage );
                    if (System.IO.File.Exists(sFiletemp))
                        System.IO.File.Delete(sFiletemp);
                    System.IO.File.Copy(oFile.FullName, sFiletemp);
                    oItem = oForm.Items.Item("imgPicture");
                    SAPbouiCOM.PictureBox oPicbox = (SAPbouiCOM.PictureBox)(oItem.Specific);
                    oPicbox.Picture = @sFiletemp;
                }
            }
            catch (Exception ex)
            {
                SBO_Application.MessageBox(string.Format(@"No se ha podido cargar la imagen. ERROR {0}", ex.Message), 1, "OK");
            }
            
        }

        public void ValidateData()
        {
            this.FormToEntity();

            if (string.IsNullOrEmpty(this.Entity.oEmpleado.Nombre))
            {
                throw new Exception("El valor 'Nombre' es obligatorio");
            }
            if (string.IsNullOrEmpty(this.Entity.oEmpleado.Apellido1))
            {
                throw new Exception("El valor 'Apellido Paterno' es obligatorio");
            }
            if (string.IsNullOrEmpty(this.Entity.oEmpleado.Email))
            {
                throw new Exception("El valor 'Email' es obligatorio");
            }
            if (string.IsNullOrEmpty(this.Entity.oEmpleado.Celular))
            {
                throw new Exception("El valor 'Celular' es obligatorio");
            }
            if (string.IsNullOrEmpty(this.pass))
            {
                throw new Exception("El valor 'Contraseña' es obligatorio");
            }
            if (string.IsNullOrEmpty(this.pass2))
            {
                throw new Exception("El valor 'Confirmar Contraseña' es obligatorio");
            }
            if (pass.Trim() != pass2)
            {
                throw new Exception("Las contraseñas proporcionadas no coinciden");
            }
            if (pass.Length < 8)
            {
                throw new Exception("La Contraseña debe tener una longitud mínima de 8 caracteres");
            }
            string passIntegrity = this.ValidatePass(this.pass);
            if (!string.IsNullOrEmpty(passIntegrity))
            {
                throw new Exception(passIntegrity);
            }
        }
        #endregion

        #region User
        public void FormToUser()
        {
            oItem = this.oForm.Items.Item("txtemail");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.oEmpleado.Email = oEditText.String == string.Empty ? null : oEditText.String;

            oItem = this.oForm.Items.Item("txtpass");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.pass = oEditText.String;

            oItem = this.oForm.Items.Item("txtpass2");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.pass2 = oEditText.String;
        }

        public void EmptyUserToForm()
        {
            oItem = this.oForm.Items.Item("txtemail");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = this.Entity.oEmployee.eMail;

            oItem = this.oForm.Items.Item("txtpass");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtpass2");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;
        }

        public void UserToForm()
        {
            try
            {
                oItem = this.oForm.Items.Item("txtemail");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.oEmpleado.Email;

                oItem = this.oForm.Items.Item("txtpass");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.oEmpleado.Usuario.Clave;

                oItem = this.oForm.Items.Item("txtpass2");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.oEmpleado.Usuario.Clave;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ValidateUserData()
        {
            this.FormToUser();

            if (string.IsNullOrEmpty(this.Entity.oEmpleado.Email))
            {
                throw new Exception("El valor 'Email' es obligatorio");
            }
            if (string.IsNullOrEmpty(this.pass))
            {
                throw new Exception("El valor 'Contraseña' es obligatorio");
            }
            if (string.IsNullOrEmpty(this.pass2))
            {
                throw new Exception("El valor 'Confirmar Contraseña' es obligatorio");
            }
            if (pass.Trim() != pass2.Trim())
            {
                throw new Exception("Las contraseñas proporcionadas no coinciden");
            }
            if (pass.Length < 8)
            {
                throw new Exception("La Contraseña debe tener una longitud mínima de 8 caracteres");
            }
            string passIntegrity = this.ValidatePass(this.pass);
            if (!string.IsNullOrEmpty(passIntegrity))
            {
                throw new Exception(passIntegrity);
            }
        }
        #endregion

        #region Licencias
        public void FormToLicencia()
        {
            oItem = this.oForm.Items.Item("txtlicid");
            oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            this.ELicencia.licencia.LicenciaID = oStaticText.Caption == string.Empty ? null : (int?)Convert.ToInt32(oStaticText.Caption);

            try
            {
                oItem = this.oForm.Items.Item("cmbtipo");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                this.ELicencia.licencia.ClaseLicencia = oComboBox.Selected == null ? null : (TipoLicencia?)(int)Convert.ChangeType(oComboBox.Selected.Value.ToString(), typeof(int));
            }
            catch (Exception)
            {
                this.ELicencia.licencia.ClaseLicencia = null;
            }

            try
            {
                oItem = this.oForm.Items.Item("txtvig");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.ELicencia.licencia.VigenciaLicencia = String.IsNullOrEmpty(oEditText.String) ? (DateTime?)null : DateTime.Parse(oEditText.String);
            }
            catch (Exception)
            {
                this.ELicencia.licencia.VigenciaLicencia = null;
            }

            oItem = this.oForm.Items.Item("txtnum");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.ELicencia.licencia.Numero = oEditText.String == string.Empty ? null : oEditText.String;

            try
            {
                oItem = this.oForm.Items.Item("txtaviso");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.ELicencia.licencia.AvisoLicencia = String.IsNullOrEmpty(oEditText.String) ? (int?)null : (Convert.ToInt32(oEditText.String));
            }
            catch (Exception)
            {
                this.ELicencia.licencia.AvisoLicencia = null;
            }
        }

        public void EmptyLicenciaToForm()
        {
            oItem = this.oForm.Items.Item("txtlicid");
            oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            oStaticText.Caption = string.Empty;

            oItem = this.oForm.Items.Item("cmbtipo");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index);

            oItem = this.oForm.Items.Item("txtvig");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtnum");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtaviso");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;
        }

        public void LicenciaToForm()
        {
            try
            {
                oItem = this.oForm.Items.Item("txtlicid");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                oStaticText.Caption = this.ELicencia.licencia.LicenciaID == null ? string.Empty : this.ELicencia.licencia.LicenciaID.ToString();

                oItem = this.oForm.Items.Item("cmbtipo");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                if (this.ELicencia.licencia.ClaseLicencia.HasValue)
                    oComboBox.Select(this.ELicencia.licencia.ClaseLicencia.ToString(), BoSearchKey.psk_ByValue);

                oItem = this.oForm.Items.Item("txtnum");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.ELicencia.licencia.Numero;

                oItem = this.oForm.Items.Item("txtvig");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.ELicencia.licencia.VigenciaLicencia != null ? this.ELicencia.licencia.VigenciaLicencia.Value.ToShortDateString() : string.Empty;

                oItem = this.oForm.Items.Item("txtaviso");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = ELicencia.licencia.AvisoLicencia != null ? ELicencia.licencia.AvisoLicencia.ToString() : string.Empty;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ValidateLicenciaData()
        {
            this.FormToLicencia();

            if (string.IsNullOrEmpty(this.ELicencia.licencia.Numero.Trim()))
            {
                throw new Exception("El valor 'Número de Licencia' es obligatorio");
            }
            if (!ELicencia.licencia.ClaseLicencia.HasValue)
            {
                throw new Exception("El valor 'Tipo de Licencia' es obligatorio");
            }
            if (ELicencia.licencia.VigenciaLicencia == null)
            {
                throw new Exception("El valor 'Vigencia de Licencia' es obligatorio");
            }
            if (ELicencia.licencia.AvisoLicencia == null || ELicencia.licencia.AvisoLicencia <= 0)
            {
                throw new Exception("El valor 'Días para aviso' es obligatorio");
            }
        }

        public void NewLicenciaToGrid()
        {
            try
            {
                this.ValidateLicenciaData();
                if (this.listlicencia == null) listlicencia = new List<Licencia>();
                listlicencia.Add((Licencia)ELicencia.licencia.Clone());
                FillLicenciaGridFromList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void GetLicenciaFromGrid()
        {
            try
            {
                if (this.LicRowIndex != null && this.LicRowIndex > -1)
                {
                    oGrid = this.oForm.Items.Item("grdlic").Specific;
                    int licid = Convert.ToInt32(oGrid.DataTable.Columns.Item("colid").Cells.Item((int)this.LicRowIndex).Value);
                    //if (listobservacion != null && listobservacion.Count > 0 && obsid != 0)
                    if (listlicencia != null && listlicencia.Count > 0)
                    {
                        bool exist = listlicencia.Exists(svc => svc.LicenciaID == licid);
                        if (exist)
                        {
                            this.ELicencia.licencia = this.listlicencia.FirstOrDefault(svc => svc.LicenciaID == licid);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void FillLicenciaGridFromList()
        {
            try
            {
                DataTable tab = oForm.DataSources.DataTables.Item("dtlic");
                oGrid = this.oForm.Items.Item("grdlic").Specific;
                tab.Rows.Clear();
                int index = 0;
                foreach (Licencia lic in listlicencia)
                {
                    tab.Rows.Add();
                    tab.SetValue("colid", index, lic.LicenciaID ?? 0);
                    tab.SetValue("colnumero", index, lic.Numero);
                    tab.SetValue("colestatus", index, (lic.VigenciaLicencia >= DateTime.Today ? "Vigente" : "Vencida"));
                    tab.SetValue("coltipo", index, lic.ClaseLicencia.Value.ToString());
                    tab.SetValue("colvigencia", index, lic.VigenciaLicencia);
                    tab.SetValue("colaviso", index, lic.AvisoLicencia ?? 0);
                    index++;
                }
                oGrid.DataTable = tab;
                oGrid.Columns.Item("colid").Width = 50;
                oGrid.Columns.Item("colnumero").Width = 150;
                oGrid.Columns.Item("colestatus").Width = 100;
                oGrid.Columns.Item("coltipo").Width = 50;
                oGrid.Columns.Item("colvigencia").Width = 100;
                oGrid.Columns.Item("colaviso").Width = 100;

                oGrid.Columns.Item("colid").Visible = false;
                oGrid.Columns.Item("colnumero").Editable = false;
                oGrid.Columns.Item("colestatus").Editable = false;
                oGrid.Columns.Item("coltipo").Editable = false;
                oGrid.Columns.Item("colvigencia").Editable = false;
                oGrid.Columns.Item("colaviso").Editable = false;

                var title = oGrid.Columns.Item("colnumero").TitleObject;
                title.Caption = "Número";
                title = oGrid.Columns.Item("colestatus").TitleObject;
                title.Caption = "Estatus";
                title = oGrid.Columns.Item("coltipo").TitleObject;
                title.Caption = "Tipo";
                title = oGrid.Columns.Item("colvigencia").TitleObject;
                title.Caption = "Vigencia";
                title = oGrid.Columns.Item("colaviso").TitleObject;
                title.Caption = "Dias de Aviso";

                oGrid.SelectionMode = BoMatrixSelect.ms_Single;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void GetListLicencia()
        {
            ELicencia.Sincronizado = null;
            ELicencia.UUID = null;
            ELicencia.opid = EntityOperador.operador.OperadorID;
            var rs = ELicencia.Consultar(new Licencia());
            this.listlicencia = ELicencia.RecordSetToListLicencia(rs);
        }

        public bool ExistLicencia()
        {
            oItem = this.oForm.Items.Item("txtlicid");
            oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            return !string.IsNullOrEmpty(oStaticText.Caption);
        }
        #endregion

        #region Infracciones
        public void FormToInfraccion()
        {
            oItem = this.oForm.Items.Item("txtfio");
            oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            this.oInfraccion.U_INFRACCIONID = oStaticText.Caption == string.Empty ? 0 : Convert.ToInt32(oStaticText.Caption);
             
            //FOLIO
            oItem = this.oForm.Items.Item("txtiin");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.oInfraccion.U_FOLIO = oEditText.String == string.Empty ? null : oEditText.String;
             
            //NUMERO ECONOMICO
            try
            {
                oItem = this.oForm.Items.Item("cmbNEU");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                if(oComboBox.Selected != null)
                    this.oInfraccion.U_VEHICULOID =  (int)Convert.ChangeType(oComboBox.Selected.Value.ToString(), typeof(int));
                else
                    this.oInfraccion.U_VEHICULOID = null;
            }
            catch (Exception)
            {
                this.oInfraccion.U_VEHICULOID  = null;
            }
            //NUMERO INFRACCION
            try
            {
                oItem = this.oForm.Items.Item("cmbTIn");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                if (oComboBox.Selected != null)
                    this.oInfraccion.U_TIPOINFRACCIONID = (int)Convert.ChangeType(oComboBox.Selected.Value.ToString(), typeof(int));
                else
                    this.oInfraccion.U_TIPOINFRACCIONID = null;
            }
            catch (Exception)
            {
                this.oInfraccion.U_TIPOINFRACCIONID = null;
            }

            try
            {
                oItem = this.oForm.Items.Item("txtFIn");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.oInfraccion.U_FECHAINFRACCION = Convert.ToDateTime( oEditText.String);
            }
            catch (Exception)
            {
                this.oInfraccion.U_FECHAINFRACCION = null;
            }

            try
            {
                string horainfraccion = "";
                oItem = this.oForm.Items.Item("txthin");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                horainfraccion =  oEditText.String ;

                oItem = this.oForm.Items.Item("txmin");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                horainfraccion += ":"+oEditText.String;

                this.oInfraccion.U_HORA = horainfraccion;
            }
            catch (Exception)
            {
                this.oInfraccion.U_HORA = null;
            }

            try
            { 
                oItem = this.oForm.Items.Item("txtObI");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.oInfraccion.U_OBSERVACION = oEditText.String;
                  
            }
            catch (Exception)
            {
                this.oInfraccion.U_OBSERVACION = null;
            }

            this.CargaMueveEvidencia("txtAIn");//JBASTO20200716
 
        }

        public void EmptyInfraccionToForm()
        {
            

            oItem = this.oForm.Items.Item("txtfio");
            oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            oStaticText.Caption = string.Empty;

            oItem = this.oForm.Items.Item("txtiin");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("cmbNEU");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index);

            oItem = this.oForm.Items.Item("cmbTIn");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index);



            oItem = this.oForm.Items.Item("txtFIn");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txthin");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txmin");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtAIn");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtObI");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;


        }

        public void InfraccionToForm()
        {
            try
            {
                oItem = this.oForm.Items.Item("txtfio");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                oStaticText.Caption = this.oInfraccion.U_INFRACCIONID.ToString();
                 
                oItem = this.oForm.Items.Item("txtiin");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.oInfraccion.U_FOLIO;
                //this.oInfraccion.U_FOLIO = oEditText.String == string.Empty ? null : oEditText.String;

                oItem = this.oForm.Items.Item("cmbNEU");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                if (this.oInfraccion.U_VEHICULOID.HasValue)
                    oComboBox.Select(this.oInfraccion.U_VEHICULOID.ToString(), BoSearchKey.psk_ByValue);

                oItem = this.oForm.Items.Item("cmbTIn");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                if (this.oInfraccion.U_TIPOINFRACCIONID.HasValue)
                    oComboBox.Select(this.oInfraccion.U_TIPOINFRACCIONID.ToString(), BoSearchKey.psk_ByValue);
                //cmbTIn
                oItem = this.oForm.Items.Item("txtFIn");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.oInfraccion.U_FECHAINFRACCION.ToString().Substring(0,10);
                //txthin   :  txmin
                string hora = "";
                oItem = this.oForm.Items.Item("txthin");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                hora = this.oInfraccion.U_HORA; // .U_FECHAINFRACCION.ToString();
                oEditText.String = hora.Substring(0, 2);

                oItem = this.oForm.Items.Item("txmin");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = hora.Substring(2, 2);


                //txtAIn
                oItem = this.oForm.Items.Item("txtAIn");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.oInfraccion.U_ARCHIVO != null ? this.oInfraccion.U_ARCHIVO : string.Empty;
                //txtObI


                oItem = this.oForm.Items.Item("txtObI");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.oInfraccion.U_OBSERVACION != null ? this.oInfraccion.U_OBSERVACION : string.Empty;
                 
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ValidateInfraccionData()
        {
            this.FormToInfraccion();

            if (string.IsNullOrEmpty(this.oInfraccion.U_OBSERVACION ))
            {
                throw new Exception("El valor 'Observaciones' es obligatorio");
            }
            if (!this.oInfraccion.U_VEHICULOID.HasValue  )
            {
                throw new Exception("El valor 'Vehículo' es obligatorio");
            }
            if (!this.oInfraccion.U_TIPOINFRACCIONID.HasValue)
            {
                throw new Exception("El valor 'Tipo de infraccion' es obligatorio");
            }
           
        }

        public void NewInfraccionToGrid()
        {
            try
            {
                this.ValidateInfraccionData();
                if (this.listInfracciones == null) listInfracciones = new List<SBO_KF_INFRACCION_INFO>();
                listInfracciones.Add(oInfraccion);
                FillInfraccionesGridFromList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void GetInfraccionFromGrid()
        {
            try
            {
                if (this.InfracRowIndex != null && this.InfracRowIndex > -1)
                {
                    oGrid = this.oForm.Items.Item("grdinfr").Specific;
                    int infid = Convert.ToInt32(oGrid.DataTable.Columns.Item("colid").Cells.Item((int)this.InfracRowIndex).Value);
                    //if (listobservacion != null && listobservacion.Count > 0 && obsid != 0)
                    if (this.listInfracciones != null && listInfracciones.Count > 0)
                    {
                        bool exist = listInfracciones.Exists(svc => svc.U_INFRACCIONID == infid);
                        if (exist)
                        {
                            this.oInfraccion = this.listInfracciones.FirstOrDefault(svc => svc.U_INFRACCIONID == infid);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void FillInfraccionesGridFromList()
        {
            try
            {
                DataTable tab = oForm.DataSources.DataTables.Item("dtinfr");
                oGrid = this.oForm.Items.Item("grdinfr").Specific;
                tab.Rows.Clear();
                int index = 0;
                foreach (SBO_KF_INFRACCION_INFO infr in listInfracciones)
                {
                    tab.Rows.Add();
                    tab.SetValue("colid", index, infr.U_INFRACCIONID ?? 0);
                    tab.SetValue("colFol", index, infr.U_FOLIO);
                    tab.SetValue("colNEU", index, infr.U_NOMBREVEHICULO);
                    tab.SetValue("colInf", index, infr.U_NOMBRETIPOINFRACCION);
                    tab.SetValue("colFec", index, infr.U_FECHAINFRACCION);
                    tab.SetValue("colHor", index, infr.U_HORA);
                    tab.SetValue("coObs", index, infr.U_OBSERVACION);
                    index++;
                }
                oGrid.DataTable = tab;
                oGrid.Columns.Item("colid").Width = 100;
                oGrid.Columns.Item("colFol").Width = 50;
                oGrid.Columns.Item("colNEU").Width = 100;
                oGrid.Columns.Item("colInf").Width = 200;
                oGrid.Columns.Item("colFec").Width = 100;
                oGrid.Columns.Item("colHor").Width = 100;
                oGrid.Columns.Item("coObs").Width = 250;

                oGrid.Columns.Item("colid").Visible = false;
                oGrid.Columns.Item("colFol").Editable = false;
                oGrid.Columns.Item("colNEU").Editable = false;
                oGrid.Columns.Item("colInf").Editable = false;
                oGrid.Columns.Item("colFec").Editable = false;
                oGrid.Columns.Item("colHor").Editable = false;
                oGrid.Columns.Item("coObs").Editable = false;

                var title = oGrid.Columns.Item("colFol").TitleObject;
                title.Caption = "Folio";
                title = oGrid.Columns.Item("colNEU").TitleObject;
                title.Caption = "N. Eco";
                title = oGrid.Columns.Item("colInf").TitleObject;
                title.Caption = "Tipo";
                title = oGrid.Columns.Item("colFec").TitleObject;
                title.Caption = "Fecha";
                title = oGrid.Columns.Item("colHor").TitleObject;
                title.Caption = "Hora";
                title = oGrid.Columns.Item("coObs").TitleObject;
                title.Caption = "Observacion";
                oGrid.SelectionMode = BoMatrixSelect.ms_Single;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void GetListInfraccion()
        {
            this.listInfracciones = new List<SBO_KF_INFRACCION_INFO>();
            SBO_KF_INFRACCION_PD infCtrl = new SBO_KF_INFRACCION_PD();
            oInfraccion = new SBO_KF_INFRACCION_INFO();
            oInfraccion.U_OPERADORID = EntityOperador.operador.OperadorID;
            infCtrl.listaInfracciones(ref this.oCompany, ref  this.listInfracciones, oInfraccion, false);
        }

        public bool ExistInfraccion()
        {
            oItem = this.oForm.Items.Item("txtfio");
            oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            return !string.IsNullOrEmpty(oStaticText.Caption);
        }

        #endregion

        #region Cursos
        public void FormToCurso()
        {
            oItem = this.oForm.Items.Item("txtcurid");
            oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            this.ECurso.curso.CursoID = oStaticText.Caption == string.Empty ? null : (int?)Convert.ToInt32(oStaticText.Caption);

            oItem = this.oForm.Items.Item("txtcurso");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.ECurso.curso.Nombre = oEditText.String == string.Empty ? null : oEditText.String;

            oItem = this.oForm.Items.Item("txtclug");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.ECurso.curso.Lugar = oEditText.String == string.Empty ? null : oEditText.String;

            oItem = this.oForm.Items.Item("txtempo");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.ECurso.curso.EmpresaOtorga = oEditText.String == string.Empty ? null : oEditText.String;

            try
            {
                oItem = this.oForm.Items.Item("txtfcurs");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.ECurso.curso.Fecha = String.IsNullOrEmpty(oEditText.String) ? (DateTime?)null : DateTime.Parse(oEditText.String);
            }
            catch (Exception)
            {
                this.ELicencia.licencia.VigenciaLicencia = null;
            }
        }

        public void EmptyCursoToForm()
        {
            oItem = this.oForm.Items.Item("txtcurid");
            oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            oStaticText.Caption = string.Empty;

            oItem = this.oForm.Items.Item("txtcurso");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtclug");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtempo");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtfcurs");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;
        }

        public void CursoToForm()
        {
            try
            {
                oItem = this.oForm.Items.Item("txtcurid");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                oStaticText.Caption = this.ECurso.curso.CursoID == null ? string.Empty : this.ECurso.curso.CursoID.ToString();

                oItem = this.oForm.Items.Item("txtcurso");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.ECurso.curso.Nombre;

                oItem = this.oForm.Items.Item("txtclug");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.ECurso.curso.Lugar;

                oItem = this.oForm.Items.Item("txtempo");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.ECurso.curso.EmpresaOtorga;

                oItem = this.oForm.Items.Item("txtfcurs");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.ECurso.curso.Fecha != null ? this.ECurso.curso.Fecha.Value.ToShortDateString() : string.Empty;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ValidateCursoData()
        {
            this.FormToCurso();

            if (string.IsNullOrEmpty(this.ECurso.curso.Nombre.Trim()))
            {
                throw new Exception("El valor 'Curso' es obligatorio");
            }
            if (string.IsNullOrEmpty(this.ECurso.curso.Lugar.Trim()))
            {
                throw new Exception("El valor 'Lugar' es obligatorio");
            }
            if (string.IsNullOrEmpty(this.ECurso.curso.EmpresaOtorga.Trim()))
            {
                throw new Exception("El valor 'Empresa que otorga' es obligatorio");
            }
            if (ECurso.curso.Fecha == null)
            {
                throw new Exception("El valor 'Fecha' es obligatorio");
            }
        }

        public void NewCursoToGrid()
        {
            try
            {
                this.ValidateCursoData();
                if (this.listcurso == null) listcurso = new List<Curso>();
                listcurso.Add((Curso)ECurso.curso.Clone());
                FillCursosGridFromList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void GetCursoFromGrid()
        {
            try
            {
                if (this.CurRowIndex != null && this.CurRowIndex > -1)
                {
                    oGrid = this.oForm.Items.Item("grdcurs").Specific;
                    int id = Convert.ToInt32(oGrid.DataTable.Columns.Item("colid").Cells.Item((int)this.CurRowIndex).Value);
                    //if (listobservacion != null && listobservacion.Count > 0 && obsid != 0)
                    if (listcurso != null && listcurso.Count > 0)
                    {
                        bool exist = listcurso.Exists(svc => svc.CursoID == id);
                        if (exist)
                        {
                            this.ECurso.curso = this.listcurso.FirstOrDefault(svc => svc.CursoID == id);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void FillCursosGridFromList()
        {
            try
            {
                DataTable tab = oForm.DataSources.DataTables.Item("dtcur");
                oGrid = this.oForm.Items.Item("grdcurs").Specific;
                tab.Rows.Clear();
                int index = 0;
                foreach (Curso cur in listcurso)
                {
                    tab.Rows.Add();
                    tab.SetValue("colid", index, cur.CursoID ?? 0);
                    tab.SetValue("colnombre", index, cur.Nombre);
                    tab.SetValue("colemp", index, cur.EmpresaOtorga);
                    tab.SetValue("collugar", index, cur.Lugar);
                    tab.SetValue("colfecha", index, cur.Fecha);
                    index++;
                }
                oGrid.DataTable = tab;
                oGrid.Columns.Item("colid").Width = 50;
                oGrid.Columns.Item("colnombre").Width = 150;
                oGrid.Columns.Item("colemp").Width = 200;
                oGrid.Columns.Item("collugar").Width = 150;
                oGrid.Columns.Item("colfecha").Width = 100;

                oGrid.Columns.Item("colid").Visible = false;
                oGrid.Columns.Item("colnombre").Editable = false;
                oGrid.Columns.Item("colemp").Editable = false;
                oGrid.Columns.Item("collugar").Editable = false;
                oGrid.Columns.Item("colfecha").Editable = false;

                var title = oGrid.Columns.Item("colnombre").TitleObject;
                title.Caption = "Nombre";
                title = oGrid.Columns.Item("colemp").TitleObject;
                title.Caption = "Empresa que otorga";
                title = oGrid.Columns.Item("collugar").TitleObject;
                title.Caption = "Lugar";
                title = oGrid.Columns.Item("colfecha").TitleObject;
                title.Caption = "Fecha";

                oGrid.SelectionMode = BoMatrixSelect.ms_Single;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void GetListCursos()
        {
            ECurso.Sincronizado = null;
            ECurso.UUID = null;
            ECurso.opid = EntityOperador.operador.OperadorID;
            var rs = ECurso.Consultar(new Curso());
            this.listcurso = ECurso.RecordSetToListCursos(rs);
        }

        public bool ExistCurso()
        {
            oItem = this.oForm.Items.Item("txtcurid");
            oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            return !string.IsNullOrEmpty(oStaticText.Caption);
        }
        #endregion

        #region Observaciones
        public void FormToObservacion()
        {
            try
            {
                oItem = this.oForm.Items.Item("txtobsid");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                this.EObservacion.observacion.ObservacionID = oStaticText.Caption == string.Empty ? (int?)null : Convert.ToInt32(oStaticText.Caption);
            }
            catch (Exception ex)
            {
                this.EObservacion.observacion.ObservacionID = null;
            }

            oItem = this.oForm.Items.Item("txtobsrv");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.EObservacion.observacion.Descripcion = oEditText.String == string.Empty ? null : oEditText.String;

            try
            {
                oItem = this.oForm.Items.Item("txtfobsv");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.EObservacion.observacion.FechaObservacion = String.IsNullOrEmpty(oEditText.String) ? (DateTime?)null : DateTime.Parse(oEditText.String);
            }
            catch (Exception)
            {
                this.ELicencia.licencia.VigenciaLicencia = null;
            }
        }

        public void EmptyObservacionToForm()
        {
            oItem = this.oForm.Items.Item("txtobsid");
            oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            oStaticText.Caption = string.Empty;
            
            oItem = this.oForm.Items.Item("txtobsrv");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtfobsv");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;
        }

        public void ObservacionToForm()
        {
            try
            {
                oItem = this.oForm.Items.Item("txtobsid");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                oStaticText.Caption = this.EObservacion.observacion.ObservacionID != null || this.EObservacion.observacion.ObservacionID > 0? this.EObservacion.observacion.ObservacionID.ToString() : string.Empty;

                oItem = this.oForm.Items.Item("txtobsrv");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.EObservacion.observacion.Descripcion;

                oItem = this.oForm.Items.Item("txtfobsv");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.EObservacion.observacion.FechaObservacion != null ? this.EObservacion.observacion.FechaObservacion.Value.ToShortDateString() : string.Empty;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ValidateObservacionData()
        {
            this.FormToObservacion();

            if (string.IsNullOrEmpty(this.EObservacion.observacion.Descripcion.Trim()))
            {
                throw new Exception("El valor 'Descripción' es obligatorio");
            }
            if (EObservacion.observacion.FechaObservacion == null)
            {
                throw new Exception("El valor 'Fecha de Observación' es obligatorio");
            }
        }

        public void NewObservacionToGrid()
        {
            try
            {
                this.ValidateObservacionData();
                if (this.listobservacion == null) listobservacion = new List<Observacion>();
                listobservacion.Add((Observacion)EObservacion.observacion.Clone());
                FillObservacionesGridFromList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void GetObservacionFromGrid()
        {
            try
            {
                if (this.ObsRowIndex != null && this.ObsRowIndex > -1)
                {
                    oGrid = this.oForm.Items.Item("grdobsrv").Specific;
                    int obsid = Convert.ToInt32(oGrid.DataTable.Columns.Item("colobsid").Cells.Item((int)this.ObsRowIndex).Value);
                    //if (listobservacion != null && listobservacion.Count > 0 && obsid != 0)
                    if (listobservacion != null && listobservacion.Count > 0)
                    {
                        bool exist = listobservacion.Exists(svc => svc.ObservacionID == obsid);
                        if (exist)
                        {
                            this.EObservacion.observacion = this.listobservacion.FirstOrDefault(svc => svc.ObservacionID == obsid);
                        }
                    }
                }
            }
            catch (Exception ex )
            {
                throw new Exception(ex.Message);
            }
        }

        public void FillObservacionesGridFromList()
        {
            try
            {
                DataTable tab = oForm.DataSources.DataTables.Item("dtobs");
                oGrid = this.oForm.Items.Item("grdobsrv").Specific;
                tab.Rows.Clear();
                int index = 0;
                foreach (Observacion obs in listobservacion)
                {
                    tab.Rows.Add();
                    tab.SetValue("colobsid", index, obs.ObservacionID ?? 0);
                    tab.SetValue("colfecha", index, obs.FechaObservacion);
                    tab.SetValue("colobs", index, obs.Descripcion);
                    index++;
                }
                oGrid.DataTable = tab;
                oGrid.Columns.Item("colobsid").Width = 50;
                oGrid.Columns.Item("colfecha").Width = 100;
                oGrid.Columns.Item("colobs").Width = 550;

                oGrid.Columns.Item("colobsid").Visible = false;
                oGrid.Columns.Item("colfecha").Editable = false;
                oGrid.Columns.Item("colobs").Editable = false;

                var title = oGrid.Columns.Item("colfecha").TitleObject;
                title.Caption = "Fecha";
                title = oGrid.Columns.Item("colobs").TitleObject;
                title.Caption = "Observación";

                oGrid.SelectionMode = BoMatrixSelect.ms_Single;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void GetListObservaciones()
        {
            EObservacion.Sincronizado = null;
            EObservacion.UUID = null;
            EObservacion.opid = EntityOperador.operador.OperadorID;
            var rs = EObservacion.Consultar(new Observacion());
            this.listobservacion = EObservacion.RecordSetToListObservacion(rs);
        }

        public bool ExistObservacion()
        {
            oItem = this.oForm.Items.Item("txtobsid");
            oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            return !string.IsNullOrEmpty(oStaticText.Caption);
        }
        #endregion

        #region Asignar Unidad
        public void FormToAsignacion()
        {
            this.Asignacion.operadorv.OperadorAsignado.OperadorID = this.EntityOperador.operador.OperadorID;
            this.Asignacion.operadorv.OperadorAsignado.EmpleadoID = this.Entity.oEmpleado.EmpleadoID;

            oItem = this.oForm.Items.Item("cmbvehi");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            this.Asignacion.operadorv.VehiculoAsignado.VehiculoID = Convert.ToInt32(oComboBox.Selected.Value);

            oItem = this.oForm.Items.Item("txtafecha");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Asignacion.operadorv.FechaAsignacion = String.IsNullOrEmpty(oEditText.String.Trim()) ? (DateTime?)null : DateTime.Parse(oEditText.String);

            oItem = this.oForm.Items.Item("txthr");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (!string.IsNullOrEmpty(oEditText.String.Trim()) && !oEditText.String.Trim().Equals("00") && Asignacion.operadorv.FechaAsignacion != null)
            {
                try
                {
                    var hr = Convert.ToInt32(oEditText.String);
                    //if (hr <= 0 || hr > 12)
                    this.Asignacion.operadorv.FechaAsignacion = Asignacion.operadorv.FechaAsignacion.Value.AddHours(hr);
                    //else
                    //{
                    //    throw new Exception("Valor 'Horas' invalido en campo 'Fecha'");
                    //}

                }
                catch
                {
                    throw new Exception("Valor 'Horas' invalido en campo 'Fecha'");
                }
            }

            oItem = this.oForm.Items.Item("txtmin");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (!string.IsNullOrEmpty(oEditText.String.Trim()) && !oEditText.String.Trim().Equals("00") && Asignacion.operadorv.FechaAsignacion != null)
            {
                try
                {
                    var min = Convert.ToInt32(oEditText.String);
                    //if (min <= 0 || min > 12)
                    this.Asignacion.operadorv.FechaAsignacion = Asignacion.operadorv.FechaAsignacion.Value.AddMinutes(min);
                    //else
                    //{
                    //    throw new Exception("Valor 'Minutos' invalido en campo 'Fecha'");
                    //}
                }
                catch
                {
                    throw new Exception("Valor 'Minutos' invalido en campo 'Fecha'");
                }
            }
            oItem = this.oForm.Items.Item("txtobserv");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Asignacion.operadorv.ObservacionAsignacion = oEditText.String;
        }

        public void FormToLiberacion()
        {
            this.Asignacion.operadorv.OperadorAsignado.OperadorID = this.EntityOperador.operador.OperadorID;

            oItem = this.oForm.Items.Item("cmbvehi");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            this.Asignacion.operadorv.VehiculoAsignado.VehiculoID = Convert.ToInt32(oComboBox.Value);

            oItem = this.oForm.Items.Item("txtafecha");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Asignacion.operadorv.FechaLiberacion = String.IsNullOrEmpty(oEditText.String.Trim()) ? (DateTime?)null : DateTime.Parse(oEditText.String);


            oItem = this.oForm.Items.Item("txthr");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (!string.IsNullOrEmpty(oEditText.String.Trim()) && !oEditText.String.Trim().Equals("00") && Asignacion.operadorv.FechaLiberacion != null)
            {
                try
                {
                    var hr = Convert.ToInt32(oEditText.String);
                    //if (hr <= 0 || hr > 12)
                    this.Asignacion.operadorv.FechaLiberacion = Asignacion.operadorv.FechaLiberacion.Value.AddHours(hr);
                    //else
                    //{
                    //    throw new Exception("Valor 'Horas' invalido en campo 'Fecha'");
                    //}

                }
                catch
                {
                    throw new Exception("Valor 'Horas' invalido en campo 'Fecha'");
                }
            }

            oItem = this.oForm.Items.Item("txtmin");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (!string.IsNullOrEmpty(oEditText.String.Trim()) && !oEditText.String.Trim().Equals("00") && Asignacion.operadorv.FechaLiberacion != null)
            {
                try
                {
                    var min = Convert.ToInt32(oEditText.String);
                    //if (min <= 0 || min > 12)
                    this.Asignacion.operadorv.FechaLiberacion = Asignacion.operadorv.FechaLiberacion.Value.AddMinutes(min);
                    //else
                    //{
                    //    throw new Exception("Valor 'Minutos' invalido en campo 'Fecha'");
                    //}
                }
                catch
                {
                    throw new Exception("Valor 'Minutos' invalido en campo 'Fecha'");
                }
            }

            /*oItem = this.oForm.Items.Item("txthr");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            Asignacion.operadorv.FechaLiberacion.Value.AddHours(Convert.ToInt32(oEditText.String.ToString().Trim()));

            oItem = this.oForm.Items.Item("txtmin");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            Asignacion.operadorv.FechaLiberacion.Value.AddMinutes(Convert.ToInt32(oEditText.String.ToString().Trim()));*/


            oItem = this.oForm.Items.Item("txtobserv");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Asignacion.operadorv.ObservacionLiberacion = oEditText.String;
        }

        public void AsignacionToForm(bool empty)
        {
            try
            {
                oItem = this.oForm.Items.Item("cmbvehi");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                if (empty) oComboBox.Select("0", SAPbouiCOM.BoSearchKey.psk_Index);
                else oComboBox.Select(this.Asignacion.operadorv.VehiculoAsignado.VehiculoID.ToString());

                oItem = this.oForm.Items.Item("txtafecha");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = DateTime.Today.ToShortDateString();

                oItem = this.oForm.Items.Item("txthr");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = "00";

                oItem = this.oForm.Items.Item("txtmin");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = "00";

                oItem = this.oForm.Items.Item("txtobserv");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = String.Empty;
            }
            catch { }
        }

        public void ValidateAsignation()
        {
            oItem = this.oForm.Items.Item("cmbvehi");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            if (oComboBox.Selected == null) throw new Exception("El valor 'Vehículo' es obligatorio");
            if (oComboBox.Selected.Value.Trim() == "0") throw new Exception("El valor 'Vehículo' es obligatorio");

            oItem = this.oForm.Items.Item("txtobserv");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (string.IsNullOrEmpty(oEditText.String.Trim())) throw new Exception("El valor 'Observación' es obligatorio");

            oItem = this.oForm.Items.Item("txtafecha");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (string.IsNullOrEmpty(oEditText.String.Trim())) throw new Exception("El valor 'Fecha' es obligatorio");

            try
            {
                oItem = this.oForm.Items.Item("txthr");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (string.IsNullOrEmpty(oEditText.String.Trim()))
                    throw new Exception("El valor 'Hora' es obligatorio");
                if (Convert.ToInt32(oEditText.String.ToString().Trim()) > 23)
                    throw new Exception("El valor de horas debe ser menor a 24");
            }
            catch
            {
                throw new Exception("Proporciona un formato de hora válido (24 horas)");
            }

            try
            {
                oItem = this.oForm.Items.Item("txtmin");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (string.IsNullOrEmpty(oEditText.String.Trim()))
                    throw new Exception("El valor 'Minutos' es obligatorio");
                if (Convert.ToInt32(oEditText.String.ToString().Trim()) > 59)
                    throw new Exception("El valor de minutos debe ser menor a 60");
            }
            catch
            {
                throw new Exception("Proporciona un formato de minutos válido");
            }
        }

        public void ValidateLiberation()
        {
            oItem = this.oForm.Items.Item("cmbvehi");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            if (oComboBox.Value == null)
                throw new Exception("El valor 'Vehículo' es obligatorio");
            if (oComboBox.Value.Trim() == "0")
                throw new Exception("El valor 'Vehículo' es obligatorio");

            oItem = this.oForm.Items.Item("txtobserv");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (string.IsNullOrEmpty(oEditText.String.Trim()))
                throw new Exception("El valor 'Observación' es obligatorio");

            oItem = this.oForm.Items.Item("txtafecha");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (string.IsNullOrEmpty(oEditText.String.Trim()))
                throw new Exception("El valor 'Fecha' es obligatorio");

            try
            {
                oItem = this.oForm.Items.Item("txthr");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (string.IsNullOrEmpty(oEditText.String.Trim()))
                    throw new Exception("El valor 'Hora' es obligatorio");
                if (Convert.ToInt32(oEditText.String.ToString().Trim()) > 24)
                    throw new Exception("El valor de horas debe ser menor a 24");
            }
            catch {
                throw new Exception("Proporciona un formato de hora válido (24 horas)");
            }

            try
            {
                oItem = this.oForm.Items.Item("txtmin");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (string.IsNullOrEmpty(oEditText.String.Trim()))
                    throw new Exception("El valor 'Minutos' es obligatorio");
                if (Convert.ToInt32(oEditText.String.ToString().Trim()) > 59)
                    throw new Exception("El valor de minutos debe ser menor a 60");
            }
            catch
            {
                throw new Exception("Proporciona un formato de minutos válido");
            }
        }

        public void SetEstatusAsignacion(bool asigna)
        {
            try
            {
                //oItem = oForm.Items.Item("btnasignar");
                //oItem.Visible = asigna;
                //oItem = oForm.Items.Item("btnliberar");
                //oItem.Visible = !asigna;

                oItem = this.oForm.Items.Item("lblfecha");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                oStaticText.Caption = asigna ? "Fecha Asignación (Formato 24 hrs)" : "Fecha Liberación (Formato 24 hrs)";

                oItem = this.oForm.Items.Item("lblobserv");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                oStaticText.Caption = asigna ? "Observaciones de Asignación:" : "Observaciones de Liberación:";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void PrepareAsignacion()
        {


            oItem = oForm.Items.Item("cmbvehi");
            oItem.Enabled = true;

            oItem = oForm.Items.Item("txtafecha");
            oItem.Enabled = true;

            oItem = oForm.Items.Item("txthr");
            oItem.Enabled = true;

            oItem = oForm.Items.Item("txtmin");
            oItem.Enabled = true;

            oItem = oForm.Items.Item("txtobserv");
            oItem.Enabled = true;

            oItem = oForm.Items.Item("btnasignar");
            oItem.Enabled = true;

            oItem = oForm.Items.Item("btnliberar");
            oItem.Enabled = false;

            SetEstatusAsignacion(true);
        }

        public void PrepareLiberacion()
        {
            oItem = oForm.Items.Item("cmbvehi");
            oItem.Enabled = false;

            oItem = oForm.Items.Item("txtafecha");
            oItem.Enabled = true;

            oItem = oForm.Items.Item("txthr");
            oItem.Enabled = true;

            oItem = oForm.Items.Item("txtmin");
            oItem.Enabled = true;

            oItem = oForm.Items.Item("txtobserv");
            oItem.Enabled = true;

            oItem = oForm.Items.Item("btnasignar");
            oItem.Enabled = false;

            oItem = oForm.Items.Item("btnliberar");
            oItem.Enabled = true;

            SetEstatusAsignacion(false);
        }

        public void DisableAsignacion()
        {
            oItem = oForm.Items.Item("cmbvehi");
            oItem.Enabled = false;

            oItem = oForm.Items.Item("txtafecha");
            oItem.Enabled = false;

            oItem = oForm.Items.Item("txthr");
            oItem.Enabled = false;

            oItem = oForm.Items.Item("txtmin");
            oItem.Enabled = false;

            oItem = oForm.Items.Item("txtobserv");
            oItem.Enabled = false;

            oItem = oForm.Items.Item("btnasignar");
            oItem.Enabled = false;

            oItem = oForm.Items.Item("btnliberar");
            oItem.Enabled = false;

            SetEstatusAsignacion(true);
        }
        #endregion

        #region Metodos Auxiliares
        public void SetMessage(bool exist)
        {
            try
            {
                oItem = oForm.Items.Item("lblmsg");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                oStaticText.Caption = exist ? "El usuario FuelData ya existe, se actualizará con los datos proporcionados"
                    : "Usuario FuelData no existe, se creará con los datos proporcionados";
                /*if (exist)
                {
                    ShowDeleteButton();
                }
                else
                {
                    HideDeleteButton();
                }*/
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private string ValidatePass(string pass)
        {
            bool mayus = tiene_mayuscula(pass);
            bool minus = tiene_minuscula(pass);
            bool numeric = tiene_numeros(pass);
            if (!mayus) return "La Contraseña debe contener al menos una letra mayúscula";
            if (!minus) return "La Contraseña debe contener al menos una letra minúscula";
            if (!numeric) return "La Contraseña debe contener al menos un caracter numérico";
            return string.Empty;
        }

        private bool tiene_numeros(string texto)
        {
            var numeros = "0123456789";
            bool numeric = false;
            foreach (char c in texto.Where(c => numeros.IndexOf(c) != -1))
            {
                numeric = true;
            }
            return numeric;
        }

        private bool tiene_minuscula(string texto)
        {
            var minusculas = "abcdefghyjklmnñopqrstuvwxyz";
            bool minus = false;
            foreach (char c in texto.Where(c => minusculas.IndexOf(c) != -1))
            {
                minus = true;
            }
            return minus;
        }

        private bool tiene_mayuscula(string texto)
        {
            var mayusculas = "ABCDEFGHYJKLMNÑOPQRSTUVWXYZ";
            bool mayus = false;
            foreach (char c in texto.Where(c => mayusculas.IndexOf(c) != -1))
            {
                mayus = true;
            }
            return mayus;
        } 
        #endregion


        #region CargaFile
        public void CargaFileTexBox(string uicaja)
        {
            string path = "";
            try
            {
                Thread t = new Thread((ThreadStart)delegate
                {
                    System.Windows.Forms.FileDialog objDialog = new System.Windows.Forms.OpenFileDialog();
                    objDialog.Filter = "PDF files (*.pdf)|*.pdf|WORD files (*.doc)|*.doc|Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.png";
                    System.Windows.Forms.Form g = new System.Windows.Forms.Form();
                    g.Width = 1;
                    g.Height = 1;
                    g.Activate();
                    g.BringToFront();
                    g.Visible = true;
                    g.TopMost = true;
                    g.Focus();

                    System.Windows.Forms.DialogResult objResult = objDialog.ShowDialog(g);
                    Thread.Sleep(100);
                    if (objResult == System.Windows.Forms.DialogResult.OK)
                    {
                        path = objDialog.FileName;
                    }
                })
                {
                    IsBackground = false,
                    Priority = ThreadPriority.Highest
                };
                t.SetApartmentState(ApartmentState.STA);
                t.Start();
                while (!t.IsAlive) ;
                Thread.Sleep(1);
                t.Join();
                if (!String.IsNullOrEmpty(path))
                {
                    if (File.Exists(path))
                    {
                        this.oItem = this.oForm.Items.Item(uicaja);
                        this.oEditText = (EditText)this.oItem.Specific;
                        this.oEditText.String = path;
                    }
                }
            }
            catch (Exception ex)
            {
                this.SBO_Application.MessageBox("Error al seleccionar el archivo: " + ex.Message, 1, "OK");
            }

        }
        public void ViewDocPlaca(string uicaja)
        {
            try
            {
                this.oItem = this.oForm.Items.Item(uicaja);
                this.oEditText = (EditText)this.oItem.Specific;
                if (!string.IsNullOrEmpty(this.oEditText.String))
                {
                    string sPathimage = string.Format(@"{0}\{1}", sPathInfraccion, this.oEditText.String);
                    if (File.Exists(sPathimage))
                        Process.Start(sPathimage);
                }
            }
            catch (Exception) { }
        }
        void CargaMueveEvidencia(string scajaui)
        {
            try
            { 
                this.oInfraccion.U_ARCHIVO = string.Empty; 
                this.oItem = this.oForm.Items.Item(scajaui);
                this.oEditText = (EditText)this.oItem.Specific;
                if (!string.IsNullOrEmpty(this.oEditText.String))
                {
                    if (File.Exists(this.oEditText.String))
                    {
                        FileInfo oInfo = new FileInfo(this.oEditText.String);
                        this.oInfraccion.U_ARCHIVO = string.Format(@"{0}_{1}", Guid.NewGuid().ToString(), oInfo.Name);
                        string sOrigen = oInfo.FullName;
                        string sDestino = string.Format(@"{0}\{1}", this.sPathInfraccion, this.oInfraccion.U_ARCHIVO);

                        //this.ELicencia.licencia.Rutalicencia

                        File.Copy(sOrigen, sDestino);

                    }
                }
                else throw new Exception("Favor de seleccionar un documento de evidencia");
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #endregion
    }
}
