﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using Etecno.Security2.BO;
using Kanan.Comun.BO2;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using KananWS.Interface;
using SAPbouiCOM;
using SAPinterface.Data.INFO.Objetos;
using KananSAP.GestionDatos.MigradorDatos.Librerias;
using SAPinterface.Data.PD;

namespace KananSAP.GestionUsuarios.AdministraUsuarios
{
    public class AdministraUsuarioPresenter
    {

        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private SAPbouiCOM.Item oItem;
        public AdministraUsuarioView view { get; set; }
        public UsuarioView userview { get; set; }
        //private GestionGlobales oGlobales = null;
        private Configurations configs;
        private EmpleadoWS EmpleadoService;
        private bool Exist;
        private bool Modal;
        Helper.KananSAPHerramientas oHerramientas;
        #endregion

        #region Constructor
        public AdministraUsuarioPresenter(SAPbouiCOM.Application aplication, SAPbobsCOM.Company company, Configurations c)
        {
            this.configs = c;
            this.SBO_Application = aplication;
            this.Company = company;
            //this.SBO_Application = this.oGlobales.SBO_Application;
            this.view = new AdministraUsuarioView(this.SBO_Application, this.Company);
            this.userview = new UsuarioView(this.SBO_Application, this.Company, this.configs);
            this.EmpleadoService = new EmpleadoWS();
            this.Exist = false;
            this.Modal = false;
            SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
            this.oHerramientas = new Helper.KananSAPHerramientas(this.SBO_Application);
        }
        #endregion

        #region Eventos
        private void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                #region ListadoEmpleados
                if (pVal.FormTypeEx == "UsersForm")
                {
                    if (Modal)
                    {
                        userview.ShowForm();
                        BubbleEvent = false;
                        return;
                    }
                    #region beforeaction

                    if (pVal.BeforeAction)
                    {
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnSearch")
                        {
                            this.view.SetConditions();
                            this.view.BindDataToForm();
                        }
                        if (pVal.EventType == BoEventTypes.et_MATRIX_LINK_PRESSED)
                        {
                            this.view.SetID(pVal.Row);
                            this.userview.ShowForm();
                            if (this.view.id != null)
                            {
                                Exist = this.userview.Entity.SetEmployeeRelationById((int)this.view.id);
                                PrepareUserForm();
                                //this.userview.FillVehicles();
                            }
                            this.Modal = true;
                            //int index = pVal.Row;
                            //var str = this.view.GetDataRow(index);
                            //SBO_Application.StatusBar.SetText("linkbutton pressed - EmpleadoID = " + str,BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                        }
                    }

                    #endregion

                    #region After Action
                    else
                    {
                        //if (Modal && pVal.EventType == BoEventTypes.et_FORM_ACTIVATE)
                        //{
                        //    userview.ShowForm();
                        //    BubbleEvent = false;
                        //    return;
                        //}
                    }
                    #endregion 
                }
                #endregion

                #region Empleado - Operador
                if (pVal.FormTypeEx == "KFUser")
                {
                    //choose from list
                    if (pVal.EventType == BoEventTypes.et_CHOOSE_FROM_LIST)
                    {
                        if (!pVal.Before_Action)
                        {
                            if ((pVal.ItemUID == "txtOCrd"))
                            {

                                SAPbouiCOM.IChooseFromListEvent oCFLEvento = null;
                                oCFLEvento = ((SAPbouiCOM.IChooseFromListEvent)(pVal));
                                string sCFL_ID = null;
                                sCFL_ID = oCFLEvento.ChooseFromListUID;
                                SAPbouiCOM.Form oForm = null;
                                oForm = SBO_Application.Forms.Item(FormUID);
                                SAPbouiCOM.ChooseFromList oCFL = null;
                                oCFL = oForm.ChooseFromLists.Item(sCFL_ID);
                                if (oCFLEvento.BeforeAction == false)
                                {
                                    SAPbouiCOM.DataTable oDataTable = null;
                                    oDataTable = oCFLEvento.SelectedObjects;
                                    string val = val = System.Convert.ToString(oDataTable.GetValue(0, 0));
                                    oForm.DataSources.UserDataSources.Item("ChoCRD").ValueEx = val;
                                }
                            }

                        }
                    }

                    if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_CLOSE & Modal)
                    {
                        Modal = false;
                    }
                    #region BeforeAction
                                        //  If the modal from is closed...
                    if (pVal.BeforeAction)
                    {
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnEdit")
                        {
                            this.userview.ValidateUserData();
                            this.userview.ValidateData();
                            if (Exist)
                            {
                                //Acondiciones para actualizar
                                this.Update();
                                BubbleEvent = false;
                            }
                            else
                            {
                                //Acondicionando para insertar con sentencia SQL.
                                this.Insert();
                            }
                            this.userview.EmptyEntityToForm();
                            this.userview.EmptyUserToForm();
                            this.userview.CloseForm();
                        }

                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnElim")
                        {
                            if (Exist)
                            {
                                /*Se elimina el operador*/
                                this.Delete();
                            }
                            this.userview.EmptyEntityToForm();
                            this.userview.EmptyUserToForm();
                            this.userview.CloseForm();
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnFoto")                        
                            this.userview.UploadPhoto();

                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnArc")
                        {
                            this.userview.CargaFileTexBox("txtAIn");
                        }

                        #region Asignacion de Unidad
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnasignar")
                        {
                            this.userview.ValidateAsignation();
                            this.Asigna();
                            this.userview.AsignacionToForm(false);
                            this.userview.PrepareLiberacion();
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnliberar")
                        {
                            this.userview.ValidateLiberation();
                            this.Libera();
                            this.userview.AsignacionToForm(true);
                            this.userview.PrepareAsignacion();
                            this.userview.FillVehicles();
                        } 
                        #endregion

                        #region Licencias
                        if (pVal.ItemUID == "grdlic" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            BubbleEvent = false;
                            this.userview.SetLicRowIndex(pVal.Row);
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnlic")
                        {
                            if (this.userview.ExistLicencia())
                            {
                                this.userview.FormToLicencia();
                                int index = userview.listlicencia.FindIndex(
                                        x => x.LicenciaID == userview.ELicencia.licencia.LicenciaID);
                                userview.listlicencia[index] = (Licencia)userview.ELicencia.licencia.Clone();
                                userview.edtlicencia.Add((Licencia)userview.ELicencia.licencia.Clone());
                                userview.FillLicenciaGridFromList();
                            }
                            else
                            {
                                /*Comentado para poder editar sin repetir en el grid*/
                                //this.userview.NewLicenciaToGrid();

                                /*Nueva forma de insertar.*/
                                this.userview.ValidateLicenciaData();
                                this.InsertLicencia();
                                this.userview.GetListLicencia();
                                this.userview.FillLicenciaGridFromList();
                            }
                            this.userview.EmptyLicenciaToForm();
                            BubbleEvent = false;
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnedtlic")
                        {
                            this.userview.GetLicenciaFromGrid();
                            this.userview.LicenciaToForm();
                            BubbleEvent = false;
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnelilic")
                        {
                            this.userview.GetLicenciaFromGrid();
                            //int index = userview.listobservacion.FindIndex(
                            //        x => x.ObservacionID == userview.EObservacion.observacion.ObservacionID);
                            if (this.userview.LicRowIndex == null) throw new Exception("Indice de objeto no establecido");
                            int index = (int)this.userview.LicRowIndex;
                            userview.listlicencia.RemoveAt(index);
                            userview.ELicencia.Remove();
                            if (userview.ELicencia.licencia.LicenciaID > 0)
                            {
                                userview.dellicencia.Add((Licencia)userview.ELicencia.licencia.Clone());
                            }
                            userview.FillLicenciaGridFromList();
                            BubbleEvent = false;
                        }
                        #endregion

                        #region Cursos
                        if (pVal.ItemUID == "grdcurs" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            BubbleEvent = false;
                            this.userview.SetCurRowIndex(pVal.Row);
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btncur")
                        {
                            if (this.userview.ExistCurso())
                            {
                                this.userview.FormToCurso();
                                int index = userview.listcurso.FindIndex(
                                        x => x.CursoID == userview.ECurso.curso.CursoID);
                                userview.listcurso[index] = (Curso)userview.ECurso.curso.Clone();
                                userview.edtcurso.Add((Curso)userview.ECurso.curso.Clone());
                                userview.FillCursosGridFromList();
                            }
                            else
                            {
                                /*Comentado para poder editar sin repetir en el grid*/
                                //this.userview.NewCursoToGrid();

                                /*Nueva forma de insertar.*/
                                this.userview.ValidateCursoData();
                                this.InsertCurso();
                                this.userview.GetListCursos();
                                this.userview.FillCursosGridFromList();
                            }
                            this.userview.EmptyCursoToForm();
                            BubbleEvent = false;
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "tbnedtcur")
                        {
                            this.userview.GetCursoFromGrid();
                            this.userview.CursoToForm();
                            BubbleEvent = false;
                        }

                        

                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnelicur")
                        {
                            this.userview.GetCursoFromGrid();
                            if (this.userview.CurRowIndex == null)
                                throw new Exception("Indice de objeto no establecido");
                            int index = (int)this.userview.CurRowIndex;
                            userview.listcurso.RemoveAt(index);
                            userview.ECurso.Remove();
                            if (userview.ECurso.curso.CursoID > 0)
                            {
                                userview.delcurso.Add((Curso)userview.ECurso.curso.Clone());
                            }
                            userview.FillCursosGridFromList();
                            BubbleEvent = false;
                        }
                        #endregion

                        #region Observaciones
                        if (pVal.ItemUID == "grdobsrv" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            BubbleEvent = false;
                            this.userview.SetObsRowIndex(pVal.Row);
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnobs")
                        {
                            if (this.userview.ExistObservacion())
                            {
                                this.userview.FormToObservacion();
                                int index = userview.listobservacion.FindIndex(
                                        x => x.ObservacionID == userview.EObservacion.observacion.ObservacionID);
                                userview.listobservacion[index] = (Observacion) userview.EObservacion.observacion.Clone();
                                userview.edtobservacion.Add((Observacion) userview.EObservacion.observacion.Clone());
                                userview.FillObservacionesGridFromList();
                            }
                            else
                            {
                                /*Comentado para poder editar sin repetir en el grid*/
                                //this.userview.NewObservacionToGrid();

                                /*Nueva forma de insertar.*/
                                this.userview.ValidateObservacionData();
                                this.InsertObservacion();
                                this.userview.GetListObservaciones();
                                this.userview.FillObservacionesGridFromList();
                            }
                            this.userview.EmptyObservacionToForm();
                            BubbleEvent = false;
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnedtobs")
                        {
                            this.userview.GetObservacionFromGrid();
                            this.userview.ObservacionToForm();
                            BubbleEvent = false;
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btneliobs")
                        {
                            this.userview.GetObservacionFromGrid();
                            //int index = userview.listobservacion.FindIndex(
                            //        x => x.ObservacionID == userview.EObservacion.observacion.ObservacionID);
                            if (this.userview.ObsRowIndex == null) throw new Exception("Indice de objeto no establecido");
                            int index = (int) this.userview.ObsRowIndex;
                            userview.listobservacion.RemoveAt(index);
                            userview.EObservacion.Remove();
                            if (userview.EObservacion.observacion.ObservacionID > 0)
                            {
                                userview.delobservacion.Add((Observacion)userview.EObservacion.observacion.Clone());
                            } 
                            userview.FillObservacionesGridFromList();
                            BubbleEvent = false;
                        }
                        #endregion

                        #region Infracciones
                        if (pVal.ItemUID == "grdinfr" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            BubbleEvent = false;
                            //this.userview.SetObsRowIndex(pVal.Row);
                            this.userview.SetInfrRowIndex(pVal.Row);
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnAIn")
                        {
                            if (this.userview.ExistInfraccion())
                            {
                                this.userview.FormToInfraccion();
                                this.UpdateInfraccion();
                                //int index = userview.listInfracciones.FindIndex(
                                //        x => x.U_INFRACCIONID == userview.oInfraccion.U_INFRACCIONID);

                                //userview.listInfracciones[index] = (SBO_KF_INFRACCION_INFO)userview.oInfraccion;

                                //userview.listInfraccionesIns.Add((SBO_KF_INFRACCION_INFO)userview.oInfraccion);
                                this.userview.GetListInfraccion();
                                this.userview.FillInfraccionesGridFromList(); // .FillObservacionesGridFromList();
                            }
                            else
                            {
                                /*Comentado para poder editar sin repetir en el grid*/
                                //this.userview.NewObservacionToGrid();

                                /*Nueva forma de insertar.*/
                                this.userview.ValidateInfraccionData();//  ValidateObservacionData();
                                this.InsertInfraccion();// .insertInfraccion();
                                
                                this.userview.GetListInfraccion();//  .GetListObservaciones();
                                this.userview.FillInfraccionesGridFromList();// .FillObservacionesGridFromList();
                            }
                            this.userview.EmptyInfraccionToForm(); // .EmptyObservacionToForm();
                            BubbleEvent = false;
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnedinf")
                        {
                            this.userview.GetInfraccionFromGrid();// .GetCursoFromGrid();
                            this.userview.InfraccionToForm(); // .CursoToForm();
                            BubbleEvent = false;
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnelinf")
                        {
                            this.userview.GetInfraccionFromGrid(); //   .GetObservacionFromGrid();
                             
                            if (this.userview.InfracRowIndex == null) throw new Exception("Indice de objeto no establecido");
                            int index = (int)this.userview.InfracRowIndex;
                            userview.listInfracciones.RemoveAt(index); // .listobservacion.RemoveAt(index);
                            this.EliminarInfraccion( Convert.ToInt32( this.userview.oInfraccion.Code));
                            this.userview.GetListInfraccion();
                            this.userview.FillInfraccionesGridFromList();
                             
                            BubbleEvent = false;
                        }
                        #endregion

                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btninf")
                        {
                            this.userview.ViewDocPlaca("txtAIn");
                        }

                        if (pVal.EventType == BoEventTypes.et_FORM_UNLOAD)
                        {
                            this.Exist = false;
                        }
                    }
                    #endregion

                    #region After Action
                    else
                    {
                        //if (pVal.EventType == BoEventTypes.et_FORM_LOAD)
                        //{
                        //    this.userview.ChangeFolder(4);
                        //}
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
                this.oHerramientas.ColocarMensaje(ex.Message,BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            }
        }
        #endregion

        #region Metodos

        #region Acciones Operadores
        private void Insert()
        {
            try
            {
                //if(this.userview.Entity.oEmpleado != null && this.userview.Entity.oEmpleado.Usuario != null)
                this.userview.Entity.oEmpleado = new Empleado();
                this.userview.Entity.oEmpleado.Usuario = new Usuario();
                this.userview.Entity.oEmpleado.EmpleadoSucursal = new Sucursal();
                this.userview.EntityOperador.ItemCode = null;

                //Se captura la información al objeto Usuario (no se valida e inserta valores perdetermiandos)
                this.userview.FormToUser();

                //Se capturan los datos de la interfaz en el objeto y se valida (Se agregan configuraciones predeterminadas en caso de no ser definidas).
                this.userview.FormToEntity();

                var id = this.view.id;
                if (id != null) this.userview.Entity.SetEmployeeById((int)id);
                else
                    throw new Exception("Error en la transacción");
                this.userview.EntityOperador.operador.OperadorID = id;
                EmpleadoToOperador(this.userview.Entity.oEmpleado, this.userview.EntityOperador.operador);
                this.userview.EntityOperador.UUID = Guid.NewGuid();
                this.userview.EntityOperador.Sincronizado = 0;
                this.userview.EntityOperador.OperadorToUDT();
                this.userview.EntityOperador.Insertar();

                //Se verifica si existen sucursales pertenecientes la empresa y en caso de no tenerla(s) se destruye la instancia del objeto sucursales.
                if (this.userview.Entity.oEmpleado.EmpleadoSucursal != null && this.userview.Entity.oEmpleado.EmpleadoSucursal.SucursalID == 0)
                    this.userview.Entity.oEmpleado.EmpleadoSucursal.SucursalID = null;
                this.userview.Entity.oEmpleado = EmpleadoService.Add(this.userview.Entity.oEmpleado, this.userview.configs.UserFull.Usuario);
                EmpleadoToOperador(this.userview.Entity.oEmpleado, this.userview.EntityOperador.operador);
                this.userview.EntityOperador.operador = this.EmpleadoService.AddOprdr(this.userview.EntityOperador.operador);

                this.userview.EntityOperador.Sincronizado = 1;
                this.userview.EntityOperador.ActualizarCode(this.userview.EntityOperador.operador);
                this.userview.Entity.AddEmployeeRelationSimple();

                ManageLicencia();
                ManageCursos();
                ManageObservaciones();

               this.oHerramientas.ColocarMensaje("Operación Exitosa", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            }
            catch (Exception ex)
            {
                this.userview.EmptyEntityToForm();
                this.userview.CloseForm();
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->Insert()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void Update()
        {
            try
            {
                this.userview.FormToEntity();
                
                EmpleadoToOperador(this.userview.Entity.oEmpleado, this.userview.EntityOperador.operador);
                this.userview.EntityOperador.Sincronizado = 2;
                this.userview.EntityOperador.OperadorToUDT();
                this.userview.EntityOperador.Update();

                try
                {
                    this.EmpleadoService.Update(this.userview.Entity.oEmpleado);
                    this.EmpleadoService.UpdateOprdr(this.userview.EntityOperador.operador);
                }
                catch { }

                this.userview.EntityOperador.Sincronizado = 0;
                this.userview.EntityOperador.OperadorToUDT();
                this.userview.EntityOperador.Update();

                if (!string.IsNullOrEmpty(this.userview.EntityOperador.operador.sCadenaImagen))
                {
                    string sresponse = string.Empty;
                    OperadorWS oWSOperador = new OperadorWS();
                    SBO_KF_OPERADOR_INFO Operadorimage = new SBO_KF_OPERADOR_INFO();
                    string sPathimage = string.Format(@"{0}\{1}", this.userview.sPathOperadores, this.userview.EntityOperador.operador.sCadenaImagen);
                    FileInfo oInfo = new FileInfo(sPathimage);
                    Operadorimage.NombreImagen = oInfo.Name; //this.userview.EntityOperador.operador.sCadenaImagen;
                    Operadorimage.OperadorID = this.userview.EntityOperador.operador.OperadorID.ToString();
                    Operadorimage.Extension = oInfo.Extension;
                    Operadorimage.Imagen64 = this.ImageToBase64(sPathimage);
                    oWSOperador.ActualizaImagen(Operadorimage, out sresponse);
                
                }
                ManageLicencia();
                ManageCursos();
                ManageObservaciones();

                this.oHerramientas.ColocarMensaje("Operación Exitosa", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            }
            catch (Exception ex)
            {
                this.userview.EmptyEntityToForm();
                this.userview.CloseForm();
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->Update()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public string ImageToBase64(string sourcePath)
        {
            string resultEncode64 = null;

            using (System.Drawing.Image imgOperador = System.Drawing.Image.FromFile(sourcePath))
            {
                using (MemoryStream _mStream = new MemoryStream())
                {
                    imgOperador.Save(_mStream, imgOperador.RawFormat);
                    byte[] _imageBytes = _mStream.ToArray();
                    resultEncode64 = Convert.ToBase64String(_imageBytes);
                    return /*"data:image/jpg;base64," +*/ resultEncode64;
                }
            }
        }
        #endregion

        #region Delete
        private void Delete()
        {
            try
            {
                /*Operador*/
                #region Incializar
                this.userview.Entity.oEmpleado = new Empleado();
                this.userview.Entity.oEmpleado.Usuario = new Usuario();
                this.userview.Entity.oEmpleado.EmpleadoSucursal = new Sucursal();
                this.userview.EntityOperador.ItemCode = null;
                #endregion Inicializar

                //Se captura la información al objeto Usuario (no se valida e inserta valores perdetermiandos)
                this.userview.FormToUser();

                //Se capturan los datos de la interfaz en el objeto y se valida (Se agregan configuraciones predeterminadas en caso de no ser definidas).
                this.userview.FormToEntity();

                //Se obtienen valores de operador y se pasan tanto al UDT como al BO.
                var id = this.view.id;
                if (id != null)
                    this.userview.Entity.SetEmployeeById((int)id);
                else
                    throw new Exception("Error en la transacción");

                EmpleadoToOperador(this.userview.Entity.oEmpleado, this.userview.EntityOperador.operador);
                this.userview.EntityOperador.UUID = Guid.NewGuid();
                this.userview.EntityOperador.Sincronizado = 1;
                this.userview.EntityOperador.OperadorToUDT();
                this.userview.EntityOperador.Deactivate();

                //Se verifica si existen sucursales pertenecientes la empresa y en caso de no tenerla(s) se destruye la instancia del objeto sucursales.
                if (this.userview.Entity.oEmpleado.EmpleadoSucursal != null && this.userview.Entity.oEmpleado.EmpleadoSucursal.SucursalID == 0)
                    this.userview.Entity.oEmpleado.EmpleadoSucursal.SucursalID = null;
                /*Comentado porque sólo de debe actualizar el operador.*/
                //this.userview.Entity.oEmpleado = EmpleadoService.Update(this.userview.Entity.oEmpleado, this.userview.configs.UserFull.Usuario);

                this.userview.EntityOperador.operador.EsActivo = false;
                EmpleadoToOperador(this.userview.Entity.oEmpleado, this.userview.EntityOperador.operador);
                this.userview.EntityOperador.operador = this.EmpleadoService.UpdateOprdr(this.userview.EntityOperador.operador);

                /*Libera*/
                Libera();
                /*Licencia*/
                DeleteLicencia();
                /*Curso*/
                DeleteCurso();
                /*Observacion*/
                DeleteObservacion();
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->Delete()", ex.Message);
                SBO_Application.SetStatusBarMessage("Error al intentar eliminar el operador. " + ex.Message, BoMessageTime.bmt_Short, false);
            }
        }
        #endregion Delete

        #region Acciones Asignacion de Vehiculo
        private void Asigna()
        {
            try
            {
                long iFolioSig = new Add(ref Company, null).recuperIDMaximoTabla("VSKF_OPERADORVEHICL");
                if (iFolioSig > 0)
                {
                    this.userview.Asignacion.ItemCode = string.Empty;
                    this.userview.Asignacion.operadorv.OperadorVehiculoID = null;
                    this.userview.FormToAsignacion();
                    this.userview.Asignacion.UUID = Guid.NewGuid();
                    this.userview.Asignacion.Sincronizado = 1;
                    this.userview.Asignacion.OperadorToUDT();
                    this.userview.Asignacion.Insert(iFolioSig.ToString());
                    this.userview.Asignacion.operadorv = this.EmpleadoService.AsignaOperador(this.userview.Asignacion.operadorv);
                    this.userview.Asignacion.Sincronizado = 0;
                    this.userview.Asignacion.ActualizarCode(this.userview.Asignacion.operadorv);
                    this.oHerramientas.ColocarMensaje("Operación Exitosa", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                }
                else throw new Exception("No se ha podido asignar al operador");
            }
            catch (Exception ex)
            {
                this.userview.AsignacionToForm(true);
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->Asigna()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void Libera()
        {
            try
            {
                this.userview.FormToLiberacion();
                this.userview.Asignacion.ItemCode = this.userview.Asignacion.operadorv.OperadorVehiculoID.ToString();
                this.userview.Asignacion.Sincronizado = 2;
                this.userview.Asignacion.OperadorToUDT();
                this.userview.Asignacion.Update();
                this.EmpleadoService.LiberaOperador(this.userview.Asignacion.operadorv);
                this.userview.Asignacion.Sincronizado = 0;
                this.userview.Asignacion.ActualizarCode(this.userview.Asignacion.operadorv);
                this.oHerramientas.ColocarMensaje("Operación Exitosa", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            }
            catch (Exception ex)
            {
                this.userview.AsignacionToForm(false);
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->Libera()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Acciones Licencia
        public void ManageLicencia()
        {
            try
            {
                foreach (var lic in userview.listlicencia)
                {
                    if (lic.LicenciaID == null || lic.LicenciaID <= 0)
                    {
                        this.userview.ELicencia.licencia = lic;
                        InsertLicencia();
                    }
                }

                foreach (var lic in userview.edtlicencia)
                {
                    this.userview.ELicencia.oUDT.GetByKey(lic.LicenciaID.ToString());
                    this.userview.ELicencia.UDTToLicencia();
                    this.userview.ELicencia.licencia = lic;
                    UpdateLicencia();
                }

                foreach (var lic in userview.dellicencia)
                {
                    if (lic.LicenciaID != null && lic.LicenciaID > 0)
                    {
                        this.userview.ELicencia.licencia = lic;
                        DeleteLicencia();
                    }
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->ManageLicencia()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public void InsertLicencia()
        {
            try
            {
                this.userview.ELicencia.opid = this.userview.EntityOperador.operador.OperadorID;
                this.userview.ELicencia.UUID = Guid.NewGuid();
                this.userview.ELicencia.Sincronizado = 1;
                //this.userview.ELicencia.LicenciaToUDT();
                //this.userview.ELicencia.Insert();

                this.userview.ELicencia.licencia = this.EmpleadoService.AddLicencia(userview.EntityOperador.operador,
                    userview.ELicencia.licencia);

                this.userview.ELicencia.LicenciaToUDT();
                this.userview.ELicencia.Insert();

                this.userview.ELicencia.Sincronizado = 0;
                this.userview.ELicencia.ActualizarCode(userview.ELicencia.licencia);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->InsertLicencia()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public void UpdateLicencia()
        {
            try
            {
                this.userview.ELicencia.opid = this.userview.EntityOperador.operador.OperadorID;
                this.userview.ELicencia.LicenciaToUDT();
                this.userview.ELicencia.Update();

                this.userview.ELicencia.licencia = this.EmpleadoService.UpdateLicencia(userview.ELicencia.licencia);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->UpdateLicencia()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public void DeleteLicencia()
        {
            try
            {
                this.userview.ELicencia.oUDT.GetByKey(this.userview.ELicencia.licencia.LicenciaID.ToString());
                this.userview.ELicencia.Sincronizado = 3;
                this.userview.ELicencia.LicenciaToUDT();
                this.userview.ELicencia.Update();

                this.userview.ELicencia.licencia = this.EmpleadoService.DeleteLicencia((int)userview.ELicencia.licencia.LicenciaID);

                this.userview.ELicencia.Remove();
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->DeleteLicencia()", ex.Message);
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Acciones Cursos
        public void ManageCursos()
        {
            try
            {
                foreach (var cur in userview.listcurso)
                {
                    if (cur.CursoID == null || cur.CursoID <= 0)
                    {
                        this.userview.ECurso.curso = cur;
                        InsertCurso();
                    }
                    //else
                    //{
                    //    this.userview.EObservacion.oUDT.GetByKey(obs.ObservacionID.ToString());
                    //    this.userview.EObservacion.UDTToObservacion();
                    //    this.userview.EObservacion.observacion = obs;
                    //    UpdateObservacion();
                    //}
                    //Observacion temp = (Observacion)userview.EObservacion.observacion.Clone();
                    //newlist.Add(temp);
                    //this.userview.listobservacion = newlist;
                }

                foreach (var cur in userview.edtcurso)
                {
                    this.userview.ECurso.oUDT.GetByKey(cur.CursoID.ToString());
                    this.userview.ECurso.UDTToCurso();
                    this.userview.ECurso.curso = cur;
                    UpdateCurso();
                }

                foreach (var cur in userview.delcurso)
                {
                    if (cur.CursoID != null && cur.CursoID > 0)
                    {
                        this.userview.ECurso.curso = cur;
                        DeleteCurso();
                    }
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->ManageCursos()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public void InsertCurso()
        {
            try
            {
                this.userview.ECurso.opid = this.userview.EntityOperador.operador.OperadorID;
                this.userview.ECurso.UUID = Guid.NewGuid();
                this.userview.ECurso.Sincronizado = 1;
                //this.userview.ECurso.cursoToUDT();
                //this.userview.ECurso.Insert(); 

                this.userview.ECurso.curso = this.EmpleadoService.AddCurso(userview.EntityOperador.operador,
                    userview.ECurso.curso);

                this.userview.ECurso.cursoToUDT();
                this.userview.ECurso.Insert(); 

                this.userview.ECurso.Sincronizado = 0;
                this.userview.ECurso.ActualizarCode(userview.ECurso.curso);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->InsertCurso()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public void UpdateCurso()
        {
            try
            {
                this.userview.ECurso.opid = this.userview.EntityOperador.operador.OperadorID;
                this.userview.ECurso.cursoToUDT();
                this.userview.ECurso.Update();

                this.userview.ECurso.curso = this.EmpleadoService.UpdateCurso(userview.ECurso.curso);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->UpdateCurso()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public void DeleteCurso()
        {
            try
            {
                this.userview.ECurso.oUDT.GetByKey(this.userview.ECurso.curso.CursoID.ToString());
                this.userview.ECurso.Sincronizado = 3;
                this.userview.ECurso.cursoToUDT();
                this.userview.ECurso.Update();

                this.userview.ECurso.curso = this.EmpleadoService.DeleteCurso((int)userview.ECurso.curso.CursoID);

                this.userview.ECurso.Remove();
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->DeleteCurso()", ex.Message);
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Acciones Observaciones
        public void ManageObservaciones()
        {
            try
            {
                var newlist = new List<Observacion>();
                foreach (var obs in userview.listobservacion)
                {
                    if (obs.ObservacionID == null || obs.ObservacionID <= 0)
                    {
                        this.userview.EObservacion.observacion = obs;
                        InsertObservacion();
                    }
                    //else
                    //{
                    //    this.userview.EObservacion.oUDT.GetByKey(obs.ObservacionID.ToString());
                    //    this.userview.EObservacion.UDTToObservacion();
                    //    this.userview.EObservacion.observacion = obs;
                    //    UpdateObservacion();
                    //}
                    //Observacion temp = (Observacion)userview.EObservacion.observacion.Clone();
                    //newlist.Add(temp);
                    //this.userview.listobservacion = newlist;
                }

                foreach (var obs in userview.edtobservacion)
                {
                    this.userview.EObservacion.oUDT.GetByKey(obs.ObservacionID.ToString());
                    this.userview.EObservacion.UDTToObservacion();
                    this.userview.EObservacion.observacion = obs;
                    UpdateObservacion();
                }

                foreach (var obs in userview.delobservacion)
                {
                    if (obs.ObservacionID != null && obs.ObservacionID > 0)
                    {
                        this.userview.EObservacion.observacion = obs;
                        DeleteObservacion();
                    }
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public void InsertObservacion()
        {
            try
            {
                this.userview.EObservacion.observacion.FechaRegistro = DateTime.Today;
                this.userview.EObservacion.opid = this.userview.EntityOperador.operador.OperadorID;
                this.userview.EObservacion.UUID = Guid.NewGuid();
                this.userview.EObservacion.Sincronizado = 1;
                //this.userview.EObservacion.observacionToUDT();
                //this.userview.EObservacion.Insert();

                this.userview.EObservacion.observacion = this.EmpleadoService.AddObservacion(userview.EntityOperador.operador,
                    userview.EObservacion.observacion);

                this.userview.EObservacion.observacionToUDT();
                this.userview.EObservacion.Insert();

                this.userview.EObservacion.Sincronizado = 0;
                this.userview.EObservacion.ActualizarCode(userview.EObservacion.observacion);
                
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->InsertObservacion()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public void UpdateObservacion()
        {
            try
            {
                this.userview.EObservacion.opid = this.userview.EntityOperador.operador.OperadorID;
                this.userview.EObservacion.observacionToUDT();
                this.userview.EObservacion.Update();

                this.userview.EObservacion.observacion = this.EmpleadoService.UpdateObservacion(userview.EObservacion.observacion);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->UpdateObservacion()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public void DeleteObservacion()
        {
            try
            {
                this.userview.EObservacion.oUDT.GetByKey(this.userview.EObservacion.observacion.ObservacionID.ToString());
                this.userview.EObservacion.Sincronizado = 3;
                this.userview.EObservacion.observacionToUDT();
                this.userview.EObservacion.Update();

                this.userview.EObservacion.observacion = this.EmpleadoService.DeleteObservacion((int) userview.EObservacion.observacion.ObservacionID);

                this.userview.EObservacion.Remove();
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->DeleteObservacion()", ex.Message);
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Acciones Infracciones
        public void ManageInfracciones()
        {
            SBO_KF_INFRACCION_PD CtrlInfr = new SBO_KF_INFRACCION_PD();
            SBO_KF_INFRACCION_INFO oInf = new SBO_KF_INFRACCION_INFO();

            try
            {
                var newlist = new List<Observacion>();
                foreach (var infr in userview.listInfraccionesIns)
                {
                    if (infr.U_INFRACCIONID == null || infr.U_INFRACCIONID <= 0)
                    {
                        CtrlInfr.InsertUpdateInfracciones(ref this.Company, infr, false);
                    }
                    
                }

                foreach (SBO_KF_INFRACCION_INFO infrac in userview.listInfracciones)
                {
                    //InsertUpdateInfracciones(ref Company oCompany, SBO_KF_INFRACCION_INFO oInfraccion, bool bUpdate = true)
                    //CtrlInfr.InsertUpdateInfracciones(ref this.company, infrac, true);
                    CtrlInfr.InsertUpdateInfracciones(ref this.Company, infrac, true); 
                }

                foreach (SBO_KF_INFRACCION_INFO infrac in userview.listInfraccionesDel)
                {
                    if (infrac.U_INFRACCIONID != null && infrac.U_INFRACCIONID > 0)
                    {
                        CtrlInfr.EliminaInfracciones(ref this.Company, (int)infrac.U_INFRACCIONID);
                        //this.userview.EObservacion.observacion = obs;
                        //DeleteObservacion();
                    }
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public void InsertInfraccion()
        {
            SBO_KF_INFRACCION_PD CtrlInfr = new SBO_KF_INFRACCION_PD();
            try
            {
                long iCode = new Add(ref Company, null).recuperIDMaximoTabla("VSKF_INFRACCIONES");
                this.userview.oInfraccion.Code = iCode.ToString();
                this.userview.oInfraccion.Name = iCode.ToString();
                this.userview.oInfraccion.U_INFRACCIONID = (int)iCode;
                CtrlInfr.InsertUpdateInfracciones(ref this.Company, this.userview.oInfraccion, false); 

            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->InsertObservacion()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public void UpdateInfraccion()
        {
            SBO_KF_INFRACCION_PD CtrlInfr = new SBO_KF_INFRACCION_PD();
            try
            {
                //long iCode = new Add(ref Company, null).recuperIDMaximoTabla("VSKF_INFRACCIONES");
                //this.userview.oInfraccion.Code = iCode.ToString();
                //this.userview.oInfraccion.Name = iCode.ToString();
                CtrlInfr.InsertUpdateInfracciones(ref this.Company, this.userview.oInfraccion, true);

            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->InsertObservacion()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public void EliminarInfraccion(int infraccionid)
        {
            SBO_KF_INFRACCION_PD CtrlInfr = new SBO_KF_INFRACCION_PD();
            try
            {
                /*long iCode = new Add(ref Company, null).recuperIDMaximoTabla("VSKF_INFRACCIONES");
                this.userview.oInfraccion.Code = iCode.ToString();
                this.userview.oInfraccion.Name = iCode.ToString();*/
                CtrlInfr.EliminaInfracciones(ref this.Company, infraccionid);

            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->InsertObservacion()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        //public void UpdateObservacion()
        //{
        //    try
        //    {
        //        this.userview.EObservacion.opid = this.userview.EntityOperador.operador.OperadorID;
        //        this.userview.EObservacion.observacionToUDT();
        //        this.userview.EObservacion.Update();

        //        this.userview.EObservacion.observacion = this.EmpleadoService.UpdateObservacion(userview.EObservacion.observacion);
        //    }
        //    catch (Exception ex)
        //    {
        //        KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->UpdateObservacion()", ex.Message);
        //        throw new Exception(ex.Message);
        //    }
        //}

        //public void DeleteObservacion()
        //{
        //    try
        //    {
        //        this.userview.EObservacion.oUDT.GetByKey(this.userview.EObservacion.observacion.ObservacionID.ToString());
        //        this.userview.EObservacion.Sincronizado = 3;
        //        this.userview.EObservacion.observacionToUDT();
        //        this.userview.EObservacion.Update();

        //        this.userview.EObservacion.observacion = this.EmpleadoService.DeleteObservacion((int)userview.EObservacion.observacion.ObservacionID);

        //        this.userview.EObservacion.Remove();
        //    }
        //    catch (Exception ex)
        //    {
        //        KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->DeleteObservacion()", ex.Message);
        //        throw new Exception(ex.Message);
        //    }
        //}
        #endregion

        private void PrepareUserForm()
        {
            try
            {
                this.userview.listlicencia = new List<Licencia>();
                this.userview.listcurso = new List<Curso>();
                this.userview.listobservacion = new List<Observacion>();
                this.userview.dellicencia = new List<Licencia>();
                this.userview.delcurso = new List<Curso>();
                this.userview.delobservacion = new List<Observacion>();
                this.userview.edtlicencia = new List<Licencia>();
                this.userview.edtcurso = new List<Curso>();
                this.userview.edtobservacion = new List<Observacion>();
                if (Exist)
                {
                    var id = this.userview.Entity.oUDT.UserFields.Fields.Item("U_KFEMPLEADOID").Value.ToString();
                    var i = Convert.ToInt32(id);
                    this.userview.EntityOperador.SetOperadorByEmpleadoID(i);
                    this.userview.Entity.oEmpleado = this.EmpleadoService.LogginByID(i).UserFull;

                    this.userview.GetListLicencia();
                    this.userview.GetListCursos();
                    this.userview.GetListObservaciones();
                    this.userview.GetListInfraccion();
                    this.userview.FillLicenciaGridFromList();
                    this.userview.FillCursosGridFromList();
                    this.userview.FillObservacionesGridFromList();
                    this.userview.FillInfraccionesGridFromList();
                    this.userview.SetMessage(Exist);

                    if (userview.EntityOperador.operador.OperadorID!=null && this.userview.Asignacion.SetCurrentOperadorVehiculoByOperadorID((int) userview.EntityOperador.operador.OperadorID))
                    {
                        this.userview.PrepareLiberacion();
                        this.userview.AsignacionToForm(false);
                    }
                    else
                    {
                        this.userview.PrepareAsignacion();
                        this.userview.AsignacionToForm(true);
                    }
                    this.userview.ChangeFolder(1);

                    this.userview.UserToForm();
                    this.userview.EntityToForm();
                }
                else
                {
                    var id = this.view.id;
                    if (id != null) this.userview.Entity.SetEmployeeById((int) id);
                    this.userview.AsignacionToForm(true);
                    this.userview.EmptyEntityToForm();
                    this.userview.EmptyUserToForm();
                    this.userview.FillLicenciaGridFromList();
                    this.userview.FillCursosGridFromList();
                    this.userview.FillObservacionesGridFromList();
                    this.userview.DisableAsignacion();
                    this.userview.SetMessage(Exist);
                    this.userview.ChangeFolder(1);

                }

                this.userview.EmptyLicenciaToForm();
                this.userview.EmptyCursoToForm();
                this.userview.EmptyObservacionToForm();
                //this.userview.FillLicenciaGridFromList();
                //this.userview.FillCursosGridFromList();
                //this.userview.FillObservacionesGridFromList();

                //this.userview.SetMessage(Exist);
                //this.userview.ChangeFolder(1);

            }
            catch (Exception ex)
            {
                this.userview.EmptyEntityToForm();
                this.userview.CloseForm();
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraUsuarioPresenter.cs->PrepareUserForm()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        #region Metodos Auxiliares

        private void EmpleadoToOperador(Empleado oEmpleado, Operador op)
        {
            op.Nombre = oEmpleado.Nombre;
            op.Apellido1 = oEmpleado.Apellido1;
            op.Apellido2 = oEmpleado.Apellido2;
            op.Telefono = oEmpleado.Telefono;
            op.EmpleadoID = oEmpleado.EmpleadoID;
            op.Correo = oEmpleado.Email;
            op.Direccion = oEmpleado.Direccion;
            op.Propietario = oEmpleado.Dependencia;
            op.EsActivo = true;
            op.Celular = oEmpleado.Celular;
            op.SubPropietario = oEmpleado.EmpleadoSucursal == null ? new Sucursal() : oEmpleado.EmpleadoSucursal;
            op.Picture = new Imagen();
        }

        #endregion

        #endregion
    }
}
