﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Kanan.Comun.BO2;
using Kanan.Mantenimiento.BO2;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using KananSAP.Autentificacion.Views;
using KananSAP.Mantenimiento.Data;
using KananSAP.Vehiculos.Data;
using KananWS.Interface;
using SAPbouiCOM;
using SAPinterface.Data;

namespace KananSAP.GestionUsuarios.Autentificacion
{
    public class AutentificacionPresenter
    {
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        public AutentificacionView view { get; set; }
        private EmpleadoWS ws { get; set; }
        private ServiciosWS serviciosWs;
        private VehiculoWS vehiculoWs;
        private bool DoAction;
        private ServicioSAP EServicios;
        private TipoServicioData ETipoServicios;
        private TipoVehiculoSAP ETVehiculo;
    
        #region Constructor
        public AutentificacionPresenter(SAPbouiCOM.Application sApplication, SAPbobsCOM.Company Cmpny)
        {
            this.SBO_Application = sApplication;
            this.Company = Cmpny;
            this.view = new AutentificacionView(SBO_Application,Company);
            this.ws = new EmpleadoWS();
            this.DoAction = true;
            try
            {
                this.EServicios = new ServicioSAP(Company);
            }
            catch(Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AutentificacionPresenter.cs->AutentificacionPresenter()", ex.Message);
            }
            this.ETipoServicios = new TipoServicioData(Company);
            this.ETVehiculo = new TipoVehiculoSAP(Company);

            #region Comentado. La aplicación principal maneja los eventos
            //SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
            #endregion Comentado. La aplicación principal maneja los eventos

            //SBO_Application.MenuEvent += new SAPbouiCOM._IApplicationEvents_MenuEventEventHandler(SBO_Application_MenuEvent);
        }
        #endregion

        #region Eventos
        public void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (pVal.FormTypeEx == "LogginKF")
                {
                    #region beforeaction
                    if (pVal.BeforeAction)
                    {
                        if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_LOAD)
                        {
                            this.view.SetForm(pVal.FormTypeEx, pVal.FormTypeCount);
                        }
                        if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                        {
                            if (pVal.ItemUID == "chkusersap")
                            {
                                this.view.SetSystemUserLoggedNow(this.view.IsChecked());
                            }
                            if (pVal.ItemUID == "btnloggin")
                            {
                                DoAction = false;
                                if (!this.view.ValidarDatos())
                                {
                                    SBO_Application.StatusBar.SetText("Favor de ingresar los datos que hacen falta en el formulario");
                                }
                                else if(!this.view.VerificaUsuarioSAP())
                                {
                                    SBO_Application.StatusBar.SetText("El usuario SAP proporcionado no existe, favor de verificar");
                                }
                                else
                                {
                                    DoAction = true;
                                }
                            }
                        }
                    }
                    #endregion
                    #region AfterAction
                    else if (!pVal.BeforeAction)
                    {
                        if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                        {
                            if (pVal.ItemUID == "btnloggin" && DoAction)
                            {
                                this.AsociarKananFleet();
                                this.view.CloseForm();
                            }
                            else
                            {
                                DoAction = true;
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AutentificacionPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
                SBO_Application.StatusBar.SetText(ex.Message);
            }
        }
        #endregion

        #region Metodos Auxiliares

        private void AsociarKananFleet()
        {

            try
            {
                this.view.FormToEntity();
                this.view.Entity.SetSAPUserByUserCode();
                this.view.Entity.SetEmployeeByUserId();
                this.AutentificacionKF(this.view.Entity.oEmpleado.Usuario.Nombre,
                    this.view.Entity.oEmpleado.Usuario.Clave);
                this.view.SetKFCompanyByEmployee();
                if (this.view.EntityEmp.GetCompanyBySAPCompany())
                {
                    SBO_Application.StatusBar.SetText("Ya existe una relación entre esta Compañia en SAP y una Empresa Kananfleet ",
                        BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                    return;
                }
                this.view.EntityEmp.AddRelacionEmpresa();
                this.view.Entity.AddEmployeeRelation();
                //this.ws.AddOprdr(this.view.Entity.oEmpleado);
                var config = this.LogInCurrentSAPUserToKF();
                this.view.Entity.oEmpleado = config.UserFull;
                this.GetAllServicios(config);
                this.GetAllTipoVehiculos();
                this.GetAllTipoMoneda(config);
                this.GetAllTipoPago(config);
                this.GetAllEstatusOrden();
                this.SBO_Application.StatusBar.SetText("Autentificación KananFleet exitosa", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                this.SBO_Application.MessageBox("Reiniciar la sesión para aplicar las nuevas configuraciones y módulos KananFleet");
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AutentificacionPresenter.cs->AsociarKananFleet()", ex.Message);
                this.SBO_Application.StatusBar.SetText(ex.Message);
            }
        }

        public void LogInSAPUserToKF(string sap_user)
        {

        }

        public Configurations LogInCurrentSAPUserToKF()
        {
            try
            {
                this.view.Entity.oUser.UserCode = this.SBO_Application.Company.UserName;
                this.view.Entity.SetSAPUserByUserCode();
                this.view.Entity.SetEmployeeByUserId();
                this.view.Entity.GetEmployeeRealtionBySapEmployee();
                var empleadoId = this.view.Entity.oEmpleado.EmpleadoID;
                if (empleadoId != null)
                {
                    return this.AutentificacionKF((int)empleadoId);
                }
                throw new Exception("Cuenta Kananfleet no encontrada");
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AutentificacionPresenter.cs->LogInCurrentSAPUserToKF()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public Configurations AutentificacionKF(int KF_empid)
        {
            try
            {
                var configs = this.ws.LogginByID(KF_empid);
                this.view.Entity.oEmpleado = configs.UserFull?? new Empleado();
                return configs;
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AutentificacionPresenter.cs->AutentificaciónKF()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public void AutentificacionKF(string KF_user,string KF_pass)
        {
            try
            {
                this.view.Entity.oEmpleado = this.ws.LogginByUser(KF_user, KF_pass);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AutentificacionPresenter.cs->AutentificaciónKF()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void IsLogged()
        {

        }

        public void UpdateAPIKeys()
        {
            this.ws.PublicKey = Guid.Parse(this.view.Entity.oEmpleado.Usuario.PublicKey);
            this.ws.PrivateKey = this.view.Entity.oEmpleado.Usuario.PrivateKey;
        }

        private void GetAllServicios(Configurations configs)
        {
            try
            {
                var publicKey = Guid.Parse(this.view.Entity.oEmpleado.Usuario.PublicKey);
                var privateKey = this.view.Entity.oEmpleado.Usuario.PrivateKey;
                this.serviciosWs = new ServiciosWS(publicKey, privateKey);
                List<Servicio> list = serviciosWs.GetServiciosByID((int)this.view.Entity.oEmpleado.Dependencia.EmpresaID);
                List<TipoServicio> listtipo = new List<TipoServicio>();
                foreach (Servicio svc in list)
                {
                    if (listtipo.All(x => x.TipoServicioID != svc.TipoServicio.TipoServicioID))
                    {
                        svc.TipoServicio.Propietario = new Empresa{EmpresaID = configs.UserFull.Dependencia.EmpresaID};
                        if (svc.TipoServicio.Nombre.ToUpper().Contains("PREVENTIVO"))
                        {
                            svc.TipoServicio.Descripcion = "prevención de servicios";
                        }
                        else
                        {
                            if (svc.TipoServicio.Nombre.ToUpper().Contains("CORRECTIVO"))
                            {
                                svc.TipoServicio.Descripcion = "reparación o cambio de pieza";
                            }
                        }
                        listtipo.Add((TipoServicio) svc.TipoServicio.Clone());
                    }
                    this.EServicios.oServicio = svc;
                    this.EServicios.ItemCode = svc.ServicioID.ToString();
                    this.EServicios.UUID = Guid.NewGuid();
                    this.EServicios.Sincronizado = 0;
                    this.EServicios.ServicioToUDT();
                    this.EServicios.Insertar();
                }
                foreach (TipoServicio tipoServicio in listtipo)
                {
                    this.ETipoServicios.oTipoServicio = tipoServicio;
                    this.ETipoServicios.ItemCode = tipoServicio.TipoServicioID.ToString();
                    this.ETipoServicios.UUID = Guid.NewGuid();
                    this.ETipoServicios.Sincronizado = 0;
                    this.ETipoServicios.TipoServicioToUDT();
                    this.ETipoServicios.Inserta();
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AutentificacionPresenter.cs->GetAllServicios()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void GetAllTipoVehiculos()
        {
            try
            {
                var publicKey = Guid.Parse(this.view.Entity.oEmpleado.Usuario.PublicKey);
                var privateKey = this.view.Entity.oEmpleado.Usuario.PrivateKey;
                this.vehiculoWs = new VehiculoWS(publicKey, privateKey);
                List<TipoVehiculo> list = this.vehiculoWs.GetVehicleTypes((int)this.view.Entity.oEmpleado.Dependencia.EmpresaID);
                foreach (TipoVehiculo tipoVehiculo in list)
                {
                    this.ETVehiculo.TipoVehiculo = tipoVehiculo;
                    this.ETVehiculo.UUID = Guid.NewGuid();
                    this.ETVehiculo.Sincronizado = 0;
                    this.ETVehiculo.TipoVehiculoToUDT();
                    this.ETVehiculo.Insertar();
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AutentificacionPresenter.cs->GetAllTipoVehiculos()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void GetAllTipoMoneda(Configurations configs)
        {
            try
            {
                var oUDT = Company.UserTables.Item("VSKF_TIPOMONEDA");
                List<TipoMoneda> list = configs.TiposMoneda;
                foreach (TipoMoneda tipomoneda in list)
                {
                    string tks = DateTime.Now.Ticks.ToString();
                    oUDT.Code = tipomoneda.TipoMonedaID != null ? tipomoneda.TipoMonedaID.ToString() : tks;
                    oUDT.Name = tipomoneda.Descripcion ?? tks;
                    oUDT.UserFields.Fields.Item("U_Estatus").Value = (tipomoneda.Estatus ? 1 : 0);

                    int flag = oUDT.Add();
                    if (flag != 0)
                    {
                        int errornum;
                        string errormsj;
                        Company.GetLastError(out errornum, out errormsj);
                        throw new Exception("Error: " + errornum + " - " + errormsj);
                    }
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AutentificacionPresenter.cs->GetAllTipoMoneda()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void GetAllTipoPago(Configurations configs)
        {
            try
            {
                var oUDT = Company.UserTables.Item("VSKF_TIPODEPAGO");
                List<TipoDePago> list = configs.TiposPago;
                foreach (TipoDePago tipopago in list)
                {
                    string tks = DateTime.Now.Ticks.ToString();
                    oUDT.Code = tipopago.TipoPagoID != null ? tipopago.TipoPagoID.ToString() : tks;
                    oUDT.Name = tipopago.TipoPago ?? tks;
                    oUDT.UserFields.Fields.Item("U_Estatus").Value = 1; //(tipopago.Estatus ? 1 : 0);
                    int flag = oUDT.Add();
                    if (flag != 0)
                    {
                        int errornum;
                        string errormsj;
                        Company.GetLastError(out errornum, out errormsj);
                        throw new Exception("Error: " + errornum + " - " + errormsj);
                    }
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AutentificacionPresenter.cs->GetAllTipoPago()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void GetAllEstatusOrden()
        {
            try
            {
                var oUDT = Company.UserTables.Item("VSKF_ESTATUSORDEN");
                var list = this.generatEstatusOrden();
                foreach (EstatusOrden estatus in list)
                {
                    string tks = DateTime.Now.Ticks.ToString();
                    oUDT.Code = estatus.EstatusOrdenID.ToString();
                    oUDT.Name = estatus.Nombre;
                    //oUDT.UserFields.Fields.Item("U_EstatusOrdenID").Value = estatus.EstatusOrdenID;
                    //oUDT.UserFields.Fields.Item("U_Descripcion").Value = estatus.Nombre;
                    oUDT.UserFields.Fields.Item("U_Activo").Value = ((bool)estatus.EsActivo ? 1 : 0);
                    //oUDT.UserFields.Fields.Item("U_Sincronizado").Value = 1;
                    //oUDT.UserFields.Fields.Item("U_UUID").Value = Guid.NewGuid().ToString();
                    int flag = oUDT.Add();
                    if (flag != 0)
                    {
                        int errornum;
                        string errormsj;
                        Company.GetLastError(out errornum, out errormsj);
                        throw new Exception("Error: " + errornum + " - " + errormsj);
                    }
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AutentificacionPresenter.cs->GetAllEstatusOrden()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private List<EstatusOrden> generatEstatusOrden()
        {
            var result = new List<EstatusOrden>();
            result.Add(new EstatusOrden { EstatusOrdenID = 1, Nombre = "Solicitada", EsActivo = true });
            result.Add(new EstatusOrden { EstatusOrdenID = 2, Nombre = "Aprobada", EsActivo = true });
            result.Add(new EstatusOrden { EstatusOrdenID = 3, Nombre = "Rechazada", EsActivo = true });
            result.Add(new EstatusOrden { EstatusOrdenID = 4, Nombre = "Eliminada", EsActivo = true });
            result.Add(new EstatusOrden { EstatusOrdenID = 5, Nombre = "Atendida", EsActivo = true });
            result.Add(new EstatusOrden { EstatusOrdenID = 6, Nombre = "Realizada", EsActivo = true });
            return result;
        }

        private List<TipoServicio> generateTipoServicios(Configurations config)
        {
            var result = new List<TipoServicio>();
            result.Add(new TipoServicio { TipoServicioID = 1, Nombre = "Correctivo", EsActivo = true, Propietario = new Empresa{EmpresaID = config.UserFull.Dependencia.EmpresaID}});
            result.Add(new TipoServicio { TipoServicioID = 2, Nombre = "Preventivo", EsActivo = true, Propietario = new Empresa { EmpresaID = config.UserFull.Dependencia.EmpresaID } });
            return result;
        }

        #endregion
    }
}
