﻿using System;
using SAPbouiCOM;
using SAPinterface.Data;
using Application = System.Windows.Forms.Application;

namespace KananSAP.Autentificacion.Views
{
    public class AutentificacionView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.CheckBox oCheckBox;
        public SAPinterface.Data.UsuarioSAP Entity { get; set; }
        public SAPinterface.Data.EmpresaSAP EntityEmp { get; set; }
        public string FormUniqueID { get; set; }
        private XMLPerformance XmlApplication;
        #endregion

        #region Constructor
        public AutentificacionView(SAPbouiCOM.Application Application, SAPbobsCOM.Company company
            /*,string formtype, int formcount*/)
        {
            this.SBO_Application = Application;
            this.Entity = new UsuarioSAP(company);
            this.EntityEmp = new EmpresaSAP(company);
            this.XmlApplication = new XMLPerformance(SBO_Application);
            this.LoadForm();
            this.oItem = null;
            this.oEditText = null;
        }
        #endregion

        #region Metodos

        #region FormMethods
        public void SetForm(string type, int count)
        {
            try
            {
                if (count > 1)
                {
                    this.oForm = SBO_Application.Forms.GetForm(type, count);
                    this.oForm.Close();
                    this.oForm = SBO_Application.Forms.GetForm(type, 1);
                    this.oForm.Select();
                    this.FormUniqueID = this.oForm.UniqueID;
                }
                else
                {
                    this.oForm = SBO_Application.Forms.GetForm(type, count);
                    this.FormUniqueID = this.oForm.UniqueID;
                }
            }
            catch (Exception ex)
            {
                this.oForm = null;
                throw new Exception(ex.Message);
            }
        }

        public void LoadForm()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("LogginKF");   
            }
            catch (Exception)
            {
                string sPath = Application.StartupPath;
                this.XmlApplication.LoadFromXML(sPath + "\\XML", "LogginForm.xml");
                this.oForm = SBO_Application.Forms.Item("LogginKF");
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
            }
        }

        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
            }
            catch (Exception)
            {
                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                this.oForm.Select();
            }
        }

        public void CloseForm()
        {
            this.oForm.Close();
        }

        public void FormToEntity()
        {
            this.oItem = this.oForm.Items.Item("txtusersap");
            this.oEditText = (SAPbouiCOM.EditText)(this.oItem.Specific);
            this.Entity.oUser.UserCode = oEditText.String;
            
            this.oItem = this.oForm.Items.Item("txtkfuser");
            this.oEditText = (SAPbouiCOM.EditText)(this.oItem.Specific);
            this.Entity.oEmpleado.Usuario.Nombre = oEditText.String;

            this.oItem = this.oForm.Items.Item("txtkfpass");
            this.oEditText = (SAPbouiCOM.EditText)(this.oItem.Specific);
            this.Entity.oEmpleado.Usuario.Clave = oEditText.String;
        }
        #endregion

        #region ItemsMethods

        public bool IsChecked()
        {
            this.oItem = this.oForm.Items.Item("chkusersap");
            this.oCheckBox = (SAPbouiCOM.CheckBox) (this.oItem.Specific);
            return this.oCheckBox.Checked;
        }

        public void SetSystemUserLoggedNow(bool value)
        {
            this.oItem = this.oForm.Items.Item("txtusersap");
            this.oEditText = (SAPbouiCOM.EditText)(this.oItem.Specific);
            this.oEditText.String = value? SBO_Application.Company.UserName : string.Empty;
        }

        public Item GetItemFromForm(string ItemID)
        {
            try
            {
                oItem = this.oForm.Items.Item(ItemID);
                return oItem;
            }
            catch
            {
                return null;
            }
        }

        public bool ValidarDatos()
        {
            bool ItsOk= true;
            this.FormToEntity();

            if (string.IsNullOrEmpty(this.Entity.oUser.UserCode.Trim()))
            {
                ItsOk = false;
            }
            if (string.IsNullOrEmpty(this.Entity.oEmpleado.Usuario.Nombre.Trim()))
            {
                ItsOk = false;
            }
            if (string.IsNullOrEmpty(this.Entity.oEmpleado.Usuario.Clave.Trim()))
            {
                ItsOk = false;
            }
            return ItsOk;
        }


        #endregion

        #region Metodos Auxiliares
        public bool VerificaUsuarioSAP()
        {
            this.oItem = this.oForm.Items.Item("txtusersap");
            this.oEditText = (SAPbouiCOM.EditText)(this.oItem.Specific);
            this.Entity.oUser.UserCode = oEditText.String;
            return this.Entity.IsInUsersSAPList();
        }

        public void SetKFCompanyByEmployee()
        {
            this.EntityEmp.oEmpresa.Nombre = this.Entity.oEmpleado.Dependencia.Nombre;
            this.EntityEmp.oEmpresa.EmpresaID = this.Entity.oEmpleado.Dependencia.EmpresaID;
        }


        #endregion

        #endregion
    }
}
