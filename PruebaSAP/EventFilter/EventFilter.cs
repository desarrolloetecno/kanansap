﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//comentario para que chuco lovea :v
namespace KananSAP
{
    public class EventFilter
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;

        //**********************************************************
        // declaring an Event filters container object and an
        // event filter object
        //**********************************************************

        public SAPbouiCOM.EventFilters oFilters;

        public SAPbouiCOM.EventFilter oFilter;
        #endregion

        #region Constructor

        public EventFilter(SAPbouiCOM.Application application)
        {
            this.SBO_Application = application;
        }

        #endregion

        #region Metodos
        public void SetFilter()
        {

            // Create a new EventFilters object
            oFilters = new SAPbouiCOM.EventFilters();

            // add an event type to the container
            // this method returns an EventFilter object
            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);

            // assign the form type on which the event would be processed
            oFilter.AddEx("AsignMail");//Asignar Correos de Alertas
            oFilter.AddEx("150"); // Orders Form
            oFilter.AddEx("GesCPorte"); //Formulario carta porte         
            oFilter.AddEx("133");//factura clientes
            oFilter.AddEx("GestConf"); //Formulario configuraciones
            oFilter.AddEx("GestVieI"); //Formulario itinerario
            oFilter.AddEx("0");
            oFilter.AddEx("134"); //Gestión de unidades SAP B1.
            oFilter.AddEx("13000005");
            oFilter.AddEx("198");
            oFilter.AddEx("188");
            oFilter.AddEx("LogginKF");
            //Se agregó para controlar eventos de precion de botónes en SAP B1 R.Santos
            oFilter.AddEx("migrador");
            oFilter.AddEx("Gestoria");
            oFilter.AddEx("GestInci");
            oFilter.AddEx("EInci");
            oFilter.AddEx("GestCarga");
            oFilter.AddEx("CargaCom");
            oFilter.AddEx("UsersForm");
            oFilter.AddEx("KFUser");
            oFilter.AddEx("ServiceForm");
            oFilter.AddEx("ConfSvForm");
            oFilter.AddEx("TServicio");
            oFilter.AddEx("AddServ");
            oFilter.AddEx("LstTServ");
            //Lista de servicios configurados a vehiculo
            oFilter.AddEx("LstServ");
            oFilter.AddEx("FormLstTVh");
            oFilter.AddEx("AdmLlantas");
            oFilter.AddEx("CfgLlanta");
            oFilter.AddEx("AdmCausaraiz");
            oFilter.AddEx("AddUpdateCR");
            //oFilter.AddEx("MigDat");
            oFilter.AddEx("Llanta");
            //Agregar Tipo de Vehiculo
            oFilter.AddEx("FormTVh");
            //Confiogurar parametro para tipo de vehiculo
            oFilter.AddEx("FormCPSTVh");
            oFilter.AddEx("FrmCLstTVh");
            oFilter.AddEx("FrmConCor");
            //Orden de serivio con alerta generada
            oFilter.AddEx("FrmOrdSv");
            //Orden de serivio sin alerta generada
            oFilter.AddEx("frmAddOS");
            //Orden de serivio con alerta generada
            oFilter.AddEx("frmConOS");
            //Visor de Alertas
            oFilter.AddEx("FrmAlerta");
            //SucursalAdmon
            oFilter.AddEx("frmScAdmon");
            //Sucursal Nuevo/Actualizar
            oFilter.AddEx("frmScNew");
            //Sucursal Nuevo SAP
            oFilter.AddEx("943");
            oFilter.AddEx("1473000075");
            oFilter.AddEx("AdminOServ");
            oFilter.AddEx("AdminOServ_1");
            


            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD);
            // assign the form type on which the event would be processed
            oFilter.AddEx("150"); // Orders Form
            oFilter.AddEx("133"); // Factura Clientes            
            //oFilter.AddEx("EInci");
            //oFilter.AddEx("GestInci");


            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE);
            // assign the form type on which the event would be processed
            oFilter.AddEx("150"); // Factura Clientes

            // this method returns an EventFilter object
            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_LOAD);

            // assign the form type on which the event would be processed
            oFilter.AddEx("150"); // Orders Form
            oFilter.AddEx("133"); // Factura Clientes
            oFilter.AddEx("134");
            oFilter.AddEx("13000005"); //Gestión de unidades SAP B1.
            oFilter.AddEx("KFUser");
            //Se agregó para controlar el evento de cargar ventanas en SAP B1 R.Santos
            oFilter.AddEx("Gestoria");
            //oFilter.AddEx("GestInci");
            oFilter.AddEx("EInci");
            oFilter.AddEx("CargaCom");
            oFilter.AddEx("AddServ");
            oFilter.AddEx("LstTServ");
            //Lista de servicios configurados a vehiculo
            oFilter.AddEx("LstServ");
            oFilter.AddEx("FormLstTVh");
            //Configurar servicio a vehiculo
            oFilter.AddEx("ConfSvForm");
            //Agregar Tipo de Vehiculo
            oFilter.AddEx("FormTVh");
            //Confiogurar parametro para tipo de vehiculo
            oFilter.AddEx("FormCPSTVh");
            oFilter.AddEx("FrmCLstTVh");
            //Orden de serivio con alerta generada
            oFilter.AddEx("FrmOrdSv");
            //Orden de serivio sin alerta generada
            oFilter.AddEx("frmAddOS");
            //Orden de serivio con alerta generada
            oFilter.AddEx("frmConOS");
            //Sucursal Nuevo/Actualizar 
            oFilter.AddEx("frmScNew");
            oFilter.AddEx("1473000075");



            // this method returns an EventFilter object
            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE);

            // assign the form type on which the event would be processed
            oFilter.AddEx("150"); // Orders Form
            oFilter.AddEx("134");
            oFilter.AddEx("13000005"); //Gestión de unidades SAP B1.
            //Se agregó para controlar el evento de formulario activo en SAP B1 R.Santos
            oFilter.AddEx("Gestoria");
            oFilter.AddEx("GestInci");
            oFilter.AddEx("EInci");
            oFilter.AddEx("GestCarga");
            oFilter.AddEx("133");//factura clientes
            oFilter.AddEx("CargaCom");
            oFilter.AddEx("KFUser");
            oFilter.AddEx("UsersForm");
            oFilter.AddEx("ServiceForm");
            //Configurar servicio a vehiculo
            oFilter.AddEx("ConfSvForm");
            oFilter.AddEx("TServicio");
            oFilter.AddEx("AddServ");
            oFilter.AddEx("LstTServ");

            //Lista de servicios configurados a vehiculo
            oFilter.AddEx("LstServ");
            oFilter.AddEx("FormLstTVh");
            //Agregar Tipo de Vehiculo
            oFilter.AddEx("FormTVh");
            //Confiogurar parametro para tipo de vehiculo
            oFilter.AddEx("FormCPSTVh");
            oFilter.AddEx("FrmCLstTVh");
            oFilter.AddEx("FrmConCor");
            //Orden de serivio con alerta generada
            oFilter.AddEx("FrmOrdSv");
            //Orden de serivio sin alerta generada
            oFilter.AddEx("frmAddOS");
            //Orden de serivio sin alerta generada
            oFilter.AddEx("frmConOS");
            //Visor de Alertas
            oFilter.AddEx("FrmAlerta");
            //SucursalAdmon
            oFilter.AddEx("frmScAdmon");
            //Sucursal Nuevo/Actualizar
            oFilter.AddEx("frmScNew");




            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DEACTIVATE);
            // assign the form type on which the event would be processed
            oFilter.AddEx("150"); // Orders Form
            //Se agregó para controlar el evento de formulario desactivado en SAP B1 R.Santos
            oFilter.AddEx("Gestoria");
            oFilter.AddEx("EInci");
            oFilter.AddEx("GestInci");
            oFilter.AddEx("CargaCom");
            oFilter.AddEx("GestCarga");
            oFilter.AddEx("KFUser");
            oFilter.AddEx("UsersForm");
            oFilter.AddEx("ServiceForm");
            //Configurar servicio a vehiculo
            oFilter.AddEx("ConfSvForm");
            oFilter.AddEx("TServicio");
            oFilter.AddEx("AddServ");
            oFilter.AddEx("LstTServ");
            //Lista de servicios configurados a vehiculo
            oFilter.AddEx("LstServ");
            oFilter.AddEx("FormLstTVh");
            //Agregar Tipo de Vehiculo
            oFilter.AddEx("FormTVh");
            //Confiogurar parametro para tipo de vehiculo
            oFilter.AddEx("FormCPSTVh");
            oFilter.AddEx("FrmCLstTVh");
            //Orden de serivio con alerta generada
            oFilter.AddEx("FrmOrdSv");
            //Orden de serivio sin alerta generada
            oFilter.AddEx("frmAddOS");
            //Orden de serivio sin alerta generada
            oFilter.AddEx("frmConOS");
            //Visor de Alertas
            oFilter.AddEx("FrmAlerta");
            //SucursalAdmon
            oFilter.AddEx("frmScAdmon");
            //Sucursal Nuevo/Actualizar
            oFilter.AddEx("frmScNew");



            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE);
            // assign the form type on which the event would be processed
            //Se agregó para controlar el evento CLOSE de las ventanas en SAP B1 R.Santos
            oFilter.AddEx("Gestoria");
            oFilter.AddEx("EInci");
            oFilter.AddEx("GesCPorte");
            oFilter.AddEx("GestInci");
            oFilter.AddEx("CargaCom");
            oFilter.AddEx("GestCarga");
            //oFilter.AddEx("GestInci");
            oFilter.AddEx("KFUser");
            oFilter.AddEx("ACXBrowser");
            oFilter.AddEx("ServiceForm");
            oFilter.AddEx("AdmCausaraiz");
            oFilter.AddEx("AddUpdateCR");        
            //Configurar servicio a vehiculo
            oFilter.AddEx("ConfSvForm");
            oFilter.AddEx("TServicio");
            oFilter.AddEx("AddServ");
            oFilter.AddEx("LstTServ");
            //Lista de servicios configurados a vehiculo
            oFilter.AddEx("LstServ");
            oFilter.AddEx("FormLstTVh");
            oFilter.AddEx("Llanta");
            //Agregar Tipo de Vehiculo
            oFilter.AddEx("FormTVh");
            //Confiogurar parametro para tipo de vehiculo
            oFilter.AddEx("FormCPSTVh");
            oFilter.AddEx("FrmCLstTVh");
            oFilter.AddEx("FrmConCor");
            //Orden de serivio con alerta generada
            oFilter.AddEx("FrmOrdSv");
            //Orden de serivio sin alerta generada
            oFilter.AddEx("frmAddOS");
            //Orden de serivio sin alerta generada
            oFilter.AddEx("frmConOS");
            //Visor de Alertas
            oFilter.AddEx("FrmAlerta");
            //Sucursal Nuevo/Actualizar
            oFilter.AddEx("frmScNew");
            //SucursalAdmon
            oFilter.AddEx("frmScAdmon");


            // this method returns an EventFilter object
            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD);

            // assign the form type on which the event would be processed
            oFilter.AddEx("10003");
            oFilter.AddEx("150"); oFilter.AddEx("188");
            //Se agregó para controlar el evento FORM_UNLOAD en SAP B1 R.Santos
            oFilter.AddEx("Gestoria");
            oFilter.AddEx("EInci");
            oFilter.AddEx("GestInci");
            oFilter.AddEx("CargaCom");
            oFilter.AddEx("GestCarga");
            oFilter.AddEx("KFUser");
            //Lista de servicios configurados a vehiculo
            oFilter.AddEx("LstServ");
            //Configurar servicio a vehiculo
            oFilter.AddEx("ConfSvForm");
            //Agregar Tipo de Vehiculo
            oFilter.AddEx("FormTVh");
            //Agregar tipo de servicio
            oFilter.AddEx("TServicio");
            //Agregar Servicios
            oFilter.AddEx("AddServ");
            //Confiogurar parametro para tipo de vehiculo
            oFilter.AddEx("FormCPSTVh");
            //Sucursal Nuevo/Actualizar
            oFilter.AddEx("frmScNew");
            //SucursalAdmon
            oFilter.AddEx("frmScAdmon");
            //Orden de serivio con alerta generada
            oFilter.AddEx("FrmOrdSv");


            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED);
            oFilter.AddEx("UsersForm"); // Orders Form
            oFilter.AddEx("ServiceForm"); oFilter.AddEx("188");
            oFilter.AddEx("LstTServ");
            //Lista de servicios configurados a vehiculo
            oFilter.AddEx("LstServ");
            oFilter.AddEx("FormLstTVh");
            oFilter.AddEx("FrmCLstTVh");
            oFilter.AddEx("AdmCausaraiz");
            oFilter.AddEx("AddUpdateCR");
            //Se agregó para controlar el evento de precionar en matriz en SAP B1 R.Santos
            oFilter.AddEx("Gestoria");
            oFilter.AddEx("GestInci");
            oFilter.AddEx("GestCarga");
            //oFilter.AddEx("GestInci");
            //Visor de Alertas
            oFilter.AddEx("FrmAlerta");
            //SucursalAdmon
            oFilter.AddEx("frmScAdmon");
            oFilter.AddEx("AdmLlantas");
            oFilter.AddEx("CfgLlanta");
            oFilter.AddEx("GestVieI");
            oFilter.AddEx("FormCPSTVh");

            

            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_CLICK);
            //Configurar servicio a vehiculo
            oFilter.AddEx("ConfSvForm");
            oFilter.AddEx("FormCPSTVh");
            

            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_KEY_DOWN);
            oFilter.AddEx("ServiceForm");
            //Configurar servicio a vehiculo
            oFilter.AddEx("ConfSvForm");
            oFilter.AddEx("FormCPSTVh");

            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK);
            oFilter.AddEx("198");
            oFilter.AddEx("188");
            oFilter.AddEx("FrmAlerta");

            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT);
            //Orden de serivio con alerta generada
            oFilter.AddEx("FrmOrdSv");
            //Orden de serivicio sin alerta generada
            oFilter.AddEx("frmAddOS");
            //Orden de servicio con alerta generada
            oFilter.AddEx("frmConOS");
            //Módulo de gestoría
            oFilter.AddEx("Gestoria");

            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST);
            // assign the form type on which the event would be processed
            oFilter.AddEx("Llanta");
            oFilter.AddEx("GestConf");
            oFilter.AddEx("GestVieI");
            oFilter.AddEx("GesCPorte");
            oFilter.AddEx("frmAddOS");
            oFilter.AddEx("frmConOS");
            oFilter.AddEx("KFUser");
            oFilter.AddEx("AddServ");
            oFilter.AddEx("frmScNew");
            oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_MENU_CLICK);

            // assign the form type on which the event would be processed

            SBO_Application.SetFilter(oFilters);

        }
        #endregion

    }
}