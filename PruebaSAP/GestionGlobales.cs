﻿using KananSAP.Configuracion.Configuraciones.Objects;
using KananSAP.Helper;
using KananWS.Interface;
using SAPbobsCOM;
using SAPbouiCOM;
using SAPinterface.Data.INFO.Tablas;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP
{
    public class GestionGlobales
    {
        #region Atributos
        public static Boolean bSqlConnection = false;
        public SAPbouiCOM.Application SBO_Application;
        public SAPbobsCOM.Company Company;
        public Configurations configurations;
        public static int? CurrentVehiculoID { get; set; }
        public static string CurrentVehiculoName { get; set; }
        public static string sURLWs = string.Empty;
        public static string sURLReport = string.Empty;
        public static bool ventanaAbierta;

        #endregion

        /*
         * Clase para el control de consultas y ventos auxiliares
         * **/

        public GestionGlobales(Boolean bConexion, SAPbobsCOM.Company Company, SAPbouiCOM.Application SBO_Application, Configurations configurations)
        {
            this.configurations = configurations;
            this.SBO_Application = SBO_Application;
            this.Company = Company;
            bSqlConnection = bConexion;
            GestionGlobales.sURLWs = System.Configuration.ConfigurationManager.AppSettings.Get("WS_URL");
            GestionGlobales.sURLReport = System.Configuration.ConfigurationManager.AppSettings.Get("WS_Report"); 
        }
        public GestionGlobales() { }

        public String GenerarUUID(String DatoAmarre1, String DatoAmerre2, int iLongitud = 15)
        {
            String oTimer = DateTime.Now.ToString("yyyyMMddHHmmss");
            String UID = Guid.NewGuid().ToString();
            UID = UID.Substring(0, UID.IndexOf('-'));
            return string.Format("{0}{1}{2}{3}", DatoAmarre1, DatoAmerre2, oTimer, UID);
        }

        public void Mostrarmensaje(String Mensaje, BoMessageTime TiempoMensaje, BoStatusBarMessageType Tipo, Boolean bPausar = false)
        {
            try
            {
                this.SBO_Application.StatusBar.SetText(Mensaje, TiempoMensaje, Tipo);
                if (bPausar)
                    System.Threading.Thread.Sleep(3000);
            }
            catch { }
        }
        public void FillCombo(string DisplaySelect, String Key, String Description, String UICombo, String Query, Form oForm, SAPbobsCOM.Company oCompany = null)
        {
            if (this.Company == null)
                this.Company = oCompany;
            Recordset rs = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            Item oItem = oForm.Items.Item(UICombo);
            ComboBox oCombo = oItem.Specific;
            KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);
            oCombo.ValidValues.Add("0", DisplaySelect);
            rs.DoQuery(Query);

            if (rs.RecordCount > 0)
            {
                for (int i = 0; i < rs.RecordCount; i++)
                {
                    string value = Convert.ToString(rs.Fields.Item(Key).Value);
                    string description = string.Format("{0} - {1}", rs.Fields.Item(Key).Value, rs.Fields.Item(Description).Value);
                    oCombo.ValidValues.Add(value.Trim(), description.Trim());
                    rs.MoveNext();
                }
                oCombo.Item.DisplayDesc = true;
            }
        }

        #region Consultas 

        public static String ConceptoFuel(string Code)
        {
            String strQuery = "";
              strQuery = string.Format(@"SELECT * FROM ""@VSKF_CONCEPTOFUEL"" WHERE ""Code"" = '{0}'", Code);

            return strQuery;
        }
        public static String AlmacenRecarga()
        {
            String strQuery = "";
             strQuery = @"SELECT ""WhsCode"",""WhsName"" FROM ""OWHS""";
            return strQuery;
        }

        public string TipoEmpleadoCode(SAPbobsCOM.Company oCompany, String sTipo)
        {
            String strQuery = "", sTabla = "", iCode = "-1";

            if (GestionGlobales.bSqlConnection)
                sTabla = "OSLP";
            else
                sTabla = @"""OSLP""";

            strQuery = string.Format("SELECT * FROM {0} WHERE {1} = '{2}'", sTabla, GestionGlobales.bSqlConnection ? "SlpName" : @"""SlpName""", sTipo);
            Recordset oGrupo = (Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            oGrupo.DoQuery(strQuery);
            if (oGrupo.RecordCount > 0)
            {
                oGrupo.MoveFirst();
                iCode = Convert.ToString(oGrupo.Fields.Item("SlpCode").Value);

            }
            return iCode;
        }
        public String iFiltroGrupo(SAPbobsCOM.Company oCompany, String Filtro)
        {

            String strQuery = "", sTabla = "", iFiltro = "-1";

            if (GestionGlobales.bSqlConnection)
                sTabla = "OITB";
            else
                sTabla = @"""OITB""";

            strQuery = string.Format("SELECT * FROM {0} WHERE {1} = '{2}'", sTabla, GestionGlobales.bSqlConnection ? "ItmsGrpNam" : @"""ItmsGrpNam""", Filtro);
            Recordset oGrupo = (Recordset)oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            oGrupo.DoQuery(strQuery);
            if (oGrupo.RecordCount > 0)
            {
                oGrupo.MoveFirst();
                iFiltro = Convert.ToString(oGrupo.Fields.Item("ItmsGrpCod").Value);

            }
            return iFiltro;
        }

        public static String Notificarcorreo(SBO_KF_Configuracion_INFO KNCfing, String MailTo, String Asunto,
            String HTMLBody, String Attachs = "", String MailCC = "")
        {
            String response = "OK", proceso = "";

            try
            {
                if (MailTo == "" || MailTo.ToUpper().Trim() == "NULL")
                    return "Error al enviar correo: No se ha determinado un correo electrónico";
                System.Net.Mail.MailMessage correo = new System.Net.Mail.MailMessage();
                correo.From = new System.Net.Mail.MailAddress(KNCfing.CorreoCuenta/*nombre de cuenta*/);
                proceso = string.Format("To: {0}|CC: {1}|CCO: {2}|Asunto: {3}|Attachments: {4}", MailTo, MailCC, "", Asunto, Attachs);
                try
                {
                    foreach (string em in MailTo.Split(';'))
                    {
                        if (!string.IsNullOrEmpty(em))
                            correo.To.Add(em);
                    }
                }
                catch { correo.To.Add(MailTo); }

                if (MailCC != "")
                {
                    try
                    {
                        foreach (string emCCO in MailCC.Split(';'))
                        {
                            if (!string.IsNullOrEmpty(emCCO))
                                correo.CC.Add(emCCO);
                        }
                    }
                    catch { }
                }



                correo.Subject = Asunto;
                correo.Body = HTMLBody;

                correo.IsBodyHtml = true;
                correo.Priority = System.Net.Mail.MailPriority.Normal;
                if (Attachs != "")
                {
                    var rutas = Attachs.Split('|');
                    System.Net.Mail.Attachment at;
                    foreach (String r in rutas)
                    {
                        if (!string.IsNullOrEmpty(r))
                        {
                            if (File.Exists(r))
                            {
                                at = new System.Net.Mail.Attachment(r);
                                correo.Attachments.Add(at);
                            }
                        }

                    }

                }
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                smtp.Host = KNCfing.Server;//servidor de correos
                smtp.Port = KNCfing.Puerto; //puerto de salida
                smtp.Credentials = new System.Net.NetworkCredential(
                    KNCfing.CorreoCuenta, //cuenta de correo
                    KNCfing.PassWord //contraseña de cuenta
                    );
                if (KNCfing.cSSL == 'Y')
                    smtp.EnableSsl = true;  
                smtp.Send(correo);
                response = "OK";
            }
            catch (Exception ex)
            {
                response = "Error al enviar correo: " + ex.Message;
                KananSAPHerramientas.LogError("GestionGlobales.cs->Notificarcorreo()", ex.Message);
            }

            return response;
        }
        #endregion
    }
}