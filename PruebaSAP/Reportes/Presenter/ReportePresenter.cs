﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KananSAP.Reportes.View;

namespace KananSAP.Reportes.Presenter
{
    public class ReportePresenter
    {
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        public Reportes.View.ReporteView view { get; set; }
        public bool logged;
        private bool DoAction;
    
        #region Constructor
        public ReportePresenter(SAPbouiCOM.Application sApplication, SAPbobsCOM.Company Cmpny, bool login, string id, string uri)
        {
            this.SBO_Application = sApplication;
            this.Company = Cmpny;
            this.view = new ReporteView(SBO_Application,Company, uri, id);
            this.DoAction = true;
            //SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);

            //SBO_Application.MenuEvent += new SAPbouiCOM._IApplicationEvents_MenuEventEventHandler(SBO_Application_MenuEvent);
        }
#endregion
    }
}
