﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SHDocVw;
using System.Diagnostics;

namespace KananSAP.Reportes.View
{
    public class ReporteView
    {
        private string URL;
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company oCompany;
        private SAPbouiCOM.FormCreationParams CP;
        private SAPbouiCOM.Form oForm; 
        private SAPbouiCOM.ActiveX AcXTree;
        private SAPbouiCOM.Item oItem;
        private string uniqueID;

        public int vehiculoid { get; set; }
        private Process oProcess;
        #region Constructor

        public ReporteView(SAPbouiCOM.Application application, SAPbobsCOM.Company comp, string url, string id)
        {
            this.SBO_Application = application;
            this.oCompany = comp;
            this.URL = url;
            this.uniqueID = id;
            //LoadForm();
            //this.WebBrowserChen = new WebBrowserClass();
            //this.ShowForm();
        }

        #endregion

        public void LoadForm()
        {
            try
            {
                ////  Set the form creation parameters
                //CP = ((SAPbouiCOM.FormCreationParams)(SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)));
                //CP.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Fixed;
                //CP.FormType = "ACXBrowser";
                //CP.UniqueID = uniqueID;

                ////  Add a new form
                //oForm = SBO_Application.Forms.AddEx(CP);
                //oForm.Left = 100;
                //oForm.Top = 100;
                //oForm.Width = 1400;
                //oForm.Height = 1200;
                //oForm.Title = "Reportes KananFleet";

                ////  Add the TreeView Control to the form
                //oItem = oForm.Items.Add("Tree", SAPbouiCOM.BoFormItemTypes.it_ACTIVE_X);
                //oItem.Left = 20;
                //oItem.Top = 40;
                //oItem.Width = oForm.Width - 40;
                //oItem.Height = oForm.Height - 40;

                //SAPbouiCOM.ActiveX oActive = ((SAPbouiCOM.ActiveX)(oItem.Specific));
                ////  Create the new activeX control
                //AcXTree = ((SAPbouiCOM.ActiveX)(oItem.Specific));
                //AcXTree.ClassID = "Shell.Explorer.2";
                ////AcXTree.ClassID = "MSComctlLib.TreeView"; 
                ////oActive = ((MSComctlLib.TreeView)(AcXTree.Object)); 
                //var WebBrowserChen = ((SHDocVw.WebBrowser)(oActive.Object));
                ////WebBrowserChen.Navigate2(@"http://test.administraflotilla/ReportesSAP/ListadoCatalogoVehiculos");
                //WebBrowserChen.Navigate2(this.URL);
                //WebBrowserChen.Silent = false;

                this.oProcess = Process.Start( this.URL);
            }
            catch 
            {
                
            }
        }

        public void ShowForm()
        {
            this.LoadForm();
            //try
            //{
            //    this.oForm.Visible = true;
            //    this.oForm.Select();
            //}
            //catch (Exception)
            //{
            //    this.LoadForm();
            //    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
            //    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
            //    this.oForm.Visible = true;
            //    this.oForm.Select();
            //}
        }

        public void Refresh()
        {
            //oForm = SBO_Application.Forms.Item(uniqueID);
            //oItem = oForm.Items.Item("Tree");

            //SAPbouiCOM.ActiveX oActive = ((SAPbouiCOM.ActiveX)(oItem.Specific));
            ////  Create the new activeX control
            //AcXTree = ((SAPbouiCOM.ActiveX)(oItem.Specific));
            //AcXTree.ClassID = "Shell.Explorer.2";
            ////AcXTree.ClassID = "MSComctlLib.TreeView"; 
            ////oActive = ((MSComctlLib.TreeView)(AcXTree.Object)); 
            //var WebBrowserChen = ((SHDocVw.WebBrowser)(oActive.Object));
            ////WebBrowserChen.Navigate2(@"http://test.administraflotilla/ReportesSAP/ListadoCatalogoVehiculos");
            //WebBrowserChen.Navigate2(this.URL);
            this.oProcess.Refresh();
        }

        public void Close()
        {
            this.oProcess.Close();
            //this.oProcess.MainWindowHandle
        }
    }
}
