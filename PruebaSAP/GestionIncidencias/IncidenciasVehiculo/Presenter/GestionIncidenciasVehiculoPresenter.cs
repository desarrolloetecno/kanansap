﻿using KananSAP.GestionIncidencias.IncidenciasVehiculo.View;
using KananWS.Interface;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.GestionIncidencias.IncidenciasVehiculo.Presenter
{
    public class GestionIncidenciasVehiculoPresenter
    {
        #region Atributo
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private SAPbouiCOM.Item oItem;
        public GestionIncidenciasVehiculoView view { get; set; }
        //public RecargaView cargaview { get; set; }
        //private RecargaCombustibleWS.LogSAPCargaCombustible log;
        private Configurations configs;
        //private RecargaCombustibleWS cargaws;
        private bool Modal;


        public Reportes.Presenter.ReportePresenter ReportesPresenter { get; set; }
        private string EndPoint;
        #endregion

         #region Constructor
        public GestionIncidenciasVehiculoPresenter(SAPbouiCOM.Application aplication, SAPbobsCOM.Company company, int id, string vehiclename, Configurations c)
        {
            this.configs = c;
            this.SBO_Application = aplication;
            this.Company = company;
            this.view = new GestionIncidenciasVehiculoView(this.SBO_Application, this.Company, vehiclename, id);
            //this.cargaws = new RecargaCombustibleWS(Guid.Parse(this.configs.UserFull.Usuario.PublicKey), this.configs.UserFull.Usuario.PrivateKey);
            //this.log = new RecargaCombustibleWS.LogSAPCargaCombustible();
            this.Modal = false;
            //this.EndPoint = System.Configuration.ConfigurationManager.AppSettings.Get("EndPoint");
            //SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
            //cargaws.ObtenerUnidadVolumen();
        }
        #endregion

        #region Eventos
        private void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (pVal.FormTypeEx == "GestInci")
                {
                    if (Modal)
                    {
                        //cargaview.ShowForm();
                        BubbleEvent = false;
                        return;
                    }
                    #region beforeaction

                    if (pVal.BeforeAction)
                    {
                        //if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnSearch")
                        //{
                        //    this.view.BindDataToForm();
                        //}
                        if (pVal.EventType == BoEventTypes.et_FORM_LOAD)
                        {

                        }
                        //if (pVal.EventType == BoEventTypes.et_MATRIX_LINK_PRESSED)
                        //{
                        //    this.view.SetID(pVal.Row);
                        //    if (this.view.id != null)
                        //    {
                        //        this.cargaview = new RecargaView(this.SBO_Application, this.Company, configs, view.name, view.vehiculoid, view.id);
                        //        var code = ((int)this.view.id).ToString().Trim();
                        //        var flag = this.cargaview.Entity.oUDT.GetByKey(code);
                        //        if (!flag)
                        //        {
                        //            int errornum;
                        //            string errormsj;
                        //            Company.GetLastError(out errornum, out errormsj);
                        //            throw new Exception("Error: " + errornum + " - " + errormsj);
                        //        }
                        //        this.cargaview.LoadForm();
                        //        PrepareUserForm();
                        //        this.cargaview.ShowForm();
                        //    }
                        //    this.Modal = true;
                        //    BubbleEvent = false;
                        //}

                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnnew")
                        {
                            //this.cargaview = null;
                            //this.cargaview = new RecargaView(this.SBO_Application, this.Company, configs, view.name, view.vehiculoid, null);
                            //this.cargaview.LoadForm();
                            //this.cargaview.ShowForm();
                            //this.Modal = true;
                            //BubbleEvent = false;
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnextraer")
                        {
                            //this.ExtraerCargasKanan();
                            //this.view.BindDataToForm();
                            //BubbleEvent = false;
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "MtxFR")
                        {
                            //this.view.SetID(pVal.Row);
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnEdCarga")
                        {
                            //this.view.GetCargaFromGrid();
                            //if (this.view.id != null)
                            //{
                            //    this.cargaview = new RecargaView(this.SBO_Application, this.Company, configs, view.name, view.vehiculoid, view.id);
                            //    var code = ((int)this.view.id).ToString().Trim();
                            //    var flag = this.cargaview.Entity.oUDT.GetByKey(code);
                            //    if (!flag)
                            //    {
                            //        int errornum;
                            //        string errormsj;
                            //        Company.GetLastError(out errornum, out errormsj);
                            //        throw new Exception("Error: " + errornum + " - " + errormsj);
                            //    }
                            //    this.cargaview.LoadForm();
                            //    PrepareUserForm();
                            //    this.cargaview.ShowForm();
                            //}
                            //this.Modal = true;
                            //BubbleEvent = false;

                        }

                    }
                    #endregion

                    #region After Action
                    else
                    {
                        //if (Modal && pVal.EventType == BoEventTypes.et_FORM_ACTIVATE)
                        //{
                        //    userview.ShowForm();
                        //    BubbleEvent = false;
                        //    return;
                        //}
                    }
                    #endregion
                }

                if (pVal.FormTypeEx == "CargaCom")
                {
                    #region beforeaction
                    //  If the modal form is closed...
                    //if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_CLOSE & Modal)
                    //{
                    //    Modal = false;
                    //}
                    if (pVal.BeforeAction)
                    {
                        if (pVal.ItemUID == "btnguardar" && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED && Modal)
                        {
                            //this.cargaview.ValidateData();
                            //this.GuardarCargaCombustible();
                            //this.cargaview.CloseForm();
                            //this.view.BindDataToForm();
                            //BubbleEvent = false;
                        }
                        if (pVal.ItemUID == "btnelimina" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED && Modal)
                        {
                            //this.Delete();
                            //BubbleEvent = false;
                            //this.cargaview.CloseForm();
                            //this.view.BindDataToForm();
                        }
                        if (pVal.ItemUID == "btnphoto" && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED && Modal)
                        {
                            //this.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepFotos", @EndPoint + "Vehiculos/ImagenesRecarga.aspx?_modal=true&CargaCombustibleID=" + this.cargaview.Entity.carga.CargaCombustibleID);
                            ////this.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepFotos", @"http://w.administraflotilla.com/Vehiculos/ImagenesRecarga.aspx?_modal=true&CargaCombustibleID=6757");
                            //this.ReportesPresenter.view.ShowForm();
                            //BubbleEvent = false;
                        }
                        if (pVal.ItemUID == "btngeo" && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED && Modal)
                        {
                            //this.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepGeopos", @EndPoint + "Vehiculos/GeoposicionRecarga.aspx?_modal=true&CargaCombustibleID=" + this.cargaview.Entity.carga.CargaCombustibleID);
                            ////this.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepGeopos", @"http://w.administraflotilla.com/Vehiculos/GeoposicionRecarga.aspx?_modal=true&CargaCombustibleID=6757");
                            //this.ReportesPresenter.view.ShowForm();
                            //BubbleEvent = false;
                        }
                        if (pVal.EventType == BoEventTypes.et_FORM_UNLOAD)
                        {
                            this.Modal = false;
                            BubbleEvent = false;
                        }

                    }
                    #endregion

                    #region After Action
                    else
                    {
                        //if (pVal.EventType == BoEventTypes.et_FORM_LOAD)
                        //{
                        //    if (this.view.id != null)
                        //    {
                        //        Exist = this.userview.Entity.SetEmployeeRelationById((int)this.view.id);
                        //        PrepareUserForm();
                        //    }
                        //}
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                BubbleEvent = false;
                KananSAP.Helper.KananSAPHerramientas.LogError("GestionIncidenciasVehiculoPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
                SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
            }
        }
        #endregion
    }
}
