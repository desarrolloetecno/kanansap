﻿using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application = System.Windows.Forms.Application;

namespace KananSAP.GestionIncidencias.IncidenciasVehiculo.View
{
    public class GestionIncidenciasVehiculoView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.StaticText oStaticText;
        private SAPbouiCOM.LinkedButton oLinkedButton;
        private SAPbouiCOM.Matrix oMtx;
        private SAPbouiCOM.DBDataSource oDBDataSource;
        private SAPbouiCOM.Columns oColumns;
        private SAPbouiCOM.Column oColumn;
        private SAPbouiCOM.Button oButton;
        private SAPbouiCOM.Conditions oConditions;
        private SAPbouiCOM.Condition oCondition;
        private SAPbouiCOM.Grid oGridDatos;
        //public CargaCombustibleSAP Entity { get; set; }
        public string FormUniqueID { get; set; }
        private XMLPerformance XmlApplication;
        public int? id;
        public string name;
        public int? vehiculoid;

        #endregion

        #region Constructor
        public GestionIncidenciasVehiculoView(SAPbouiCOM.Application Application, SAPbobsCOM.Company company, string Name, int? id)
        {
            this.SBO_Application = Application;
            //this.Entity = new CargaCombustibleSAP(company);
            this.XmlApplication = new XMLPerformance(SBO_Application);
            //this.LoadForm();
            this.oItem = null;
            this.oLinkedButton = null;
            this.oStaticText = null;
            this.oEditText = null;
            this.oConditions = null;
            this.oCondition = null;
            this.oColumns = null;
            this.oColumn = null;
            this.id = null;
            this.name = Name;
            this.vehiculoid = id;
        }
        #endregion

        #region Metodos
        #region Metodos Form
        public void LoadForm()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("GestInci");
                BindDataToForm();
            }
            catch
            {
                string sPath = Application.StartupPath;                
                this.XmlApplication.LoadFromXML(sPath + "\\XML",
                        "GestionInci.xml");
                this.oForm = SBO_Application.Forms.Item("GestInci");

                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;

                //oItem = oForm.Items.Item("txtvname");
                //oStaticText = (StaticText)(oItem.Specific);
                //oStaticText.Caption = this.name;

                //oItem = oForm.Items.Item("lblvid");
                //oStaticText = (StaticText)(oItem.Specific);
                //oStaticText.Caption = this.vehiculoid.ToString();
                //oItem.Visible = false;
            }
        }

        public void ShowForm()
        {


            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
                //BindDataToForm();
            }
            catch (Exception)
            {
                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                //BindDataToForm();
                this.oForm.Select();
            }
        }

        #endregion
        #region Metodos Matrix
        public void BindDataToForm()
        {
            
//            SetConditions();
//            string sQuery = string.Format(@"
//                            SELECT Code, U_Fecha, U_Ticket, U_Litros, 
//                                (
//	                                CASE WHEN U_UVolumenID  IS NULL THEN ''
//		                                ELSE
//			                                CASE U_UVolumenID
//				                                WHEN 1 THEN 'Litros'
//				                                WHEN 2 THEN 'Galones'
//				                                WHEN NULL THEN ''
//				                                ELSE CAST(U_UVolumenID AS varchar(20))
//			                                END
//	                                END
//                                ) AS U_UnidadVolumen, U_Pesos
//                            FROM [@VSKF_CRGCOMBUSTIBLE]
//                            {0}
//                            ", SetConditionsQuery());


//            this.oForm.DataSources.DataTables.Item("VKF_DT_Comb").ExecuteQuery(sQuery);
//            oGridDatos = (Grid)oForm.Items.Item("MtxFR").Specific;
//            this.oGridDatos.Columns.Item(0).TitleObject.Caption = "Código";
//            this.oGridDatos.Columns.Item(1).TitleObject.Caption = "Fecha Carga";
//            this.oGridDatos.Columns.Item(2).TitleObject.Caption = "Ticket";
//            this.oGridDatos.Columns.Item(3).TitleObject.Caption = "Vol. Carga";
//            this.oGridDatos.Columns.Item(4).TitleObject.Caption = "Unidad";
//            this.oGridDatos.Columns.Item(5).TitleObject.Caption = "Precio";

//            this.oGridDatos.Columns.Item(0).Type = BoGridColumnType.gct_EditText;
//            EditTextColumn oEditTextColumn;
//            oEditTextColumn = (EditTextColumn)this.oGridDatos.Columns.Item(0);

//            oEditTextColumn.LinkedObjectType = "U_VSKF_CrgCombustible";
        }

        public string GetDataRow(int index)
        {
            //try
            //{
            //    oItem = oForm.Items.Item("MtxFR");
            //    oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
            //    oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(1, index);
            //    return oEditText.String;
            //}
            //catch (Exception ex)
            //{
            //    return "Error en la aplicacion";
            //}
            return String.Empty;
        }
        public void GetCargaFromGrid()
        {
            //try
            //{
            //    if (this.id != null && this.id > -1)
            //    {
            //        oGridDatos = this.oForm.Items.Item("MtxFR").Specific;
            //        this.id = Convert.ToInt32(oGridDatos.DataTable.Columns.Item(0).Cells.Item((int)this.id).Value);

            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(ex.Message);
            //}
        }
        public void SetRowIndex(int rowIndex)
        {
        //    try
        //    {
        //        this.id = rowIndex;
        //    }
        //    catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        public void SetID(int index)
        {
            //try
            //{
            //    //oItem = oForm.Items.Item("MtxFR");
            //    //oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
            //    //oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(1, index);
            //    ////this.oItem = this.oForm.Items.Item("txtempl");
            //    ////this.oEditText = (SAPbouiCOM.EditText)(this.oItem.Specific);
            //    //this.id = Convert.ToInt32(oEditText.String.Trim());
            //    this.id = index;
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(ex.Message);
            //}
        }

        public void SetConditions()
        {
            //try
            //{
            //    oConditions = new SAPbouiCOM.Conditions();
            //    oItem = oForm.Items.Item("lblvid");
            //    oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            //    if (!string.IsNullOrEmpty(oStaticText.Caption))
            //    {
            //        oCondition = oConditions.Add();
            //        oCondition.Alias = "U_VehiculoID";
            //        oCondition.Operation = BoConditionOperation.co_EQUAL;
            //        oCondition.CondVal = oStaticText.Caption.Trim();
            //        //oCondition.Relationship = BoConditionRelationship.cr_OR;
            //    }
            //    else
            //    {
            //        oConditions = null;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(ex.Message);
            //}
        }
        public string SetConditionsQuery()
        {
            //string sQuery = "";
            //bool bAND = false;
            //try
            //{
            //    oConditions = new SAPbouiCOM.Conditions();
            //    oItem = oForm.Items.Item("lblvid");
            //    oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            //    if (!string.IsNullOrEmpty(oStaticText.Caption))
            //    {
            //        sQuery += bAND ? string.Format(" AND U_VehiculoID = {0}", oStaticText.Caption.Trim()) :
            //            string.Format(" WHERE U_VehiculoID = {0}", oStaticText.Caption.Trim());
            //        bAND = true;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(ex.Message);
            //}
            //return sQuery;
            return String.Empty;
        }

        #endregion
    }
}
