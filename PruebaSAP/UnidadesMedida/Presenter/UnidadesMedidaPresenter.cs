﻿#region C#
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion C#

#region Kananfleet
using Kanan.Comun.BO2;
using KananWS.Interface;
using KananSAP.Comun.Data;
using KananSAP.UnidadesMedida.View;
#endregion Kananfleet

#region SAP
using SAPbouiCOM;
#endregion SAP

namespace KananSAP.UnidadesMedida.Presenter
{
    public class UnidadesMedidaPresenter
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private SAPbouiCOM.Item oItem;

        private List<UnidadesMedidaView> Views;
        private UnidadesMedidaView view;
        private UnidadMedidaWS UnidadMedidaWS;
        private BoFormMode mode;

        private bool UnidadMedida;
        public bool IsLogged { get; set; }
        private Configurations configs;
        private Helper.KananSAPHerramientas oHerramientas;

        private string captionbefore;
        #endregion Atributos

        #region Constructor
        public UnidadesMedidaPresenter(SAPbouiCOM.Application application, SAPbobsCOM.Company company, Configurations c)
        {
            this.SBO_Application = application;
            this.Company = company;
            this.configs = c;
            this.Views = new List<UnidadesMedidaView>();
            this.UnidadMedida = false;
            try
            {
                this.UnidadMedidaWS = new UnidadMedidaWS(Guid.Parse(this.configs.UserFull.Usuario.PublicKey), this.configs.UserFull.Usuario.PrivateKey);
            }
            catch
            {
                this.UnidadMedidaWS = new UnidadMedidaWS(new Guid(), string.Empty);
            }

            oHerramientas = new Helper.KananSAPHerramientas(this.SBO_Application);
        }
        #endregion Constructor

        #region Métodos

        private void SetActiveView(string FormUID)
        {
            this.view = Views.First(x => x.FormUniqueID == FormUID);
        }

        #region Eventos

        public void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            if (!IsLogged) return;
            string group = string.Empty;
            try
            {
                #region Form Datos Maestros de Socios de Negocios

                if (pVal.FormType == 13000005)
                {
                    #region Before Action
                    if (pVal.Before_Action)
                    {
                        if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_LOAD)
                        {
                            this.Views.Add(new UnidadesMedidaView(this.SBO_Application, this.Company, pVal.FormTypeEx, pVal.FormTypeCount));
                            this.SetActiveView(FormUID);
                            this.view.Entity.ListOld = this.view.Entity.GetList();
                        }

                        if (pVal.ItemUID == "10000001" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                        {
                            mode = this.view.GetFormMode();
                            oItem = SBO_Application.Forms.GetForm(pVal.FormTypeEx, pVal.FormTypeCount).Items.Item("10000001");
                            var btn = (SAPbouiCOM.Button)(oItem.Specific);
                            captionbefore = btn.Caption;
                        }
                    }
                    #endregion Before Action

                    #region After Action
                    else
                    {
                        if (pVal.EventType == BoEventTypes.et_FORM_UNLOAD)
                        {
                            this.Views.Remove(this.Views.First(x => x.FormUniqueID == FormUID));
                            this.view = null;
                        }

                        if (pVal.ItemUID == "10000001" & pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            switch (mode)
                            {
                                case BoFormMode.fm_ADD_MODE:
                                    this.view.FormToEntity();
                                    InsertarActualizarEliminar();
                                    break;
                                case BoFormMode.fm_UPDATE_MODE:
                                    this.view.FormToEntity();
                                    InsertarActualizarEliminar();
                                    break;
                            }

                            mode = BoFormMode.fm_OK_MODE;
                            BubbleEvent = false;
                        }
                    }
                    #endregion After Action
                }
                #endregion Form Datos Maestros de Socios de Negocios
            }
            catch (Exception ex)
            {
                BubbleEvent = false;
                oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            }
        }

        #endregion Eventos

        #region Acciones
        public void InsertarActualizarEliminar()
        {
            /*Se inicializan variables.*/
            view.Entity.ListToInsert = null;
            view.Entity.ListToUpdate = null;
            view.Entity.ListToDelete = null;
            view.Entity.ListToInsert = new List<UnidadMedida>();
            view.Entity.ListToUpdate = new List<UnidadMedida>();
            view.Entity.ListToDelete = new List<UnidadMedida>();
            try
            {
                /*Se validan listas null. */
                if (view != null & view.Entity != null)
                {
                    if (view.Entity.ListNew != null & view.Entity.ListOld != null)
                    {
                        #region Llenando
                        for (int i = 0; i < view.Entity.ListNew.Count; i++)
                        {
                            #region Explicación
                            /*
                             * Se valida la existencia de algun campo para acualizarlo
                             * independientemente de su modificación para evitar controlar
                             * la captura de las litas insertar y actualizar.
                             */
                            #endregion Explicación
                            if (view.Entity.ExistSAP(view.Entity.ListNew[i]))
                            {
                                view.Entity.ListToUpdate.Add(view.Entity.ListNew[i]);
                            }
                            else
                            {
                                #region Explicación
                                /*
                                 * Se pregunta si no existe con el objetivo para confirmar
                                 * la inserción del registro.
                                 */
                                #endregion Explicación
                                if (!view.Entity.ExistSAP(view.Entity.ListNew[i]))
                                {
                                    view.Entity.ListToInsert.Add(view.Entity.ListNew[i]);
                                }
                            }
                        }

                        for (int j = 0; j < view.Entity.ListOld.Count; j++)
                        {
                            #region Explicación
                            /*
                             * Se recorre la lista inicial la cual se captura en
                             * el evento del cargado del formulario para obtener
                             * todos los valores y comprar la existencia de los
                             * mismos en la base de datos en el evento AfterAction
                             * para obtener la lista de los valores a eliminar
                             * en el sincronizado de las unidades.
                             */
                            #endregion Exlicación
                            if (!view.Entity.ExistSAP(view.Entity.ListOld[j]))
                            {
                                view.Entity.ListToDelete.Add(view.Entity.ListOld[j]);
                            }
                        }
                        #endregion Llenando

                        #region Procesando

                        #region Explicación
                        /*
                         * Se recorren todas las lista para realizar
                         * su rescpectiva accion, insertar,actualizar
                         * o eliminar en la lista correspondiente.
                         */
                        #endregion Explicación
                        for (int k = 0; k < view.Entity.ListToInsert.Count; k++)
                        {
                            view.Entity.unidad = null;
                            view.Entity.unidad = view.Entity.ListToInsert[k];

                            if (view.Entity.ExistKF(view.Entity.unidad))
                            {
                                if (view.Entity.unidad.Code != "-1")
                                {
                                    UnidadMedidaWS.Update(view.Entity.unidad);
                                    view.Entity.Update();
                                }
                            }
                            else
                            {
                                UnidadMedidaWS.Add(view.Entity.unidad);
                                view.Entity.Insertar();
                            }
                        }

                        for (int l = 0; l < view.Entity.ListToUpdate.Count; l++)
                        {
                            view.Entity.unidad = null;
                            view.Entity.unidad = view.Entity.ListToUpdate[l];

                            if (view.Entity.ExistKF(view.Entity.unidad))
                            {
                                UnidadMedidaWS.Update(view.Entity.unidad);
                                view.Entity.Update();
                            }
                            else
                            {
                                UnidadMedidaWS.Add(view.Entity.unidad);
                                view.Entity.Insertar();
                            }
                        }

                        for (int m = 0; m < view.Entity.ListToDelete.Count; m++)
                        {
                            view.Entity.unidad = null;
                            view.Entity.unidad = view.Entity.ListToDelete[m];

                            if (view.Entity.ExistKF(view.Entity.unidad))
                            {
                                UnidadMedidaWS.Delete(Convert.ToInt32(view.Entity.unidad.UnidadMedidaID));
                                view.Entity.Delete();
                            }
                        }
                        #endregion Procesando
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar actualizar un registros de la lista. " + ex.Message);
            }
        }

        #endregion Acciones

        #endregion Métodos
    }
}

