﻿#region C#
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion C#

#region Kananfleet
using KananSAP.Comun.Data;
#endregion Kananfleet

#region SAP
using SAPbouiCOM;
#endregion SAP

namespace KananSAP.UnidadesMedida.View
{
    public class UnidadesMedidaView
    {
        #region Atributos

        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oNewItem;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.Folder oFolderItem;
        private SAPbouiCOM.StaticText oStaticText;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.Grid oGrid;
        private SAPbouiCOM.ComboBox oComboBox;
        private SAPbouiCOM.Button oButton;
        public UnidadesMedidaSAP Entity { get; private set; }
        public string FormUniqueID { get; private set; }
        SAPbobsCOM.Company company;
        #endregion Atributos

        #region Contructor

        public UnidadesMedidaView(SAPbouiCOM.Application Application, SAPbobsCOM.Company company, string formtype, int formcount)
        {
            this.company = company;
            this.Entity = new UnidadesMedidaSAP(company);
            this.SBO_Application = Application;
            this.SetForm(formtype, formcount);
            this.oNewItem = null;
            this.oItem = null;
            this.oGrid = null;
            this.oFolderItem = null;
            this.oStaticText = null;
            this.oEditText = null;
            this.oComboBox = null;
        }

        #endregion Contructor

        #region Métodos

        public Item GetItemFromForm(string ItemID)
        {
            try
            {
                oItem = this.oForm.Items.Item(ItemID);
                return oItem;
            }
            catch
            {
                return null;
            }
        }

        public void SetForm(string type, int count)
        {
            try
            {
                this.oForm = SBO_Application.Forms.GetForm(type, count);
                this.FormUniqueID = this.oForm.UniqueID;
            }
            catch (Exception ex)
            {
                this.oForm = null;
                throw new Exception(ex.Message);
            }
        }

        public void SetFormMode(SAPbouiCOM.BoFormMode mode)
        {
            this.oForm.Mode = mode;
        }

        public BoFormMode GetFormMode()
        {
            return this.oForm != null ? this.oForm.Mode : new BoFormMode();
        }

        public void FormToEntity()
        {
            //Se obtiene la nueva lista para ellos es importante realizar el llamado en el AfterAction();
            //de esta manera se tiene la existente en el cargado del formulario y otra cuando se termine de insertar
            //es decir, en el AfterAction de la acción.

            SAPinterface.Data.EmpresaSAP EmpresaSAP = new SAPinterface.Data.EmpresaSAP(this.company);
            int EmpresaID = EmpresaSAP.GetEmpresaFromSAP();

            this.Entity.ListNew = this.Entity.GetList();

            for (int i = 0; i < Entity.ListNew.Count; i++)
            {
                Entity.ListNew[i].EmpresaID = EmpresaID;
            }
        }
        #endregion Métodos
    }
}
