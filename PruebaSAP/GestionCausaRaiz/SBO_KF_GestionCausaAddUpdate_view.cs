﻿using Kanan.Vehiculos.BO2;
using KananSAP;
using KananSAP.Configuracion.Configuraciones.Objects;
using KananSAP.GestionDatos.MigradorDatos.Librerias;
using KananSAP.Helper;
using KananWS.Interface;
using SAPbobsCOM;
using SAPbouiCOM;
using SAPinterface.Data.INFO.Objetos;
using SAPinterface.Data.PD;
using SAPinterface.Data.WS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application = System.Windows.Forms.Application;

namespace KananFleet.GestionCausaRaiz.Control
{
    public class SBO_KF_GestionCausaAddUpdate_view
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private Grid oMatDocs;
        private DataTable oDocsBase, oTableOCRD;
        private SAPbouiCOM.Matrix oMtx;
        private SAPbouiCOM.DBDataSource oDBDataSource;
        public Boolean bGuardado = false;
        private XMLPerformance XmlApplication;
        private GestionGlobales oGlobales = null;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private CheckBox oCheck;
        private SAPbouiCOM.ComboBox oCombo;
        private SAPbouiCOM.Conditions oConditions;
        private SAPbouiCOM.Condition oCondition;
        private string iConsecutivo = string.Empty;
        private SAPbobsCOM.Company oCompany;
        const string CONST_CLASE = "SBO_KF_GestionCausaAddUpdate_view.cs";
        private KananSAP.Helper.KananSAPHerramientas oHerramientas;
        private int CausaID = -1;
        private int TotalCausas = -1;
        private int rowID { get; set; }
        private bool bUpdateDetalle { get; set; }
        private int? CausaIDWs { get; set; }
        private List<SBO_KF_CAUSADETALLE_INFO> lstDetalle { get; set; }
        private SBO_KF_CRAIZ_INFO oCausa { get; set; }
        private Configurations oConfig = null;
        private int current_idselct = -1;
        private bool bEditCurrent = false;
        #endregion

        public SBO_KF_GestionCausaAddUpdate_view(GestionGlobales oGlobales, SAPbobsCOM.Company Company, Configurations oConfig, int CausaID = -1)
        {

            this.oConfig = oConfig;
            this.bUpdateDetalle = false;
            this.lstDetalle = new List<SBO_KF_CAUSADETALLE_INFO>();
            this.oCausa = new SBO_KF_CRAIZ_INFO();
            this.CausaID = CausaID;
            this.oGlobales = oGlobales;
            this.SBO_Application = this.oGlobales.SBO_Application;
            this.XmlApplication = new XMLPerformance(SBO_Application);
            this.oCompany = Company;
            Boolean bContinuar = LoadForm();
            this.ShowForm();
            this.oHerramientas = new KananSAPHerramientas(this.SBO_Application);
            this.current_idselct = -1;

        }

        private Boolean LoadForm()
        {
            string UID = "SBOCARDET";
            FormCreationParams oFormCreate = null;
            Boolean bContinuar = false;
            String XmlData = "";
            try
            {

                this.oForm = SBO_Application.Forms.Item(UID);
                if (oForm.UniqueID != UID)
                    throw new Exception("");
                bContinuar = true;
            }
            catch (Exception exp)
            {
                KananSAPHerramientas.LogError("SBO_KF_GestionCausaAddUpdate_view.cs->LoadForm()", exp.Message);
                bContinuar = false;
                try
                {
                    string sPath = Application.StartupPath;
                    XmlData = this.XmlApplication.LoadFromXML(sPath + "\\XML", "", "GestionCausaRaizDetalle.xml");
                    bContinuar = true;
                    oFormCreate = (FormCreationParams)this.oGlobales.SBO_Application.CreateObject(BoCreatableObjectType.cot_FormCreationParams);
                    oFormCreate.UniqueID = UID;
                    oFormCreate.FormType = "AddUpdateCR";
                    oFormCreate.XmlData = XmlData;

                    this.oForm = this.oGlobales.SBO_Application.Forms.AddEx(oFormCreate);//SBO_Application.Forms.Item(UID);

                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                    this.oForm.Title = CausaID > 0 ? "Actualizar sistemas" : "Agregar sistemas";
                    this.oItem = this.oForm.Items.Item("btnupdate");
                    Button btnUpdateAdd = (Button)this.oItem.Specific;
                    btnUpdateAdd.Caption = CausaID > 0 ? "Actualizar" : "Agregar";
                }
                catch (Exception ex)
                {
                    bContinuar = false;
                    KananSAPHerramientas.LogError("SBO_KF_GestionCausaAddUpdate_view.cs->LoadForm()", ex.Message);
                }
            }
            return bContinuar;
        }

        public void ShowForm()
        {
            try
            {

                this.oForm.Visible = true;
                this.oForm.Select();
                this.InicializaItems();
            }
            catch (Exception ex)
            {

                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                this.oForm.Select();
                this.InicializaItems();
                KananSAPHerramientas.LogError("SBO_KF_GestionCausaAddUpdate_view.cs->ShowForm()", ex.Message);

            }
        }

        void InicializaItems()
        {
            try
            {
                this.oCausa = new SBO_KF_CRAIZ_INFO();
                this.lstDetalle = new List<SBO_KF_CAUSADETALLE_INFO>();
                if (this.CausaID > 0)
                {
                    this.LlenaObjetosFormulario();
                    this.LlenaEntityForms();
                }
                else
                {
                    this.oItem = this.oForm.Items.Item("chkactivo");
                    this.oCheck = (CheckBox)this.oItem.Specific;
                    this.oCheck.Checked = true;
                }

                this.oItem = this.oForm.Items.Item("chkco");
                this.oCheck = (CheckBox)this.oItem.Specific;
                this.oCheck.Checked = true;
            }
            catch (Exception ex)
            {
                oHerramientas.ColocarMensaje(string.Format("No se ha encontrado información. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                KananSAPHerramientas.LogError("SBO_KF_GestionCausaRaiz.cs->ShowForm()", ex.Message);
            }

        }
        void LlenaObjetosFormulario()
        {
            try
            {
                List<SBO_KF_CAUSADETALLE_INFO> lstDetalleCausa = new List<SBO_KF_CAUSADETALLE_INFO>();
                List<SBO_KF_CRAIZ_INFO> lstCausas = new List<SBO_KF_CRAIZ_INFO>();
                SBO_KF_CRAIZ_INFO oTemp = new SBO_KF_CRAIZ_INFO();
                oTemp.Code = this.CausaID.ToString();
                SBO_KF_CausaRaiz_PD oControl = new SBO_KF_CausaRaiz_PD();
                string sresponse = oControl.listaCausaraiz(ref this.oCompany, ref lstCausas, oTemp, false);
                if (sresponse == "OK")
                {
                    this.oCausa = lstCausas[0];
                    this.CausaIDWs = Convert.ToInt32( this.oCausa.Name);
                    sresponse = oControl.listaCausaDetalle(ref this.oCompany, ref lstDetalleCausa, oCausa);
                    this.lstDetalle = lstDetalleCausa;
                    this.TotalCausas = this.lstDetalle.Count;
                }
                else throw new Exception("La información de causa raiz no se encuentra");

            }
            catch (Exception ex)
            {
                oHerramientas.ColocarMensaje(string.Format("No se ha podido recuperar la información de causa raíz. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                KananSAPHerramientas.LogError("SBO_KF_GestionCausaRaiz.cs->LlenaFormulario()", ex.Message);
            }
        }
        void LlenaEntityForms()
        {
            try
            {
                this.ComponenteFill();
                this.oItem = this.oForm.Items.Item("txtcausa");
                this.oEditText = (EditText)this.oItem.Specific;
                this.oEditText.String = this.oCausa.NombreSistema;

                this.oItem = this.oForm.Items.Item("chkactivo");
                this.oCheck = (CheckBox)this.oItem.Specific;
                this.oCheck.Checked = this.oCausa.Activo;
            }
            catch (Exception ex)
            {

            }
        }

        public void SeleccionaComponente()
        {
            try
            {
                if (current_idselct > 0)
                {
                    int cur = current_idselct - 1;
                    SBO_KF_CAUSADETALLE_INFO oComponente = this.lstDetalle[cur];
                    this.oItem = this.oForm.Items.Item("txtdesc");
                    this.oEditText = (EditText)this.oItem.Specific;
                    this.oEditText.String = oComponente.Componente;

                    this.oItem = this.oForm.Items.Item("chkco");
                    this.oCheck = (CheckBox)this.oItem.Specific;
                    this.oCheck.Checked = oComponente.Activo;
                    bEditCurrent = true;

                }
                else oHerramientas.ColocarMensaje("Favor de seleccionar un registro para su edición", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);

            }
            catch { }
        }

        void EliminaComponente()
        {
            try
            {
                if (this.rowID > 0)
                {
                    oItem = oForm.Items.Item("MtxFR");
                    oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                    oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(1, this.rowID);
                    Int32 DocumentID = Convert.ToInt32(oEditText.String.ToString().Trim());
                    var oComponente = this.lstDetalle.FirstOrDefault(x => x.Code.Equals(DocumentID.ToString(), StringComparison.InvariantCultureIgnoreCase));
                    if (oComponente != null)
                    {
                        this.lstDetalle.Remove(oComponente);
                        this.ComponenteFill();
                        this.bUpdateDetalle = true;
                    }
                    else oHerramientas.ColocarMensaje("Favor de seleccionar un registro", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                }
                else oHerramientas.ColocarMensaje("Favor de seleccionar un registro", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
            catch (Exception ex)
            {
                oHerramientas.ColocarMensaje(string.Format("No se ha podido remover el componente de la lista. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                KananSAPHerramientas.LogError("SBO_KF_GestionCausaRaiz.cs->EliminaComponente()", ex.Message);
            }
        }
        void ComponenteFill()
        {
            try
            {
                this.oForm.Freeze(true);
                if (lstDetalle.Count > 0)
                {
                    Matrix oCausamat = (Matrix)this.oForm.Items.Item("MtxFR").Specific;
                    Columns oColum = (SAPbouiCOM.Columns)oCausamat.Columns;
                    oCausamat.Clear();
                    int iLinea = 1;
                    foreach (SBO_KF_CAUSADETALLE_INFO data in lstDetalle)
                    {
                        this.oForm.DataSources.UserDataSources.Item("oUMAT01").Value = iLinea.ToString();
                        this.oForm.DataSources.UserDataSources.Item("oUMAT02").Value = data.Code.ToString();
                        this.oForm.DataSources.UserDataSources.Item("oUMAT03").Value = data.Componente;
                        this.oForm.DataSources.UserDataSources.Item("oUMAT04").Value = data.Activo ? "Y" : "N";
                        oCausamat.AddRow(1, -1);
                        iLinea++;
                    }
                }
                this.oForm.Freeze(false);
            }
            catch (Exception ex)
            {
                this.oForm.Freeze(false);
                oHerramientas.ColocarMensaje(string.Format("No se ha podido agregar el componente. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                KananSAPHerramientas.LogError("SBO_KF_GestionCausaRaiz.cs->ComponenteFill()", ex.Message);
            }
        }
        void AgregaComponente()
        {

            try
            {
                this.oItem = this.oForm.Items.Item("chkco");
                this.oCheck = (CheckBox)this.oItem.Specific;

                this.oItem = this.oForm.Items.Item("txtdesc");
                oEditText = (EditText)this.oItem.Specific;
                if (!string.IsNullOrEmpty(oEditText.String))
                {
                    if (bEditCurrent && current_idselct > 0)
                    {
                        SBO_KF_CAUSADETALLE_INFO ocomponetne = this.lstDetalle[current_idselct - 1];
                        ocomponetne.Componente = oEditText.String.ToString().Trim();
                        ocomponetne.Activo = this.oCheck.Checked;
                        var otmpcom = this.lstDetalle.FirstOrDefault(x => x.ComponenteID == ocomponetne.ComponenteID);
                        otmpcom = ocomponetne;

                    }
                    else
                    {
                        SBO_KF_CAUSADETALLE_INFO oComponente = this.lstDetalle.OrderByDescending(x => Convert.ToInt32(x.Code)).FirstOrDefault();
                        if (oComponente == null)
                        {
                            oComponente = new SBO_KF_CAUSADETALLE_INFO();
                            oComponente.Code = "1";
                        }

                        int iSiguiente = Convert.ToInt32(oComponente.Code);
                        iSiguiente++;
                        SBO_KF_CAUSADETALLE_INFO oTemp = new SBO_KF_CAUSADETALLE_INFO();
                        oTemp.Code = iSiguiente.ToString();
                        oTemp.Name = iSiguiente.ToString();
                        oTemp.Componente = oEditText.String.ToString().Trim();
                        oTemp.NivelID = this.CausaID.ToString();
                        oTemp.Activo = this.oCheck.Checked;


                        lstDetalle.Add(oTemp);
                    }
                    bEditCurrent = false;
                    current_idselct = -1;
                    oEditText.String = "";
                    this.ComponenteFill();
                    this.bUpdateDetalle = true;
                    this.oCheck.Checked = true;
                }
                else oHerramientas.ColocarMensaje("Favor de proporcionar la causa", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
            catch (Exception ex)
            {
                oHerramientas.ColocarMensaje(string.Format("No se ha podido agregar el componente. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                KananSAPHerramientas.LogError("SBO_KF_GestionCausaRaiz.cs->AgregaComponente()", ex.Message);
            }

        }

        bool FormToEntity()
        {
            bool bContinuar = false;
            try
            {
                this.oCausa = new SBO_KF_CRAIZ_INFO();

                this.oItem = this.oForm.Items.Item("chkactivo");
                this.oCheck = (CheckBox)this.oItem.Specific;
                this.oCausa.Activo = this.oCheck.Checked;
                this.oItem = this.oForm.Items.Item("txtcausa");
                this.oEditText = (EditText)this.oItem.Specific;
                if (!string.IsNullOrEmpty(this.oEditText.String))
                {
                    this.oCausa.NombreSistema = this.oEditText.String;
                    this.oCausa.Code = this.CausaID.ToString();
                    //this.oCausa.Name = this.CausaID.ToString();
                    if (this.lstDetalle.Count > 0)
                        bContinuar = true;
                    else oHerramientas.ColocarMensaje("Favor de proporcionar el detalle de componentes", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                }
                else oHerramientas.ColocarMensaje("Proporciona el nombre de la causa", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
            catch (Exception ex)
            {
                oHerramientas.ColocarMensaje(string.Format("No se ha podido verificar la informacion. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                KananSAPHerramientas.LogError("SBO_KF_GestionCausaRaiz.cs->FormToEntity()", ex.Message);
            }
            return bContinuar;
        }

        void AgregaUpdateCausa()
        {
            try
            {
                if (this.FormToEntity())
                {
                    string sresponse = string.Empty;
                    Int32 SistemaWebID = -1, ComponenteWebID = -1;
                    SBO_KF_CausaRaiz_PD oCausactrol = new SBO_KF_CausaRaiz_PD();
                    SBO_KF_CAUSARAIZ_WS oCausaWeb = new SBO_KF_CAUSARAIZ_WS();
                    this.oCompany.StartTransaction();
                    if (this.CausaID > 0)
                    {
                        //se debe actualizar el objeto
                        this.oCausa.Name = this.CausaIDWs.ToString();
                        sresponse = oCausaWeb.CreActualizaSistemateWebApi(GestionGlobales.sURLWs, this.oCausa, Convert.ToInt32(this.oConfig.UserFull.Dependencia.EmpresaID), true, out SistemaWebID);
                        if (sresponse == "OK")
                        {
                            sresponse = oCausactrol.InsertUpdateCausa(ref this.oCompany, this.oCausa, true);
                            if (sresponse == "OK")
                            {
                                if (this.bUpdateDetalle)
                                {
                                    long iCodigoSig = new Add(ref this.oCompany, null).recuperIDMaximoTabla("VSKF_CAUSADETALLE");
                                    if (iCodigoSig < 0)
                                        throw new Exception("No se ha podido recuperar el folio consecutivo");
                                    sresponse = oCausactrol.EliminaCausaDetalle(ref this.oCompany, this.CausaID);
                                    if (sresponse == "OK")
                                    {
                                        int ilinea = 1;
                                        foreach (SBO_KF_CAUSADETALLE_INFO oDetalle in this.lstDetalle)
                                        {
                                            bool bUpdateDetalle = false;
                                            if (ilinea <= this.TotalCausas)
                                                bUpdateDetalle = true;
                                            oDetalle.NivelID = SistemaWebID.ToString();
                                            sresponse = oCausaWeb.CreActualizaComponenteteWebApi(GestionGlobales.sURLWs, oDetalle, Convert.ToInt32(this.oConfig.UserFull.Dependencia.EmpresaID), bUpdateDetalle, out ComponenteWebID);
                                            if (sresponse.Equals("OK", StringComparison.InvariantCultureIgnoreCase) && ComponenteWebID > 0)
                                            {

                                                oDetalle.NivelID = this.CausaID.ToString();
                                                oDetalle.Code = iCodigoSig.ToString();
                                                oDetalle.Name = ComponenteWebID.ToString();
                                                sresponse = oCausactrol.InsertaCausaDetalle(ref this.oCompany, oDetalle);
                                                if (sresponse != "OK")
                                                    throw new Exception(string.Format("No se ha podido actualizar la información de la causa raíz. ERROR {0}", sresponse));
                                            }
                                            else throw new Exception(sresponse);
                                            iCodigoSig++;
                                            ilinea++;
                                        }
                                    }
                                    else throw new Exception(string.Format("No se ha podido actualizar la información de la causa raíz. ERROR {0}", sresponse));
                                }
                                oHerramientas.ColocarMensaje("Se ha actualizado la información de la causa raíz", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                            }
                            else throw new Exception(string.Format("No se ha podido actualizar la información de la causa raíz. ERROR {0}", sresponse));
                        }
                        else throw new Exception(sresponse);
                    }
                    else
                    {
                        //se debe crear un nuevo registro junto con su detalle 
                        sresponse = oCausaWeb.CreActualizaSistemateWebApi(GestionGlobales.sURLWs, oCausa, Convert.ToInt32(this.oConfig.UserFull.Dependencia.EmpresaID), false, out SistemaWebID);
                        if (sresponse.Equals("OK", StringComparison.InvariantCultureIgnoreCase) && SistemaWebID > 0)
                        {
                            long iCodigoPadre = new Add(ref this.oCompany, null).recuperIDMaximoTabla("VSKF_CAUSARAIZ");
                            this.oCausa.Name = SistemaWebID.ToString();
                            this.oCausa.Code = iCodigoPadre.ToString();
                            this.oCausa.FechaCreado = DateTime.Now;
                            sresponse = oCausactrol.InsertUpdateCausa(ref this.oCompany, this.oCausa, false);
                            if (sresponse == "OK")
                            {


                                long iCodigoSig = new Add(ref this.oCompany, null).recuperIDMaximoTabla("VSKF_CAUSADETALLE");
                                if (iCodigoSig < 0)
                                    throw new Exception("No se ha podido recuperar el folio consecutivo");
                                foreach (SBO_KF_CAUSADETALLE_INFO oDetalle in this.lstDetalle)
                                {
                                    oDetalle.NivelID = SistemaWebID.ToString();
                                    sresponse = oCausaWeb.CreActualizaComponenteteWebApi(GestionGlobales.sURLWs, oDetalle, Convert.ToInt32(this.oConfig.UserFull.Dependencia.EmpresaID), false, out ComponenteWebID);
                                    if (sresponse.Equals("OK", StringComparison.InvariantCultureIgnoreCase) && ComponenteWebID > 0)
                                    {
                                        oDetalle.NivelID = iCodigoPadre.ToString();
                                        oDetalle.Code = iCodigoSig.ToString();
                                        oDetalle.Name = ComponenteWebID.ToString();
                                        sresponse = oCausactrol.InsertaCausaDetalle(ref this.oCompany, oDetalle);
                                        if (sresponse != "OK")
                                            throw new Exception(string.Format("No se ha podido actualizar la información de la causa raíz. ERROR {0}", sresponse));
                                        iCodigoSig++;
                                    }
                                    else throw new Exception(sresponse);
                                }
                            }
                            else throw new Exception(string.Format("No se ha podido crear la información de la causa raíz. ERROR {0}", sresponse));
                            oHerramientas.ColocarMensaje("Se ha creado el registro de causa raíz", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                        }
                        else throw new Exception(sresponse);
                    }
                    this.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    this.SBO_Application.Forms.ActiveForm.Close();
                }
            }
            catch (Exception ex)
            {
                if (this.oCompany.InTransaction)
                    this.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                oHerramientas.ColocarMensaje(string.Format("No se ha podido realizar la operación sobre causa raíz. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                KananSAPHerramientas.LogError("SBO_KF_GestionCausaRaiz.cs->AgregaUpdateCausa()", ex.Message);
            }
        }

        public void EventoControl(ItemEvent pVal, out bool BubbleEvent)
        {
            #region Comentarios
            /* 
             */
            #endregion Comentarios

            BubbleEvent = true;
            try
            {

                if (!pVal.BeforeAction)
                {
                    #region After Action
                    switch (pVal.EventType)
                    {
                        case BoEventTypes.et_ITEM_PRESSED:
                            switch (pVal.ItemUID)
                            {
                                case "btndel":
                                    this.EliminaComponente();
                                    break;
                                case "MtxFR":
                                    this.rowID = pVal.Row;
                                    break;
                                case "btnadd":
                                    this.AgregaComponente();
                                    break;

                                case "btncancel":
                                    this.SBO_Application.Forms.ActiveForm.Close();
                                    break;

                                case "btnupdate":
                                    this.AgregaUpdateCausa();
                                    break;

                                case "btnselc":
                                    this.SeleccionaComponente();
                                    break;
                            }
                            break;
                        case BoEventTypes.et_MATRIX_LINK_PRESSED:

                            /*if (pVal.Row >= 0)
                            {
                                try
                                {
                                    oItem = oForm.Items.Item("MtxFR");
                                    oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                                    oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(1, pVal.Row);
                                    Int32 DocumentID = Convert.ToInt32(oEditText.String.ToString().Trim());
                                    oCausaView = new SBO_KF_GestionCausaAddUpdate_view(this.oGlobales, this.oCompany, DocumentID);
                                }
                                catch (Exception ex)
                                {
                                    oHerramientas.ColocarMensaje(string.Format("El registro no está disponible. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                }
                            }*/

                            break;

                        case BoEventTypes.et_CHOOSE_FROM_LIST:
                            switch (pVal.ItemUID)
                            {

                            }
                            break;

                            
                    }
                    if (pVal.ItemUID == "MtxFR" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        this.current_idselct = pVal.Row;
                    #endregion After Action
                }
            }
            catch (Exception ex)
            {

                KananSAPHerramientas.LogError(string.Format("{0}->EventoControl()", CONST_CLASE), ex.Message);
            }
        }
    }
}
