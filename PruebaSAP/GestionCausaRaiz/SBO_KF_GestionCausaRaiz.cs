﻿using Kanan.Vehiculos.BO2;
using KananSAP;
using KananSAP.Configuracion.Configuraciones.Objects;
using KananSAP.Helper;
using KananWS.Interface;
using SAPbobsCOM;
using SAPbouiCOM;
using SAPinterface.Data.INFO.Objetos;
using SAPinterface.Data.PD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Application = System.Windows.Forms.Application;

namespace KananFleet.GestionCausaRaiz.Control
{
    public class SBO_KF_GestionCausaRaiz
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private Grid oMatDocs;
        private DataTable oDocsBase, oTableOCRD;
        private SAPbouiCOM.Matrix oMtx;
        private SAPbouiCOM.DBDataSource oDBDataSource;
        public Boolean bGuardado = false;
        private XMLPerformance XmlApplication;
        private GestionGlobales oGlobales = null;
        private ConfiguracionView oInterfaz = null;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private CheckBox oCheck;
        private SAPbouiCOM.ComboBox oCombo;
        private SAPbouiCOM.Conditions oConditions;
        private SAPbouiCOM.Condition oCondition;
        private string iConsecutivo = string.Empty;
        private SAPbobsCOM.Company oCompany;
        const string CONST_CLASE = "SBO_KF_GestionCausaRaiz.cs";
        private KananSAP.Helper.KananSAPHerramientas oHerramientas;
        private SBO_KF_GestionCausaAddUpdate_view oCausaView = null;
        private Configurations oConfig = null;
        #endregion

        public SBO_KF_GestionCausaRaiz(GestionGlobales oGlobales, ref SAPbobsCOM.Company Company, Configurations oConfig)
        {

            this.oConfig = oConfig;
            this.oGlobales = oGlobales;
            this.SBO_Application = this.oGlobales.SBO_Application;
            this.XmlApplication = new XMLPerformance(SBO_Application);
            this.oCompany = Company;
            Boolean bContinuar = LoadForm();
            this.ShowForm();
            this.oHerramientas = new KananSAPHerramientas(this.SBO_Application);

        }

        private Boolean LoadForm()
        {
            string UID = "SBOCAR";
            FormCreationParams oFormCreate = null;
            Boolean bContinuar = false;
            String XmlData = "";
            try
            {

                this.oForm = SBO_Application.Forms.Item(UID);
                if (oForm.UniqueID != UID)
                    throw new Exception("");
                bContinuar = true;
            }
            catch (Exception exp)
            {
                KananSAPHerramientas.LogError("SBO_KF_GestionCausaRaiz.cs->LoadForm()", exp.Message);
                bContinuar = false;
                try
                {
                    string sPath = Application.StartupPath;
                    XmlData = this.XmlApplication.LoadFromXML(sPath + "\\XML", "", "GestionCausaRaiz.xml");
                    bContinuar = true;
                    oFormCreate = (FormCreationParams)this.oGlobales.SBO_Application.CreateObject(BoCreatableObjectType.cot_FormCreationParams);
                    oFormCreate.UniqueID = UID;
                    oFormCreate.FormType = "AdmCausaraiz";
                    oFormCreate.XmlData = XmlData;
                    this.oForm = this.oGlobales.SBO_Application.Forms.AddEx(oFormCreate);//SBO_Application.Forms.Item(UID);

                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;

                }
                catch (Exception ex)
                {
                    bContinuar = false;
                    KananSAPHerramientas.LogError("SBO_KF_GestionCausaRaiz.cs->LoadForm()", ex.Message);
                }
            }
            return bContinuar;
        }

        public void ShowForm()
        {
            try
            {

                this.oForm.Visible = true;
                this.oForm.Select();
                this.InicializaItems();
            }
            catch (Exception ex)
            {

                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                this.oForm.Select();
                this.InicializaItems();
                KananSAPHerramientas.LogError("SBO_KF_GestionCausaRaiz.cs->ShowForm()", ex.Message);

            }
        }

        void InicializaItems()
        {
            try
            {
                oItem = this.oForm.Items.Item("chkactivo");
                oCheck = (CheckBox)this.oItem.Specific;
                oCheck.Checked = true;
                this.BuscaCausas();
            }
            catch (Exception ex)
            {
                oHerramientas.ColocarMensaje(string.Format("No se ha encontrado información. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                KananSAPHerramientas.LogError("SBO_KF_GestionCausaRaiz.cs->ShowForm()", ex.Message);
            }

        }

        void BuscaCausas()
        {
            List<SBO_KF_CRAIZ_INFO> lstCusaRaiz = new List<SBO_KF_CRAIZ_INFO>();
            SBO_KF_CRAIZ_INFO oCausa = new SBO_KF_CRAIZ_INFO();
            bool bAnd = false;
            oConditions = new SAPbouiCOM.Conditions();
            oCondition = oConditions.Add();


            oItem = this.oForm.Items.Item("txtcausa");
            oEditText = (EditText)this.oItem.Specific;
            if (!string.IsNullOrEmpty(oEditText.String.ToString().Trim()))
            {
                oCondition.Alias = "U_Descripcion";
                oCondition.Operation = BoConditionOperation.co_CONTAIN;
                oCondition.CondVal = oEditText.String.ToString().Trim();
                bAnd = true;
            }

            string sFechaInicio = string.Empty, sFechaFin = string.Empty;
            oItem = this.oForm.Items.Item("txtdesde");
            oEditText = (EditText)this.oItem.Specific;
            sFechaInicio = oEditText.String.ToString().Trim();

            oItem = this.oForm.Items.Item("txthasta");
            oEditText = (EditText)this.oItem.Specific;
            sFechaFin = oEditText.String.ToString().Trim();

            if (!string.IsNullOrEmpty(sFechaInicio) && !string.IsNullOrEmpty(sFechaFin))
            {
                try
                {
                    if (bAnd)
                        oCondition.Relationship = BoConditionRelationship.cr_AND;
                    oCondition.Alias = "U_Fecharegistro";
                    oCondition.Operation = BoConditionOperation.co_GRATER_THAN;
                    oCondition.CondVal = Convert.ToDateTime(sFechaInicio).ToString("yyyyMMdd");

                    if (bAnd)
                        oCondition.Relationship = BoConditionRelationship.cr_AND;
                    oCondition.Alias = "U_Fecharegistro";
                    oCondition.Operation = BoConditionOperation.co_LESS_THAN;
                    oCondition.CondVal = Convert.ToDateTime(sFechaFin).ToString("yyyyMMdd");

                    bAnd = true;
                }
                catch { }
            }

            oItem = this.oForm.Items.Item("chkactivo");
            oCheck = (CheckBox)this.oItem.Specific;

            if (oCheck.Checked)
            {
                if (bAnd)
                    oCondition.Relationship = BoConditionRelationship.cr_AND;
                oCondition.Alias = "U_Activo";
                oCondition.Operation = BoConditionOperation.co_EQUAL;
                oCondition.CondVal = "Y";
                bAnd = true;
            }
            oDBDataSource = oForm.DataSources.DBDataSources.Item("@VSKF_CAUSARAIZ");
            oDBDataSource.Query(bAnd ? oConditions : null);
            oItem = oForm.Items.Item("MtxFR");
            oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
            oMtx.LoadFromDataSource();
        }

        public void EventoControl(ItemEvent pVal, out bool BubbleEvent)
        {
            #region Comentarios
            /* 
             */
            #endregion Comentarios

            BubbleEvent = true;
            try
            {

                if (!pVal.BeforeAction)
                {
                    #region After Action
                    switch (pVal.FormTypeEx)
                    {
                        case "AdmCausaraiz":
                            #region acciones de formulario que controla esta clase
                            switch (pVal.EventType)
                            {
                                case BoEventTypes.et_ITEM_PRESSED:
                                    switch (pVal.ItemUID)
                                    {
                                        case "btnbusca":
                                            this.BuscaCausas();
                                            break;
                                        case "btnadd":
                                            oCausaView = new SBO_KF_GestionCausaAddUpdate_view(this.oGlobales, this.oCompany, this.oConfig);
                                            break;
                                    }
                                    break;
                                case BoEventTypes.et_MATRIX_LINK_PRESSED:

                                    if (pVal.Row >= 0)
                                    {
                                        try
                                        {
                                            oItem = oForm.Items.Item("MtxFR");
                                            oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                                            oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(1, pVal.Row);
                                            Int32 DocumentID = Convert.ToInt32(oEditText.String.ToString().Trim());
                                            oCausaView = new SBO_KF_GestionCausaAddUpdate_view(this.oGlobales, this.oCompany, this.oConfig, DocumentID);
                                        }
                                        catch (Exception ex)
                                        {
                                            oHerramientas.ColocarMensaje(string.Format("El registro no está disponible. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                        }
                                    }

                                    break;

                                case BoEventTypes.et_CHOOSE_FROM_LIST:
                                    switch (pVal.ItemUID)
                                    {

                                    }
                                    break;
                            }
                            #endregion acciones de formulario que controla esta clase
                            break;

                        case "AddUpdateCR":
                            #region acciones de formulario que controla clase view
                            if (oCausaView != null)
                                oCausaView.EventoControl(pVal, out BubbleEvent);
                            #endregion acciones de formulario que controla clase view
                            if (pVal.EventType == BoEventTypes.et_FORM_CLOSE)
                            {
                                this.oItem = this.oForm.Items.Item("btnbusca");
                                Button btnsearch = (Button)this.oItem.Specific;
                                btnsearch.Item.Click();
                                //Thread.Sleep(500);
                                //this.BuscaCausas();
                            }
                            break;
                    }
                    #endregion After Action
                }

            }
            catch (Exception ex)
            {

                KananSAPHerramientas.LogError(string.Format("{0}->EventoControl()", CONST_CLASE), ex.Message);
            }
        }

    }
}