﻿using System;
using System.Web.UI;
using KananSAP.GestionLlantas.View;
using KananWS.Interface;
using SAPbouiCOM;
using Kanan.Llantas.BO2;
using KananSAP.GestionDatos.MigradorDatos.Librerias;
using KananSAP.Helper;

namespace KananSAP.GestionLlantas.Presenter
{
    public class GestionLlantasPresenter
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private SAPbouiCOM.Item oItem;
        public GestionLlantasVIew view { get; set; }
        public LlantaView llantaview { get; set; }
        private Configurations configs;
        private LlantasWS LlantaService;
        private bool Modal;
        private Helper.KananSAPHerramientas oHerramientas;
        public bool? EstaInstalado { get; set; }
        private static bool run { get; set; }
        #endregion

        #region Constructor
        public GestionLlantasPresenter(SAPbouiCOM.Application aplication, SAPbobsCOM.Company company, Configurations c)
        {
            this.configs = c;
            this.SBO_Application = aplication;
            this.Company = company;
            this.view = new GestionLlantasVIew(this.SBO_Application, this.Company);
            //this.userview = new UsuarioView(this.SBO_Application, this.Company, this.configs);
            this.LlantaService = new LlantasWS(Guid.Parse(this.configs.UserFull.Usuario.PublicKey), this.configs.UserFull.Usuario.PrivateKey);
            this.Modal = false;
            SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
            this.oHerramientas = new Helper.KananSAPHerramientas(this.SBO_Application);
        }
        #endregion

        #region Eventos
        private void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                #region Gestion de LLantas

                if (pVal.FormTypeEx == "AdmLlantas")
                {
                    if (Modal)
                    {
                        llantaview.ShowForm();
                        BubbleEvent = false;
                        return;
                    }

                    #region beforeaction

                    if (pVal.BeforeAction)
                    {
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnnew")
                        {
                            this.llantaview = null;
                            this.llantaview = new LlantaView(this.SBO_Application, this.Company, configs);
                            this.llantaview.LoadForm();
                            this.llantaview.ShowForm();
                            this.Modal = true;
                            BubbleEvent = false;
                        }

                        if (pVal.EventType == BoEventTypes.et_MATRIX_LINK_PRESSED)
                        {
                            if (pVal.Row > 0)
                                this.view.SetID(pVal.Row);
                            else
                                SBO_Application.StatusBar.SetText("Error en registro, verificar el campo Code.");
                            if (this.view.llantaid != null)
                            {
                                this.llantaview = new LlantaView(this.SBO_Application, this.Company, configs)
                                {
                                    id = this.view.llantaid
                                };
                                var code = ((int)this.view.llantaid).ToString();
                                var flag = this.llantaview.Entity.oUDT.GetByKey(code);
                                if (!flag)
                                {
                                    int errornum;
                                    string errormsj;
                                    Company.GetLastError(out errornum, out errormsj);
                                    throw new Exception("Error: " + errornum + " - " + errormsj);
                                }
                                this.llantaview.LoadForm();                                
                                PrepareUserForm();
                                this.llantaview.ShowForm();
                            }
                            this.Modal = true;
                            BubbleEvent = false;
                        }
                    }

                    #endregion
                }
                if (pVal.FormTypeEx == "Llanta")
                {
                    #region beforeaction
                    //  If the modal form is closed...
                    if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_CLOSE & Modal)
                    {
                        Modal = false;
                        BubbleEvent = false;
                        return;
                    }
                    if (pVal.BeforeAction)
                    {
                        if (pVal.ItemUID == "chkMsvo" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                            this.llantaview.HabilitaMasivo();

                        if (pVal.ItemUID == "btnguardar" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                        {
                            run = this.llantaview.ValidateLlanta();
                            if (run)
                            {
                                string sID = llantaview.GetLlantaID().Trim();
                                this.GuardarLlanta();
                                this.llantaview.CloseForm();
                                this.view.BindDataToForm();
                                BubbleEvent = false;                                
                                oHerramientas.ColocarMensaje( string.IsNullOrEmpty(sID) ?"El registro de llanta ha sido creado": "El registro ha sido actualizado con éxito", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                            }
                        }
                        if (pVal.ItemUID == "btnelimina" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.Delete();
                            this.llantaview.CloseForm();
                            this.view.BindDataToForm();
                            BubbleEvent = false;
                        }
                        if (pVal.EventType == BoEventTypes.et_FORM_UNLOAD)
                        {
                            this.Modal = false;
                        }
                    }

                    if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST)
                    {
                        SAPbouiCOM.IChooseFromListEvent oCFLEvento = null;
                        oCFLEvento = ((SAPbouiCOM.IChooseFromListEvent)(pVal));
                        string sCFL_ID = null;
                        sCFL_ID = oCFLEvento.ChooseFromListUID;
                        SAPbouiCOM.Form oForm = null;
                        oForm = SBO_Application.Forms.Item(FormUID);
                        SAPbouiCOM.ChooseFromList oCFL = null;
                        oCFL = oForm.ChooseFromLists.Item(sCFL_ID);
                        if (oCFLEvento.BeforeAction == false)
                        {
                            SAPbouiCOM.DataTable oDataTable = null;
                            oDataTable = oCFLEvento.SelectedObjects;
                            string val = null;
                            try
                            {
                                val = System.Convert.ToString(oDataTable.GetValue(0, 0));
                            }
                            catch (Exception ex)
                            {
                                KananSAP.Helper.KananSAPHerramientas.LogError("GestionLlantasPresenter.cs->ChoseFromList()", ex.Message);
                            }
                            if ((pVal.ItemUID == "txtitem"))
                            {
                                oForm.DataSources.UserDataSources.Item("EditDS").ValueEx = val;
                            }

                        }
                    }
                    #endregion
                }
                #endregion

                if (pVal.FormType == 10003)
                {
                    
                }
            }
            catch (Exception ex)
            {
                BubbleEvent = false;
                KananSAP.Helper.KananSAPHerramientas.LogError("GestionLlantasPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
                oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            }
        }
        #endregion

        #region Metodos

        #region Acciones
        private void GuardarLlanta()
        {
            try
            {
                this.Company.StartTransaction();
                Add oConnfigAdd = new Add(ref this.Company, null);
                long iCodigomx = oConnfigAdd.recuperIDMaximoTabla("VSKF_LLANTA");
                KananSAP.Helper.KananSAPHerramientas.LogError("GestionLlantasPresenter.cs->GuardarLlanta()", "Máximo ID" + iCodigomx.ToString());
                if (string.IsNullOrEmpty(llantaview.GetLlantaID().Trim()) || llantaview.GetLlantaID()=="0")
                {
                    int iCantidad = 0;
                    string sPrefijo = string.Empty;
                    if (this.llantaview.defineRepeticionInsert(ref iCantidad, ref sPrefijo))
                    {
                        for (int i = 0; i <iCantidad; i++)
                        {
                            
                            sPrefijo = KananSAPHerramientas.SiguienteSerie(sPrefijo);
                            this.llantaview.Entity.llanta.ConsecutivoName = string.Format("{0}{1}", sPrefijo, iCodigomx);
                            this.llantaview.Entity.llanta.LlantaID = Convert.ToInt32(iCodigomx);
                            Insert(iCodigomx.ToString(), sPrefijo);
                            iCodigomx++;
                        }
                    }
                    else Insert(iCodigomx.ToString(), string.Format("LP{0}", iCodigomx ));
                }
                else
                {
                    KananSAP.Helper.KananSAPHerramientas.LogError("GestionLlantasPresenter.cs->GuardarLlanta()", "Se intenta actualizar");
                    Update();
                }
                this.Company.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
            }
            catch (Exception ex)
            {
                if (this.Company.InTransaction)
                    this.Company.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                KananSAP.Helper.KananSAPHerramientas.LogError("GestionLlantasPresenter.cs->GuardarLlanta()", ex.Message);
                throw new Exception(ex.Message);
            }
        }
        

        private void Insert(string siguienteID, string sNamePrefij)
        {
            try
            {
                //this.llantaview.Entity.llanta.NumeroEconomico
                //Se implementó la propiedad "Esta instalado" para evitar actualizar con false en caso contradio (Si estuviera instalada).
                this.EstaInstalado = false;
                this.llantaview.FormToEntity();
                this.llantaview.Entity.Sincronizado = 1;
                this.llantaview.Entity.UUID = Guid.NewGuid();
                //this.llantaview.Entity.LlantaToUDT();
                this.llantaview.Entity.llanta.EstaInstalado = this.EstaInstalado;
                this.llantaview.Entity.llanta = LlantaService.Add(this.llantaview.Entity.llanta);///WS revisar
                this.llantaview.Entity.llanta.ConsecutivoCode = siguienteID;
                this.llantaview.Entity.llanta.ConsecutivoName = sNamePrefij;
                this.llantaview.Entity.Sincronizado = 0;
                this.llantaview.Entity.AddUpdateUDOLlanta();
                //this.llantaview.Entity.LlantaToUDT();
                //this.llantaview.Entity.llanta.EstaInstalado = this.EstaInstalado;                
                /*try
                {
                    this.llantaview.Entity.Insert();
                }
                catch (Exception ex)
                {
                    //Se eliminan registros en línea. 
                    LlantaService.Delete(Convert.ToInt32(llantaview.Entity.llanta.LlantaID));
                    KananSAP.Helper.KananSAPHerramientas.LogError("GestionLlantasPresenter.cs->Insert()", ex.Message);
                    throw new Exception(ex.Message);
                }*/
                this.llantaview.Entity.ActualizarCode(this.llantaview.Entity.llanta);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("GestionLlantasPresenter.cs->Insert()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void Update()
        {
            try
            {
                this.EstaInstalado = LlantaService.Get(Convert.ToInt32(this.llantaview.Entity.llanta.LlantaID)).EstaInstalado;

                this.llantaview.Entity.llanta.EstaInstalado = this.EstaInstalado;
                if (this.llantaview.FormToEntity())
                {
                    this.llantaview.Entity.Sincronizado = 2;
                    this.llantaview.Entity.llanta.EstaInstalado = this.EstaInstalado;
                    //this.llantaview.Entity.LlantaToUDT();
                    this.llantaview.Entity.llanta.EstaInstalado = this.EstaInstalado;
                    this.llantaview.Entity.Sincronizado = 1;
                    this.llantaview.Entity.AddUpdateUDOLlanta(false, this.llantaview.Entity.llanta.LlantaID.ToString());
                    this.llantaview.Entity.llanta = LlantaService.Update(this.llantaview.Entity.llanta);
                    //this.llantaview.Entity.Update();


                    //this.llantaview.Entity.ActualizarCode(this.llantaview.Entity.llanta);
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("GestionLlantasPresenter.cs->Update()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void Delete()
        {
            try
            {
                this.llantaview.Entity.llanta = this.LlantaService.Delete((int) this.llantaview.Entity.llanta.LlantaID);
                this.llantaview.Entity.Code = this.llantaview.Entity.llanta.LlantaID.ToString();
                this.llantaview.Entity.Deactivate();
                this.llantaview.EmptyEntityToForm();
                this.oHerramientas.ColocarMensaje("Operación exitosa", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("GestionLlantasPresenter.cs->Delete()", ex.Message);
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Metodos Auxiliares
        private void PrepareUserForm()
        {
            try
            {
                var id = this.llantaview.Entity.oUDT.Code;
                var i = Convert.ToInt32(id);
                this.llantaview.Entity.Code = i.ToString();
                this.llantaview.Entity.UDTToLlanta();
                this.llantaview.EntityToForm();
            }
            catch (Exception ex)
            {
                this.llantaview.EmptyEntityToForm();
                this.llantaview.CloseForm();
                KananSAP.Helper.KananSAPHerramientas.LogError("GestionLlantasPresenter.cs->PrepareUserForm()", ex.Message);
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion

    }
}
