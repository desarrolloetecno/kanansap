﻿using System;
using System.Web.UI;
using KananSAP.GestionLlantas.ConfiguracionLlantas.View;
using KananWS.Interface;
using SAPbouiCOM;
//using KananSAP.Helper;

namespace KananSAP.GestionLlantas.ConfiguracionLlantas.Presenter
{
    public class ConfiguracionLlantasPresenter
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private SAPbouiCOM.Item oItem;
        public ConfiguracionLlantasView view { get; set; }
        //public LlantaView llantaview { get; set; }
        private Configurations configs;
        //private LlantasWS LlantaService;
        private ParametroDesgasteWS ParametroDesgasteWS { get; set; }
        private bool Modal;
        private Helper.KananSAPHerramientas oHerramientas;
        #endregion

        #region Constructor
        public ConfiguracionLlantasPresenter(SAPbouiCOM.Application aplication, SAPbobsCOM.Company company, Configurations c)
        {
            this.configs = c;
            this.SBO_Application = aplication;
            this.Company = company;
            this.view = new ConfiguracionLlantasView(this.SBO_Application, this.Company, this.configs);
            //this.Modal = false;
            this.ParametroDesgasteWS = new ParametroDesgasteWS(Guid.Parse(this.configs.UserFull.Usuario.PublicKey), this.configs.UserFull.Usuario.PrivateKey);
            SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
            this.oHerramientas = new Helper.KananSAPHerramientas(this.SBO_Application);
        }
        #endregion

        #region Eventos
        private void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            //SBO_Application.SetStatusBarMessage(pVal.ItemUID + " " + pVal.EventType.ToString());
            try
            {
                #region Configuracion de LLantas
                switch (pVal.ItemUID)
                {
                    case "BtnGrd":
                        if (!pVal.Before_Action)
                        {
                            //Print("CheckUso");
                            if (view.CheckedUso())
                            {
                                //Print("LlantaID = null");
                                view.Entity.oParametroDesgaste.Llanta.LlantaID = null;

                                //Print("Porciento = true, Profundidad = false");
                                if (view.CheckedPorciento() == true && view.CheckedProfundidad() == false)
                                {
                                    //Print("Validar uso porciento");
                                    view.ValidateUsoPorciento();
                                    //Print("FormToEntityUsoPorciento");
                                    view.FormToEntityUsoPorciento();
                                    //Print("HasParametroDesgaste");
                                    if (!view.Entity.HasParametroDesgaste(view.Entity.ConsultarByID(view.RegistroIDUso())))
                                    {
                                        //Print("ParametroDesgasteToUDT");
                                        view.Entity.ParametroDesgasteToUDT();
                                        //Se inserta en web para obtener el Code y Name del registro.
                                        //Print("Add webapi");
                                        view.Entity.oParametroDesgaste = this.ParametroDesgasteWS.Add(view.Entity.oParametroDesgaste);
                                        //Print("Insert SAP");
                                        view.Entity.Insert();
                                        //Print("SetRegistroID");
                                        view.SetRegistroID(view.Entity.oParametroDesgaste.ParametroDesgasteID);
                                        //SBO_Application.SetStatusBarMessage("Se ha guardado exitosamente");
                                        SBO_Application.StatusBar.SetText("¡Registro guardado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                    }
                                    else
                                    {
                                        //Print("RegistroIDUso");
                                        view.Entity.oParametroDesgaste.ParametroDesgasteID = view.RegistroIDUso();
                                        //Print("Update webapi");
                                        view.Entity.oParametroDesgaste = this.ParametroDesgasteWS.Update(view.Entity.oParametroDesgaste);
                                        //Print("Update SAP");
                                        view.Entity.Actualizar(view.Entity.oParametroDesgaste);
                                        SBO_Application.StatusBar.SetText("¡Registro actualizado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                    }
                                    BubbleEvent = false;
                                }

                                if (view.CheckedProfundidad() == true && view.CheckedPorciento() == false)
                                {
                                    //Print("ValidateUsoProfundidad");
                                    view.ValidateUsoProfundidad();
                                    //Print("FormToEntityUsoProfundidad");
                                    view.FormToEntityUsoProfundidad();
                                    //Print("HasParametroDesgaste");
                                    if (!view.Entity.HasParametroDesgaste(view.Entity.ConsultarByID(view.RegistroIDUso())))
                                    {
                                        //view.Entity.GetNewCode();
                                        //Print("ParametroDesgasteToUDT");
                                        view.Entity.ParametroDesgasteToUDT();
                                        //Print("Add webapi");
                                        view.Entity.oParametroDesgaste = this.ParametroDesgasteWS.Add(view.Entity.oParametroDesgaste);
                                        //Print("Insert SAP");
                                        view.Entity.Insert();
                                        //Print("SetRegistroID");
                                        view.SetRegistroID(view.Entity.oParametroDesgaste.ParametroDesgasteID);
                                        SBO_Application.StatusBar.SetText("¡Registro guardado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                    }
                                    else
                                    {
                                        //Print("GetRegistroIDUso");
                                        view.Entity.oParametroDesgaste.ParametroDesgasteID = view.RegistroIDUso();
                                        //Print("Update webapi");
                                        view.Entity.oParametroDesgaste = this.ParametroDesgasteWS.Update(view.Entity.oParametroDesgaste);
                                        //Print("Update webapi");
                                        view.Entity.Actualizar(view.Entity.oParametroDesgaste);
                                        SBO_Application.StatusBar.SetText("¡Registro actualizado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                    }
                                }
                                BubbleEvent = false;
                            }

                            if (view.CheckedLlanta())
                            {
                                //Print("oParametroDesgasteUso = null");
                                view.Entity.oParametroDesgaste.Uso = null;

                                //Print("CheckPorciento = true, CheckProfundidad = false");
                                if (view.CheckedPorciento() == true && view.CheckedProfundidad() == false)
                                {
                                    //Print("ValidateLlantaPorciento");
                                    view.ValidateLlantaPorciento();
                                    //Print("FormToEntityLlantaPorciento");
                                    view.FormToEntityLlantaPorciento();
                                    //Print("HasParametroDesgaste");
                                    if (!view.Entity.HasParametroDesgaste(view.Entity.ConsultarByID(view.RegistroIDLlanta())))
                                    {
                                        //view.Entity.GetNewCode();
                                        //Print("ParametroDesgasteToUDT");
                                        view.Entity.ParametroDesgasteToUDT();
                                        //Print("Add webapi");
                                        view.Entity.oParametroDesgaste = this.ParametroDesgasteWS.Add(view.Entity.oParametroDesgaste);
                                        //Print("Insert SAP");
                                        view.Entity.Insert();
                                        //Print("SetRegistroID");
                                        view.SetRegistroID(view.Entity.oParametroDesgaste.ParametroDesgasteID);
                                        SBO_Application.StatusBar.SetText("¡Registro guardado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                    }
                                    else
                                    {
                                        //Print("GetRegistroIDUso");
                                        view.Entity.oParametroDesgaste.ParametroDesgasteID = view.RegistroIDLlanta();
                                        //Print("Update webapi");
                                        view.Entity.oParametroDesgaste = this.ParametroDesgasteWS.Update(view.Entity.oParametroDesgaste);
                                        //Print("Update SAP");
                                        view.Entity.Actualizar(view.Entity.oParametroDesgaste);
                                        SBO_Application.StatusBar.SetText("¡Registro actualizado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                    }
                                    BubbleEvent = false;
                                }

                                //Print("CheckProfundidad = true, CheckPorciento = False");
                                if (view.CheckedProfundidad() == true && view.CheckedPorciento() == false)
                                {
                                    //Print("ValidateLlantaProfundidad");
                                    view.ValidateLlantaProfundidad();
                                    //Print("FormToEntityLlantaProfundiad");
                                    view.FormToEntityLlantaProfundidad();
                                    //Print("HasParametroDesgaste");
                                    if (!view.Entity.HasParametroDesgaste(view.Entity.ConsultarByID(view.RegistroIDLlanta())))
                                    {
                                        //view.Entity.GetNewCode();
                                        //Print("ParametroDesgasteToUDO");
                                        view.Entity.ParametroDesgasteToUDT();
                                        //Print("Add webapi");
                                        view.Entity.oParametroDesgaste = this.ParametroDesgasteWS.Add(view.Entity.oParametroDesgaste);
                                        //Print("Insert SAP");
                                        view.Entity.Insert();
                                        //Print("SetRegistroID");
                                        view.SetRegistroID(view.Entity.oParametroDesgaste.ParametroDesgasteID);
                                        SBO_Application.StatusBar.SetText("¡Registro guardado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                    }
                                    else
                                    {
                                        //Print("RegistroIDLlanta");
                                        view.Entity.oParametroDesgaste.ParametroDesgasteID = view.RegistroIDLlanta();
                                        //Print("Update webapi");
                                        view.Entity.oParametroDesgaste = this.ParametroDesgasteWS.Update(view.Entity.oParametroDesgaste);
                                        //Print("Update SAP");
                                        view.Entity.Actualizar(view.Entity.oParametroDesgaste);
                                        SBO_Application.StatusBar.SetText("¡Registro actualizado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                    }
                                    BubbleEvent = false;
                                }
                            }

                            this.Modal = true;
                            BubbleEvent = false;
                        }
                        break;
                    case "BtnCan":

                        view.CloseForm();
                        BubbleEvent = false;
                        break;
                    case "BtnCer":

                        view.CloseForm();
                        BubbleEvent = false;
                        break;

                    case "CBP":
                        if (pVal.Before_Action)
                        {
                            view.CheckPorciento();
                            view.ShowConfigurationPorciento();
                            BubbleEvent = false;
                        }
                        break;
                    case "CBN":
                        if (pVal.Before_Action)
                        {
                            view.CheckProfundidad();
                            view.ShowConfigurationNumerico();
                            BubbleEvent = false;
                        }
                        break;
                    case "CBGU":
                        if (pVal.Before_Action)
                        {
                            view.CheckGestUso();
                            view.ShowConfigurationUso();
                            BubbleEvent = false;
                        }
                        break;
                    case "CBGL":
                        if (pVal.Before_Action)
                        {
                            view.CheckGestLlanta();
                            view.ShowConfigurationLlanta();
                            BubbleEvent = false;
                        }
                        break;

                    case "BtnCon":
                        if (pVal.Before_Action)
                        {
                            if (view.CheckedUso())
                            {
                                view.GetUsoData();
                            }

                            if (view.CheckedLlanta())
                            {
                                view.GetLlantaData();
                            }
                            if (!view.CheckedUso() & !view.CheckedLlanta())
                            {
                                SBO_Application.StatusBar.SetText("Seleccione un parámetro de gestión para ejecutar la consulta. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                            }
                        }
                        break;

                    case "mat":
                        if (pVal.Before_Action)
                        {
                            if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                            {
                                this.view.SetID(pVal.Row);
                                this.Modal = true;
                                BubbleEvent = false;
                            }
                        }
                        break;
                }

                switch (pVal.FormTypeEx)
                {
                    case "CfgLlanta":

                        break;
                }

                #endregion
            }
            catch (Exception ex)
            {
                BubbleEvent = false;
                oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            }
        }
        #endregion

        #region Métodos

        #region Guardar
        private void GuardarLlanta()
        {
            try
            {
                //if (string.IsNullOrEmpty(llantaview.GetLlantaID().Trim()))
                //{
                //    Insert();
                //}
                //else
                //{
                //    Update();
                //}
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Insertar
        private void Insert()
        {
            try
            {
                //this.llantaview.FormToEntity();
                //this.llantaview.Entity.Sincronizado = 1;
                //this.llantaview.Entity.UUID = Guid.NewGuid();
                //this.llantaview.Entity.LlantaToUDT();

                //this.llantaview.Entity.llanta = LlantaService.Add(this.llantaview.Entity.llanta);
                //this.llantaview.Entity.LlantaToUDT();
                //this.llantaview.Entity.Insert();

                //this.llantaview.Entity.Sincronizado = 0;
                //this.llantaview.Entity.ActualizarCode(this.llantaview.Entity.llanta);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Actualizar
        private void Update()
        {
            try
            {
                //this.llantaview.FormToEntity();
                //this.llantaview.Entity.Sincronizado = 2;
                //this.llantaview.Entity.LlantaToUDT();
                //this.llantaview.Entity.Update();

                //this.llantaview.Entity.llanta = LlantaService.Update(this.llantaview.Entity.llanta);

                //this.llantaview.Entity.LlantaToUDT();
                //this.llantaview.Entity.Sincronizado = 0;
                //this.llantaview.Entity.ActualizarCode(this.llantaview.Entity.llanta);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Eliminar
        private void Delete()
        {
            try
            {
                //this.llantaview.Entity.llanta = this.LlantaService.Delete((int)this.llantaview.Entity.llanta.LlantaID);
                //this.llantaview.Entity.Code = this.llantaview.Entity.llanta.LlantaID.ToString();
                //this.llantaview.Entity.Deactivate();
                //this.llantaview.EmptyEntityToForm();
                this.oHerramientas.ColocarMensaje("Operación exitosa", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Métodos auxiliares

        private void Print(String text)
        {
            try
            {
                //oHerramientas.LogError(String.Empty, text);
                oHerramientas.ColocarMensaje(text, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al imprimir una mensaje. " + ex.Message);
            }
        }

        private void PrepareUserForm()
        {
            try
            {
                //var id = this.llantaview.Entity.oUDT.Code;
                //var i = Convert.ToInt32(id);
                //this.llantaview.Entity.Code = i.ToString();
                //this.llantaview.Entity.UDTToLlanta();
                //this.llantaview.EntityToForm();
            }
            catch (Exception ex)
            {
                //this.llantaview.EmptyEntityToForm();
                //this.llantaview.CloseForm();
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #endregion

    }
}