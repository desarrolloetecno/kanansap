﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbouiCOM;
using KananWS.Interface;
using KananSAP.Costos.Data;
using KananSAP.Vehiculos.Data;
using Kanan.Vehiculos.BO2;
using KananSAP.Operaciones.Data;
using Kanan.Mantenimiento.BO2;
using KananSAP.Mantenimiento.Data;
using Application = System.Windows.Forms.Application;

/*
 * -------Autor-------
 * ----Rair Santos----
 */

namespace KananSAP.GestionLlantas.ConfiguracionLlantas.View
{
    public class ConfiguracionLlantasView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.StaticText oStaticText;
        private SAPbouiCOM.LinkedButton oLinkedButton;
        private SAPbouiCOM.Matrix oMtx;
        private SAPbouiCOM.DBDataSource oDBDataSource;
        private SAPbouiCOM.Columns oColumns;
        private SAPbouiCOM.Column oColumn;
        private SAPbouiCOM.Button oButton;
        private SAPbouiCOM.Conditions oConditions;
        private SAPbouiCOM.Condition oCondition;
        private SAPbouiCOM.ComboBox oComboBox;
        private SAPbouiCOM.CheckBox oCheckBoxPor;
        private SAPbouiCOM.CheckBox oCheckBoxPro;
        private SAPbouiCOM.CheckBox oCheckBoxUso;
        private SAPbouiCOM.CheckBox oCheckBoxLlanta;
        //private SAPbouiCOM.Gr
        public ParametroDesgasteData Entity { get; set; }
        public SucursalSAP EntitySucursal { get; set; }
        public ProveedorSAP EntityProveedor { get; set; }
        public Configurations configs;
        public string FormUniqueID { get; set; }
        private XMLPerformance XmlApplication;
        public int? id { get; set; }
        public int? llantaid;
        public int val = 0;
        private TipoVehiculoSAP eTipoVehiculo;
        #endregion

        #region Constructor
        public ConfiguracionLlantasView(SAPbouiCOM.Application Application, SAPbobsCOM.Company company, Configurations c)
        {
            this.SBO_Application = Application;
            this.XmlApplication = new XMLPerformance(SBO_Application);
            /*this.data = new ParametroDesgaste() {
            Empresa = new Kanan.Operaciones.BO2.Empresa(),
            Sucursal = new Kanan.Operaciones.BO2.Sucursal(),
            Llanta = new Kanan.Llantas.BO2.Llanta() };*/
            this.Entity = new ParametroDesgasteData(company)
            {
                oParametroDesgaste = new ParametroDesgaste()
                {
                    Empresa = new Kanan.Operaciones.BO2.Empresa(),
                    Sucursal = new Kanan.Operaciones.BO2.Sucursal(),
                    Llanta = new Kanan.Llantas.BO2.Llanta()
                }
            };
            this.EntitySucursal = new SucursalSAP(company);
            this.EntityProveedor = new ProveedorSAP(company);
            this.eTipoVehiculo = new TipoVehiculoSAP(company);
            this.oItem = null;
            this.oLinkedButton = null;
            this.oStaticText = null;
            this.oEditText = null;
            this.oConditions = null;
            this.oCondition = null;
            this.oColumns = null;
            this.oColumn = null;
            this.llantaid = null;
            this.oCheckBoxPor = null;
            this.oCheckBoxPro = null;
            this.oCheckBoxUso = null;
            this.oCheckBoxLlanta = null;
            this.oComboBox = null;
            this.oForm = null;
            this.configs = c;
            this.id = null;
        }
        #endregion

        #region Métodos

        #region LoadForm
        public void LoadForm()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("CfgLlanta");
                BindDataToForm(0, "", "", "");
            }
            catch
            {
                string sPath = Application.StartupPath;

                this.XmlApplication.LoadFromXML(sPath + "\\XML",
                        "ConfiguracionLlanta.xml");
                this.oForm = SBO_Application.Forms.Item("CfgLlanta");

                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;

                //this.InitializeComponents();

                //oItem = oForm.Items.Item("lblid");
                //oStaticText = (StaticText)(oItem.Specific);
                //oStaticText.Caption = this.llantaid.ToString();
                //oItem.Visible = false;
            }
        }
        #endregion

        #region ShowForm
        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
                this.InitializeComponents();
                this.BindDataToForm(0, "", "", "");
            }
            catch (Exception)
            {
                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                this.InitializeComponents();
                this.BindDataToForm(0, "", "", "");
                this.oForm.Select();
            }
        }
        #endregion

        #region Inicializar componentes
        public void InitializeComponents()
        {
            try
            {
                #region Label
                //Label RegistroID
                oItem = oForm.Items.Item("RegID");
                oStaticText = (StaticText)(oItem.Specific);
                oStaticText.Caption = "";
                //Label Empresa
                oItem = oForm.Items.Item("emp");
                oStaticText = (StaticText)(oItem.Specific);
                oItem = oForm.Items.Item("EmpID");
                oStaticText = (StaticText)(oItem.Specific);
                oStaticText.Caption = this.configs.UserFull.Dependencia.EmpresaID.ToString() == null ? Convert.ToString(0)
                    : this.configs.UserFull.Dependencia.EmpresaID.ToString();
                //Lalbe Sucursal
                oItem = oForm.Items.Item("suc");
                oStaticText = (StaticText)(oItem.Specific);
                oItem = oForm.Items.Item("SucID");
                oStaticText = (StaticText)(oItem.Specific);
                oStaticText.Caption = this.configs.UserFull.EmpleadoSucursal.SucursalID.ToString() == null ? Convert.ToString(0)
                    : this.configs.UserFull.EmpleadoSucursal.SucursalID.ToString();
                //Label Llanta número económico
                oItem = oForm.Items.Item("numeco");
                oStaticText = (StaticText)(oItem.Specific);
                oItem = oForm.Items.Item("NumEco");
                oStaticText = (StaticText)(oItem.Specific);
                //Label Marca
                oItem = oForm.Items.Item("mar");
                oStaticText = (StaticText)(oItem.Specific);
                oItem = oForm.Items.Item("Mar");
                //Label modelo
                oItem = oForm.Items.Item("mod");
                oStaticText = (StaticText)(oItem.Specific);
                oItem = oForm.Items.Item("Mod");
                oStaticText = (StaticText)(oItem.Specific);
                //Label Gestión por tipo de uso
                oItem = oForm.Items.Item("GesUso");
                oStaticText = (StaticText)(oItem.Specific);
                //Label Gestión por llanta
                oItem = oForm.Items.Item("GesLlan");
                oStaticText = (StaticText)(oItem.Specific);
                //Label Tipo de uso
                oItem = oForm.Items.Item("TUso");
                oStaticText = (StaticText)(oItem.Specific);
                //Label Límite porcentaje
                oItem = oForm.Items.Item("limpor");
                oStaticText = (StaticText)(oItem.Specific);
                //Label Límite profundidad
                oItem = oForm.Items.Item("limpro");
                oStaticText = (StaticText)(oItem.Specific);
                //Label Aviso límite porcentaje
                oItem = oForm.Items.Item("avlimpor");
                oStaticText = (StaticText)(oItem.Specific);
                //Label Aviso límite profundidad
                oItem = oForm.Items.Item("avlimpro");
                oStaticText = (StaticText)(oItem.Specific);
                //Laben Tiempo
                oItem = oForm.Items.Item("tiempo");
                oStaticText = (StaticText)(oItem.Specific);
                #endregion

                #region TextBox
                //TextBox LimPor
                oItem = oForm.Items.Item("LimPor");
                oEditText = (EditText)(oItem.Specific);
                //TextBox LimPro
                oItem = oForm.Items.Item("LimPro");
                oEditText = (EditText)(oItem.Specific);
                //TextBox AvLimPor
                oItem = oForm.Items.Item("AvLimPor");
                oEditText = (EditText)(oItem.Specific);
                //TextBox AvLimPro
                oItem = oForm.Items.Item("AvLimPro");
                oEditText = (EditText)(oItem.Specific);
                //TextBox Tiempo
                oItem = oForm.Items.Item("Tiempo");
                oEditText = (EditText)(oItem.Specific);
                #endregion

                #region CheckBox
                //Gestión por uso
                oItem = oForm.Items.Item("CBGU");
                oCheckBoxUso = (CheckBox)(oItem.Specific);
                oCheckBoxUso.Checked = false;
                oForm.DataSources.UserDataSources.Add("CheckUso", BoDataType.dt_SHORT_TEXT, 1);
                oCheckBoxUso.DataBind.SetBound(true, "", "CheckUso");

                //Gestión por llanta
                oItem = oForm.Items.Item("CBGL");
                oCheckBoxLlanta = (CheckBox)(oItem.Specific);
                oCheckBoxLlanta.Checked = false;
                oForm.DataSources.UserDataSources.Add("CheckLlan", BoDataType.dt_SHORT_TEXT, 1);
                oCheckBoxLlanta.DataBind.SetBound(true, "", "CheckLlan");

                //CheckBox Porcentaje
                oItem = oForm.Items.Item("CBP");
                oCheckBoxPor = (CheckBox)(oItem.Specific);
                oCheckBoxPor.Checked = false;
                oForm.DataSources.UserDataSources.Add("por", BoDataType.dt_SHORT_TEXT, 1);
                oCheckBoxPor.DataBind.SetBound(true, "", "por");

                //CheckBox Profundidad
                oItem = oForm.Items.Item("CBN");
                oCheckBoxPro = (CheckBox)(oItem.Specific);
                oCheckBoxPro.Checked = false;
                oForm.DataSources.UserDataSources.Add("pro", BoDataType.dt_SHORT_TEXT, 1);
                oCheckBoxPro.DataBind.SetBound(true, "", "pro");
                #endregion

                #region ComboBox
                //ComboBox UsoID
                oItem = oForm.Items.Item("uso");
                oComboBox = (ComboBox)(oItem.Specific);
                CleanComboBox(oComboBox);

                var rs = this.eTipoVehiculo.Consultar(new TipoVehiculo { EsActivo = true });
                rs.MoveFirst();
                for (int i = 0; i < rs.RecordCount; i++)
                {
                    string nombre = rs.Fields.Item("U_Nombre").Value.ToString();
                    string value = (string)Convert.ChangeType(rs.Fields.Item("U_TVeiculoID").Value, typeof(string));
                    oComboBox.ValidValues.Add(value, nombre);
                    rs.MoveNext();
                }
                oComboBox.Item.DisplayDesc = true;
                #endregion

                #region Matrix
                //Matrix mat
                oItem = oForm.Items.Item("mat");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                #endregion

                #region Buttons
                //Button Guardar
                oItem = oForm.Items.Item("BtnGrd");
                oButton = (Button)(oItem.Specific);
                //Button Cancelar
                oItem = oForm.Items.Item("BtnCan");
                oButton = (Button)(oItem.Specific);
                //Button Cerrar
                oItem = oForm.Items.Item("BtnCer");
                oButton = (Button)(oItem.Specific);
                #endregion

                //Se dirige el foto a TextBox Tiempo
                oItem = oForm.Items.Item("Tiempo");
                oEditText = (EditText)(oItem.Specific);
                oEditText.Value = "";
            }
            catch (Exception ex)
            {
                throw new Exception("Error en inicializar componentes " + ex.Message);
            }
        }
        #endregion

        #region Validar Uso Porciento
        public void ValidateUsoPorciento()
        {
            try
            {
                oItem = oForm.Items.Item("LimPor");
                oEditText = (EditText)(oItem.Specific);
                if (oEditText.Value == null || string.IsNullOrEmpty(oEditText.Value))
                {
                    throw new Exception("Inserte un valor en Límite porcentaje.");
                }
                oItem = oForm.Items.Item("AvLimPor");
                oEditText = (EditText)(oItem.Specific);
                if (oEditText.Value == null || string.IsNullOrEmpty(oEditText.Value))
                {
                    throw new Exception("Inserte un valor en Aviso límite porcentaje.");
                }
                oItem = oForm.Items.Item("Tiempo");
                oEditText = (EditText)(oItem.Specific);
                if (oEditText.Value == null || string.IsNullOrEmpty(oEditText.Value))
                {
                    throw new Exception("Inserte un valor en Tiempo actualizar treadwear.");
                }
                oItem = oForm.Items.Item("uso");
                oComboBox = (ComboBox)(oItem.Specific);
                if (oComboBox.Value == null || string.IsNullOrEmpty(oComboBox.Value))
                {
                    throw new Exception("Inserte un elemento en Tipo de uso.");
                }
                oItem = oForm.Items.Item("EmpID");
                oStaticText = (StaticText)(oItem.Specific);
                if (oStaticText.Caption == null || string.IsNullOrEmpty(oStaticText.Caption))
                {
                    throw new Exception("Ninguna empresa vinculada.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Validar Uso Profundidad
        public void ValidateUsoProfundidad()
        {
            try
            {
                oItem = oForm.Items.Item("LimPro");
                oEditText = (EditText)(oItem.Specific);
                if (oEditText.Value == null || string.IsNullOrEmpty(oEditText.Value))
                {
                    throw new Exception("Inserte un valor en Límite profundidad.");
                }
                oItem = oForm.Items.Item("AvLimPro");
                oEditText = (EditText)(oItem.Specific);
                if (oEditText.Value == null || string.IsNullOrEmpty(oEditText.Value))
                {
                    throw new Exception("Inserte un valor en Aviso límite profundidad.");
                }
                oItem = oForm.Items.Item("Tiempo");
                oEditText = (EditText)(oItem.Specific);
                if (oEditText.Value == null || string.IsNullOrEmpty(oEditText.Value))
                {
                    throw new Exception("Inserte un valor en Tiempo actualizar treadwear.");
                }
                oItem = oForm.Items.Item("uso");
                oComboBox = (ComboBox)(oItem.Specific);
                if (oComboBox.Value == null || string.IsNullOrEmpty(oComboBox.Value))
                {
                    throw new Exception("Inserte un elemento en Tipo de uso.");
                }
                oItem = oForm.Items.Item("EmpID");
                oStaticText = (StaticText)(oItem.Specific);
                if (oStaticText.Caption == null || string.IsNullOrEmpty(oStaticText.Caption))
                {
                    throw new Exception("Ninguna empresa vinculada.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Validar Llanta Porciento
        public void ValidateLlantaPorciento()
        {
            try
            {
                double limPro;
                double avilimPro;

                oItem = oForm.Items.Item("LimPor");
                oEditText = (EditText)(oItem.Specific);
                if (oEditText.Value == null || string.IsNullOrEmpty(oEditText.Value))
                {
                    throw new Exception("Inserte un valor en Límite porcentaje.");
                }
                else
                    limPro = Convert.ToDouble(oEditText.Value);
                oItem = oForm.Items.Item("AvLimPor");
                oEditText = (EditText)(oItem.Specific);
                if (oEditText.Value == null || string.IsNullOrEmpty(oEditText.Value))
                {
                    throw new Exception("Inserte un valor en Aviso límite porcentaje.");
                }
                else
                    avilimPro = Convert.ToDouble(oEditText.Value);
                oItem = oForm.Items.Item("Tiempo");
                oEditText = (EditText)(oItem.Specific);
                if (oEditText.Value == null || string.IsNullOrEmpty(oEditText.Value))
                {
                    throw new Exception("Inserte un valor en Tiempo actualizar treadwear.");
                }
                oItem = oForm.Items.Item("NumEco");
                oStaticText = (StaticText)(oItem.Specific);
                if (oStaticText.Caption == null || string.IsNullOrEmpty(oStaticText.Caption))
                {
                    throw new Exception("Seleccione una llanta.");
                }
                oItem = oForm.Items.Item("Mar");
                oStaticText = (StaticText)(oItem.Specific);
                if (oStaticText.Caption == null || string.IsNullOrEmpty(oStaticText.Caption))
                {
                    throw new Exception("Seleccione una llanta.");
                }
                oItem = oForm.Items.Item("Mod");
                oStaticText = (StaticText)(oItem.Specific);
                if (oStaticText.Caption == null || string.IsNullOrEmpty(oStaticText.Caption))
                {
                    throw new Exception("Seleccione una llanta.");
                }
                if (limPro > avilimPro)
                    throw new Exception("El aviso de límite de produndidad no puede ser menor al límite de profundidad. ");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Validar Llanta Profundidad
        public void ValidateLlantaProfundidad()
        {
            try
            {
                double limPro;
                double avilimPro;
                oItem = oForm.Items.Item("LimPro");
                oEditText = (EditText)(oItem.Specific);
                if (oEditText.Value == null || string.IsNullOrEmpty(oEditText.Value))
                {
                    throw new Exception("Inserte un valor en Límite profundidad.");
                }
                else
                    limPro = Convert.ToDouble(oEditText.Value);
                oItem = oForm.Items.Item("AvLimPro");
                oEditText = (EditText)(oItem.Specific);
                if (oEditText.Value == null || string.IsNullOrEmpty(oEditText.Value))
                {
                    throw new Exception("Inserte un valor en Aviso límite profundidad.");
                }
                else
                    avilimPro = Convert.ToDouble(oEditText.Value);
                oItem = oForm.Items.Item("Tiempo");
                oEditText = (EditText)(oItem.Specific);
                if (oEditText.Value == null || string.IsNullOrEmpty(oEditText.Value))
                {
                    throw new Exception("Inserte un valor en Tiempo actualizar treadwear.");
                }
                oItem = oForm.Items.Item("NumEco");
                oStaticText = (StaticText)(oItem.Specific);
                if (oStaticText.Caption == null || string.IsNullOrEmpty(oStaticText.Caption))
                {
                    throw new Exception("Seleccione una llanta.");
                }
                oItem = oForm.Items.Item("Mar");
                oStaticText = (StaticText)(oItem.Specific);
                if (oStaticText.Caption == null || string.IsNullOrEmpty(oStaticText.Caption))
                {
                    throw new Exception("Seleccione una llanta.");
                }
                oItem = oForm.Items.Item("Mod");
                oStaticText = (StaticText)(oItem.Specific);
                if (oStaticText.Caption == null || string.IsNullOrEmpty(oStaticText.Caption))
                {
                    throw new Exception("Seleccione una llanta.");
                }
                if (limPro > avilimPro)
                    throw new Exception("El aviso de límite de produndidad no puede ser menor al límite de profundidad. ");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region EntityToFormFull
        public void entityToFormFull(ParametroDesgaste pd)
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("CfgLlanta");
                this.Entity.oParametroDesgaste = null;
                this.Entity.oParametroDesgaste = new ParametroDesgaste()
                {
                    Empresa = new Kanan.Operaciones.BO2.Empresa(),
                    Sucursal = new Kanan.Operaciones.BO2.Sucursal(),
                    Llanta = new Kanan.Llantas.BO2.Llanta()
                };
                this.Entity.oParametroDesgaste = pd;

                oItem = this.oForm.Items.Item("RegID");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                if (!string.IsNullOrEmpty(this.Entity.oParametroDesgaste.ParametroDesgasteID.ToString()))
                    oStaticText.Caption = this.Entity.oParametroDesgaste.ParametroDesgasteID.ToString() ?? string.Empty;

                oItem = this.oForm.Items.Item("LimPor");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (!string.IsNullOrEmpty(this.Entity.oParametroDesgaste.LimPorc.ToString()))
                    oEditText.Value = this.Entity.oParametroDesgaste.LimPorc.ToString() ?? string.Empty;

                oItem = this.oForm.Items.Item("LimPro");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (!string.IsNullOrEmpty(this.Entity.oParametroDesgaste.LimProf.ToString()))
                    oEditText.Value = this.Entity.oParametroDesgaste.LimProf.ToString() ?? string.Empty;

                oItem = this.oForm.Items.Item("AvLimPor");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (!string.IsNullOrEmpty(this.Entity.oParametroDesgaste.AvisoLimPorc.ToString()))
                    oEditText.Value = this.Entity.oParametroDesgaste.AvisoLimPorc.ToString() ?? string.Empty;

                oItem = this.oForm.Items.Item("AvLimPro");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (!string.IsNullOrEmpty(this.Entity.oParametroDesgaste.AvisoLimProf.ToString()))
                    oEditText.Value = this.Entity.oParametroDesgaste.AvisoLimProf.ToString() ?? string.Empty;

                oItem = this.oForm.Items.Item("Tiempo");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (!string.IsNullOrEmpty(this.Entity.oParametroDesgaste.TiempActTread.ToString()))
                    oEditText.Value = this.Entity.oParametroDesgaste.TiempActTread.ToString() ?? string.Empty;

                oItem = this.oForm.Items.Item("uso");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                if (!string.IsNullOrEmpty(this.Entity.oParametroDesgaste.Uso.ToString()))
                    oComboBox.Select(this.Entity.oParametroDesgaste.Uso.ToString());
                else oComboBox.Select("0");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region FormToEntityUsoPorciento
        public void FormToEntityUsoPorciento()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("CfgLlanta");

                oItem = this.oForm.Items.Item("RegID");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                if (!string.IsNullOrEmpty(oStaticText.Caption))
                    this.Entity.oParametroDesgaste.ParametroDesgasteID = Convert.ToInt32(oStaticText.Caption);

                oItem = this.oForm.Items.Item("EmpID");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                this.Entity.oParametroDesgaste.Empresa.EmpresaID = Convert.ToInt32(oStaticText.Caption);

                oItem = this.oForm.Items.Item("SucID");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                if (!string.IsNullOrEmpty(oStaticText.Caption))
                    this.Entity.oParametroDesgaste.Empresa.EmpresaID = Convert.ToInt32(oStaticText.Caption);

                oItem = this.oForm.Items.Item("LimPor");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Entity.oParametroDesgaste.LimPorc = Convert.ToDouble(oEditText.Value);

                this.Entity.oParametroDesgaste.LimProf = 0;

                oItem = this.oForm.Items.Item("AvLimPor");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Entity.oParametroDesgaste.AvisoLimPorc = Convert.ToDouble(oEditText.Value);

                this.Entity.oParametroDesgaste.AvisoLimProf = 0;

                oItem = this.oForm.Items.Item("Tiempo");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Entity.oParametroDesgaste.TiempActTread = Convert.ToInt32(oEditText.Value);

                oItem = this.oForm.Items.Item("uso");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                this.Entity.oParametroDesgaste.Uso = Convert.ToInt32(oComboBox.Value);

                this.Entity.oParametroDesgaste.Llanta.LlantaID = 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region FormToEntityUsoProfundidad
        public void FormToEntityUsoProfundidad()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("CfgLlanta");

                oItem = this.oForm.Items.Item("RegID");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                if (!string.IsNullOrEmpty(oStaticText.Caption))
                    this.Entity.oParametroDesgaste.ParametroDesgasteID = Convert.ToInt32(oStaticText.Caption);

                oItem = this.oForm.Items.Item("EmpID");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                this.Entity.oParametroDesgaste.Empresa.EmpresaID = Convert.ToInt32(oStaticText.Caption);

                oItem = this.oForm.Items.Item("SucID");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                if (!string.IsNullOrEmpty(oStaticText.Caption))
                    this.Entity.oParametroDesgaste.Empresa.EmpresaID = Convert.ToInt32(oStaticText.Caption);

                oItem = this.oForm.Items.Item("LimPro");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Entity.oParametroDesgaste.LimProf = Convert.ToDouble(oEditText.Value);

                this.Entity.oParametroDesgaste.LimPorc = 0;

                oItem = this.oForm.Items.Item("AvLimPro");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Entity.oParametroDesgaste.AvisoLimProf = Convert.ToDouble(oEditText.Value);

                this.Entity.oParametroDesgaste.AvisoLimPorc = 0;

                oItem = this.oForm.Items.Item("Tiempo");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Entity.oParametroDesgaste.TiempActTread = Convert.ToInt32(oEditText.Value);

                oItem = this.oForm.Items.Item("uso");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                this.Entity.oParametroDesgaste.Uso = Convert.ToInt32(oComboBox.Value);

                this.Entity.oParametroDesgaste.Llanta.LlantaID = 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region FormToEntityLlantaPorciento
        public void FormToEntityLlantaPorciento()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("CfgLlanta");

                oItem = this.oForm.Items.Item("RegID");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                if (!string.IsNullOrEmpty(oStaticText.Caption))
                    this.Entity.oParametroDesgaste.ParametroDesgasteID = Convert.ToInt32(oStaticText.Caption);

                oItem = this.oForm.Items.Item("NumEco");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                if (!string.IsNullOrEmpty(oStaticText.Caption))
                    this.Entity.oParametroDesgaste.Llanta.NumeroEconomico = oStaticText.Caption;

                oItem = this.oForm.Items.Item("Mar");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                if (!string.IsNullOrEmpty(oStaticText.Caption))
                    this.Entity.oParametroDesgaste.Llanta.Marca = oStaticText.Caption;

                oItem = this.oForm.Items.Item("Mod");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                if (!string.IsNullOrEmpty(oStaticText.Caption))
                    this.Entity.oParametroDesgaste.Llanta.Modelo = oStaticText.Caption;

                oItem = this.oForm.Items.Item("LimPor");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Entity.oParametroDesgaste.LimPorc = Convert.ToDouble(oEditText.Value);

                this.Entity.oParametroDesgaste.LimProf = 0;

                oItem = this.oForm.Items.Item("AvLimPor");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Entity.oParametroDesgaste.AvisoLimPorc = Convert.ToDouble(oEditText.Value);

                this.Entity.oParametroDesgaste.AvisoLimProf = 0;

                oItem = this.oForm.Items.Item("Tiempo");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Entity.oParametroDesgaste.TiempActTread = Convert.ToInt32(oEditText.Value);

                /*Se valida que si la llanta no tiene un valor, no se sobreescriba en RegistroIDLlanta()*/
                /*if (this.Entity.oParametroDesgaste.Llanta == null)
                    this.Entity.oParametroDesgaste.Llanta = new Kanan.Llantas.BO2.Llanta()
                    {
                        LlantaID = Convert.ToInt32(this.id)
                    };
                else
                    this.Entity.oParametroDesgaste.Llanta.LlantaID = Convert.ToInt32(this.id);*/

                this.Entity.oParametroDesgaste.Uso = 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region FormToEntityLlantaProfundidad
        public void FormToEntityLlantaProfundidad()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("CfgLlanta");

                oItem = this.oForm.Items.Item("RegID");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                if (!string.IsNullOrEmpty(oStaticText.Caption))
                    this.Entity.oParametroDesgaste.ParametroDesgasteID = Convert.ToInt32(oStaticText.Caption);

                oItem = this.oForm.Items.Item("EmpID");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                if (!string.IsNullOrEmpty(oStaticText.Caption))
                    this.Entity.oParametroDesgaste.Empresa.EmpresaID = Convert.ToInt32(oStaticText.Caption);

                oItem = this.oForm.Items.Item("SucID");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                if (!string.IsNullOrEmpty(oStaticText.Caption))
                    this.Entity.oParametroDesgaste.Sucursal.SucursalID = Convert.ToInt32(oStaticText.Caption);

                oItem = this.oForm.Items.Item("NumEco");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                if (!string.IsNullOrEmpty(oStaticText.Caption))
                    this.Entity.oParametroDesgaste.Llanta.NumeroEconomico = oStaticText.Caption;

                oItem = this.oForm.Items.Item("Mar");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                if (!string.IsNullOrEmpty(oStaticText.Caption))
                    this.Entity.oParametroDesgaste.Llanta.Marca = oStaticText.Caption;

                oItem = this.oForm.Items.Item("Mod");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                if (!string.IsNullOrEmpty(oStaticText.Caption))
                    this.Entity.oParametroDesgaste.Llanta.Modelo = oStaticText.Caption;

                oItem = this.oForm.Items.Item("LimPro");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Entity.oParametroDesgaste.LimProf = Convert.ToDouble(oEditText.Value);

                this.Entity.oParametroDesgaste.LimPorc = 0;

                oItem = this.oForm.Items.Item("AvLimPro");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Entity.oParametroDesgaste.AvisoLimProf = Convert.ToDouble(oEditText.Value);

                this.Entity.oParametroDesgaste.AvisoLimPorc = 0;

                oItem = this.oForm.Items.Item("Tiempo");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                this.Entity.oParametroDesgaste.TiempActTread = Convert.ToInt32(oEditText.Value);

                /*Se valida que si la llanta no tiene un valor, no se sobreescriba en RegistroIDLlanta()*/
                /*if (this.Entity.oParametroDesgaste.Llanta == null)
                    this.Entity.oParametroDesgaste.Llanta = new Kanan.Llantas.BO2.Llanta()
                    {
                        LlantaID = Convert.ToInt32(this.id)
                    };
                else
                    this.Entity.oParametroDesgaste.Llanta.LlantaID = Convert.ToInt32(this.id);*/

                this.Entity.oParametroDesgaste.Uso = 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region EntityToFormPorciento
        public void EntityToFormPorcieto()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("CfgLlanta");

                oItem = this.oForm.Items.Item("RegID");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.oParametroDesgaste.ParametroDesgasteID.ToString() ?? string.Empty;

                oItem = this.oForm.Items.Item("LimPor");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.oParametroDesgaste.LimPorc.ToString() ?? string.Empty;

                oItem = this.oForm.Items.Item("AvLimPor");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.oParametroDesgaste.AvisoLimPorc.ToString() ?? string.Empty;

                oItem = this.oForm.Items.Item("Tiempo");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.oParametroDesgaste.TiempActTread.ToString() ?? string.Empty;

                oItem = this.oForm.Items.Item("uso");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                if (this.Entity.oParametroDesgaste.Uso != null && this.Entity.oParametroDesgaste.Uso > 0)
                    oComboBox.Select(this.Entity.oParametroDesgaste.Uso.ToString());
                else oComboBox.Select("0");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region EntityToFormProfundidad
        public void EntityToFormProfundidad()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("CfgLlanta");

                oItem = this.oForm.Items.Item("RegID");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.oParametroDesgaste.ParametroDesgasteID.ToString() ?? string.Empty;

                oItem = this.oForm.Items.Item("LimPro");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.oParametroDesgaste.LimProf.ToString() ?? string.Empty;

                oItem = this.oForm.Items.Item("AvLimPro");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.oParametroDesgaste.AvisoLimProf.ToString() ?? string.Empty;

                oItem = this.oForm.Items.Item("Tiempo");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.ItemCode ?? string.Empty;

                oItem = this.oForm.Items.Item("uso");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                if (this.Entity.oParametroDesgaste.Uso != null && this.Entity.oParametroDesgaste.Uso >= 0)
                    oComboBox.Select(this.Entity.oParametroDesgaste.Uso.ToString());
                else oComboBox.Select("0");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region EmptyEntityToForm
        public void EmptyEntityToForm()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("CfgLlanta");

                oItem = this.oForm.Items.Item("LimPor");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = string.Empty;

                oItem = this.oForm.Items.Item("LimPro");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = string.Empty;

                oItem = this.oForm.Items.Item("AvLimPor");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = string.Empty;

                oItem = this.oForm.Items.Item("AvLimPro");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = string.Empty;

                oItem = this.oForm.Items.Item("Tiempo");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = string.Empty;

                oItem = this.oForm.Items.Item("uso");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                oComboBox.Select(0);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region CloseForm
        public void CloseForm()
        {
            this.id = null;
            if (this.oForm.Visible & this.oForm.Selected)
            {
                this.oForm.Close();
            }
            else
            {
                this.oForm.Close();
            }
        }
        #endregion

        #region CleanComboBox
        private void CleanComboBox(ComboBox combo)
        {
            if (combo == null || combo.ValidValues == null || combo.ValidValues.Count <= 0) return;
            var f = combo.ValidValues.Count;
            combo.Value.Remove(0, 5);
            for (int i = 0; i < f; i++)
            {
                combo.ValidValues.Remove(i, BoSearchKey.psk_Index);
            }
        }
        #endregion

        #region CleanMatrix
        private void CleanMatrix()
        {
            oItem = oForm.Items.Item("mat");
            oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
            oMtx.Clear();
        }
        #endregion

        #region Checks True/False

        #region CheckPorTrue
        public void CheckPorciento()
        {
            oForm.DataSources.UserDataSources.Item("por").Value = "Y";
            oForm.DataSources.UserDataSources.Item("pro").Value = "N";
        }
        #endregion

        #region CheckedProTrue
        public void CheckProfundidad()
        {
            oForm.DataSources.UserDataSources.Item("por").Value = "N";
            oForm.DataSources.UserDataSources.Item("pro").Value = "Y";
        }
        #endregion

        #region CheckGestUso
        public void CheckGestUso()
        {
            oForm.DataSources.UserDataSources.Item("CheckUso").Value = "Y";
            oForm.DataSources.UserDataSources.Item("CheckLlan").Value = "N";
            oItem = this.oForm.Items.Item("RegID");
            oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            oStaticText.Caption = "0";
        }
        #endregion

        #region CheckGestLlanta
        public void CheckGestLlanta()
        {
            oForm.DataSources.UserDataSources.Item("CheckUso").Value = "N";
            oForm.DataSources.UserDataSources.Item("CheckLlan").Value = "Y";
            oItem = this.oForm.Items.Item("RegID");
            oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            oStaticText.Caption = "0";
        }
        #endregion

        #endregion

        #region CheckedPorciento
        public bool CheckedPorciento()
        {
            oItem = oForm.Items.Item("CBP");
            if (oCheckBoxPor.Checked)
            { return true; }
            else
            { return false; }
        }
        #endregion

        #region CheckedProfundidad
        public bool CheckedProfundidad()
        {
            oItem = oForm.Items.Item("CBN");
            if (oCheckBoxPro.Checked)
            { return true; }
            else
            { return false; }
        }
        #endregion

        #region CheckedUso
        public bool CheckedUso()
        {
            oItem = oForm.Items.Item("CBGU");
            if (oCheckBoxUso.Checked)
            { return true; }
            else
            { return false; }
        }
        #endregion

        #region CheckedLlanta
        public bool CheckedLlanta()
        {
            oItem = oForm.Items.Item("CBGL");
            if (oCheckBoxLlanta.Checked)
            { return true; }
            else
            { return false; }
        }
        #endregion

        //Aquí se comprueba el valor seleccionado del ComboBox y se realiza la consulta para llenar los campos
        public void GetUsoData()
        {
            val = 1;
            string valUso = "";
            string valEmpID = "";
            string valSucID = "";

            oItem = oForm.Items.Item("uso");
            oComboBox = (ComboBox)(oItem.Specific);
            valUso = oComboBox.Value;

            oItem = oForm.Items.Item("EmpID");
            oStaticText = (StaticText)(oItem.Specific);
            valEmpID = oStaticText.Caption;

            oItem = oForm.Items.Item("SucID");
            oStaticText = (StaticText)(oItem.Specific);
            valSucID = oStaticText.Caption;

            if (string.IsNullOrEmpty(valUso))
                throw new Exception("Numero económico no válido, Selecciona un tipo de uso");
            if (string.IsNullOrEmpty(valEmpID))
                throw new Exception("Marca no válida, Empresa no válida.");

            //Sucursal No es un campo obligatorio.
            //if (string.IsNullOrEmpty(valSucID))
            //throw new Exception("Modelo no válido");

            BindDataToForm(val, valUso, valEmpID, valSucID);
            val = 0;
        }

        public void GetLlantaData()
        {
            val = 2;
            string valID;
            string valMar;
            string valMod;

            oItem = oForm.Items.Item("NumEco");
            oStaticText = (StaticText)(oItem.Specific);
            valID = oStaticText.Caption;

            oItem = oForm.Items.Item("Mar");
            oStaticText = (StaticText)(oItem.Specific);
            valMar = oStaticText.Caption;

            oItem = oForm.Items.Item("Mod");
            oStaticText = (StaticText)(oItem.Specific);
            valMod = oStaticText.Caption;

            if (string.IsNullOrEmpty(valID))
                throw new Exception("Numero económico no válido, Selecciona un llanta.");
            if (string.IsNullOrEmpty(valMar))
                throw new Exception("Marca no válida, Selecciona un llanta.");
            if (string.IsNullOrEmpty(valMod))
                throw new Exception("Modelo no válido, Selecciona un llanta.");

            BindDataToForm(val, id.ToString(), valMar, valMod);
            val = 0;
        }


        #region ShowConfigurationPorciento
        public void ShowConfigurationPorciento()
        {
            //Se dirige el foto a TextBox Tiempo
            oItem = oForm.Items.Item("Tiempo");
            oEditText = (EditText)(oItem.Specific);
            oEditText.Value = string.IsNullOrEmpty(oEditText.Value) ? "" : oEditText.Value;

            //Se verifica que se muestres los campos de porcentaje
            oItem = oForm.Items.Item("LimPor");
            oEditText = (EditText)(oItem.Specific);
            oItem.Enabled = true;
            //oItem.Visible = true;
            oItem = oForm.Items.Item("AvLimPor");
            oEditText = (EditText)(oItem.Specific);
            oItem.Enabled = true;
            //oItem.Visible = true;

            //Se verifica que se oculten los campos numéricos
            oItem = oForm.Items.Item("LimPro");
            oEditText = (EditText)(oItem.Specific);
            //oEditText.Value = "";
            oItem.Enabled = false;
            //oItem.Visible = false;
            oItem = oForm.Items.Item("AvLimPro");
            oEditText = (EditText)(oItem.Specific);
            //oEditText.Value = "";
            oItem.Enabled = false;
            //oItem.Visible = false;
        }
        #endregion

        #region ShowConfigurationNumerico
        public void ShowConfigurationNumerico()
        {
            //Se dirige el foto a TextBox Tiempo
            oItem = oForm.Items.Item("Tiempo");
            oEditText = (EditText)(oItem.Specific);
            oEditText.Value = string.IsNullOrEmpty(oEditText.Value) ? "" : oEditText.Value;

            //Se verifica que se  muestren los campos numéricos
            oItem = oForm.Items.Item("LimPro");
            oEditText = (EditText)(oItem.Specific);
            oItem.Enabled = true;
            //oItem.Visible = true;
            oItem = oForm.Items.Item("AvLimPro");
            oEditText = (EditText)(oItem.Specific);
            oItem.Enabled = true;
            //oItem.Visible = true;

            //Se verifica que se oculten los campos de porcentaje
            oItem = oForm.Items.Item("LimPor");
            oEditText = (EditText)(oItem.Specific);
            //oEditText.Value = ""; 
            oItem.Enabled = false;
            //oItem.Visible = false;
            oItem = oForm.Items.Item("AvLimPor");
            oEditText = (EditText)(oItem.Specific);
            //oEditText.Value = ""; 
            oItem.Enabled = false;
            //oItem.Visible = false;
        }
        #endregion

        #region ShowConfigurationUso
        public void ShowConfigurationUso()
        {
            //Se dirige el foto a TextBox Tiempo
            oItem = oForm.Items.Item("Tiempo");
            oEditText = (EditText)(oItem.Specific);
            oEditText.Value = string.IsNullOrEmpty(oEditText.Value) ? "" : oEditText.Value;

            oItem = oForm.Items.Item("NumEco");
            oStaticText = (StaticText)(oItem.Specific);
            oStaticText.Caption = "";
            oItem = oForm.Items.Item("Mar");
            oStaticText = (StaticText)(oItem.Specific);
            oStaticText.Caption = "";
            oItem = oForm.Items.Item("Mod");
            oStaticText = (StaticText)(oItem.Specific);
            oStaticText.Caption = "";

            oItem = oForm.Items.Item("uso");
            oComboBox = (ComboBox)(oItem.Specific);
            oItem.Enabled = true;

            this.CleanMatrix();
        }
        #endregion

        #region ShowConfigurationLlanta
        public void ShowConfigurationLlanta()
        {
            oItem = oForm.Items.Item("Tiempo");
            oEditText = (EditText)(oItem.Specific);
            oEditText.Value = string.IsNullOrEmpty(oEditText.Value) ? "" : oEditText.Value;

            oItem = oForm.Items.Item("uso");
            oComboBox = (ComboBox)(oItem.Specific);
            oItem.Enabled = false;

            this.BindDataToForm(0, "", "", "");
        }
        #endregion

        #region Métodos Item
        public void SetID(int index)
        {
            try
            {
                string valLlanta = "";
                oItem = oForm.Items.Item("mat");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);

                try
                {
                    oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(0, index);
                    if (String.IsNullOrEmpty(oEditText.Value))
                    {
                        SBO_Application.SetStatusBarMessage("No existe registro de llanta.", BoMessageTime.bmt_Short, false);
                    }
                }
                catch
                {
                    SBO_Application.SetStatusBarMessage("No existe registro de llanta.", BoMessageTime.bmt_Short, false);
                    return;
                }

                valLlanta = oEditText.String.Trim();
                Entity.oParametroDesgaste.Llanta.LlantaID = Convert.ToInt32(valLlanta);
                id = Convert.ToInt32(valLlanta);

                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(1, index);
                valLlanta = oEditText.String.Trim();
                oItem = oForm.Items.Item("NumEco");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                oStaticText.Caption = valLlanta;

                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(2, index);
                valLlanta = oEditText.String.Trim();
                oItem = oForm.Items.Item("Mar");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                oStaticText.Caption = valLlanta;

                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(3, index);
                valLlanta = oEditText.String.Trim();
                oItem = oForm.Items.Item("Mod");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                oStaticText.Caption = valLlanta;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region RegistroIDUso
        public int RegistroIDUso()
        {
            try
            {
                int tempUso;
                int iFor = 0;
                string valUso = "";
                string valEmpID = "";
                string valSucID = "";

                ParametroDesgaste pdTempUso = new ParametroDesgaste()
                {
                    Empresa = new Kanan.Operaciones.BO2.Empresa(),
                    Sucursal = new Kanan.Operaciones.BO2.Sucursal(),
                    Llanta = new Kanan.Llantas.BO2.Llanta()
                };

                oItem = oForm.Items.Item("uso");
                oComboBox = (ComboBox)(oItem.Specific);
                valUso = oComboBox.Value;

                oItem = oForm.Items.Item("EmpID");
                oStaticText = (StaticText)(oItem.Specific);
                valEmpID = oStaticText.Caption;

                oItem = oForm.Items.Item("SucID");
                oStaticText = (StaticText)(oItem.Specific);
                valSucID = oStaticText.Caption;

                pdTempUso.Uso = Convert.ToInt32(valUso);
                pdTempUso.Empresa.EmpresaID = Convert.ToInt32(valEmpID);
                if (!string.IsNullOrEmpty(valSucID))
                    pdTempUso.Sucursal.SucursalID = Convert.ToInt32(valSucID);
                pdTempUso.Llanta.LlantaID = 0;
                SAPbobsCOM.Recordset rs = this.Entity.Consultar(pdTempUso);
                List<ParametroDesgaste> list = this.Entity.RecordSetToList(rs);
                if (list.Count > 1)
                {
                    for (int i = 0; i <= list.Count - 1; i++)
                    {
                        if (list[i].Llanta.LlantaID == 0)
                        { iFor = i; }
                    }
                    tempUso = Convert.ToInt32(list[iFor].ParametroDesgasteID);
                    return tempUso;
                }
                else
                {
                    try
                    {
                        if (list[0].ParametroDesgasteID != null)
                        {
                            tempUso = Convert.ToInt32(list[0].ParametroDesgasteID);
                            return tempUso;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    catch
                    {
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Incongruencia de datos. " + ex.Message);
            }
        }
        #endregion

        #region RegistroIDLlanta
        public int RegistroIDLlanta()
        {
            int tempLlanta = 0;
            ParametroDesgaste ListLlanta = new ParametroDesgaste()
            {
                Empresa = new Kanan.Operaciones.BO2.Empresa(),
                Sucursal = new Kanan.Operaciones.BO2.Sucursal(),
                Llanta = new Kanan.Llantas.BO2.Llanta()
            };
            ParametroDesgaste pdTempLlanta = new ParametroDesgaste()
            {
                Empresa = new Kanan.Operaciones.BO2.Empresa(),
                Sucursal = new Kanan.Operaciones.BO2.Sucursal(),
                Llanta = new Kanan.Llantas.BO2.Llanta()
            };
            pdTempLlanta.Llanta.LlantaID = Convert.ToInt32(id);
            SAPbobsCOM.Recordset rsLlanta = this.Entity.Consultar(pdTempLlanta);
            if (rsLlanta.RecordCount > 0)
            {
                ListLlanta = this.Entity.LastRecordSetToParametroDesgaste(rsLlanta);
                tempLlanta = Convert.ToInt32(ListLlanta.ParametroDesgasteID);
            }
            else
            {
                tempLlanta = Convert.ToInt32(this.id);
            }
            return tempLlanta;
        }
        #endregion

        #region SetRegistroID
        public void SetRegistroID(int? setRegistroID)
        {
            oItem = this.oForm.Items.Item("RegID");
            oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
            oStaticText.Caption = Convert.ToString(setRegistroID);
        }
        #endregion

        #region MétodoMatiz
        public void BindDataToForm(int tipo, string Variante1, string Variante2, string Variante3)
        {
            //Genera consulta para llenar la Matriz con todos los datos Rair Santos
            switch (tipo)
            {
                case 0:
                    SetConditions(0, "", "", "");
                    oDBDataSource = oForm.DataSources.DBDataSources.Item("@VSKF_LLANTA");
                    oDBDataSource.Query(oConditions ?? null);
                    oItem = oForm.Items.Item("mat");
                    oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                    oMtx.LoadFromDataSource();
                    val = 0;
                    break;
                case 1:
                    this.Entity.oParametroDesgaste = null;
                    this.Entity.oParametroDesgaste = new ParametroDesgaste()
                    {
                        Empresa = new Kanan.Operaciones.BO2.Empresa(),
                        Sucursal = new Kanan.Operaciones.BO2.Sucursal(),
                        Llanta = new Kanan.Llantas.BO2.Llanta()
                    };
                    Entity.oParametroDesgaste.Uso = Convert.ToInt32(Variante1);
                    Entity.oParametroDesgaste.Empresa.EmpresaID = Convert.ToInt32(Variante2);
                    if (!string.IsNullOrEmpty(Variante3))
                        Entity.oParametroDesgaste.Sucursal.SucursalID = Convert.ToInt32(Variante3);
                    SAPbobsCOM.Recordset rs = this.Entity.Consultar(Entity.oParametroDesgaste);
                    ParametroDesgaste list = this.Entity.LastRecordSetToParametroDesgaste(rs);
                    this.entityToFormFull(list);
                    val = 0;
                    break;
                case 2:
                    this.Entity.oParametroDesgaste = null;
                    this.Entity.oParametroDesgaste = new ParametroDesgaste()
                    {
                        Empresa = new Kanan.Operaciones.BO2.Empresa(),
                        Sucursal = new Kanan.Operaciones.BO2.Sucursal(),
                        Llanta = new Kanan.Llantas.BO2.Llanta()
                    };
                    Entity.oParametroDesgaste.Llanta.LlantaID = Convert.ToInt32(Variante1);
                    SAPbobsCOM.Recordset rsLlanta = this.Entity.Consultar(Entity.oParametroDesgaste);
                    ParametroDesgaste ListLlanta = this.Entity.LastRecordSetToParametroDesgaste(rsLlanta);
                    this.entityToFormFull(ListLlanta);
                    val = 0;
                    break;

            }

        }

        public void SetConditions(int tipo, string variante1, string variante2, string variante3)
        {
            try
            {
                switch (tipo)
                {
                    #region Case = 0
                    case 0:
                        oConditions = new SAPbouiCOM.Conditions();
                        oItem = oForm.Items.Item("SucID");
                        oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                        if (!string.IsNullOrEmpty(oStaticText.Caption))
                        {
                            oCondition = oConditions.Add();
                            oCondition.Alias = "U_SucursalID";
                            oCondition.Operation = BoConditionOperation.co_EQUAL;
                            oCondition.CondVal = oStaticText.Caption.Trim();
                        }
                        else
                        {
                            //oItem = oForm.Items.Item("EmpID");
                            //oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                            oCondition = oConditions.Add();
                            oCondition.Alias = "U_EsActivo";
                            oCondition.Operation = BoConditionOperation.co_EQUAL;
                            oCondition.CondVal = "1"; //oStaticText.Caption.Trim();
                        }
                        break;
                    #endregion

                    case 1:

                        break;
                    case 2:
                        oItem = oForm.Items.Item("NumEco");
                        oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                        oCondition = oConditions.Add();
                        oCondition.Alias = "U_NumEconomico";
                        oCondition.Operation = BoConditionOperation.co_EQUAL;
                        oCondition.CondVal = oStaticText.Caption.Trim();
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #endregion
    }
}