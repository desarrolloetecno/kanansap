﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kanan.Costos.BO2;
using Kanan.Operaciones.BO2;
using KananSAP.Costos.Data;
using KananSAP.Operaciones.Data;
using KananWS.Interface;
using KananSAP.Llantas.Data;
using KananSAP.Vehiculos.Data;
using Kanan.Vehiculos.BO2;
using SAPbouiCOM;

namespace KananSAP.GestionLlantas.View
{
    public class LlantaView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.ComboBox oComboBox;
        private SAPbouiCOM.StaticText oStaticText;
        public LlantaSAP Entity { get; set; }
        public SucursalSAP EntitySucursal { get; set; }
        public ProveedorSAP EntityProveedor { get; set; }
        private TipoVehiculoSAP eTipoVehiculo;
        public string FormUniqueID { get; set; }
        public int? id { get; set; }
        private XMLPerformance XmlApplication;
        public Configurations configs;
        #endregion

        #region Constructor
        public LlantaView(SAPbouiCOM.Application Application, SAPbobsCOM.Company company, Configurations c)
        {
            this.SBO_Application = Application;
            this.Entity = new LlantaSAP(ref company);
            this.EntitySucursal = new SucursalSAP(company);
            this.EntityProveedor = new ProveedorSAP(company);
            this.XmlApplication = new XMLPerformance(SBO_Application);
            this.eTipoVehiculo = new TipoVehiculoSAP(company);
            this.configs = c;
            this.oItem = null;
            this.oEditText = null;
            this.oComboBox = null;
            this.oStaticText = null;
            this.oForm = null;
            this.id = null;
        }
        #endregion

        #region Metodos Form
        public void LoadForm()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("Llanta");
                //FillVehicles();
            }
            catch (Exception ex)
            {
                if (oForm == null || ex.Message.Contains("Invalid Form"))
                {
                    string sPath = System.Windows.Forms.Application.StartupPath;                   
                    this.XmlApplication.LoadFromXML(sPath + "\\XML", "Llantas.xml");
                    this.oForm = SBO_Application.Forms.Item("Llanta");

                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;

                    oItem = oForm.Items.Item("lblid");
                    oStaticText = (StaticText)(oItem.Specific);
                    oStaticText.Caption = this.id != null ? this.id.ToString() : string.Empty;
                    oItem.Visible = false ;

                    AddItemsCFL();
                    FillProveedor();
                    FillSucursales();
                    FillTipoVehiculo();

                    if (id == null)
                    {
                        HideDeleteButton();
                    }
                    else
                        ShowDeleteButton();
                }
                else
                {
                    SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                }
            }
        }

        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
            }
            catch (Exception)
            {
                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                this.oForm.Select();
            }
        }

        public void CloseForm()
        {
            this.id = null;
            if (this.oForm.Visible & this.oForm.Selected)
                this.oForm.Close();
        }

        public void AddItemsCFL()
        {
            try
            {
                oForm.DataSources.UserDataSources.Add("EditDS", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254); 
                SAPbouiCOM.ChooseFromListCollection oCFLs = null;
                oCFLs = oForm.ChooseFromLists;
                SAPbouiCOM.ChooseFromList oCFL = null;
                SAPbouiCOM.ChooseFromListCreationParams oCFLCreationParams = null;
                oCFLCreationParams = ((SAPbouiCOM.ChooseFromListCreationParams)(SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)));
                oCFLCreationParams.MultiSelection = false;
                oCFLCreationParams.ObjectType = "4";
                oCFLCreationParams.UniqueID = "CFL1";
                oCFL = oCFLs.Add(oCFLCreationParams);
                oItem = oForm.Items.Item("txtitem");
                oEditText = (EditText)(oItem.Specific);
                oEditText.DataBind.SetBound(true, "", "EditDS");
                oEditText.ChooseFromListUID = "CFL1";
                oEditText.ChooseFromListAlias = "ItemCode"; 
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void FillSucursales()
        {
            var rs = this.EntitySucursal.GetAllSucursales();
            rs.MoveFirst();

            oItem = this.oForm.Items.Item("cmbsuc");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            CleanComboBox(oComboBox);

            oComboBox.ValidValues.Add("0", "Ninguna");
            for (int i = 0; i < rs.RecordCount; i++)
            {
                oComboBox.ValidValues.Add(rs.Fields.Item("U_SUCURSALID").Value.ToString(), rs.Fields.Item("Name").Value.ToString());
                rs.MoveNext();
            }
            /*Se declara 2 veces debido a que en la primera no es visible a nivel XML*/
            oComboBox.Select("0", BoSearchKey.psk_ByValue);
            oComboBox.Select("0", BoSearchKey.psk_ByValue);
        }

        private void FillProveedor()
        {
            try
            {
                var rs = this.EntityProveedor.GetProveedoresKF();

                oItem = this.oForm.Items.Item("cmbprov");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                CleanComboBox(oComboBox);

                oComboBox.ValidValues.Add("0", "Ninguna");
                foreach (Proveedor t in rs)
                {
                    oComboBox.ValidValues.Add(t.ProveedorID.ToString(), t.Nombre);
                }

                /*Se declara 2 veces debido a que en la primera no se muestra corectamente hasta la segunda declaración. */
                oComboBox.Select("0", BoSearchKey.psk_ByValue);
                oComboBox.Select("0", BoSearchKey.psk_ByValue);
            }
            catch { }
        }
        private void FillTipoVehiculo()
        {
            try
            {
                var rs = this.eTipoVehiculo.Consultar(new TipoVehiculo { EsActivo = true });
                oItem = oForm.Items.Item("cmbuso");
                oComboBox = (ComboBox)(oItem.Specific);
                CleanComboBox(oComboBox);

                rs.MoveFirst();
                for (int i = 0; i < rs.RecordCount; i++)
                {
                    string nombre = rs.Fields.Item("U_Nombre").Value.ToString();
                    string value = (string)Convert.ChangeType(rs.Fields.Item("U_TVeiculoID").Value, typeof(string));
                    oComboBox.ValidValues.Add(value, nombre);
                    rs.MoveNext();
                }
                oComboBox.Item.DisplayDesc = true;
            }
            catch { }
        }

        private void CleanComboBox(ComboBox combo)
        {
            if (combo == null || combo.ValidValues == null || combo.ValidValues.Count <= 0) return;
            var f = combo.ValidValues.Count;
            for (int i = 0; i < f; i++)
            {
                combo.ValidValues.Remove(i, BoSearchKey.psk_Index);
            }
        }

        public bool defineRepeticionInsert(ref int Repeticion, ref string sCadena)
        {
            bool bRepetir = false;

            oItem = this.oForm.Items.Item("chkMsvo");
            CheckBox oChkMasivo = (CheckBox)this.oItem.Specific;
            bRepetir = oChkMasivo.Checked;
            if (bRepetir)
            {
                oItem = this.oForm.Items.Item("txtPref");
                oItem.Enabled = oChkMasivo.Checked;
                EditText txtCaja = (EditText)this.oItem.Specific;
                sCadena = Convert.ToString(txtCaja.Value);
                oItem = this.oForm.Items.Item("txtRep");
                oItem.Enabled = oChkMasivo.Checked;
                txtCaja = (EditText)this.oItem.Specific;
                Repeticion = Convert.ToInt32(txtCaja.Value);
            }
            return bRepetir;
        }
        public void HabilitaMasivo()
        {
            oItem = this.oForm.Items.Item("chkMsvo");
            CheckBox oChkMasivo = (CheckBox)this.oItem.Specific;

            oItem = this.oForm.Items.Item("txtPref");
            oItem.Enabled = oChkMasivo.Checked;
            oItem = this.oForm.Items.Item("txtRep");
            oItem.Enabled = oChkMasivo.Checked;
        }
        public void HideDeleteButton()
        {
            oItem = oForm.Items.Item("btnelimina");
            oItem.Visible = false;
            Item hided = oItem;

            oItem = oForm.Items.Item("2");
            oItem.Left = hided.Left;
        }

        public void ShowDeleteButton()
        {
            oItem = oForm.Items.Item("btnelimina");
            oItem.Visible = true;
            Item hided = oItem;

            oItem = oForm.Items.Item("2");
            oItem.Left = hided.Left + hided.Width + 3;
        }
        #endregion

        #region Metodos Items

        public bool FormToEntity()
        {
            bool bContinua = false;
            string scadenaintefaz = string.Empty;
            Int32 iEnteroValido;
            this.oForm = SBO_Application.Forms.Item("Llanta");

            oItem = this.oForm.Items.Item("txtitem");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.ItemCode = oEditText.String;

            oItem = this.oForm.Items.Item("cmbsuc");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            this.Entity.llanta.SubPropietario = oComboBox.Selected == null ? new Sucursal() : new Sucursal { SucursalID = Convert.ToInt32(oComboBox.Selected.Value) == 0 ? (int?)null : Convert.ToInt32(oComboBox.Selected.Value) };
            /*this.Entity.llanta.SubPropietario = oComboBox.Selected == null ? new Sucursal() : new Sucursal { SucursalID = Convert.ToInt32(oComboBox.Selected.Value) == 0 ? (int?)null : Convert.ToInt32(oComboBox.Selected.Value) };*/

            oItem = this.oForm.Items.Item("cmbprov");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            this.Entity.llanta.proveedorLlanta = oComboBox.Selected == null ? new Proveedor() : new Proveedor() { ProveedorID = Convert.ToInt32(oComboBox.Selected.Value) == 0 ? (int?)null : Convert.ToInt32(oComboBox.Selected.Value) };
            /*Ethis.Entity.llanta.proveedorLlanta = oComboBox.Selected == null ? new Proveedor() : new Proveedor() { ProveedorID = Convert.ToInt32(oComboBox.Selected.Value) == 0 ? (int?)null : Convert.ToInt32(oComboBox.Selected.Value) };*/

            oItem = this.oForm.Items.Item("txtnoeco");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.llanta.NumeroEconomico = oEditText.String;

            oItem = this.oForm.Items.Item("txtnoserie");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.llanta.SN = oEditText.String;

            oItem = this.oForm.Items.Item("txtmarca");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.llanta.Marca = oEditText.String;

            oItem = this.oForm.Items.Item("txtmodelo");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.llanta.Modelo = oEditText.String;

            oItem = this.oForm.Items.Item("txtnorev");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            try
            {
                this.Entity.llanta.NumeroRevestimientos = string.IsNullOrEmpty(oEditText.String) ? (int?)null : Convert.ToInt32(oEditText.String);
                
            }
            catch (Exception)
            {
                throw new Exception("El valor del campo 'No. Revestiminetos' debe ser numérico");
            }

            oItem = this.oForm.Items.Item("txtnocapas");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            try
            {
                this.Entity.llanta.NumeroCapas = string.IsNullOrEmpty(oEditText.String) ? (int?)null : Convert.ToInt32(oEditText.String);
            }
            catch (Exception)
            {
                throw new Exception("El valor del campo 'No. de Capas' debe ser numérico");
            }

            oItem = this.oForm.Items.Item("txtcosto");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            try
            {
                this.Entity.llanta.Costo = string.IsNullOrEmpty(oEditText.String) ? (decimal?)null : Convert.ToDecimal(oEditText.String);
            }
            catch (Exception)
            {
                throw new Exception("El valor del campo 'Costo' debe ser numérico");
            }
           
            oItem = this.oForm.Items.Item("txtnofac");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.llanta.NumeroFactura = oEditText.String;

            oItem = this.oForm.Items.Item("txtfcompra");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            try
            {
                this.Entity.llanta.FechaCompra = string.IsNullOrEmpty(oEditText.String) ? (DateTime?)null : DateTime.Parse(oEditText.String);
            }
            catch (Exception)
            {
                throw new Exception("El valor del campo 'Fecha de Compra' debe ser una fecha válida");
            }

            oItem = this.oForm.Items.Item("txtobserv");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.llanta.Observaciones = oEditText.String;

            oItem = this.oForm.Items.Item("txtkmcad");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            decimal KilometrosCaducidad;
            if (Decimal.TryParse(oEditText.String, out KilometrosCaducidad))
                this.Entity.llanta.KilometrosCaducidad = Convert.ToDecimal(oEditText.Value);

            oItem = this.oForm.Items.Item("txtdist");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            try
            {
                this.Entity.llanta.KilometrosRecorridos = string.IsNullOrEmpty(oEditText.String) ? (double?)null : Convert.ToDouble(oEditText.String);
            }
            catch (Exception)
            {
                throw new Exception("El valor del campo 'Distancia Recorrida' debe ser numérico");
            }

            oItem = this.oForm.Items.Item("cmbuso");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            this.Entity.llanta.Uso = oComboBox.Selected == null ? null : (int?)Convert.ToInt32(oComboBox.Selected.Value);

            oItem = this.oForm.Items.Item("txtancho");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.llanta.Ancho = oEditText.String;

            oItem = this.oForm.Items.Item("txtrelasp");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.llanta.Aspecto = oEditText.String;

            oItem = this.oForm.Items.Item("cmbconstr");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            this.Entity.llanta.Construccion = oComboBox.Selected == null ? null : (int?)Convert.ToInt32(oComboBox.Selected.Value);

            oItem = this.oForm.Items.Item("txtdiamtr");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.llanta.DiametroRin = oEditText.String.Trim();

            oItem = this.oForm.Items.Item("txtindice");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.llanta.IndiceCarga = oEditText.String.Trim();

            oItem = this.oForm.Items.Item("cmbvel");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            this.Entity.llanta.RangoVelocidad = oComboBox.Selected == null ? (int?)null : Convert.ToInt32(oComboBox.Selected.Value);

            oItem = this.oForm.Items.Item("cmbaplic");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            this.Entity.llanta.Aplicacion = oComboBox.Selected == null ? (int?)null : (Convert.ToInt32(oComboBox.Selected.Value));

            oItem = this.oForm.Items.Item("cmbtracc");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            this.Entity.llanta.Traccion = oComboBox.Selected == null ? (int?)null : (Convert.ToInt32(oComboBox.Selected.Value));

            oItem = this.oForm.Items.Item("txttread");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.llanta.Treadwear = oEditText.String.Trim();

            oItem = this.oForm.Items.Item("cmbtemp");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            this.Entity.llanta.Temperatura = oComboBox.Selected == null ? (int?)null : Convert.ToInt32(oComboBox.Selected.Value);

            oItem = this.oForm.Items.Item("txtmaxpres");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.llanta.MaximaPresionInflado = oEditText.String.Trim();

            oItem = this.oForm.Items.Item("txtminpres");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.llanta.MinimaPresionInflado = oEditText.String.Trim();

            oItem = this.oForm.Items.Item("txtffabr");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            try
            {
                if (!string.IsNullOrEmpty(oEditText.String))
                    this.Entity.llanta.FechaFabricacion = string.IsNullOrEmpty(oEditText.String) ? (DateTime?)null : DateTime.Parse(oEditText.String.Trim());
                else throw new Exception("El valor del campo 'Fecha de Fabricación' debe ser una fecha válida");
            }
            catch (Exception)
            {
                throw new Exception("El valor del campo 'Fecha de Fabricación' debe ser una fecha válida");
            }

            oItem = this.oForm.Items.Item("txtfechcad");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            try
            {
                if (!string.IsNullOrEmpty(oEditText.String))
                    this.Entity.llanta.FechaCaducidad = string.IsNullOrEmpty(oEditText.String) ? (DateTime?)null : DateTime.Parse(oEditText.String.Trim());
                else throw new Exception("El valor del campo 'Fecha de caducidad' debe ser una fecha válida");
            }
            catch (Exception)
            {
                throw new Exception("El valor del campo 'Fecha de caducidad' debe ser una fecha válida");
            }

            this.Entity.llanta.Propietario = new Empresa{EmpresaID = configs.UserFull.Dependencia.EmpresaID};
            bContinua = true;
            //this.Entity.llanta.EstaInstalado = false;
            return bContinua;
        }

        public void EntityToForm()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("Llanta");

                oForm.DataSources.UserDataSources.Item("EditDS").ValueEx = this.Entity.ItemCode ?? string.Empty;

                oItem = this.oForm.Items.Item("txtitem");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.ItemCode ?? string.Empty;

                oItem = this.oForm.Items.Item("cmbsuc");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                if (this.Entity.llanta.SubPropietario != null && this.Entity.llanta.SubPropietario.SucursalID != null)
                    oComboBox.Select(this.Entity.llanta.SubPropietario.SucursalID.ToString());
                else oComboBox.Select("0");
                if (this.Entity.llanta.SubPropietario != null && this.Entity.llanta.SubPropietario.SucursalID != null)
                    oComboBox.Select(this.Entity.llanta.SubPropietario.SucursalID.ToString());
                else
                    oComboBox.Select("0");

                oItem = this.oForm.Items.Item("cmbprov");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                if (this.Entity.llanta.proveedorLlanta != null && this.Entity.llanta.proveedorLlanta.ProveedorID != null)
                    oComboBox.Select(this.Entity.llanta.proveedorLlanta.ProveedorID.ToString());
                else oComboBox.Select("0");
                if (this.Entity.llanta.proveedorLlanta != null && this.Entity.llanta.proveedorLlanta.ProveedorID != null)
                    oComboBox.Select(this.Entity.llanta.proveedorLlanta.ProveedorID.ToString());
                else
                    oComboBox.Select("0");

                oItem = this.oForm.Items.Item("txtnoeco");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.llanta.NumeroEconomico;

                oItem = this.oForm.Items.Item("txtnoserie");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.llanta.SN;

                oItem = this.oForm.Items.Item("txtmarca");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.llanta.Marca;

                oItem = this.oForm.Items.Item("txtmodelo");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.llanta.Modelo;

                oItem = this.oForm.Items.Item("txtnorev");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.llanta.NumeroRevestimientos.ToString();

                oItem = this.oForm.Items.Item("txtnocapas");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.llanta.NumeroCapas.ToString();

                oItem = this.oForm.Items.Item("txtcosto");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.llanta.Costo.ToString();

                oItem = this.oForm.Items.Item("txtnofac");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.llanta.NumeroFactura;

                oItem = this.oForm.Items.Item("txtfcompra");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.llanta.FechaCompra != null ? this.Entity.llanta.FechaCompra.Value.ToShortDateString() : string.Empty;

                oItem = this.oForm.Items.Item("txtkmcad");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = Convert.ToString(this.Entity.llanta.KilometrosCaducidad);

                oItem = this.oForm.Items.Item("txtobserv");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.llanta.Observaciones;

                oItem = this.oForm.Items.Item("txtdist");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.llanta.KilometrosRecorridos.ToString();

                oItem = this.oForm.Items.Item("cmbuso");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                oComboBox.Select(this.Entity.llanta.Uso != null ? this.Entity.llanta.Uso.ToString() : "0");

                oItem = this.oForm.Items.Item("txtancho");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.llanta.Ancho;

                oItem = this.oForm.Items.Item("txtrelasp");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.llanta.Aspecto;

                oItem = this.oForm.Items.Item("cmbconstr");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                oComboBox.Select(this.Entity.llanta.Construccion != null ? this.Entity.llanta.Construccion.ToString() : "0");

                oItem = this.oForm.Items.Item("txtdiamtr");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.llanta.DiametroRin;

                oItem = this.oForm.Items.Item("txtindice");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.llanta.IndiceCarga;

                oItem = this.oForm.Items.Item("cmbvel");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                oComboBox.Select(this.Entity.llanta.RangoVelocidad != null ? this.Entity.llanta.RangoVelocidad.ToString() : "0");

                oItem = this.oForm.Items.Item("cmbaplic");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                oComboBox.Select(this.Entity.llanta.Aplicacion != null ? this.Entity.llanta.Aplicacion.ToString() : "0");

                oItem = this.oForm.Items.Item("cmbtracc");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                oComboBox.Select(this.Entity.llanta.Traccion != null ? this.Entity.llanta.Traccion.ToString() : "0");

                oItem = this.oForm.Items.Item("txttread");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.llanta.Treadwear;

                oItem = this.oForm.Items.Item("cmbtemp");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                oComboBox.Select(this.Entity.llanta.Temperatura != null ? this.Entity.llanta.Temperatura.ToString() : "0");

                oItem = this.oForm.Items.Item("txtmaxpres");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.llanta.MaximaPresionInflado;

                oItem = this.oForm.Items.Item("txtminpres");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.llanta.MinimaPresionInflado;

                oItem = this.oForm.Items.Item("txtffabr");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.llanta.FechaFabricacion != null ? this.Entity.llanta.FechaFabricacion.Value.ToShortDateString() : string.Empty;

                oItem = this.oForm.Items.Item("txtfechcad");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                oEditText.String = this.Entity.llanta.FechaCaducidad != null ? this.Entity.llanta.FechaCaducidad.Value.ToShortDateString() : string.Empty;
            }
            catch (Exception oError)
            {
                throw new Exception(oError.Message);
            }
        }

        public void EmptyEntityToForm()
        {
            this.oForm = SBO_Application.Forms.Item("Llanta");

            oItem = this.oForm.Items.Item("txtitem");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("cmbsuc");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select("0", SAPbouiCOM.BoSearchKey.psk_ByValue);

            oItem = this.oForm.Items.Item("cmbprov");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select("0", SAPbouiCOM.BoSearchKey.psk_ByValue);

            oItem = this.oForm.Items.Item("txtnoeco");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtnoserie");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtmarca");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtmodelo");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtnorev");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtnocapas");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtcosto");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtnofac");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtfcompra");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtkmcad");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtobserv");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtdist");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("cmbuso");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select("0");

            oItem = this.oForm.Items.Item("txtancho");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtrelasp");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("cmbconstr");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select("0");

            oItem = this.oForm.Items.Item("txtdiamtr");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtindice");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("cmbvel");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select("0");

            oItem = this.oForm.Items.Item("cmbaplic");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select("0");

            oItem = this.oForm.Items.Item("cmbtracc");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select("0");

            oItem = this.oForm.Items.Item("txttread");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("cmbtemp");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select("0");

            oItem = this.oForm.Items.Item("txtmaxpres");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtminpres");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtffabr");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtfechcad");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;
        }

        public bool ValidateLlanta()
        {
            bool continuar = true;
            try
            {
                int FechaActual = 0;

                oItem = oForm.Items.Item("txtnoeco");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (string.IsNullOrEmpty(oEditText.String))
                {
                    SBO_Application.SetStatusBarMessage("El campo 'No. Economico' es obligatorio", BoMessageTime.bmt_Short, false);
                    continuar = false;
                }

                oItem = oForm.Items.Item("txtnoserie");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (string.IsNullOrEmpty(oEditText.String))
                {
                    SBO_Application.SetStatusBarMessage("El campo 'No. Serie' es obligatorio", BoMessageTime.bmt_Short, false);
                    continuar = false;
                }

                oItem = oForm.Items.Item("txtmarca");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (string.IsNullOrEmpty(oEditText.String))
                {
                    SBO_Application.SetStatusBarMessage("El campo 'Marca' es obligatorio", BoMessageTime.bmt_Short, false);
                    continuar = false;
                }

                oItem = oForm.Items.Item("txtmodelo");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (string.IsNullOrEmpty(oEditText.String))
                {
                    SBO_Application.SetStatusBarMessage("El campo 'Modelo' es obligatorio", BoMessageTime.bmt_Short, false);
                    continuar = false;
                }

                oItem = oForm.Items.Item("cmbprov");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                if (string.IsNullOrEmpty(oComboBox.Selected.Value) || string.IsNullOrEmpty(oComboBox.Selected.Description))
                {
                    SBO_Application.SetStatusBarMessage("El campo 'Proveedor' es obligatorio", BoMessageTime.bmt_Short, false);
                    continuar = false;
                }

                if (oComboBox.Selected.Value == "0")
                {
                    SBO_Application.SetStatusBarMessage("El campo 'Proveedor' no puede ser '0' o 'Ninguno'. ", BoMessageTime.bmt_Short, false);
                    continuar = false;
                }

                oItem = oForm.Items.Item("txtfcompra");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                int fechaCapturada;
                fechaCapturada = String.IsNullOrEmpty(oEditText.Value) ? 0 : Convert.ToInt32(oEditText.Value);
                if (String.IsNullOrEmpty(oEditText.Value))
                {
                    SBO_Application.SetStatusBarMessage("La fecha de compra debe existir.", BoMessageTime.bmt_Short, false);
                    continuar = false;
                }
                else
                    FechaActual = ObtenerFechaActualSAP();
                if (fechaCapturada > FechaActual)
                {
                    SBO_Application.SetStatusBarMessage("La fecha de compra no puede ser mayor a la fecha actual.", BoMessageTime.bmt_Short, false);
                    continuar = false;
                }

                /* Se valida que los valores del campo kilimetraje sea de tipo numérico. */
                oItem = oForm.Items.Item("txtkmcad");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (!String.IsNullOrEmpty(oEditText.String))
                {
                    try
                    {
                        if (Convert.ToDecimal(oEditText.String).GetType() != typeof(decimal))
                        {
                            SBO_Application.StatusBar.SetText("El valor del campo 'Kilometaje caducidad', debe ser de tipo numérico. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                            continuar = false;
                        }
                    }
                    catch
                    {
                        SBO_Application.StatusBar.SetText("El valor del campo 'Kilometaje caducidad', debe ser de tipo numérico. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                        continuar = false;
                    }
                }

                /*Se valida que la fecha de caducidad no sea menor que la fecha de fabricación así como la fecha de compra. */
                oItem = oForm.Items.Item("txtfechcad");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if(!String.IsNullOrEmpty(oEditText.String))
                {
                    if (Convert.ToDateTime(oEditText.String).GetType() == typeof(DateTime))
                    {
                        DateTime FechaCaducidad = Convert.ToDateTime(oEditText.String);
                        DateTime FechaCompra;
                        DateTime FechaFabricacion;

                        oItem = oForm.Items.Item("txtffabr");
                        oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                        if (!String.IsNullOrEmpty(oEditText.String) & Convert.ToDateTime(oEditText.String).GetType() == typeof(DateTime))
                        {
                            FechaFabricacion = Convert.ToDateTime(oEditText.String);
                            if (FechaCaducidad < FechaFabricacion)
                            {
                                SBO_Application.StatusBar.SetText("El campo 'Fecha Caducidad' no debe ser menor que 'Fecha de fabricación'. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                continuar = false;
                            }
                        }

                        oItem = oForm.Items.Item("txtfcompra");
                        oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                        if (!String.IsNullOrEmpty(oEditText.String) & Convert.ToDateTime(oEditText.String).GetType() == typeof(DateTime))
                        {
                            FechaCompra = Convert.ToDateTime(oEditText.String);
                            if (FechaCaducidad < FechaCompra)
                            {
                                SBO_Application.StatusBar.SetText("El campo 'Fecha Caducidad' debe ser menor que 'Fecha compra'. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                continuar = false;
                            }
                        }
                    }
                    else
                    {
                        SBO_Application.StatusBar.SetText("El campo 'Fecha Caducidad' debe ser de tipo fecha. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                        continuar = false;
                    }
                }

                return continuar;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Metodos Auxiliares
        public string GetLlantaID()
        {
            oItem = oForm.Items.Item("lblid");
            oStaticText = (StaticText)(oItem.Specific);
            return oStaticText.Caption;
        }

        public int ObtenerFechaActualSAP()
        {
            String fecha;
            DateTime DateNow = DateTime.Today;
            String Year = "";
            String Month = "";
            String Day = "";

            Year = DateNow.Year.ToString();
            Month = DateNow.Month.ToString();
            Day = DateNow.Day.ToString();

            if (Month.Count() < 2)
                Month = "0" + Month;
            if (Day.Count() < 2)
                Day = "0" + Day;

            fecha = Year.ToString() + Month.ToString() + Day.ToString();

            return Convert.ToInt32(fecha);
        }

        #endregion

    }
}
