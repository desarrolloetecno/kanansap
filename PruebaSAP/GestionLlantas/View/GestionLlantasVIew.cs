﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbouiCOM;
using Application = System.Windows.Forms.Application;

namespace KananSAP.GestionLlantas.View
{
    public class GestionLlantasVIew
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.StaticText oStaticText;
        private SAPbouiCOM.LinkedButton oLinkedButton;
        private SAPbouiCOM.Matrix oMtx;
        private SAPbouiCOM.DBDataSource oDBDataSource;
        private SAPbouiCOM.Columns oColumns;
        private SAPbouiCOM.Column oColumn;
        private SAPbouiCOM.Button oButton;
        private SAPbouiCOM.Conditions oConditions;
        private SAPbouiCOM.Condition oCondition;
        //public CargaCombustibleSAP Entity { get; set; }
        public string FormUniqueID { get; set; }
        private XMLPerformance XmlApplication;
        public int? llantaid;
        #endregion

        #region Constructor
        public GestionLlantasVIew(SAPbouiCOM.Application Application, SAPbobsCOM.Company company)
        {
            this.SBO_Application = Application;
            //this.Entity = new CargaCombustibleSAP(company);
            this.XmlApplication = new XMLPerformance(SBO_Application);
            //this.LoadForm();
            this.oItem = null;
            this.oLinkedButton = null;
            this.oStaticText = null;
            this.oEditText = null;
            this.oConditions = null;
            this.oCondition = null;
            this.oColumns = null;
            this.oColumn = null;
            this.llantaid = null;
        }
        #endregion

        #region Metodos
        #region Metodos Form
        public void LoadForm()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("AdmLlantas");
                BindDataToForm();
            }
            catch
            {
                string sPath = Application.StartupPath;
                
                this.XmlApplication.LoadFromXML(sPath + "\\XML",
                        "GestionLlantas.xml");
                this.oForm = SBO_Application.Forms.Item("AdmLlantas");

                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;

                //oItem = oForm.Items.Item("lblid");
                //oStaticText = (StaticText)(oItem.Specific);
                //oStaticText.Caption = this.llantaid.ToString();
                //oItem.Visible = false;
            }
        }

        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
                BindDataToForm();
            }
            catch (Exception)
            {
                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                BindDataToForm();
                this.oForm.Select();
            }
        }
        #endregion

        #region Metodos Items
        public void SetID(int index)
        {
            try
            {
                oItem = oForm.Items.Item("MtxFR");
                oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
                oEditText = (SAPbouiCOM.EditText)oMtx.GetCellSpecific(1, index);
                //this.oItem = this.oForm.Items.Item("txtempl");
                //this.oEditText = (SAPbouiCOM.EditText)(this.oItem.Specific);
                this.llantaid = Convert.ToInt32(oEditText.String.Trim());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Metodos Matrix
        public void BindDataToForm()
        {
            SetConditions();
            oDBDataSource = oForm.DataSources.DBDataSources.Item("@VSKF_LLANTA");
            oDBDataSource.Query(oConditions ?? null);
            oItem = oForm.Items.Item("MtxFR");
            oMtx = (SAPbouiCOM.Matrix)(oItem.Specific);
            oMtx.LoadFromDataSource();
        }

        public void SetConditions()
        {
            oConditions = new SAPbouiCOM.Conditions();
            oCondition = oConditions.Add();
            oCondition.Alias = "U_EsActivo";
            oCondition.Operation = BoConditionOperation.co_EQUAL;
            oCondition.CondVal = "1";
        }

        #endregion

        #endregion
    }
}
