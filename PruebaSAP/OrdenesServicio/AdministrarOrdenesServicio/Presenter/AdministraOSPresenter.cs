﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KananSAP.AdministrarOrdenesServicio.Views;
using KananSAP.GestionUsuarios.AdministraUsuarios;
using KananWS.Interface;
using SAPbouiCOM;

namespace KananSAP.AdministrarOrdenesServicio.Presenter
{
    public class AdministraOSPresenter
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private SAPbouiCOM.Item oItem;
        public AdministraOSView view { get; set; }
        private Configurations configs;
        private OrdenServicioWS OrdenServicioWs;
        private bool Exist;
        private bool Modal;

        #endregion

        #region Constructor
        public AdministraOSPresenter(SAPbouiCOM.Application aplication, SAPbobsCOM.Company company, Configurations c)
        {
            this.configs = c;
            this.SBO_Application = aplication;
            this.Company = company;
            this.view = new AdministraOSView(this.SBO_Application, this.Company);
            this.OrdenServicioWs = new OrdenServicioWS(Guid.Parse(this.configs.UserFull.Usuario.PublicKey), this.configs.UserFull.Usuario.PrivateKey); ;
            this.Exist = false;
            this.Modal = false;
            SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
        }
        #endregion

        #region Eventos
        private void SBO_Application_ItemEvent(string FormUID, ref ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                #region ListadoOrdenes
                if (pVal.FormTypeEx == "AdminOServ")
                {
                    if (Modal)
                    {
                        //userview.ShowForm();
                        BubbleEvent = false;
                        return;
                    }
                    #region beforeaction

                    if (pVal.BeforeAction)
                    {
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "grdorden")
                        {
                            this.view.SetOSRowIndex(pVal.Row);
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnfiltro")
                        {
                            this.view.AplyFilter();
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnorden")
                        {
                            this.view.GetOrdenFromGrid();
                        }
                    }
                    #endregion
                    #region After Action
                    #endregion 
                }
                #endregion
            }
            catch(Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraOSPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
                SBO_Application.StatusBar.SetText(ex.Message,BoMessageTime.bmt_Short);
            }
        }
        #endregion

        #region Metodos

        #region Acciones Operadores
        private void Update()
        {
            try
            {
                //this.userview.FormToEntity();
                
                //EmpleadoToOperador(this.userview.Entity.oEmpleado, this.userview.EntityOperador.operador);
                //this.userview.EntityOperador.Sincronizado = 2;
                //this.userview.EntityOperador.OperadorToUDT();
                //this.userview.EntityOperador.Update();

                //this.EmpleadoService.Update(this.userview.Entity.oEmpleado);
                //this.EmpleadoService.UpdateOprdr(this.userview.EntityOperador.operador);

                //this.userview.EntityOperador.Sincronizado = 0;
                //this.userview.EntityOperador.OperadorToUDT();
                //this.userview.EntityOperador.Update();
                
                //ManageLicencia();
                //ManageCursos();
                //ManageObservaciones();

                this.SBO_Application.StatusBar.SetText("Operación Exitosa", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            }
            catch (Exception ex)
            {
                //this.userview.EmptyEntityToForm();
                //this.userview.CloseForm();
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraOSPresenter.cs->Update()", ex.Message);
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Acciones Licencia
        public void ManageLicencia()
        {
            try
            {
                //foreach (var lic in userview.listlicencia)
                //{
                //    if (lic.LicenciaID == null || lic.LicenciaID <= 0)
                //    {
                //        this.userview.ELicencia.licencia = lic;
                //        InsertLicencia();
                //    }
                //}

                //foreach (var lic in userview.edtlicencia)
                //{
                //    this.userview.ELicencia.oUDT.GetByKey(lic.LicenciaID.ToString());
                //    this.userview.ELicencia.UDTToLicencia();
                //    this.userview.ELicencia.licencia = lic;
                //    UpdateLicencia();
                //}

                //foreach (var lic in userview.dellicencia)
                //{
                //    if (lic.LicenciaID != null && lic.LicenciaID > 0)
                //    {
                //        this.userview.ELicencia.licencia = lic;
                //        DeleteLicencia();
                //    }
                //}
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraOSPresenter.cs->_MergeLicencia()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public void InsertLicencia()
        {
            try
            {
                //this.userview.ELicencia.opid = this.userview.EntityOperador.operador.OperadorID;
                //this.userview.ELicencia.UUID = Guid.NewGuid();
                //this.userview.ELicencia.Sincronizado = 1;
                //this.userview.ELicencia.LicenciaToUDT();
                //this.userview.ELicencia.Insert();

                //this.userview.ELicencia.licencia = this.EmpleadoService.AddLicencia(userview.EntityOperador.operador,
                //    userview.ELicencia.licencia);

                //this.userview.ELicencia.Sincronizado = 0;
                //this.userview.ELicencia.ActualizarCode(userview.ELicencia.licencia);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraOSPresenter.cs->InsertLicencia()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public void UpdateLicencia()
        {
            try
            {
                //this.userview.ELicencia.opid = this.userview.EntityOperador.operador.OperadorID;
                //this.userview.ELicencia.LicenciaToUDT();
                //this.userview.ELicencia.Update();

                //this.userview.ELicencia.licencia = this.EmpleadoService.UpdateLicencia(userview.ELicencia.licencia);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraOSPresenter.cs->UpdateLicencia()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public void DeleteLicencia()
        {
            try
            {
                //this.userview.ELicencia.oUDT.GetByKey(this.userview.ELicencia.licencia.LicenciaID.ToString());
                //this.userview.ELicencia.Sincronizado = 3;
                //this.userview.ELicencia.LicenciaToUDT();
                //this.userview.ELicencia.Update();

                //this.userview.ELicencia.licencia = this.EmpleadoService.DeleteLicencia((int)userview.ELicencia.licencia.LicenciaID);

                //this.userview.ELicencia.Remove();
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AdministraOSPresenter.cs->DeleteLicencia()", ex.Message);
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion
    }
}
