﻿using KananFleet.OrdenesServicio.SolicitarOrdenServicio.Views;
using KananSAP;
using KananSAP.Helper;
using KananWS.Interface;
using SAPbouiCOM;
using SAPinterface.Data.INFO.Objetos;
using SAPinterface.Data.INFO.Tablas;
using SAPinterface.Data.PD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application = System.Windows.Forms.Application;

namespace KananFleet.OrdenesServicio.AdministrarOrdenesServicio.Views
{
    public class SBO_KF_AdministraOSView
    {

        #region Atributos
        private SAPbouiCOM.Application SBO_Application;                
        private XMLPerformance XmlApplication;
        private GestionGlobales oGlobales = null;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;        
        private SAPbouiCOM.Grid oGrid;
        private SAPbouiCOM.ComboBox oCombo;
        private string iConsecutivo = string.Empty;
        private SAPbobsCOM.Company oCompany;
        private int OSRowIndex = -1;
        private Configurations Oconfig = null;
        private List<SBO_KF_EstatusOrden_INFO> lstEstatus = null;
        private List<SBO_KF_OrdenServicioAdmin_INFO> lstOrdenes = null;
        const string CONST_CLASE = "SBO_KF_AdministraOSView.cs";
        private KananSAP.Helper.KananSAPHerramientas oHerramientas;
        private SBO_KF_SolicitaOrdenesView oView = null;
        private Dictionary<string, SBO_KF_SolicitaOrdenesView> lstOrdenesView = new Dictionary<string, SBO_KF_SolicitaOrdenesView>();
        #endregion

        public SBO_KF_AdministraOSView(GestionGlobales oGlobales, Configurations Oconnfig, ref SAPbobsCOM.Company Company)
        {

            this.Oconfig = Oconnfig;
            this.oGlobales = oGlobales;
            this.SBO_Application = this.oGlobales.SBO_Application;
            this.XmlApplication = new XMLPerformance(SBO_Application);
            this.oCompany = Company;
            Boolean bContinuar = LoadForm();
            this.ShowForm();
            this.oHerramientas = new KananSAPHerramientas(this.SBO_Application);

        }

        private Boolean LoadForm()
        {
            string UID = "AdminOServ_1";
            FormCreationParams oFormCreate = null;
            Boolean bContinuar = false;
            String XmlData = "";
            try
            {

                this.oForm = SBO_Application.Forms.Item(UID);
                if (oForm.UniqueID != UID)
                    throw new Exception("");
                bContinuar = true;
            }
            catch (Exception exp)
            {
                KananSAPHerramientas.LogError("SBO_KF_AdministraOSView.cs->LoadForm()", exp.Message);
                bContinuar = false;
                try
                {
                    string sPath = Application.StartupPath;
                    XmlData = this.XmlApplication.LoadFromXML(sPath + "\\XML", "", "AdminOrdenesServicio.xml");
                    bContinuar = true;
                    oFormCreate = (FormCreationParams)this.oGlobales.SBO_Application.CreateObject(BoCreatableObjectType.cot_FormCreationParams);
                    oFormCreate.UniqueID = UID;
                    oFormCreate.FormType = "AdminOServ";
                    oFormCreate.XmlData = XmlData;
                    this.oForm = this.oGlobales.SBO_Application.Forms.AddEx(oFormCreate);
                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;

                }
                catch (Exception ex)
                {
                    bContinuar = false;
                    KananSAPHerramientas.LogError("SBO_KF_AdministraOSView.cs->LoadForm()", ex.Message);
                }
            }
            return bContinuar;
        }

        public void ShowForm()
        {
            try
            {

                this.oForm.Visible = true;
                this.oForm.Select();
                this.InicializaItems();
            }
            catch (Exception ex)
            {

                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                this.oForm.Select();
                this.InicializaItems();
                KananSAPHerramientas.LogError("SBO_KF_AdministraOSView.cs->ShowForm()", ex.Message);

            }
        }

        void InicializaItems()
        {
            try
            {
                SBO_KF_ORDENSERVICIO_PD OrdenesPD = new SBO_KF_ORDENSERVICIO_PD();
                lstEstatus = new List<SBO_KF_EstatusOrden_INFO>();            
                OrdenesPD.lstEstatusOrdenes(ref this.oCompany, ref lstEstatus);

                SBO_KF_OrdenServicioAdmin_INFO otemp = new SBO_KF_OrdenServicioAdmin_INFO();
                otemp.idEstatus = 1;
                this.fillComboEstatus();
                this.fillComboEmpleados();
                this.CleanFilters();
                this.CargaOrdenesRegistradas(otemp);
            }
            catch (Exception ex)
            {
                oHerramientas.ColocarMensaje(string.Format("No se ha encontrado información. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                KananSAPHerramientas.LogError("SBO_KF_AdministraOSView.cs->ShowForm()", ex.Message);
            }

        }

        private void CleanFilters()
        {
            oItem = this.oForm.Items.Item("cmbestatus");
            oCombo = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oCombo.Select("0", SAPbouiCOM.BoSearchKey.psk_ByValue);

            oItem = this.oForm.Items.Item("cmbresp");
            oCombo = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oCombo.Select("0", SAPbouiCOM.BoSearchKey.psk_ByValue);

            oItem = this.oForm.Items.Item("txtfinicio");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            /*Se controla el formato de las fechas para evitar conflictos con el validador
             de datos que incrustado en el XML*/
            oEditText.String = DateTime.Today.AddDays(-5).ToString("dd/MM/yyyy");

            oItem = this.oForm.Items.Item("txtffinal");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            /*Se controla el formato de las fechas para evitar conflictos con el validador
             de datos que incrustado en el XML*/
            oEditText.String = DateTime.Today.ToString("dd/MM/yyyy");

            oItem = this.oForm.Items.Item("txtfolio");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtfolord");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;
        }
        private void fillComboEstatus()
        {
            try
            {
                
                oItem = this.oForm.Items.Item("cmbestatus");
                oCombo = oItem.Specific;
                KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo); 
                oCombo.ValidValues.Add("0", "Todos");
                foreach (SBO_KF_EstatusOrden_INFO estatus in lstEstatus)                
                    oCombo.ValidValues.Add(estatus.EstatusOrdenID.ToString(), estatus.Nombre);
                oCombo.Item.DisplayDesc = true;
                oCombo.Select("0", SAPbouiCOM.BoSearchKey.psk_ByValue);
            }
            catch 
            {
                
            }
        }

        private void fillComboEmpleados()
        {
            try
            {
                List<SBO_KF_OHEM_INFO> lstEmpleados =new List<SBO_KF_OHEM_INFO>();
                SBO_KF_OHEM_PD oempleado = new SBO_KF_OHEM_PD();
                oempleado.listaEmpleadosKF(ref this.oCompany, ref lstEmpleados);
                if (lstEmpleados.Count > 0)
                {
                    oItem = this.oForm.Items.Item("cmbresp");
                    oCombo = oItem.Specific;
                    KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Todos");
                    foreach (SBO_KF_OHEM_INFO oEmpleado in lstEmpleados)
                        oCombo.ValidValues.Add(oEmpleado.empID.ToString(), oEmpleado.firstName);
                    oCombo.Item.DisplayDesc = true;
                    oCombo.Select("0", SAPbouiCOM.BoSearchKey.psk_ByValue);
                }
            }
            catch
            {

            }
        }


        void CargaOrdenesRegistradas(SBO_KF_OrdenServicioAdmin_INFO Orden)
        {
            string response = string.Empty;
            SBO_KF_ORDENSERVICIO_PD OrdenesPD = new SBO_KF_ORDENSERVICIO_PD();            
            lstOrdenes = new List<SBO_KF_OrdenServicioAdmin_INFO>();
            try
            {

                response = OrdenesPD.listaOrdenes(ref this.oCompany, ref lstOrdenes, Orden, false);
                if (response == "OK" && lstOrdenes.Count > 0)
                {
                    DataTable tab = oForm.DataSources.DataTables.Item("DT_Ordenes");
                    oGrid = this.oForm.Items.Item("grdorden").Specific;
                    tab.Rows.Clear();

                    int index = 0;
                    this.oForm.Freeze(true);
                    foreach (SBO_KF_OrdenServicioAdmin_INFO orden in lstOrdenes)
                    {
                        tab.Rows.Add();
                        tab.SetValue("colid", index, Convert.ToInt32(orden.Code));
                        var oEstatus = lstEstatus.FirstOrDefault(x => x.EstatusOrdenID == orden.idEstatus);
                        if (oEstatus != null)
                            tab.SetValue("colestatus", index, oEstatus.Nombre);
                        tab.SetValue("colfolio", index, orden.Name.ToString());
                        tab.SetValue("colifolio", index, orden.folioOS);
                        tab.SetValue("colfecha", index, Convert.ToDateTime(orden.fecha));
                        tab.SetValue("colcapt", index, Convert.ToDateTime(orden.fechaCaptura));
                        tab.SetValue("colcosto", index, orden.Costo.ToString());
                        tab.SetValue("coldesc", index, orden.descripcion);
                        index++;
                    }
                    this.oForm.Freeze(false);

                    oGrid.Columns.Item("colid").Visible = false;
                    oGrid.Columns.Item("colestatus").Editable = false;
                    oGrid.Columns.Item("colfolio").Editable = false;
                    oGrid.Columns.Item("colifolio").Editable = false;
                    oGrid.Columns.Item("colfecha").Editable = false;
                    oGrid.Columns.Item("colcapt").Editable = false;
                    oGrid.Columns.Item("colcosto").Editable = false;
                    oGrid.Columns.Item("coldesc").Editable = false;

                    var title = oGrid.Columns.Item("colestatus").TitleObject;
                    title.Caption = "Estatus";
                    title = oGrid.Columns.Item("colfolio").TitleObject;
                    title.Caption = "Folio Orden";
                    title = oGrid.Columns.Item("colifolio").TitleObject;
                    title.Caption = "Folio";
                    title = oGrid.Columns.Item("colfecha").TitleObject;
                    title.Caption = "Fecha Orden";
                    title = oGrid.Columns.Item("colcapt").TitleObject;
                    title.Caption = "Fecha Captura";
                    title = oGrid.Columns.Item("colcosto").TitleObject;
                    title.Caption = "Costo";
                    title = oGrid.Columns.Item("coldesc").TitleObject;
                    title.Caption = "Descripción";
                    oGrid.SelectionMode = BoMatrixSelect.ms_Single;
                }
                else
                {
                    oHerramientas.ColocarMensaje(string.Format("No se han encontrado ordens de servicio registradas. ERROR {0}", response), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                    try
                    {
                        DataTable tab = oForm.DataSources.DataTables.Item("DT_Ordenes");
                        oGrid = this.oForm.Items.Item("grdorden").Specific;
                        tab.Rows.Clear();
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                oHerramientas.ColocarMensaje(string.Format("No se ha podido recuperar las ordenes de servicio. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
        }

        void BuscaPorFiltros()
        {
            try
            {
                int idEstatus = -1;
                SBO_KF_OrdenServicioAdmin_INFO otemp = new SBO_KF_OrdenServicioAdmin_INFO();
                oItem = this.oForm.Items.Item("cmbestatus");
                oCombo = (SAPbouiCOM.ComboBox)(oItem.Specific);
                if (!string.IsNullOrEmpty(oCombo.Selected.Value) && oCombo.Selected.Value != "0" &&
                    int.TryParse(oCombo.Selected.Value, out idEstatus))
                    otemp.idEstatus = idEstatus;

                oItem = this.oForm.Items.Item("cmbresp");
                oCombo = (SAPbouiCOM.ComboBox)(oItem.Specific);
                if (!string.IsNullOrEmpty(oCombo.Selected.Value) && oCombo.Selected.Value != "0" &&
                    int.TryParse(oCombo.Selected.Value, out idEstatus))
                    otemp.iResponsable = idEstatus;

                oItem = this.oForm.Items.Item("txtfinicio");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (!string.IsNullOrEmpty(oEditText.String))
                    otemp.fechainicio = Convert.ToDateTime(oEditText.String).ToString("yyyyMMdd");

                oItem = this.oForm.Items.Item("txtffinal");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (!string.IsNullOrEmpty(oEditText.String))
                    otemp.fechafin = Convert.ToDateTime(oEditText.String).ToString("yyyyMMdd");

                oItem = this.oForm.Items.Item("txtfolio");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (!string.IsNullOrEmpty(oEditText.String))
                    otemp.folioOS = oEditText.String;

                this.CargaOrdenesRegistradas(otemp);
            }
            catch (Exception ex)
            {
                oHerramientas.ColocarMensaje(string.Format("No se ha podido recuperar las ordenes de servicio. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
        }

        void SetOSRowIndex(int rowIndex)
        {
            try
            {
                this.OSRowIndex = rowIndex;
                
            }
            catch {  }
        }
        public void GetOrdebByGrid()
        {
            try
            {                
                if (this.OSRowIndex != null && this.OSRowIndex > -1)
                {
                    oGrid = this.oForm.Items.Item("grdorden").Specific;
                    int id = Convert.ToInt32(oGrid.DataTable.Columns.Item("colid").Cells.Item((int)this.OSRowIndex).Value);
                    if (lstOrdenes != null && lstOrdenes.Count > 0)
                    {
                        var ordenOS = lstOrdenes.FirstOrDefault(x => x.Code.Equals(id.ToString(), StringComparison.InvariantCultureIgnoreCase));
                        if (ordenOS != null)
                            oView = new SBO_KF_SolicitaOrdenesView(this.oGlobales, Oconfig, ref oCompany, "amn", "", OrdenID: Convert.ToInt32(ordenOS.Code));
                            /*this.Entity.OrdenServicio = this.listordenes.FirstOrDefault(svc => svc.OrdenServicioID == id);
                            this.Entity.oUDT.GetByKey(this.Entity.OrdenServicio.OrdenServicioID.ToString());
                            this.Entity.UDTToOrdenServicio();*/
                        
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void EventoControl(ItemEvent pVal, out bool BubbleEvent)
        {
            #region Comentarios
            /* 
             */
            #endregion Comentarios

            BubbleEvent = true;
            try
            {
                if (!pVal.BeforeAction)
                {
                    if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "grdorden")
                        this.SetOSRowIndex(pVal.Row);

                    if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnfiltro")
                        this.BuscaPorFiltros();

                    if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnorden")
                        this.GetOrdebByGrid();
                }
                else
                {

                }

                if (oView != null)
                    oView.SBO_Application_ItemEvent(pVal.FormUID, ref pVal, out BubbleEvent);
                BubbleEvent = false;
            }
            catch (Exception ex)
            {

                KananSAPHerramientas.LogError(string.Format("{0}->EventoControl()", CONST_CLASE), ex.Message);
            }
        }

    }
}
