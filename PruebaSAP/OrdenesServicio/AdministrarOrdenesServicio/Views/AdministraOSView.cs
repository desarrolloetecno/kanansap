﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kanan.Mantenimiento.BO2;
using Kanan.Operaciones.BO2;
using KananSAP.Mantenimiento.Data;
using SAPbouiCOM;
using Application = System.Windows.Forms.Application;

namespace KananSAP.AdministrarOrdenesServicio.Views
{
    public class AdministraOSView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company oCompany;

        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.StaticText oStaticText;
        private SAPbouiCOM.Grid oGrid;
        private SAPbouiCOM.ComboBox oCombo;
        private SAPbouiCOM.DBDataSource oDBDataSource;
        private SAPbouiCOM.Button oButton;
        private SAPbouiCOM.Conditions oConditions;
        private SAPbouiCOM.Condition oCondition;
        public OrdenServicioSAP Entity { get; set; }
        public DateTime? fechainicio { get; set; }
        public DateTime? fechafin { get; set; }
        private XMLPerformance XmlApplication;
        public List<OrdenServicio> listordenes;
        public List<EstatusOrden> listestatus;
        public int? OSRowIndex { get; set; }
        public int? IDOSActual { get; set; }
        public int? TabServGridID;

        #endregion

    
        #region Constructor
        public AdministraOSView(SAPbouiCOM.Application Application, SAPbobsCOM.Company company)
        {
            this.SBO_Application = Application;
            this.oCompany = company;
            this.Entity = new OrdenServicioSAP(ref company);
            this.XmlApplication = new XMLPerformance(SBO_Application);
            //this.LoadForm();
            this.oItem = null;
            this.oGrid = null;
            this.oStaticText = null;
            this.oEditText = null;
            this.oConditions = null;
            this.oCondition = null;
            this.oCombo = null;
            this.fechainicio = null;
            this.fechafin = null;
            this.listordenes = new List<OrdenServicio>();
            this.listestatus = new List<EstatusOrden>();
        }
        #endregion

        #region Metodos
        #region Metodos Form
        public void LoadForm()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("AdminOServ");
            }
            catch
            {
                string sPath = Application.StartupPath;
                this.XmlApplication.LoadFromXML(sPath + "\\XML", "AdminOrdenesServicio.xml");

                this.oForm = SBO_Application.Forms.Item("AdminOServ");

                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;

                PrepareForm();
                //oItem = oForm.Items.Item("txtvname");
                //oStaticText = (StaticText)(oItem.Specific);
                //oStaticText.Caption = this.name;

                //oItem = oForm.Items.Item("lblvid");
                //oStaticText = (StaticText)(oItem.Specific);
                //oStaticText.Caption = this.vehiculoid.ToString();
                //oItem.Visible = false;
            }
        }

        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
            }
            catch (Exception)
            {
                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                this.oForm.Select();
            }
        }

        #region Prepare Form

        private void PrepareForm()
        {
            FillComboEmpleados();
            FillComboEstatus();
            CleanFilters();
            AplyFilter();
        }

        public void AplyFilter()
        {
            SetConditions();
            GetListOrdeneServicio();
            FillOrdenesGridFromList();
        }

        private void FillComboEmpleados()
        {
            try
            {
                string query = string.Empty;
                query = @" select e.""Name"", e.U_KFEmpleadoID from ""@VSKF_RELEMPLEADO"" e ";
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    oItem = this.oForm.Items.Item("cmbresp");
                    oCombo = oItem.Specific;
                    Helper.KananSAPHelpers.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Todos");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("U_KFEmpleadoID").Value);
                        string description = rs.Fields.Item("Name").Value;
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }
                    oCombo.Item.DisplayDesc = true;
                    oCombo.Select("0", SAPbouiCOM.BoSearchKey.psk_ByValue);
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        private void FillComboEstatus()
        {
            try
            {
                GetListEstatus();
                oItem = this.oForm.Items.Item("cmbestatus");
                oCombo = oItem.Specific;
                Helper.KananSAPHelpers.CleanComboBox(oCombo); //this.CleanComboBox(oCombo);
                oCombo.ValidValues.Add("0", "Todos");
                foreach (EstatusOrden estatus in listestatus)
                {
                    string value = estatus.EstatusOrdenID.ToString();
                    string description = estatus.Nombre;
                    oCombo.ValidValues.Add(value, description);
                }
                oCombo.Item.DisplayDesc = true;
                oCombo.Select("0", SAPbouiCOM.BoSearchKey.psk_ByValue);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CleanFilters()
        {
            oItem = this.oForm.Items.Item("cmbestatus");
            oCombo = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oCombo.Select("0", SAPbouiCOM.BoSearchKey.psk_ByValue);

            oItem = this.oForm.Items.Item("cmbresp");
            oCombo = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oCombo.Select("0", SAPbouiCOM.BoSearchKey.psk_ByValue);

            oItem = this.oForm.Items.Item("txtfinicio");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            /*Se controla el formato de las fechas para evitar conflictos con el validador
             de datos que incrustado en el XML*/
            oEditText.String = DateTime.Today.AddDays(-5).ToString("dd/MM/yyyy");

            oItem = this.oForm.Items.Item("txtffinal");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            /*Se controla el formato de las fechas para evitar conflictos con el validador
             de datos que incrustado en el XML*/
            oEditText.String = DateTime.Today.ToString("dd/MM/yyyy");

            oItem = this.oForm.Items.Item("txtfolio");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtfolord");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;
        }
        #endregion

        #endregion
        #region Metodos Grid
        public void SetOSRowIndex(int rowIndex)
        {
            try
            {
                this.OSRowIndex = rowIndex;
                //this.TabServGridID = rowIndex;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void FillOrdenesGridFromList()
        {
            try
            {
                DataTable tab = oForm.DataSources.DataTables.Item("DT_Ordenes");
                oGrid = this.oForm.Items.Item("grdorden").Specific;
                tab.Rows.Clear();

                int index = 0;
                this.oForm.Freeze(true);
                this.listordenes = listordenes.OrderBy(x => x.OrdenServicioID).ToList();
                foreach (OrdenServicio orden in listordenes)
                {
                    tab.Rows.Add();
                    tab.SetValue("colid", index, orden.OrdenServicioID ?? 0);
                    var firstOrDefault = listestatus.FirstOrDefault(x => x.EstatusOrdenID == orden.EstatusOrden.EstatusOrdenID);
                    if (firstOrDefault != null)
                        tab.SetValue("colestatus", index, firstOrDefault.Nombre);
                    tab.SetValue("colfolio", index, orden.FolioOrden.Folio);
                    tab.SetValue("colifolio", index, orden.Folio);
                    tab.SetValue("colfecha", index, orden.Fecha.Value.Date);
                    tab.SetValue("colcapt", index, orden.FechaCaptura.Value.Date);
                    tab.SetValue("colcosto", index, orden.Costo.ToString());
                    tab.SetValue("coldesc", index, orden.Descripcion);
                    index++;
                }
                this.oForm.Freeze(false);
                //oGrid.DataTable = tab;
                //oGrid.Columns.Item("colid").Width = 50;
                //oGrid.Columns.Item("colnumero").Width = 150;
                //oGrid.Columns.Item("colestatus").Width = 100;
                //oGrid.Columns.Item("coltipo").Width = 50;
                //oGrid.Columns.Item("colvigencia").Width = 100;
                //oGrid.Columns.Item("colaviso").Width = 100;

                oGrid.Columns.Item("colid").Visible = false;
                oGrid.Columns.Item("colestatus").Editable = false;
                oGrid.Columns.Item("colfolio").Editable = false;
                oGrid.Columns.Item("colifolio").Editable = false;
                oGrid.Columns.Item("colfecha").Editable = false;
                oGrid.Columns.Item("colcapt").Editable = false;
                oGrid.Columns.Item("colcosto").Editable = false;
                oGrid.Columns.Item("coldesc").Editable = false;

                var title = oGrid.Columns.Item("colestatus").TitleObject;
                title.Caption = "Estatus";
                title = oGrid.Columns.Item("colfolio").TitleObject;
                title.Caption = "Folio Orden";
                title = oGrid.Columns.Item("colifolio").TitleObject;
                title.Caption = "Folio";
                title = oGrid.Columns.Item("colfecha").TitleObject;
                title.Caption = "Fecha Orden";
                title = oGrid.Columns.Item("colcapt").TitleObject;
                title.Caption = "Fecha Captura";
                title = oGrid.Columns.Item("colcosto").TitleObject;
                title.Caption = "Costo";
                title = oGrid.Columns.Item("coldesc").TitleObject;
                title.Caption = "Descripción";

                oGrid.SelectionMode = BoMatrixSelect.ms_Single;
            }
            catch (Exception ex)
            {
                this.oForm.Freeze(false);
                //throw new Exception(ex.Message);
            }
        }

        public void GetListOrdeneServicio()
        {
            Entity.Sincronizado = null;
            Entity.UUID = null;
            OrdenServicioFilter orden = new OrdenServicioFilter();
            if (this.fechafin != null)
            {
                orden.FechaFinFilter = this.fechafin;
                orden.sFechaFin = this.Entity.OrdenServicio.sFechaFin;
            }
            if (this.fechainicio != null)
            {
                orden.FechaCaptura = this.fechainicio;
                orden.sFechaInicio = this.Entity.OrdenServicio.sFechaInicio;
            }
            if (this.Entity.OrdenServicio.EstatusOrden != null && this.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID != null)
            {
                orden.EstatusOrden = new EstatusOrden();
                orden.EstatusOrden.EstatusOrdenID = this.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID;
            }
            if (this.Entity.OrdenServicio.ResponsableOrdenServicio != null && this.Entity.OrdenServicio.ResponsableOrdenServicio.EmpleadoID != null)
            {
                orden.ResponsableOrdenServicio = new Empleado();
                orden.ResponsableOrdenServicio.EmpleadoID = this.Entity.OrdenServicio.ResponsableOrdenServicio.EmpleadoID;
            }
            if (!string.IsNullOrEmpty(this.Entity.OrdenServicio.Folio))
            {
                orden.Folio = this.Entity.OrdenServicio.Folio.Trim();
            }
            if (this.Entity.OrdenServicio.FolioOrden != null && this.Entity.OrdenServicio.FolioOrden.Folio != null)
            {
                orden.FolioOrden.Folio = Entity.OrdenServicio.FolioOrden.Folio;
            }

            var rs = Entity.Consultar(orden);
            listordenes = Entity.HashOrdenServicio(rs) ? Entity.RecordSetToListOrdenesServicio(rs) : new List<OrdenServicio>();
        }

        public void GetOrdenFromGrid()
        {
            try
            {
                //Agregado
                /*if (this.TabServGridID != null)
                    this.OSRowIndex = this.TabServGridID;*/
                if (this.OSRowIndex != null && this.OSRowIndex > -1)
                {
                    oGrid = this.oForm.Items.Item("grdorden").Specific;
                    int id = Convert.ToInt32(oGrid.DataTable.Columns.Item("colid").Cells.Item((int)this.OSRowIndex).Value);
                    //if (listobservacion != null && listobservacion.Count > 0 && obsid != 0)
                    if (listordenes != null && listordenes.Count > 0)
                    {
                        bool exist = listordenes.Exists(svc => svc.OrdenServicioID == id);
                        if (exist)
                        {
                            this.Entity.OrdenServicio = this.listordenes.FirstOrDefault(svc => svc.OrdenServicioID == id);
                            this.Entity.oUDT.GetByKey(this.Entity.OrdenServicio.OrdenServicioID.ToString());
                            this.Entity.UDTToOrdenServicio();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        public void SetConditions()
        {
            try
            {
                Entity.OrdenServicio = new OrdenServicio();
                int id;
                DateTime date;
                oItem = this.oForm.Items.Item("cmbestatus");
                oCombo = (SAPbouiCOM.ComboBox)(oItem.Specific);
                if (!string.IsNullOrEmpty(oCombo.Selected.Value) && oCombo.Selected.Value != "0" && int.TryParse(oCombo.Selected.Value, out id))
                    Entity.OrdenServicio.EstatusOrden.EstatusOrdenID = id;

                oItem = this.oForm.Items.Item("cmbresp");
                oCombo = (SAPbouiCOM.ComboBox)(oItem.Specific);
                if (!string.IsNullOrEmpty(oCombo.Selected.Value) && oCombo.Selected.Value != "0" && int.TryParse(oCombo.Selected.Value, out id))
                    Entity.OrdenServicio.ResponsableOrdenServicio.EmpleadoID = id;

                oItem = this.oForm.Items.Item("txtfinicio");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (!string.IsNullOrEmpty(oEditText.String) && DateTime.TryParse(oEditText.String, out date))
                {
                    fechainicio = date;
                    Entity.OrdenServicio.FechaCaptura = fechainicio;
                    Entity.OrdenServicio.sFechaInicio = oEditText.Value;
                }

                oItem = this.oForm.Items.Item("txtffinal");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (!string.IsNullOrEmpty(oEditText.String) && DateTime.TryParse(oEditText.String, out date))
                {
                    Entity.OrdenServicio.sFechaFin = oEditText.Value;
                    //Se le agragan días.
                    date = date.AddDays(1);
                    //Se ñe agregan días.
                    fechafin = date;
                }

                oItem = this.oForm.Items.Item("txtfolio");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                Entity.OrdenServicio.Folio = oEditText.String;

                oItem = this.oForm.Items.Item("txtfolord");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (!string.IsNullOrEmpty(oEditText.String) && int.TryParse(oEditText.String, out id))
                    Entity.OrdenServicio.FolioOrden.Folio = id;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetListEstatus()
        {
            try
            {
                string query = @"select * from ""@VSKF_ESTATUSORDEN"" where U_Activo = 1";
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    listestatus = new List<EstatusOrden>();
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        EstatusOrden estatus = new EstatusOrden();
                        estatus.EstatusOrdenID = Convert.ToInt32(rs.Fields.Item("Code").Value);
                        estatus.Nombre = rs.Fields.Item("Name").Value;
                        listestatus.Add((EstatusOrden)estatus.Clone());
                        rs.MoveNext();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion
    }
}
