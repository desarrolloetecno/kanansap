﻿using KananSAP.AdministrarOrdenesServicio.Views;
using KananSAP.Mantenimiento.Data;
using Kanan.Mantenimiento.BO2;
using Kanan.Vehiculos.BO2;
using KananSAP.GestionAlertas.View;
using KananWS.Interface;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KananSAP.Alertas.Data;
using KananSAP.Vehiculos.Data;
using KananSAP.Helpers;
using KananSAP.Configuracion.Configuraciones.Objects;
using KananSAP.Helper;
using SAPbobsCOM;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.IO;

using KananSAP.GestionDatos.MigradorDatos.Librerias;
using SAPinterface.Data.INFO.Tablas;
using SAPinterface.Data.PD;
using SAPinterface.Data.AD;

using Application = System.Windows.Forms.Application;
using KananSAP;
using SAPinterface.Data.INFO.Objetos;
using SAPinterface.Data.WS;
using Kanan.Costos.BO2;
using Kanan.Operaciones.BO2;
using System.Threading;
using Newtonsoft.Json;

namespace KananFleet.OrdenesServicio.SolicitarOrdenServicio.Views
{
    public class SBO_KF_SolicitaOrdenesView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private XMLPerformance XmlApplication;
        private GestionGlobales oGlobales = null;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.EditText oEditText2;
        private SAPbouiCOM.Grid oGrid;
        private SAPbouiCOM.ComboBox oCombo;
        private SAPbouiCOM.ComboBox oComboR;
        private SAPbouiCOM.CheckBox oCheck;
        private SAPbouiCOM.Button oButton;
        private SAPbouiCOM.Folder oFolder;
        private SAPbouiCOM.StaticText oLabel;
        private string iConsecutivo = string.Empty;
        private SAPbobsCOM.Company oCompany;
        private int? OSRowIndex = -1;
        private List<SBO_KF_EstatusOrden_INFO> lstEstatus = null;
        private SBO_KF_Configuracion_INFO oSetting = null;
        private List<SBO_KF_OrdenServicioAdmin_INFO> lstOrdenes = null;
        private List<Kanan.Mantenimiento.BO2.SBO_KF_OHEM_INFO> lstmecanicos = null;
        private List<DetalleRefaccion> lstrefacciones = null;
        private List<SBOKF_CL_CostosAdicional_INFO> lstcostoadicional = null;
        private List<SBO_KF_Herramientas_INFO> lstherramientas = null;
        private List<DetalleOrden> lstservicios = null;
        private string FORMUUID = string.Empty;
        private Int32 StatsOrdenID = -1;
        private Int32 articuloID = -1;
        private Double costoTotal { get; set; }
        const string CONST_CLASE = "SBO_KF_SolicitaOrdenesView.cs";
        private KananSAP.Helper.KananSAPHerramientas oTools;
        private int OrdenServicioID = -1;
        private string OrdenServicoWS = string.Empty;
        private int AlertaID = -1;
        private int IncidenciaID = -1;
        private string MOOD = string.Empty;
        private Configurations oConnfig = null;
        private bool bLocked = false; // variable para determinar si la UI se puede editar
        private bool bEditaServicio = false, bEditaRefaccion = false, bEditaCosto = false, bEditaMecanicos = false, bEditaHerramientas = false;
        #region propiedades UI form
        private string[] cajas = { "txtFechaOS", "txtEnPro", "txthEntr", "txtFolioOS", "txtSalPro", "txtHsal", "txtEnReal", "txtHReale", "txtDetOSv", "txtSalReal", "txtHSalr", "txtDescOs", "txtClisap", "txtInspec", "txtCostoOS", "txtRefSAP", "txtCostoRf", "txtQntyRf", "txtAcost", "txtADesc1", "txtADesc2", "txtADesc3", "txttoolid", "txttoolq", "txttool", "txtSueldoT", "txtmeca" };
        private string[] combos = { "cmbSyscr", "cmbCompo", "cmbrutina", "cmbsuc", "comboProOS", "comboMPOSv", "cmbResp", "cmbAprob", "comboVehOS", "comoServOS", "cmbAlmacn", "cmbATipo", "cmbmeca", "cmbestatus", "cmblevel", "cmbstasi" };
        private string[] checks = { "CBTI", "chkoinv" };
        private string[] grids = { "dgvCstos", "gdRFa", "dtServSAG", "dgvMecan", "dgvTools" };
        private string[] labels = { "lblCostV" };
        private string[] cajahora = { "txthEntr", "txtHsal", "txtHReale", "txtHSalr" };
        #endregion propiedades UI form

        #endregion

        public SBO_KF_SolicitaOrdenesView(GestionGlobales oGlobales, Configurations oConfig, ref SAPbobsCOM.Company Company, string FUUID, string MOOD, int OrdenID = -1, int AlertaID = -1, int IncidenciaID =-1)
        {
            this.MOOD = MOOD;
            this.FORMUUID = string.Format("AdOS_{0}", FUUID);
            this.OrdenServicioID = OrdenID;
            this.AlertaID = AlertaID;
            this.IncidenciaID = IncidenciaID;
            this.oConnfig = oConfig;
            this.oGlobales = oGlobales;
            this.SBO_Application = this.oGlobales.SBO_Application;
            this.XmlApplication = new XMLPerformance(SBO_Application);
            this.oCompany = Company;
            Boolean bContinuar = LoadForm();
            this.ShowForm();
            this.oTools = new KananSAPHerramientas(this.SBO_Application);
            this.InicializaItems();

        }
        

        private Boolean LoadForm()
        {            
            FormCreationParams oFormCreate = null;
            Boolean bContinuar = false;
            String XmlData = "";
            try
            {
                this.oForm = SBO_Application.Forms.Item(FORMUUID);
                if (oForm.UniqueID != FORMUUID)
                    throw new Exception("");
                bContinuar = true;
            }
            catch (Exception exp)
            {
                KananSAPHerramientas.LogError("SBO_KF_SolicitaOrdenesView.cs->LoadForm()", exp.Message);
                bContinuar = false;
                try
                {
                    string sPath = Application.StartupPath;
                    XmlData = this.XmlApplication.LoadFromXML(sPath + "\\XML", "", "OrdenServicioSinAlerta.xml");
                    bContinuar = true;
                    oFormCreate = (FormCreationParams)this.oGlobales.SBO_Application.CreateObject(BoCreatableObjectType.cot_FormCreationParams);
                    oFormCreate.UniqueID = FORMUUID;
                    oFormCreate.FormType = "frmAddOS";
                    oFormCreate.XmlData = XmlData;
                    this.oForm = this.oGlobales.SBO_Application.Forms.AddEx(oFormCreate);
                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                    oForm.Title = this.OrdenServicioID > 0 ? "Actualizar Orden de Servicio" : "Registrar Orden de Servicio";
                }
                catch (Exception ex)
                {
                    bContinuar = false;
                    KananSAPHerramientas.LogError("SBO_KF_SolicitaOrdenesView.cs->LoadForm()", ex.Message);
                }
            }
            return bContinuar;
        }

        public void ShowForm()
        {
            try
            {

                this.oForm.Visible = true;
                this.oForm.Select();
            }
            catch (Exception ex)
            {

                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                this.oForm.Select();                
                KananSAPHerramientas.LogError("SBO_KF_SolicitaOrdenesView.cs->ShowForm()", ex.Message);

            }
        }

        void InicializaItems()
        {
            string sresponse = string.Empty;
            //ProgressBar oProgress = null;
            try
            {
                bLocked = true;
                //oProgress = this.SBO_Application.StatusBar.CreateProgressBar("Cargando información", 1, true);
                #region cargar información en modo nueva solicitud & edición
                oFolder = this.oForm.Items.Item("tabDataSer").Specific;
                oFolder.Select();
                SBO_KF_Configuracion_PD oConfiguracion = new SBO_KF_Configuracion_PD();
                oSetting = new SBO_KF_Configuracion_INFO();
                //oProgress.Text = "Recuperando información de AddON";
                sresponse = oConfiguracion.recuperaConfiguracion(ref oCompany, ref oSetting);
                if (sresponse == "OK")
                {
                    //oProgress.Text = "Preparando componentes para OS";
                    SBO_KF_ORDENSERVICIO_PD oRequest = new SBO_KF_ORDENSERVICIO_PD();
                    this.GestionInventario();
                    FillComboTipoPago();
                    //oTools.ColocarMensaje("FillComboTipoPago", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    FillComboServicios();
                    //oTools.ColocarMensaje("FillComboServicios", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    FillComboVehiculos();
                    //oTools.ColocarMensaje("FillComboVehiculos", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    FillComboSucursales();
                    //oTools.ColocarMensaje("FillComboSucursales", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    FillComboProveedores();
                    //oTools.ColocarMensaje("FillComboProveedores", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    FillComboEmpleados();
                    //oTools.ColocarMensaje("FillComboEmpleados", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    FillComboEstatus();
                    //oTools.ColocarMensaje("FillComboEstatus", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    FillComboSistemas();
                    //oTools.ColocarMensaje("FillComboSistemas", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    Thread.Sleep(10);
                    FillComboMecanicos();
                    //oTools.ColocarMensaje("FillComboMecanicos", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    ChooseArticulo();
                    //oTools.ColocarMensaje("ChooseArticulo", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    ChooseSocioNegocio();
                    //oTools.ColocarMensaje("ChooseSocioNegocio", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    ChooseRecurso();
                    //oTools.ColocarMensaje("ChooseRecurso", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    Thread.Sleep(50);
                    this.CombosStyle();
                    this.LimpiaCampos();
                    
                    Thread.Sleep(50);
                    if (this.OrdenServicioID > 0)
                    {
                        //recupera datos es modo actualizacion de Orden Servicio
                        //oProgress.Text = "Recuperando información de Orden de Servicio";
                        OrdenServicio OrdenServicio = new OrdenServicio();
                        sresponse = oRequest.OrdenServicioDetalle(ref this.oCompany, this.OrdenServicioID, ref OrdenServicio);
                        if (sresponse == "OK")
                        {
                           
                            OrdenServicoWS = OrdenServicio.Name;
                            StatsOrdenID = Convert.ToInt32(OrdenServicio.EstatusOrden.EstatusOrdenID);
                            this.EntityToForm(OrdenServicio);
                            #region recupera lista de datos relacionados
                            //oProgress.Text = "Cargar servicios relacionados";
                            sresponse = oRequest.buscaServiciosOrden(ref this.oCompany, ref lstservicios, this.OrdenServicioID);
                            if (sresponse != "OK" && sresponse != "SIN RESULTADOS")
                                throw new Exception(string.Format("los servicios no se han podido cargar. {0}", sresponse));
                            this.FillGridServiciosFromList();
                            //oProgress.Text = "Cargar herramientas relacionadas";
                            sresponse = oRequest.buscaHerramientasOrden(ref this.oCompany, ref lstherramientas, this.OrdenServicioID);
                            if (sresponse != "OK" && sresponse != "SIN RESULTADOS")
                                throw new Exception(string.Format("las herramientas no se han podido cargar. {0}", sresponse));
                            this.FillGridHerramientas();
                            //oProgress.Text = "Cargar refacciones relacionadas";
                            sresponse = oRequest.buscaRefaccionesOrden(ref this.oCompany, ref lstrefacciones, this.OrdenServicioID);
                            if (sresponse != "OK" && sresponse != "SIN RESULTADOS")
                                throw new Exception(string.Format("las refacciones no se han podido cargar. {0}", sresponse));


                            /*CAMBIOS PARA INMSO*/
                            SBO_KF_VEHICULOS_PD controlVehiculo = new SBO_KF_VEHICULOS_PD();
                            List<SBO_KF_VEHICULOS_INFO> lstVehiculos = new List<SBO_KF_VEHICULOS_INFO>();
                            BloqueaComboVehiculo(false);
                            if (lstservicios.Count > 0)
                            {
                                string responsev = controlVehiculo.listaVehiculosSucursal(ref oCompany, ref lstVehiculos, Convert.ToInt32(lstservicios[0].MantenibleID));
                                 
                            }

                            /*CAMBIOS PARA INMSO*/
                            
                            
                            if (lstrefacciones.Count > 0)
                            {
                                List<SBO_KF_OITM_INFO> lstArt = new List<SBO_KF_OITM_INFO>();

                                List<SBO_KF_OITM_INFO> lstArtVH = new List<SBO_KF_OITM_INFO>();

                                string itemsCodes = string.Empty;
                                foreach (DetalleRefaccion orefacc in lstrefacciones)
                                    itemsCodes += string.Format("'{0}',", orefacc.Articulo);
                                new SBO_KF_OITM_AD().buscaArticulo(ref this.oCompany, ref lstArt, ItemCodeIN: itemsCodes.TrimEnd(','));
                                
                                string itemsCodesVH = string.Empty;
                                foreach (DetalleRefaccion orefacc in lstrefacciones)
                                {
                                    /*CAMBIOS PARA INMSO*/
                                    /*orefacc.DIM2 = "GOP0001";
                                    orefacc.DIM3 = "MTT0001";*/
                                    //if (lstVehiculos.Count > 0)
                                    //{
                                    //    itemsCodesVH += string.Format("'{0}-KF'", lstVehiculos[0].U_NOMBRE);
                                    //    new SBO_KF_OITM_AD().buscaArticulo(ref this.oCompany, ref lstArtVH, ItemCodeIN: itemsCodesVH);
                                    //    oTools.ColocarMensaje(string.Format("Se está buscando {0}", itemsCodesVH), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                    //    orefacc.DIM1 = lstArtVH[0].U_DIM1;
                                    //    orefacc.DIM2 = lstArtVH[0].U_DIM2;
                                    //    orefacc.DIM3 = lstArtVH[0].U_DIM3;
                                    //    orefacc.DIM4 = lstArtVH[0].U_DIM4;
                                    //}
                                    /*CAMBIOS PARA INMSO*/

                                    var OITM = lstArt.First(x => x.ItemCode.Equals(orefacc.Articulo, StringComparison.InvariantCultureIgnoreCase));
                                    if (OITM != null)
                                    {
                                        orefacc.ArticuloID = OITM.iIdArticulo;
                                             

                                        
                                    }
                                        
                                }

                            }
                            this.FillGridRefaccionesFromList();
                            //oProgress.Text = "Cargar costos de la OS";
                            sresponse = oRequest.buscaCostoAdicionalesOrden(ref this.oCompany, ref lstcostoadicional, this.OrdenServicioID);
                            if (sresponse != "OK" && sresponse != "SIN RESULTADOS")
                                throw new Exception(string.Format("los costos adicionales no se han podido cargar. {0}", sresponse));
                            this.FillGridGastosAdicionales();
                            //oProgress.Text = "Cargar mecanicos";
                            sresponse = oRequest.buscamecanicosOrden(ref this.oCompany, ref lstmecanicos, this.OrdenServicioID);
                            if (sresponse != "OK" && sresponse != "SIN RESULTADOS")
                                throw new Exception(string.Format("los mecanicos no se han podido cargar. {0}", sresponse));
                            this.FillGridMecanicos();
                            #endregion recupera lista de datos relacionados
                            #region Se oculta los controles de rutina para no permitir modificarla
                            OcultaControlesRutina(false);
                            #endregion
                            switch (Convert.ToInt32( OrdenServicio.EstatusOrden.EstatusOrdenID))
                            {
                                case 0:
                                case 1:
                                    this.PrepareOrdenSolicitada();
                                    break;
                                case 2:
                                    this.PrepareOrdenAprobada();
                                    break;
                                case 3:
                                    this.PrepareOrdenRechazada();
                                    break;
                                case 4:
                                    this.PrepareOrdenEliminada();
                                    break;
                                case 5:
                                    this.PrepareOrdenAtendida();
                                    break;
                                case 6:
                                    this.PrepareOrdenRealizada();
                                    break;
                            }
                        }
                        else
                        {
                            oTools.ColocarMensaje("No se ha encontrado la información de la Orden de servicio", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                            return;
                        }
                    }
                    else
                    {
                        //oProgress.Text = "Preparando datos de la orden";
                        KananSAP.Helper.KananSAPHelpers.DeshabilitaItem(this.oForm, "cmbestatus");
                        this.PrepareOrdenSolicitada();
                        if (MOOD == "alerta")
                            this.CargaDatosAlerta();
                        if (MOOD == "incidencia")
                            this.CargaIncidencia();
                    }

                    
                        this.OcultaTabsTallerInterno();
                }
                else throw new Exception("No se ha podido recuperar la configuración del AddON");
                #endregion cargar información en modo nueva solicitud & edición
                oFolder = this.oForm.Items.Item("tabDataSer").Specific;
                oFolder.Select();
                try
                {
                    oItem = this.oForm.Items.Item("txtRefSAP");
                    oItem.FromPane = 3;
                    oItem.ToPane = 3;
                }
                catch { }
                //oProgress.Stop();
                bLocked = false;
            }
            catch (Exception ex)
            {
                //if (oProgress != null)
                //    oProgress.Stop();
                bLocked = false;
                oTools.ColocarMensaje(string.Format("No se ha podido inicializar el formulario. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                KananSAPHerramientas.LogError("SBO_KF_SolicitaOrdenesView.cs->ShowForm()", ex.Message);
            }

        }

        #region eventos & delegados de SAP B1
        public void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            
            BubbleEvent = true;
            if (pVal.FormUID.Equals(this.FORMUUID, StringComparison.InvariantCultureIgnoreCase) && !bLocked)
            {
                //if (!IsLogged) return;
                string FileName = string.Empty;
                string group = string.Empty;
                try
                {
                    if (!pVal.BeforeAction)
                    {
                        if (pVal.FormTypeEx == "frmAddOS")
                        {
                            #region ChooseFromList
                            ///se agrega evento de seleccion de datos por lista al componente refacciones
                            if (pVal.EventType == BoEventTypes.et_CHOOSE_FROM_LIST)
                            {
                                SAPbouiCOM.IChooseFromListEvent oCFLEvento = null;
                                oCFLEvento = ((SAPbouiCOM.IChooseFromListEvent)(pVal));
                                string sCFL_ID = null;
                                sCFL_ID = oCFLEvento.ChooseFromListUID;
                                SAPbouiCOM.Form oForm = null;
                                oForm = SBO_Application.Forms.Item(FormUID);
                                SAPbouiCOM.ChooseFromList oCFL = null;
                                oCFL = oForm.ChooseFromLists.Item(sCFL_ID);
                                if (oCFLEvento.BeforeAction == false)
                                {
                                    SAPbouiCOM.DataTable oDataTable = null;
                                    oDataTable = oCFLEvento.SelectedObjects;
                                    string val = null;
                                    try
                                    {
                                        val = System.Convert.ToString(oDataTable.GetValue(0, 0));
                                        string sitem = "";
                                        switch (sCFL_ID)
                                        {
                                            case "CFSrc":
                                                sitem = "EdtCstoAr";
                                                break;
                                            case "CFArt":
                                                sitem = "EdtRSCto";
                                                break;
                                            case "CFLTOOL":
                                                sitem = "EdtODS";
                                                break;

                                            case "CFSoc":
                                                sitem = "EdtSoci";
                                                break;
                                            default:
                                                sitem = "EditDS";
                                                break;
                                        }
                                        oForm.DataSources.UserDataSources.Item(sitem).ValueEx = val;
                                        this.GetChooseList(sCFL_ID, val);
                                    }
                                    catch { }
                                }
                            }
                            #endregion ChooseFromList

                            #region asignar identificador de grid seleccionado
                            if ((pVal.ItemUID == "gdOS" || pVal.ItemUID == "gdRFa" || pVal.ItemUID == "GvCsto" || pVal.ItemUID == "gvTool" || pVal.ItemUID == "gvMecan") && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                                this.OSRowIndex = pVal.Row;
                            #endregion asignar identificador de grid seleccionado

                            #region eventos sobre combos
                            if (pVal.ItemUID == "comboProOS" && pVal.EventType == BoEventTypes.et_COMBO_SELECT)
                                this.AsignaImpuesto();

                            if (pVal.EventType == BoEventTypes.et_COMBO_SELECT && pVal.ItemUID == "comboMPOSv")
                                this.HabilitaDetallePago(this.isEfectivo());

                            if (pVal.ItemUID == "txtRefSAP" && pVal.EventType == BoEventTypes.et_CHOOSE_FROM_LIST)
                                //this.EstableceCostoRefaccion();

                            if (pVal.ItemUID == "cmbATipo")
                                this.DefineChooseCosto();

                            if (pVal.ItemUID == "cmbmeca")
                                this.cargaSalarioEmpleado();

                            if (pVal.ItemUID == "cmbSyscr")
                                this.FillComboComponenteSistema();

                            if (pVal.ItemUID == "cmbrutina")
                                this.FormLoadPlantillaMood();

                            //Si selecciono algún valor en el combo de vehículos se deberá seleccionar la sucursal del mismo en todas los campos donde se requira.
                            //Se modifica el comportamiento, ahora se deberá seleccionar primero la sucursal. 26/08/2020
                            if (pVal.ItemUID == "comboVehOS")
                                this.FormLoadSucursalesDeVehiculo();
                            
                            if (pVal.ItemUID == "cmbsuc")
                                this.FormLoadVehiculoDeSucursales();

                            if (pVal.ItemUID == "cmbPla")
                                this.PlantillaToForm();


                            #endregion eventos sobre combos

                            #region eventos sobre botones
                            if (pVal.ItemUID == "btnPDF" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                                this.ExportaPDF(out FileName);

                            if (pVal.ItemUID == "btnCanOS" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                                this.SBO_Application.Forms.ActiveForm.Close();

                            if (pVal.ItemUID == "btnAddCto" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                                this.AgregaCostoToCostoList();

                            if (pVal.ItemUID == "btnOSvDone" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                                this.AgregaServiciosToList();

                            if (pVal.ItemUID == "btnVchg" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                                this.BloqueaComboVehiculo(true);

                            if (pVal.ItemUID == "btnRefn" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                                this.AgregaRefaccionesToList();

                            

                            if (pVal.ItemUID == "btnaddtool" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                                this.AgregaHerramientasToList();

                            if (pVal.ItemUID == "btnaddmec" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                                this.AgregaMecanicosToList();

                            if (pVal.ItemUID == "btnSaveOS" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                            {
                                if (this.OrdenServicioID > 0)
                                    this.ActualizaOrdenServicio();
                                else
                                    this.GuardaOrdenServicio();
                            }
                            if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && (pVal.ItemUID == "btndelmeca" || pVal.ItemUID == "btndeltool" || pVal.ItemUID == "btnDelcto" || pVal.ItemUID == "btnDelOSv" || pVal.ItemUID == "btnDelRf"))
                                this.EliminaregistroGrid(pVal.ItemUID);

                            if (pVal.ItemUID == "btnEdtOSv" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                                this.LanzaServicioForm();

                            if (pVal.ItemUID == "CBTI" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                                this.OcultaTabsTallerInterno();

                            #endregion eventos sobre botones

                        }
                    }
                }
                catch (Exception ex)
                {
                    BubbleEvent = false;
                    KananSAP.Helper.KananSAPHerramientas.LogError("SBO_KF_SolicitaOrdenesView.cs->SBO_Application_ItemEvent()", ex.Message);
                    oTools.ColocarMensaje(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                }
            }
        }
        #endregion eventos & delegados de SAP B1

        #region Metodos
        void OcultaTabsTallerInterno()
        {
            //CBTI
            //this.oForm.Items.Item("CBTI").Specific;

            oItem = oForm.Items.Item("CBTI");
            oCheck = (CheckBox)(oItem.Specific);
            if (!oCheck.Checked)
            {
                oItem = oForm.Items.Item("tabtools");
                oItem.Enabled = false;

                oItem = oForm.Items.Item("tabmecani");
                oItem.Enabled = false;

                //oItem = oForm.Items.Item("tabDataRef");
                //oItem.Enabled = false;
            }//oTools.ColocarMensaje("Se presionó el check True ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            else
            {
                oItem = oForm.Items.Item("tabtools");
                oItem.Enabled = true;

                oItem = oForm.Items.Item("tabmecani");
                oItem.Enabled = true;

                //oItem = oForm.Items.Item("tabDataRef");
                //oItem.Enabled = true;
            }
            //oItem = oForm.Items.Item("tabDataSer");
            
            
                //oTools.ColocarMensaje("Se presionó el check False ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
        }

        void LimpiaCampos()
        {
            try
            {
                foreach (string caja in cajas)
                {
                    try
                    {
                        oEditText = this.oForm.Items.Item(caja).Specific;
                        oEditText.String = "";
                    }
                    catch { continue; }
                }

                foreach (string grid in grids)
                {
                    try
                    {
                        try
                        {
                            Matrix oRefacciones = (Matrix)this.oForm.Items.Item(grid).Specific;
                            oRefacciones.Clear();
                        }
                        catch { }
                        DataTable oGrid = oForm.DataSources.DataTables.Item(grid);
                        oGrid.Rows.Clear();
                    }
                    catch { continue; }
                }
                foreach (string check in checks)
                {
                    try
                    {
                        oItem = oForm.Items.Item(check);
                        oCheck = (CheckBox)(oItem.Specific);
                        oCheck.Checked = false;
                    }
                    catch { continue; }
                }

                foreach (string combo in combos)
                {
                    try
                    {
                        if (combo.Equals("cmbRefKF", StringComparison.InvariantCultureIgnoreCase) && oSetting.cKANAN == 'N')
                            continue;
                        if (combo.Equals("cmbestatus", StringComparison.InvariantCultureIgnoreCase))
                            continue;

                        oCombo = this.oForm.Items.Item(combo).Specific;
                        if (combo.Equals("cmbCompo", StringComparison.InvariantCultureIgnoreCase))
                            KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);
                        else oCombo.Select("0", BoSearchKey.psk_ByValue);
                    }
                    catch { continue; }
                }
                foreach (string label in labels )
                {
                    try
                    {
                        oLabel = this.oForm.Items.Item(label).Specific;
                        if (label.Equals("lblCostV", StringComparison.InvariantCultureIgnoreCase))
                            oLabel.Caption = "0.00";
                        else
                            oLabel.Caption = string.Empty;
                    }
                    catch { continue; }
                }

                foreach (string cajhora in cajahora)
                {
                    try
                    {
                        oItem = oForm.Items.Item(cajhora);
                        oEditText = (EditText)this.oItem.Specific;
                        oEditText.String =   "00:00";
                    }
                    catch { }
                }

                /***/
                //FECHA FECHA DE ENTRADA PROGRAMADA
                oEditText = this.oForm.Items.Item("txtEnPro").Specific;
                oEditText.String = DateTime.Now.ToShortDateString();
               

                oEditText = this.oForm.Items.Item("txthEntr").Specific;
                oEditText.String = DateTime.Now.ToString("HH:mm"); //DateTime.Now.ToShortTimeString().Substring(0, 5);
 

                //FECHA FECHA DE SALIDA PROGRAMADA
                oEditText = this.oForm.Items.Item("txtSalPro").Specific;
                oEditText.String = DateTime.Now.ToShortDateString();
  

                oEditText = this.oForm.Items.Item("txtHsal").Specific;
                oEditText.String = DateTime.Now.ToString("HH:mm"); //DateTime.Now.ToShortTimeString().Substring(0, 5);
 

                //FECHA FECHA DEL SERVICIO
                oEditText = this.oForm.Items.Item("txtFechaOS").Specific;
                oEditText.String = DateTime.Now.ToShortDateString();
 

                oEditText = this.oForm.Items.Item("txtHoraOS").Specific;
                oEditText.String = DateTime.Now.ToString("HH:mm");
 
                 
                //FECHA ENTRADA REAL
                oEditText = this.oForm.Items.Item("txtEnReal").Specific;
                oEditText.String = string.Empty;//DateTime.Now.ToShortDateString();
                if (oEditText.Item.Visible)
                    oEditText.Active = false;

                oEditText = this.oForm.Items.Item("txtHReale").Specific;
                oEditText.String = string.Empty; //DateTime.Now.ToString("HH:mm");
                if (oEditText.Item.Visible)
                    oEditText.Active = false;

                //FECHA SALIDA REAL
                oEditText = this.oForm.Items.Item("txtSalReal").Specific;
                oEditText.String = string.Empty;// DateTime.Now.ToShortDateString();
                if (oEditText.Item.Visible)
                    oEditText.Active = false;

                oEditText = this.oForm.Items.Item("txtHSalr").Specific;
                oEditText.String = string.Empty; //DateTime.Now.ToString("HH:mm");
                if (oEditText.Item.Visible)
                    oEditText.Active = false;
                /**/

                bEditaServicio = false;
                bEditaRefaccion = false;
                bEditaCosto = false;
                bEditaMecanicos = false;
                bEditaHerramientas = false;
                lstmecanicos = new List<Kanan.Mantenimiento.BO2.SBO_KF_OHEM_INFO>();
                lstrefacciones = new List<DetalleRefaccion>();
                lstcostoadicional = new List<SBOKF_CL_CostosAdicional_INFO>();
                lstherramientas = new List<SBO_KF_Herramientas_INFO>();
                lstservicios = new List<DetalleOrden>();
                costoTotal = 0;
                StatsOrdenID = -1;
                OrdenServicoWS = string.Empty;

                try
                {
                    oItem = this.oForm.Items.Item("comboVehOS");
                    oItem.Enabled = true;
                }
                catch { }

                try {
                    oCombo = oForm.Items.Item("cmbCompo").Specific;
                    KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);
                }
                catch { }

                try
                {
                    FormLoadPlantillaMood();
                }
                catch { }

                if (this.OrdenServicioID <= 0)
                {
                    //asigna nuevo folio consecutivo
                    oLabel = oForm.Items.Item("txtfolio").Specific;
                    SBO_KF_OrdenServicio_WS controlws = new SBO_KF_OrdenServicio_WS();
                    FolioOrden oFolio = new FolioOrden();
                    string response = controlws.FolioConsecutivoWebAPI(GestionGlobales.sURLWs, ref oFolio, this.oConnfig.UserFull.Dependencia.EmpresaID);
                    if (response == "OK")
                        oLabel.Caption = string.Format("{0}/{1}", oFolio.Folio, oFolio.AnioFolio);
                    else
                    {
                        oLabel.Caption = string.Format("{0}/{1}", "000001", DateTime.Now.ToString("yyyyMM"));
                        oTools.ColocarMensaje("Folio consecutivo no asignado", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                    }
                
                }
            }
            catch
            {

            }

        }
        void EntityToForm(OrdenServicio OrdenServicio)
        {
            List<SBO_KF_VEHICULOS_INFO> lstResultados = new List<SBO_KF_VEHICULOS_INFO>();
            SBO_KF_VEHICULOS_PD oVehiculopd = new SBO_KF_VEHICULOS_PD();
            SBO_KF_VEHICULOS_INFO otemp = new SBO_KF_VEHICULOS_INFO();
            string Almacen = "";
            int SucursalID =  0;
            


            oLabel = oForm.Items.Item("lblid").Specific;
            if (OrdenServicio.OrdenServicioID != null && OrdenServicio.OrdenServicioID > 0)
                oLabel.Caption = OrdenServicio.OrdenServicioID.ToString();

            oCombo = oForm.Items.Item("comboProOS").Specific;
            if (OrdenServicio.ProveedorServicio != null && OrdenServicio.ProveedorServicio.ProveedorID != null)
                oCombo.Select(OrdenServicio.ProveedorServicio.ProveedorID.ToString());


            if (OrdenServicio.OrdenServicioID != null)
            {
            oCombo = oForm.Items.Item("cmbsuc").Specific;
            if (OrdenServicio.SucursalOrden != null && OrdenServicio.SucursalOrden.SucursalID != null)
                oCombo.Select(OrdenServicio.SucursalOrden.SucursalID.ToString());

            SucursalID = (int)OrdenServicio.SucursalOrden.SucursalID;

            oVehiculopd.listaVehiculosSucursal(ref oCompany, ref lstResultados, -1, -1, -1, SucursalID, "", -1);

            if (lstResultados.Count > 0)
            {
                SucursalID = lstResultados[0].U_SUCURSALID;
                Almacen = lstResultados[0].U_ALMACEN;
            }

            oItem = this.oForm.Items.Item("cmbAlmacn");
            oCombo = (ComboBox)this.oItem.Specific;
            oCombo.Item.Enabled = false;
            if (string.IsNullOrEmpty(Almacen))
                oCombo.Select(0);
            else
                oCombo.Select(Almacen);
            }
            
             
            //DateTime utcTime;//Fecha de servicio
            oEditText = oForm.Items.Item("txtFechaOS").Specific;
            if (OrdenServicio.Fecha != null)
            {
                if (OrdenServicio.Fecha.Value.Year < 1900)
                    oEditText.String = DateTime.Now.ToString("dd/MM/yyyy");
                else
                    oEditText.String = OrdenServicio.Fecha.Value.ToString("dd/MM/yyyy");
            }

            //DateTime EnPro;//Fecha de entrada programada
            oEditText = oForm.Items.Item("txtEnPro").Specific;
            if (OrdenServicio.FechaRecepcion != null)
            {
                if (OrdenServicio.FechaRecepcion.Value.Year < 1900)
                    oEditText.String = DateTime.Now.ToString("dd/MM/yyyy");
                else
                    oEditText.String = OrdenServicio.FechaRecepcion.Value.ToString("dd/MM/yyyy");
            }

            //DateTime SalPro;//Fecha de salida programada
            oEditText = oForm.Items.Item("txtSalPro").Specific;
            if (OrdenServicio.FechaLiberacion != null)
            {
                if (OrdenServicio.FechaLiberacion.Value.Year < 1900)
                    oEditText.String = DateTime.Now.ToString("dd/MM/yyyy");
                else
                    oEditText.String = OrdenServicio.FechaLiberacion.Value.ToString("dd/MM/yyyy");

            }


            //DateTime EnReal;//Fecha de entrada real
            oEditText = oForm.Items.Item("txtEnReal").Specific;
            if (OrdenServicio.FechaRecepcionReal != null)
            {
                if (OrdenServicio.FechaRecepcionReal.Value.Year < 1900)
                    oEditText.String = DateTime.Now.ToString("dd/MM/yyyy");
                else
                    oEditText.String = OrdenServicio.FechaRecepcionReal.Value.ToString("dd/MM/yyyy");
            }

            //DateTime SalReal;//Fecha de salida real
            oEditText = oForm.Items.Item("txtSalReal").Specific;
            if (OrdenServicio.FechaLiberacionReal != null)
            {
                if (OrdenServicio.FechaLiberacionReal.Value.Year < 1900)
                    oEditText.String = DateTime.Now.ToString("dd/MM/yyyy");
                else
                    oEditText.String = OrdenServicio.FechaLiberacionReal.Value.ToString("dd/MM/yyyy");
            }

            //Folio
            oEditText = oForm.Items.Item("txtFolioOS").Specific;
            if (!string.IsNullOrEmpty(OrdenServicio.Folio))
                oEditText.String = OrdenServicio.Folio;
            //Descripcion
            oEditText = oForm.Items.Item("txtDescOs").Specific;
            if (!string.IsNullOrEmpty(OrdenServicio.Descripcion))
                oEditText.String = OrdenServicio.Descripcion;
            //Metodo de pago
            oCombo = oForm.Items.Item("comboMPOSv").Specific;
            if (OrdenServicio.TipoDePago != null && OrdenServicio.TipoDePago.TipoPagoID != null)
                oCombo.Select(OrdenServicio.TipoDePago.TipoPagoID.ToString());

            //Detalle de pago
            oEditText = oForm.Items.Item("txtDetOSv").Specific;
            if (OrdenServicio.TipoDePago != null && !string.IsNullOrEmpty(OrdenServicio.TipoDePago.DetallePago))
                oEditText.String = OrdenServicio.TipoDePago.DetallePago;
            //Costo
            oLabel = this.oForm.Items.Item("lblCostV").Specific;
            if (OrdenServicio.Costo != null)
                oLabel.Caption = OrdenServicio.Costo.ToString();

            //Impuesto
            //ESTE VALOR PROVOCA BUG
            oLabel = oForm.Items.Item("lblImpOSv").Specific;
            if (OrdenServicio.Impuesto != null)
                oLabel.Caption = string.Format("{0} %", OrdenServicio.Impuesto);
            else oLabel.Caption= "0.00 %";
            
                //oForm.Items.Item("lblImpOSv").Enabled = boolTextBoxImpuesto;

            

            //Responsable dela orden de servicio
            oCombo = oForm.Items.Item("cmbResp").Specific;
            if (OrdenServicio.ResponsableOrdenServicio != null && OrdenServicio.ResponsableOrdenServicio.EmpleadoID != null)
                oCombo.Select(OrdenServicio.ResponsableOrdenServicio.EmpleadoID.ToString());
            //Aprobador de la orden de servico
            oCombo = oForm.Items.Item("cmbAprob").Specific;
            if (OrdenServicio.AprobadorOrdenServicio != null && OrdenServicio.AprobadorOrdenServicio.EmpleadoID != null)
                oCombo.Select(OrdenServicio.AprobadorOrdenServicio.EmpleadoID.ToString());

            //oCombo = oForm.Items.Item("cmbestatus").Specific;
            //OrdenServicio.EstatusOrden.EstatusOrdenID = Convert.ToInt32(oCombo.Selected.Value);

            //OrdenServicio.alertas.empresa = this.Config.UserFull.Dependencia;
            //OrdenServicio.SucursalOrden = this.Config.UserFull.EmpleadoSucursal;
            //OrdenServicio.DetalleOrden = this.GetListDetalleOrdenFromListServicios();
            //EFolio.folio = OrdenServicio.FolioOrden;
            oLabel = oForm.Items.Item("txtfolio").Specific;
            {
                if (OrdenServicio.FolioOrden.Folio != null && OrdenServicio.FolioOrden.AnioFolio != null)
                {
                    oLabel.Caption = string.Format("{0}/{1}", OrdenServicio.FolioOrden.Folio,
                        OrdenServicio.FolioOrden.AnioFolio);
                    OrdenServicio.FolioOrden.Propietario = new Empresa();
                    OrdenServicio.FolioOrden.Propietario.EmpresaID = this.oConnfig.UserFull.Dependencia.EmpresaID;
                }
            }

            /*if (this.OrdenServicio.DetalleOrden.Any())
            {
                this.ListaServiciosAgregados = this.OrdenServicio.DetalleOrden;
                //this.FillGridServiciosFromList();
            }*/

            #region nuevos datos INMSO & hertz
            try
            {
                if (OrdenServicio.ComponenteID > 0 && OrdenServicio.SistemaID > 0)
                {
                    try
                    {
                        if (OrdenServicio.SistemaID != 0 && OrdenServicio.ComponenteID != 0)
                        {
                            oCombo = oForm.Items.Item("cmbSyscr").Specific;
                            oCombo.Select(OrdenServicio.SistemaID.ToString(), BoSearchKey.psk_ByValue);
                            FillComboComponenteSistema(); //llena los componente con los datos del sistema auto seleccionado
                            oCombo = oForm.Items.Item("cmbCompo").Specific;
                            oCombo.Select(OrdenServicio.ComponenteID.ToString(), BoSearchKey.psk_ByValue);
                        }
                    }
                    catch { }
                    oCombo = oForm.Items.Item("cmblevel").Specific;
                    oCombo.Select(OrdenServicio.PrioridadID.ToString(), BoSearchKey.psk_ByValue);
                }
            }
            catch { }

            if (!string.IsNullOrEmpty(OrdenServicio.inspeccionID))
            {
                oEditText = oForm.Items.Item("txtInspec").Specific;
                oEditText.String = OrdenServicio.inspeccionID;
            }

            if (!string.IsNullOrEmpty(OrdenServicio.socioCliente))
            {
                oEditText = oForm.Items.Item("txtClisap").Specific;
                oEditText.String = OrdenServicio.socioCliente;
            }

            if (!string.IsNullOrEmpty(OrdenServicio.HoraEnProgramada))
            {
                oEditText = oForm.Items.Item("txthEntr").Specific;
                oEditText.String = OrdenServicio.HoraEnProgramada;
            }

            if (!string.IsNullOrEmpty(OrdenServicio.HoraSalProgramada))
            {
                oEditText = oForm.Items.Item("txtHsal").Specific;
                oEditText.String = OrdenServicio.HoraSalProgramada;
            }

            if (!string.IsNullOrEmpty(OrdenServicio.HoraEnReal))
            {
                oEditText = oForm.Items.Item("txtHReale").Specific;
                oEditText.String = OrdenServicio.HoraEnReal;
            }

            if (!string.IsNullOrEmpty(OrdenServicio.HoraSalReal))
            {
                oEditText = oForm.Items.Item("txtHSalr").Specific;
                oEditText.String = OrdenServicio.HoraSalReal;
            }

            oItem = oForm.Items.Item("CBTI");
            oCheck = (CheckBox)(oItem.Specific);
            oCheck.Checked = OrdenServicio.TallerInterno;

            if (OrdenServicio.FacturableCliente != null)
            {
                oItem = oForm.Items.Item("chkoinv");
                oCheck = (CheckBox)(oItem.Specific);
                oCheck.Checked = OrdenServicio.FacturableCliente == 1;
            }

            #endregion nuevos datos impso & hertz

            //DateTime EnReal;//Fecha de entrada real
            oEditText = oForm.Items.Item("txtHoroOs").Specific;
            if (OrdenServicio.Horometro != null)
                oEditText.String = OrdenServicio.Horometro.ToString();

            oEditText = oForm.Items.Item("txtOdomOs").Specific;
            if (OrdenServicio.Odometro != null)
                oEditText.String = OrdenServicio.Odometro.ToString();



        }
        string FormToEntity(ref OrdenServicio OrdenServicio)
        {
            string response = string.Empty;
            try
            {
               
                oItem = this.oForm.Items.Item("cmbrutina");
                oCombo = (ComboBox)this.oItem.Specific;
                string sTypeRutina = oCombo.Selected.Value;
                if (!string.IsNullOrEmpty(sTypeRutina))
                {
                    if (sTypeRutina.Equals("1", StringComparison.InvariantCultureIgnoreCase))
                    {
                        oItem = this.oForm.Items.Item("txtPlanti");
                        oEditText = (EditText)this.oItem.Specific;
                        OrdenServicio.RutinaNombre = oEditText.String;
                        if (string.IsNullOrEmpty(oEditText.String))
                            throw new Exception("Favor de proporcionar el nombre de la rutina");
                    }
                     

                    OrdenServicio.bCreaRutina = true;
                    OrdenServicio.ObjetoRutina = new OrdenServicio.RutinaWS();
                    OrdenServicio.ObjetoRutina.RutinaNombre = OrdenServicio.RutinaNombre;
                    OrdenServicio.ObjetoRutina.EmpresaID = (int)this.oConnfig.UserFull.Dependencia.EmpresaID;
                    OrdenServicio.ObjetoRutina.Sincronizado = 1;

                    oItem = this.oForm.Items.Item("cmbPla");
                    oComboR = (ComboBox)this.oItem.Specific;
                    int rutinaID;
                    if(int.TryParse(this.oComboR.Value,out rutinaID))
                    {
                        OrdenServicio.ObjetoRutina.RutinaID = rutinaID;
                        OrdenServicio.ObjetoRutina.RutinaNombre = oComboR.Selected.Description.Replace(rutinaID.ToString()+" - ",string.Empty) ;


                       
                    }

                    oItem = this.oForm.Items.Item("comboVehOS");
                    oCombo = (ComboBox)this.oItem.Specific;
                    int VehiculoID;
                    int.TryParse(this.oCombo.Value, out VehiculoID);
                    SBO_KF_VEHICULOS_PD vhCtrl = new SBO_KF_VEHICULOS_PD();
                    List<SBO_KF_VEHICULOS_INFO> lstVehiculos = new List<SBO_KF_VEHICULOS_INFO>();
                    string resp = vhCtrl.listaVehiculos(ref oCompany, ref lstVehiculos, VehiculoID);
                    if (lstVehiculos.Count > 0)
                    {
                        OrdenServicio.ObjetoRutina.TipoVehiculoID = lstVehiculos[0].U_TIPOVEHICULOID;
                    }
                }


                int proveedorID;//Proveedor
                oCombo = oForm.Items.Item("comboProOS").Specific;
                if (int.TryParse(this.oCombo.Value, out proveedorID))
                {
                    OrdenServicio.ProveedorServicio.ProveedorID = proveedorID;
                }
                else
                {
                    OrdenServicio.ProveedorServicio = new Proveedor();
                }

                int sucursalID;//Sucursal
                oCombo = oForm.Items.Item("cmbsuc").Specific;
                if (int.TryParse(this.oCombo.Value, out sucursalID))
                {
                    if (OrdenServicio.SucursalOrden == null)
                        OrdenServicio.SucursalOrden = new Sucursal();
                    OrdenServicio.SucursalOrden.SucursalID = sucursalID;
                }
                else
                {
                    OrdenServicio.SucursalOrden = new Sucursal();
                    throw new Exception("Selecciona la sucursal");
                }
                if (sucursalID == 0)
                    throw new Exception("Selecciona la sucursal");



                if (string.IsNullOrEmpty(OrdenServicio.RutinaNombre) && !sTypeRutina.Equals("3", StringComparison.InvariantCultureIgnoreCase)  )
                {
                    DateTime utcTime;//Fecha de servicio
                    oEditText = oForm.Items.Item("txtFechaOS").Specific;
                    if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    {
                        //if (this.dateHelper == null)
                        //    this.GetUserConfigs();
                        //utcTime = this.dateHelper.ToServerTime(Convert.ToDateTime(oEditText.String.Trim()));
                        //OrdenServicio.Fecha = utcTime;

                        OrdenServicio.Fecha = Convert.ToDateTime(oEditText.String.Trim());
                    }
                    else throw new Exception("Selecciona la fecha de servicio");

                    oEditText = oForm.Items.Item("txtHoraOS").Specific;
                    if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    {
                        //if (this.dateHelper == null)
                        //    this.GetUserConfigs();
                        //utcTime = this.dateHelper.ToServerTime(Convert.ToDateTime(oEditText.String.Trim()));
                        //OrdenServicio.Fecha = utcTime;

                        OrdenServicio.Fecha = Convert.ToDateTime(((DateTime)OrdenServicio.Fecha).ToShortDateString() + " " + oEditText.String.Trim()); //Convert.ToDateTime(oEditText.String.Trim());
                    }
                    else throw new Exception("Selecciona la fecha de servicio");

                    




                    //DateTime EnPro;//Fecha de entrada programada
                    oEditText = oForm.Items.Item("txtEnPro").Specific;
                    if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    {
                        //if (this.dateHelper == null)
                        //    this.GetUserConfigs();
                        //EnPro = this.dateHelper.ToServerTime(Convert.ToDateTime(oEditText.String.Trim()));
                        //OrdenServicio.FechaRecepcion = EnPro;

                        OrdenServicio.FechaRecepcion = Convert.ToDateTime(oEditText.String);
                    }
                    else throw new Exception("Selecciona la fecha de entrada programada");

                    //DateTime SalPro;//Fecha de salida programada
                    oEditText = oForm.Items.Item("txtSalPro").Specific;
                    if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    {
                        //if (this.dateHelper == null)
                        //    this.GetUserConfigs();
                        //SalPro = this.dateHelper.ToServerTime(Convert.ToDateTime(oEditText.String.Trim()));
                        //OrdenServicio.FechaLiberacion = SalPro;

                        OrdenServicio.FechaLiberacion = Convert.ToDateTime(oEditText.String);
                    }
                    else throw new Exception("Selecciona la fecha de salida programada");

                    //DateTime EnReal;//Fecha de entrada real
                    oEditText = oForm.Items.Item("txtEnReal").Specific;
                    if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    {
                        //if (this.dateHelper == null)
                        //    this.GetUserConfigs();
                        //EnReal = this.dateHelper.ToServerTime(Convert.ToDateTime(oEditText.String.Trim()));
                        //OrdenServicio.FechaRecepcionReal = EnReal;

                        OrdenServicio.FechaRecepcionReal = Convert.ToDateTime(oEditText.String);
                    }

                    //DateTime SalReal;//Fecha de salida real
                    oEditText = oForm.Items.Item("txtSalReal").Specific;
                    if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    {
                        //if (this.dateHelper == null)
                        //    this.GetUserConfigs();
                        //SalReal = this.dateHelper.ToServerTime(Convert.ToDateTime(oEditText.String.Trim()));
                        //OrdenServicio.FechaLiberacionReal = SalReal;

                        OrdenServicio.FechaLiberacionReal = Convert.ToDateTime(oEditText.String);
                    }

                    //Folio
                    oEditText = oForm.Items.Item("txtFolioOS").Specific;
                    if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                        OrdenServicio.Folio = oEditText.String.Trim();

                    //Responsable dela orden de servicio
                    oCombo = oForm.Items.Item("cmbResp").Specific;
                    int respID;
                    if (int.TryParse(oCombo.Value, out respID))
                        OrdenServicio.ResponsableOrdenServicio.EmpleadoID = respID;
                    if (respID == 0)
                        throw new Exception("Asigna el responsable");

                    //Aprobador de la orden de servico
                    oCombo = oForm.Items.Item("cmbAprob").Specific;
                    int apID;
                    if (int.TryParse(oCombo.Value, out apID))
                        OrdenServicio.AprobadorOrdenServicio.EmpleadoID = apID;
                    if (apID == 0)
                        throw new Exception("Asigna el aprobador");

                    oCombo = oForm.Items.Item("cmbestatus").Specific;
                    OrdenServicio.EstatusOrden.EstatusOrdenID = Convert.ToInt32(oCombo.Selected.Value);

                    if (OrdenServicio.FacturableCliente == 1)
                    {
                        if (string.IsNullOrEmpty(OrdenServicio.socioCliente))
                            throw new Exception("Selecciona el cliente para la facturación");
                    }

                    try
                    {
                        OrdenServicio.FolioOrden = new FolioOrden();
                        oLabel = oForm.Items.Item("txtfolio").Specific;
                        var lblfolio = oLabel.Caption.Split('/');
                        OrdenServicio.FolioOrden.Folio = Convert.ToInt32(lblfolio[0]);
                        OrdenServicio.FolioOrden.AnioFolio = Convert.ToInt32(lblfolio[1]);
                        OrdenServicio.FolioOrden.Propietario = new Empresa();
                        OrdenServicio.FolioOrden.Propietario.EmpresaID = this.oConnfig.UserFull.Dependencia.EmpresaID;
                    }
                    catch { }

                    #region nuevos datos impso & hertz
                    //--cmbSyscr
                    //--cmbCompo

                    try
                    {
                        oCombo = oForm.Items.Item("cmbSyscr").Specific;
                        if (!string.IsNullOrEmpty(oCombo.Value))
                        {
                            OrdenServicio.SistemaID = Convert.ToInt32(oCombo.Value);
                            oCombo = oForm.Items.Item("cmbCompo").Specific;
                            if (!string.IsNullOrEmpty(oCombo.Value))
                                OrdenServicio.ComponenteID = Convert.ToInt32(oCombo.Value);
                            else OrdenServicio.SistemaID = 0;

                        }

                        OrdenServicio.CausaRaiz = new List<SBO_KF_CAUSAOS_INFO>();
                        if (OrdenServicio.SistemaID != 0 && OrdenServicio.ComponenteID != 0)
                        {
                            SBO_KF_CAUSAOS_INFO ocausa = new SBO_KF_CAUSAOS_INFO();
                            ocausa.ComponenteCR = new SBO_KF_COMPONENTEOS_INFO()
                            {
                                ComponenteID = OrdenServicio.ComponenteID
                            };

                            ocausa.SistemaCR = new SBO_KF_SISTEMAOS_INFO()
                            {
                                SistemaID = OrdenServicio.SistemaID
                            };
                            OrdenServicio.CausaRaiz.Add(ocausa);
                        }
                    }
                    catch
                    {
                        throw new Exception("No se ha determinado el sistema - componente");
                    }


                    oCombo = oForm.Items.Item("cmblevel").Specific;
                    try
                    {
                        OrdenServicio.PrioridadID = Convert.ToInt32(this.oCombo.Value);
                        switch (OrdenServicio.PrioridadID)
                        {
                            case 0:
                                OrdenServicio.Prioridad = "No urgente";
                                break;
                            case 1:
                                OrdenServicio.Prioridad = "Urgente";
                                break;
                            case 2:
                                OrdenServicio.Prioridad = "Muy urgente";
                                break;
                        }
                    }
                    catch { }

                    oEditText = oForm.Items.Item("txtInspec").Specific;
                    if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                        OrdenServicio.inspeccionID = oEditText.String.Trim();

                    oEditText = oForm.Items.Item("txtClisap").Specific;
                    if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                        OrdenServicio.socioCliente = oEditText.String.Trim();


                    oEditText = oForm.Items.Item("txthEntr").Specific;
                    if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    {
                        OrdenServicio.HoraEnProgramada = oEditText.String.Trim();
                        if (OrdenServicio.HoraEnProgramada != "00:00")
                        {
                            if (!this.formatoHora(OrdenServicio.HoraEnProgramada))
                                throw new Exception("El formato de hora entrada programada no es valida");
                        }
                    }
                    else throw new Exception("El formato de hora entrada programada no es valida");

                    oEditText = oForm.Items.Item("txtHsal").Specific;
                    if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    {
                        OrdenServicio.HoraSalProgramada = oEditText.String.Trim();
                        if (OrdenServicio.HoraSalProgramada != "00:00")
                        {
                            if (!this.formatoHora(OrdenServicio.HoraSalProgramada))
                                throw new Exception("El formato de hora salida programada no es valido");
                        }
                    }
                    else throw new Exception("El formato de hora salida programada no es valido");

                    oEditText = oForm.Items.Item("txtHReale").Specific;
                    if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    {
                        OrdenServicio.HoraEnReal = oEditText.String.Trim();
                        if (OrdenServicio.HoraEnReal != "00:00")
                        {
                            if (!this.formatoHora(OrdenServicio.HoraEnReal))
                                throw new Exception("El formato de hora entrada real no es valido");
                        }
                    }

                    oEditText = oForm.Items.Item("txtHSalr").Specific;
                    if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    {
                        OrdenServicio.HoraSalReal = oEditText.String.Trim();
                        if (OrdenServicio.HoraSalReal != "00:00")
                        {
                            if (!this.formatoHora(OrdenServicio.HoraSalReal))
                                throw new Exception("El formato de hora salida real no es valido");
                        }
                    }

                    #endregion nuevos datos impso & hertz

                    oItem = oForm.Items.Item("CBTI");
                    oCheck = (CheckBox)(oItem.Specific);
                    OrdenServicio.TallerInterno = oCheck.Checked;


                    oItem = oForm.Items.Item("chkoinv");
                    oCheck = (CheckBox)(oItem.Specific);
                    OrdenServicio.FacturableCliente = oCheck.Checked ? 1 : 0;
                }

                
                //Descripcion
                oEditText = oForm.Items.Item("txtDescOs").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    OrdenServicio.Descripcion = oEditText.String.Trim();
                else throw new Exception("Proporciona la descripción");
                //Metodo de pago
                oCombo = oForm.Items.Item("comboMPOSv").Specific;
                int pagoID;
                if (int.TryParse(oCombo.Value, out pagoID))
                {
                    OrdenServicio.TipoDePago.TipoPagoID = pagoID;
                    OrdenServicio.TipoDePago.TipoPago = oCombo.Selected.Description;
                }
                //Detalle de pago
                oEditText = oForm.Items.Item("txtDetOSv").Specific;
                if (!string.IsNullOrEmpty(oEditText.String))
                    OrdenServicio.TipoDePago.DetallePago = oEditText.String.Trim();
                //Costo
                oLabel = this.oForm.Items.Item("lblCostV").Specific;
                if (!string.IsNullOrEmpty(oLabel.Caption.Trim()))
                    OrdenServicio.Costo = Convert.ToDecimal(costoTotal);


                //Impuesto
                oLabel = oForm.Items.Item("lblImpOSv").Specific;
                if (!string.IsNullOrEmpty(oLabel.Caption))
                {
                    string simpuesto = oLabel.Caption.Trim();
                    if (!string.IsNullOrEmpty(simpuesto))
                    {
                        try
                        {
                            simpuesto = simpuesto.Replace("%", "").Replace(".00", "").Trim();
                            OrdenServicio.Impuesto = Convert.ToDecimal(simpuesto);
                        }
                        catch { }
                    }
                }
                
                OrdenServicio.FechaCaptura = DateTime.UtcNow;
                //DateTime SalReal;//Fecha de salida real
                oEditText = oForm.Items.Item("txtHoroOs").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                {
                 
                    OrdenServicio.Horometro = Convert.ToInt32(oEditText.String);
                }
                else
                {
                    OrdenServicio.Horometro = 0;
                }

                oEditText = oForm.Items.Item("txtOdomOs").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                {

                    OrdenServicio.Odometro = Convert.ToInt32(oEditText.String);
                }
                else
                {
                    OrdenServicio.Odometro = 0;
                }

                if ( !OrdenServicio.TallerInterno && proveedorID == 0 && (this.oSetting.EstatusOS == OrdenServicio.EstatusOrden.EstatusOrdenID))
                    throw new Exception("Favor de seleccionar el proveedor de la orden");
                

                

                
                //OrdenServicio.alertas.empresa = this.Config.UserFull.Dependencia;
                //OrdenServicio.SucursalOrden = this.Config.UserFull.EmpleadoSucursal;
                OrdenServicio.EmpresaOrden = this.oConnfig.UserFull.Dependencia;
                OrdenServicio.EncargadoOrdenServicio = this.oConnfig.UserFull;
                //OrdenServicio.DetalleOrden = this.ListaServiciosAgregados;
                //OrdenServicio.FolioOrden = (FolioOrden)EFolio.folio.Clone();
                OrdenServicio.alertas = new AlertaMantenimiento();
                OrdenServicio.alertas.servicio = new Servicio();
                OrdenServicio.alertas.servicio.ServicioID = 0;

                if (this.lstservicios.Count == 0)
                    throw new Exception("Favor de proporcionar los servicios");
                OrdenServicio.Sincronizado = 1;
                response = "OK";
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }
            return response;
        }
        bool formatoHora(string hora)
        {
            bool bEstatus = false;
            try
            {
                System.Globalization.CultureInfo provider = System.Globalization.CultureInfo.InvariantCulture;
                DateTime dt = DateTime.ParseExact(hora.Replace(":", string.Empty), "HHmm", provider);
                if (dt.Hour != null)
                    bEstatus = true;
            }
            catch { }
            return bEstatus;
        }
        void GestionInventario()
        {
            try
            {

                /*string sresponse = string.Empty, sconsultaSql = string.Empty;
                SBOKF_CL_Configuracion_INFO oSetting = new SBOKF_CL_Configuracion_INFO();
                SBOKF_CL_Configuracion_AD oConfiguracion = new SBOKF_CL_Configuracion_AD();
                sresponse = oConfiguracion.recuperaConfiguracion(ref oCompany, out sconsultaSql, ref oSetting);
                if (sresponse == "OK")
                {*/
                GestionGlobales oGlobales = new GestionGlobales();
                oGlobales.FillCombo("Ninguno", "WhsCode", "WhsName", "cmbAlmacn", GestionGlobales.AlmacenRecarga(), this.oForm, this.oCompany);
                this.AddChooseHerramientas();
                //this.AddChooseMecanicos();
                if (oSetting.cKANAN == 'N')
                {
                    oItem = this.oForm.Items.Item("txtRefSAP");
                    oItem.Visible = true;
                    oItem = this.oForm.Items.Item("cmbRefKF");
                    oItem.Visible = false;
                    this.AddItemsCFL();
                }
                else
                {
                    oItem = this.oForm.Items.Item("cmbAlmacn");
                    oItem.Visible = false;

                    oItem = this.oForm.Items.Item("txtRefSAP");
                    oItem.Visible = false;
                    oItem = this.oForm.Items.Item("cmbRefKF");
                    oItem.Visible = true;
                    RefaccionesWS ReffWS = new RefaccionesWS();
                    List<DetalleRefaccion> dataRef = ReffWS.SolicitarRefacciones(this.oConnfig.UserFull.Dependencia.EmpresaID);
                    List<DetalleRefaccion> RefaccionesKF = dataRef;
                    ComboBox oCombo = oItem.Specific;
                    KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Selecciona la refacción");
                    foreach (DetalleRefaccion rfaccion in dataRef)
                    {
                        string value = rfaccion.ArticuloID.ToString();
                        string description = string.Format("{0} - {1}", value, rfaccion.NombreArticulo.ToString().Trim());
                        oCombo.ValidValues.Add(rfaccion.NombreArticulo.ToString().Trim(), description.Trim());
                    }
                    
                }
                //}
            }
            catch { }
        }
        void SelectComboBoxValue(string comboname, string value)
        {
            oCombo = oForm.Items.Item(comboname).Specific;
            oCombo.Select(value, BoSearchKey.psk_ByValue);
        }
        void CargaDatosAlerta()
        {
            if (this.AlertaID > 0)
            {
                string serviciosIN = string.Empty, VehiculosIN = string.Empty;
                List<SBO_KF_ALERTA_INFO> lstalertas = new List<SBO_KF_ALERTA_INFO>();
                List<SBO_KF_VEHICULOS_INFO> lstVehiculos = new List<SBO_KF_VEHICULOS_INFO>();
                List<DetalleOrden> lstServicios = new List<DetalleOrden>();
                SBO_KF_ALERTA_PD control = new SBO_KF_ALERTA_PD();
                SBO_KF_VEHICULOS_PD contolVehiculo = new SBO_KF_VEHICULOS_PD();
                SBO_KF_ORDENSERVICIO_PD controlServicio = new SBO_KF_ORDENSERVICIO_PD();
                string response = control.lstadoAlertas(ref this.oCompany, ref lstalertas, this.oConnfig.UserFull.Dependencia.EmpresaID, (int?)this.AlertaID);
                if (response == "OK")
                {
                    if (lstalertas[0].U_RUTINAID == 0)
                    {

                        oEditText = oForm.Items.Item("txtDescOs").Specific;
                        oEditText.String = lstalertas[0].U_DESCRIPCION_ES;
                        try
                        {
                            oCombo = oForm.Items.Item("cmbsuc").Specific;
                            oCombo.Select(lstalertas[0].U_SUCURSALID.ToString());
                        }
                        catch { }

                        foreach (SBO_KF_ALERTA_INFO alerta in lstalertas)
                        {
                            serviciosIN += string.Format("{0},", alerta.U_SERVICIOID);
                            VehiculosIN += string.Format("{0},", alerta.U_MANTENIBLEID);
                        }
                        response = contolVehiculo.listaVehiculos(ref this.oCompany, ref  lstVehiculos, VehiculosIN: VehiculosIN.TrimEnd(','));
                        if (response != "OK")
                            throw new Exception("Los vehiculos no se han encontrado como datos dentro del sistema");
                        response = controlServicio.listaServiciosAutomotriz(ref this.oCompany, ref lstServicios, serviciosIN: serviciosIN.TrimEnd(','));
                        if (response != "OK")
                            throw new Exception("Los servicios no se han encontrado como datos dentro del sistema");


                        this.lstservicios = new List<DetalleOrden>();
                        foreach (SBO_KF_ALERTA_INFO oAlerta in lstalertas)
                        {
                            DetalleOrden oServicio = new DetalleOrden();
                            oServicio.Vehiculo = new Vehiculo();
                            oServicio.Servicio = new Servicio();
                            var frServicio = lstServicios.FirstOrDefault(x => x.Servicio.ServicioID == oAlerta.U_SERVICIOID);
                            var frVehiculo = lstVehiculos.FirstOrDefault(x => x.U_VEHICULOID == oAlerta.U_MANTENIBLEID);
                            oServicio.Vehiculo.MantenibleID = (int?)oAlerta.U_MANTENIBLEID;
                            oServicio.Servicio.ServicioID = (int?)oAlerta.U_SERVICIOID;
                            if (frServicio != null)
                                oServicio.Servicio.Nombre = frServicio.Servicio.Nombre;
                            if (frVehiculo != null)
                                oServicio.Vehiculo.Nombre = frVehiculo.U_NOMBRE;
                            oServicio.Costo = 0;
                            this.lstservicios.Add(oServicio);
                        }
                        this.FillGridServiciosFromList();
                    }
                    else
                    {
                        oCombo = this.oForm.Items.Item("comboVehOS").Specific;
                        oCombo.Select(lstalertas[0].U_MANTENIBLEID.ToString());

                        oCombo = this.oForm.Items.Item("cmbrutina").Specific;
                        oCombo.Select("2");
                        this.FormLoadPlantillaMood();

                        oCombo = this.oForm.Items.Item("cmbPla").Specific;
                        oCombo.Select(lstalertas[0].U_RUTINAID.ToString());                       

                        this.PlantillaToForm();
                        this.FormLoadSucursalDeVehiculo();
                    }
                }
                else this.SBO_Application.MessageBox("No se ha encontrado la información de la alerta.", 1, "Ok");
            }
        }

        void CargaIncidencia()
        {
            if (this.IncidenciaID > 0)
            {
                List<SBO_KF_INCIDENCIA_INFO> lstIncidencias = new List<SBO_KF_INCIDENCIA_INFO>();
                SBO_KF_ALERTA_PD control = new SBO_KF_ALERTA_PD();
                string response = control.lstIncidencias(ref this.oCompany, ref lstIncidencias, (int?)this.IncidenciaID);
                if (response == "OK")
                {
                    SBO_KF_INCIDENCIA_INFO oIncidencia = lstIncidencias[0];
                    oEditText = oForm.Items.Item("txtDescOs").Specific;
                    oEditText.String = oIncidencia.U_DESCRIPCION;

                    oEditText = oForm.Items.Item("txtFechaOS").Specific;
                    oEditText.String = oIncidencia.U_FECHAINCIDENCIA.ToString("dd/MM/yyyy");

                    oCombo = oForm.Items.Item("comboVehOS").Specific;
                    oCombo.Select(oIncidencia.U_VEHICULOID.ToString());
                }
                else this.SBO_Application.MessageBox("No se ha podido cargar la información de la incidencia.", 1, "Ok");
            }
        }
        string NotificaSolicitud(int? EmpleadoID, OrdenServicio orden = null)
        {
            String Attachment = string.Empty, resultado = string.Empty;
            try
            {
                resultado = "ERROR";
                List<SBO_KF_OPERADOR_INFO> lstOperador = new List<SBO_KF_OPERADOR_INFO>();
                this.ExportaPDF(out Attachment, false);
                resultado = new SBO_KF_OPERADOR_PD().listaOperadores(ref this.oCompany, ref lstOperador, Convert.ToInt32(EmpleadoID));
                string DatosOrdenServicio = "";
                if (orden != null)
                {
                    DatosOrdenServicio = "<b>Folio de la Orden: </b>" + orden.FolioOrden.Folio.ToString() + "/" + orden.FolioOrden.AnioFolio.ToString() + "<br>";
                    DatosOrdenServicio += "<b>Fecha: </b>" + string.Format("{0:MM/dd/yyyy}", orden.Fecha) + "<br>";
                    DatosOrdenServicio += "<b>Fecha de Ingreso Programada: </b>" + string.Format("{0:MM/dd/yyyy}", orden.FechaRecepcion) + "<br>";
                    DatosOrdenServicio += "<b>Fecha de Salida Programada: </b>" + string.Format("{0:MM/dd/yyyy}", orden.FechaLiberacion) + "<br>";
                    DatosOrdenServicio += "<b>Descripción: </b>" +  orden.Descripcion + "<br>";
                    

                }
                string logoKanan = " Administrador de Flotillas <B>KANANFLEET</B> ";

                if (resultado == "OK" && lstOperador.Count > 0)
                {
                    String HTML = string.Format(@"<html><head><title></title></head><body>
                                        <p>Solicitud de orden de servicio</p>  
                                        <h2>{0} {1} </h2>
                                        <p> Se ha creado una solicitud de orden de servicio se envia documento de detalle de la solicitud como adjunto.  </p>
                                        {2}
                                        <p>*Este es un correo enviado a través de un servicio de forma automatica, favor de no responderlo.*</p>
                                        <BR><BR>
                                        {3}
                                        </body></html>", lstOperador[0].U_NOMBRE, lstOperador[0].U_APELLIDO1, DatosOrdenServicio, logoKanan);

                    resultado = GestionGlobales.Notificarcorreo(this.oSetting, lstOperador[0].U_CORREO, "Solicitud de orden de servicio", HTML, Attachment);

                }
                else resultado = "Empleado no encontrado";
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("SBO_KF_SolicitaOrdenesView.cs->NotificaSolicitud()", ex.Message);

            }
            return resultado;
        }
        string NotificaSalidaInv(int? SucursalID, OrdenServicio orden = null)
        {
            String Attachment = string.Empty, resultado = string.Empty;
            try
            {
                resultado = "ERROR";
                List<SBO_KF_SUCURSAL_INFO> lstSucursales = new List<SBO_KF_SUCURSAL_INFO>();
                this.ExportaPDF(out Attachment, false);
                resultado = new SBO_KF_SUCURSALES_PD().listaSucursales(ref oCompany, ref lstSucursales, Convert.ToInt32(-1), string.Empty,  SucursalID.ToString());
//                resultado = new SBO_KF_OPERADOR_PD().listaOperadores(ref this.oCompany, ref lstOperador, Convert.ToInt32(EmpleadoID));
                string DatosOrdenServicio = "";
                if (orden != null)
                {
                    DatosOrdenServicio = "<b>Folio de la Orden: </b>" + orden.FolioOrden.Folio.ToString() + "/" + orden.FolioOrden.AnioFolio.ToString() + "<br>";
                    DatosOrdenServicio += "<b>Fecha: </b>" + string.Format("{0:MM/dd/yyyy}", orden.Fecha) + "<br>";
                    DatosOrdenServicio += "<b>Fecha de Ingreso Programada: </b>" + string.Format("{0:MM/dd/yyyy}", orden.FechaRecepcion) + "<br>";
                    DatosOrdenServicio += "<b>Fecha de Salida Programada: </b>" + string.Format("{0:MM/dd/yyyy}", orden.FechaLiberacion) + "<br>";
                    DatosOrdenServicio += "<b>Descripción: </b>" + orden.Descripcion + "<br>";  

                    DatosOrdenServicio += "<table><tr><th>Cantidad</th><th>Código</th><th>Costo </th> </tr>";
                    foreach(DetalleRefaccion dref in orden.RefaccionesDto)
                    {
                        DatosOrdenServicio += "<tr><th>"+dref.Cantidad.ToString() +"</th><th>"+dref.Articulo+"</th><th>"+dref.Costo+"</th> </tr>";
                    }
                    DatosOrdenServicio += "</table>";

                }
                string logoKanan = " Administrador de Flotillas <B>KANANFLEET</B> ";

                if (resultado == "OK" && lstSucursales.Count > 0)
                {
                    String HTML = string.Format(@"<html><head><title></title></head><body>
                                        <p>Notificación de Salida de Refacciones</p>  
                                        <h2>{0}   </h2>
                                        <p> Se ha creado una salida de refacciones, se envia el documento de detalle de la solicitud de Orden de Servicio que genera el movimiento como adjunto.  </p>
                                        {1}
                                        <p>*Este es un correo enviado a través de un servicio de forma automatica, favor de no responderlo.*</p>
                                        <BR><BR>
                                        {2}
                                        </body></html>", lstSucursales[0].U_ALMACEN, DatosOrdenServicio, logoKanan);

                    resultado = GestionGlobales.Notificarcorreo(this.oSetting, lstSucursales[0].U_MAILALMACEN, "Salida de Inventario", HTML, Attachment);

                }
                else resultado = "Empleado no encontrado";
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("SBO_KF_SolicitaOrdenesView.cs->NotificaSolicitud()", ex.Message);

            }
            return resultado;
        }
        #region acciones disparadas desde evento control, botones, combos, choose list, etc.

        void GuardaOrdenServicio()
        {
            Int64 codigosiguiente = -1,  IDOrdenServicio = -1;
            string response = string.Empty;
            OrdenServicio OrdenServicio = new Kanan.Mantenimiento.BO2.OrdenServicio();
            Kanan.Mantenimiento.BO2.OrdenServicio.OrdenServicioJSON OrdenWS = new Kanan.Mantenimiento.BO2.OrdenServicio.OrdenServicioJSON();
            SBO_KF_ORDENSERVICIO_PD controlOS = new SBO_KF_ORDENSERVICIO_PD();
            SBO_KF_OrdenServicio_WS controlwsOS = new SBO_KF_OrdenServicio_WS();
            SBO_KF_BITACORA_PD controlBitacora = new SBO_KF_BITACORA_PD();
            Add oOUDT = new Add(ref this.oCompany, null);
            //ProgressBar oProgress = null;
            try
            {
                bLocked = true;
                //oProgress = this.SBO_Application.StatusBar.CreateProgressBar("Guardando información", 1, true);
                response = this.FormToEntity(ref OrdenServicio);
                if (response == "OK")
                {
                    oItem = this.oForm.Items.Item("cmbrutina");
                    oCombo = (ComboBox)this.oItem.Specific;
                    string sTypeRutina = oCombo.Selected.Value;



                    if (string.IsNullOrEmpty(OrdenServicio.RutinaNombre) && !sTypeRutina.Equals("3", StringComparison.InvariantCultureIgnoreCase))
                    {
                        #region GUARDA ORDEN DE SERVICIO
                        this.oCompany.StartTransaction();
                        IDOrdenServicio = oOUDT.recuperIDMaximoTabla("VSKF_ORDENSERVICIO");
                        if (IDOrdenServicio <= 0)
                            throw new Exception("No se ha podido asignar el folio a la orden de servicio");

                        OrdenServicio.OrdenServicioID = (Int32?)IDOrdenServicio;
                        OrdenServicio.Code = (Int32)IDOrdenServicio;
                        OrdenServicio.Name = IDOrdenServicio.ToString();
                        
                        response = controlOS.InsertaOrdenServicio(ref this.oCompany, OrdenServicio);
                        if (response != "OK")
                            throw new Exception(response);

                        #region agrega registros en tablas relacionadas a orden de servicio
                        if (this.lstmecanicos.Count > 0)
                        {
                            codigosiguiente = -1;
                            codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_MECANICOSOS");
                            if (codigosiguiente < 0)
                                throw new Exception("No se ha podido recueperar el folio siguiente para el detalle de mecanicos");
                            foreach (Kanan.Mantenimiento.BO2.SBO_KF_OHEM_INFO omecanico in this.lstmecanicos)
                            {
                                omecanico.U_ORDENSERVICIOID = OrdenServicio.OrdenServicioID.ToString();
                                response = controlOS.InsertaMecanicos(ref this.oCompany, omecanico, codigosiguiente.ToString());
                                if (response != "OK")
                                    throw new Exception(response);
                                codigosiguiente++;
                            }
                        }

                        if (this.lstcostoadicional.Count > 0)
                        {
                            codigosiguiente = -1;
                            codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_OSCOSTOSADICIO");
                            if (codigosiguiente < 0)
                                throw new Exception("No se ha podido recueperar el folio siguiente para costos adicionales");
                            foreach (SBOKF_CL_CostosAdicional_INFO ocosto in this.lstcostoadicional)
                            {
                                ocosto.Code = OrdenServicio.OrdenServicioID.ToString();
                                response = controlOS.InsertaCostoAdicional(ref this.oCompany, ocosto, codigosiguiente.ToString());
                                if (response != "OK")
                                    throw new Exception(response);
                                codigosiguiente++;

                            }
                        }

                        if (this.lstherramientas.Count > 0)
                        {
                            codigosiguiente = -1;
                            codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_HERRAMIENTAOS");
                            if (codigosiguiente < 0)
                                throw new Exception("No se ha podido recueperar el folio siguiente para el detalle de herramientas");
                            foreach (SBO_KF_Herramientas_INFO oherramienta in this.lstherramientas)
                            {
                                oherramienta.OrdenServicioID = OrdenServicio.OrdenServicioID.ToString();
                                response = controlOS.InsertaHerramientas(ref this.oCompany, oherramienta, codigosiguiente.ToString());
                                if (response != "OK")
                                    throw new Exception(response);
                                codigosiguiente++;
                            }
                        }

                        if (this.lstrefacciones.Count > 0)
                        {
                            codigosiguiente = -1;
                            codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_DTLLEREFACCION");
                            if (codigosiguiente < 0)
                                throw new Exception("No se ha podido recueperar el folio siguiente para el detalle de refacciones");
                            

                            foreach (DetalleRefaccion orefaccion in this.lstrefacciones)
                            {
                                orefaccion.OrdenServicio = new Kanan.Mantenimiento.BO2.OrdenServicio();
                                orefaccion.OrdenServicio.OrdenServicioID = OrdenServicio.OrdenServicioID;
                                response = controlOS.InsertaRefaccion(ref this.oCompany, codigosiguiente.ToString(), orefaccion);
                                if (response != "OK")
                                    throw new Exception(response);
                                codigosiguiente++;
                            }
                        }

                        if (this.lstservicios.Count > 0)
                        {
                            codigosiguiente = -1;
                            codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_DETALLEORDEN");
                            if (codigosiguiente < 0)
                                throw new Exception("No se ha podido recueperar el folio siguiente para el detalle de orden");
                            foreach (DetalleOrden odetalle in this.lstservicios)
                            {
                                odetalle.OrdenServicio = new Kanan.Mantenimiento.BO2.OrdenServicio();
                                odetalle.OrdenServicio.OrdenServicioID = OrdenServicio.OrdenServicioID;
                                odetalle.ConsecutivoID = Convert.ToInt32(codigosiguiente);
                                odetalle.TipoMantenibleID = 1;
                                response = controlOS.InsertaDetalleOrdenServicio(ref this.oCompany, odetalle);
                                if (response != "OK")
                                    throw new Exception(response);
                                codigosiguiente++;
                            }
                        }
                        #endregion agrega registros en tablas relacionadas a orden de servicio

                        #region enviar informacion a webservice & actualizar tablas con identificadores desde web
                        this.lstrefacciones.ForEach(x => x.SucursalID = OrdenServicio.SucursalOrden.SucursalID);
                        OrdenServicio.DetalleOrden = this.lstservicios;
                        OrdenServicio.CostosAdicionalesDto = this.lstcostoadicional;
                        OrdenServicio.HerramientasDto = this.lstherramientas;
                        OrdenServicio.EmpleadosDto = this.lstmecanicos;
                        OrdenServicio.RefaccionesDto = this.lstrefacciones;

                        this.lstherramientas.ForEach(x=>x.SucursalID = OrdenServicio.SucursalOrden.SucursalID);
                        this.lstservicios.ForEach(x => x.DetalleOrdenID = 0);
                        this.lstcostoadicional.ForEach(x=>x.DetalleOrdenID = 0);
                        this.lstherramientas.ForEach(x=>x.DetalleOrdenID = 0);
                        this.lstmecanicos.ForEach(x => x.DetalleOrdenID = 0);
                        this.lstrefacciones.ForEach(x => x.DetalleRefaccionID = 0);

                        //oTools.ColocarMensaje("1. controlwsOS.GestionOrdenesWebAPI", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

                        response = controlwsOS.GestionOrdenesWebAPI(GestionGlobales.sURLWs, ref OrdenWS, OrdenServicio);
                        if (response != "OK")
                            throw new Exception(response);
                        try
                        {
                            OrdenWS.Code = IDOrdenServicio.ToString();
                            response = controlOS.ActualizaFoliosWS(ref oCompany, OrdenWS);
                            if (response != "OK")
                                throw new Exception(string.Format("Los folio del WS no se han asignado a la orden creada en SAP {0}", response));
                        }
                        catch (Exception ex)
                        {
                            KananSAP.Helper.KananSAPHerramientas.LogError("SBO_KF_SolicitaOrdenesView.cs->GuardaOrdenServicio()", ex.Message);
                        }
                        //actualiza identificadores de WS
                        #endregion enviar informacion a webservice & actualizar tablas con identificadores desde web

                        //oTools.ColocarMensaje("2. this.InsertaBitacora", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

                        this.InsertaBitacora(1, IDOrdenServicio.ToString(), OrdenWS.OrdenServicioID.ToString(), true);
                        //oTools.ColocarMensaje("3. CreaDocumentoSBO", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                        this.CreaDocumentoSBO(OrdenServicio);
                        response = this.NotificaSolicitud(OrdenServicio.AprobadorOrdenServicio.EmpleadoID, OrdenServicio);
                        if (response != "OK")
                            oTools.ColocarMensaje("La orden de servicio se ha creado con éxito. El correo no se ha podido enviar", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                        else
                            oTools.ColocarMensaje("La orden de servicio se ha creado con éxito.", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                        this.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                        this.LimpiaCampos();
                        //oProgress.Stop();
                        bLocked = false;
                    #endregion
                    }
                    else
                    {
                        #region GUARDAR RUTINA
                        this.oCompany.StartTransaction();
                        IDOrdenServicio = oOUDT.recuperIDMaximoTabla("VSKF_ORDENSERVICIO");
                        if (IDOrdenServicio <= 0)
                            throw new Exception("No se ha podido asignar el folio a la orden de servicio");

                        OrdenServicio.OrdenServicioID = (Int32?)IDOrdenServicio;
                        OrdenServicio.Code = (Int32)IDOrdenServicio;
                        OrdenServicio.Name = IDOrdenServicio.ToString();
                        ////////////response = controlOS.InsertaOrdenServicio(ref this.oCompany, OrdenServicio);
                        ////////////if (response != "OK")
                        ////////////    throw new Exception(response);

                        #region agrega registros en tablas relacionadas a orden de servicio
                        if (this.lstmecanicos.Count > 0)
                        {
                            codigosiguiente = -1;
                            codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_MECANICOSOS");
                            if (codigosiguiente < 0)
                                throw new Exception("No se ha podido recueperar el folio siguiente para el detalle de mecanicos");
                            foreach (Kanan.Mantenimiento.BO2.SBO_KF_OHEM_INFO omecanico in this.lstmecanicos)
                            {
                                omecanico.U_ORDENSERVICIOID = OrdenServicio.OrdenServicioID.ToString();
                                ////response = controlOS.InsertaMecanicos(ref this.oCompany, omecanico, codigosiguiente.ToString());
                                ////if (response != "OK")
                                ////    throw new Exception(response);
                                codigosiguiente++;
                            }
                        }

                        if (this.lstcostoadicional.Count > 0)
                        {
                            codigosiguiente = -1;
                            codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_OSCOSTOSADICIO");
                            if (codigosiguiente < 0)
                                throw new Exception("No se ha podido recueperar el folio siguiente para costos adicionales");
                            foreach (SBOKF_CL_CostosAdicional_INFO ocosto in this.lstcostoadicional)
                            {
                                ocosto.Code = OrdenServicio.OrdenServicioID.ToString();
                                //////response = controlOS.InsertaCostoAdicional(ref this.oCompany, ocosto, codigosiguiente.ToString());
                                //////if (response != "OK")
                                //////    throw new Exception(response);
                                codigosiguiente++;

                            }
                        }

                        if (this.lstherramientas.Count > 0)
                        {
                            codigosiguiente = -1;
                            codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_HERRAMIENTAOS");
                            if (codigosiguiente < 0)
                                throw new Exception("No se ha podido recueperar el folio siguiente para el detalle de herramientas");
                            foreach (SBO_KF_Herramientas_INFO oherramienta in this.lstherramientas)
                            {
                                oherramienta.OrdenServicioID = OrdenServicio.OrdenServicioID.ToString();
                                //response = controlOS.InsertaHerramientas(ref this.oCompany, oherramienta, codigosiguiente.ToString());
                                //if (response != "OK")
                                //    throw new Exception(response);
                                codigosiguiente++;
                            }
                        }

                        if (this.lstrefacciones.Count > 0)
                        {
                            codigosiguiente = -1;
                            codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_DTLLEREFACCION");
                            if (codigosiguiente < 0)
                                throw new Exception("No se ha podido recueperar el folio siguiente para el detalle de refacciones");
                            foreach (DetalleRefaccion orefaccion in this.lstrefacciones)
                            {
                                orefaccion.OrdenServicio = new Kanan.Mantenimiento.BO2.OrdenServicio();
                                orefaccion.OrdenServicio.OrdenServicioID = OrdenServicio.OrdenServicioID;
                                //response = controlOS.InsertaRefaccion(ref this.oCompany, codigosiguiente.ToString(), orefaccion);
                                //if (response != "OK")
                                //    throw new Exception(response);
                                codigosiguiente++;
                            }
                        }

                        if (this.lstservicios.Count > 0)
                        {
                            codigosiguiente = -1;
                            codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_DETALLEORDEN");
                            if (codigosiguiente < 0)
                                throw new Exception("No se ha podido recueperar el folio siguiente para el detalle de orden");
                            foreach (DetalleOrden odetalle in this.lstservicios)
                            {
                                odetalle.OrdenServicio = new Kanan.Mantenimiento.BO2.OrdenServicio();
                                odetalle.OrdenServicio.OrdenServicioID = OrdenServicio.OrdenServicioID;
                                odetalle.ConsecutivoID = Convert.ToInt32(codigosiguiente);
                                odetalle.TipoMantenibleID = 1;
                                //response = controlOS.InsertaDetalleOrdenServicio(ref this.oCompany, odetalle);
                                //if (response != "OK")
                                //    throw new Exception(response);
                                codigosiguiente++;
                            }
                        }
                        #endregion agrega registros en tablas relacionadas a orden de servicio

                        #region enviar informacion a webservice & actualizar tablas con identificadores desde web
                        this.lstrefacciones.ForEach(x => x.SucursalID = OrdenServicio.SucursalOrden.SucursalID);
                        OrdenServicio.DetalleOrden = this.lstservicios;
                        OrdenServicio.CostosAdicionalesDto = this.lstcostoadicional;
                        OrdenServicio.HerramientasDto = this.lstherramientas;
                        OrdenServicio.EmpleadosDto = this.lstmecanicos;
                        OrdenServicio.RefaccionesDto = this.lstrefacciones;

                        this.lstherramientas.ForEach(x => x.SucursalID = OrdenServicio.SucursalOrden.SucursalID);
                        this.lstservicios.ForEach(x => x.DetalleOrdenID = 0);
                        this.lstcostoadicional.ForEach(x => x.DetalleOrdenID = 0);
                        this.lstherramientas.ForEach(x => x.DetalleOrdenID = 0);
                        this.lstmecanicos.ForEach(x => x.DetalleOrdenID = 0);
                        this.lstrefacciones.ForEach(x => x.DetalleRefaccionID = 0);
                         
                        #endregion enviar informacion a webservice & actualizar tablas con identificadores desde web



                        this.InsertaBitacora(1, IDOrdenServicio.ToString(), OrdenWS.OrdenServicioID.ToString(), true);
                        if (!sTypeRutina.Equals("3", StringComparison.InvariantCultureIgnoreCase)) 
                        {
                            response = controlwsOS.GestionOrdenesRutinaWebAPI(GestionGlobales.sURLWs, ref OrdenWS, OrdenServicio);
                            this.creaRutinaDeOrden(OrdenServicio, OrdenWS.Rutina.RutinaID);
                        }                            
                        else
                        {
                            response = controlwsOS.GestionOrdenesRutinaWebAPI(GestionGlobales.sURLWs, ref OrdenWS, OrdenServicio, true);
                            this.updateRutinaDeOrden(OrdenServicio, OrdenWS.Rutina.RutinaID);
                        }
                            
                        
                        


                        this.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                        this.LimpiaCampos();
                        //oProgress.Stop();
                        bLocked = false;
                         
#endregion
                      
                    }
                }
                else throw new Exception(response);
            }
            catch (Exception ex)
            {
                //if (oProgress != null)
                    //oProgress.Stop();
                bLocked = false;
                KananSAP.Helper.KananSAPHerramientas.LogError("SBO_KF_SolicitaOrdenesView.cs->GuardaOrdenServicio()", ex.Message);
                oTools.ColocarMensaje(string.Format("No se ha podido crear la orden de servicio. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                if (this.oCompany.InTransaction)
                    this.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
            }
        }

        void ActualizaOrdenServicio()
        {
            Int64 codigosiguiente = -1;
            string response = string.Empty;
            OrdenServicio OrdenServicio = new Kanan.Mantenimiento.BO2.OrdenServicio();
            Kanan.Mantenimiento.BO2.OrdenServicio.OrdenServicioJSON OrdenWS = new Kanan.Mantenimiento.BO2.OrdenServicio.OrdenServicioJSON();
            SBO_KF_ORDENSERVICIO_PD controlOS = new SBO_KF_ORDENSERVICIO_PD();
            SBO_KF_OrdenServicio_WS controlwsOS = new SBO_KF_OrdenServicio_WS();
            Add oOUDT = new Add(ref this.oCompany, null);
            //ProgressBar oProgress = null;
            try
            {
                bLocked = true;
                //oProgress = this.SBO_Application.StatusBar.CreateProgressBar("Guardando información", 1, true);
                response = this.FormToEntity(ref OrdenServicio);
                if (response == "OK")
                {
                    this.oCompany.StartTransaction();
                    OrdenServicio.OrdenServicioID = (int?)this.OrdenServicioID;
                    OrdenServicio.Code = (int?)this.OrdenServicioID;
                    #region edicion de tablas relacionadas a orden de servicio
                    if (this.lstmecanicos.Count > 0)
                    {
                        if (this.bEditaMecanicos)
                        {
                            codigosiguiente = -1;
                            oOUDT.LimpiaTablaCode("VSKF_MECANICOSOS", this.OrdenServicioID.ToString(), "U_OrdenServicioID");
                            codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_MECANICOSOS");
                            if (codigosiguiente < 0)
                                throw new Exception("No se ha podido recueperar el folio siguiente para el detalle de mecanicos");
                            foreach (Kanan.Mantenimiento.BO2.SBO_KF_OHEM_INFO omecanico in this.lstmecanicos)
                            {
                                omecanico.U_ORDENSERVICIOID = OrdenServicioID.ToString();
                                response = controlOS.InsertaMecanicos(ref this.oCompany, omecanico, codigosiguiente.ToString());
                                if (response != "OK")
                                    throw new Exception(response);
                                codigosiguiente++;
                            }
                        }
                    }
                    if (this.lstcostoadicional.Count > 0)
                    {
                        if (this.bEditaCosto)
                        {
                            codigosiguiente = -1;
                            oOUDT.LimpiaTablaCode("VSKF_OSCOSTOSADICIO", this.OrdenServicioID.ToString(), "U_Ordenservico");
                            codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_OSCOSTOSADICIO");
                            if (codigosiguiente < 0)
                                throw new Exception("No se ha podido recueperar el folio siguiente para costos adicionales");
                            foreach (SBOKF_CL_CostosAdicional_INFO ocosto in this.lstcostoadicional)
                            {
                                ocosto.Code = OrdenServicioID.ToString();
                                response = controlOS.InsertaCostoAdicional(ref this.oCompany, ocosto, codigosiguiente.ToString());
                                if (response != "OK")
                                    throw new Exception(response);
                                codigosiguiente++;
                            }
                        }
                    }

                    if (this.lstherramientas.Count > 0)
                    {
                        if (this.bEditaHerramientas)
                        {
                            codigosiguiente = -1;
                            oOUDT.LimpiaTablaCode("VSKF_HERRAMIENTAOS", this.OrdenServicioID.ToString(), "U_OrdenServicioID");
                            codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_HERRAMIENTAOS");
                            if (codigosiguiente < 0)
                                throw new Exception("No se ha podido recueperar el folio siguiente para el detalle de herramientas");
                            foreach (SBO_KF_Herramientas_INFO oherramienta in this.lstherramientas)
                            {
                                oherramienta.OrdenServicioID = OrdenServicioID.ToString();
                                response = controlOS.InsertaHerramientas(ref this.oCompany, oherramienta, codigosiguiente.ToString());
                                if (response != "OK")
                                    throw new Exception(response);
                                codigosiguiente++;
                            }
                        }
                    }

                    if (this.lstrefacciones.Count > 0)
                    {
                        //SE VALIDA QUE ESTAMOS EN EL ÚLTIMO ESTATUS (REALIZADA) PARA ACTUALIZAR PRECIOS.

                        if (OrdenServicio.EstatusOrden.EstatusOrdenID == 6)
                        {
                            List<SBO_KF_OITM_INFO> lstResultados = new List<SBO_KF_OITM_INFO>();
                            SBO_KF_REFACCION_PD oRefaccionpd = new SBO_KF_REFACCION_PD();
                            List<DetalleRefaccion> nDr = new List<DetalleRefaccion>();
                            foreach (DetalleRefaccion dR in this.lstrefacciones)
                            {
                                lstResultados = new List<SBO_KF_OITM_INFO>();
                                oRefaccionpd.listaRefacciones(ref oCompany, ref lstResultados, dR.Articulo.ToString(), dR.CodigoAlmacen.ToString());
                                dR.Costo = 0;
                                if (lstResultados.Count > 0)
                                {
                                    //costorefaccion = lstResultados[0].AvgPrice;
                                    dR.Costo = lstResultados[0].AvgPrice;
                                    if (lstResultados[0].AvgPrice == 0)
                                        throw new Exception(String.Format("La refacción {0} no tiene especificado un costo en el almacen.", dR.Articulo));
                                }

                                dR.Total = dR.Cantidad * dR.Costo;
                                nDr.Add(dR);
                            }
                            this.lstrefacciones = nDr;
                            this.bEditaRefaccion = true;
                        }

                        if (this.bEditaRefaccion)
                        {
                            codigosiguiente = -1;
                            oOUDT.LimpiaTablaCode("VSKF_DTLLEREFACCION", this.OrdenServicioID.ToString(), "U_OrdenServicioID");
                            codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_DTLLEREFACCION");
                            if (codigosiguiente < 0)
                                throw new Exception("No se ha podido recueperar el folio siguiente para el detalle de refacciones");
                            foreach (DetalleRefaccion orefaccion in this.lstrefacciones)
                            {
                                orefaccion.Servicio = new Servicio();
                                orefaccion.OrdenServicio = new Kanan.Mantenimiento.BO2.OrdenServicio();
                                orefaccion.OrdenServicio.OrdenServicioID = OrdenServicioID;
                                response = controlOS.InsertaRefaccion(ref this.oCompany, codigosiguiente.ToString(), orefaccion);
                                if (response != "OK")
                                    throw new Exception(response);
                                codigosiguiente++;
                            }
                        }
                    }

                    if (this.lstservicios.Count > 0)
                    {
                        if (this.bEditaServicio)
                        {
                            codigosiguiente = -1;
                            oOUDT.LimpiaTablaCode("VSKF_DETALLEORDEN", this.OrdenServicioID.ToString(), "U_OrdenServicioID");
                            codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_DETALLEORDEN");
                            if (codigosiguiente < 0)
                                throw new Exception("No se ha podido recueperar el folio siguiente para el detalle de orden");
                            foreach (DetalleOrden odetalle in this.lstservicios)
                            {
                                odetalle.OrdenServicio = new Kanan.Mantenimiento.BO2.OrdenServicio();
                                odetalle.OrdenServicio.OrdenServicioID = OrdenServicioID;
                                odetalle.ConsecutivoID = Convert.ToInt32(codigosiguiente);
                                response = controlOS.InsertaDetalleOrdenServicio(ref this.oCompany, odetalle);
                                if (response != "OK")
                                    throw new Exception(response);
                                codigosiguiente++;
                            }
                        }
                    }
                    #endregion edicion de tablas relacionadas a orden de servicio
                    OrdenServicio.OrdenServicioID = Convert.ToInt32(OrdenServicoWS);

                    //SE VALIDA EL ESTATUS PARA ACTUALIZAR LAS FECHAS Y HORAS DE SALIDA Y ENTRADAS REALES
                    switch (Convert.ToInt32(OrdenServicio.EstatusOrden.EstatusOrdenID))
                    {

                        case 5:

                            SBO_KF_ALERTA_PD oAlertaCtrl = new SBO_KF_ALERTA_PD();
                            List<SBO_KF_ALERTA_INFO> oAlertas = new List<SBO_KF_ALERTA_INFO>();
                            SBO_KF_ALERTA_INFO oAlerta = new SBO_KF_ALERTA_INFO();
                            string respuesta = "";
                             if (this.lstservicios.Count > 0){
                                 foreach(DetalleOrden oDetOrd in this.lstservicios)
                                 {
                                     respuesta= oAlertaCtrl.lstadoAlertasADesactivar(ref this.oCompany, ref oAlertas,  (int)OrdenServicio.OrdenServicioID, (int)oDetOrd.MantenibleID);
                                     foreach(SBO_KF_ALERTA_INFO oA in oAlertas)
                                     {
                                         respuesta = oAlertaCtrl.desactivaAlerta(ref this.oCompany, oA);
                                     }

                                 }
                                 
                             }

                            OrdenServicio.FechaRecepcionReal = DateTime.Now;
                            OrdenServicio.FechaLiberacionReal = DateTime.Now;
                            break;
                        case 6:
                            OrdenServicio.FechaLiberacionReal = DateTime.Now;
                            

                             
                            break;
                    }

                    response = controlOS.ActualizaOrdenServicio(ref this.oCompany, OrdenServicio);

                    #region enviar informacion a webservice & actualizar tablas con identificadores desde web
                    this.lstherramientas.ForEach(x => x.SucursalID = OrdenServicio.SucursalOrden.SucursalID);
                    this.lstservicios.ForEach(x => x.DetalleOrdenID = Convert.ToInt32(OrdenServicoWS));
                    this.lstcostoadicional.ForEach(x => x.DetalleOrdenID = Convert.ToInt32(OrdenServicoWS));
                    this.lstherramientas.ForEach(x => x.DetalleOrdenID = Convert.ToInt32(OrdenServicoWS));
                    this.lstmecanicos.ForEach(x => x.DetalleOrdenID = Convert.ToInt32(OrdenServicoWS));
                    this.lstrefacciones.ForEach(x => x.DetalleRefaccionID = Convert.ToInt32(OrdenServicoWS));
                    this.lstrefacciones.ForEach(x=>x.SucursalID  = OrdenServicio.SucursalOrden.SucursalID);

                    OrdenServicio.DetalleOrden = this.lstservicios;
                    OrdenServicio.CostosAdicionalesDto = this.lstcostoadicional;
                    OrdenServicio.HerramientasDto = this.lstherramientas;
                    OrdenServicio.EmpleadosDto = this.lstmecanicos;
                    OrdenServicio.RefaccionesDto = this.lstrefacciones;
                    OrdenServicio.bEditaServicio = bEditaServicio;
                    OrdenServicio.bEditaRefaccion = bEditaRefaccion;
                    OrdenServicio.bEditaCosto = bEditaCosto;
                    OrdenServicio.bEditaMecanicos = bEditaMecanicos;
                    OrdenServicio.bEditaHerramientas = bEditaHerramientas;
                    

                    response = controlwsOS.GestionOrdenesWebAPI(GestionGlobales.sURLWs, ref OrdenWS, OrdenServicio, true);
                    if (response != "OK")
                        throw new Exception(response);
                    //actualiza identificadores de WS

                    #endregion enviar informacion a webservice & actualizar tablas con identificadores desde web

                    OrdenServicio.OrdenServicioID = OrdenServicioID;
                    this.InsertaBitacora((int)OrdenServicio.EstatusOrden.EstatusOrdenID, OrdenServicioID.ToString(), OrdenServicoWS);
                    this.CreaDocumentoSBO(OrdenServicio);
                    oTools.ColocarMensaje("La orden de servicio se ha actualizado.", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    this.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                    //oProgress.Stop();
                    bLocked = false;
                    this.SBO_Application.Forms.ActiveForm.Close();

                }
                else throw new Exception(response);
                
            }
            catch (Exception ex)
            {
                //if (oProgress != null)
                //    oProgress.Stop();
                bLocked = false;
                KananSAP.Helper.KananSAPHerramientas.LogError("SBO_KF_SolicitaOrdenesView.cs->ActualizaOrdenServicio()", ex.Message);
                oTools.ColocarMensaje(string.Format("No se ha podido actualizar la información. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                if (this.oCompany.InTransaction)
                    this.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
            }
        }

        void creaRutinaDeOrden(OrdenServicio OrdenServicio, int RutinaID)
        {
            try
            {
                SBO_KF_PLANTILLA_PD controlPlatilla = new SBO_KF_PLANTILLA_PD();
                Int64 codigosiguiente = -1;
                Add oOUDT = new Add(ref this.oCompany, null);
                string response = string.Empty;

                //codigoPlantilla = oOUDT.recuperIDMaximoTabla("VSKF_OSPLANTILLA");
                //if (codigoPlantilla < 0)
                //    throw new Exception("No se ha podido asignar el folio a la plantilla");
                OrdenServicio oPlantillaOS = OrdenServicio;
                oPlantillaOS.Code = Convert.ToInt32(RutinaID);
                oPlantillaOS.Name = OrdenServicio.RutinaNombre.Trim().Length > 30 ? OrdenServicio.RutinaNombre.Substring(0, 30) : OrdenServicio.RutinaNombre.Trim();
                if(OrdenServicio.ObjetoRutina !=null  && OrdenServicio.ObjetoRutina.TipoVehiculoID !=null)
                    oPlantillaOS.ObjetoRutina.TipoVehiculoID = OrdenServicio.ObjetoRutina.TipoVehiculoID;
                //oPlantillaOS.RutinaID = RutinaID;
                response = controlPlatilla.InsertaPlantilla(ref this.oCompany, oPlantillaOS);
                if (response != "OK")
                    throw new Exception(response);
                #region inserta en tablas asociadas a la plantilla
                if (this.lstmecanicos.Count > 0)
                {
                    codigosiguiente = -1;
                    codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_PLANTMECANICOS");
                    if (codigosiguiente < 0)
                        throw new Exception("No se ha podido asignar el folio a la plantilla mecanicos");
                    foreach (Kanan.Mantenimiento.BO2.SBO_KF_OHEM_INFO omecanico in this.lstmecanicos)
                    {
                        omecanico.Code = codigosiguiente.ToString();
                        omecanico.OrdeServicioID = RutinaID.ToString();
                        response = controlPlatilla.InsertaMecanico(ref this.oCompany, omecanico);
                        if (response != "OK")
                            throw new Exception(response);
                        codigosiguiente++;

                    }
                }
                if (this.lstcostoadicional.Count > 0)
                {
                    codigosiguiente = -1;
                    codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_PLANTCOSTOSAD");
                    if (codigosiguiente < 0)
                        throw new Exception("No se ha podido asignar el folio a la plantilla para costos adicionales");
                    foreach (SBOKF_CL_CostosAdicional_INFO ocosto in this.lstcostoadicional)
                    {
                        ocosto.Code = codigosiguiente.ToString();
                        ocosto.U_ORDENSERVICO = RutinaID.ToString();
                        response = controlPlatilla.InsertaCostos(ref this.oCompany, ocosto);
                        if (response != "OK")
                            throw new Exception(response);
                        codigosiguiente++;
                    }

                }

                if (this.lstherramientas.Count > 0)
                {
                    codigosiguiente = -1;
                    codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_PLANTHERRAMIEN");
                    if (codigosiguiente < 0)
                        throw new Exception("No se ha podido asignar el folio a la plantilla para el detalle de herramientas");
                    foreach (SBO_KF_Herramientas_INFO oherramienta in this.lstherramientas)
                    {
                        oherramienta.Code = codigosiguiente.ToString();
                        oherramienta.OrdenServicioID = RutinaID.ToString();
                        response = controlPlatilla.InsertaHerramienta(ref this.oCompany, oherramienta);
                        if (response != "OK")
                            throw new Exception("No se ha podido actualizar el detalle de herramientas");
                        codigosiguiente++;
                    }

                }

                if (this.lstrefacciones.Count > 0)
                {
                    codigosiguiente = -1;
                    codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_PLANTREFACCION");
                    if (codigosiguiente < 0)
                        throw new Exception("No se ha podido asignar el folio a la plantilla para el detalle de refacciones");
                    foreach (DetalleRefaccion orefaccion in this.lstrefacciones)
                    {
                        orefaccion.Code = codigosiguiente.ToString();
                        orefaccion.OrdenServicio = new OrdenServicio();
                        orefaccion.OrdenServicio.OrdenServicioID = Convert.ToInt32(RutinaID);
                        response = controlPlatilla.InsertaRefaccion(ref this.oCompany, orefaccion);
                        if (response != "OK")
                            throw new Exception(response);
                        codigosiguiente++;

                    }
                }

                if (this.lstservicios.Count > 0)
                {
                    codigosiguiente = -1;
                    codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_OSPLANTILLADET");
                    if (codigosiguiente < 0)
                        throw new Exception("No se ha podido asignar el folio a la plantilla para el servicio");
                    foreach (DetalleOrden odetalle in this.lstservicios)
                    {
                        odetalle.Code = codigosiguiente.ToString();
                        odetalle.OrdenServicio = new Kanan.Mantenimiento.BO2.OrdenServicio();
                        odetalle.OrdenServicio.OrdenServicioID = Convert.ToInt32(RutinaID);
                        response = controlPlatilla.InsertaServicio(ref this.oCompany, odetalle);
                        if (response != "OK")
                            throw new Exception(response);
                        codigosiguiente++;
                    }
                }
                #endregion inserta en tablas asociadas a la plantilla


            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("SBO_KF_SolicitaOrdenesView.cs->creaRutinaDeOrden()", ex.Message);
                //throw new Exception(string.Format("La rutina no se ha podido crear. {0}", ex.Message));
            }
        }

        void updateRutinaDeOrden(OrdenServicio OrdenServicio, int RutinaID)
        {
            try
            {
                SBO_KF_PLANTILLA_PD controlPlatilla = new SBO_KF_PLANTILLA_PD();
                Int64 codigosiguiente = -1;
                Add oOUDT = new Add(ref this.oCompany, null);
                string response = string.Empty;
                string PlantillaPadre = "";
                //codigoPlantilla = oOUDT.recuperIDMaximoTabla("VSKF_OSPLANTILLA");
                //if (codigoPlantilla < 0)
                //    throw new Exception("No se ha podido asignar el folio a la plantilla");
                OrdenServicio oPlantillaOS = OrdenServicio;
                oPlantillaOS.Code = Convert.ToInt32(RutinaID);
                oPlantillaOS.Name = OrdenServicio.ObjetoRutina.RutinaNombre.Trim().Length > 30 ? OrdenServicio.ObjetoRutina.RutinaNombre.Substring(0, 30) : OrdenServicio.ObjetoRutina.RutinaNombre.Trim();
                //oPlantillaOS.RutinaID = RutinaID;

                PlantillaPadre = oPlantillaOS.Code.ToString();

                if (OrdenServicio.ObjetoRutina != null && OrdenServicio.ObjetoRutina.TipoVehiculoID != null)
                    oPlantillaOS.ObjetoRutina.TipoVehiculoID = OrdenServicio.ObjetoRutina.TipoVehiculoID;

                response = controlPlatilla.UpdatePlantilla(ref this.oCompany, oPlantillaOS);
                if (response != "OK")
                    throw new Exception(response);
                #region inserta en tablas asociadas a la plantilla
                if (this.lstmecanicos.Count > 0)
                {
                    codigosiguiente = -1;
                    codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_PLANTMECANICOS");
                    if (codigosiguiente < 0)
                        throw new Exception("No se ha podido asignar el folio a la plantilla mecanicos");
                    //SE QUITAN LOS QUE YA ESTÁN
                     
                    List<Kanan.Mantenimiento.BO2.SBO_KF_OHEM_INFO> lstlPlamecanicosRegistrados = new List<Kanan.Mantenimiento.BO2.SBO_KF_OHEM_INFO>();
                    controlPlatilla.listaMecanicos(ref oCompany,  ref  lstlPlamecanicosRegistrados, PlantillaPadre);
                    foreach (Kanan.Mantenimiento.BO2.SBO_KF_OHEM_INFO omecanico in lstlPlamecanicosRegistrados)
                    {
                        response = controlPlatilla.RemoveMecanico(ref this.oCompany, omecanico);
                    }

                    foreach (Kanan.Mantenimiento.BO2.SBO_KF_OHEM_INFO omecanico in this.lstmecanicos)
                    {
                        omecanico.OrdeServicioID = RutinaID.ToString();
                        omecanico.Code = codigosiguiente.ToString();
                        response = controlPlatilla.InsertaMecanico(ref this.oCompany, omecanico);
                        if (response != "OK")
                            throw new Exception(response);
                        codigosiguiente++;

                    }
                }
                if (this.lstcostoadicional.Count > 0)
                {
                    codigosiguiente = -1;
                    codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_PLANTCOSTOSAD");
                    if (codigosiguiente < 0)
                        throw new Exception("No se ha podido asignar el folio a la plantilla para costos adicionales");

                    List<SBOKF_CL_CostosAdicional_INFO> lstlPlacostosRegistrados = new List<SBOKF_CL_CostosAdicional_INFO>();
                    controlPlatilla.listaCostos(ref oCompany, ref  lstlPlacostosRegistrados, PlantillaPadre);
                    foreach (Kanan.Mantenimiento.BO2.SBOKF_CL_CostosAdicional_INFO ocosto in lstlPlacostosRegistrados)
                    {
                        response = controlPlatilla.RemoveCostos(ref this.oCompany, ocosto);
                    }

                    foreach (SBOKF_CL_CostosAdicional_INFO ocosto in this.lstcostoadicional)
                    {
                        ocosto.Code = codigosiguiente.ToString();
                        ocosto.U_ORDENSERVICO = RutinaID.ToString();
                        response = controlPlatilla.InsertaCostos(ref this.oCompany, ocosto);
                        if (response != "OK")
                            throw new Exception(response);
                        codigosiguiente++;
                    }

                }

                if (this.lstherramientas.Count > 0)
                {
                    codigosiguiente = -1;
                    codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_PLANTHERRAMIEN");
                    if (codigosiguiente < 0)
                        throw new Exception("No se ha podido asignar el folio a la plantilla para el detalle de herramientas");

                    List<SBO_KF_Herramientas_INFO> lstlPlaherramientaRegistrados = new List<SBO_KF_Herramientas_INFO>();
                    controlPlatilla.listaHerramientas(ref oCompany, ref  lstlPlaherramientaRegistrados, PlantillaPadre);
                    foreach (Kanan.Mantenimiento.BO2.SBO_KF_Herramientas_INFO oherramienta in lstlPlaherramientaRegistrados)
                    {
                        response = controlPlatilla.RemoveHerramienta(ref this.oCompany, oherramienta);
                    }


                    foreach (SBO_KF_Herramientas_INFO oherramienta in this.lstherramientas)
                    {
                        oherramienta.Code = codigosiguiente.ToString();
                        oherramienta.OrdenServicioID = RutinaID.ToString();
                        response = controlPlatilla.InsertaHerramienta(ref this.oCompany, oherramienta);
                        if (response != "OK")
                            throw new Exception("No se ha podido actualizar el detalle de herramientas");
                        codigosiguiente++;
                    }

                }

                if (this.lstrefacciones.Count > 0)
                {
                    codigosiguiente = -1;
                    codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_PLANTREFACCION");
                    if (codigosiguiente < 0)
                        throw new Exception("No se ha podido asignar el folio a la plantilla para el detalle de refacciones");

                    List<DetalleRefaccion> lstlPlarefaccionRegistrados = new List<DetalleRefaccion>();
                    controlPlatilla.listaRefacciones(ref oCompany, ref  lstlPlarefaccionRegistrados, PlantillaPadre);
                    foreach (DetalleRefaccion oherramienta in lstlPlarefaccionRegistrados)
                    {
                        response = controlPlatilla.RemoveRefaccion(ref this.oCompany, oherramienta);
                    }

                    foreach (DetalleRefaccion orefaccion in this.lstrefacciones)
                    {
                        orefaccion.Code = codigosiguiente.ToString();
                        orefaccion.OrdenServicio = new OrdenServicio();
                        orefaccion.OrdenServicio.OrdenServicioID = Convert.ToInt32(RutinaID);
                        response = controlPlatilla.InsertaRefaccion(ref this.oCompany, orefaccion);
                        if (response != "OK")
                            throw new Exception(response);
                        codigosiguiente++;

                    }
                }

                if (this.lstservicios.Count > 0)
                {
                    codigosiguiente = -1;
                    codigosiguiente = oOUDT.recuperIDMaximoTabla("VSKF_OSPLANTILLADET");
                    if (codigosiguiente < 0)
                        throw new Exception("No se ha podido asignar el folio a la plantilla para el servicio");

                    List<DetalleOrden> lstlPlaDetalleRegistrados = new List<DetalleOrden>();
                    controlPlatilla.listaServicios(ref oCompany, ref  lstlPlaDetalleRegistrados, PlantillaPadre);
                    foreach (Kanan.Mantenimiento.BO2.DetalleOrden oservicio in lstlPlaDetalleRegistrados)
                    {
                        response = controlPlatilla.RemoveServicio(ref this.oCompany, oservicio);
                    }


                    foreach (DetalleOrden odetalle in this.lstservicios)
                    {
                        odetalle.Code = codigosiguiente.ToString();
                        odetalle.OrdenServicio = new Kanan.Mantenimiento.BO2.OrdenServicio();
                        odetalle.OrdenServicio.OrdenServicioID = Convert.ToInt32(RutinaID);
                        response = controlPlatilla.InsertaServicio(ref this.oCompany, odetalle);
                        if (response != "OK")
                            throw new Exception(response);
                        codigosiguiente++;
                    }
                }
                #endregion inserta en tablas asociadas a la plantilla


            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("SBO_KF_SolicitaOrdenesView.cs->creaRutinaDeOrden()", ex.Message);
                //throw new Exception(string.Format("La rutina no se ha podido crear. {0}", ex.Message));
            }
        }

        void CreaDocumentoSBO(OrdenServicio OrdenServicio)
        {
            string response = string.Empty, DocEntry = string.Empty, consultaSql = string.Empty, NumAtCard = string.Empty, serviciosID = string.Empty, CentrodeCosto = string.Empty, CardCode = string.Empty;
            bool bDraft = false;
            List<SBO_KF_VEHICULOS_INFO> lstVehiculos = new List<SBO_KF_VEHICULOS_INFO>();  //INMSO
            List<SBO_KF_OCRD_INFO> lstProveedores = new List<SBO_KF_OCRD_INFO>();
            List<SBO_KF_SUCURSAL_INFO> lstSucursal = new List<SBO_KF_SUCURSAL_INFO>();
            SBO_KF_Documents_PD controlSBO = new SBO_KF_Documents_PD();
            SBO_KF_Configuracion_PD controlConnfig = new SBO_KF_Configuracion_PD();
            SBO_KF_ORDENSERVICIO_PD controlOS = new SBO_KF_ORDENSERVICIO_PD();
            SBO_KF_VEHICULOS_PD controlVehiculo = new SBO_KF_VEHICULOS_PD();
            SBO_KF_SUCURSALES_PD controlSucursal = new SBO_KF_SUCURSALES_PD();
            SBO_KF_OCRD_PD controlProveedor = new SBO_KF_OCRD_PD();
            List<SBO_KF_OITM_INFO> lstArt = new List<SBO_KF_OITM_INFO>();
            string DIM1 = "";
            string DIM2 = "";
            string DIM3 = "";
            string DIM4 = "";
            NumAtCard = string.Format(@"{0}/{1}", OrdenServicio.FolioOrden.Folio, OrdenServicio.FolioOrden.AnioFolio);

            if (this.lstservicios.Count > 0)
            {

                
                response = controlVehiculo.listaVehiculos(ref this.oCompany, ref lstVehiculos, Convert.ToInt32(lstservicios[0].Vehiculo.VehiculoID));
                //oTools.ColocarMensaje(String.Format("CODIGO DEL VEHÍCULO: {0}", lstVehiculos[0].Code), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                new SBO_KF_OITM_AD().buscaArticulo(ref this.oCompany, ref lstArt, ItemCodeIN: "'" + lstVehiculos[0].Code + "'");
                DIM1 = lstArt[0].U_DIM1;
                DIM2 = lstArt[0].U_DIM2;
                DIM3 = lstArt[0].U_DIM3;
                DIM4 = lstArt[0].U_DIM4;


            }

            


            if (this.oSetting.EstatusOS == OrdenServicio.EstatusOrden.EstatusOrdenID && !OrdenServicio.TallerInterno)
            {
                #region crea documento de factura de proveedor segun el estatus de la orden de servicio vs la configuracion
                response = controlProveedor.listaProveedores(ref oCompany, ref lstProveedores, Convert.ToInt32(OrdenServicio.ProveedorServicio.ProveedorID));
                if (response == "OK")
                {
                    bDraft = oSetting.DraftOS == 1;
                    CardCode = lstProveedores[0].Code;
                    response = SAPinterface.Data.tools.tools.DocumentoB1Creado(ref this.oCompany, out DocEntry, out consultaSql, bDraft ? "ODRF" : "OPCH", NumAtCard: bDraft ? "" : NumAtCard, FolioKF: OrdenServicio.OrdenServicioID.ToString());
                    if (response.Equals("SIN RESULTADOS", StringComparison.InvariantCultureIgnoreCase))
                    {
                        SBO_KF_DOCUMENTS_INFO oInvoiceService = new SBO_KF_DOCUMENTS_INFO();
                        string sComments = string.Format("Factura creada desde addon Kanan Fleet {0}", OrdenServicio.Descripcion.Trim().ToString());
                        oInvoiceService.bDraftType = oSetting.DraftOS == 1;
                        oInvoiceService.DocDueDate = Convert.ToDateTime(OrdenServicio.Fecha);
                        oInvoiceService.TaxDate = Convert.ToDateTime(OrdenServicio.Fecha);
                        oInvoiceService.DocDate = Convert.ToDateTime(OrdenServicio.Fecha);
                        oInvoiceService.NumAtCard = NumAtCard;
                        oInvoiceService.Comments = sComments.Length > 254 ? sComments.Substring(0, 254) : sComments;
                        oInvoiceService.U_KF_FOLIOKF = OrdenServicio.OrdenServicioID.ToString();
                        oInvoiceService.CardCode = CardCode;
                        oInvoiceService.Branch = SAPinterface.Data.tools.tools.UtilizaBranchs(ref oCompany);
                        oInvoiceService.DocumentLines = new List<SBO_KF_DOCUMENTS_LINE_INFO>();
                        foreach (DetalleOrden oServicio in this.lstservicios)
                        {
                            int BranchID = -1;
                            lstVehiculos = new List<SBO_KF_VEHICULOS_INFO>();
                            List<DetalleOrden> lstServicioAuto = new List<DetalleOrden>();
                            SBO_KF_DOCUMENTS_LINE_INFO oDocLine = new SBO_KF_DOCUMENTS_LINE_INFO();
                            response = controlOS.listaServiciosAutomotriz(ref this.oCompany, ref lstServicioAuto, oServicio.Servicio.ServicioID.ToString());
                            if (response != "OK")
                                throw new Exception("No se han definido las cuentas para los servicios");
                            if (string.IsNullOrEmpty(lstServicioAuto[0].U_CUENTACONTABLE))
                                throw new Exception("No se han definido las cuentas para los servicios");
                            response = controlConnfig.recuperaCentrodeCosto(ref this.oCompany, Convert.ToInt32(OrdenServicio.SucursalOrden.SucursalID), Convert.ToInt32(oServicio.Vehiculo.VehiculoID), CardCode, out CentrodeCosto, out BranchID);
                            if (response != "OK" && string.IsNullOrEmpty(CentrodeCosto))
                                throw new Exception("No se ha definido el centro de costo para emitir la salida de refacciones");

                            response = controlVehiculo.listaVehiculos(ref this.oCompany, ref lstVehiculos, Convert.ToInt32(oServicio.Vehiculo.VehiculoID));
                            if (response != "OK")
                                throw new Exception("No se ha encontrado la información del vehiculo");

                            oInvoiceService.BPL_IDAssignedToInvoice = BranchID;
                            
                            

                            oDocLine.U_DIM1 = DIM1;
                            oDocLine.U_DIM2 = DIM2;
                            oDocLine.U_DIM3 = DIM3;
                            oDocLine.U_DIM4 = DIM4;

                            oDocLine.U_VSKF_VH = lstVehiculos[0].U_PLACA;
                            oDocLine.U_VSKF_VHNOECO = lstVehiculos[0].Name;
                            oDocLine.AccountCode = lstServicioAuto[0].U_CUENTACONTABLE;
                            oDocLine.CostingCode = CentrodeCosto;
                            oDocLine.ItemDescription = oServicio.Servicio.Nombre;
                            oDocLine.LineTotal = Convert.ToDouble(oServicio.Costo);
                            oDocLine.Quantity = 1;
                            oDocLine.Dimension = oSetting.DimCode;
                            oInvoiceService.DocumentLines.Add(oDocLine);
                        }
                        response = controlSBO.oPurchaseInvoiceServiceDocumentSBO(ref this.oCompany, oInvoiceService, out DocEntry);
                        //oTools.ColocarMensaje(String.Format("RESPUESTA DE LA GENERACIÓN DE FACTURAS: {0}",response), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                        if (response != "OK" && string.IsNullOrEmpty(DocEntry))
                            throw new Exception(string.Format("Factura de servicio no creada. ERROR {0}", response));
                    }
                }
                else throw new Exception("No se ha encontrado el proveedor asignado para la orden de servicio");
                #endregion crea documento de factura de proveedor segun el estatus de la orden de servicio vs la configuracion
            }

            if (OrdenServicio.EstatusOrden.EstatusOrdenID == 5)
            {
                if (OrdenServicio.FacturableCliente == 1)
                {
                    lstProveedores = new List<SBO_KF_OCRD_INFO>();
                    response = controlProveedor.listaProveedores(ref oCompany, ref lstProveedores, Convert.ToInt32(OrdenServicio.ProveedorServicio.ProveedorID));
                    if (response == "OK")
                    {
                        CardCode = lstProveedores[0].Code;
                        #region crea factura de cliente segun criterio de la orden de servicio
                        if (string.IsNullOrEmpty(OrdenServicio.socioCliente))
                            throw new Exception("No se ha determinado el cliente para la factura");

                        response = SAPinterface.Data.tools.tools.DocumentoB1Creado(ref this.oCompany, out DocEntry, out consultaSql, "OINV", NumAtCard: NumAtCard, FolioKF: string.Format("FC_{0}", OrdenServicio.OrdenServicioID.ToString()));
                        if (response == "SIN RESULTADOS")
                        {
                            string comments = string.Format("Factura de cliente para orden de servicio, creada desde AddON KF en fecha {0}", DateTime.Now.ToString("dd/MM/yyyy"));
                            SBO_KF_DOCUMENTS_INFO oInvoice = new SBO_KF_DOCUMENTS_INFO();
                            oInvoice.CardCode = OrdenServicio.socioCliente;
                            oInvoice.NumAtCard = NumAtCard;
                            oInvoice.DocDate = DateTime.Now;
                            oInvoice.DocDueDate = DateTime.Now;
                            oInvoice.TaxDate = DateTime.Now;
                            oInvoice.Branch = SAPinterface.Data.tools.tools.UtilizaBranchs(ref oCompany);
                            oInvoice.Comments = comments.Length > 254 ? comments.Substring(0, 254) : comments;
                            oInvoice.U_KF_FOLIOKF = OrdenServicio.OrdenServicioID.ToString();
                            oInvoice.DocumentLines = new List<SBO_KF_DOCUMENTS_LINE_INFO>();
                            lstSucursal = new List<SBO_KF_SUCURSAL_INFO>();
                            response = controlSucursal.listaSucursales(ref this.oCompany, ref lstSucursal, Convert.ToInt32(OrdenServicio.SucursalOrden.SucursalID));
                            if (response != "OK")
                                throw new Exception("No se encuentra el almacen para la factura de cliente");
                            
                            //response = controlVehiculo.listaVehiculos(ref this.oCompany, ref lstVehiculos, Convert.ToInt32(oServicio.Vehiculo.VehiculoID));

                            #region se agregan al documento los servicios
                            foreach (DetalleOrden oServicio in this.lstservicios)
                            {
                                SBO_KF_DOCUMENTS_LINE_INFO oDocLine = new SBO_KF_DOCUMENTS_LINE_INFO();
                                lstVehiculos = new List<SBO_KF_VEHICULOS_INFO>();
                                List<DetalleOrden> lstServicioAuto = new List<DetalleOrden>();
                                oDocLine.Dimension = this.oSetting.DimCode;
                                response = controlOS.listaServiciosAutomotriz(ref this.oCompany, ref lstServicioAuto, oServicio.Servicio.ServicioID.ToString());
                                if (response != "OK")
                                    throw new Exception("No se han definido el dato maestro para los servicios");

                                //response = controlVehiculo.listaVehiculos(ref this.oCompany, ref lstVehiculos, Convert.ToInt32(oServicio.Vehiculo.VehiculoID));
                                if (response != "OK")
                                    throw new Exception("No se ha encontrado la información del vehiculo");

                                oDocLine.U_VSKF_VH = lstVehiculos[0].U_PLACA;
                                oDocLine.U_VSKF_VHNOECO = lstVehiculos[0].Name;
                                oDocLine.ItemCode = lstServicioAuto[0].ItemCode;
                                oDocLine.UnitPrice = Convert.ToDouble(oServicio.Costo);
                                oDocLine.U_DIM1 = DIM1;
                                oDocLine.U_DIM2 = DIM2;
                                oDocLine.U_DIM3 = DIM3;
                                oDocLine.U_DIM4 = DIM4;

                                if (oServicio.Costo <= 0)
                                    throw new Exception("Determina el costo de los servicios");
                                oDocLine.Quantity = 1;
                                oDocLine.WarehouseCode = lstSucursal[0].U_ALMACEN;
                                oInvoice.BPL_IDAssignedToInvoice = 1;
                                oInvoice.DocumentLines.Add(oDocLine);
                            }
                            #endregion se agregan al documento los servicios

                            #region Si corresponde a los criterios se facturan las refacciones
                            if (this.oSetting.cSBO == 'Y' && this.oSetting.cAlmacen == 'Y')
                            {
                                foreach (DetalleRefaccion oLine in this.lstrefacciones)
                                {
                                    int BranchID = -1;
                                    SBO_KF_DOCUMENTS_LINE_INFO oDocLine = new SBO_KF_DOCUMENTS_LINE_INFO();
                                    oDocLine.Dimension = this.oSetting.DimCode;
                                    oDocLine.ItemCode = oLine.Articulo;
                                    oDocLine.Quantity = Convert.ToDouble(oLine.Cantidad);
                                    oDocLine.UnitPrice = Convert.ToDouble(oLine.Costo);
                                    if (oDocLine.Quantity <= 0)
                                        throw new Exception("Todas la líneas deben tener cantidad de herramientas");

                                    oDocLine.WarehouseCode = oLine.CodigoAlmacen;
                                    response = controlConnfig.recuperaCentrodeCosto(ref this.oCompany, Convert.ToInt32(OrdenServicio.SucursalOrden.SucursalID), Convert.ToInt32(this.lstservicios[0].Vehiculo.VehiculoID), CardCode, out CentrodeCosto, out BranchID);
                                    if (response != "OK" && string.IsNullOrEmpty(CentrodeCosto))
                                        throw new Exception("No se ha definido el centro de costo para emitir la salida de refacciones");
                                    oInvoice.BPL_IDAssignedToInvoice = BranchID;
                                    oDocLine.CostingCode = CentrodeCosto;
                                    oInvoice.DocumentLines.Add(oDocLine);
                                }
                            }
                            #endregion Si corresponde a los criterios se facturan las refacciones

                            response = controlSBO.oInvoiceDocumentSBO(ref this.oCompany, oInvoice, out DocEntry);
                            if (response != "OK" && string.IsNullOrEmpty(DocEntry))
                                throw new Exception(string.Format("Factura de cliente no creada. ERROR {0}", response));
                        }
                        #endregion crea factura de cliente segun criterio de la orden de servicio
                    }
                }
            }


            


            //CONDICIÓN ORIGINAL QUE SEA EN EL ESTATOS REALIZADA  = 6
            //if (OrdenServicio.EstatusOrden.EstatusOrdenID == 6)
            //CONDICIÓN MODIFICADA QUE ESA EN EL ESTATUS APROBADA = 2
            if (OrdenServicio.EstatusOrden.EstatusOrdenID >= this.oSetting.EstatusSALIDA)
            {
                lstProveedores = new List<SBO_KF_OCRD_INFO>();
                response = controlProveedor.listaProveedores(ref oCompany, ref lstProveedores, Convert.ToInt32(OrdenServicio.ProveedorServicio.ProveedorID));
                if (response == "OK")
                {
                    //oTools.ColocarMensaje("Si existe el proveedor", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    CardCode = lstProveedores[0].Code;
                    //oTools.ColocarMensaje( string.Format("sSBO: {0}, cAlmacen: {1}", this.oSetting.cSBO, this.oSetting.cAlmacen), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    //if (this.oSetting.cSBO == 'Y' && this.oSetting.cAlmacen == 'Y')
                    if (this.oSetting.cSBO == 'Y' )
                    {
                        //crear una salida de inventario sobre refacciones
                        //oTools.ColocarMensaje("Se comprpobará si existe un documento", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                        #region salida de mercancia de las refacciones utilizadas

                        if(oSetting.DraftSALIDA ==1)//SI ES DRAFT BUSCO EN ODRF, SINO BUSCO EN OIGE
                            response = SAPinterface.Data.tools.tools.DocumentoB1CreadoTipoDocumento(ref this.oCompany, out DocEntry, out consultaSql, "ODRF", FolioKF: OrdenServicioID.ToString(), typeOfDocument:"60");
                        else
                            response = SAPinterface.Data.tools.tools.DocumentoB1Creado(ref this.oCompany, out DocEntry, out consultaSql, "OIGE", FolioKF: OrdenServicioID.ToString());

                        if (response == "SIN RESULTADOS" && string.IsNullOrEmpty(DocEntry))
                        {
                            //oTools.ColocarMensaje("No existió", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                            SBO_KF_DOCUMENTS_INFO oInventory = new SBO_KF_DOCUMENTS_INFO();
                            oInventory.TaxDate = Convert.ToDateTime(OrdenServicio.Fecha);
                            oInventory.DocDate = Convert.ToDateTime(OrdenServicio.Fecha);
                            oInventory.Reference2 = string.Format(@"{0}/{1}", OrdenServicio.FolioOrden.Folio, OrdenServicio.FolioOrden.AnioFolio);
                            oInventory.Comments = string.Format(@"Salida de mercancía realizada desde Addon Kanan Fleet en fecha {0} ", DateTime.Now.ToString("dd/MM/yyyy"));
                            oInventory.U_KF_FOLIOKF = OrdenServicio.OrdenServicioID.ToString();
                            oInventory.Branch = SAPinterface.Data.tools.tools.UtilizaBranchs(ref oCompany);
                            //CONDICIÓN MODIFICADA QUE ESA EN EL ESTATUS APROBADA = 2 Y EN VERSIÓN DRAFT
                            oInventory.bDraftType = oSetting.DraftSALIDA == 1  ; 
                            oInventory.DocumentLines = new List<SBO_KF_DOCUMENTS_LINE_INFO>();
                            foreach (DetalleRefaccion oLine in this.lstrefacciones)
                            {
                                SBO_KF_DOCUMENTS_LINE_INFO oDocLine = new SBO_KF_DOCUMENTS_LINE_INFO();
                                int BranchID = -1;
                                oDocLine.Dimension = this.oSetting.DimCode;
                                oDocLine.ItemCode = oLine.Articulo;
                                oDocLine.Quantity = Convert.ToDouble(oLine.Cantidad);
                                //CAMPOS AGREGADOS POR INMSO*/
                                oDocLine.U_DIM1 = DIM1;
                                oDocLine.U_DIM2 = DIM2;
                                oDocLine.U_DIM3 = DIM3;
                                oDocLine.U_DIM4 = DIM4;

                                if (oDocLine.Quantity <= 0)
                                    throw new Exception("Todas la líneas deben tener cantidad de herramientas");
                                oDocLine.WarehouseCode = oLine.CodigoAlmacen;
                                response = controlConnfig.recuperaCentrodeCosto(ref this.oCompany, Convert.ToInt32(OrdenServicio.SucursalOrden.SucursalID), Convert.ToInt32(this.lstservicios[0].Vehiculo.VehiculoID), CardCode, out CentrodeCosto, out BranchID);
                                if (response != "OK" && string.IsNullOrEmpty(CentrodeCosto))
                                    throw new Exception("No se ha definido el centro de costo para emitir la salida de refacciones");
                                oInventory.BPL_IDAssignedToInvoice = BranchID;
                                oDocLine.CostingCode = CentrodeCosto;
                                
                                oInventory.DocumentLines.Add(oDocLine);

                               

                            }
                            if (lstrefacciones.Count>0)
                            {
                                response = controlSBO.oInventoryGenExitDocumentSBO(ref this.oCompany, oInventory, out DocEntry);
                                //oTools.ColocarMensaje("Se intenta crear el documento", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                if (response != "OK" && string.IsNullOrEmpty(DocEntry))
                                    throw new Exception(string.Format("Salida de mercancia no creada. ERROR {0}", response));

                                response = this.NotificaSalidaInv(OrdenServicio.SucursalOrden.SucursalID, OrdenServicio);
                                if (response != "OK")
                                    oTools.ColocarMensaje("Salida de mercancía generada con éxito. El correo no se ha podido enviar", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                else
                                    oTools.ColocarMensaje("La Salida de mercancía generada con éxito.", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                            }
                                
                            
                            
                        }
                        else
                        {
                            string  respuestas ="";
                            bool encontrado = false;
                            //SI YA EXISTE UNA ENTRADA, SE VALIDARÁ SI SE ESTÁ PIDIENDO MÁS REFACCIONES PARA GENERAR OTRA SALIDA DE INVENTARIO.
                            List<DetalleRefaccion> dtrSolicitadas = new List<DetalleRefaccion>();
                            List<DetalleRefaccion> dtrPorSolicitar = new List<DetalleRefaccion>(); 
                            if(oSetting.DraftSALIDA ==1)
                            {
                                
                                
                                response = SAPinterface.Data.tools.tools.SalidaDrafExistente(ref this.oCompany, out DocEntry, out consultaSql, "ODRF",ref respuestas, "ODRF",  OrdenServicioID.ToString());
                                dtrSolicitadas = JsonConvert.DeserializeObject<List<DetalleRefaccion>>(respuestas);
                                
                                foreach(DetalleRefaccion dr in this.lstrefacciones)
                                {
                                    encontrado = false;
                                    foreach(DetalleRefaccion dr2 in dtrSolicitadas)
                                    {
                                        if(dr.Articulo == dr2.Articulo && dr.Cantidad == dr2.Cantidad)
                                        {
                                            encontrado = true;
                                        }                                        
                                    }
                                    if (!encontrado)
                                    {
                                        dtrPorSolicitar.Add(dr);
                                    }
                                }


                                /****/
                                 if (dtrPorSolicitar.Count>0)
                                {
                                    SBO_KF_DOCUMENTS_INFO oInventory = new SBO_KF_DOCUMENTS_INFO();
                                    oInventory.TaxDate = Convert.ToDateTime(OrdenServicio.Fecha);
                                    oInventory.DocDate = Convert.ToDateTime(OrdenServicio.Fecha);
                                    oInventory.Reference2 = string.Format(@"{0}/{1}", OrdenServicio.FolioOrden.Folio, OrdenServicio.FolioOrden.AnioFolio);
                                    oInventory.Comments = string.Format(@"Salida de mercancía realizada desde Addon Kanan Fleet en fecha {0} ", DateTime.Now.ToString("dd/MM/yyyy"));
                                    oInventory.U_KF_FOLIOKF = OrdenServicio.OrdenServicioID.ToString();
                                    oInventory.Branch = SAPinterface.Data.tools.tools.UtilizaBranchs(ref oCompany);
                                    //CONDICIÓN MODIFICADA QUE ESA EN EL ESTATUS APROBADA = 2 Y EN VERSIÓN DRAFT
                                    oInventory.bDraftType = oSetting.DraftSALIDA == 1;
                                    oInventory.DocumentLines = new List<SBO_KF_DOCUMENTS_LINE_INFO>();
                                    foreach (DetalleRefaccion oLine in dtrPorSolicitar)
                                    {
                                        SBO_KF_DOCUMENTS_LINE_INFO oDocLine = new SBO_KF_DOCUMENTS_LINE_INFO();
                                        int BranchID = -1;
                                        oDocLine.Dimension = this.oSetting.DimCode;
                                        oDocLine.ItemCode = oLine.Articulo;
                                        oDocLine.Quantity = Convert.ToDouble(oLine.Cantidad);
                                        //CAMPOS AGREGADOS POR INMSO*/
                                        oDocLine.U_DIM1 = DIM1;
                                        oDocLine.U_DIM2 = DIM2;
                                        oDocLine.U_DIM3 = DIM3;
                                        oDocLine.U_DIM4 = DIM4;

                                        if (oDocLine.Quantity <= 0)
                                            throw new Exception("Todas la líneas deben tener cantidad de herramientas");
                                        oDocLine.WarehouseCode = oLine.CodigoAlmacen;
                                        response = controlConnfig.recuperaCentrodeCosto(ref this.oCompany, Convert.ToInt32(OrdenServicio.SucursalOrden.SucursalID), Convert.ToInt32(this.lstservicios[0].Vehiculo.VehiculoID), CardCode, out CentrodeCosto, out BranchID);
                                        if (response != "OK" && string.IsNullOrEmpty(CentrodeCosto))
                                            throw new Exception("No se ha definido el centro de costo para emitir la salida de refacciones");
                                        oInventory.BPL_IDAssignedToInvoice = BranchID;
                                        oDocLine.CostingCode = CentrodeCosto;

                                        oInventory.DocumentLines.Add(oDocLine);



                                    }
                                    if (lstrefacciones.Count > 0)
                                    {
                                        response = controlSBO.oInventoryGenExitDocumentSBO(ref this.oCompany, oInventory, out DocEntry);
                                        //oTools.ColocarMensaje("Se intenta crear el documento", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                        if (response != "OK" && string.IsNullOrEmpty(DocEntry))
                                            throw new Exception(string.Format("Salida de mercancia no creada. ERROR {0}", response));

                                        response = this.NotificaSalidaInv(OrdenServicio.SucursalOrden.SucursalID, OrdenServicio);
                                        if (response != "OK")
                                            oTools.ColocarMensaje("Salida de mercancía generada con éxito. El correo no se ha podido enviar", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                        else
                                            oTools.ColocarMensaje("La Salida de mercancía generada con éxito.", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                    }
                                }


                                /**/
                            }
                            else
                            {
                                response = SAPinterface.Data.tools.tools.SalidaExistente(ref this.oCompany, out DocEntry, out consultaSql, "OIGE", ref respuestas, "OIGE", OrdenServicioID.ToString());
                                dtrSolicitadas = JsonConvert.DeserializeObject<List<DetalleRefaccion>>(respuestas);

                                foreach (DetalleRefaccion dr in this.lstrefacciones)
                                {
                                    encontrado = false;
                                    foreach (DetalleRefaccion dr2 in dtrSolicitadas)
                                    {
                                        if (dr.Articulo == dr2.Articulo && dr.Cantidad == dr2.Cantidad)
                                        {
                                            encontrado = true;
                                        }
                                    }
                                    if (!encontrado)
                                    {
                                        dtrPorSolicitar.Add(dr);
                                    }
                                }


                                /****/
                                if (dtrPorSolicitar.Count > 0)
                                {
                                    SBO_KF_DOCUMENTS_INFO oInventory = new SBO_KF_DOCUMENTS_INFO();
                                    oInventory.TaxDate = Convert.ToDateTime(OrdenServicio.Fecha);
                                    oInventory.DocDate = Convert.ToDateTime(OrdenServicio.Fecha);
                                    oInventory.Reference2 = string.Format(@"{0}/{1}", OrdenServicio.FolioOrden.Folio, OrdenServicio.FolioOrden.AnioFolio);
                                    oInventory.Comments = string.Format(@"Salida de mercancía realizada desde Addon Kanan Fleet en fecha {0} ", DateTime.Now.ToString("dd/MM/yyyy"));
                                    oInventory.U_KF_FOLIOKF = OrdenServicio.OrdenServicioID.ToString();
                                    oInventory.Branch = SAPinterface.Data.tools.tools.UtilizaBranchs(ref oCompany);
                                    //CONDICIÓN MODIFICADA QUE ESA EN EL ESTATUS APROBADA = 2 Y EN VERSIÓN DRAFT
                                    oInventory.bDraftType = oSetting.DraftSALIDA == 1;
                                    oInventory.DocumentLines = new List<SBO_KF_DOCUMENTS_LINE_INFO>();
                                    foreach (DetalleRefaccion oLine in dtrPorSolicitar)
                                    {
                                        SBO_KF_DOCUMENTS_LINE_INFO oDocLine = new SBO_KF_DOCUMENTS_LINE_INFO();
                                        int BranchID = -1;
                                        oDocLine.Dimension = this.oSetting.DimCode;
                                        oDocLine.ItemCode = oLine.Articulo;
                                        oDocLine.Quantity = Convert.ToDouble(oLine.Cantidad);
                                        //CAMPOS AGREGADOS POR INMSO*/
                                        oDocLine.U_DIM1 = DIM1;
                                        oDocLine.U_DIM2 = DIM2;
                                        oDocLine.U_DIM3 = DIM3;
                                        oDocLine.U_DIM4 = DIM4;

                                        if (oDocLine.Quantity <= 0)
                                            throw new Exception("Todas la líneas deben tener cantidad de herramientas");
                                        oDocLine.WarehouseCode = oLine.CodigoAlmacen;
                                        response = controlConnfig.recuperaCentrodeCosto(ref this.oCompany, Convert.ToInt32(OrdenServicio.SucursalOrden.SucursalID), Convert.ToInt32(this.lstservicios[0].Vehiculo.VehiculoID), CardCode, out CentrodeCosto, out BranchID);
                                        if (response != "OK" && string.IsNullOrEmpty(CentrodeCosto))
                                            throw new Exception("No se ha definido el centro de costo para emitir la salida de refacciones");
                                        oInventory.BPL_IDAssignedToInvoice = BranchID;
                                        oDocLine.CostingCode = CentrodeCosto;

                                        oInventory.DocumentLines.Add(oDocLine);



                                    }
                                    if (lstrefacciones.Count > 0)
                                    {
                                        response = controlSBO.oInventoryGenExitDocumentSBO(ref this.oCompany, oInventory, out DocEntry);
                                        //oTools.ColocarMensaje("Se intenta crear el documento", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                        if (response != "OK" && string.IsNullOrEmpty(DocEntry))
                                            throw new Exception(string.Format("Salida de mercancia no creada. ERROR {0}", response));

                                        response = this.NotificaSalidaInv(OrdenServicio.SucursalOrden.SucursalID, OrdenServicio);
                                        if (response != "OK")
                                            oTools.ColocarMensaje("Salida de mercancía generada con éxito. El correo no se ha podido enviar", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                        else
                                            oTools.ColocarMensaje("La Salida de mercancía generada con éxito.", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                    }
                                }


                                /**/
                            }

                        }
                        #endregion salida de mercancia de las refacciones utilizadas
                    }
                }
                else throw new Exception("No se ha encontrado el proveedor asignado para la orden de servicio");
            }

        }
        void InsertaBitacora( Int32 EstatusOrdenID, string OrdenServicioID, string OrdenServicioWebID, bool bEsNueva = false)
        {
            string sresponse = string.Empty;
            long IDBitacora = -1;
            Add control = new Add(ref this.oCompany, oConnfig);
            List<SBO_KF_EstatusOrden_INFO> lstlEstatusActual = new List<SBO_KF_EstatusOrden_INFO>();
            List<SBO_KF_EstatusOrden_INFO> lstlEstatusAnterior = new List<SBO_KF_EstatusOrden_INFO>();
            SBO_KF_BITACORA_PD controlBitacora = new SBO_KF_BITACORA_PD();
            SBO_KF_BITACORA_INFO oBitacora = new SBO_KF_BITACORA_INFO();
            SBO_KF_ORDENSERVICIO_PD controlOS = new SBO_KF_ORDENSERVICIO_PD();
            bool bContinua = bEsNueva;
            if (!bContinua)
            {
                if (EstatusOrdenID != this.StatsOrdenID)
                    bContinua = true;
            }           

            if (bContinua)
            {
                IDBitacora = control.recuperIDMaximoTabla("VSKF_OSCAMESTADO");
                if (IDBitacora > 0)
                {
                    sresponse = controlOS.lstEstatusOrdenes(ref this.oCompany, ref lstlEstatusActual, Code: EstatusOrdenID.ToString());
                    if (sresponse != "OK")
                        throw new Exception("No se ha podido evaluar el estatus actual");
                    if (!bEsNueva)
                    {
                        sresponse = controlOS.lstEstatusOrdenes(ref this.oCompany, ref lstlEstatusAnterior, Code: this.StatsOrdenID.ToString());
                        if (sresponse != "OK")
                            throw new Exception("No se ha podido evaluar el estatus anterior");
                    }

                    oBitacora.fechaHora = DateTime.Now;
                    oBitacora.sincronizado = false;
                    oBitacora.origen = "S";
                    oBitacora.estatusAnt = string.Empty;
                    oBitacora.estatusAntID = -1;
                    oBitacora.Code = IDBitacora.ToString();
                    oBitacora.Name = IDBitacora.ToString();
                    oBitacora.usuario = this.oCompany.UserName;
                    oBitacora.OrdenServicioSAPID = OrdenServicioID;
                    oBitacora.OrdenServicioWebID = OrdenServicioWebID;
                    oBitacora.estatusActID = EstatusOrdenID;
                    oBitacora.estatusAct = lstlEstatusActual[0].Nombre;
                    if (!bEsNueva)
                    {
                        oBitacora.estatusAntID = StatsOrdenID;
                        oBitacora.estatusAnt = lstlEstatusAnterior[0].Nombre;
                    }
                    sresponse = controlBitacora.CreateBitacora(ref this.oCompany, oBitacora);
                    if (sresponse != "OK")
                        throw new Exception("No se ha podido crear el registro de bitacora");
                }
                else throw new Exception("No se ha podido asignar el folio para la bitacora");
            }
        }
        void PlantillaToForm()
        {
            //ProgressBar oProgress = null;
            try
            {
                oCombo = this.oForm.Items.Item("comboVehOS").Specific;
                if (Convert.ToInt32(oCombo.Value) > 0)
                {


                    string response = string.Empty, Codeplantilla = string.Empty;
                    List<OrdenServicio> lstlPlantillas = new List<OrdenServicio>();
                    SBO_KF_PLANTILLA_PD controlRutina = new SBO_KF_PLANTILLA_PD();
                    oItem = this.oForm.Items.Item("cmbPla");
                    oCombo = (ComboBox)this.oItem.Specific;
                    Codeplantilla = oCombo.Selected.Value.ToString();
                    if (!string.IsNullOrEmpty(Codeplantilla) && Codeplantilla != "0")
                    {
                        //oProgress = this.SBO_Application.StatusBar.CreateProgressBar("Cargando información", 1, true);
                        response = controlRutina.listaPlantillas(ref this.oCompany, ref lstlPlantillas, Codeplantilla);
                        if (response == "OK")
                        {
                            OrdenServicio oPlantilla = lstlPlantillas[0];
                            this.EntityToForm(oPlantilla);


                            //SE CARGA EL COMBO DE VEHÍCULOS DEPENDIENDO DEL TIPO DE VH
                            //if(lstlPlantillas[0].ObjetoRutina != null && lstlPlantillas[0].ObjetoRutina.TipoVehiculoID != null)
                            //    FillComboVehiculosFiltrados(lstlPlantillas[0].ObjetoRutina.TipoVehiculoID);
                            #region detalle de plantilla
                            //oProgress.Text = "Cargar servicios relacionados";
                            lstservicios = new List<DetalleOrden>();
                            response = controlRutina.listaServicios(ref this.oCompany, ref lstservicios, Codeplantilla);
                            if (response != "OK" && response != "SIN RESULTADOS")
                                throw new Exception(string.Format("los servicios no se han podido cargar. {0}", response));
                            else {

                                oCombo = this.oForm.Items.Item("comboVehOS").Specific;
                                lstservicios.ForEach(x=>x.Vehiculo.VehiculoID = Convert.ToInt32(oCombo.Value));
                                lstservicios.ForEach(x => x.Vehiculo.Nombre = oCombo.Selected.Description);
                            }
                            this.FillGridServiciosFromList();
                            //oProgress.Text = "Cargar herramientas relacionadas";
                            lstherramientas = new List<SBO_KF_Herramientas_INFO>();
                            response = controlRutina.listaHerramientas(ref this.oCompany, ref lstherramientas, Codeplantilla);
                            if (response != "OK" && response != "SIN RESULTADOS")
                                throw new Exception(string.Format("las herramientas no se han podido cargar. {0}", response));
                            this.FillGridHerramientas();
                            //oProgress.Text = "Cargar refacciones relacionadas";
                            lstrefacciones = new List<DetalleRefaccion>();
                            response = controlRutina.listaRefacciones(ref this.oCompany, ref lstrefacciones, Codeplantilla);
                            if (response != "OK" && response != "SIN RESULTADOS")
                                throw new Exception(string.Format("las refacciones no se han podido cargar. {0}", response));

                            /*CAMBIOS PARA INMSO*/
                            SBO_KF_VEHICULOS_PD controlVehiculo = new SBO_KF_VEHICULOS_PD();
                            List<SBO_KF_VEHICULOS_INFO> lstVehiculos = new List<SBO_KF_VEHICULOS_INFO>();

                            if (lstservicios.Count > 0)
                            {
                                string responsev = controlVehiculo.listaVehiculosSucursal(ref oCompany, ref lstVehiculos, Convert.ToInt32(lstservicios[0].MantenibleID));
                            }

                            /*CAMBIOS PARA INMSO*/

                            if (lstrefacciones.Count > 0)
                            {
                                List<SBO_KF_OITM_INFO> lstArt = new List<SBO_KF_OITM_INFO>();
                                string itemsCodes = string.Empty;
                                foreach (DetalleRefaccion orefacc in lstrefacciones)
                                    itemsCodes += string.Format("'{0}',", orefacc.Articulo);
                                new SBO_KF_OITM_AD().buscaArticulo(ref this.oCompany, ref lstArt, ItemCodeIN: itemsCodes.TrimEnd(','));

                                foreach(DetalleRefaccion orefacc in lstrefacciones)
                                {
                                    var OITM = lstArt.First(x => x.ItemCode.Equals(orefacc.Articulo, StringComparison.InvariantCultureIgnoreCase));
                                    if (OITM != null)
                                    {
                                        orefacc.ArticuloID = OITM.iIdArticulo;
                                        orefacc.DIM1 = lstVehiculos[0].U_DIM1 ;
                                        orefacc.DIM2 = lstVehiculos[0].U_DIM2;
                                        orefacc.DIM3 = lstVehiculos[0].U_DIM3;
                                        orefacc.DIM4 = lstVehiculos[0].U_DIM4;
                                    }
                                }

                            }
                            this.FillGridRefaccionesFromList();
                            //oProgress.Text = "Cargar costos de la OS";
                            lstcostoadicional = new List<SBOKF_CL_CostosAdicional_INFO>();
                            response = controlRutina.listaCostos(ref this.oCompany, ref lstcostoadicional, Codeplantilla);
                            if (response != "OK" && response != "SIN RESULTADOS")
                                throw new Exception(string.Format("los costos adicionales no se han podido cargar. {0}", response));
                            this.FillGridGastosAdicionales();
                            //oProgress.Text = "Cargar mecanicos";
                            lstmecanicos = new List<Kanan.Mantenimiento.BO2.SBO_KF_OHEM_INFO>();
                            response = controlRutina.listaMecanicos(ref this.oCompany, ref lstmecanicos, Codeplantilla);
                            if (response != "OK" && response != "SIN RESULTADOS")
                                throw new Exception(string.Format("los mecanicos no se han podido cargar. {0}", response));
                            this.FillGridMecanicos();
                            #endregion detalle de plantilla
                        }
                        else oTools.ColocarMensaje("No se ha podido cargar la información de la rutina", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                        //oProgress.Stop();
                    }
                    else oTools.ColocarMensaje("Favor de seleccionar la rutina", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                }
                else this.SBO_Application.MessageBox("Favor de establecer el vehículo para asiganr la rutina.", 1, "Ok");
            }
            catch (Exception ex)
            {
               // if (oProgress != null)
                //    oProgress.Stop();
                KananSAP.Helper.KananSAPHerramientas.LogError("SBO_KF_SolicitaOrdenesView.cs->ActualizaOrdenServicio()", ex.Message);
                oTools.ColocarMensaje(string.Format("No se ha podido cargar los datos de la rutina. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
        }
        void FormLoadSucursalesDeVehiculo()
        {
            //oTools.ColocarMensaje( "Ya pude atrabar el evento" , BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);


            //UBICAMOS EL VEHÍCULO PARA OBTENER SUS CARACTERISTICAS
            oItem = this.oForm.Items.Item("comboVehOS");
            oCombo = (ComboBox)this.oItem.Specific;
            int vehiculoID;
            int SucursalID = 0;
            int TipoVehiculoID = 0;
            string Almacen = "";
            int.TryParse(this.oCombo.Value, out vehiculoID);

            List<SBO_KF_VEHICULOS_INFO> lstResultados = new List<SBO_KF_VEHICULOS_INFO>();
            SBO_KF_VEHICULOS_PD oVehiculopd = new SBO_KF_VEHICULOS_PD();
            SBO_KF_VEHICULOS_INFO otemp = new SBO_KF_VEHICULOS_INFO();

            oVehiculopd.listaVehiculosSucursal(ref oCompany, ref lstResultados, vehiculoID, -1,-1,-1,"",-1);
            if (lstResultados.Count > 0)
            {
                SucursalID = lstResultados[0].U_SUCURSALID;
                Almacen = lstResultados[0].U_ALMACEN;
                TipoVehiculoID = lstResultados[0].U_TIPOVEHICULOID;
            }
            //listaVehiculosSucursal

            oItem = this.oForm.Items.Item("cmbsuc");
            oCombo = (ComboBox)this.oItem.Specific;
            //oCombo.Item.Enabled = false;

            //oCombo.Select(SucursalID.ToString());
            //oCombo.Item.Enabled = false;

            //oItem = this.oForm.Items.Item("cmbAlmacn");
            //oCombo = (ComboBox)this.oItem.Specific;
            //oCombo.Item.Enabled = false;
            //if (string.IsNullOrEmpty(Almacen))
            //    oCombo.Select(0 );
            //else
            //    oCombo.Select(Almacen);

            oItem = this.oForm.Items.Item("cmbrutina");
            oCombo = (ComboBox)this.oItem.Specific;
            string sTypeRutina = oCombo.Selected.Value;
            if (!string.IsNullOrEmpty(sTypeRutina))
            {
                switch (sTypeRutina)
                {
                    case "0":
                    case "1":
                        break;
                    case "3":
                        
                    case "2":
                        new GestionGlobales().FillCombo("Ninguno", "Code", "Name", "cmbPla", this.sQueryPlantillaFiltradaTipoVH(TipoVehiculoID.ToString()), this.oForm, this.oCompany);
                    break;

                }
            }
            //SE BLOQUEA EL COSTO A PETICIÓN DE INMSO 
            //SE DES-BLOQUEA EL COSTO A PETICIÓN DE INMSO 26/08/2020
            oItem = this.oForm.Items.Item("txtCostoRf");
            oEditText = (EditText)this.oItem.Specific;
            oEditText.Item.Enabled = true;
            
        }

        void FormLoadVehiculoDeSucursales()
        {
            //oTools.ColocarMensaje( "Ya pude atrabar el evento" , BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);


            //UBICAMOS EL VEHÍCULO PARA OBTENER SUS CARACTERISTICAS
            oItem = this.oForm.Items.Item("cmbsuc");
            oCombo = (ComboBox)this.oItem.Specific;
            int vehiculoID = 0;
            int SucursalID = 0;
            int TipoVehiculoID = 0;
            string Almacen = "";
            int.TryParse(this.oCombo.Value, out SucursalID);

            List<SBO_KF_VEHICULOS_INFO> lstResultados = new List<SBO_KF_VEHICULOS_INFO>();
            SBO_KF_VEHICULOS_PD oVehiculopd = new SBO_KF_VEHICULOS_PD();
            SBO_KF_VEHICULOS_INFO otemp = new SBO_KF_VEHICULOS_INFO();

            oVehiculopd.listaVehiculosSucursal(ref oCompany, ref lstResultados, -1, -1, -1, SucursalID, "", -1);

            if (lstResultados.Count > 0)
            {
                SucursalID = lstResultados[0].U_SUCURSALID;
                Almacen = lstResultados[0].U_ALMACEN;
                TipoVehiculoID = lstResultados[0].U_TIPOVEHICULOID;
            }
            //listaVehiculosSucursal

            //oItem = this.oForm.Items.Item("cmbsuc");
            //oCombo = (ComboBox)this.oItem.Specific;
            //oCombo.Item.Enabled = false;

            //oCombo.Select(SucursalID.ToString());
            //oCombo.Item.Enabled = false;

            ///sQueryPlantillaFiltradaVHDeSuc

            oItem = this.oForm.Items.Item("cmbAlmacn");
            oCombo = (ComboBox)this.oItem.Specific;
            oCombo.Item.Enabled = false;
            if (string.IsNullOrEmpty(Almacen))
                oCombo.Select(0);
            else
                oCombo.Select(Almacen);

            oItem = this.oForm.Items.Item("comboVehOS");
            oCombo = (ComboBox)this.oItem.Specific;

            FillComboVehiculosFiltradoXSucursal(SucursalID.ToString());

            //new GestionGlobales().FillCombo("Ninguno", "Code", "Name", "comboVehOS", this.sQueryPlantillaFiltradaVHDeSuc(SucursalID.ToString()), this.oForm, this.oCompany);

            //SE BLOQUEA EL COSTO A PETICIÓN DE INMSO 
            //SE DES-BLOQUEA EL COSTO A PETICIÓN DE INMSO 26/08/2020
            oItem = this.oForm.Items.Item("txtCostoRf");
            oEditText = (EditText)this.oItem.Specific;
            oEditText.Item.Enabled = true;

        }

        void FormLoadPlantillaMood()
        {
            try
            {
                oItem = this.oForm.Items.Item("cmbrutina");
                oCombo = (ComboBox)this.oItem.Specific;
                string sTypeRutina = oCombo.Selected.Value;
                if (!string.IsNullOrEmpty(sTypeRutina))
                {
                    oItem = this.oForm.Items.Item("lbltyper");
                    oItem.Visible = false;
                    oItem = this.oForm.Items.Item("txtPlanti");
                    oItem.Visible = false;
                    oItem = this.oForm.Items.Item("cmbPla");
                    oItem.Visible = false;


                    

                    switch (sTypeRutina)
                    {
                        case "0":
                            OcultaControlesPorRutina(true);
                            break;

                        case "1":
                            oItem = this.oForm.Items.Item("lbltyper");
                            oItem.Visible = true;
                            oLabel = oItem.Specific;
                            oLabel.Caption = "Nombre de rutina";
                            oItem = this.oForm.Items.Item("txtPlanti");
                            oItem.Visible = true;

                            OcultaControlesPorRutina(false);

                            oItem = oForm.Items.Item("tabtools");
                            oItem.Enabled = true;

                            oItem = oForm.Items.Item("tabmecani");
                            oItem.Enabled = true;

                            //oItem = oForm.Items.Item("tabDataRef");
                            //oItem.Enabled = true;
                            break;
                        case "2":
                            oItem = this.oForm.Items.Item("lbltyper");
                            oItem.Visible = true;
                            oLabel = oItem.Specific;
                            oLabel.Caption = "Selecciona la rutina";
                            oItem = this.oForm.Items.Item("cmbPla");
                            oItem.Visible = true;
                            new GestionGlobales().FillCombo("Ninguno", "Code", "Name", "cmbPla", this.sQueryPlantilla(), this.oForm, this.oCompany);

                            OcultaControlesPorRutina(true);
                            oItem = oForm.Items.Item("tabtools");
                            oItem.Enabled = false;

                            oItem = oForm.Items.Item("tabmecani");
                            oItem.Enabled = false;

                            //oItem = oForm.Items.Item("tabDataRef");
                            //oItem.Enabled = false;
                            
                            break;
                        case "3":
                            oItem = this.oForm.Items.Item("lbltyper");
                            oItem.Visible = true;
                            oLabel = oItem.Specific;
                            oLabel.Caption = "Selecciona la rutina";
                            oItem = this.oForm.Items.Item("cmbPla");
                            oItem.Visible = true;
                            new GestionGlobales().FillCombo("Ninguno", "Code", "Name", "cmbPla", this.sQueryPlantilla(), this.oForm, this.oCompany);

                            OcultaControlesPorRutina(false);

                            oItem = oForm.Items.Item("tabtools");
                            oItem.Enabled = true;

                            oItem = oForm.Items.Item("tabmecani");
                            oItem.Enabled = true;

                            oItem = oForm.Items.Item("tabDataRef");
                            oItem.Enabled = true;

                            break;
                    }
                }
            }
            catch { }
        }


        void OcultaControlesPorRutina(bool Activar)
        {
            //SE OCULTA EL COMBO DE SISTEMAS
            oItem = this.oForm.Items.Item("cmbSyscr");
            oItem.Visible = Activar;
            oItem = this.oForm.Items.Item("Item_10");
            oItem.Visible = Activar;

            //SE OCULTA EL COMBO DE COMPONENTES
            oItem = this.oForm.Items.Item("Item_14");
            oItem.Visible = Activar;
            oItem = this.oForm.Items.Item("cmbCompo");
            oItem.Visible = Activar;

            //SE OCULTA EL COMBO DE PRIORIDAD
            oItem = this.oForm.Items.Item("Item_18");
            oItem.Visible = Activar;
            oItem = this.oForm.Items.Item("cmblevel");
            oItem.Visible = Activar;

            //SE OCULTA EL COMBO DE ESTATUS
            oItem = this.oForm.Items.Item("Item_4");
            oItem.Visible = Activar;
            oItem = this.oForm.Items.Item("cmbestatus");
            oItem.Visible = Activar;

            //SE OCULTA FECHA ENTRADA PROGRAMADA
            oItem = this.oForm.Items.Item("lblEnPro");
            oItem.Visible = Activar;
            oItem = this.oForm.Items.Item("txtEnPro");
            oItem.Visible = Activar;

            //SE OCULTA FECHA SALIDA PROGRAMADA
            oItem = this.oForm.Items.Item("lblSalPro");
            oItem.Visible = Activar;
            oItem = this.oForm.Items.Item("txtSalPro");
            oItem.Visible = Activar;

            //SE OCULTA FECHA ENTRADA REAL
            oItem = this.oForm.Items.Item("lblEnReal");
            oItem.Visible = Activar;
            oItem = this.oForm.Items.Item("txtEnReal");
            oItem.Visible = Activar;

            //SE OCULTA FECHA SALIDA REAL
            oItem = this.oForm.Items.Item("lblSalReal");
            oItem.Visible = Activar;
            oItem = this.oForm.Items.Item("txtSalReal");
            oItem.Visible = Activar;

            //SE OCULTA FECHA SERVICIO
            oItem = this.oForm.Items.Item("lblFchOS");
            oItem.Visible = Activar;
            oItem = this.oForm.Items.Item("txtFechaOS");
            oItem.Visible = Activar;

            //SE OCULTA LA HORA SERVICIO
            oItem = this.oForm.Items.Item("txtHoraOS");
            oItem.Visible = Activar; 

            //SE OCULTA COMBO TALLER INTERNO
            oItem = this.oForm.Items.Item("CBTI");
            oItem.Visible = Activar;

            //SE OCULTA RESPONSABLE
            oItem = this.oForm.Items.Item("lblRsp");
            oItem.Visible = Activar;
            oItem = this.oForm.Items.Item("cmbResp");
            oItem.Visible = Activar;

            //SE OCULTA APROBADOR
            oItem = this.oForm.Items.Item("lblAprob");
            oItem.Visible = Activar;
            oItem = this.oForm.Items.Item("cmbAprob");
            oItem.Visible = Activar;

            //SE OCULTA HORA ENTRADA PROGRAMADA
            oItem = this.oForm.Items.Item("txthEntr");
            oItem.Visible = Activar;

            //SE OCULTA HORA SALIDA PROGRAMADA
            oItem = this.oForm.Items.Item("txtHsal");
            oItem.Visible = Activar;

            //SE OCULTA HORA ENTRADA REAL
            oItem = this.oForm.Items.Item("txtHReale");
            oItem.Visible = Activar;

            //SE OCULTA HORA SALIDA REAL
            oItem = this.oForm.Items.Item("txtHSalr");
            oItem.Visible = Activar;

            //SE OCULTA FOLIO
            oItem = this.oForm.Items.Item("Item_2");
            oItem.Visible = Activar;
            oItem = this.oForm.Items.Item("txtfolio");
            oItem.Visible = Activar;
            
            //SE OCULTA CLIENTE SAP
            oItem = this.oForm.Items.Item("Item_5");
            oItem.Visible = Activar;
            oItem = this.oForm.Items.Item("txtClisap");
            oItem.Visible = Activar;

            //SE OCULTA CLIENTE SAP
            oItem = this.oForm.Items.Item("Item_8");
            oItem.Visible = Activar;
            oItem = this.oForm.Items.Item("txtInspec");
            oItem.Visible = Activar;

            //SE OCULTA ODOMETRO
            oItem = this.oForm.Items.Item("lblOdomOS");
            oItem.Visible = Activar;
            oItem = this.oForm.Items.Item("txtOdomOs");
            oItem.Visible = Activar;
            oItem = this.oForm.Items.Item("txtHoroOs");
            oItem.Visible = Activar;

        }

        void OcultaControlesRutina(bool Activar)
        {
            oItem = this.oForm.Items.Item("txtPlanti");
            oItem.Visible = Activar;
            oItem = this.oForm.Items.Item("cmbPla");
            oItem.Visible = Activar;

            oItem = this.oForm.Items.Item("Rutina");
            oItem.Visible = Activar;
            oItem = this.oForm.Items.Item("cmbrutina");
            oItem.Visible = Activar;
            oItem = this.oForm.Items.Item("lbltyper");
            oItem.Visible = Activar;
        }

        void FillComboComponenteSistema()
        {
            try
            {
                List<SBO_KF_CAUSADETALLE_INFO> lstResultados = new List<SBO_KF_CAUSADETALLE_INFO>();
                SBO_KF_CausaRaiz_PD oCausapd = new SBO_KF_CausaRaiz_PD();
                SBO_KF_CRAIZ_INFO otemp = new SBO_KF_CRAIZ_INFO();
                oItem = this.oForm.Items.Item("cmbSyscr");
                oCombo = (ComboBox)this.oItem.Specific;
                if (!string.IsNullOrEmpty(oCombo.Selected.Value))
                {
                    if (oCombo.Selected.Value != "0")
                    {
                        //este es el identificador web 
                        string sistemaWSID = oCombo.Selected.Value.ToString();
                        List<SBO_KF_CRAIZ_INFO> lstCausaPadre = new List<SBO_KF_CRAIZ_INFO>();
                        SBO_KF_CRAIZ_INFO oPadre = new SBO_KF_CRAIZ_INFO();
                        oPadre.Name = sistemaWSID;
                        oCausapd.listaCausaraiz(ref oCompany, ref lstCausaPadre, oPadre, false);
                        if (lstCausaPadre.Count > 0)
                        {
                            otemp.Code = lstCausaPadre[0].Code;
                            oCausapd.listaCausaDetalle(ref oCompany, ref lstResultados, otemp);
                            if (lstResultados.Count > 0)
                            {
                                oItem = this.oForm.Items.Item("cmbCompo");
                                oCombo = (ComboBox)this.oItem.Specific;
                                KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);
                                oCombo.ValidValues.Add("0", "Selecciona un componente");
                                foreach (SBO_KF_CAUSADETALLE_INFO oSistema in lstResultados)
                                    oCombo.ValidValues.Add(oSistema.Name, oSistema.Componente);
                                oCombo.Item.DisplayDesc = true;
                                try {
                                    oCombo.Select("0");
                                }
                                catch { }
                            }
                        }
                        else oTools.ColocarMensaje("Los datos no coiciden entre WS y SAP", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                    }
                }
            }
            catch (Exception ex)
            {
                oTools.ColocarMensaje(string.Format("No se ha podido cargar el componente. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                //throw new Exception(ex.Message);
            }
        }
        void cargaSalarioEmpleado()
        {
            oItem = this.oForm.Items.Item("cmbmeca");
            oCombo = (ComboBox)this.oItem.Specific;
            oEditText = oForm.Items.Item("txtSueldoT").Specific;
            string sValue = oCombo.Selected.Value;
            if (!string.IsNullOrEmpty(sValue))
            {
                SBO_KF_OHEM_PD oMecanicos = new SBO_KF_OHEM_PD();

                List<SAPinterface.Data.INFO.Tablas.SBO_KF_OHEM_INFO> lstmecanicos = new List<SAPinterface.Data.INFO.Tablas.SBO_KF_OHEM_INFO>();
                oMecanicos.listaEmpleados(ref oCompany, ref lstmecanicos, Convert.ToInt32(sValue));
                if (lstmecanicos.Count > 0)
                oEditText.String = lstmecanicos[0].salary.ToString();
                 
            }
            else oEditText.String = "00.0";
        }

        void EstableceCostoRefaccion()
        {
            string CodigoArticulo = "";
            decimal costorefaccion = 0 ;
            string Almacen = "";

            List<SBO_KF_OITM_INFO> lstResultados = new List<SBO_KF_OITM_INFO>();
            SBO_KF_REFACCION_PD oRefaccionpd = new SBO_KF_REFACCION_PD();
            SBO_KF_OITM_INFO otemp = new SBO_KF_OITM_INFO();
            
            oCombo = this.oForm.Items.Item("cmbAlmacn").Specific;
            Almacen = oCombo.Selected.Value;

            oEditText = this.oForm.Items.Item("txtRefSAP").Specific;
            CodigoArticulo = oEditText.Value;
            //oTools.ColocarMensaje(String.Format("El Almacen es: {0}", Almacen), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            //oTools.ColocarMensaje(String.Format("El Codigo es: {0}", CodigoArticulo), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

            oRefaccionpd.listaRefacciones(ref oCompany, ref lstResultados, CodigoArticulo, Almacen );
            if (lstResultados.Count > 0)
            {
                costorefaccion = lstResultados[0].AvgPrice; 
            }

           // oTools.ColocarMensaje(String.Format("El Trae {0} resultados", lstResultados.Count), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

             //NO SE PUEDE ASIGNAR DIRECTAMENTE PORQUE TIENE LIGADO UN UDS USER DATA SOURCES LA CAJA ES TXTCOSTORF Y EL UDS ES UD_CSTOR
             //oEditText = this.oForm.Items.Item("txtCostoRf").Specific;
            // oEditText.Value = "abc";//  costorefaccion.ToString();
            EditText oEdit = oForm.Items.Item("txtCostoRf").Specific;
            oEdit.DataBind.SetBound(true, "", "UD_CstoR");

             SAPbouiCOM.UserDataSource  oUserDataSource =  oForm.DataSources.UserDataSources.Item("UD_CstoR");
             oUserDataSource.ValueEx = costorefaccion.ToString();

            
            //oTools.ColocarMensaje(String.Format("El precio es: {0}", costorefaccion), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
        }

        void DefineChooseCosto()
        {
            oItem = this.oForm.Items.Item("cmbATipo");
            oCombo = (ComboBox)this.oItem.Specific;
            string sValue = oCombo.Selected.Value;
            if (!string.IsNullOrEmpty(sValue))
            {
                oItem = this.oForm.Items.Item("txtADesc1");
                oItem.Visible = false;
                oItem = this.oForm.Items.Item("txtADesc2");
                oItem.Visible = false;
                oItem = this.oForm.Items.Item("txtADesc3");
                oItem.Visible = false;
                switch (sValue)
                {
                    case "0":
                        oItem = this.oForm.Items.Item("txtADesc1");
                        break;
                    case "1":
                        oItem = this.oForm.Items.Item("txtADesc2");
                        break;
                    case "2":
                        oItem = this.oForm.Items.Item("txtADesc3");
                        break;
                }
                oItem.Visible = true;

            }

        }

        void LanzaServicioForm()
        {
            try
            {
                if(this.OSRowIndex != null && this.OSRowIndex >=0)
                {
                    DetalleOrden oServicio = this.lstservicios[(int)OSRowIndex];
                    try
                    {
                        
                        oCombo = oForm.Items.Item("comboVehOS").Specific;
                        oCombo.Select(oServicio.Vehiculo.VehiculoID.ToString());
                    }
                    catch { }

                    try
                    {
                        oCombo = oForm.Items.Item("comoServOS").Specific;
                        oCombo.Select(oServicio.Servicio.ServicioID.ToString());
                    }
                    catch { }
                    oEditText = oForm.Items.Item("txtCostoOS").Specific;
                    if (oServicio.Costo != null)
                        oEditText.String = oServicio.Costo.ToString();
                    this.bEditaServicio = true;
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("SBO_KF_SolicitaOrdenesView.cs->LanzaServicioForm()", ex.Message);
                oTools.ColocarMensaje(string.Format("No se ha podido seleccionar el registro. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
        
        }
        void EliminaregistroGrid(string grid)
        {
            try
            {
                if (this.OSRowIndex != null)
                {
                    switch (grid)
                    {
                        case "btnDelOSv":
                            this.lstservicios.Remove(lstservicios[(Int32)this.OSRowIndex]);
                            this.bEditaServicio = true;
                            this.FillGridServiciosFromList();
                            break;

                        case "btnDelRf":
                            this.lstrefacciones.Remove(lstrefacciones[((Int32)this.OSRowIndex) - 1]);
                            this.bEditaRefaccion = true;
                            this.FillGridRefaccionesFromList();
                            break;

                        case "btnDelcto":
                            this.lstcostoadicional.Remove(lstcostoadicional[(Int32)this.OSRowIndex]);
                            this.bEditaCosto = true;
                            this.FillGridGastosAdicionales();
                            break;

                        case "btndeltool":
                            this.lstherramientas.Remove(lstherramientas[ (Int32)this.OSRowIndex  ]);
                            this.bEditaHerramientas = true;
                            this.FillGridHerramientas();
                            break;

                        case "btndelmeca":
                            this.lstmecanicos.Remove(lstmecanicos[ (Int32)this.OSRowIndex  ]);
                            this.bEditaMecanicos = true;
                            this.FillGridMecanicos();
                            break;
                    }
                    this.calculaCostoTotal();
                }
                else throw new Exception("El elemento no ha sido seleccionado");

            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("SBO_KF_SolicitaOrdenesView.cs->EliminaregistroGrid()", ex.Message);
                oTools.ColocarMensaje(string.Format("El registro no se puede borrar favor de realizar la selección. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
        }
        void ExportaPDF(out string PathPDF, bool bStart = true)
        {
            PathPDF = string.Empty;
            OrdenServicio OrdenServicio = new OrdenServicio();
            List<SAPinterface.Data.INFO.Tablas.SBO_KF_OHEM_INFO> lstEmpleados = new List<SAPinterface.Data.INFO.Tablas.SBO_KF_OHEM_INFO>();
            List<SBO_KF_OCRD_INFO> lstProveedores = new List<SBO_KF_OCRD_INFO>();

            try
            {
                
                string response = this.FormToEntity(ref OrdenServicio);
                if (response == "OK")
                {

                    
                    String pathFile = SAPinterface.Data.tools.tools.DirectorioSBO(ref this.oCompany);

                    if (string.IsNullOrEmpty(pathFile))
                        throw new Exception("No se ha determinado el directorio de PDF");

                    pathFile = string.Format(@"{0}\OrdenServicio", pathFile);
                    if (!Directory.Exists(pathFile))
                        Directory.CreateDirectory(pathFile);
                    PathPDF = string.Format(@"{0}\{1}{2}.pdf", pathFile, "OS_00001", this.OrdenServicioID);

                    String FechActual = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                    PdfWriter oPDFWrite;
                    PdfContentByte oPDFcb;

                    if (File.Exists(PathPDF))
                        File.Delete(PathPDF);

                    //if (this.OrdenServicioID > 0)
                    //{
                        if (this.lstservicios.Count > 0)
                        {
                            iTextSharp.text.Font oFontConcepto = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 11, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
                            iTextSharp.text.Font oFontTittle = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                            iTextSharp.text.Font oFontSubtittle = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 11, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                            iTextSharp.text.Font oespacios = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 1, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);


                            #region Formato & armado de documento PDF
                            Double dCosto = costoTotal;
                            Double dImpuesto = Convert.ToDouble(OrdenServicio.Impuesto);
                            Double dSubtotal = dCosto - dImpuesto;
                            Double dTotal = dSubtotal + dImpuesto;

                            Document oDocumento;
                            oDocumento = new Document(PageSize.A4, 20, 20, 25, 10);
                            oPDFWrite = PdfWriter.GetInstance(oDocumento, new FileStream(PathPDF, FileMode.Create));
                            oDocumento.Open();
                            oPDFcb = oPDFWrite.DirectContent;
                            oPDFcb.BeginText();

                            float cellHeight = oDocumento.TopMargin;
                            iTextSharp.text.Rectangle oPage = oDocumento.PageSize;
                            oPDFcb.EndText();

                            PdfPTable oTitulo = new PdfPTable(2);
                            oTitulo.WidthPercentage = 100;
                            float[] widthHeader = { 400f, 400f };
                            oTitulo.SetWidths(widthHeader);
                            oTitulo.HorizontalAlignment = 0;

                            PdfPCell oCelda = new PdfPCell();
                            oCelda.Colspan = 2;
                            oCelda.FixedHeight = 18f;

                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "REPORTE DETALLE DE ORDEN DE SERVICIO", oFontTittle);
                            oCelda.VerticalAlignment = Element.ALIGN_LEFT;
                            oCelda.HorizontalAlignment = Element.ALIGN_LEFT;
                            oTitulo.AddCell(oCelda);

                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, string.Format("Fecha:   {0}", FechActual), oFontConcepto);
                            oCelda.VerticalAlignment = Element.ALIGN_RIGHT;
                            oCelda.HorizontalAlignment = Element.ALIGN_RIGHT;
                            oTitulo.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontSubtittle);
                            oCelda.Colspan = 2;
                            oCelda.FixedHeight = 5f;
                            oCelda.BorderWidthBottom = .5f;
                            oTitulo.AddCell(oCelda);
                            oDocumento.Add(oTitulo);

                            PdfPTable oConceptos = new PdfPTable(4);
                            oConceptos.WidthPercentage = 100;
                            float[] widthHeader2 = { 200f, 20f, 400f, 400f };
                            oConceptos.SetWidths(widthHeader2);
                            oConceptos.HorizontalAlignment = 0;

                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontSubtittle);
                            oCelda.Colspan = 2;
                            oCelda.FixedHeight = 15f;
                            oConceptos.AddCell(oCelda);

                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontSubtittle);
                            oCelda.Colspan = 2;
                            oCelda.FixedHeight = 15f;
                            oConceptos.AddCell(oCelda);
                            oCelda = new PdfPCell();


                            oDocumento.Add(oConceptos);
                            PdfPTable oDetalle = new PdfPTable(4);
                            oDetalle.WidthPercentage = 100;
                            float[] widthHeaders = { 60f, 5f, 140f, 120f };
                            oDetalle.SetWidths(widthHeaders);
                            oDetalle.HorizontalAlignment = 0;

                            oCelda = new PdfPCell();
                            oCelda.Colspan = 2;
                            oCelda.FixedHeight = 18f;
                            

                            #region CONCEPTOS
                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, "Estatus:", oFontConcepto, new BaseColor(231, 231, 233));
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontConcepto);
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, recuperaNombreServicio(OrdenServicio.EstatusOrden.EstatusOrdenID), oFontConcepto, new BaseColor(231, 231, 233));
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontConcepto);
                            oDetalle.AddCell(oCelda);

                            SAPinterface.Data.tools.tools.EspacioBlanco(ref oCelda, oespacios, oDetalle);

                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, "Proveedor:", oFontConcepto, new BaseColor(231, 231, 233));
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontConcepto);
                            oDetalle.AddCell(oCelda);

                            lstProveedores = new List<SBO_KF_OCRD_INFO>();
                            new SBO_KF_OCRD_PD().listaProveedores(ref this.oCompany, ref lstProveedores, Convert.ToInt32(OrdenServicio.ProveedorServicio.ProveedorID));
                            
                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, string.Format("{0} - {1}", lstProveedores[0].Code, lstProveedores[0].Name), oFontConcepto, new BaseColor(231, 231, 233));
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontConcepto);
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.EspacioBlanco(ref oCelda, oespacios, oDetalle);

                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, "Fecha:", oFontConcepto, new BaseColor(231, 231, 233));
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontConcepto);
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, OrdenServicio.Fecha.Value.ToString("dd/MM/yyyy"), oFontConcepto, new BaseColor(231, 231, 233));
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontConcepto);
                            oDetalle.AddCell(oCelda);

                            SAPinterface.Data.tools.tools.EspacioBlanco(ref oCelda, oespacios, oDetalle);


                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, "Folio:", oFontConcepto, new BaseColor(231, 231, 233));
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontConcepto);
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, OrdenServicio.FolioOrden.Folio == 0 ? "Desconocido" : OrdenServicio.FolioOrden.Folio.ToString(), oFontConcepto, new BaseColor(231, 231, 233));
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontConcepto);
                            oDetalle.AddCell(oCelda);

                            SAPinterface.Data.tools.tools.EspacioBlanco(ref oCelda, oespacios, oDetalle);


                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, "Descripción:", oFontConcepto, new BaseColor(231, 231, 233));
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontConcepto);
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, OrdenServicio.Descripcion, oFontConcepto, new BaseColor(231, 231, 233));
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontConcepto);
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.EspacioBlanco(ref oCelda, oespacios, oDetalle);


                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, "Método de pago:", oFontConcepto, new BaseColor(231, 231, 233));
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontConcepto);
                            oDetalle.AddCell(oCelda);
                            //String TipoPago = GetNameTipoPago(OrdenServicio.TipoDePago.TipoPagoID);
                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, OrdenServicio.TipoDePago.TipoPago, oFontConcepto, new BaseColor(231, 231, 233));
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontConcepto);
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.EspacioBlanco(ref oCelda, oespacios, oDetalle);


                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, "Detalle de pago:", oFontConcepto, new BaseColor(231, 231, 233));
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontConcepto);
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, OrdenServicio.TipoDePago.DetallePago, oFontConcepto, new BaseColor(231, 231, 233));
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontConcepto);
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.EspacioBlanco(ref oCelda, oespacios, oDetalle);

                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, "Responsable:", oFontConcepto, new BaseColor(231, 231, 233));
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontConcepto);
                            oDetalle.AddCell(oCelda);

                            lstEmpleados = new List<SAPinterface.Data.INFO.Tablas.SBO_KF_OHEM_INFO>();
                            new SBO_KF_OHEM_PD().listaEmpleadosKF(ref this.oCompany, ref lstEmpleados, Convert.ToInt32(OrdenServicio.ResponsableOrdenServicio.EmpleadoID));

                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, lstEmpleados.Count > 0 ? lstEmpleados[0].firstName : "Desconocido", oFontConcepto, new BaseColor(231, 231, 233));
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontConcepto);
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.EspacioBlanco(ref oCelda, oespacios, oDetalle);

                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, "Aprobador:", oFontConcepto, new BaseColor(231, 231, 233));
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontConcepto);
                            oDetalle.AddCell(oCelda);

                            lstEmpleados = new List<SAPinterface.Data.INFO.Tablas.SBO_KF_OHEM_INFO>();
                            new SBO_KF_OHEM_PD().listaEmpleadosKF(ref this.oCompany, ref lstEmpleados, Convert.ToInt32(OrdenServicio.AprobadorOrdenServicio.EmpleadoID));


                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, lstEmpleados.Count > 0 ? lstEmpleados[0].firstName : "Desconocido", oFontConcepto, new BaseColor(231, 231, 233));
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontConcepto);
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.EspacioBlanco(ref oCelda, oespacios, oDetalle);

                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, "Subtotal:", oFontConcepto, new BaseColor(231, 231, 233));
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontConcepto);
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, string.Format("$ {0}", dSubtotal), oFontConcepto, new BaseColor(231, 231, 233));
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontConcepto);
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.EspacioBlanco(ref oCelda, oespacios, oDetalle);

                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, "Impuesto:", oFontConcepto, new BaseColor(231, 231, 233));
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontConcepto);
                            oDetalle.AddCell(oCelda);

                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, string.Format("$ {0}", dImpuesto), oFontConcepto, new BaseColor(231, 231, 233));
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontConcepto);
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.EspacioBlanco(ref oCelda, oespacios, oDetalle);

                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, "Total:", oFontConcepto, new BaseColor(231, 231, 233));
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontConcepto);
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, string.Format("$ {0}", dTotal), oFontConcepto, new BaseColor(231, 231, 233));
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "", oFontConcepto);
                            oDetalle.AddCell(oCelda);
                            SAPinterface.Data.tools.tools.EspacioBlanco(ref oCelda, oespacios, oDetalle);

                            oDocumento.Add(oDetalle);
                            #endregion

                            PdfPTable Orden = new PdfPTable(1);
                            Orden.WidthPercentage = 100;
                            Orden.HorizontalAlignment = 0;
                            oCelda = new PdfPCell();
                            oCelda.FixedHeight = 18f;
                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, "Detalle de la orden", oFontSubtittle, new BaseColor(231, 231, 233), false);
                            Orden.AddCell(oCelda);


                            oDocumento.Add(Orden);

                            PdfPTable lstConcepto = new PdfPTable(2);
                            lstConcepto.WidthPercentage = 100;
                            float[] widthConcepto = { 600f, 600f  };
                            lstConcepto.SetWidths(widthConcepto);
                            lstConcepto.HorizontalAlignment = 0;

                            oCelda = new PdfPCell();
                            oCelda.Colspan = 2;
                            oCelda.FixedHeight = 18f;

                            //SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, "Vehículo", oFontSubtittle, new BaseColor(174, 174, 174), false);
                            //lstConcepto.AddCell(oCelda);

                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, "Servicio", oFontSubtittle, new BaseColor(174, 174, 174), false);
                            lstConcepto.AddCell(oCelda);

                            SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, "Costo", oFontSubtittle, new BaseColor(174, 174, 174), false);
                            lstConcepto.AddCell(oCelda);
                             
                            int iLine = 0;
                            if (this.lstservicios.Count == 0)
                                throw new Exception("No se han encontrado servicios agregados");

                            foreach (DetalleOrden svc in this.lstservicios)
                            {
                                iLine++;
                                var BaseOcolor = new BaseColor(231, 231, 231);
                                if (iLine % 2 == 0)
                                {
                                    BaseOcolor = new BaseColor(207, 207, 207);
                                }

                                //SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, Vehiculo(svc.Mantenible.MantenibleID), oFontConcepto, BaseOcolor);
                                //lstConcepto.AddCell(oCelda);

                                SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, svc.Servicio.Nombre, oFontConcepto, BaseOcolor, false);
                                lstConcepto.AddCell(oCelda);

                                SAPinterface.Data.tools.tools.estiloCeldaColor(ref oCelda, string.Format("$ {0}", svc.Costo.ToString("0.00")), oFontConcepto, BaseOcolor, false);
                                lstConcepto.AddCell(oCelda);

                            }

                            PdfPTable oBarraFirma = new PdfPTable(1);
                            oBarraFirma.WidthPercentage = 60;
                            oBarraFirma.HorizontalAlignment = Element.ALIGN_CENTER;
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "      ", oFontSubtittle);
                            oCelda.VerticalAlignment = Element.ALIGN_CENTER;
                            oCelda.HorizontalAlignment = Element.ALIGN_CENTER;
                            oCelda.PaddingTop += 100;
                            oCelda.BorderWidthBottom = .2f;
                            oBarraFirma.AddCell(oCelda);

                            PdfPTable oFirma = new PdfPTable(1);
                            oFirma.WidthPercentage = 60;
                            oFirma.HorizontalAlignment = Element.ALIGN_CENTER;
                            SAPinterface.Data.tools.tools.estiloSBorde(ref oCelda, "NOMBRE & FIRMA", oFontSubtittle);
                            oCelda.VerticalAlignment = Element.ALIGN_CENTER;
                            oCelda.HorizontalAlignment = Element.ALIGN_CENTER;
                            oCelda.PaddingTop += 2;
                            oFirma.AddCell(oCelda);


                            oDocumento.Add(lstConcepto);
                            oDocumento.Add(oBarraFirma);
                            oDocumento.Add(oFirma);
                            oDocumento.Close();
                            oPDFWrite.Close();
                            #endregion Formato & armado de documento PDF

                            if (bStart)
                                System.Diagnostics.Process.Start(PathPDF);
                        }
                        else oTools.ColocarMensaje("Proporciona información antes de generar el PDF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                    //}
                    //else oTools.ColocarMensaje("El PDF no puede ser visualizado durante la solicitud ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                }
                else oTools.ColocarMensaje(response, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
            catch (Exception ex)
            {
                PathPDF = string.Empty;
                KananSAP.Helper.KananSAPHerramientas.LogError("SBO_KF_SolicitaOrdenesView.cs->ExportaPDF()", ex.Message);
                oTools.ColocarMensaje(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            }
        }
        void GetChooseList(string smood, string sValue)
        {
            List<SBO_KF_OITM_INFO> lstArt = new List<SBO_KF_OITM_INFO>();
            SBO_KF_OITM_AD oItems = new SBO_KF_OITM_AD();
            SBO_KF_OHEM_PD oEmpleado = new SBO_KF_OHEM_PD();
            if (smood == "CFLTOOL" || smood == "CFArt" || smood == "CFL10")
            {
                try
                {

                    oItems.buscaArticulo(ref this.oCompany, ref lstArt, sValue);
                    if (lstArt.Count > 0)
                    {
                        if (smood == "CFLTOOL")
                        {
                            this.oForm.DataSources.UserDataSources.Item("udttool").Value = lstArt[0].ItemName.ToString();
                            articuloID = lstArt[0].iIdArticulo;
                        }
                        else if (smood == "CFArt")
                            this.oForm.DataSources.UserDataSources.Item("UDCStoAd").Value = lstArt[0].AvgPrice.ToString();
                        else if (smood == "CFL10")
                            articuloID = lstArt[0].iIdArticulo;
                    }
                }
                catch { }
            }
        }
        public bool isEfectivo()
        {
            try
            {
                oItem = oForm.Items.Item("comboMPOSv");
                ComboBox cbx = oItem.Specific;
                string val = cbx.Value;
                if (!string.IsNullOrEmpty(val))
                {
                    if (val.Equals("4"))
                        return false;
                    else
                        return true;
                }
                return false;
            }
            catch (Exception ex) { return false; }
        }
        public void HabilitaDetallePago(bool bHabilita)
        {
            oItem = oForm.Items.Item("lblDetOS");
            oItem.Visible = bHabilita;
            oItem = oForm.Items.Item("txtDetOSv");
            oItem.Visible = bHabilita;
            oItem = oForm.Items.Item("lblImpOSv");
            oItem.Click(BoCellClickType.ct_Regular);
        }
        void BloqueaComboVehiculo(bool bLock)
        {
            try
            {

                Thread.Sleep(15);
                oItem = this.oForm.Items.Item("comboVehOS");
                oCombo = this.oForm.Items.Item("comboVehOS").Specific;
                oCombo.Item.Enabled = bLock;
                Thread.Sleep(15);
                //oForm.DataSources.UserDataSources.Item("comboVehOS")
                
                if (bLock)
                {
                    var i = this.SBO_Application.MessageBox("Al cambiar la selección de vehículo los registros seleccionados serán omitidos. ¿Deseas cambiar la selección de los vehículos?.", 1, "Ok");
                    if (i == 1)
                    {
                        this.lstservicios = new List<DetalleOrden>();
                        oCombo = oItem.Specific;
                        oCombo.Select("0", BoSearchKey.psk_ByValue);
                        this.FillGridServiciosFromList();
                    }
                }
            }
            catch { }
        }
        void AsignaImpuesto()
        {
            try
            {
                oItem = this.oForm.Items.Item("comboProOS");
                oCombo = (SAPbouiCOM.ComboBox)(oItem.Specific);
                //ProveedorID = Convert.ToInt32(oCombo.Selected.Value);
            }
            catch
            { 
            
            }
            
            

           /* double? IVA = 0;
            //Comentado porque cuando se limpia el formulario llama al evento del combo e intenta traer los valores de fechas
            //por lo que al limpiar algunos campos del XML alguna informaci�n llega como null en el BO.

            //Se utilizar� un m�todo que obtenga ProveedorID desde la interfaz.
            //this.view.FormToEntity();
            int? ProveedorID = this.view.GetProveedorFromXML();

            if (ProveedorID != 0)
            {
                IVA = this.view.Entity.GetIVAByProveedorID(ProveedorID);
                if (IVA != null || IVA > 0.0)
                {
                    this.view.SetIVA(IVA);
                }
                else
                {
                    this.view.SetIVA(0.0);
                    SBO_Application.StatusBar.SetText("Impuesto no valido, favor de validar. ", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Warning);
                }
            }
            else
            {
                this.view.SetIVA(0.0);
                SBO_Application.StatusBar.SetText("Impuesto no valido, favor de validar. ", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Warning);
            }*/
        }
        #endregion acciones disparadas desde evento control, botones, combos, choose list, etc.

        #region Determina el estatus para la orden
        public string recuperaNombreServicio(int? EstatusOrdenID)
        {
            string Servicio = "";

            switch (EstatusOrdenID)
            {
                case 1:
                    Servicio = "Servicio solicitado";
                    break;
                case 2:
                    Servicio = "Solicitud aprobada";
                    break;
                case 3:
                    Servicio = "Solicitud eliminada";
                    break;
                case 5:
                    Servicio = "Solicitud atendida";
                    break;
                case 6:
                    Servicio = "Solicitud realizada";
                    break;
            }

            return Servicio;
        }
        private void PrepareOrdenSolicitada()
        {
            try
            {
                string[] lockitems = { "comboMPOSv", "txtDetOSv", "txtFolioOS" }; //
                string[] elementos = { "4", "5" };
                this.LimitaEstatus(elementos);

                try
                {
                    SelectComboBoxValue("cmbestatus", "1");
                    oItem = this.oForm.Items.Item("cmbestatus");
                    oItem.Enabled = false;
                }
                catch { }

                try
                {
                    foreach (string item in lockitems)
                    {
                        try
                        {
                            oItem = this.oForm.Items.Item(item);
                            oItem.Enabled = false;
                        }
                        catch { }
                    }
                }
                catch { }


                oItem = this.oForm.Items.Item("lblDetOS");
                oItem.Visible = false;
                oItem = this.oForm.Items.Item("txtDetOSv");
                oItem.Visible = false; 
            }
            catch (Exception ex) {
            
            }
        }


        /// <summary>
        /// Establece los campos de la ventana para orden de servicio aprobada
        /// </summary>
        private void PrepareOrdenAprobada()
        {
            try
            {
                string[] lockitems = { "txtFechaOS", "txtEnPro", "txthEntr", "txtSalPro", "txtHsal", "comboMPOSv", "txtDetOSv", "txtFolioOS", "txtHoraOS" }; //, "txtSalReal", "txtEnReal", "txtHReale", "txtHSalr"
                string[] elementos = { "1", "3", "4" };
                this.LimitaEstatus(elementos);
                try
                {
                    SelectComboBoxValue("cmbestatus", "2");
                    oItem = this.oForm.Items.Item("Item_3");
                    oItem.Enabled = false;
                }
                catch { }

                 try
                {
                    foreach (string item in lockitems)
                    {
                        try
                        {
                            oItem = this.oForm.Items.Item(item);
                            oItem.Enabled = false;
                        }
                        catch { }
                    }
                }
                catch { }

                 oItem = this.oForm.Items.Item("lblDetOS");
                 oItem.Visible = false;
                 oItem = this.oForm.Items.Item("txtDetOSv");
                 oItem.Visible = false; 
                
            }
            catch (Exception ex) { 
            }
        }

        /// <summary>
        /// Establece los campos de la ventana para orden de servicio atendida
        /// </summary>
        private void PrepareOrdenAtendida()
        {
            string[] lockitems = { "cmbrutina","cmbsuc","txtFechaOS","txtEnPro","txthEntr","txtSalPro","txtHsal",
                                     "comboMPOSv","txtDetOSv","txtDescOs","cmbResp","cmbAprob","txtClisap","txtInspec","txtFolioOS", "txtHoraOS" };
            string[] elementos = { "1", "4" };
            //string[] lstItems = { "btnRefn", "btnDelRf", "btnAddCto", "btnDelcto", "btnaddtool", "btndeltool", "btnaddmec", "btndelmeca" };  //, "txtSalReal", "txtEnReal", "txtHReale", "txtHSalr"
            string[] lstItems = { "btnAddCto", "btnDelcto", "btnaddtool", "btndeltool", "btnaddmec", "btndelmeca" };  //, "txtSalReal", "txtEnReal", "txtHReale", "txtHSalr"
            this.LimitaEstatus(elementos);

            foreach (string oitem in lstItems)
            {
                try
                {
                    KananSAP.Helper.KananSAPHelpers.DeshabilitaItem(this.oForm, oitem);
                }
                catch { }
            }
            try
            {
                SelectComboBoxValue("cmbestatus", "5");
                try
                {
                    foreach (string item in lockitems)
                    {
                        try
                        {
                            oItem = this.oForm.Items.Item(item);
                            oItem.Enabled = false;
                        }
                        catch { }
                    }
                }
                catch { }

                oItem = this.oForm.Items.Item("lblDetOS");
                oItem.Visible = false;
                oItem = this.oForm.Items.Item("txtDetOSv");
                oItem.Visible = false; 
            }
            catch (Exception ex) { }
        }

        /// <summary>
        /// Establece los campos de la ventana para orden de servicio realizada
        /// </summary>
        private void PrepareOrdenRealizada()
        {
            string[] lockitems = { "cmbSyscr", "cmbCompo", "cmbrutina", "cmbsuc", "comboProOS", "txtFechaOS", "cmblevel", "cmbestatus", "txtEnPro", "txthEntr", "txtSalPro", "txtHsal", "txtEnReal", "txtHReale", "txtSalReal", "txtHSalr", "cmbAprob", "txtInspec", "txtClisap", "cmbResp", "txtDescOs", "txtDetOSv", "comboMPOSv", "txtFolioOS",
                                     "btnOSvDone","btnEdtOSv","btnDelOSv","btnVchg","btnRefn",
                                     "btnDelRf","btnAddCto","btnDelcto",
                                     "btnaddtool","btndeltool","btnaddmec","btndelmeca", "txtHoraOS","txtFolioOS" };  //, "txtSalReal", "txtEnReal", "txtHReale", "txtHSalr"

            try
            {
                SelectComboBoxValue("cmbestatus", "6");

                 try
                {
                    foreach (string item in lockitems)
                    {
                        try
                        {
                            oItem = this.oForm.Items.Item(item);
                            oItem.Enabled = false;
                        }
                        catch { }
                    }
                }
                catch { }

                oItem = this.oForm.Items.Item("lblDetOS");
                oItem.Visible = false;
                oItem = this.oForm.Items.Item("txtDetOSv");
                oItem.Visible = false; 

                /*oEditText = this.oForm.Items.Item("txtRefSAP").Specific;//Refacci�n
                oEditText.Item.Enabled = false;
                oEditText = this.oForm.Items.Item("txtFechaOS").Specific;//Fecha
                oEditText.Item.Enabled = false;
                oEditText = this.oForm.Items.Item("txtFolioOS").Specific;//Folio
                oEditText.Item.Enabled = false;
                oEditText = this.oForm.Items.Item("txtDescOs").Specific;//Descripcion
                oEditText.Item.Enabled = false;
                oEditText = this.oForm.Items.Item("txtQntyRf").Specific;//Catidad refacciones
                oEditText.Item.Enabled = false;
                oEditText = this.oForm.Items.Item("txtCostoRf").Specific;//Costo refacciones
                oEditText.Item.Enabled = false;
                oEditText = this.oForm.Items.Item("txtCostoOS").Specific;//Costo
                oEditText.Item.Enabled = false;
                oEditText = this.oForm.Items.Item("lblImpOSv").Specific;//Impuesto
                oEditText.Item.Enabled = false;

                //oFolder = this.oForm.Items.Item("tabDataOrd").Specific;
                //oFolder.Select();
                //oFolder.Item.Enabled = true;
                //oFolder = this.oForm.Items.Item("tabDataSer").Specific;
                //oFolder.Item.Enabled = true;
                //oFolder = this.oForm.Items.Item("tabDataRef").Specific;
                //oFolder.Item.Enabled = true;

                oItem = this.oForm.Items.Item("txtDetOSv");//DetallePago
                oItem.Enabled = false;

                oLabel = this.oForm.Items.Item("lblCostV").Specific;
                oLabel.Item.Enabled = false;

                oCombo = this.oForm.Items.Item("cmbResp").Specific;//Combo Responsable
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("cmbsuc").Specific;//Combo Sucursal
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("cmbAprob").Specific;//Combo Aprobador
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("comboProOS").Specific;//Combo Proveedor
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("comboMPOSv").Specific;//Combo Metodo de pago
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("comboVehOS").Specific;//Combo Vehiculo
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("comoServOS").Specific;//Combo Servicio
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("cmbestatus").Specific;//Combo Estatus
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("cmbAlmacn").Specific;//Combo Almacen refacciones
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("cmbRefKF").Specific;//Combo refacci�nes
                oCombo.Item.Enabled = false;

                oButton = this.oForm.Items.Item("btnOSvDone").Specific;
                oButton.Item.Enabled = false;
                oButton = this.oForm.Items.Item("btnDelOSv").Specific;
                oButton.Item.Enabled = false;
                oButton = this.oForm.Items.Item("btnEdtOSv").Specific;
                oButton.Item.Enabled = false;
                oButton = this.oForm.Items.Item("btnSaveOS").Specific;
                oButton.Item.Enabled = false;
                oButton = this.oForm.Items.Item("btnRefn").Specific;/*Bot�n agregar refacci�n*/
                /*oButton.Item.Enabled = false;
                oButton = this.oForm.Items.Item("btnDelRf").Specific;/*Bot�n borrar refacci�n*/
               /* oButton.Item.Enabled = false;

                oItem = oForm.Items.Item("CBTI");
                oCheck = (CheckBox)(oItem.Specific);
                oCheck.Item.Enabled = false;*/
            }
            catch (Exception ex) { 
            }
        }

        /// <summary>
        /// Establece los campos de la ventana de una Orden de Servicio rechazada
        /// </summary>
        private void PrepareOrdenRechazada()
        {
            try
            {
                SelectComboBoxValue("cmbestatus", "3");
                oEditText = this.oForm.Items.Item("txtFechaOS").Specific;//Fecha
                oEditText.Item.Enabled = false;
                oEditText = this.oForm.Items.Item("txtHoraOS").Specific;//Hora Servicio
                oEditText.Item.Enabled = false;
                
                oEditText = this.oForm.Items.Item("txtFolioOS").Specific;//Folio
                oEditText.Item.Enabled = false;
                oEditText = this.oForm.Items.Item("txtDescOs").Specific;//Descripcion
                oEditText.Item.Enabled = false;

                oCombo = this.oForm.Items.Item("comboMPOSv").Specific;//Combo Metodo de pago
                oCombo.Item.Enabled = false;
                oItem = this.oForm.Items.Item("txtDetOSv");//DetallePago
                oItem.Enabled = false;
                oEditText = this.oForm.Items.Item("lblImpOSv").Specific;//Impuesto
                oEditText.Item.Enabled = false;

                oLabel = this.oForm.Items.Item("lblCostV").Specific;
                oLabel.Item.Enabled = false;

                oCombo = this.oForm.Items.Item("cmbResp").Specific;//Combo Responsable
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("cmbsuc").Specific;//Combo Sucursal
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("cmbAprob").Specific;//Combo Aprobador
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("comboProOS").Specific;//Combo Proveedor
                oCombo.Item.Enabled = false;

                oEditText = this.oForm.Items.Item("txtCostoOS").Specific;//Costo
                oEditText.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("comboVehOS").Specific;//Combo Vehiculo
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("comoServOS").Specific;//Combo Servicio
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("cmbestatus").Specific;//Combo Estatus
                oCombo.Item.Enabled = false;

                //oFolder = this.oForm.Items.Item("tabDataOrd").Specific;
                //oFolder.Select();
                

                oButton = this.oForm.Items.Item("btnOSvDone").Specific;
                oButton.Item.Enabled = false;

                oButton = this.oForm.Items.Item("btnDelOSv").Specific;
                oButton.Item.Enabled = false;

                oButton = this.oForm.Items.Item("btnEdtOSv").Specific;
                oButton.Item.Enabled = false;
                oButton = this.oForm.Items.Item("btnSaveOS").Specific;
                oButton.Item.Enabled = false;

                oButton = this.oForm.Items.Item("btnDelRf").Specific;
                oButton.Item.Enabled = false;

                oButton = this.oForm.Items.Item("btnRefn").Specific;
                oButton.Item.Enabled = false;

                //oFolder = this.oForm.Items.Item("tabDataRef").Specific;
                //oFolder.Item.Visible = false;

                oItem = oForm.Items.Item("CBTI");
                oCheck = (CheckBox)(oItem.Specific);
                oCheck.Item.Enabled = false;
            }
            catch (Exception ex) { 
            }
        }

        /// <summary>
        /// Establece los campos de la ventana de ua Orden de Servicio Eliminada
        /// </summary>
        private void PrepareOrdenEliminada()
        {
            try
            {

                string[] lstItems = { "cmbestatus", "btnPDF", "btnSaveOS", "txtFechaOS", "txtFolioOS", "txtDescOs", "comboMPOSv", "txtDetOSv", "lblImpOSv", "lblCostV", "cmbResp", "cmbsuc", "cmbAprob", "comboProOS", "txtCostoOS", "comboVehOS", "comoServOS", "btnOSvDone", "btnDelOSv", "btnEdtOSv", "btnDelRf", "btnRefn", "lblImpOSv", "btnRefn", "btnDelRf", "btnAddCto", "btnDelcto", "btnaddtool", "btndeltool", "btnaddmec", "btndelmeca", "txtHoraOS" };
                try
                {
                    SelectComboBoxValue("cmbestatus", "4");
                }
                catch { }
                foreach (string oitem in lstItems)
                {
                    try
                    {
                        KananSAP.Helper.KananSAPHelpers.DeshabilitaItem(this.oForm, oitem);
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
            }
        }
        void LimitaEstatus(string[] elementos)
        {
            oCombo = this.oForm.Items.Item("cmbestatus").Specific as ComboBox;
            try
            {
                foreach (string estatus in elementos)
                    oCombo.ValidValues.Remove(estatus);
            }
            catch { }
        }
        #endregion

        #region Llenado de objetos (choose from list & combos)

        private void FillComboTipoPago()
        {
            try
            {
                string query = @"select * from ""@VSKF_TIPODEPAGO"" where U_Estatus = 1";
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    oItem = this.oForm.Items.Item("comboMPOSv");
                    oCombo = oItem.Specific;
                    KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Ninguno");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("Code").Value);
                        string description = rs.Fields.Item("Name").Value;
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }
                    
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void FillComboSucursales()
        {
            try
            {
                string query = @"select * from ""@VSKF_SUCURSAL""";
                //query += string.Format(" where U_Propietario = {0} ", this.Config.UserFull.Dependencia.EmpresaID);
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    oItem = this.oForm.Items.Item("cmbsuc");
                    oCombo = oItem.Specific;
                    KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);//this.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Ninguno");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("U_SUCURSALID").Value);
                        string description = rs.Fields.Item("U_DIRECCION").Value;
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }
                   
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void FillComboProveedores()
        {
            try
            {
                List<SBO_KF_OCRD_INFO> lstProveedores = new List<SBO_KF_OCRD_INFO>();
                SBO_KF_OCRD_PD controlProveedor = new SBO_KF_OCRD_PD();
                string response = controlProveedor.listaProveedores(ref oCompany, ref lstProveedores);

                if (response == "OK")
                {
                    oItem = this.oForm.Items.Item("comboProOS");
                    oCombo = oItem.Specific;
                    KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Ninguno");

                    foreach (SBO_KF_OCRD_INFO oProveedor in lstProveedores)
                        oCombo.ValidValues.Add(oProveedor.ProveedorID.ToString(), oProveedor.U_Nombre);
                }
                else throw new Exception("No se han registrado proveedores");
                
                #region version anterior
                /*string query = @"select * from ""@VSKF_PROVEEDOR""";
                //query += string.Format(" where U_Propietario = {0} ", this.Config.UserFull.Dependencia.EmpresaID);
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    oItem = this.oForm.Items.Item("comboProOS");
                    oCombo = oItem.Specific;
                    KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);//this.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Ninguno");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("U_ProveedorID").Value);
                        string description = rs.Fields.Item("U_Nombre").Value;
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }
                    
                }*/
                #endregion version anterior
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void FillComboServicios()
        {
            try
            {

                SBO_KF_ORDENSERVICIO_PD controlOS = new SBO_KF_ORDENSERVICIO_PD();
                List<DetalleOrden> lstServicioAuto = new List<DetalleOrden>();
                SBO_KF_DOCUMENTS_LINE_INFO oDocLine = new SBO_KF_DOCUMENTS_LINE_INFO();
                string response = controlOS.listaServiciosAutomotriz(ref this.oCompany, ref lstServicioAuto);

                if (response == "OK")
                {
                    oItem = this.oForm.Items.Item("comoServOS");
                    oCombo = oItem.Specific;
                    KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Ninguno");
                    foreach (DetalleOrden oServicio in lstServicioAuto)                    
                        oCombo.ValidValues.Add( oServicio.Servicio.ServicioID.ToString(), oServicio.Servicio.Nombre);                    

                }
                else throw new Exception("No se han registrado servicos");
                #region version anterior
                //               if (response != "OK")

                /*ServicioSAP svData = new ServicioSAP(this.oCompany);
                Servicio sv = new Servicio();
                sv.Propietario = new Kanan.Operaciones.BO2.Empresa();
                sv.Propietario.EmpresaID = this.oConnfig.UserFull.Dependencia.EmpresaID;
                SAPbobsCOM.Recordset rs = svData.Consultar(sv);
                if (svData.HashServicios(rs))
                {
                    oItem = this.oForm.Items.Item("comoServOS");
                    oCombo = oItem.Specific;
                    KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);//this.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Ninguno");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("U_ServicioID").Value);
                        string description = rs.Fields.Item("Servicio").Value;
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }
                    
                }*/
                #endregion version anterior
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        private void FillComboVehiculos()
        {
            try
            {
                string query = string.Empty;
                query = @" select vh.U_VehiculoID, vh.U_Nombre from ""@VSKF_VEHICULO"" vh ";
                if (this.oConnfig.UserFull.Dependencia.EmpresaID.HasValue)
                    query += string.Format(" where vh.U_EmpresaID = {0} ", this.oConnfig.UserFull.Dependencia.EmpresaID);
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    oItem = this.oForm.Items.Item("comboVehOS");
                    oCombo = oItem.Specific;
                    KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);//this.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Ninguno");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("U_VehiculoID").Value);
                        string description = rs.Fields.Item("U_Nombre").Value;
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }
                    
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        private void FillComboVehiculosFiltradoXSucursal(string sucursalid)
        {
            try
            {
                string query = string.Empty;
                query = @" select vh.U_VehiculoID, vh.U_Nombre from ""@VSKF_VEHICULO"" vh ";
                if (this.oConnfig.UserFull.Dependencia.EmpresaID.HasValue)
                    query += string.Format(" where vh.U_EmpresaID = {0} and vh.U_SucursalID = {1}", this.oConnfig.UserFull.Dependencia.EmpresaID, sucursalid);
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    oItem = this.oForm.Items.Item("comboVehOS");
                    oCombo = oItem.Specific;
                    KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);//this.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Ninguno");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("U_VehiculoID").Value);
                        string description = rs.Fields.Item("U_Nombre").Value;
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }

                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        private void FillComboVehiculosFiltrados(int? TipoVehiculoID)
        {
            try
            {
                string query = string.Empty;
                query = @" select vh.U_VehiculoID, vh.U_Nombre from ""@VSKF_VEHICULO"" vh ";
                if (this.oConnfig.UserFull.Dependencia.EmpresaID.HasValue)
                    query += string.Format(" where vh.U_EmpresaID = {0} ", this.oConnfig.UserFull.Dependencia.EmpresaID);

                if (TipoVehiculoID != null && TipoVehiculoID >0)
                {
                    query += string.Format(" and vh.U_TipoVeID = {0} ", TipoVehiculoID);
                }
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    oItem = this.oForm.Items.Item("comboVehOS");
                    oCombo = oItem.Specific;
                    KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);//this.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Ninguno");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("U_VehiculoID").Value);
                        string description = rs.Fields.Item("U_Nombre").Value;
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }

                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        private void DevuelveVehiculoSeleccionado()
        {
            try
            {
                string query = string.Empty;
                query = @" select vh.code, vh.Name, vh.U_VEHICULOID, vh.U_SUCURSALID  from ""@VSKF_VEHICULO"" vh ";
                if (this.oConnfig.UserFull.Dependencia.EmpresaID.HasValue)
                    query += string.Format(" where vh.U_EmpresaID = {0} ", this.oConnfig.UserFull.Dependencia.EmpresaID);
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    oItem = this.oForm.Items.Item("comboVehOS");
                    oCombo = oItem.Specific;
                    KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);//this.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Ninguno");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("U_VehiculoID").Value);
                        string description = rs.Fields.Item("U_Nombre").Value;
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }
                    
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }


        private void FillComboEmpleados()
        {
            try
            {
                string query = string.Empty;
                query = @" select e.""Name"", e.U_KFEmpleadoID from ""@VSKF_RELEMPLEADO"" e ";
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    oItem = this.oForm.Items.Item("cmbResp");
                    oCombo = oItem.Specific;
                    ComboBox cmb = this.oForm.Items.Item("cmbAprob").Specific;
                    KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);
                    KananSAP.Helper.KananSAPHelpers.CleanComboBox(cmb);
                    oCombo.ValidValues.Add("0", "Ninguno");
                    cmb.ValidValues.Add("0", "Ninguno");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("U_KFEmpleadoID").Value);
                        string description = rs.Fields.Item("Name").Value;
                        oCombo.ValidValues.Add(value, description);
                        cmb.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }
                    
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        private void FillComboEstatus()
        {
            try
            {
                string query = @"select * from ""@VSKF_ESTATUSORDEN"" where U_Activo = 1";
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    oItem = this.oForm.Items.Item("cmbestatus");
                    oCombo = oItem.Specific;
                    KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo); //this.CleanComboBox(oCombo);
                    //oCombo.ValidValues.Add("0", "Ninguno");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("Code").Value);
                        string description = rs.Fields.Item("Name").Value;
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }

                }
                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        
        private void FillComboMecanicos()
        {
            try
            {
                SBO_KF_OHEM_PD oMecanicos = new SBO_KF_OHEM_PD();
                List<SAPinterface.Data.INFO.Tablas.SBO_KF_OHEM_INFO> lstmecanicos = new List<SAPinterface.Data.INFO.Tablas.SBO_KF_OHEM_INFO>();
                oMecanicos.listaEmpleados(ref oCompany, ref lstmecanicos);
                if (lstmecanicos.Count > 0)
                {
                    oItem = this.oForm.Items.Item("cmbmeca");
                    oCombo = oItem.Specific;
                    KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Selecciona un mecánico");

                    foreach (SAPinterface.Data.INFO.Tablas.SBO_KF_OHEM_INFO oHEM in lstmecanicos)
                        oCombo.ValidValues.Add(oHEM.empID.ToString(), string.Format("{0} {1} {2}", oHEM.firstName, oHEM.middleName, oHEM.lastName));

                    
                    SelectComboBoxValue("cmbmeca", "0");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void FillComboSistemas()
        {
            try
            {
                List<SBO_KF_CRAIZ_INFO> lstResultados = new List<SBO_KF_CRAIZ_INFO>();
                SBO_KF_CausaRaiz_PD oCausapd = new SBO_KF_CausaRaiz_PD();
                SBO_KF_CRAIZ_INFO otemp = new SBO_KF_CRAIZ_INFO();
                otemp.Activo = true;
                oCausapd.listaCausaraiz(ref oCompany, ref lstResultados, otemp, false);
                if (lstResultados.Count > 0)
                {
                    oItem = this.oForm.Items.Item("cmbSyscr");
                    oCombo = oItem.Specific;
                    KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Selecciona un sistema");

                    foreach (SBO_KF_CRAIZ_INFO oSistema in lstResultados)
                        oCombo.ValidValues.Add(oSistema.Name, oSistema.NombreSistema);
                }
            }
            catch { }
        }
        void CombosStyle()
        {
            foreach (string combo in combos)
            {
                try
                {
                    if (combo.Equals("cmbCompo", StringComparison.InvariantCultureIgnoreCase))
                        continue;
                    oItem = this.oForm.Items.Item(combo);
                    oCombo = oItem.Specific;
                    oCombo.Item.DisplayDesc = true;
                }
                catch { continue; }
            }
        }
        void ChooseRecurso()
        {
            try
            {
                oForm.DataSources.UserDataSources.Add("EdtCstoAr", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);
                SAPbouiCOM.ChooseFromListCollection oCFLs = null;
                oCFLs = oForm.ChooseFromLists;
                SAPbouiCOM.ChooseFromList oCFL = null;
                SAPbouiCOM.ChooseFromListCreationParams oCFLCreationParams = null;
                oCFLCreationParams = ((SAPbouiCOM.ChooseFromListCreationParams)(SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)));
                oCFLCreationParams.MultiSelection = false;
                oCFLCreationParams.ObjectType = "290";
                oCFLCreationParams.UniqueID = "CFSrc";
                oCFL = oCFLs.Add(oCFLCreationParams);
                oItem = oForm.Items.Item("txtADesc2");
                EditText oEditText = (EditText)(oItem.Specific);
                oEditText.DataBind.SetBound(true, "", "EdtCstoAr");
                oEditText.ChooseFromListUID = "CFSrc";
                oEditText.ChooseFromListAlias = "ResName";
            }
            catch { }
        }
        
        void ChooseSocioNegocio()
        {
            try
            {
                oForm.DataSources.UserDataSources.Add("EdtSoci", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);
                SAPbouiCOM.ChooseFromListCollection oCFLs = null;
                oCFLs = oForm.ChooseFromLists;
                SAPbouiCOM.ChooseFromList oCFL = null;
                SAPbouiCOM.ChooseFromListCreationParams oCFLCreationParams = null;
                oCFLCreationParams = ((SAPbouiCOM.ChooseFromListCreationParams)(SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)));
                oCFLCreationParams.MultiSelection = false;
                oCFLCreationParams.ObjectType = "2";
                oCFLCreationParams.UniqueID = "CFSoc";
                oCFL = oCFLs.Add(oCFLCreationParams);
                SAPbouiCOM.Conditions oConditions = this.SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_Conditions);
                oConditions = oCFL.GetConditions();
                SAPbouiCOM.Condition oCondition = oConditions.Add();
                oCondition.Alias = "CardType";
                oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
                oCondition.CondVal = "C";
                oCFL.SetConditions(oConditions);
                oItem = oForm.Items.Item("txtClisap");
                EditText oEditText = (EditText)(oItem.Specific);
                oEditText.DataBind.SetBound(true, "", "EdtSoci");
                oEditText.ChooseFromListUID = "CFSoc";
                oEditText.ChooseFromListAlias = "CardCode";
            }
            catch { }
        }
        
        void ChooseArticulo()
        {
            try
            {
                oForm.DataSources.UserDataSources.Add("EdtRSCto", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);
                SAPbouiCOM.ChooseFromListCollection oCFLs = null;
                oCFLs = oForm.ChooseFromLists;
                SAPbouiCOM.ChooseFromList oCFL = null;
                SAPbouiCOM.ChooseFromListCreationParams oCFLCreationParams = null;
                oCFLCreationParams = ((SAPbouiCOM.ChooseFromListCreationParams)(SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)));
                oCFLCreationParams.MultiSelection = false;
                oCFLCreationParams.ObjectType = "4";
                oCFLCreationParams.UniqueID = "CFArt";
                oCFL = oCFLs.Add(oCFLCreationParams);
                oItem = oForm.Items.Item("txtADesc3");
                EditText oEditText = (EditText)(oItem.Specific);
                oEditText.DataBind.SetBound(true, "", "EdtRSCto");
                oEditText.ChooseFromListUID = "CFArt";
                oEditText.ChooseFromListAlias = "ItemName";
                //oItem = this.oForm.Items.Item("txtADesc3");
                //oItem.Visible = false;
            }
            catch { }
        }
        public void AddItemsCFL()
        {
            try
            {


                Item oItem = null;
                oForm.DataSources.UserDataSources.Add("EditDS", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);
                SAPbouiCOM.ChooseFromListCollection oCFLs = null;
                oCFLs = oForm.ChooseFromLists;
                SAPbouiCOM.ChooseFromList oCFL = null;
                SAPbouiCOM.ChooseFromListCreationParams oCFLCreationParams = null;
                oCFLCreationParams = ((SAPbouiCOM.ChooseFromListCreationParams)(SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)));
                oCFLCreationParams.MultiSelection = false;
                oCFLCreationParams.ObjectType = "4";
                oCFLCreationParams.UniqueID = "CFL10";

                oCFL = oCFLs.Add(oCFLCreationParams);
                SAPbouiCOM.Conditions oConditions = this.SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_Conditions);
                oConditions = oCFL.GetConditions();
                SAPbouiCOM.Condition oCondition = oConditions.Add();
                
                //oCondition.Alias = "ItmsGrpCod";
                oCondition.Alias = "U_KF_TIPART";
                oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
                //oCondition.CondVal = new GestionGlobales().iFiltroGrupo(this.oCompany, "REFACCIONES KF");
                oCondition.CondVal = "2";
                oCFL.SetConditions(oConditions);
                oItem = oForm.Items.Item("txtRefSAP");
                EditText oEditText = (EditText)(oItem.Specific);
                oEditText.DataBind.SetBound(true, "", "EditDS");
                oEditText.ChooseFromListUID = "CFL10";
                oEditText.ChooseFromListAlias = "ItemCode";



            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError("OrdenServicioSinAlertaView.cs->AddItemsCFL()", ex.Message);
                throw new Exception(ex.Message);
            }
        }
        
        public void AddChooseHerramientas()
        {
            try
            {


                Item oItem = null;
                oForm.DataSources.UserDataSources.Add("EdtODS", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);
                SAPbouiCOM.ChooseFromListCollection oCFLs = null;
                oCFLs = oForm.ChooseFromLists;
                SAPbouiCOM.ChooseFromList oCFL = null;
                SAPbouiCOM.ChooseFromListCreationParams oCFLCreationParams = null;
                oCFLCreationParams = ((SAPbouiCOM.ChooseFromListCreationParams)(SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)));
                oCFLCreationParams.MultiSelection = false;
                oCFLCreationParams.ObjectType = "4";
                oCFLCreationParams.UniqueID = "CFLTOOL";

                oCFL = oCFLs.Add(oCFLCreationParams);
                SAPbouiCOM.Conditions oConditions = this.SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_Conditions);
                oConditions = oCFL.GetConditions();
                SAPbouiCOM.Condition oCondition = oConditions.Add();
                //oCondition.Alias = "ItmsGrpCod";
                oCondition.Alias = "U_KF_TIPART";
                oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
                //oCondition.CondVal = new GestionGlobales().iFiltroGrupo(this.oCompany, "HERRAMIENTAS KF");
                oCondition.CondVal = "3"; //new GestionGlobales().iFiltroGrupo(this.oCompany, "HERRAMIENTAS KF");
                oCFL.SetConditions(oConditions);
                oItem = oForm.Items.Item("txttoolid");
                EditText oEditText = (EditText)(oItem.Specific);
                oEditText.DataBind.SetBound(true, "", "EdtODS");
                oEditText.ChooseFromListUID = "CFLTOOL";
                oEditText.ChooseFromListAlias = "ItemCode";
            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError("OrdenServicioSinAlertaView.cs->AddChooseHerramientas()", ex.Message);
                throw new Exception(ex.Message);
            }
        }
      
        #endregion Llenado de objetos (choose from list & combos)

        #region llenado & asignacion de grids
        void FillGridHerramientas()
        {
            DataTable tab = oForm.DataSources.DataTables.Item("dgvTools");
            oGrid = this.oForm.Items.Item("gvTool").Specific;
            tab.Rows.Clear();
            if (this.lstherramientas.Count > 0)
            {               
                int index = 0;
                foreach (Kanan.Mantenimiento.BO2.SBO_KF_Herramientas_INFO oTool in lstherramientas)
                {
                    tab.Rows.Add();
                    tab.SetValue("#", index, (index + 1).ToString());
                    tab.SetValue("Folio", index, oTool.ItemCode);
                    tab.SetValue("Herramienta", index, oTool.ItemName);
                    tab.SetValue("Cantidad", index, oTool.Quantity.ToString());
                    index++;
                }
                this.calculaCostoTotal();
            }
        }

        void FillGridMecanicos()
        {
            DataTable tab = oForm.DataSources.DataTables.Item("dgvMecan");
            oGrid = this.oForm.Items.Item("gvMecan").Specific;
            tab.Rows.Clear();
            if (this.lstmecanicos.Count > 0)
            {                
                int index = 0;
                foreach (Kanan.Mantenimiento.BO2.SBO_KF_OHEM_INFO oMecanico in lstmecanicos)
                {
                    tab.Rows.Add();
                    tab.SetValue("#", index, (index + 1).ToString());
                    tab.SetValue("Folio", index, oMecanico.U_EMPLEADOID);
                    tab.SetValue("Mecanico", index, oMecanico.U_NOMBRE);
                    tab.SetValue("Salario", index, oMecanico.U_SUELDO.ToString());
                    index++;
                }
                this.calculaCostoTotal();
            }
        }
        void FillGridGastosAdicionales()
        {
            DataTable tab = oForm.DataSources.DataTables.Item("dgvCstos");
            oGrid = this.oForm.Items.Item("GvCsto").Specific;
            tab.Rows.Clear();
            if (this.lstcostoadicional.Count > 0)
            {
                int index = 0;
                foreach (SBOKF_CL_CostosAdicional_INFO oCosto in lstcostoadicional)
                {
                    tab.Rows.Add();
                    tab.SetValue("#", index, (index + 1).ToString());
                    tab.SetValue("Descripción", index, oCosto.U_DESCRIPCION);
                    tab.SetValue("Costo", index, String.Format("{0:n}", oCosto.U_COSTO));
                    index++;
                }
                this.calculaCostoTotal();
            }
        }
        void FillGridServiciosFromList()
        {
            int vehiculoid = 0;
            DataTable tab = oForm.DataSources.DataTables.Item("dtServSAG");
            oGrid = this.oForm.Items.Item("gdOS").Specific;
            tab.Rows.Clear();
            if (this.lstservicios.Count > 0)
            {
                int index = 0;
                //try
                //{
                //    oItem = this.oForm.Items.Item("comboVehOS");
                //    if (oItem.Enabled)
                //    {
                //        oCombo = oItem.Specific;
                //        oCombo.Select(lstservicios[0].Vehiculo.VehiculoID.ToString(), BoSearchKey.psk_ByValue);
                //    }
                //}
                //catch { }
                
                foreach (DetalleOrden oservicio in lstservicios)
                {
                    tab.Rows.Add();
                    tab.SetValue("id", index, oservicio.DetalleOrdenID ?? 0);
                    tab.SetValue("Vehiculo", index, oservicio.Vehiculo.Nombre);
                    tab.SetValue("Servicio", index, oservicio.Servicio.Nombre);
                    tab.SetValue("Costo", index, oservicio.Costo.ToString());
                    tab.SetValue("IVA", index, 0);//impuesto.ToString());
                    tab.SetValue("ServicioID", index, oservicio.Servicio.ServicioID.ToString());
                    index++;
                    try
                    {
                        oservicio.Mantenible = new Vehiculo();
                        oservicio.Mantenible.MantenibleID = oservicio.Vehiculo.VehiculoID;
                        vehiculoid = (int)oservicio.Vehiculo.VehiculoID;
                        oservicio.MantenibleID = oservicio.Vehiculo.VehiculoID;
                        oservicio.TipoMantenibleID = 1;
                        oservicio.Sincronizado = 1;
                        oservicio.ServicioID = oservicio.Servicio.ServicioID;
                    }
                    catch { }
                }
                try
                {
                    oCombo = oForm.Items.Item("comboVehOS").Specific;
                    oCombo.Select(vehiculoid.ToString(), BoSearchKey.psk_ByValue);
                }
                catch { }
                //this.BloqueaComboVehiculo(true);
                this.calculaCostoTotal();
            }
        }
        void FillGridRefaccionesFromList()
        {
            /**/
            List<SBO_KF_OITM_INFO> lstResultados = new List<SBO_KF_OITM_INFO>();
            SBO_KF_REFACCION_PD oRefaccionpd = new SBO_KF_REFACCION_PD();
            SBO_KF_OITM_INFO otemp = new SBO_KF_OITM_INFO();

            /**/

            Matrix oRefacciones = (Matrix)this.oForm.Items.Item("gdRFa").Specific;
            Columns oColum = (SAPbouiCOM.Columns)oRefacciones.Columns;
            oRefacciones.Clear();
            if (lstrefacciones.Count > 0)
            {
                int iLine = 1;
                foreach (DetalleRefaccion data in lstrefacciones)
                {
                    /****/
                    //lstResultados = new List<SBO_KF_OITM_INFO>();
                    //oRefaccionpd.listaRefacciones(ref oCompany, ref lstResultados, data.Articulo.ToString(), data.CodigoAlmacen.ToString());
                    
                    //INMSO SOLICITÓ QUE SE PUEDA INGRESAR MANUALMENTE LOS COSTOS.
                    //YA NO SE TRAE DEL PRECIO DEL ALMACEN
                     
                    //data.Costo = 0;
                    //if (lstResultados.Count > 0)
                    //{
                    //    //costorefaccion = lstResultados[0].AvgPrice;
                    //    data.Costo = lstResultados[0].AvgPrice;
                    //}

                    data.Total = data.Cantidad * data.Costo;


                    /***/

                    data.iLineaID = iLine;
                    this.oForm.DataSources.UserDataSources.Item("oUMAT01").Value = iLine.ToString();
                    this.oForm.DataSources.UserDataSources.Item("oUMAT02").Value = data.Almacen;
                    this.oForm.DataSources.UserDataSources.Item("oUMAT03").Value = data.Articulo;
                    this.oForm.DataSources.UserDataSources.Item("oUMAT04").Value = Convert.ToDecimal(data.Costo).ToString("0.000");
                    this.oForm.DataSources.UserDataSources.Item("oUMAT05").Value = data.Cantidad.ToString();
                    this.oForm.DataSources.UserDataSources.Item("oUMAT06").Value = data.Total.ToString();
                    iLine++;
                    oRefacciones.AddRow(1, -1);
                }
                this.calculaCostoTotal();
            }
        }
        void AgregaServiciosToList()
        {
            try
            {
                DetalleOrden oServicio = new DetalleOrden();
                try
                {
                    oCombo = this.oForm.Items.Item("comboVehOS").Specific;
                    oServicio.Vehiculo = new Vehiculo();
                    oServicio.Vehiculo.VehiculoID = Convert.ToInt32(oCombo.Value);
                    oServicio.Vehiculo.Nombre = oCombo.Selected.Description;
                }
                catch { new Exception("Favor de seleccionar el vehiculo"); }

                try
                {
                    oCombo = this.oForm.Items.Item("comoServOS").Specific;
                    oServicio.Servicio = new Servicio();
                    oServicio.Servicio.ServicioID = Convert.ToInt32(oCombo.Value);
                    oServicio.Servicio.Nombre = oCombo.Selected.Description;
                }
                catch { new Exception("Favor de seleccionar el servicio"); }

                oEditText = this.oForm.Items.Item("txtCostoOS").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Replace(",", "").Replace(".00", "")))
                    oServicio.Costo = Convert.ToDecimal(oEditText.String);
                else oServicio.Costo = 0;

                if (oServicio.Servicio.ServicioID > 0)
                {
                    if (oServicio.Vehiculo.VehiculoID > 0)
                    {
                        var frServicio = this.lstservicios.FirstOrDefault(x => x.Servicio.ServicioID == oServicio.Servicio.ServicioID && x.Vehiculo.VehiculoID == oServicio.Vehiculo.VehiculoID);
                        if (frServicio == null)
                            lstservicios.Add(oServicio);
                        else frServicio.Costo += oServicio.Costo;
                        this.bEditaServicio = true;
                        this.FillGridServiciosFromList();

                        oEditText = this.oForm.Items.Item("txtCostoOS").Specific;
                        oEditText.String = "0.00";

                        try
                        {
                            oCombo = this.oForm.Items.Item("comoServOS").Specific;
                            oCombo.Select("0", BoSearchKey.psk_ByValue);
                        }
                        catch { }
                        this.BloqueaComboVehiculo(false);
                    }
                    else SBO_Application.StatusBar.SetText("Favor de proporcionar el vehiculo", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                }
                else SBO_Application.StatusBar.SetText("Favor de proporcionar el servicio", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
            catch (Exception ex)
            {
                SBO_Application.StatusBar.SetText(string.Format("No se ha podido agregar el servicio. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
        }
        void AgregaRefaccionesToList()
        {
            try
            {
                List<string> rcajas  = new List<string> { "txtQntyRf", "txtCostoRf" };
                string rcombos = string.Empty;
                DetalleRefaccion oRefaccion = new DetalleRefaccion();
                oRefaccion.Servicio = new Servicio();

                if (oSetting.cKANAN == 'N')
                {
                    rcajas.Add("txtRefSAP");
                    oEditText = this.oForm.Items.Item("txtRefSAP").Specific;
                    oRefaccion.Articulo = oEditText.String.Trim();

                    rcombos = "cmbAlmacn";
                    oCombo = this.oForm.Items.Item("cmbAlmacn").Specific;
                    oRefaccion.Almacen = oCombo.Selected.Description.Trim();
                    oRefaccion.Almacen = oRefaccion.Almacen.Substring(oRefaccion.Almacen.LastIndexOf('-') + 1);
                    oRefaccion.CodigoAlmacen = oCombo.Value;
                }
                else
                {
                    rcombos = "cmbRefKF";
                    oCombo = this.oForm.Items.Item("cmbRefKF").Specific;
                    oRefaccion.Articulo = Convert.ToString(oCombo.Value);
                    oRefaccion.NombreArticulo = oCombo.Selected.Description;
                }

                oEditText = this.oForm.Items.Item("txtQntyRf").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Replace(",", "").Replace(".00", "")))
                    oRefaccion.Cantidad = Convert.ToDecimal(oEditText.String);

                oEditText = this.oForm.Items.Item("txtCostoRf").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Replace(",", "").Replace(".00", "")))
                    oRefaccion.Costo = Convert.ToDecimal(oEditText.String);
                oRefaccion.Total = Math.Round(Convert.ToDecimal(oRefaccion.Cantidad * oRefaccion.Costo), 4);
                oRefaccion.ArticuloID = articuloID;

                if (!string.IsNullOrEmpty(oRefaccion.Articulo))
                {
                    if (oRefaccion.Cantidad > 0)
                    {
                        //INMSO SOLICITÓ QUE SE PUEDA AGREGAR ARTÍCULOS SIN COSTO UNITARIO,
                        //if (oRefaccion.Costo > 0)
                        //{
                            var frRefaccion = lstrefacciones.FirstOrDefault(x => x.Articulo.Equals(oRefaccion.Articulo) && x.CodigoAlmacen.Equals(oRefaccion.CodigoAlmacen));
                            if (frRefaccion == null)
                                lstrefacciones.Add(oRefaccion);
                            else
                            {
                                
                                frRefaccion.Costo = oRefaccion.Costo;
                                frRefaccion.Cantidad += oRefaccion.Cantidad;

                                frRefaccion.Total = frRefaccion.Costo * frRefaccion.Cantidad;
                                frRefaccion.ArticuloID = articuloID;
                            }
                            this.bEditaRefaccion = true;
                            this.FillGridRefaccionesFromList();

                            foreach (string caja in rcajas)
                            {
                                try
                                {
                                    oEditText = this.oForm.Items.Item(caja).Specific;
                                    oEditText.String = caja.Equals("txtRefSAP", StringComparison.InvariantCultureIgnoreCase) ? "" : "0.00";
                                }
                                catch { continue; }
                            }
                            //SE COMENTA ESTA SECCIÓN DEBIDO A QUE EL USUARIO  YA NO TENDRÁ CONTROL SOBRE EL COMBO DE ALMACEN.
                            //oCombo = this.oForm.Items.Item(rcombos).Specific;
                            //oCombo.Select("0", BoSearchKey.psk_ByValue);
                        //}
                        //else SBO_Application.StatusBar.SetText("Proporciona el costo unitario de la refacción", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);

                    }
                    else SBO_Application.StatusBar.SetText("Proporciona la cantidad refacción", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);

                }
                else SBO_Application.StatusBar.SetText("Proporciona la refacción", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning); 
            }
            catch (Exception ex)
            {
                SBO_Application.StatusBar.SetText(string.Format("No se ha podido agregar la refacción. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
        }
        void AgregaCostoToCostoList()
        {
            string sCadenaAlerta = string.Empty;
            try
            {
                oItem = this.oForm.Items.Item("cmbATipo");
                oCombo = (ComboBox)this.oItem.Specific;
                string sValue = oCombo.Selected.Value;
                SAPbouiCOM.EditText oCajaDesc = null;
                if (!string.IsNullOrEmpty(sValue))
                {
                    switch (sValue)
                    {
                        case "0":
                            oCajaDesc = oForm.Items.Item("txtADesc1").Specific;
                            break;
                        case "1":
                            oCajaDesc = oForm.Items.Item("txtADesc2").Specific;
                            break;
                        case "2":
                            oCajaDesc = oForm.Items.Item("txtADesc3").Specific;
                            break;
                    }

                    SBOKF_CL_CostosAdicional_INFO oTempCosto = new SBOKF_CL_CostosAdicional_INFO();
                    oTempCosto.U_DESCRIPCION = oCajaDesc.Value.Trim();
                    if (!string.IsNullOrEmpty(oTempCosto.U_DESCRIPCION))
                    {
                        oEditText = oForm.Items.Item("txtAcost").Specific;
                        oTempCosto.U_COSTO = Convert.ToDouble(oEditText.Value);
                        if (oTempCosto.U_COSTO > 0)
                        {
                            if (this.lstcostoadicional == null)
                                this.lstcostoadicional = new List<SBOKF_CL_CostosAdicional_INFO>();
                            var oCosto = this.lstcostoadicional.FirstOrDefault(x => x.U_DESCRIPCION.Equals(oTempCosto.U_DESCRIPCION));
                            if (oCosto == null)
                                this.lstcostoadicional.Add(oTempCosto);
                            else this.lstcostoadicional.FirstOrDefault(x => x.U_DESCRIPCION.Equals(oTempCosto.U_DESCRIPCION)).U_COSTO += oTempCosto.U_COSTO;
                            oEditText = oForm.Items.Item("txtAcost").Specific;
                            oEditText.Value = "";
                            oCajaDesc.Value = "";
                            this.bEditaCosto = true;
                            this.FillGridGastosAdicionales();
                        }
                        else sCadenaAlerta = "El importe del costo debe ser mayor a cero";
                    }
                    else sCadenaAlerta = "Proporciona el nombre del costo adicional";
                }
                else sCadenaAlerta = "Favor de determinar el tipo de costo";

                if (!string.IsNullOrEmpty(sCadenaAlerta))
                    SBO_Application.StatusBar.SetText(sCadenaAlerta, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
            catch
            {
                SBO_Application.StatusBar.SetText("Favor de determinar el tipo de costo", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }

        }
        void  AgregaHerramientasToList()
        {
            try
            {
                Kanan.Mantenimiento.BO2.SBO_KF_Herramientas_INFO oTool = new Kanan.Mantenimiento.BO2.SBO_KF_Herramientas_INFO();
                oItem = this.oForm.Items.Item("txttoolid");
                oEditText = (EditText)this.oItem.Specific;
                oTool.ItemCode = oEditText.String;

                oItem = this.oForm.Items.Item("txttool");
                oEditText = (EditText)this.oItem.Specific;
                oTool.ItemName = oEditText.String;

                oItem = this.oForm.Items.Item("txttoolq");
                oEditText = (EditText)this.oItem.Specific;
                oTool.Quantity = Convert.ToDouble(oEditText.String);

                if (!string.IsNullOrEmpty(oTool.ItemCode) && !string.IsNullOrEmpty(oTool.ItemName))
                {
                    if (oTool.Quantity > 0)
                    {

                        var oHerramienta = this.lstherramientas.FirstOrDefault(x => x.ItemName.Equals(oTool.ItemName, StringComparison.InvariantCultureIgnoreCase));
                        if (oHerramienta == null)
                        {

                            oItem = this.oForm.Items.Item("txttoolid");
                            oEditText = (EditText)this.oItem.Specific;
                            oEditText.String = string.Empty;
                            oItem = this.oForm.Items.Item("txttool");
                            oEditText = (EditText)this.oItem.Specific;
                            oEditText.String = string.Empty;
                            oItem = this.oForm.Items.Item("txttoolq");
                            oEditText = (EditText)this.oItem.Specific;
                            oEditText.String = "0";
                            oTool.ArticuloId = articuloID;
                            this.bEditaHerramientas = true;
                            this.lstherramientas.Add(oTool);
                            this.FillGridHerramientas();
                        }
                        else SBO_Application.StatusBar.SetText("La herramienta ya se ha agregado a la lista", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                    }
                    else SBO_Application.StatusBar.SetText("Proporciona la cantidad de herramientas", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                }
                else SBO_Application.StatusBar.SetText("Proporciona la herramienta para agregarla a la lista", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
            catch (Exception ex)
            {
                SBO_Application.StatusBar.SetText(string.Format("No se ha podido agregar la herramienta. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
        }

        void AgregaMecanicosToList()
        {
            try
            {
                //string sMecanicoID = string.Empty, sMecanico = string.Empty;
                Kanan.Mantenimiento.BO2.SBO_KF_OHEM_INFO oEmpleado = new Kanan.Mantenimiento.BO2.SBO_KF_OHEM_INFO();

                oItem = this.oForm.Items.Item("cmbmeca");
                oCombo = (ComboBox)this.oItem.Specific;
                oEditText = oForm.Items.Item("txtSueldoT").Specific;
                int imecanicoid = -1;
                if (!string.IsNullOrEmpty(oCombo.Selected.Value))
                {
                    if (int.TryParse(oCombo.Selected.Value, out imecanicoid))
                    {
                        if (imecanicoid > 0)
                        {
                            SBO_KF_OHEM_PD oMecanicos = new SBO_KF_OHEM_PD();
                            List<SAPinterface.Data.INFO.Tablas.SBO_KF_OHEM_INFO> lstmecanicos = new List<SAPinterface.Data.INFO.Tablas.SBO_KF_OHEM_INFO>();
                            oMecanicos.listaEmpleados(ref oCompany, ref lstmecanicos, Convert.ToInt32(oCombo.Selected.Value));
                            if (lstmecanicos[0].salary > 0)
                            {
                                var oMecanicoList = this.lstmecanicos.FirstOrDefault(x => x.empID == lstmecanicos[0].empID);
                                if (oMecanicoList == null)
                                {
                                    oEmpleado.U_EMPLEADOID = lstmecanicos[0].empID;
                                    oEmpleado.U_SUELDO = lstmecanicos[0].salary;
                                    oEmpleado.salaryUnit = lstmecanicos[0].salaryUnit;
                                    oEmpleado.empID = lstmecanicos[0].EmpleadoKF;
                                    oEmpleado.U_NOMBRE = string.Format("{0} {1} {2}", lstmecanicos[0].firstName, lstmecanicos[0].lastName, lstmecanicos[0].middleName);
                                    this.lstmecanicos.Add(oEmpleado);                                    
                                    SelectComboBoxValue("cmbmeca", "0");
                                    oEditText.String = "00.0";
                                    this.bEditaMecanicos = true;
                                    this.FillGridMecanicos();

                                }
                                else SBO_Application.StatusBar.SetText("El mecanico ya se ha agregado a la lista", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);

                            }
                            else SBO_Application.StatusBar.SetText("El mecanico no cuenta con una configuración de sueldo", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                        }
                        else SBO_Application.StatusBar.SetText("Proporciona el mecanico para agregarla a la lista", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                    }
                    else SBO_Application.StatusBar.SetText("Proporciona el mecanico para agregarla a la lista", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                }
                else SBO_Application.StatusBar.SetText("Proporciona el mecanico para agregarla a la lista", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
            catch (Exception ex)
            {
                SBO_Application.StatusBar.SetText(string.Format("No se ha podido agregar al mecanico. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
        }

        private void calculaCostoTotal()
        {
            Double horas = (Double)this.lstmecanicos.Sum(x => x.U_SUELDO);

            DateTime fentrada = new DateTime();
            DateTime fsalida = new DateTime();

            string sFentrada;
            string sFsalida;



            //FECHA HORA ENTRADA
            
            oEditText = oForm.Items.Item("txtEnReal").Specific;
            sFentrada = oEditText.String;
            oEditText = oForm.Items.Item("txtHReale").Specific;
            sFentrada += " " + oEditText.String;

            //FECHA HORA SALIDA
            oEditText = oForm.Items.Item("txtSalReal").Specific;
            sFsalida = oEditText.String;
            oEditText = oForm.Items.Item("txtHSalr").Specific;
            sFsalida += " " + oEditText.String;

            if (!string.IsNullOrEmpty(sFentrada.Trim()) && !string.IsNullOrEmpty(sFsalida.Trim()))
            {
                fentrada = Convert.ToDateTime(sFentrada);
                fsalida = Convert.ToDateTime(sFsalida);

                horas = (fsalida - fentrada).TotalHours;
            }
            else
            {
                horas = 0;
            }
            



            costoTotal = 0;
            if (this.lstservicios != null)
                costoTotal += (Double)lstservicios.Sum(x => x.Costo);
            if (this.lstcostoadicional != null)
                costoTotal += (Double)this.lstcostoadicional.Sum(x => x.U_COSTO);
            if (this.lstmecanicos != null)
                costoTotal += ((Double)this.lstmecanicos.Sum(x => x.U_SUELDO)) * horas;
            if (this.lstrefacciones != null)
                costoTotal += (Double)this.lstrefacciones.Sum(x => x.Total);
            oLabel = this.oForm.Items.Item("lblCostV").Specific;
            oLabel.Caption = String.Format("$ {0:n}", costoTotal);



            



             // SBO_Application.StatusBar.SetText(string.Format("Tiempo trabajado:  {0}", horas), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);

        }
        #endregion llenado & asignacion de grids


        public String sQueryPlantilla(string sCode = "")
        {
            String strQuery = "";
            if (GestionGlobales.bSqlConnection)
                strQuery = "SELECT * FROM [@VSKF_OSPLANTILLA]";
            else
                strQuery = @"SELECT * FROM ""@VSKF_OSPLANTILLA"" ";

            if (!string.IsNullOrEmpty(sCode))
            {
                strQuery += string.Format(@" WHERE ""Code"" = '{0}' ", sCode);
            }
            return strQuery;
        }

        public String sQueryPlantillaFiltradaTipoVH(string sTipoVHID = "")
        {
            String strQuery = "";
            if (GestionGlobales.bSqlConnection)
                strQuery = "SELECT * FROM [@VSKF_OSPLANTILLA]";
            else
                strQuery = @"SELECT * FROM ""@VSKF_OSPLANTILLA"" ";

            if (!string.IsNullOrEmpty(sTipoVHID))
            {
                strQuery += string.Format(@" WHERE ""U_TipoVehiculoID"" = '{0}' ", sTipoVHID);
            }
            return strQuery;
        }

        public String sQueryPlantillaFiltradaVHDeSuc(string sSucursal = "")
        {
            String strQuery = "";
            if (GestionGlobales.bSqlConnection)
                strQuery = "SELECT * FROM [@VSKF_VEHICULO]";
            else
                strQuery = @"SELECT * FROM ""@VSKF_VEHICULO"" ";

            if (!string.IsNullOrEmpty(sSucursal))
            {
                strQuery += string.Format(@" WHERE ""U_SUCURSALID"" = '{0}' ", sSucursal);
            }
            return strQuery;
        }

        void FormLoadSucursalDeVehiculo()
        {
            //oTools.ColocarMensaje( "Ya pude atrabar el evento" , BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);


            //UBICAMOS EL VEHÍCULO PARA OBTENER SUS CARACTERISTICAS
            oItem = this.oForm.Items.Item("comboVehOS");
            oCombo = (ComboBox)this.oItem.Specific;
            int vehiculoID;
            int SucursalID = 0;
            int TipoVehiculoID = 0;
            string Almacen = "";
            int.TryParse(this.oCombo.Value, out vehiculoID);

            List<SBO_KF_VEHICULOS_INFO> lstResultados = new List<SBO_KF_VEHICULOS_INFO>();
            SBO_KF_VEHICULOS_PD oVehiculopd = new SBO_KF_VEHICULOS_PD();
            SBO_KF_VEHICULOS_INFO otemp = new SBO_KF_VEHICULOS_INFO();

            oVehiculopd.listaVehiculosSucursal(ref oCompany, ref lstResultados, vehiculoID, -1, -1, -1, "", -1);
            if (lstResultados.Count > 0)
            {
                SucursalID = lstResultados[0].U_SUCURSALID;
                Almacen = lstResultados[0].U_ALMACEN;
                TipoVehiculoID = lstResultados[0].U_TIPOVEHICULOID;
            }
            //listaVehiculosSucursal

            //oItem = this.oForm.Items.Item("cmbsuc");
            //oCombo = (ComboBox)this.oItem.Specific;
            //oCombo.Item.Enabled = false;

            oCombo.Select(SucursalID.ToString());
            oCombo.Item.Enabled = false;

            oItem = this.oForm.Items.Item("cmbAlmacn");
            oCombo = (ComboBox)this.oItem.Specific;
            oCombo.Item.Enabled = false;
            if (string.IsNullOrEmpty(Almacen))
                oCombo.Select(0);
            else
                oCombo.Select(Almacen);
            this.FillGridRefaccionesFromListRutina(Almacen);

        }

        void FillGridRefaccionesFromListRutina(string almacen)
        {
            /**/
            List<SBO_KF_OITM_INFO> lstResultados = new List<SBO_KF_OITM_INFO>();
            SBO_KF_REFACCION_PD oRefaccionpd = new SBO_KF_REFACCION_PD();
            SBO_KF_OITM_INFO otemp = new SBO_KF_OITM_INFO();

            /**/

            Matrix oRefacciones = (Matrix)this.oForm.Items.Item("gdRFa").Specific;
            Columns oColum = (SAPbouiCOM.Columns)oRefacciones.Columns;
            oRefacciones.Clear();
            if (lstrefacciones.Count > 0)
            {
                int iLine = 1;
                foreach (DetalleRefaccion data in lstrefacciones)
                {
                    /****/
                    lstResultados = new List<SBO_KF_OITM_INFO>();
                    oRefaccionpd.listaRefacciones(ref oCompany, ref lstResultados, data.Articulo.ToString(), almacen);
                    data.Costo = 0;
                    if (lstResultados.Count > 0)
                    {
                        //costorefaccion = lstResultados[0].AvgPrice;
                        data.Costo = lstResultados[0].AvgPrice;
                    }

                    data.Total = data.Cantidad * data.Costo;


                    /***/

                    data.iLineaID = iLine;
                    this.oForm.DataSources.UserDataSources.Item("oUMAT01").Value = iLine.ToString();
                    this.oForm.DataSources.UserDataSources.Item("oUMAT02").Value = almacen;
                    this.oForm.DataSources.UserDataSources.Item("oUMAT03").Value = data.Articulo;
                    this.oForm.DataSources.UserDataSources.Item("oUMAT04").Value = Convert.ToDecimal(data.Costo).ToString("0.000");
                    this.oForm.DataSources.UserDataSources.Item("oUMAT05").Value = data.Cantidad.ToString();
                    this.oForm.DataSources.UserDataSources.Item("oUMAT06").Value = data.Total.ToString();
                    iLine++;
                    oRefacciones.AddRow(1, -1);
                }
                this.calculaCostoTotal();
            }
        }
        #endregion Metodos

    }
}
