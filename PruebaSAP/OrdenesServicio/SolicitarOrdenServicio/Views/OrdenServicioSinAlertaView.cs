﻿
using Kanan.Comun.BO2;
using Kanan.Costos.BO2;
using Kanan.Mantenimiento.BO2;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using KananSAP.Configuracion.Configuraciones.Objects;
using KananSAP.GestionFacturaClientes.Objects;
using KananSAP.Helper;
using KananSAP.Mantenimiento.Data;
using KananWS.Interface;
using SAPbobsCOM;
using SAPbouiCOM;
using SAPinterface.Data.AD;
using SAPinterface.Data.INFO.Objetos;
using SAPinterface.Data.INFO.Tablas;
using SAPinterface.Data.PD;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KananSAP.SolicitarOrdenServicio.Presenter
{
    public class OrdenServicioSinAlertaView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.Button oButton;
        private SAPbouiCOM.StaticText oStaticText;
        private SAPbouiCOM.ComboBox oCombo;
        private SAPbouiCOM.CheckBox oCheckBoxTallerInterno;
        private XMLPerformance XmlApplication;
        public Kanan.Mantenimiento.BO2.AlertaMantenimiento AlertaMantenimiento { get; set; }
        //public OrdenServicio OrdenServicio { get; set; }
        public OrdenServicioSAP Entity { get; set; }
        public DetalleRefaccionesSAP EntityRefaccion { get; set; }
        public RefaccionesWS ReffWS = null;
        public DetalleOrdenSAP EntityDetalle { get; set; }
        public List<DetalleRefaccion> Refacciones { get; set; }
        public List<DetalleRefaccion> RefaccionesKF { get; set; }
        public RefaccionesView RefaccionVista { get; set; }
        public int iLineaRefaccion { get; set; }
        public int? ServicioID { get; set; }
        private SAPbobsCOM.Company oCompany;
        private const string INDEFINIDO = "No definido";
        public bool Exist { get; set; }
        private Configurations Config;
        private NodaTimeHelper.Services.DateTimeHelper dateHelper;
        public List<DetalleOrden> ListaServiciosAgregados { get; set; }
        public List<SBOKF_CL_CostosAdicional_INFO> ListaCostosAdicionales { get; set; }
        public List<Kanan.Mantenimiento.BO2.SBO_KF_Herramientas_INFO> ListaHerramientas { get; set; }
        public List<Kanan.Mantenimiento.BO2.SBO_KF_OHEM_INFO> ListaMecanicos { get; set; }
        public List<DetalleOrden> ListaServiciosEliminados { get; set; }
        public List<DetalleOrden> ListaServiciosEditados { get; set; }

        private SAPbouiCOM.Grid oGrid;
        public int? RowIndex { get; set; }
        private SAPbouiCOM.Folder oFolder;
        public int? estatus;
        public int? statusInicial { get; set; }
        public string FolioOrdenSercio { get; set; }
        private bool EsSQL;

        public FolioOrdenServicioSAP EFolio { get; set; }

        public decimal? TotalServicios;
        public decimal? TotalRefacciones;
        public decimal? TotalOrdenServicio;

        #endregion

        #region Constructor
        public OrdenServicioSinAlertaView(SAPbouiCOM.Application Application, SAPbobsCOM.Company company, Configurations c)
        {
            try
            {
                this.SBO_Application = Application;
                this.oCompany = company;
                this.oItem = null;
                oCombo = null;
                oStaticText = null;
                oEditText = null;
                this.Config = c;
                this.XmlApplication = new XMLPerformance(this.SBO_Application);
                this.PrepareObjects();
                this.oGrid = null;
                this.GetUserConfigs();
                this.ListaServiciosAgregados = new List<DetalleOrden>();
                this.EFolio = new FolioOrdenServicioSAP(this.oCompany);
                this.Entity = new OrdenServicioSAP(ref this.oCompany);
                this.EntityDetalle = new DetalleOrdenSAP(ref this.oCompany);
                this.EsSQL = oCompany.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.oCheckBoxTallerInterno = null;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region FORM

        public void LoadForm(int? step, bool exist)
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("frmAddOS");
                this.SetBoundsToControl();
                this.FillCombos();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Invalid Form"))
                {
                    string sPath = System.Windows.Forms.Application.StartupPath;
                    this.XmlApplication.LoadFromXML(sPath + "\\XML",  "OrdenServicioSinAlerta.xml");
                    
                    this.oForm = SBO_Application.Forms.Item("frmAddOS");
                    this.SetBoundsToControl();
                    this.FillCombos();
                    //this.SetCurrentFolioOrden();
                    this.ListaServiciosAgregados = new List<DetalleOrden>();
                    this.Refacciones = new List<DetalleRefaccion>();
                    this.RefaccionVista = new RefaccionesView();
                    this.ListaCostosAdicionales = new List<SBOKF_CL_CostosAdicional_INFO>();
                    this.ListaHerramientas = new List<Kanan.Mantenimiento.BO2.SBO_KF_Herramientas_INFO>();
                    this.ListaMecanicos = new List<Kanan.Mantenimiento.BO2.SBO_KF_OHEM_INFO>();
                    this.statusInicial = step;
                    this.estatus = step;
                    this.ChooseArticulo();
                    this.ChooseRecurso();
                    this.ChooseSocioNegocio();
                    this.GestionInventario();
                    oFolder = this.oForm.Items.Item("tabDataSer").Specific;
                    oFolder.Select();

                    //
                    switch (step)
                    {
                        case 1:
                            PrepareOrdenSolicitada();
                            HideCosto();
                            if (exist)
                            {
                                //ShowCosto();
                                this.oForm.Title = "Actualizar Orden de Servicio";
                                oItem = this.oForm.Items.Item("cmbestatus");
                                oItem.Enabled = true;

                                //Hablitando bot�n notificaci�n
                               // oItem = this.oForm.Items.Item("btnNotif");
                               // oItem.Enabled = true;
                            }
                            else { 
                             //mostrar check para guardar cargar plantilla
                               /* oItem = this.oForm.Items.Item("chkSPla");
                                oItem.Enabled = true;
                                oItem = this.oForm.Items.Item("chkLpla");
                                oItem.Enabled = true;*/
                                oItem = this.oForm.Items.Item("tabDataRef");
                                oItem.Visible = true;
                                this.ConsumeSiguienteFolioWS();
                            }
                            break;
                        case 2:
                            PrepareOrdenAprobada();
                            if (exist)
                            {
                                this.oForm.Title = "Actualizar Orden de Servicio";
                            }
                            break;
                        case 5:
                            PrepareOrdenAtendida();
                            if (exist)
                            {
                                this.oForm.Title = "Detalle de Orden de Servicio";
                            }
                            break;
                        case 6:
                            PrepareOrdenRealizada();
                            if (exist)
                            {
                                this.oForm.Title = "Detalle de Orden de Servicio";
                            }
                            break;
                        case 4:
                            if (!exist)
                            {
                                PrepareOrdenEliminada();
                                SBO_Application.SetStatusBarMessage("Este registro ha sido eliminado, sólo es posible consultar. ", BoMessageTime.bmt_Short, false);
                                return;
                            }
                            break;
                        case 3:
                            PrepareOrdenRechazada();
                            if (exist)
                            {
                                this.oForm.Title = "Detalle de Orden de Servicio";
                            }
                            break;
                    }
                    //this.EnableEstatus(false);
                    //Establece la ventana centrada
                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                }
                else
                {
                    SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                }
            }
        }

        public void GestionInventario()
        {
            try
            {

                string sresponse = string.Empty, sconsultaSql = string.Empty;
                SBO_KF_Configuracion_INFO oSetting = new SBO_KF_Configuracion_INFO();                
                SBO_KF_Configuracion_AD oConfiguracion = new SBO_KF_Configuracion_AD();
                sresponse = oConfiguracion.recuperaConfiguracion(ref oCompany, out sconsultaSql, ref oSetting);
                if (sresponse == "OK")
                {
                    GestionGlobales oGlobales = new GestionGlobales();
                    oGlobales.FillCombo("Ninguno", "WhsCode", "WhsName", "cmbAlmacn", GestionGlobales.AlmacenRecarga(), this.oForm, this.oCompany);
                    this.AddChooseHerramientas();
                    //this.AddChooseMecanicos();
                    if (oSetting.cKANAN == 'N')
                    {
                        oItem = this.oForm.Items.Item("txtRefSAP");
                        oItem.Visible = true;
                        oItem = this.oForm.Items.Item("cmbRefKF");
                        oItem.Visible = false;
                        this.AddItemsCFL();
                    }
                    else
                    {
                        oItem = this.oForm.Items.Item("cmbAlmacn");
                        oItem.Visible = false;

                        oItem = this.oForm.Items.Item("txtRefSAP");
                        oItem.Visible = false;
                        oItem = this.oForm.Items.Item("cmbRefKF");
                        oItem.Visible = true;
                        ReffWS = new RefaccionesWS();
                        List<DetalleRefaccion> dataRef = ReffWS.SolicitarRefacciones(this.Config.UserFull.Dependencia.EmpresaID);
                        this.RefaccionesKF = dataRef;
                        ComboBox oCombo = oItem.Specific;
                        KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);
                        oCombo.ValidValues.Add(" ", "Selecciona la refacción");
                        foreach (DetalleRefaccion rfaccion in dataRef)
                        {
                            string value = rfaccion.ArticuloID.ToString();
                            string description = string.Format("{0} - {1}", value, rfaccion.NombreArticulo.ToString().Trim());
                            oCombo.ValidValues.Add(rfaccion.NombreArticulo.ToString().Trim(), description.Trim());
                        }
                        oCombo.Item.DisplayDesc = true;
                    }
                }
            }
            catch { }
        }
        void  ChooseRecurso()
        {
            try
            {
                oForm.DataSources.UserDataSources.Add("EdtCstoAr", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);
                SAPbouiCOM.ChooseFromListCollection oCFLs = null;
                oCFLs = oForm.ChooseFromLists;
                SAPbouiCOM.ChooseFromList oCFL = null;
                SAPbouiCOM.ChooseFromListCreationParams oCFLCreationParams = null;
                oCFLCreationParams = ((SAPbouiCOM.ChooseFromListCreationParams)(SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)));
                oCFLCreationParams.MultiSelection = false;
                oCFLCreationParams.ObjectType = "290";
                oCFLCreationParams.UniqueID = "CFSrc";
                oCFL = oCFLs.Add(oCFLCreationParams);
                oItem = oForm.Items.Item("txtADesc2");
                EditText oEditText = (EditText)(oItem.Specific);
                oEditText.DataBind.SetBound(true, "", "EdtCstoAr");
                oEditText.ChooseFromListUID = "CFSrc";
                oEditText.ChooseFromListAlias = "ResName";
            }
            catch { }
        }

        void ChooseSocioNegocio()
        {
            try
            {
                oForm.DataSources.UserDataSources.Add("EdtSoci", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);
                SAPbouiCOM.ChooseFromListCollection oCFLs = null;
                oCFLs = oForm.ChooseFromLists;
                SAPbouiCOM.ChooseFromList oCFL = null;
                SAPbouiCOM.ChooseFromListCreationParams oCFLCreationParams = null;
                oCFLCreationParams = ((SAPbouiCOM.ChooseFromListCreationParams)(SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)));
                oCFLCreationParams.MultiSelection = false;
                oCFLCreationParams.ObjectType = "2";
                oCFLCreationParams.UniqueID = "CFSoc";
                oCFL = oCFLs.Add(oCFLCreationParams);
                SAPbouiCOM.Conditions oConditions = this.SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_Conditions);
                oConditions = oCFL.GetConditions();
                SAPbouiCOM.Condition oCondition = oConditions.Add();
                oCondition.Alias = "CardType";
                oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
                oCondition.CondVal = "C";
                oCFL.SetConditions(oConditions);
                oItem = oForm.Items.Item("txtClisap");
                EditText oEditText = (EditText)(oItem.Specific);
                oEditText.DataBind.SetBound(true, "", "EdtSoci");
                oEditText.ChooseFromListUID = "CFSoc";
                oEditText.ChooseFromListAlias = "CardCode";
            }
            catch { }
        }

        void ChooseArticulo()
        {
            try
            {
                oForm.DataSources.UserDataSources.Add("EdtRSCto", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);
                SAPbouiCOM.ChooseFromListCollection oCFLs = null;
                oCFLs = oForm.ChooseFromLists;
                SAPbouiCOM.ChooseFromList oCFL = null;
                SAPbouiCOM.ChooseFromListCreationParams oCFLCreationParams = null;
                oCFLCreationParams = ((SAPbouiCOM.ChooseFromListCreationParams)(SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)));
                oCFLCreationParams.MultiSelection = false;
                oCFLCreationParams.ObjectType = "4";
                oCFLCreationParams.UniqueID = "CFArt";
                oCFL = oCFLs.Add(oCFLCreationParams);
                oItem = oForm.Items.Item("txtADesc3");
                EditText oEditText = (EditText)(oItem.Specific);
                oEditText.DataBind.SetBound(true, "", "EdtRSCto");
                oEditText.ChooseFromListUID = "CFArt";
                oEditText.ChooseFromListAlias = "ItemName";
                //oItem = this.oForm.Items.Item("txtADesc3");
                //oItem.Visible = false;
            }
            catch { }
        }
        public void AddItemsCFL()
        {
            try
            {


                Item oItem = null;
                oForm.DataSources.UserDataSources.Add("EditDS", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);
                SAPbouiCOM.ChooseFromListCollection oCFLs = null;
                oCFLs = oForm.ChooseFromLists;
                SAPbouiCOM.ChooseFromList oCFL = null;
                SAPbouiCOM.ChooseFromListCreationParams oCFLCreationParams = null;
                oCFLCreationParams = ((SAPbouiCOM.ChooseFromListCreationParams)(SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)));
                oCFLCreationParams.MultiSelection = false;
                oCFLCreationParams.ObjectType = "4";
                oCFLCreationParams.UniqueID = "CFL10";

                oCFL = oCFLs.Add(oCFLCreationParams);
                SAPbouiCOM.Conditions oConditions = this.SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_Conditions);
                oConditions = oCFL.GetConditions();
                SAPbouiCOM.Condition oCondition = oConditions.Add();
                oCondition.Alias = "ItmsGrpCod";
                oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
                oCondition.CondVal = new GestionGlobales().iFiltroGrupo(this.oCompany, "REFACCIONES KF");
                oCFL.SetConditions(oConditions);
                oItem = oForm.Items.Item("txtRefSAP");
                EditText oEditText = (EditText)(oItem.Specific);
                oEditText.DataBind.SetBound(true, "", "EditDS");
                oEditText.ChooseFromListUID = "CFL10";
                oEditText.ChooseFromListAlias = "ItemCode";

                 

            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError("OrdenServicioSinAlertaView.cs->AddItemsCFL()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public void AddChooseHerramientas()
        {
            try
            {


                Item oItem = null;
                oForm.DataSources.UserDataSources.Add("EdtODS", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);
                SAPbouiCOM.ChooseFromListCollection oCFLs = null;
                oCFLs = oForm.ChooseFromLists;
                SAPbouiCOM.ChooseFromList oCFL = null;
                SAPbouiCOM.ChooseFromListCreationParams oCFLCreationParams = null;
                oCFLCreationParams = ((SAPbouiCOM.ChooseFromListCreationParams)(SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)));
                oCFLCreationParams.MultiSelection = false;
                oCFLCreationParams.ObjectType = "4";
                oCFLCreationParams.UniqueID = "CFLTOOL";

                oCFL = oCFLs.Add(oCFLCreationParams);
                SAPbouiCOM.Conditions oConditions = this.SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_Conditions);
                oConditions = oCFL.GetConditions();
                SAPbouiCOM.Condition oCondition = oConditions.Add();
                oCondition.Alias = "ItmsGrpCod";
                oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
                oCondition.CondVal = new GestionGlobales().iFiltroGrupo(this.oCompany, "HERRAMIENTAS KF");
                oCFL.SetConditions(oConditions);
                oItem = oForm.Items.Item("txttoolid");
                EditText oEditText = (EditText)(oItem.Specific);
                oEditText.DataBind.SetBound(true, "", "EdtODS");
                oEditText.ChooseFromListUID = "CFLTOOL";
                oEditText.ChooseFromListAlias = "ItemCode";
            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError("OrdenServicioSinAlertaView.cs->AddChooseHerramientas()", ex.Message);
                throw new Exception(ex.Message);
            }
        }
      

        public void ShowForm(int? step, bool exist)
        {
            try
            {
                //Se quit� el if para poder visualizar una orden eliminada pero no podermodificarla.
                //if (step == 4)
                    //return;
                this.oForm.Visible = true;
                this.oForm.Select();
            }
            catch (Exception ex)
            {
                //Se quit� el if para poder visualizar una orden eliminada pero no podermodificarla.
                //if (step == 4)
                    //return;
                this.LoadForm(step, exist);
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                this.oForm.Select();

            }
        }

        public void CloseForm()
        {
            if (this.oForm.Visible & this.oForm.Selected)
                this.oForm.Close();
        }

        public void ClicComboProeedor()
        {
            try
            {
                //oItem = oForm.Items.Item("comboProOS");
                //oItem.Click();

                //oItem = oForm.Items.Item("comboProOS");
                //oStaticText = (SAPbouiCOM.StaticText) (oItem.Specific);
                //oStaticText.Item.Click();
            }
            catch(Exception ex)
            {
                throw new Exception("Clic proveedor. " + ex.Message);
            }
        }

        public void ClicButtonSaveOS()
        {
            try
            {
                oItem = oForm.Items.Item("btnSaveOS");
                oButton = (SAPbouiCOM.Button)(oItem.Specific);
                oButton.Item.Click();
            }
            catch (Exception ex)
            {
                throw new Exception("Clic proveedor. " + ex.Message);
            }
        }

        public void SetCostoRefaccionKF()
        {
            oItem = this.oForm.Items.Item("cmbRefKF");
            ComboBox oCombo = oItem.Specific;
            if (oCombo != null)
            {
                for (int i = 0; i < this.RefaccionesKF.Count; i++)
                {
                    if (oCombo.Selected.Value == RefaccionesKF[i].ArticuloID.ToString())
                    {
                        oItem = this.oForm.Items.Item("txtCostoRf");
                        EditText oEditText = oItem.Specific;
                        oEditText.Value = RefaccionesKF[i].Costo.ToString();
                    }
                }
            }
        }

        public String SetCostoRefaccion()
        {
            String message = String.Empty;
            double? Cantidad = 0;
            String ItemCode = String.Empty;
            String WhsCode = String.Empty;

            oItem = this.oForm.Items.Item("txtRefSAP");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            ItemCode = Convert.ToString(oEditText.String);

            oItem = this.oForm.Items.Item("txtQntyRf");
            oEditText = (SAPbouiCOM.EditText) (oItem.Specific);
            if (!String.IsNullOrEmpty(oEditText.String))
            {
                //Formatenado cadena.
                String temp = oEditText.String;
                temp = temp.Replace(".00", "");
                Cantidad = Convert.ToDouble(temp);

                //Validando que la cantidad sea mayor a 0.
                if (Cantidad <= 0)
                    message += "La cantidad debe ser un entero mayor a 0. ";
            }
            else
            {
                message += "Indique la cantidad. ";
            }

            oItem = this.oForm.Items.Item("cmbAlmacn");
            oCombo = (SAPbouiCOM.ComboBox)(oItem.Specific);
            if (oCombo.Selected != null)
                WhsCode = Convert.ToString(oCombo.Selected.Value);

            if (String.IsNullOrEmpty(ItemCode))
                message += "Seleccione un artículo. ";
            if (String.IsNullOrEmpty(WhsCode))
                message += "Seleccione un almacén. ";

            if (String.IsNullOrEmpty(message))
            {
                oItem = this.oForm.Items.Item("txtCostoRf");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                double costo = Convert.ToDouble(this.Entity.GetCostoMercancia(ItemCode, WhsCode));
                String final = Convert.ToString(costo * Cantidad);
                final = final.Replace(".00","");
                oEditText.String = final;
                return String.Empty;
            }
            else
            {
                return message;
            }
        }

        public void SetIVA(double? iva)
        {
            oItem = this.oForm.Items.Item("lblImpOSv");
            oStaticText = (SAPbouiCOM.StaticText) (oItem.Specific);
            oStaticText.Caption = Convert.ToString(iva);
        }

        /// <summary>
        /// Establece los UserDataSource para los controles que lo requieran
        /// </summary>
        private void SetBoundsToControl()
        {
            try
            {
                oEditText = this.oForm.Items.Item("txtFechaOS").Specific as EditText;
                oEditText.DataBind.SetBound(true, "", "UD_DateOS");
                oEditText = this.oForm.Items.Item("txtCostoOS").Specific as EditText;
                oEditText.DataBind.SetBound(true, "", "UD_Costo");
                /* No se requiere dado que no es un input, es un item que �nicamente
                 * muestra informaci�n. Por tal motivo es comentada. */
                //oStaticText = this.oForm.Items.Item("lblImpOSv").Specific as StaticText;
                //oStaticText.DataBind.SetBound(true, "", "UD_IVA");
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        /// <summary>
        /// Establece los campos de la ventana para solicitud de orden de servicio o orden solicitada
        /// </summary>
        
        /// <summary>
        /// Oculta los campos de ingreso de costos.
        /// </summary>
        public void HideCosto()
        {
            try
            {
                oItem = this.oForm.Items.Item("lblCostOS");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                //oItem.Visible = false;
                oItem.Enabled = false;

                oItem = this.oForm.Items.Item("txtCostoOS");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                //oItem.Visible = false;
                oItem.Enabled = false;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al ocultar campos de costo. Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Muestra los campos de ingreso de costos.
        /// </summary>
        public void ShowCosto()
        {
            try
            {
                oItem = this.oForm.Items.Item("lblCostOS");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                //oItem.Visible = false;
                oItem.Enabled = true;

                oItem = this.oForm.Items.Item("txtCostoOS");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                //oItem.Visible = false;
                oItem.Enabled = true;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al mostrar campos de costo. Error: " + ex.Message);
            }
        }

        private void PrepareOrdenSolicitada()
        {
            try
            {
                oItem = this.oForm.Items.Item("cmbestatus");
                oItem.Visible = true;
                oItem.Enabled = false;
                SelectComboBoxValue("cmbestatus", "1");

                oItem = this.oForm.Items.Item("lblProvOS");
                oItem.Visible = false;
                oItem = this.oForm.Items.Item("comboProOS");
                oItem.Visible = false;

                oItem = this.oForm.Items.Item("lblFoliOS");
                oItem.Visible = false;
                oItem = this.oForm.Items.Item("txtFolioOS");
                oItem.Visible = false;

                oItem = this.oForm.Items.Item("lblMPOS");
                oItem.Visible = false;
                oItem = this.oForm.Items.Item("comboMPOSv");
                oItem.Visible = false;

                oItem = this.oForm.Items.Item("lblDetOS");
                oItem.Visible = false;
                oItem = this.oForm.Items.Item("txtDetOSv");
                oItem.Visible = false;

                oItem = this.oForm.Items.Item("lblCost");
                oItem.Visible = false;
                oItem = this.oForm.Items.Item("Item_1");
                oItem.Visible = false;
                oItem = this.oForm.Items.Item("lblCostV");
                oItem.Visible = false;
                oItem = this.oForm.Items.Item("Item_22");
                oItem.Visible = false;

                oItem = this.oForm.Items.Item("lblImpOS");
                oItem.Visible = false;
                oItem = this.oForm.Items.Item("lblImpOSv");
                oItem.Visible = false;

                oItem = this.oForm.Items.Item("lblCostOS");
                oItem.Visible = false;
                //oItem.Enabled = false;
                oItem = this.oForm.Items.Item("txtCostoOS");
                oItem.Visible = false;
                //oItem.Enabled = false;
                //oFolder = this.oForm.Items.Item("tabDataRef").Specific;
                //oFolder.Item.Visible = false;

                oItem = oForm.Items.Item("txtEnPro");
                oItem.Enabled = true;
                oItem = oForm.Items.Item("txtSalPro");
                oItem.Enabled = true;

                try
                {
                    oItem = oForm.Items.Item("txthEntr");
                    oEditText = (EditText)this.oItem.Specific;
                    oEditText.String = "00:00";

                    oItem = oForm.Items.Item("txtHsal");
                    oEditText = (EditText)this.oItem.Specific;
                    oEditText.String = "00:00";
                }
                catch { }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }


        /// <summary>
        /// Establece los campos de la ventana para orden de servicio aprobada
        /// </summary>
        private void PrepareOrdenAprobada()
        {
            try
            {
                oItem = this.oForm.Items.Item("Item_3");
                oItem.Enabled = false;
                oItem = this.oForm.Items.Item("cmbsuc");
                oItem.Enabled = false;

                oItem = this.oForm.Items.Item("lblProvOS");
                oItem.Enabled = false;
                oItem = this.oForm.Items.Item("comboProOS");
                oItem.Enabled = false;

                oItem = this.oForm.Items.Item("lblFchOS");
                oItem.Enabled = false;
                oItem = this.oForm.Items.Item("txtFechaOS");
                oItem.Enabled = false;
                
                oItem = this.oForm.Items.Item("txtHoraOS");
                oItem.Enabled = false;

                oItem = this.oForm.Items.Item("lblDesOS");
                oItem.Enabled = false;
                oItem = this.oForm.Items.Item("txtDescOs");
                oItem.Enabled = false;

                oItem = this.oForm.Items.Item("lblRsp");
                oItem.Enabled = false;
                oItem = this.oForm.Items.Item("cmbResp");
                oItem.Enabled = false;

                oItem = this.oForm.Items.Item("lblAprob");
                oItem.Enabled = false;
                oItem = this.oForm.Items.Item("cmbAprob");
                oItem.Enabled = false;

                oFolder = this.oForm.Items.Item("tabDataRef").Specific;
                oFolder.Item.Visible = true;
                //oFolder.Item.Enabled = false;

                SetEstatusAprobada();
                SelectComboBoxValue("cmbestatus", "2");
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        /// <summary>
        /// Establece los campos de la ventana para orden de servicio atendida
        /// </summary>
        private void PrepareOrdenAtendida()
        {
            try
            {
                oEditText = this.oForm.Items.Item("txtFechaOS").Specific;//Fecha
                oEditText.Item.Enabled = false;
                oEditText = this.oForm.Items.Item("txtHoraOS").Specific;//Fecha
                oEditText.Item.Enabled = false;
                

                oEditText = this.oForm.Items.Item("txtFolioOS").Specific;//Folio
                oEditText.Item.Enabled = false;
                oEditText = this.oForm.Items.Item("txtDescOs").Specific;//Descripcion
                oEditText.Item.Enabled = false;
                oEditText = this.oForm.Items.Item("txtCostoOS").Specific;//Costo
                oEditText.Item.Enabled = false;

                oItem = this.oForm.Items.Item("txtRefSAP");
                oItem.Enabled = false;
                oItem = this.oForm.Items.Item("txtQntyRf");
                oItem.Enabled = false;
                oItem = this.oForm.Items.Item("cmbAlmacn");
                oItem.Enabled = false;
                //oItem = this.oForm.Items.Item("gdOS");
                //oItem.Enabled = false;

                oItem = this.oForm.Items.Item("txtDetOSv");//DetallePago
                oItem.Enabled = false;
                oStaticText = this.oForm.Items.Item("lblImpOSv").Specific;//Impuesto
                oEditText.Item.Enabled = false;

                oStaticText = this.oForm.Items.Item("lblCostV").Specific;
                oStaticText.Item.Enabled = false;

                SelectComboBoxValue("cmbestatus", "5");
                oCombo = this.oForm.Items.Item("cmbResp").Specific;//Combo Responsable
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("cmbsuc").Specific;//Combo Sucursal
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("cmbAprob").Specific;//Combo Aprobador
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("comboProOS").Specific;//Combo Proveedor
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("comboMPOSv").Specific;//Combo Metodo de pago
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("comboVehOS").Specific;//Combo Vehiculo
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("comoServOS").Specific;//Combo Servicio
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("cmbestatus").Specific;//Combo Estatus
                oCombo.Item.Enabled = true;

                //oFolder = this.oForm.Items.Item("tabDataOrd").Specific;
                //oFolder.Select();

                oButton = this.oForm.Items.Item("btnOSvDone").Specific;
                oButton.Item.Enabled = false;
                oButton = this.oForm.Items.Item("btnDelOSv").Specific;
                oButton.Item.Enabled = false;
                oButton = this.oForm.Items.Item("btnEdtOSv").Specific;
                oButton.Item.Enabled = false;
                oButton = this.oForm.Items.Item("btnSaveOS").Specific;
                oButton.Item.Enabled = true;
                oButton = this.oForm.Items.Item("btnDelRf").Specific;
                oButton.Item.Enabled = false;
                oButton = this.oForm.Items.Item("btnRefn").Specific;
                oButton.Item.Enabled = false;

                oFolder = this.oForm.Items.Item("tabDataRef").Specific;
                oFolder.Item.Visible = true;
                oFolder.Item.Enabled = true;

                oItem = oForm.Items.Item("CBTI");
                oCheckBoxTallerInterno = (CheckBox)(oItem.Specific);
                oCheckBoxTallerInterno.Item.Enabled = false;

                //FECHA ENTRADA REAL
                oEditText = this.oForm.Items.Item("txtEnReal").Specific;
                oEditText.String = DateTime.Now.ToShortDateString();
                oEditText.Active = false;

                oEditText = this.oForm.Items.Item("txtHReale").Specific;
                oEditText.String = DateTime.Now.ToString("HH:mm");
                oEditText.Active = false;

                //FECHA SALIDA REAL
                oEditText = this.oForm.Items.Item("txtSalReal").Specific;
                oEditText.String = DateTime.Now.ToShortDateString();
                oEditText.Active = false;

                oEditText = this.oForm.Items.Item("txtHSalr").Specific;
                oEditText.String = DateTime.Now.ToString("HH:mm");
                oEditText.Active = false;

            }
            catch (Exception ex) {  }
        }

        /// <summary>
        /// Establece los campos de la ventana para orden de servicio realizada
        /// </summary>
        private void PrepareOrdenRealizada()
        {
            try
            {
                oEditText = this.oForm.Items.Item("txtRefSAP").Specific;//Refacci�n
                oEditText.Item.Enabled = false;
                oEditText = this.oForm.Items.Item("txtFechaOS").Specific;//Fecha
                oEditText.Item.Enabled = false;
                oEditText = this.oForm.Items.Item("txtHoraOS").Specific;//Fecha
                oEditText.Item.Enabled = false;
                
                oEditText = this.oForm.Items.Item("txtFolioOS").Specific;//Folio
                oEditText.Item.Enabled = false;
                oEditText = this.oForm.Items.Item("txtDescOs").Specific;//Descripcion
                oEditText.Item.Enabled = false;
                oEditText = this.oForm.Items.Item("txtQntyRf").Specific;//Catidad refacciones
                oEditText.Item.Enabled = false;
                oEditText = this.oForm.Items.Item("txtCostoRf").Specific;//Costo refacciones
                oEditText.Item.Enabled = false;
                oEditText = this.oForm.Items.Item("txtCostoOS").Specific;//Costo
                oEditText.Item.Enabled = false;
                oEditText = this.oForm.Items.Item("lblImpOSv").Specific;//Impuesto
                oEditText.Item.Enabled = false;

                //oFolder = this.oForm.Items.Item("tabDataOrd").Specific;
                //oFolder.Select();
                oFolder.Item.Enabled = true;
                oFolder = this.oForm.Items.Item("tabDataSer").Specific;
                oFolder.Item.Enabled = true;
                oFolder = this.oForm.Items.Item("tabDataRef").Specific;
                oFolder.Item.Enabled = true;

                oItem = this.oForm.Items.Item("txtDetOSv");//DetallePago
                oItem.Enabled = false;

                oStaticText = this.oForm.Items.Item("lblCostV").Specific;
                oStaticText.Item.Enabled = false;

                oCombo = this.oForm.Items.Item("cmbResp").Specific;//Combo Responsable
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("cmbsuc").Specific;//Combo Sucursal
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("cmbAprob").Specific;//Combo Aprobador
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("comboProOS").Specific;//Combo Proveedor
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("comboMPOSv").Specific;//Combo Metodo de pago
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("comboVehOS").Specific;//Combo Vehiculo
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("comoServOS").Specific;//Combo Servicio
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("cmbestatus").Specific;//Combo Estatus
                oCombo.Item.Enabled = false;
                SelectComboBoxValue("cmbestatus", "6");
                oCombo = this.oForm.Items.Item("cmbAlmacn").Specific;//Combo Almacen refacciones
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("cmbRefKF").Specific;//Combo refacci�nes
                oCombo.Item.Enabled = false;

                oButton = this.oForm.Items.Item("btnOSvDone").Specific;
                oButton.Item.Enabled = false;
                oButton = this.oForm.Items.Item("btnDelOSv").Specific;
                oButton.Item.Enabled = false;
                oButton = this.oForm.Items.Item("btnEdtOSv").Specific;
                oButton.Item.Enabled = false;
                oButton = this.oForm.Items.Item("btnSaveOS").Specific;
                oButton.Item.Enabled = false;
                oButton = this.oForm.Items.Item("btnRefn").Specific;/*Bot�n agregar refacci�n*/
                oButton.Item.Enabled = false;
                oButton = this.oForm.Items.Item("btnDelRf").Specific;/*Bot�n borrar refacci�n*/
                oButton.Item.Enabled = false;

                oItem = oForm.Items.Item("CBTI");
                oCheckBoxTallerInterno = (CheckBox)(oItem.Specific);
                oCheckBoxTallerInterno.Item.Enabled = false;

                //FECHA SALIDA REAL
                oEditText = this.oForm.Items.Item("txtSalReal").Specific;
                oEditText.String = DateTime.Now.ToShortDateString();
                oEditText.Active = false;

                oEditText = this.oForm.Items.Item("txtHSalr").Specific;
                oEditText.String = DateTime.Now.ToString("HH:mm");
                oEditText.Active = false;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        /// <summary>
        /// Establece los campos de la ventana de una Orden de Servicio rechazada
        /// </summary>
        private void PrepareOrdenRechazada()
        {
            try
            {
                oEditText = this.oForm.Items.Item("txtFechaOS").Specific;//Fecha
                oEditText.Item.Enabled = false;
                oEditText = this.oForm.Items.Item("txtHoraOS").Specific;//Fecha
                oEditText.Item.Enabled = false;
                
                oEditText = this.oForm.Items.Item("txtFolioOS").Specific;//Folio
                oEditText.Item.Enabled = false;
                oEditText = this.oForm.Items.Item("txtDescOs").Specific;//Descripcion
                oEditText.Item.Enabled = false;

                oCombo = this.oForm.Items.Item("comboMPOSv").Specific;//Combo Metodo de pago
                oCombo.Item.Enabled = false;
                oItem = this.oForm.Items.Item("txtDetOSv");//DetallePago
                oItem.Enabled = false;
                oEditText = this.oForm.Items.Item("lblImpOSv").Specific;//Impuesto
                oEditText.Item.Enabled = false;

                oStaticText = this.oForm.Items.Item("lblCostV").Specific;
                oStaticText.Item.Enabled = false;

                oCombo = this.oForm.Items.Item("cmbResp").Specific;//Combo Responsable
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("cmbsuc").Specific;//Combo Sucursal
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("cmbAprob").Specific;//Combo Aprobador
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("comboProOS").Specific;//Combo Proveedor
                oCombo.Item.Enabled = false;

                oEditText = this.oForm.Items.Item("txtCostoOS").Specific;//Costo
                oEditText.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("comboVehOS").Specific;//Combo Vehiculo
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("comoServOS").Specific;//Combo Servicio
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("cmbestatus").Specific;//Combo Estatus
                oCombo.Item.Enabled = false;

                //oFolder = this.oForm.Items.Item("tabDataOrd").Specific;
                //oFolder.Select();
                SelectComboBoxValue("cmbestatus", "3");

                oButton = this.oForm.Items.Item("btnOSvDone").Specific;
                oButton.Item.Enabled = false;

                oButton = this.oForm.Items.Item("btnDelOSv").Specific;
                oButton.Item.Enabled = false;

                oButton = this.oForm.Items.Item("btnEdtOSv").Specific;
                oButton.Item.Enabled = false;
                oButton = this.oForm.Items.Item("btnSaveOS").Specific;
                oButton.Item.Enabled = false;

                oButton = this.oForm.Items.Item("btnDelRf").Specific;
                oButton.Item.Enabled = false;

                oButton = this.oForm.Items.Item("btnRefn").Specific;
                oButton.Item.Enabled = false;

                //oFolder = this.oForm.Items.Item("tabDataRef").Specific;
                //oFolder.Item.Visible = false;

                oItem = oForm.Items.Item("CBTI");
                oCheckBoxTallerInterno = (CheckBox)(oItem.Specific);
                oCheckBoxTallerInterno.Item.Enabled = false;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        /// <summary>
        /// Establece los campos de la ventana de ua Orden de Servicio Eliminada
        /// </summary>
        private void PrepareOrdenEliminada()
        {
            try
            {
                oEditText = this.oForm.Items.Item("txtFechaOS").Specific;//Fecha
                oEditText.Item.Enabled = false;
                oEditText = this.oForm.Items.Item("txtHoraOS").Specific;//Fecha
                oEditText.Item.Enabled = false;
                
                oEditText = this.oForm.Items.Item("txtFolioOS").Specific;//Folio
                oEditText.Item.Enabled = false;
                oEditText = this.oForm.Items.Item("txtDescOs").Specific;//Descripcion
                oEditText.Item.Enabled = false;

                oCombo = this.oForm.Items.Item("comboMPOSv").Specific;//Combo Metodo de pago
                oCombo.Item.Enabled = false;
                oItem = this.oForm.Items.Item("txtDetOSv");//DetallePago
                oItem.Enabled = false;
                oEditText = this.oForm.Items.Item("lblImpOSv").Specific;//Impuesto
                oEditText.Item.Enabled = false;

                oStaticText = this.oForm.Items.Item("lblCostV").Specific;
                oStaticText.Item.Enabled = false;

                oCombo = this.oForm.Items.Item("cmbResp").Specific;//Combo Responsable
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("cmbsuc").Specific;//Combo Sucursal
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("cmbAprob").Specific;//Combo Aprobador
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("comboProOS").Specific;//Combo Proveedor
                oCombo.Item.Enabled = false;

                oEditText = this.oForm.Items.Item("txtCostoOS").Specific;//Costo
                oEditText.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("comboVehOS").Specific;//Combo Vehiculo
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("comoServOS").Specific;//Combo Servicio
                oCombo.Item.Enabled = false;
                oCombo = this.oForm.Items.Item("cmbestatus").Specific;//Combo Estatus
                oCombo.Item.Enabled = false;

                //oFolder = this.oForm.Items.Item("tabDataOrd").Specific;
                //oFolder.Select();
                SelectComboBoxValue("cmbestatus", "4");

                oButton = this.oForm.Items.Item("btnOSvDone").Specific;
                oButton.Item.Enabled = false;

                oButton = this.oForm.Items.Item("btnDelOSv").Specific;
                oButton.Item.Enabled = false;

                oButton = this.oForm.Items.Item("btnEdtOSv").Specific;
                oButton.Item.Enabled = false;
                oButton = this.oForm.Items.Item("btnSaveOS").Specific;
                oButton.Item.Enabled = false;

                oButton = this.oForm.Items.Item("btnDelRf").Specific;
                oButton.Item.Enabled = false;

                oButton = this.oForm.Items.Item("btnRefn").Specific;
                oButton.Item.Enabled = false;

                oFolder = this.oForm.Items.Item("tabDataRef").Specific;
                oFolder.Item.Visible = false;

                oEditText = this.oForm.Items.Item("lblImpOSv").Specific;
                oEditText.Item.Enabled = true;

                oItem = oForm.Items.Item("CBTI");
                oCheckBoxTallerInterno = (CheckBox)(oItem.Specific);
                oCheckBoxTallerInterno.Item.Enabled = false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region FILL COMBO
        private void FillComboTipoPago()
        {
            try
            {
                string query = @"select * from ""@VSKF_TIPODEPAGO"" where U_Estatus = 1";
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    oItem = this.oForm.Items.Item("comboMPOSv");
                    oCombo = oItem.Specific;
                    Helper.KananSAPHelpers.CleanComboBox(oCombo);//this.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Ninguno");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("Code").Value);
                        string description = rs.Fields.Item("Name").Value;
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }
                    oCombo.Item.DisplayDesc = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void FillComboSucursales()
        {
            try
            {
                string query = @"select * from ""@VSKF_SUCURSAL""";
                //query += string.Format(" where U_Propietario = {0} ", this.Config.UserFull.Dependencia.EmpresaID);
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    oItem = this.oForm.Items.Item("cmbsuc");
                    oCombo = oItem.Specific;
                    Helper.KananSAPHelpers.CleanComboBox(oCombo);//this.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Ninguno");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("U_SucursalID").Value);
                        //string description = rs.Fields.Item("Name").Value;
                        string description = rs.Fields.Item("Direccion").Value;
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }
                    oCombo.Item.DisplayDesc = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void FillComboProveedores()
        {
            try
            {
                string query = @"select * from ""@VSKF_PROVEEDOR""";
                //query += string.Format(" where U_Propietario = {0} ", this.Config.UserFull.Dependencia.EmpresaID);
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    oItem = this.oForm.Items.Item("comboProOS");
                    oCombo = oItem.Specific;
                    Helper.KananSAPHelpers.CleanComboBox(oCombo);//this.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Ninguno");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("U_ProveedorID").Value);
                        string description = rs.Fields.Item("U_Nombre").Value;
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }
                    oCombo.Item.DisplayDesc = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void FillComboServicios()
        {
            try
            {
                ServicioSAP svData = new ServicioSAP(this.oCompany);
                Servicio sv = new Servicio();
                sv.Propietario = new Kanan.Operaciones.BO2.Empresa();
                sv.Propietario.EmpresaID = this.Config.UserFull.Dependencia.EmpresaID;
                SAPbobsCOM.Recordset rs = svData.Consultar(sv);
                if (svData.HashServicios(rs))
                {
                    oItem = this.oForm.Items.Item("comoServOS");
                    oCombo = oItem.Specific;
                    Helper.KananSAPHelpers.CleanComboBox(oCombo);//this.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Ninguno");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("U_ServicioID").Value);
                        string description = rs.Fields.Item("Servicio").Value;
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }
                    oCombo.Item.DisplayDesc = true;
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        private void FillComboVehiculos()
        {
            try
            {
                string query = string.Empty;
                query = @" select vh.U_VehiculoID, vh.U_Nombre from ""@VSKF_VEHICULO"" vh ";
                if (this.Config.UserFull.Dependencia.EmpresaID.HasValue)
                    query += string.Format(" where vh.U_EmpresaID = {0} ", this.Config.UserFull.Dependencia.EmpresaID);
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    oItem = this.oForm.Items.Item("comboVehOS");
                    oCombo = oItem.Specific;
                    Helper.KananSAPHelpers.CleanComboBox(oCombo);//this.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Ninguno");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("U_VehiculoID").Value);
                        string description = rs.Fields.Item("U_Nombre").Value;
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }
                    oCombo.Item.DisplayDesc = true;
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        private void FillComboEmpleados()
        {
            try
            {
                string query = string.Empty;
                query = @" select e.""Name"", e.U_KFEmpleadoID from ""@VSKF_RELEMPLEADO"" e ";
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    oItem = this.oForm.Items.Item("cmbResp");
                    oCombo = oItem.Specific;
                    ComboBox cmb = this.oForm.Items.Item("cmbAprob").Specific;
                    Helper.KananSAPHelpers.CleanComboBox(oCombo);
                    Helper.KananSAPHelpers.CleanComboBox(cmb);
                    oCombo.ValidValues.Add("0", "Ninguno");
                    cmb.ValidValues.Add("0", "Ninguno");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("U_KFEmpleadoID").Value);
                        string description = rs.Fields.Item("Name").Value;
                        oCombo.ValidValues.Add(value, description);
                        cmb.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }
                    oCombo.Item.DisplayDesc = true;
                    cmb.Item.DisplayDesc = true;
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        private void FillComboEstatus()
        {
            try
            {
                string query = @"select * from ""@VSKF_ESTATUSORDEN"" where U_Activo = 1";
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    oItem = this.oForm.Items.Item("cmbestatus");
                    oCombo = oItem.Specific;
                    Helper.KananSAPHelpers.CleanComboBox(oCombo); //this.CleanComboBox(oCombo);
                    //oCombo.ValidValues.Add("0", "Ninguno");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("Code").Value);
                        string description = rs.Fields.Item("Name").Value;
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }
                    
                }
                oCombo.Item.DisplayDesc = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void FillComboComponenteSistema()
        {
            try
            {
                List<SBO_KF_CAUSADETALLE_INFO> lstResultados = new List<SBO_KF_CAUSADETALLE_INFO>();
                SBO_KF_CausaRaiz_PD oCausapd = new SBO_KF_CausaRaiz_PD();
                SBO_KF_CRAIZ_INFO otemp = new SBO_KF_CRAIZ_INFO();
                oItem = this.oForm.Items.Item("cmbSyscr");
                oCombo = (ComboBox)this.oItem.Specific;
                if (!string.IsNullOrEmpty(oCombo.Selected.Value))
                {
                    if (oCombo.Selected.Value != "0")
                    {
                        otemp.Name = oCombo.Selected.Value;
                        oCausapd.listaCausaDetalle(ref oCompany, ref lstResultados, otemp);
                        if (lstResultados.Count > 0)
                        {
                            oItem = this.oForm.Items.Item("cmbCompo");
                            oCombo = (ComboBox)this.oItem.Specific;
                            Helper.KananSAPHelpers.CleanComboBox(oCombo);
                            oCombo.ValidValues.Add(" ", "Selecciona un componente");
                            foreach (SBO_KF_CAUSADETALLE_INFO oSistema in lstResultados)
                                oCombo.ValidValues.Add(oSistema.Name, oSistema.Componente);
                            oCombo.Item.DisplayDesc = true;
                        }
                    }
                    else Helper.KananSAPHelpers.CleanComboBox(oCombo);
                }
                else Helper.KananSAPHelpers.CleanComboBox(oCombo); 
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void FillComboMecanicos()
        {
            try
            {
                SBO_KF_OHEM_PD oMecanicos = new SBO_KF_OHEM_PD();
                List<SAPinterface.Data.INFO.Tablas.SBO_KF_OHEM_INFO> lstmecanicos = new List<SAPinterface.Data.INFO.Tablas.SBO_KF_OHEM_INFO>();
                oMecanicos.listaEmpleados(ref oCompany, ref lstmecanicos);
                if (lstmecanicos.Count > 0)
                {
                    oItem = this.oForm.Items.Item("cmbmeca");
                    oCombo = oItem.Specific;
                    Helper.KananSAPHelpers.CleanComboBox(oCombo); 
                    oCombo.ValidValues.Add("0", "Selecciona un mecánico");

                    foreach (SAPinterface.Data.INFO.Tablas.SBO_KF_OHEM_INFO oHEM in lstmecanicos)
                        oCombo.ValidValues.Add(oHEM.empID.ToString(), string.Format("{0} {1} {2}", oHEM.firstName, oHEM.middleName, oHEM.lastName));

                    oCombo.Item.DisplayDesc = true;
                    SelectComboBoxValue("cmbmeca", "0");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void FillComboSistemas()
        {
            try
            {
                List<SBO_KF_CRAIZ_INFO> lstResultados = new List<SBO_KF_CRAIZ_INFO>();
                SBO_KF_CausaRaiz_PD oCausapd= new SBO_KF_CausaRaiz_PD();
                SBO_KF_CRAIZ_INFO otemp = new SBO_KF_CRAIZ_INFO();
                otemp.Activo = true;
                oCausapd.listaCausaraiz(ref oCompany, ref lstResultados, otemp, false);
                if (lstResultados.Count > 0)
                {
                    oItem = this.oForm.Items.Item("cmbSyscr");
                    oCombo = oItem.Specific;
                    Helper.KananSAPHelpers.CleanComboBox(oCombo); 
                    oCombo.ValidValues.Add(" ", "Selecciona un sistema");
                    
                    foreach (SBO_KF_CRAIZ_INFO oSistema in lstResultados)                    
                        oCombo.ValidValues.Add(oSistema.Name, oSistema.NombreSistema);

                    oCombo.Item.DisplayDesc = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Establece los valores de los combos
        /// </summary>
        private void FillCombos()
        {
            FillComboTipoPago();
            FillComboServicios();
            FillComboVehiculos();
            FillComboSucursales();
            FillComboProveedores();
            FillComboEmpleados();
            FillComboEstatus();
            FillComboSistemas();
            FillComboMecanicos();
            oItem = this.oForm.Items.Item("cmblevel");
            oCombo = oItem.Specific;
            oCombo.Item.DisplayDesc = true;

            oItem = this.oForm.Items.Item("cmbrutina");
            oCombo = oItem.Specific;
            oCombo.Item.DisplayDesc = true;

            SelectComboBoxValue("cmbrutina", "0");
            SelectComboBoxValue("cmblevel", "0");
            SelectComboBoxValue("cmbsuc", "0");
            SelectComboBoxValue("cmbResp", "0");
            SelectComboBoxValue("cmbAprob", "0");
            //Folder(Tabs)
            /*oFolder = oForm.Items.Item("tabDataOrd").Specific;
            oFolder.DataBind.SetBound(true, "", "tabOrd");
            oFolder.Item.Width = 200;
            oFolder.Select();
            oFolder = oForm.Items.Item("tabDataSer").Specific;
            oFolder.DataBind.SetBound(true, "", "tabOrd");
            oFolder.Item.Width = 200;
            oFolder.GroupWith("tabDataOrd");*/
        }

        #endregion

        #region Complemento Form

        /// <summary>
        /// Obtiene el costo mostrado en el formulario de la Orden de servicio.
        /// </summary>
        /// <returns></returns>
        public Decimal GetCostoForm()
        {
            try
            {
                Decimal Costo;

                //Se obtiene el costo del formulario
                oStaticText = this.oForm.Items.Item("lblCostV").Specific;
                Costo = String.IsNullOrEmpty(oStaticText.Caption) ? Costo = Convert.ToDecimal(oStaticText.Caption) : Costo = 0;

                return Costo;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar obtener el costo actual en el formulario. " + ex.Message);
            }
        }

        /// <summary>
        /// Verifica si el tipo de pago seleccionado por el usuario es efectivo
        /// </summary>
        /// <returns></returns>
        public bool isEfectivo()
        {
            try
            {
                oItem = null;
                oItem = oForm.Items.Item("comboMPOSv");
                ComboBox cbx = oItem.Specific;
                string val = cbx.Value;
                if (!string.IsNullOrEmpty(val))
                {
                    if (val.Equals("4"))
                        return true;
                    else
                        return false;
                }
                return false;
            }
            catch (Exception ex) { return false; }
        }
        /// <summary>
        /// Muestra control detalle de pago para capturar en caso de ser un tipo de pago diferente de efectivo
        /// </summary>
        public void ShowDetallePago()
        {
            try
            {
                oItem = oForm.Items.Item("lblDetOS");
                oItem.Visible = true;
                oItem = oForm.Items.Item("txtDetOSv");
                oEditText = oItem.Specific;
                oEditText.String = string.Empty;
                oItem.Visible = true;
                oItem = oForm.Items.Item("txtDetOSv");
                oItem.Click(BoCellClickType.ct_Regular);
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        /// <summary>
        /// Oculta el control detalle pago en caso de ser un tipo de pago en efectivo
        /// </summary>
        public void HiddeDetallePago()
        {
            try
            {
                oItem = oForm.Items.Item("lblDetOS");
                oItem.Visible = false;
                oItem = oForm.Items.Item("txtDetOSv");
                oItem.Visible = false;
                oItem = oForm.Items.Item("lblImpOSv");
                oItem.Click(BoCellClickType.ct_Regular);
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        /// <summary>
        /// Recupera el folio de sistema correspondiente a la siguiente orden
        /// </summary>
        private void SetCurrentFolioOrden()
        {
            try
            {
                String folio = String.Empty;
                String anio = String.Empty;
                /*Comentado para evitar no contar insercion por insercion erronea.*/
                Recordset rs = this.EFolio.ConsultarTodas();
                //Recordset CountFolio = this.Entity.ConsultarTodos();

                oStaticText = oForm.Items.Item("txtfolio").Specific;

                //(EFolio.HashFolioOrden(rs))
                //(this.Entity.HashOrdenServicio(CountFolio))
                if (EFolio.HashFolioOrden(rs))
                {
                    #region Antiguo
                    //Forma antigua
                    rs.MoveFirst();
                    EFolio.oUDT.GetByKey(rs.Fields.Item("Code").Value);
                    EFolio.UDTToFolio();
                    EFolio.folio.Folio = ((EFolio.folio.Folio) + 1);
                    folio = EFolio.folio.Folio.ToString();
                    anio = EFolio.folio.AnioFolio.ToString();
                    #endregion Antiguo

                    #region Nuevo
                    //Forma nueva
                    /*CountFolio.MoveFirst();
                    EFolio.oUDT.GetByKey(CountFolio.Fields.Item("Code").Value);
                    this.EFolio.folio = new FolioOrden { Propietario = new Empresa { EmpresaID = this.Config.UserFull.Dependencia.EmpresaID } };
                    folio = (CountFolio.RecordCount + 1).ToString();
                    anio = DateTime.Today.Year.ToString();*/
                    #endregion Nuevo

                    this.EFolio.folio.Folio = Convert.ToInt32(folio);
                    this.EFolio.folio.AnioFolio = Convert.ToInt32(anio);
                    oStaticText.Caption = string.Format("{0}/{1}", folio, anio);
                }
                else
                {
                    oStaticText.Caption = "1/" + DateTime.UtcNow.Year;
                    int Anio = Convert.ToInt32(oStaticText.Caption.Split('/')[1]);
                    int Folio = Convert.ToInt32(oStaticText.Caption.Split('/')[0]);
                    this.EFolio.folio = new FolioOrden { AnioFolio = Anio, Folio = Folio, Propietario = new Empresa { EmpresaID = this.Config.UserFull.Dependencia.EmpresaID } };
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// habilita o desabilita el combo de estatus
        /// </summary>
        public void SetEstatusSolicitada()
        {
            oCombo = this.oForm.Items.Item("cmbestatus").Specific as ComboBox;
            //oCombo.ValidValues.Remove("1");
            oCombo.ValidValues.Remove("4");
            oCombo.ValidValues.Remove("5");
        }
        /// <summary>
        /// Prepara el combo estatus para atender o eliminar orden
        /// </summary>
        public void SetEstatusAprobada()
        {
            oCombo = this.oForm.Items.Item("cmbestatus").Specific as ComboBox;
            oCombo.ValidValues.Remove("1");
            oCombo.ValidValues.Remove("3");
            oCombo.ValidValues.Remove("4");
            //oCombo.ValidValues.Remove("6");
        }
        /// <summary>
        /// Prepara el combo estatus para atender o eliminar orden
        /// </summary>
        public void SetEstatusAtendida()
        {
            oCombo = this.oForm.Items.Item("cmbestatus").Specific as ComboBox;
            oCombo.ValidValues.Remove("1");
            //oCombo.ValidValues.Remove("2");
            oCombo.ValidValues.Remove("4");
        }
        /// <summary>
        /// Prepara el combo estatus para atender o eliminar orden
        /// </summary>
        public void ComboEstatusRealizada()
        {
            oCombo = this.oForm.Items.Item("cmbestatus").Specific as ComboBox;
            oCombo.Select("6", BoSearchKey.psk_ByDescription);
        }
        /// <summary>
        /// RSelecciona el value especificado en un combobox
        /// </summary>
        public void SelectComboBoxValue(string comboname, string value)
        {
            oCombo = oForm.Items.Item(comboname).Specific;
            oCombo.Select(value, BoSearchKey.psk_ByValue);
        }
        #endregion

        #region Servicios
        /// <summary>
        /// Pasa los datos de la interface al objeto DetalleOrden
        /// </summary>
        public void FormToDetalleOrden()
        {
            this.EntityDetalle.oDetalleOrden = new DetalleOrden();

            oStaticText = this.oForm.Items.Item("lblsvcid").Specific;
            EntityDetalle.oDetalleOrden.DetalleOrdenID = !string.IsNullOrEmpty(oStaticText.Caption) ? Convert.ToInt32(oStaticText.Caption.Trim()) : (int?)null;

            EntityDetalle.oDetalleOrden.TipoMantenibleID = 1;
            EntityDetalle.oDetalleOrden.Mantenible = new Vehiculo();
            //Vehiculo
            oCombo = this.oForm.Items.Item("comboVehOS").Specific;
            (EntityDetalle.oDetalleOrden.Mantenible as Vehiculo).VehiculoID = Convert.ToInt32(oCombo.Value);
            (EntityDetalle.oDetalleOrden.Mantenible as Vehiculo).Nombre = oCombo.Selected.Description;

            //Servicio
            oCombo = this.oForm.Items.Item("comoServOS").Specific;
            EntityDetalle.oDetalleOrden.Servicio.ServicioID = Convert.ToInt32(oCombo.Value);
            EntityDetalle.oDetalleOrden.Servicio.Nombre = oCombo.Selected.Description;
            //Costo
            oEditText = this.oForm.Items.Item("txtCostoOS").Specific;
            //Se agreg� el Replace para evitar un error en la validaci�n por las comas.
            oEditText.String = oEditText.String.Replace(",", "").Replace(".00", "");
            EntityDetalle.oDetalleOrden.Costo = !string.IsNullOrEmpty(oEditText.String) ? Convert.ToDecimal(oEditText.String.Trim()) : 0;
            //OrdenServicio
            oStaticText = this.oForm.Items.Item("lblid").Specific;
            EntityDetalle.oDetalleOrden.OrdenServicio.OrdenServicioID = !string.IsNullOrEmpty(oStaticText.Caption) ? Convert.ToInt32(oStaticText.Caption.Trim()) : (int?)null;

        }
        public Boolean FormToListRefaccion()
        {
            Boolean bContinuar = false;
            string sresponse = string.Empty, sconsultaSql = string.Empty;
            DetalleRefaccion oTemp = new DetalleRefaccion();
            if (Refacciones == null)
                Refacciones = new List<DetalleRefaccion>();
            Decimal Quantity = 0;
            SBO_KF_Configuracion_INFO oSetting = new SBO_KF_Configuracion_INFO();
            SBO_KF_Configuracion_AD oConfiguracion = new SBO_KF_Configuracion_AD();
            sresponse = oConfiguracion.recuperaConfiguracion(ref oCompany, out sconsultaSql, ref oSetting);
            if (sresponse == "OK")
            {

                if (oSetting.cKANAN == 'N')
                {
                    oTemp.Articulo = RefaccionVista.txtRefSAP.String.Trim();

                    oTemp.Almacen = RefaccionVista.cmbAlmacn.Selected.Description.Trim();
                    oTemp.Almacen = oTemp.Almacen.Substring(oTemp.Almacen.LastIndexOf('-') + 1);
                    oTemp.CodigoAlmacen = RefaccionVista.cmbAlmacn.Value;
                    oTemp.Total = Math.Round(Convert.ToDecimal(oTemp.Cantidad * oTemp.Costo), 4);
                }
                else
                {
                    oTemp.Articulo = RefaccionVista.cmbRefaccion.Value;
                }

                RefaccionVista.txtQntyRf.String = RefaccionVista.txtQntyRf.String.Replace(",", "").Replace(".00", "");
                Quantity = Convert.ToDecimal(RefaccionVista.txtQntyRf.String.Trim());
                oTemp.Cantidad = Quantity;

                String temp = RefaccionVista.txtCostoRf.String;
                temp = temp.Replace(",", "").Replace(".00", "");
                Quantity = Convert.ToDecimal(temp.Trim());
                oTemp.Costo = Quantity;
                oTemp.Total = Math.Round(Convert.ToDecimal(oTemp.Cantidad * oTemp.Costo), 4);
                Refacciones.Add(oTemp);

                //Comentado debido a que una variable est�tica controla el total de las refacciones.
                /*Decimal CostoActual = Convert.ToDecimal(Entity.OrdenServicio.Costo);
                Decimal NuevoCosto = Convert.ToDecimal(CostoActual + oTemp.Total);
                Entity.OrdenServicio.Costo = NuevoCosto;*/

                /*Se modific�, ahora una variable est�tica controla el costo total
                 debido a que se sobre escrib�an valores.*/
                //Costototal
                /*oStaticText = this.oForm.Items.Item("lblCostV").Specific;
                oStaticText.Caption = Entity.OrdenServicio.Costo.ToString();*/
            }
            return bContinuar;
        }
        public bool EliminarLineaRefaccion()
        {
            try
            {
                var Refaccion = Refacciones.FirstOrDefault(x => x.iLineaID == iLineaRefaccion);
                if (Refaccion != null)
                {
                    Decimal CostoActual = Convert.ToDecimal(Entity.OrdenServicio.Costo);
                    Decimal NuevoCosto = Convert.ToDecimal( CostoActual - Refaccion.Total);
                    Entity.OrdenServicio.Costo = NuevoCosto;
                    oStaticText =  this.oForm.Items.Item("lblCostV").Specific;
                    oStaticText.Caption = Entity.OrdenServicio.Costo.ToString();
                    
                    iLineaRefaccion = -1;
                    Refacciones.Remove(Refaccion);
                    this.ListToMatrix();
                    return true;
                }
            }
            catch { }

            return false;
        }
        public void LimpiarObjctRefacc()
        {
            RefaccionVista.txtCostoRf.String = "0.00";
            RefaccionVista.txtQntyRf.String = "0.00";
            string sresponse = string.Empty, sconsultaSql = string.Empty;
            SBO_KF_Configuracion_INFO oSetting = new SBO_KF_Configuracion_INFO();
            SBO_KF_Configuracion_AD oConfiguracion = new SBO_KF_Configuracion_AD();
            sresponse = oConfiguracion.recuperaConfiguracion(ref oCompany, out sconsultaSql, ref oSetting);
            if (sresponse == "OK")
            {

                try
                {
                    if (oSetting.cKANAN == 'N')
                    {
                        RefaccionVista.txtRefSAP.String = "";

                        RefaccionVista.ocmbAlmacen.Enabled = false;
                        RefaccionVista.cmbAlmacn.Select(" ", BoSearchKey.psk_ByValue);
                        RefaccionVista.ocmbAlmacen.Enabled = true;
                    }

                    if (oSetting.cKANAN == 'Y')
                    {
                        RefaccionVista.ocmbReffcion.Enabled = false;
                        RefaccionVista.cmbRefaccion.Select(" ", BoSearchKey.psk_ByValue);
                        RefaccionVista.ocmbReffcion.Enabled = false;
                        RefaccionVista.ocmbAlmacen.Visible = false;
                    }
                }
                catch { }
            }
        }

        public Boolean ListToMatrix()
        {
            bool bContinuar = false;
            try
            {
                if (Refacciones != null)
                {
                    Matrix oRefacciones = (Matrix)this.oForm.Items.Item("gdRFa").Specific;
                    Columns oColum = (SAPbouiCOM.Columns)oRefacciones.Columns;
                    oRefacciones.Clear();
                    int iLine = 1;
                    foreach (DetalleRefaccion data in Refacciones)
                    {
                        data.iLineaID = iLine;
                        this.oForm.DataSources.UserDataSources.Item("oUMAT01").Value = iLine.ToString();
                        this.oForm.DataSources.UserDataSources.Item("oUMAT02").Value = data.Almacen;
                        this.oForm.DataSources.UserDataSources.Item("oUMAT03").Value = data.Articulo;
                        this.oForm.DataSources.UserDataSources.Item("oUMAT04").Value = Convert.ToDecimal(data.Costo).ToString();//string.Format("$ {0}", data.Costo);
                        this.oForm.DataSources.UserDataSources.Item("oUMAT05").Value = data.Cantidad.ToString();
                        this.oForm.DataSources.UserDataSources.Item("oUMAT06").Value = data.Total.ToString();
                        iLine++;
                        oRefacciones.AddRow(1, -1);
                    }
                }
                bContinuar = true;
                this.SetCostoRefacciones();
            }
            catch
            {

            }
            return bContinuar;
        }

        public Boolean FormValidaRefaccion(out String Cadena)
        {
            Cadena = string.Empty;
            Decimal Quantity = 0;
            string sresponse = string.Empty, sconsultaSql = string.Empty;
            SBO_KF_Configuracion_INFO oSetting = new SBO_KF_Configuracion_INFO();
            SBO_KF_Configuracion_AD oConfiguracion = new SBO_KF_Configuracion_AD();
            sresponse = oConfiguracion.recuperaConfiguracion(ref oCompany, out sconsultaSql, ref oSetting);
            if (sresponse == "OK")
            {

                if (RefaccionVista == null)
                    RefaccionVista = new RefaccionesView();

                if (oSetting.cKANAN == 'Y')
                {
                    RefaccionVista.ocmbReffcion = this.oForm.Items.Item("cmbRefKF");
                    RefaccionVista.cmbRefaccion = RefaccionVista.ocmbReffcion.Specific;
                    if (string.IsNullOrEmpty(RefaccionVista.cmbRefaccion.Value.Trim()))
                        Cadena = "Selecciona la refacción gestionada por Kanan Fleet";
                }
                else
                {
                    RefaccionVista.txtRefSAP = this.oForm.Items.Item("txtRefSAP").Specific;
                    if (string.IsNullOrEmpty(RefaccionVista.txtRefSAP.String.Trim()))
                        Cadena = "Proporciona la refacción gestionada por SAP";

                    RefaccionVista.ocmbAlmacen = this.oForm.Items.Item("cmbAlmacn");
                    RefaccionVista.cmbAlmacn = RefaccionVista.ocmbAlmacen.Specific;
                    if (string.IsNullOrEmpty(RefaccionVista.cmbAlmacn.Value.Trim()))
                        Cadena = "Selecciona el almacén de inventario";
                }

                RefaccionVista.txtQntyRf = this.oForm.Items.Item("txtQntyRf").Specific;
                Quantity = Convert.ToDecimal(RefaccionVista.txtQntyRf.String.Trim());
                if (Quantity <= 0)
                    Cadena = "Proporciona la cantidad de refacciones";
                RefaccionVista.txtCostoRf = this.oForm.Items.Item("txtCostoRf").Specific;
                String temp = RefaccionVista.txtCostoRf.String.Replace(".00", "");
                Quantity = Convert.ToDecimal(temp);

                //if (Quantity <= 0)
                //    Cadena = "Proporciona el costo por unidad";
            }
            return string.IsNullOrEmpty(Cadena) ? true : false;
        }

        /// <summary>
        /// Pasa los datos de la interface al objeto DetalleOrden
        /// </summary>
        public void DetalleOrdenToForm()
        {
            oStaticText = this.oForm.Items.Item("lblsvcid").Specific;
            oStaticText.Caption = EntityDetalle.oDetalleOrden.DetalleOrdenID != null ? EntityDetalle.oDetalleOrden.DetalleOrdenID.ToString() : string.Empty;

            //Vehiculo
            oCombo = this.oForm.Items.Item("comboVehOS").Specific;
            oCombo.Select((EntityDetalle.oDetalleOrden.Mantenible as Vehiculo).VehiculoID.ToString());

            //Servicio
            oCombo = this.oForm.Items.Item("comoServOS").Specific;
            oCombo.Select(EntityDetalle.oDetalleOrden.Servicio.ServicioID.ToString());
            //Costo
            oEditText = this.oForm.Items.Item("txtCostoOS").Specific;
            oEditText.String = EntityDetalle.oDetalleOrden.Costo.ToString();

        }
        /// <summary>
        /// Vacia los campos de los servicios para agregar uno nuevo
        /// </summary>
        public void EmptyServicioToForm()
        {
            #region Datos de los servicios Agregados
            oStaticText = this.oForm.Items.Item("lblsvcid").Specific;//Costo
            oStaticText.Caption = "";
            oEditText = this.oForm.Items.Item("txtCostoOS").Specific;//Costo
            oEditText.String = "";
            oCombo = this.oForm.Items.Item("comboVehOS").Specific;//Combo Vehiculo
            oCombo.Select("0", BoSearchKey.psk_ByValue);
            oCombo = this.oForm.Items.Item("comoServOS").Specific;//Combo Servicio
            oCombo.Select("0", BoSearchKey.psk_ByValue);

            //oCombo = this.oForm.Items.Item("comboMPOSv").Specific;//Combo Metodo de pago
            //oCombo.Select("psk_ByValue", BoSearchKey.psk_ByValue);
            //oItem = this.oForm.Items.Item("txtDetOSv");//DetallePago
            //oItem.Visible = true;
            //oEditText = oItem.Specific;
            //oEditText.String = "";
            //oEditText = this.oForm.Items.Item("lblImpOSv").Specific;//Impuesto
            //oEditText.String = "";
            #endregion
        }
        /// <summary>
        /// Valida si el servicio que se va agregar ya existe
        /// </summary>
        /// <param name="servicioID"></param>
        /// <returns></returns>
        private bool isServicioAgregado(int? servicioID, int? vehiculoID)
        {
            try
            {
                return this.ListaServiciosAgregados.Exists(s => s.Servicio.ServicioID == servicioID && s.Mantenible.MantenibleID == vehiculoID);
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        /// <summary>
        /// Agrega un servicio a la orden de servicio
        /// </summary>
        public void NewDetalleOrdenToGrid()
        {
            try
            {
                string err = this.ValidarServicio();
                if (!string.IsNullOrEmpty(err))
                    this.SBO_Application.MessageBox(err, 1, "Ok", "", "");
                else
                {
                    this.FormToDetalleOrden();
                    if (this.ListaServiciosAgregados == null) ListaServiciosAgregados = new List<DetalleOrden>();
                    if (!this.isServicioAgregado(EntityDetalle.oDetalleOrden.Servicio.ServicioID, EntityDetalle.oDetalleOrden.Mantenible.MantenibleID))
                    {
                        //if ()
                        this.ListaServiciosAgregados.Add((DetalleOrden)EntityDetalle.oDetalleOrden.Clone());
                        this.FillGridServiciosFromList();
                        this.SetCosto();
                        this.EmptyServicioToForm();
                    }
                    else
                        this.SBO_Application.MessageBox("Ya se ha agregado este servicio", 1, "Ok");
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void GetChooseList(string smood, string sValue)
        {
            List<SBO_KF_OITM_INFO> lstArt = new List<SBO_KF_OITM_INFO>();
            SBO_KF_OITM_AD oItems = new SBO_KF_OITM_AD();
            SBO_KF_OHEM_PD oEmpleado = new SBO_KF_OHEM_PD();
            if (smood == "CFLTOOL" || smood == "CFArt")
            {
                try
                {

                    oItems.buscaArticulo(ref this.oCompany, ref lstArt, sValue);
                    if (lstArt.Count > 0)
                    {
                        if (smood == "CFLTOOL")
                            this.oForm.DataSources.UserDataSources.Item("udttool").Value = lstArt[0].ItemName.ToString();
                        else if (smood == "CFArt")
                            this.oForm.DataSources.UserDataSources.Item("UDCStoAd").Value = lstArt[0].AvgPrice.ToString();
                    }
                }
                catch { }
            }
        }

        public void FormToEntityHerramientas()
        {
            try
            {
                Kanan.Mantenimiento.BO2.SBO_KF_Herramientas_INFO oTool = new Kanan.Mantenimiento.BO2.SBO_KF_Herramientas_INFO();
                oItem = this.oForm.Items.Item("txttoolid");
                oEditText = (EditText)this.oItem.Specific;
                oTool.ItemCode = oEditText.String;

                oItem = this.oForm.Items.Item("txttool");
                oEditText = (EditText)this.oItem.Specific;
                oTool.ItemName = oEditText.String;

                oItem = this.oForm.Items.Item("txttoolq");
                oEditText = (EditText)this.oItem.Specific;
                oTool.Quantity = Convert.ToDouble(oEditText.String);

                if (!string.IsNullOrEmpty(oTool.ItemCode) && !string.IsNullOrEmpty(oTool.ItemName))
                {
                    if (oTool.Quantity > 0)
                    {

                        var oHerramienta = ListaHerramientas.FirstOrDefault(x => x.ItemName.Equals(oTool.ItemName, StringComparison.InvariantCultureIgnoreCase));
                        if (oHerramienta == null)
                        {

                            oItem = this.oForm.Items.Item("txttoolid");
                            oEditText = (EditText)this.oItem.Specific;
                            oEditText.String = string.Empty;
                            oItem = this.oForm.Items.Item("txttool");
                            oEditText = (EditText)this.oItem.Specific;
                            oEditText.String = string.Empty;
                            oItem = this.oForm.Items.Item("txttoolq");
                            oEditText = (EditText)this.oItem.Specific;
                            oEditText.String = "0";
                            this.ListaHerramientas.Add(oTool);
                            this.FillHerramientas();
                        }
                        else SBO_Application.StatusBar.SetText("La herramienta ya se ha agregado a la lista", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                    }
                    else SBO_Application.StatusBar.SetText("Proporciona la cantidad de herramientas", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                }
                else SBO_Application.StatusBar.SetText("Proporciona la herramienta para agregarla a la lista", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
            catch (Exception ex)
            {
                SBO_Application.StatusBar.SetText(string.Format("No se ha podido agregar la herramienta. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
        }
        public void FormToEntityMecanico()
        {
            try
            {
                //string sMecanicoID = string.Empty, sMecanico = string.Empty;
                Kanan.Mantenimiento.BO2.SBO_KF_OHEM_INFO oEmpleado = new Kanan.Mantenimiento.BO2.SBO_KF_OHEM_INFO();

                oItem = this.oForm.Items.Item("cmbmeca");
                oCombo = (ComboBox)this.oItem.Specific;
                oEditText = oForm.Items.Item("txtSueldoT").Specific;
                int imecanicoid = -1;
                if (!string.IsNullOrEmpty(oCombo.Selected.Value))
                {
                    if (int.TryParse(oCombo.Selected.Value, out imecanicoid))
                    {
                        if (imecanicoid > 0)
                        {
                            SBO_KF_OHEM_PD oMecanicos = new SBO_KF_OHEM_PD();
                            List<SAPinterface.Data.INFO.Tablas.SBO_KF_OHEM_INFO> lstmecanicos = new List<SAPinterface.Data.INFO.Tablas.SBO_KF_OHEM_INFO>();
                            oMecanicos.listaEmpleados(ref oCompany, ref lstmecanicos, Convert.ToInt32(oCombo.Selected.Value));
                            if (lstmecanicos[0].salary > 0)
                            {
                                var oMecanicoList = ListaMecanicos.FirstOrDefault(x => x.empID == lstmecanicos[0].empID);
                                if (oMecanicoList == null)
                                {
                                    oEmpleado.empID = lstmecanicos[0].empID;
                                    oEmpleado.salary = lstmecanicos[0].salary;
                                    oEmpleado.salaryUnit = lstmecanicos[0].salaryUnit;
                                    oEmpleado.firstName = string.Format("{0} {1} {2}", lstmecanicos[0].firstName, lstmecanicos[0].lastName,lstmecanicos[0].middleName);
                                    ListaMecanicos.Add(oEmpleado);
                                    this.FillMecanicos();
                                    SelectComboBoxValue("cmbmeca", "0");
                                    oEditText.String = "00.0";

                                }
                                else SBO_Application.StatusBar.SetText("El mecanico ya se ha agregado a la lista", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);

                            }
                            else SBO_Application.StatusBar.SetText("El mecanico no cuenta con una configuración de sueldo", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                        }
                        else SBO_Application.StatusBar.SetText("Proporciona el mecanico para agregarla a la lista", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                    }
                    else SBO_Application.StatusBar.SetText("Proporciona el mecanico para agregarla a la lista", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                }
                else SBO_Application.StatusBar.SetText("Proporciona el mecanico para agregarla a la lista", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
            catch (Exception ex)
            {
                SBO_Application.StatusBar.SetText(string.Format("No se ha podido agregar al mecanico. ERROR {0}", ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
        }

        public void FormtoEntityToCostoList()
        {
            string sCadenaAlerta = string.Empty;
            try
            {
                oItem = this.oForm.Items.Item("cmbATipo");
                oCombo = (ComboBox)this.oItem.Specific;
                string sValue = oCombo.Selected.Value;
                SAPbouiCOM.EditText oCajaDesc = null;
                if (!string.IsNullOrEmpty(sValue))
                {
                    switch (sValue)
                    {
                        case "Costo manual":
                            oCajaDesc = oForm.Items.Item("txtADesc1").Specific;
                            break;
                        case "Costo por recurso":
                            oCajaDesc = oForm.Items.Item("txtADesc2").Specific;
                            break;
                        case "Costo por artículo":
                            oCajaDesc = oForm.Items.Item("txtADesc3").Specific;
                            break;
                    }

                    SBOKF_CL_CostosAdicional_INFO oTempCosto = new SBOKF_CL_CostosAdicional_INFO();
                    oTempCosto.U_DESCRIPCION = oCajaDesc.Value.Trim();
                    if (!string.IsNullOrEmpty(oTempCosto.U_DESCRIPCION))
                    {
                        oEditText = oForm.Items.Item("txtAcost").Specific;
                        oTempCosto.U_COSTO = Convert.ToDouble(oEditText.Value);
                        if (oTempCosto.U_COSTO > 0)
                        {
                            if (ListaCostosAdicionales == null)
                                ListaCostosAdicionales = new List<SBOKF_CL_CostosAdicional_INFO>();
                            var oCosto = ListaCostosAdicionales.FirstOrDefault(x => x.U_DESCRIPCION.Equals(oTempCosto.U_DESCRIPCION));
                            if (oCosto == null)
                                ListaCostosAdicionales.Add(oTempCosto);
                            else ListaCostosAdicionales.FirstOrDefault(x => x.U_DESCRIPCION.Equals(oTempCosto.U_DESCRIPCION)).U_COSTO += oTempCosto.U_COSTO;
                            this.FillGastosAdicionales();
                            oEditText = oForm.Items.Item("txtAcost").Specific;
                            oEditText.Value = "";
                            oCajaDesc.Value = "";
                        }
                        else sCadenaAlerta = "El importe del costo debe ser mayor a cero";
                    }
                    else sCadenaAlerta = "Proporciona el nombre del costo adicional";
                }
                else sCadenaAlerta = "Favor de determinar el tipo de costo";

                if(!string.IsNullOrEmpty(sCadenaAlerta))
                    SBO_Application.StatusBar.SetText(sCadenaAlerta, BoMessageTime.bmt_Short);
            }
            catch {
                SBO_Application.StatusBar.SetText("Favor de determinar el tipo de costo", BoMessageTime.bmt_Short);
            }
        
        }

        public void FillHerramientas()
        {
            try
            {
                DataTable tab = oForm.DataSources.DataTables.Item("dgvTools");
                oGrid = this.oForm.Items.Item("gvTool").Specific;
                tab.Rows.Clear();
                int index = 0;
                foreach (Kanan.Mantenimiento.BO2.SBO_KF_Herramientas_INFO oTool in ListaHerramientas)
                {
                    tab.Rows.Add();
                    tab.SetValue("#", index, (index + 1).ToString());
                    tab.SetValue("Folio", index, oTool.ItemCode);
                    tab.SetValue("Herramienta", index, oTool.ItemName);
                    tab.SetValue("Cantidad", index, oTool.Quantity.ToString());
                    index++;
                }
                
            }
            catch (Exception ex) { }
        }

        public void FillMecanicos()
        {
            try
            {
                DataTable tab = oForm.DataSources.DataTables.Item("dgvMecan");
                oGrid = this.oForm.Items.Item("gvMecan").Specific;
                tab.Rows.Clear();
                int index = 0;
                foreach (Kanan.Mantenimiento.BO2.SBO_KF_OHEM_INFO oMecanico in ListaMecanicos)
                {
                    tab.Rows.Add();
                    tab.SetValue("#", index, (index + 1).ToString());
                    tab.SetValue("Folio", index, oMecanico.empID.ToString());
                    tab.SetValue("Mecanico", index, oMecanico.firstName);
                    tab.SetValue("Salario", index, oMecanico.salary.ToString());
                    index++;
                }

            }
            catch (Exception ex) { }
        }

        public void FillGastosAdicionales()
        {
            try
            {
                DataTable tab = oForm.DataSources.DataTables.Item("dgvCstos");
                oGrid = this.oForm.Items.Item("GvCsto").Specific;
                tab.Rows.Clear();
                int index = 0;
                foreach (SBOKF_CL_CostosAdicional_INFO oCosto in ListaCostosAdicionales)
                {
                    tab.Rows.Add();
                    tab.SetValue("#", index, ( index +1).ToString());
                    tab.SetValue("Descripción", index, oCosto.U_DESCRIPCION);
                    tab.SetValue("Costo", index,  String.Format("{0:n}", oCosto.U_COSTO));
                    index++;
                }
                SetCosto();
            }
            catch (Exception ex) {  }
        }
        /// <summary>
        /// Carga la informacion de la lista de DetalleOrden al grid de servicios
        /// </summary>
        /// 
        public void cargaGridEditList()
        {
            try
            {
                List<DetalleOrden> lstDetalle = new List<DetalleOrden>();
                ServicioSAP svData = new ServicioSAP(this.oCompany);
                Servicio sv = new Servicio();
                sv.Propietario = new Kanan.Operaciones.BO2.Empresa();
                sv.Propietario.EmpresaID = this.Config.UserFull.Dependencia.EmpresaID;
                SAPbobsCOM.Recordset records = svData.Consultar(sv);
                if (records.RecordCount > 0)
                {
                    for (int i = 0; i < records.RecordCount; i++)
                    {
                        DetalleOrden oDetalle = new DetalleOrden();
                        oDetalle.OrdenServicio = new OrdenServicio();
                        oDetalle.OrdenServicio.Descripcion = records.Fields.Item("Servicio").Value;
                        oDetalle.OrdenServicio.OrdenServicioID = Convert.ToInt32(records.Fields.Item("U_ServicioID").Value);
                        lstDetalle.Add(oDetalle);
                        records.MoveNext();
                    }
                    var lstDetalleLast = ListaServiciosAgregados;
                    foreach (DetalleOrden svc in ListaServiciosAgregados)
                    {
                        var oServicio = lstDetalle.FirstOrDefault(x => x.OrdenServicio.OrdenServicioID == svc.Servicio.ServicioID);
                        if (oServicio != null)
                            svc.Servicio.Nombre = oServicio.OrdenServicio.Descripcion;
                    }
                }
            }
            catch { }
        }
        public void FillGridServiciosFromList()
        {
            try
            {
                #region Comentado
                /*try
                {
                    if (this.OSRowIndex != null && this.OSRowIndex > -1)
                    {
                        oGrid = this.oForm.Items.Item("grdorden").Specific;
                        int id = Convert.ToInt32(oGrid.DataTable.Columns.Item("colid").Cells.Item((int)this.OSRowIndex).Value);
                        //if (listobservacion != null && listobservacion.Count > 0 && obsid != 0)
                        if (listordenes != null && listordenes.Count > 0)
                        {
                            bool exist = listordenes.Exists(svc => svc.OrdenServicioID == id);
                            if (exist)
                            {
                                this.Entity.OrdenServicio = this.listordenes.FirstOrDefault(svc => svc.OrdenServicioID == id);
                                this.Entity.oUDT.GetByKey(this.Entity.OrdenServicio.OrdenServicioID.ToString());
                                this.Entity.UDTToOrdenServicio();
                            }
                        }
                    }
                }
                catch { }*/
                #endregion Comentado

                DataTable tab = oForm.DataSources.DataTables.Item("dtServSAG");
                oGrid = this.oForm.Items.Item("gdOS").Specific;
                tab.Rows.Clear();
                int index = 0;
                
                foreach (DetalleOrden svc in ListaServiciosAgregados )
                {
                    tab.Rows.Add();
                    tab.SetValue("id", index, svc.DetalleOrdenID ?? 0);
                    tab.SetValue("Vehiculo", index, (svc.Mantenible as Vehiculo).Nombre);
                    tab.SetValue("Servicio", index, svc.Servicio.Nombre);
                    tab.SetValue("Costo", index, svc.Costo.ToString());
                    tab.SetValue("IVA", index, 0);//impuesto.ToString());
                    tab.SetValue("ServicioID", index, svc.Servicio.ServicioID.ToString());
                    index++;
                }
                oGrid.DataTable = tab;
                oGrid.Columns.Item("Vehiculo").Width = 150;
                oGrid.Columns.Item("Servicio").Width = 150;
                oGrid.Columns.Item("Costo").Width = 100;
                oGrid.Columns.Item("IVA").Width = 80;
                oGrid.Columns.Item("id").Visible = false;
                oGrid.Columns.Item("ServicioID").Visible = false;
                oGrid.Columns.Item("Vehiculo").Editable = false;
                oGrid.Columns.Item("Servicio").Editable = false;
                oGrid.Columns.Item("Costo").Editable = false;
                //oGrid.Columns.Item("IVA").Editable = false;
                oGrid.Columns.Item("IVA").Visible = false;
                oGrid.SelectionMode = BoMatrixSelect.ms_Single;
                SetCosto();
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public Boolean FillGridRefaccionesFromList()
        {
            bool bContinuar = false;
            try
            {
                if (Refacciones != null)
                {
                    Matrix oRefacciones = (Matrix)this.oForm.Items.Item("gdRFa").Specific;
                    Columns oColum = (SAPbouiCOM.Columns)oRefacciones.Columns;
                    oRefacciones.Clear();
                    int iLine = 1;
                    foreach (DetalleRefaccion data in Refacciones)
                    {
                        data.iLineaID = iLine;
                        this.oForm.DataSources.UserDataSources.Item("oUMAT01").Value = iLine.ToString();
                        this.oForm.DataSources.UserDataSources.Item("oUMAT02").Value = data.Almacen;
                        this.oForm.DataSources.UserDataSources.Item("oUMAT03").Value = data.Articulo;
                        this.oForm.DataSources.UserDataSources.Item("oUMAT04").Value = Convert.ToDecimal(data.Costo).ToString("0.000");//string.Format("$ {0}", data.Costo);
                        this.oForm.DataSources.UserDataSources.Item("oUMAT05").Value = data.Cantidad.ToString();
                        this.oForm.DataSources.UserDataSources.Item("oUMAT06").Value = data.Total.ToString();
                        iLine++;
                        oRefacciones.AddRow(1, -1);
                    }
                }
                bContinuar = true;
                this.SetCostoRefacciones();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar llenar la tabla de refacciones" + ex.Message);
            }
            return bContinuar;
        }

        private void SetCosto()
        {
            TotalServicios = 0;
            if (this.ListaServiciosAgregados != null)
                TotalServicios += this.ListaServiciosAgregados.Sum(x => x.Costo);
            if (this.ListaCostosAdicionales != null)
                TotalServicios += (Decimal)this.ListaCostosAdicionales.Sum(x => x.U_COSTO);
        }

        public void SetCostoRefacciones()
        {
            if (this.Refacciones != null && this.Refacciones.Count > 0)
            {
                TotalRefacciones = 0;
                foreach (DetalleRefaccion s in this.Refacciones)
                {
                    TotalRefacciones += (s.Costo * s.Cantidad);
                }
            }
            else
                TotalRefacciones = 0;
        }

        public void SetCostoTotal()
        {
            try
            {
                decimal? total = this.TotalServicios + TotalRefacciones;
                oStaticText = this.oForm.Items.Item("lblCostV").Specific;
                oStaticText.Caption = total.ToString();
            }
            catch(Exception ex)
            {
                oStaticText.Caption = "0";
                throw new Exception("Error al mostrar el costo total. " + ex.Message);
            }
        }

        /// <summary>
        /// Actualiza la vista del grid que contiene los servicios agregados
        /// </summary>
        private void UpdateList()
        {
            try
            {
                DataTable tab = oForm.DataSources.DataTables.Item(0);
                if (this.ListaServiciosAgregados != null)
                {
                    if (this.ListaServiciosAgregados.Count > 0)
                    {
                        oGrid = null;
                        oGrid = this.oForm.Items.Item("gdOS").Specific;
                        if (oGrid.DataTable != null)
                            if (oGrid.DataTable.Rows.Count > 0)
                                oGrid.DataTable.Rows.Clear();
                        //int rows = oGrid.DataTable.Rows.Count;
                        int cont = 0, index = 0;
                        int nServicios = this.ListaServiciosAgregados.Count;
                        foreach (DetalleOrden svc in this.ListaServiciosAgregados)
                        {
                            cont++;
                            //decimal impuesto = (decimal)(svc.impuesto / 100);
                            tab.Rows.Add();
                            tab.SetValue("id", index, svc.DetalleOrdenID ?? 0);
                            tab.SetValue("Vehiculo", index, (svc.Mantenible as Vehiculo).Nombre);
                            tab.SetValue("Servicio", index, svc.Servicio.Descripcion);
                            tab.SetValue("Costo", index, svc.Costo.ToString());
                            tab.SetValue("IVA", index, 0);//impuesto.ToString());
                            tab.SetValue("ServicioID", index, svc.Servicio.ServicioID.ToString());
                            index++;
                        }
                        oGrid.DataTable = tab;
                        oGrid.Columns.Item("Vehiculo").Width = 150;
                        oGrid.Columns.Item("Servicio").Width = 150;
                        oGrid.Columns.Item("Costo").Width = 100;
                        oGrid.Columns.Item("IVA").Width = 80;
                        oGrid.Columns.Item("ServicioID").Visible = false;
                        oGrid.Columns.Item("Vehiculo").Editable = false;
                        oGrid.Columns.Item("Servicio").Editable = false;
                        oGrid.Columns.Item("Costo").Editable = false;
                        oGrid.Columns.Item("IVA").Editable = false;
                        oGrid.SelectionMode = BoMatrixSelect.ms_Single;
                        this.SetCosto();
                    }
                    else
                        oGrid.DataTable.Rows.Clear();
                }

            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        /// <summary>
        /// Estable el n�mero de la fila del grid
        /// </summary>
        /// <param name="rowIndex"></param>
        public void SetRowIndex(int rowIndex, string item)
        {
            try
            {
                if (item == "gdRFa")
                    this.iLineaRefaccion = rowIndex;
                else this.RowIndex = rowIndex;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        /// <summary>
        /// Elimina un servicio de la lista de servicios agregados a la orden
        /// </summary>
        public void EliminarServicio()
        {
            try
            {
                if (this.RowIndex != null && this.RowIndex != -1)
                {
                    oGrid = this.oForm.Items.Item("gdOS").Specific;
                    int servicioID = Convert.ToInt32(oGrid.DataTable.Columns.Item("ServicioID").Cells.Item((int)this.RowIndex).Value);
                    if (ListaServiciosAgregados != null && ListaServiciosAgregados.Count > 0)
                    {
                        bool exist = ListaServiciosAgregados.Exists(svc => svc.Servicio.ServicioID == servicioID);
                        if (exist)
                        {
                            int index = this.ListaServiciosAgregados.FindIndex(svc => svc.Servicio.ServicioID == servicioID);
                            this.ListaServiciosAgregados.RemoveAt(index);
                            this.UpdateList();
                        }
                    }
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        /// <summary>
        /// Valida si el objeto en la interface existe en la lista de Servicios ya agregados
        /// </summary>
        public bool ExistDetalleOrden()
        {
            try
            {
                oItem = this.oForm.Items.Item("lblsvcid");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                return !string.IsNullOrEmpty(oStaticText.Caption);
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Obtiene el objeto DetalleOrden seleccionado en el Grid
        /// </summary>
        public void GetDetalleOrdenFromGrid()
        {
            try
            {
                if (this.RowIndex != null && this.RowIndex > -1)
                {
                    oGrid = this.oForm.Items.Item("gdOS").Specific;
                    int licid = Convert.ToInt32(oGrid.DataTable.Columns.Item("id").Cells.Item((int)this.RowIndex).Value);
                    //if (listobservacion != null && listobservacion.Count > 0 && obsid != 0)
                    if (ListaServiciosAgregados != null && ListaServiciosAgregados.Count > 0)
                    {
                        bool exist = ListaServiciosAgregados.Exists(svc => svc.DetalleOrdenID == licid);
                        if (exist)
                        {
                            this.EntityDetalle.oDetalleOrden = this.ListaServiciosAgregados.FirstOrDefault(svc => svc.DetalleOrdenID == licid);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Valida los datos que debe contener un servicio antes de agregarlo a la lista de la orden de servicio
        /// </summary>
        /// <returns></returns>
        private string ValidarServicio()
        {
            try
            {
                string err = string.Empty;
                //Validando Vehiculo
                oCombo = this.oForm.Items.Item("comboVehOS").Specific;
                if (string.IsNullOrEmpty(oCombo.Value) || oCombo.Value.ToString() == "0")
                    err += ", Vehículo";
                //Validando Servicio
                oCombo = this.oForm.Items.Item("comoServOS").Specific;
                if (string.IsNullOrEmpty(oCombo.Value) || oCombo.Value.ToString() == "0")
                    err += ", Servicio";
                //Validando Costo
                //oEditText = this.oForm.Items.Item("txtCostoOS").Specific;
                //if (string.IsNullOrEmpty(oEditText.String.Trim()))
                //    err += ", Costo";
                //else
                //{
                //    if(Convert.ToDouble(oEditText.String.Trim()) <= 0)
                //        err += ", Costo";
                //}
                //Validando Metodo de pago
                //oCombo = this.oForm.Items.Item("comboMPOSv").Specific;
                //if (string.IsNullOrEmpty(oCombo.Value) || oCombo.Value.ToString() == "0")
                //err += ", M�todo de pago";
                if (!string.IsNullOrEmpty(err))
                    if (err.StartsWith(","))
                        err = "Los siguientes campos son obligatorios:\n" + err.Substring(1);
                return err;
            }
            catch (Exception ex)
            {
                return String.Empty;
            }
        }
        #endregion

        #region gestion plantilla
        public bool DeterminaSavePlantilla(out string sNombrePlantilla)
        {
            sNombrePlantilla = string.Empty;
            bool bContinuar = false;
            oItem = this.oForm.Items.Item("cmbrutina");
            oCombo = (ComboBox)this.oItem.Specific;
            string sTypeRutina = oCombo.Selected.Value;
            if (!string.IsNullOrEmpty(sTypeRutina))
            {
                if (sTypeRutina.Equals("1", StringComparison.InvariantCultureIgnoreCase))
                {
                    bContinuar = true;
                    oItem = this.oForm.Items.Item("txtPlanti");
                    EditText txtPlantilla = (EditText)this.oItem.Specific;
                    sNombrePlantilla = txtPlantilla.Value;
                }
            }
            return bContinuar;
        }
        

        public void FormLoadPlantillaMood ()
        {
            try
            {
                oItem = this.oForm.Items.Item("cmbrutina");
                oCombo = (ComboBox)this.oItem.Specific;
                string sTypeRutina = oCombo.Selected.Value;
                if (!string.IsNullOrEmpty(sTypeRutina))
                {
                    oItem = this.oForm.Items.Item("lbltyper");
                    oItem.Visible = false;
                    oItem = this.oForm.Items.Item("txtPlanti");
                    oItem.Visible = false;
                    oItem = this.oForm.Items.Item("cmbPla");
                    oItem.Visible = false;
                    switch (sTypeRutina)
                    {

                        case "1":
                            oItem = this.oForm.Items.Item("lbltyper");
                            oItem.Visible = true;
                            oStaticText = oItem.Specific;
                            oStaticText.Caption = "Nombre de rutina";
                            oItem = this.oForm.Items.Item("txtPlanti");
                            oItem.Visible = true;
                            break;
                        case "2":
                            oItem = this.oForm.Items.Item("lbltyper");
                            oItem.Visible = true;
                            oStaticText = oItem.Specific;
                            oStaticText.Caption = "Selecciona la rutina";
                            oItem = this.oForm.Items.Item("cmbPla");
                            oItem.Visible = true;
                            new GestionGlobales().FillCombo("Ninguno", "Code", "Name", "cmbPla", this.sQueryPlantilla(), this.oForm, this.oCompany);
                            break;
                    }
                }
            }
            catch { }
        }

        public void cargaSalarioEmpleado()
        {
            oItem = this.oForm.Items.Item("cmbmeca");
            oCombo = (ComboBox)this.oItem.Specific;
            oEditText = oForm.Items.Item("txtSueldoT").Specific;
            string sValue = oCombo.Selected.Value;
            if (!string.IsNullOrEmpty(sValue))
            {
                SBO_KF_OHEM_PD oMecanicos = new SBO_KF_OHEM_PD();
                
                List<SAPinterface.Data.INFO.Tablas.SBO_KF_OHEM_INFO> lstmecanicos = new List<SAPinterface.Data.INFO.Tablas.SBO_KF_OHEM_INFO>();
                oMecanicos.listaEmpleados(ref oCompany, ref lstmecanicos, Convert.ToInt32(sValue));
                if (lstmecanicos.Count > 0)
                    oEditText.String = lstmecanicos[0].salary.ToString();
            }
            else oEditText.String = "00.0";
        }

        public void DefineChooseCosto()
        {
            oItem = this.oForm.Items.Item("cmbATipo");
            oCombo = (ComboBox)this.oItem.Specific;
            string sValue = oCombo.Selected.Value;
            if (!string.IsNullOrEmpty(sValue))
            {
                oItem = this.oForm.Items.Item("txtADesc1");
                oItem.Visible = false;
                oItem = this.oForm.Items.Item("txtADesc2");
                oItem.Visible = false;
                oItem = this.oForm.Items.Item("txtADesc3");
                oItem.Visible = false;
                switch (sValue)
                {
                    case "Costo manual":
                        oItem = this.oForm.Items.Item("txtADesc1");                
                        break;
                    case "Costo por recurso":
                        oItem = this.oForm.Items.Item("txtADesc2");
                        break;
                    case "Costo por artículo":
                        oItem = this.oForm.Items.Item("txtADesc3");                
                        break;
                }
                oItem.Visible = true;

            }

        }
      
        public void PlantillaToForm()
        {
            oItem = this.oForm.Items.Item("cmbPla");
            oCombo = (ComboBox)this.oItem.Specific;
            if (!string.IsNullOrEmpty( oCombo.Selected.Value))
            { 
                //carga datos desde tabla KF
                string sQuery = this.sQueryPlantilla(oCombo.Selected.Value.ToString());
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(sQuery);
                if (rs.RecordCount > 0)
                {
                    rs.MoveFirst();
                    oCombo = oForm.Items.Item("cmbsuc").Specific;
                    if (rs.Fields.Item("U_SucursalID").Value != null)
                        oCombo.Select(Convert.ToInt32(rs.Fields.Item("U_SucursalID").Value).ToString());

                    oEditText = oForm.Items.Item("txtEnPro").Specific;
                    if (rs.Fields.Item("U_EntradaProgramada").Value != null)
                        oEditText.String = Convert.ToDateTime(rs.Fields.Item("U_EntradaProgramada").Value).ToString("dd/MM/yyyy");

                    oEditText = oForm.Items.Item("txtSalPro").Specific;
                    if (rs.Fields.Item("U_SalidaProgramada").Value != null)
                        oEditText.String = Convert.ToDateTime(rs.Fields.Item("U_SalidaProgramada").Value).ToString("dd/MM/yyyy");

                    oEditText = oForm.Items.Item("txtFechaOS").Specific;
                    if (rs.Fields.Item("U_FechaRevision").Value != null)
                        oEditText.String = Convert.ToDateTime(rs.Fields.Item("U_FechaRevision").Value).ToString("dd/MM/yyyy");

                    oEditText = oForm.Items.Item("txtDescOs").Specific;
                    if (rs.Fields.Item("U_Descripcion").Value != null)
                        oEditText.String = Convert.ToString(rs.Fields.Item("U_Descripcion").Value);


                    oCombo = oForm.Items.Item("cmbResp").Specific;
                    if (rs.Fields.Item("U_ResponsableID").Value != null)
                        oCombo.Select(Convert.ToInt32(rs.Fields.Item("U_ResponsableID").Value).ToString());

                    oCombo = oForm.Items.Item("cmbAprob").Specific;
                    if (rs.Fields.Item("U_AprobadorID").Value != null)
                        oCombo.Select(Convert.ToInt32(rs.Fields.Item("U_AprobadorID").Value).ToString());

                    oItem = oForm.Items.Item("CBTI");
                    oCheckBoxTallerInterno = (CheckBox)(oItem.Specific);
                    if (rs.Fields.Item("U_TallerInterno").Value != null)
                    {
                        string sInterno = Convert.ToString(rs.Fields.Item("U_TallerInterno").Value);
                        oCheckBoxTallerInterno.Checked = sInterno.Equals("Y", StringComparison.InvariantCultureIgnoreCase);
                    }
                    else oCheckBoxTallerInterno.Checked = false;
                    /* = oCheckBoxTallerInterno.Checked;*/

                    #region recuperar detalle de plantilla
                    oItem = this.oForm.Items.Item("cmbPla");
                    oCombo = (ComboBox)this.oItem.Specific;
                    sQuery = this.sQueryPlantillaDetalle(oCombo.Selected.Value.ToString());
                    rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    rs.DoQuery(sQuery);
                    ListaServiciosAgregados = new List<DetalleOrden>();
                    if (rs.RecordCount > 0)
                    {
                        
                        int index = 0;
                        for (int i = 0; i < rs.RecordCount; i++)
                        {
                            DetalleOrden otemp = new DetalleOrden();
                            otemp.DetalleOrdenID = index;
                            otemp.Mantenible = new Vehiculo();
                            otemp.Servicio = new Servicio();
                            if (rs.Fields.Item("U_MantenibleID").Value != null)
                                otemp.Mantenible.MantenibleID = Convert.ToInt32(rs.Fields.Item("U_MantenibleID").Value);

                            if (rs.Fields.Item("U_VehiculoNombre").Value != null)
                                (otemp.Mantenible as Vehiculo).Nombre = Convert.ToString(rs.Fields.Item("U_VehiculoNombre").Value);

                            if (rs.Fields.Item("U_ServicioID").Value != null)
                                otemp.Servicio.ServicioID = Convert.ToInt32(rs.Fields.Item("U_ServicioID").Value);

                            if (rs.Fields.Item("U_NombreServicio").Value != null)
                                otemp.Servicio.Nombre = Convert.ToString(rs.Fields.Item("U_NombreServicio").Value);

                            if (rs.Fields.Item("U_Costo").Value != null)
                                otemp.Costo = Convert.ToDecimal(rs.Fields.Item("U_Costo").Value);
                            otemp.UUID = Guid.NewGuid().ToString();
                            ListaServiciosAgregados.Add(otemp);
                            index++;
                            rs.MoveNext();
                        }
                    }
                    this.FillGridServiciosFromList();
                    #endregion recuperar detalle de plantilla

                    #region recupera datos de refacciones plantilla
                    sQuery = this.sQueryPlantillaDetalle(oCombo.Selected.Value.ToString(), true);
                    rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    rs.DoQuery(sQuery);
                    Refacciones = new List<DetalleRefaccion>();
                    if (rs.RecordCount > 0)
                    {                         
                         for (int i = 0; i < rs.RecordCount; i++)
                         {
                             DetalleRefaccion otemp = new DetalleRefaccion();                            

                             if (rs.Fields.Item("U_Costo").Value != null)
                                otemp.Costo = Convert.ToDecimal(rs.Fields.Item("U_Costo").Value);

                             if (rs.Fields.Item("U_Cantidad").Value != null)
                                otemp.Cantidad = Convert.ToDecimal(rs.Fields.Item("U_Cantidad").Value);

                              if (rs.Fields.Item("U_Total").Value != null)
                                otemp.Total = Convert.ToDecimal(rs.Fields.Item("U_Total").Value);

                             if (rs.Fields.Item("U_Articulo").Value != null)
                             otemp.Articulo  = Convert.ToString(rs.Fields.Item("U_Articulo").Value);

                             if (rs.Fields.Item("U_Almacen").Value != null)
                             otemp.Almacen  = Convert.ToString(rs.Fields.Item("U_Almacen").Value);

                             if (rs.Fields.Item("U_CodAlmcn").Value != null)
                             otemp.CodigoAlmacen  = Convert.ToString(rs.Fields.Item("U_CodAlmcn").Value);

                             Refacciones.Add(otemp);
                             rs.MoveNext();
                         }
                    }
                    this.ListToMatrix();
                    #endregion recupera datos de refacciones plantilla
                }
                
            }        
        }
        #endregion gestion plantilla

        #region Orden Servicio
        /// <summary>
        /// Obtine los datos de la vista hacia a la entidad
        /// </summary>
        public void FormToEntity()
        {
            try
            {
                oStaticText = oForm.Items.Item("lblid").Specific;
                if (!string.IsNullOrEmpty(oStaticText.Caption.Trim()))
                    Entity.OrdenServicio.OrdenServicioID = Convert.ToInt32(oStaticText.Caption);
                else
                    Entity.OrdenServicio.OrdenServicioID = null;

                int proveedorID;//Proveedor
                oCombo = oForm.Items.Item("comboProOS").Specific;
                if (int.TryParse(this.oCombo.Value, out proveedorID))
                {
                    Entity.OrdenServicio.ProveedorServicio.ProveedorID = proveedorID;
                }
                else
                {
                    Entity.OrdenServicio.ProveedorServicio = new Proveedor();
                }
                int sucursalID;//Sucursal
                oCombo = oForm.Items.Item("cmbsuc").Specific;
                if (int.TryParse(this.oCombo.Value, out sucursalID))
                {
                    Entity.OrdenServicio.SucursalOrden.SucursalID = sucursalID;
                }
                else
                {
                    Entity.OrdenServicio.SucursalOrden = new Sucursal();
                }
                DateTime utcTime;//Fecha de servicio
                oEditText = oForm.Items.Item("txtFechaOS").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                {
                    //if (this.dateHelper == null)
                    //    this.GetUserConfigs();
                    //utcTime = this.dateHelper.ToServerTime(Convert.ToDateTime(oEditText.String.Trim()));
                    //Entity.OrdenServicio.Fecha = utcTime;

                    Entity.OrdenServicio.Fecha = Convert.ToDateTime(oEditText.String.Trim());
                }

                oEditText = oForm.Items.Item("txtHoraOS").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                {
                    //if (this.dateHelper == null)
                    //    this.GetUserConfigs();
                    //utcTime = this.dateHelper.ToServerTime(Convert.ToDateTime(oEditText.String.Trim()));
                    //Entity.OrdenServicio.Fecha = utcTime;

                    Entity.OrdenServicio.Fecha =  Convert.ToDateTime(((DateTime)Entity.OrdenServicio.Fecha).ToShortDateString() + " " + oEditText.String.Trim());       // Convert.ToDateTime(oEditText.String.Trim());
                }

                
                //DateTime EnPro;//Fecha de entrada programada
                oEditText = oForm.Items.Item("txtEnPro").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                {
                    //if (this.dateHelper == null)
                    //    this.GetUserConfigs();
                    //EnPro = this.dateHelper.ToServerTime(Convert.ToDateTime(oEditText.String.Trim()));
                    //Entity.OrdenServicio.FechaRecepcion = EnPro;

                    Entity.OrdenServicio.FechaRecepcion = Convert.ToDateTime(oEditText.String);
                }

                //DateTime SalPro;//Fecha de salida programada
                oEditText = oForm.Items.Item("txtSalPro").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                {
                    //if (this.dateHelper == null)
                    //    this.GetUserConfigs();
                    //SalPro = this.dateHelper.ToServerTime(Convert.ToDateTime(oEditText.String.Trim()));
                    //Entity.OrdenServicio.FechaLiberacion = SalPro;

                    Entity.OrdenServicio.FechaLiberacion = Convert.ToDateTime(oEditText.String);
                }

                //DateTime EnReal;//Fecha de entrada real
                oEditText = oForm.Items.Item("txtEnReal").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                {
                    //if (this.dateHelper == null)
                    //    this.GetUserConfigs();
                    //EnReal = this.dateHelper.ToServerTime(Convert.ToDateTime(oEditText.String.Trim()));
                    //Entity.OrdenServicio.FechaRecepcionReal = EnReal;

                    Entity.OrdenServicio.FechaRecepcionReal = Convert.ToDateTime(oEditText.String);
                }

                //DateTime SalReal;//Fecha de salida real
                oEditText = oForm.Items.Item("txtSalReal").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                {
                    //if (this.dateHelper == null)
                    //    this.GetUserConfigs();
                    //SalReal = this.dateHelper.ToServerTime(Convert.ToDateTime(oEditText.String.Trim()));
                    //Entity.OrdenServicio.FechaLiberacionReal = SalReal;

                    Entity.OrdenServicio.FechaLiberacionReal = Convert.ToDateTime(oEditText.String);
                }

                //Folio
                oEditText = oForm.Items.Item("txtFolioOS").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    Entity.OrdenServicio.Folio = oEditText.String.Trim();
                //Descripcion
                oEditText = oForm.Items.Item("txtDescOs").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    Entity.OrdenServicio.Descripcion = oEditText.String.Trim();
                //Metodo de pago
                oCombo = oForm.Items.Item("comboMPOSv").Specific;
                int pagoID;
                if (int.TryParse(oCombo.Value, out pagoID))
                {
                    Entity.OrdenServicio.TipoDePago.TipoPagoID = pagoID;
                    Entity.OrdenServicio.TipoDePago.TipoPago = oCombo.Selected.Description;
                }
                //Detalle de pago
                oEditText = oForm.Items.Item("txtDetOSv").Specific;
                if (!string.IsNullOrEmpty(oEditText.String))
                    Entity.OrdenServicio.TipoDePago.DetallePago = oEditText.String.Trim();
                //Costo
                oStaticText = this.oForm.Items.Item("lblCostV").Specific;
                if (!string.IsNullOrEmpty(oStaticText.Caption.Trim()))                
                    Entity.OrdenServicio.Costo = Convert.ToDecimal(oStaticText.Caption.Trim());
                
                
                //Impuesto
                oStaticText = oForm.Items.Item("lblImpOSv").Specific;
                if (!string.IsNullOrEmpty(oStaticText.Caption))
                    Entity.OrdenServicio.Impuesto = Convert.ToDecimal(oStaticText.Caption.Trim());
                //Responsable dela orden de servicio
                oCombo = oForm.Items.Item("cmbResp").Specific;
                int respID;
                if (int.TryParse(oCombo.Value, out respID))
                    Entity.OrdenServicio.ResponsableOrdenServicio.EmpleadoID = respID;
                //Aprobador de la orden de servico
                oCombo = oForm.Items.Item("cmbAprob").Specific;
                int apID;
                if (int.TryParse(oCombo.Value, out apID))
                    Entity.OrdenServicio.AprobadorOrdenServicio.EmpleadoID = apID;
                Entity.OrdenServicio.FechaCaptura = DateTime.UtcNow;

                oCombo = oForm.Items.Item("cmbestatus").Specific;
                Entity.OrdenServicio.EstatusOrden.EstatusOrdenID = Convert.ToInt32(oCombo.Selected.Value);

                #region nuevos datos impso & hertz
                //--cmbSyscr
                //--cmbCompo

                try
                {
                    oCombo = oForm.Items.Item("cmbSyscr").Specific;
                    if (!string.IsNullOrEmpty(oCombo.Value))
                    {
                        Entity.OrdenServicio.SistemaID = Convert.ToInt32(oCombo.Value);
                        oCombo = oForm.Items.Item("cmbCompo").Specific;
                        if (!string.IsNullOrEmpty(oCombo.Value))                        
                            Entity.OrdenServicio.ComponenteID = Convert.ToInt32(oCombo.Value);
                        else Entity.OrdenServicio.SistemaID = 0;
                    
                    }
                }
                catch {
                    throw new Exception("No se ha determinado el sistema - componente");
                }


                oCombo = oForm.Items.Item("cmblevel").Specific;
                try
                {
                    Entity.OrdenServicio.PrioridadID = Convert.ToInt32(this.oCombo.Value);
                    switch (Entity.OrdenServicio.PrioridadID)
                    {
                        case 0:
                            Entity.OrdenServicio.Prioridad = "No urgente";
                            break;
                        case 1:
                            Entity.OrdenServicio.Prioridad = "Urgente";
                            break;
                        case 2:
                            Entity.OrdenServicio.Prioridad = "Muy urgente";
                            break;
                    }
                }
                catch { }

                oEditText = oForm.Items.Item("txtInspec").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))                
                    Entity.OrdenServicio.inspeccionID = oEditText.String.Trim();

                oEditText = oForm.Items.Item("txtClisap").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    Entity.OrdenServicio.socioCliente = oEditText.String.Trim();


                oEditText = oForm.Items.Item("txthEntr").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                {
                    Entity.OrdenServicio.HoraEnProgramada = oEditText.String.Trim();
                    if (Entity.OrdenServicio.HoraEnProgramada != "00:00")
                    {
                        if (!this.formatoHora(Entity.OrdenServicio.HoraEnProgramada))
                            throw new Exception("El formato de hora entrada programada no es valida");
                    }
                }

                oEditText = oForm.Items.Item("txtHsal").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                {
                    Entity.OrdenServicio.HoraSalProgramada = oEditText.String.Trim();
                    if (Entity.OrdenServicio.HoraSalProgramada != "00:00")
                    {
                        if (!this.formatoHora(Entity.OrdenServicio.HoraSalProgramada))
                            throw new Exception("El formato de hora salida programada no es valido");
                    }
                }

                oEditText = oForm.Items.Item("txtHReale").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                {
                    Entity.OrdenServicio.HoraEnReal = oEditText.String.Trim();
                    if (Entity.OrdenServicio.HoraEnReal != "00:00")
                    {
                        if (!this.formatoHora(Entity.OrdenServicio.HoraEnReal))
                            throw new Exception("El formato de hora entrada real no es valido");
                    }
                }

                oEditText = oForm.Items.Item("txtHSalr").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                {
                    Entity.OrdenServicio.HoraSalReal = oEditText.String.Trim();
                    if (Entity.OrdenServicio.HoraSalReal != "00:00")
                    {
                        if (!this.formatoHora(Entity.OrdenServicio.HoraSalReal))
                            throw new Exception("El formato de hora salida real no es valido");
                    }
                }

                #endregion nuevos datos impso & hertz

                //OrdenServicio.alertas.empresa = this.Config.UserFull.Dependencia;
                //OrdenServicio.SucursalOrden = this.Config.UserFull.EmpleadoSucursal;
                Entity.OrdenServicio.EmpresaOrden = this.Config.UserFull.Dependencia;
                Entity.OrdenServicio.EncargadoOrdenServicio = this.Config.UserFull;
                Entity.OrdenServicio.DetalleOrden = this.ListaServiciosAgregados;
                Entity.OrdenServicio.FolioOrden = (FolioOrden)EFolio.folio.Clone();
                Entity.OrdenServicio.alertas = new AlertaMantenimiento();
                Entity.OrdenServicio.alertas.servicio = new Servicio();
                Entity.OrdenServicio.alertas.servicio.ServicioID = 0;
                //this.Entity.OrdenServicio = OrdenServicio;
                EntityRefaccion = new DetalleRefaccionesSAP(this.oCompany);
                EntityRefaccion.oDetalleRefaccion = new DetalleRefaccion();
                EntityRefaccion.oDetalleRefaccion.OrdenServicio = Entity.OrdenServicio;
                //Se valid� que sea diferente de nulo para no devolver 0 en lugar de null y evitar que actualice una orden
                //en lugar de insertarla.
                if (!String.IsNullOrEmpty(this.FolioOrdenSercio))
                    EntityRefaccion.oDetalleRefaccion.OrdenServicio.OrdenServicioID = Convert.ToInt32(this.FolioOrdenSercio);
                EntityRefaccion.oDetalleRefaccion.Mantenible = new Vehiculo();
                EntityRefaccion.oDetalleRefaccion.Servicio = new Servicio();
                EntityRefaccion.oDetalleRefaccion.Servicio.ServicioID = Convert.ToInt32(oCombo.Value);
                EntityRefaccion.oDetalleRefaccion.TipoMantenibleID = 1;

            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        /// <summary>
        /// Obtine los datos de la entidad hacia la vista
        /// </summary>
        public void EntityToForm()
        {
            try
            {
                oStaticText = oForm.Items.Item("lblid").Specific;
                if (Entity.OrdenServicio.OrdenServicioID != null && Entity.OrdenServicio.OrdenServicioID > 0)
                    oStaticText.Caption = Entity.OrdenServicio.OrdenServicioID.ToString();
                else
                {
                    oStaticText.Caption = String.Empty;
                }

                oCombo = oForm.Items.Item("comboProOS").Specific;
                if (Entity.OrdenServicio.ProveedorServicio != null && Entity.OrdenServicio.ProveedorServicio.ProveedorID != null)
                {
                    oCombo.Select(Entity.OrdenServicio.ProveedorServicio.ProveedorID.ToString());
                }

                oCombo = oForm.Items.Item("cmbsuc").Specific;
                if (Entity.OrdenServicio.SucursalOrden != null && Entity.OrdenServicio.SucursalOrden.SucursalID != null)
                {
                    oCombo.Select(Entity.OrdenServicio.SucursalOrden.SucursalID.ToString());
                }

                //DateTime utcTime;//Fecha de servicio
                oEditText = oForm.Items.Item("txtFechaOS").Specific;
                if (Entity.OrdenServicio.Fecha != null)
                {
                    //if (this.dateHelper == null)
                    //    this.GetUserConfigs();
                    //utcTime = this.dateHelper.ToUserTime((DateTime)Entity.OrdenServicio.Fecha);
                    //oEditText.String = utcTime.ToShortDateString();

                    oEditText.String = Entity.OrdenServicio.Fecha.Value.ToString("dd-MM-yyyy");
                }

                oEditText = oForm.Items.Item("txtHoraOS").Specific;
                if (Entity.OrdenServicio.Fecha != null)
                {
                    oEditText.String = Entity.OrdenServicio.Fecha.Value.ToString("hh:mm");
                }


                //DateTime EnPro;//Fecha de entrada programada
                oEditText = oForm.Items.Item("txtEnPro").Specific;
                if (Entity.OrdenServicio.FechaRecepcion != null)
                {
                    //if (this.dateHelper == null)
                    //    this.GetUserConfigs();
                    //EnPro = this.dateHelper.ToUserTime((DateTime)Entity.OrdenServicio.FechaRecepcion);
                    //oEditText.String = EnPro.ToShortDateString();

                    oEditText.String = Entity.OrdenServicio.FechaRecepcion.Value.ToString("dd/MM/yyyy");
                }

                //DateTime SalPro;//Fecha de salida programada
                oEditText = oForm.Items.Item("txtSalPro").Specific;
                if (Entity.OrdenServicio.FechaLiberacion != null)
                {
                    //if (this.dateHelper == null)
                    //    this.GetUserConfigs();
                    //SalPro = this.dateHelper.ToUserTime((DateTime)Entity.OrdenServicio.FechaLiberacion);
                    //oEditText.String = SalPro.ToShortDateString();

                    oEditText.String = Entity.OrdenServicio.FechaLiberacion.Value.ToString("dd/MM/yyyy");
                }

                //DateTime EnReal;//Fecha de entrada real
                oEditText = oForm.Items.Item("txtEnReal").Specific;
                if (Entity.OrdenServicio.FechaRecepcionReal != null)
                {
                    //if (this.dateHelper == null)
                    //    this.GetUserConfigs();
                    //EnReal = this.dateHelper.ToUserTime((DateTime)Entity.OrdenServicio.FechaRecepcionReal);
                    //oEditText.String = EnReal.ToShortDateString();

                    oEditText.String = Entity.OrdenServicio.FechaRecepcionReal.Value.ToString("dd/MM/yyyy");
                }

                //DateTime SalReal;//Fecha de salida real
                oEditText = oForm.Items.Item("txtSalReal").Specific;
                if (Entity.OrdenServicio.FechaLiberacionReal != null)
                {
                    //if (this.dateHelper == null)
                    //    this.GetUserConfigs();
                    //SalReal = this.dateHelper.ToUserTime((DateTime)Entity.OrdenServicio.FechaLiberacionReal);
                    //oEditText.String = SalReal.ToShortDateString();

                    oEditText.String = Entity.OrdenServicio.FechaLiberacionReal.Value.ToString("dd/MM/yyyy");
                }

                //Folio
                oEditText = oForm.Items.Item("txtFolioOS").Specific;
                if (!string.IsNullOrEmpty(Entity.OrdenServicio.Folio))
                    oEditText.String = Entity.OrdenServicio.Folio;
                //Descripcion
                oEditText = oForm.Items.Item("txtDescOs").Specific;
                if (!string.IsNullOrEmpty(Entity.OrdenServicio.Descripcion))
                    oEditText.String = Entity.OrdenServicio.Descripcion;
                //Metodo de pago
                oCombo = oForm.Items.Item("comboMPOSv").Specific;
                if (Entity.OrdenServicio.TipoDePago != null && Entity.OrdenServicio.TipoDePago.TipoPagoID != null)
                {
                    oCombo.Select(Entity.OrdenServicio.TipoDePago.TipoPagoID.ToString());
                }
                //Detalle de pago
                oEditText = oForm.Items.Item("txtDetOSv").Specific;
                if (Entity.OrdenServicio.TipoDePago != null && !string.IsNullOrEmpty(Entity.OrdenServicio.TipoDePago.DetallePago))
                    oEditText.String = Entity.OrdenServicio.TipoDePago.DetallePago;
                //Costo
                oStaticText = this.oForm.Items.Item("lblCostV").Specific;
                if (Entity.OrdenServicio.Costo != null)
                    oStaticText.Caption = Entity.OrdenServicio.Costo.ToString();

                //Impuesto
                //ESTE VALOR PROVOCA BUG
                /*oEditText = oForm.Items.Item("lblImpOSv").Specific;
                if (Entity.OrdenServicio.Impuesto != null)
                {
                    //bool boolTextBoxImpuesto = oForm.Items.Item("lblImpOSv").Enabled;

                    oEditText.String = Entity.OrdenServicio.Impuesto.ToString();
                    //oForm.Items.Item("lblImpOSv").Enabled = boolTextBoxImpuesto;

                }*/

                //Responsable dela orden de servicio
                oCombo = oForm.Items.Item("cmbResp").Specific;
                if (Entity.OrdenServicio.ResponsableOrdenServicio != null && Entity.OrdenServicio.ResponsableOrdenServicio.EmpleadoID != null)
                    oCombo.Select(Entity.OrdenServicio.ResponsableOrdenServicio.EmpleadoID.ToString());
                //Aprobador de la orden de servico
                oCombo = oForm.Items.Item("cmbAprob").Specific;
                if (Entity.OrdenServicio.AprobadorOrdenServicio != null && Entity.OrdenServicio.AprobadorOrdenServicio.EmpleadoID != null)
                    oCombo.Select(Entity.OrdenServicio.AprobadorOrdenServicio.EmpleadoID.ToString());

                //oCombo = oForm.Items.Item("cmbestatus").Specific;
                //OrdenServicio.EstatusOrden.EstatusOrdenID = Convert.ToInt32(oCombo.Selected.Value);

                //OrdenServicio.alertas.empresa = this.Config.UserFull.Dependencia;
                //OrdenServicio.SucursalOrden = this.Config.UserFull.EmpleadoSucursal;
                //OrdenServicio.DetalleOrden = this.GetListDetalleOrdenFromListServicios();
                EFolio.folio = Entity.OrdenServicio.FolioOrden;
                oStaticText = oForm.Items.Item("txtfolio").Specific;
                oStaticText.Caption = string.Format("{0}/{1}", Entity.OrdenServicio.FolioOrden.Folio,
                   Entity.OrdenServicio.FolioOrden.AnioFolio);

                if (this.Entity.OrdenServicio.DetalleOrden.Any())
                {
                    this.ListaServiciosAgregados = this.Entity.OrdenServicio.DetalleOrden;
                    //this.FillGridServiciosFromList();
                }

                #region nuevos datos impso & hertz
                try
                {
                    if (Entity.OrdenServicio.ComponenteID > 0 && Entity.OrdenServicio.SistemaID > 0)
                    {
                        oCombo = oForm.Items.Item("cmbSyscr").Specific;
                        oCombo.Select(Entity.OrdenServicio.SistemaID.ToString(), BoSearchKey.psk_ByValue);

                        oCombo = oForm.Items.Item("cmbCompo").Specific;
                        oCombo.Select(Entity.OrdenServicio.ComponenteID.ToString(), BoSearchKey.psk_ByValue);

                        oCombo = oForm.Items.Item("cmblevel").Specific;
                        oCombo.Select(Entity.OrdenServicio.PrioridadID.ToString(), BoSearchKey.psk_ByValue);
                    }
                }
                catch { }

                oEditText = oForm.Items.Item("txtInspec").Specific;
                oEditText.String = Entity.OrdenServicio.inspeccionID;

                oEditText = oForm.Items.Item("txtClisap").Specific;
                oEditText.String = Entity.OrdenServicio.socioCliente;

                oEditText = oForm.Items.Item("txthEntr").Specific;
                oEditText.String = Entity.OrdenServicio.HoraEnProgramada;

                oEditText = oForm.Items.Item("txtHsal").Specific;
                oEditText.String = Entity.OrdenServicio.HoraSalProgramada;

                oEditText = oForm.Items.Item("txtHReale").Specific;
                oEditText.String = Entity.OrdenServicio.HoraEnReal;

                oEditText = oForm.Items.Item("txtHSalr").Specific;
                oEditText.String = Entity.OrdenServicio.HoraSalReal;
                #endregion nuevos datos impso & hertz

            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        bool formatoHora(string hora)
        {
            bool bEstatus = false;
            try
            {
                System.Globalization.CultureInfo provider = System.Globalization.CultureInfo.InvariantCulture;
                DateTime dt = DateTime.ParseExact(hora.Replace(":", string.Empty), "HHmm", provider);
                if (dt.Hour != null)
                    bEstatus = true;
            }
            catch { }
            return bEstatus;
        }

        public Int32 GetEstatus()
        {
            Int32 Estatus;
            Int32? EstatusActual = 0;
            Int32? EstatusGuardado = null;

            oCombo = oForm.Items.Item("cmbestatus").Specific;
            EstatusGuardado = Entity.OrdenServicio.EstatusOrden.EstatusOrdenID;
            EstatusActual = Convert.ToInt32(oCombo.Selected.Value);

            if ((Int32)EstatusActual == (Int32)EstatusGuardado)
                Estatus = Convert.ToInt32(EstatusActual);
            else
                Estatus = 0;

            return Estatus;
        }

        public void AsignarFocoEntity()
        {
            this.oForm.Items.Item("tabDataOrd").Click();
        }

        public void ConsumeSiguienteFolioWS()
        {

            try
            {
                OrdenServicioWS oTemp = new OrdenServicioWS(Guid.Parse(this.Config.UserFull.Usuario.PublicKey), this.Config.UserFull.Usuario.PrivateKey);
                OrdenServicio oFolio = oTemp.recuperaSiguienteFolio(Convert.ToString(this.Config.UserFull.Dependencia.EmpresaID));

                Entity.OrdenServicio.FolioOrden = new FolioOrden();
                Entity.OrdenServicio.FolioOrden.Folio = Convert.ToInt32(oFolio.Folio);
                Entity.OrdenServicio.FolioOrden.AnioFolio = Convert.ToInt32(oFolio.AnioFolio);
                Entity.OrdenServicio.FolioOrden.Propietario.EmpresaID = (int)this.Config.UserFull.Dependencia.EmpresaID;
                EFolio.folio = Entity.OrdenServicio.FolioOrden;

                oStaticText = oForm.Items.Item("txtfolio").Specific;
                oStaticText.Caption = string.Format("{0}/{1}", Entity.OrdenServicio.FolioOrden.Folio,
                   Entity.OrdenServicio.FolioOrden.AnioFolio);

            }
            catch { }
        }
        /// <summary>
        /// Vacia todos los campos del formulario
        /// </summary>
        public void EmptyEntityToForm()
        {
            try
            {
                this.ListaMecanicos = new List<Kanan.Mantenimiento.BO2.SBO_KF_OHEM_INFO>();
                this.ListaHerramientas = new List<Kanan.Mantenimiento.BO2.SBO_KF_Herramientas_INFO>();
                this.Refacciones = new List<DetalleRefaccion>();
                this.ListaServiciosAgregados = new List<DetalleOrden>();
                this.Refacciones = new List<DetalleRefaccion>();
                this.ListaCostosAdicionales = new List<SBOKF_CL_CostosAdicional_INFO>();
                this.Entity = new OrdenServicioSAP(ref this.oCompany);
                this.Entity.OrdenServicio = new OrdenServicio();
                try
                {
                    DataTable tab = oForm.DataSources.DataTables.Item("dgvCstos");
                    oGrid = this.oForm.Items.Item("GvCsto").Specific;
                    tab.Rows.Clear();
                }
                catch { }
                try
                {
                    Matrix oRefacciones = (Matrix)this.oForm.Items.Item("gdRFa").Specific;
                    Columns oColum = (SAPbouiCOM.Columns)oRefacciones.Columns;
                    oRefacciones.Clear();
                }
                catch { }

                try
                {
                    DataTable tab = oForm.DataSources.DataTables.Item("dtServSAG");
                    oGrid = this.oForm.Items.Item("gdOS").Specific;
                    tab.Rows.Clear();
                }
                catch { }

                try
                {
                    DataTable tab = oForm.DataSources.DataTables.Item("dgvMecan");
                    oGrid = this.oForm.Items.Item("gvMecan").Specific;
                    tab.Rows.Clear();
                }
                catch { }

                try
                {
                    DataTable tab = oForm.DataSources.DataTables.Item("dgvTools");
                    oGrid = this.oForm.Items.Item("gvTool").Specific;
                    tab.Rows.Clear();
                }
                catch { }

                #region Tab Datos de la Orden
                oStaticText = oForm.Items.Item("lblid").Specific;
                oStaticText.Caption = String.Empty;

                oEditText = this.oForm.Items.Item("txtFechaOS").Specific;//Fecha
                oEditText.String = "";
                oEditText = this.oForm.Items.Item("txtHoraOS").Specific;//Fecha
                oEditText.String = "";
                
                oEditText = this.oForm.Items.Item("txtFolioOS").Specific;//Folio
                oEditText.String = "";
                oEditText = this.oForm.Items.Item("txtDescOs").Specific;//Descripcion
                oEditText.String = "";

                oCombo = this.oForm.Items.Item("comboMPOSv").Specific;//Combo Metodo de pago
                oCombo.Select("0", BoSearchKey.psk_ByValue);
                oItem = this.oForm.Items.Item("txtDetOSv");//DetallePago
                //oItem.Visible = true;
                oEditText = oItem.Specific;
                oEditText.String = "";
                oStaticText = this.oForm.Items.Item("lblImpOSv").Specific;//Impuesto
                oStaticText.Caption = "";

                oStaticText = this.oForm.Items.Item("lblCostV").Specific;
                oStaticText.Caption = "0.00";

                oCombo = this.oForm.Items.Item("cmbResp").Specific;//Combo Responsable
                oCombo.Select("0", BoSearchKey.psk_ByValue);
                oCombo = this.oForm.Items.Item("cmbsuc").Specific;//Combo Sucursal
                oCombo.Select("0", BoSearchKey.psk_ByValue);
                oCombo = this.oForm.Items.Item("cmbAprob").Specific;//Combo Aprobador
                oCombo.Select("0", BoSearchKey.psk_ByValue);
                oCombo = this.oForm.Items.Item("comboProOS").Specific;//Combo Proveedor
                oCombo.Select("0", BoSearchKey.psk_ByValue);

                oEditText = this.oForm.Items.Item("txtEnPro").Specific;//Entrada programada
                oEditText.String = "";
                oEditText = this.oForm.Items.Item("txtSalPro").Specific;//Salida programada
                oEditText.String = "";
                oEditText = this.oForm.Items.Item("txtEnReal").Specific;//Entrada real
                oEditText.String = "";
                oEditText = this.oForm.Items.Item("txtSalReal").Specific;//Salida real
                oEditText.String = "";

                //oFolder = this.oForm.Items.Item("tabDataOrd").Specific;
                //SetCurrentFolioOrden();
                //oFolder.Select();
                #endregion

                #region Datos de los servicios Agregados
                this.EmptyServicioToForm();
                //oCombo = this.oForm.Items.Item("comboVehOS").Specific;//Combo Vehiculo
                //oCombo.Select("", BoSearchKey.psk_ByDescription);
                //oCombo = this.oForm.Items.Item("comoServOS").Specific;//Combo Servicio
                //oCombo.Select("", BoSearchKey.psk_ByDescription);
                //oEditText = this.oForm.Items.Item("txtCostoOS").Specific;//Costo
                //oEditText.String = "";
                #endregion
                
                this.PrepareObjects();
                if (this.ListaServiciosAgregados != null && this.ListaServiciosAgregados.Count > 0)
                    this.ListaServiciosAgregados.Clear();
                oGrid.DataTable.Rows.Clear();
            }
            catch (Exception ex) {  }
        }
        /// <summary>
        /// Obtiene la orden por medio de su id
        /// </summary>
        public void GetEntityByID()
        {
            try
            {
                var rs = this.Entity.Consultar(this.Entity.OrdenServicio);
                if (Entity.HashOrdenServicio(rs))
                {
                    this.Entity.oUDT.GetByKey(rs.Fields.Item("Code").Value.ToString());
                    this.Entity.UDTToOrdenServicio();
                    var rsd = this.EntityDetalle.ConsultarByOrdenID(this.Entity.OrdenServicio.OrdenServicioID);
                    if (EntityDetalle.HashoDetalleOrden(rsd))
                    {
                        Entity.OrdenServicio.DetalleOrden = EntityDetalle.RecordSetToListDetalleOrden(rs);
                    }
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public string ValidaSolicitudOrden()
        {
            string err = string.Empty;
            //Validando proveedor
            //oCombo = this.oForm.Items.Item("comboProOS").Specific;
            //if (string.IsNullOrEmpty(oCombo.Value) || oCombo.Value.ToString() == "0")
            //    err += ", Proveedor";
            //Validando la fecha de la orden
            oEditText = this.oForm.Items.Item("txtFechaOS").Specific;
            if (string.IsNullOrEmpty(oEditText.String.Trim()))
                err += ", Fecha";

            //Se valida que exista un servicio en la pesta�a de "Agregar servicios". R. Santos.
            oGrid = this.oForm.Items.Item("gdOS").Specific;
            if (oGrid.DataTable != null)
            {
                if (oGrid.DataTable.Rows.Count < 1)
                {
                    err += ", Servicio";
                }
            }
            else
            {
                err += ", Servicio";
            }

            oItem = this.oForm.Items.Item("cmbrutina");
            oCombo = (ComboBox)this.oItem.Specific;
            if (!string.IsNullOrEmpty(oCombo.Selected.Value))
            {
                if (oCombo.Selected.Value.Equals("1", StringComparison.InvariantCultureIgnoreCase))
                {
                    oItem = this.oForm.Items.Item("txtPlanti");
                    oEditText = this.oItem.Specific;
                    if (string.IsNullOrEmpty(oEditText.Value))
                        err = "No se ha proporcionado el nombre de la plantilla";
                }
            }


            //oEditText = this.oForm.Items.Item("txtCostoOS").Specific;
            //if (String.IsNullOrEmpty(oEditText.String.Trim()))
            //err += ", Costo (Sin Impuesto)";
            if (!string.IsNullOrEmpty(err))
            {
                if (err.StartsWith(","))
                    err = "Los siguentes campos son requeridos:\n" + err.Substring(1);
            }
            return err;
        }

        public int? GetProveedorFromXML()
        {
            int? ProveedorID = 0;
            oItem = this.oForm.Items.Item("comboProOS");
            oCombo = (SAPbouiCOM.ComboBox)(oItem.Specific);
            ProveedorID = Convert.ToInt32(oCombo.Selected.Value);
            return ProveedorID;
        }

        public void bOCultarProveedor(bool bHide)
        {
            oItem = this.oForm.Items.Item("comboProOS");
            oItem.Enabled = bHide;

            //oItem = this.oForm.Items.Item("tabDataRef");
            //oItem.Enabled = bHide;
        }

        public void EnableTabRedacciones( bool bHide)
        {
            oItem = this.oForm.Items.Item("tabDataRef");
            oItem.Enabled = bHide;
        }

        public void EditableItemCostoServicio(bool visible)
        {
            oItem = this.oForm.Items.Item("txtCostoOS");
            oItem.Enabled = visible;
        }

        public bool EstatusTallerInterno()
        {
            bool Checked = false;
            oItem = oForm.Items.Item("CBTI");
            oCheckBoxTallerInterno = (CheckBox)(oItem.Specific);
            Checked = oCheckBoxTallerInterno.Checked;
            return Checked;
        }

        public void MostrarFacturacion( bool show)
        {
            oItem = this.oForm.Items.Item("oInvoice");
            //oItem.Enabled = show;
            oItem.Visible = false;
        }

        public void MostrarProveedor(bool x)
        {
            oItem = this.oForm.Items.Item("lblProvOS");
            oItem.Visible = x;
            //--
            oCombo = oForm.Items.Item("comboProOS").Specific;
            oCombo.Item.Visible = x;
        }

        public void MostrarMetodoPago(bool x)
        {
            oItem = this.oForm.Items.Item("lblMPOS");
            oItem.Visible = x;
            // --
            oCombo = oForm.Items.Item("comboMPOSv").Specific;
            oCombo.Item.Visible = x;
        }

        public void MostrarDetallePago(bool x)
        {
            oItem = this.oForm.Items.Item("lblDetOS");
            oItem.Visible = x;
            //--
            oEditText = oForm.Items.Item("txtDetOSv").Specific;
            oEditText.Item.Visible = x;
        }

        public void MostrarImpuestos(bool x)
        {
            oItem = this.oForm.Items.Item("lblImpOS");
            oItem.Visible = x;
            //Simbolo
            oItem = this.oForm.Items.Item("Item_22");
            oItem.Visible = x;
            //--
            oStaticText = oForm.Items.Item("lblImpOSv").Specific;
            oStaticText.Item.Visible = x;
        }

        public void HabilitarTallerInterno(bool x)
        {
            oItem = oForm.Items.Item("CBTI");
            oItem.Enabled = x;
            //oCheckBoxTallerInterno = (CheckBox)(oItem.Specific);
            //oCheckBoxTallerInterno.Item.Enabled = x;
        }

        #endregion

        /// <summary>
        /// Obtiene el detalle de la orden a partir de la lista de servicios agregados
        /// </summary>
        /// <returns></returns>
        //public List<DetalleOrden> GetListDetalleOrdenFromListServicios()
        //{
        //    try
        //    {
        //        List<DetalleOrden> detalle = new List<DetalleOrden>();
        //        if(this.ListaServiciosAgregados != null && this.ListaServiciosAgregados.Count > 0)
        //        {
        //            foreach(DetalleOrden sv in this.ListaServiciosAgregados)
        //            {
        //                DetalleOrden dtor = new DetalleOrden();
        //                dtor.Servicio = new Servicio();
        //                if (sv.tipoMantenibleID == 1)
        //                    dtor.Mantenible = new Vehiculo();
        //                else if (sv.tipoMantenibleID == 2)
        //                    dtor.Mantenible = new Caja();
        //                dtor.Mantenible.MantenibleID = sv.vehiculoid;
        //                dtor.Servicio.ServicioID = sv.servicioid;
        //                dtor.TipoMantenibleID = sv.tipoMantenibleID;
        //                dtor.Costo = Convert.ToDecimal(sv.costo);
        //                detalle.Add(dtor);
        //            }
        //        }
        //        return detalle;
        //    }
        //    catch(Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}

        /// <summary>
        /// Valida los datos de la orden de servicio
        /// </summary>
        /// <returns></returns>
        public string ValidarDatos()
        {
            try
            {
                string err = string.Empty;
                //Validando proveedor
                oCombo = this.oForm.Items.Item("comboProOS").Specific;
                if (string.IsNullOrEmpty(oCombo.Value) || oCombo.Value.ToString() == "0")
                    err += ", Proveedor";
                //Validando la fecha de la orden
                oEditText = this.oForm.Items.Item("txtFechaOS").Specific;
                if (string.IsNullOrEmpty(oEditText.String.Trim()))
                    err += ", Fecha";
                //Validando el folio
                oEditText = this.oForm.Items.Item("txtFolioOS").Specific;
                if (string.IsNullOrEmpty(oEditText.String.Trim()))
                    err += ", Folio";
                //Validando el metodo de pago
                oCombo = this.oForm.Items.Item("comboMPOSv").Specific;
                if (string.IsNullOrEmpty(oCombo.Value) || oCombo.Value == "0")
                    err += ", Método de pago";
                //Validando el impuesto
                oEditText = this.oForm.Items.Item("lblImpOSv").Specific;
                if (string.IsNullOrEmpty(oEditText.String.Trim()))
                    err += ", Impuesto";
                //Validando el responsable de la orden de servicio
                oCombo = this.oForm.Items.Item("cmbResp").Specific;
                if (string.IsNullOrEmpty(oCombo.Value) || oCombo.Value == "0")
                    err += ", Responsable de la orden de servicio";
                //Validando el aprobador de la orden de servicio
                oCombo = this.oForm.Items.Item("cmbAprob").Specific;
                if (string.IsNullOrEmpty(oCombo.Value) || oCombo.Value == "0")
                    err += ", Aprobador de la orden de servicio";
                //Validando la lista de servicios agregados
                if (this.ListaServiciosAgregados == null)
                    err += ", Agrege al menos un servicio";
                if (!string.IsNullOrEmpty(err))
                {
                    if (err.StartsWith(","))
                        err = "Los siguentes campos son requeridos:\n" + err.Substring(1);
                }
                return err;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        #region Auxiliares
        /// <summary>
        /// Crea las instancias de los objetos que intervienen en el modulo, preparandolos para realizar una nueva accion
        /// </summary>
        private void PrepareObjects()
        {
            #region Alertas de mantenimiento
            this.AlertaMantenimiento = new Kanan.Mantenimiento.BO2.AlertaMantenimiento();
            this.AlertaMantenimiento.empresa = new Kanan.Operaciones.BO2.Empresa();
            this.AlertaMantenimiento.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
            this.AlertaMantenimiento.servicio = new Kanan.Mantenimiento.BO2.Servicio();
            this.AlertaMantenimiento.servicio.TipoServicio = new Kanan.Mantenimiento.BO2.TipoServicio();
            this.AlertaMantenimiento.servicio.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
            #endregion

            #region Orden de servicio
            //Entity.OrdenServicio = new Kanan.Mantenimiento.BO2.OrdenServicio();
            //OrdenServicio.alertas = new AlertaMantenimiento();
            //OrdenServicio.alertas.vehiculo = new Vehiculo();
            //OrdenServicio.alertas.servicio = new Servicio();
            //OrdenServicio.TipoDePago = new TipoDePago();
            //OrdenServicio.ProveedorServicio = new Kanan.Costos.BO2.Proveedor();
            //OrdenServicio.alertas.empresa = new Empresa();
            //OrdenServicio.SucursalOrden = new Sucursal();
            //OrdenServicio.EncargadoOrdenServicio = new Empleado();
            #endregion
        }
        /// <summary>
        /// Obtiene las configuraciones de usuario
        /// </summary>
        private void GetUserConfigs()
        {
            string configs = this.Config.UserFull.ConfigurationToString();
            var array = configs.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (array.Length > 0)
            {
                //TimeZone=America/Mexico_City
                var timeZone = array[0].Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                int index = timeZone.Length - 1;
                string timeZoneValue = (string)timeZone.GetValue(index);
                //Lang=es-MX
                var lang = array[1].Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                //es-MX
                string language = lang[1].ToString();
                //es
                var shortLang = language.ToString().Split('-')[0];

                Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(language);
                Thread.CurrentThread.CurrentCulture = new CultureInfo(language);

                /// init noda
                this.dateHelper = new NodaTimeHelper.Services.DateTimeHelper(timeZoneValue);
                string[] formatsTime = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortTimePattern.Split(':');
                string[] formatEnd = formatsTime[1].Split(' ');
                string formatCurrent = string.Format("{0} {1}:{2}", Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern, formatsTime[0], formatEnd[0]);
                this.dateHelper.SetShortDate(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern);
                this.dateHelper.SetPattern(formatCurrent);
            }

        }
        /// <summary>
        /// obtiene la informacion de vehiculo y servicio por cada DetalleOrden
        /// </summary>
        /// 
        public void GetDetalleOrdenInfo()
        {
            foreach (var dto in ListaServiciosAgregados)
            {
                #region Vehicullo
                string query = @"select * from ""@VSKF_VEHICULO"" ";
                query += string.Format(" where U_VehiculoID = {0} ", dto.Mantenible.MantenibleID);
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    rs.MoveFirst();
                    string value = Convert.ToString(rs.Fields.Item("U_Nombre").Value);
                    (dto.Mantenible as Vehiculo).Nombre = value;
                }
                #endregion

                #region Servicio
                string query2 = @"select * from ""@VSKF_SERVICIOS"" ";
                query2 += string.Format(" where U_ServicioID = {0} ", dto.Servicio.ServicioID);
                SAPbobsCOM.Recordset rs2 = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs2.DoQuery(query2);
                if (rs2.RecordCount > 0)
                {
                    rs2.MoveFirst();
                    string value = Convert.ToString(rs2.Fields.Item("U_Nombre").Value);
                    dto.Servicio.Nombre = value;
                }
                #endregion
            }
        }

        public void GetOtrosGastosDetalle()
        {
            try
            {
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(this.SqueryCostosAdicionales(Entity.OrdenServicio.OrdenServicioID.ToString()));
                if (rs.RecordCount > 0)
                {
                    ListaCostosAdicionales = new List<SBOKF_CL_CostosAdicional_INFO>();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        SBOKF_CL_CostosAdicional_INFO otemp = new SBOKF_CL_CostosAdicional_INFO();

                        if (rs.Fields.Item("U_Descripcion").Value != null)
                            otemp.U_DESCRIPCION = Convert.ToString(rs.Fields.Item("U_Descripcion").Value);

                        if (rs.Fields.Item("U_Costo").Value != null)
                            otemp.U_COSTO = Convert.ToDouble(rs.Fields.Item("U_Costo").Value);

                        ListaCostosAdicionales.Add(otemp);
                        rs.MoveNext();
                    }
                    this.FillGastosAdicionales();
                }

                DetalleOrdenSAP dorden = new DetalleOrdenSAP(ref this.oCompany);
                rs = dorden.ConsultarByOrdenID(this.Entity.OrdenServicio.OrdenServicioID);

                ListaServiciosAgregados = new List<DetalleOrden>();
                if (rs.RecordCount > 0)
                {

                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string itemcode = rs.Fields.Item("Code").Value;
                        this.EntityDetalle.oUDT.GetByKey(itemcode);
                        this.Entity.UDTToOrdenServicio();
                        var temp = (DetalleOrden)this.EntityDetalle.oDetalleOrden.Clone();
                        ListaServiciosAgregados.Add(temp);
                        rs.MoveNext();
                    }

                }

            }
            catch { }
        }

        public String sQueryPlantilla(string sCode = "")
        {
            String strQuery = "";
            if (GestionGlobales.bSqlConnection)
                strQuery = "SELECT * FROM [@VSKF_OSPLANTILLA]";
            else
                strQuery = @"SELECT * FROM ""@VSKF_OSPLANTILLA"" ";

            if (!string.IsNullOrEmpty(sCode))
            {
                strQuery += string.Format(@" WHERE ""Code"" = '{0}' ", sCode);
            }
            return strQuery;
        }
        public String SqueryCostosAdicionales(string sFolioOS)
        {
            String strQuery = "";
             strQuery = @"SELECT * FROM ""@VSKF_OSCOSTOSADICIO"" ";

            if (!string.IsNullOrEmpty(sFolioOS))
            {
                
                    strQuery += string.Format(@" WHERE U_OrdenServico = '{0}' ", sFolioOS);
            }
            return strQuery;
        }
        public String sQueryPlantillaDetalle(string sCodigoPlantilla, bool bRefacciones=false)
        {
            String strQuery = "";
            
                strQuery = string.Format(@"SELECT * FROM ""@{0}"" ", bRefacciones ? "VSKF_PLANTREFACCION": "VSKF_OSPLANTILLADET" );

            if (!string.IsNullOrEmpty(sCodigoPlantilla))
            {
                 strQuery += string.Format(@" WHERE U_PlantillaPadre = '{0}' ", sCodigoPlantilla);
            }
            return strQuery;
        }
         

        public void ActualizarFolio()
        {
            //this.EFolio.folio.Folio ++;
            if (EFolio.folio.FolioOrdenID != null)
            {
                this.EFolio.folio.Folio++;
                this.EFolio.FolioToUDT();
                
                    this.oCompany.StartTransaction();
                    this.EFolio.Actualizar(@"""@VSKF_FOLIOORDEN""");
                    if (this.oCompany.InTransaction)
                        this.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                
            }
            else
            {
                oStaticText = oForm.Items.Item("txtfolio").Specific;
                this.EFolio.Sincronizado = 1;
                this.EFolio.UUID = Guid.NewGuid();
                this.EFolio.FolioToUDT();

                
                    this.oCompany.StartTransaction();
                    this.EFolio.Insert(@"""@VSKF_FOLIOORDEN""");
                    if (this.oCompany.InTransaction)
                        this.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                
            }
        }
        #endregion
    }
}
