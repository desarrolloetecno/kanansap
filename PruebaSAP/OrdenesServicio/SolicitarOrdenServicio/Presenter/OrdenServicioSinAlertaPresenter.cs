using KananSAP.AdministrarOrdenesServicio.Views;
using KananSAP.Mantenimiento.Data;
using Kanan.Mantenimiento.BO2;
using Kanan.Vehiculos.BO2;
using KananSAP.GestionAlertas.View;
using KananWS.Interface;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KananSAP.Alertas.Data;
using KananSAP.Vehiculos.Data;
using KananSAP.Helpers;
using KananSAP.Configuracion.Configuraciones.Objects;
using KananSAP.Helper;
using SAPbobsCOM;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.IO;

using System.Windows.Forms;
using KananSAP.GestionDatos.MigradorDatos.Librerias;
using SAPinterface.Data.INFO.Tablas;
using SAPinterface.Data.PD;
using SAPinterface.Data.AD;

namespace KananSAP.SolicitarOrdenServicio.Presenter
{
    public class OrdenServicioSinAlertaPresenter
    {
        #region Atributos

        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private EventFilter Filter;
        private Configurations configurations;
        private bool ModalOrden;
        public OrdenServicioSinAlertaView view;
        //public bool IsLogged { get; set; }
        private KananSAP.Mantenimiento.Data.MotorMantenimientoData motorData;
        //private List<OrdenServicio> Ordenes { get; set; }
        private OrdenServicio OrdenServicioReturned { get; set; }
        private KananWS.Interface.OrdenServicioWS ordenWS;
        private int _itemTemp;
        private int ItemTemp { get { return this._itemTemp; } set { this._itemTemp = value; } }
        //OrdenServicioSAP ordenData;

        public AdministraOSView viewlist { get; set; }
        Helper.KananSAPHerramientas oHerramientas;
        string sIDTempAsignado = "";
        Recordset rs;

        //Se crean variables est�ticas de Estatus para validar que no se puedan saltar procesos.
        private int? EstatusActual;
        private int? EstatusSiguiente;

        #endregion Atributos

        #region Constructor
        public OrdenServicioSinAlertaPresenter(SAPbouiCOM.Application sApplication, SAPbobsCOM.Company Cmpny, Configurations c)
        {
            try
            {
                this.SBO_Application = sApplication;
                this.Company = Cmpny;
                this.configurations = c;
                //this.view = new OrdenServicioSinAlertaView(this.SBO_Application, this.Company, this.configurations);
                //this.viewlist = new AdministraOSView(this.SBO_Application, this.Company);
                //SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
                ModalOrden = false;
                motorData = new Mantenimiento.Data.MotorMantenimientoData(this.Company);
                this.ordenWS = new OrdenServicioWS(Guid.Parse(this.configurations.UserFull.Usuario.PublicKey), this.configurations.UserFull.Usuario.PrivateKey);
                //this.ordenData = new OrdenServicioSAP(this.Company);
                this.oHerramientas = new Helper.KananSAPHerramientas(this.SBO_Application);

                rs = (Recordset)Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("OrdenServicioSinAlertaPresenter.cs->OrdenServicioSinAlertaPresenter()", ex.Message);
                throw new Exception(ex.Message);
            }
        }
        #endregion

        private void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            //if (!IsLogged) return;
            string FileName = string.Empty;
            string group = string.Empty;
            try
            {
                #region Orden de servicio

                if (!pVal.BeforeAction)
                {
                    if (pVal.FormTypeEx == "frmAddOS")
                    {
                        #region ChooseFromList
                        ///se agrega evento de seleccion de datos por lista al componente refacciones
                        if (pVal.EventType == BoEventTypes.et_CHOOSE_FROM_LIST)
                        {
                            SAPbouiCOM.IChooseFromListEvent oCFLEvento = null;
                            oCFLEvento = ((SAPbouiCOM.IChooseFromListEvent)(pVal));
                            string sCFL_ID = null;
                            sCFL_ID = oCFLEvento.ChooseFromListUID;
                            SAPbouiCOM.Form oForm = null;
                            oForm = SBO_Application.Forms.Item(FormUID);
                            SAPbouiCOM.ChooseFromList oCFL = null;
                            oCFL = oForm.ChooseFromLists.Item(sCFL_ID);
                            if (oCFLEvento.BeforeAction == false)
                            {
                                SAPbouiCOM.DataTable oDataTable = null;
                                oDataTable = oCFLEvento.SelectedObjects;
                                string val = null;
                                try
                                {
                                    val = System.Convert.ToString(oDataTable.GetValue(0, 0));
                                    string sitem = "";
                                    switch (sCFL_ID)
                                    {
                                        case "CFSrc":
                                            sitem = "EdtCstoAr";
                                            break;
                                        case "CFArt":
                                            sitem = "EdtRSCto";
                                            break;
                                        case "CFLTOOL":
                                            sitem = "EdtODS";
                                            break;
                                        
                                        case "CFSoc":
                                            sitem = "EdtSoci";
                                            break;
                                        default:
                                            sitem = "EditDS";
                                            break;
                                    }
                                    oForm.DataSources.UserDataSources.Item(sitem).ValueEx = val;
                                    this.view.GetChooseList(sCFL_ID, val);
                                }
                                catch { }
                            }
                        }
                        #endregion ChooseFromList

                        if (pVal.EventType == BoEventTypes.et_FORM_DATA_ADD || pVal.EventType == BoEventTypes.et_FORM_DATA_LOAD)
                        {
                            if (pVal.ItemUID == "")
                            { 
                            
                            }
                        
                        }

                        #region ComboMPOSv
                        if (pVal.EventType == BoEventTypes.et_COMBO_SELECT && pVal.ItemUID == "comboMPOSv")
                        {
                            if (this.view.isEfectivo())
                                this.view.HiddeDetallePago();
                            else
                                this.view.ShowDetallePago();
                        }
                        #endregion ComboMPOSc

                        #region gdOS
                        if ((pVal.ItemUID == "gdOS" || pVal.ItemUID == "gdRFa" || pVal.ItemUID == "GvCsto" || pVal.ItemUID == "gvTool" || pVal.ItemUID == "gvMecan") && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            BubbleEvent = false;
                            this.view.SetRowIndex(pVal.Row, pVal.ItemUID);
                        }
                        #endregion gdOS

                        #region ComboProveedo
                        if (pVal.ItemUID == "comboProOS" && pVal.EventType == BoEventTypes.et_COMBO_SELECT)
                        {
                            double? IVA = 0;
                            //Comentado porque cuando se limpia el formulario llama al evento del combo e intenta traer los valores de fechas
                            //por lo que al limpiar algunos campos del XML alguna informaci�n llega como null en el BO.
                            
                            //Se utilizar� un m�todo que obtenga ProveedorID desde la interfaz.
                            //this.view.FormToEntity();
                            int? ProveedorID = this.view.GetProveedorFromXML();

                            if (ProveedorID != 0)
                            {
                                IVA = this.view.Entity.GetIVAByProveedorID(ProveedorID);
                                if (IVA != null || IVA > 0.0)
                                {
                                    this.view.SetIVA(IVA);
                                }
                                else
                                {
                                    this.view.SetIVA(0.0);
                                    SBO_Application.StatusBar.SetText("Impuesto no valido, favor de validar. ", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Warning);
                                }
                            }
                            else
                            {
                                this.view.SetIVA(0.0);
                                SBO_Application.StatusBar.SetText("Impuesto no valido, favor de validar. ", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Warning);
                            }
                        }
                        #endregion ComboProveedo

                        #region ComboRefaccionesKF
                        if (pVal.ItemUID == "cmbRefKF" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.view.SetCostoRefaccionKF();
                        }
                        #endregion ComboRefaccionesKF

                        #region ComboTallerInterno
                        if (pVal.ItemUID == "CBTI" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            if (this.view.EstatusTallerInterno())
                                this.view.EditableItemCostoServicio(false);
                            else
                                this.view.EditableItemCostoServicio(true);
                        }
                        #endregion ComboTallerInterno

                        #region ComboEstatus
                        if (pVal.ItemUID == "cmbestatus" && pVal.EventType == BoEventTypes.et_COMBO_SELECT)
                        {
                            int? flagold = this.view.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID;
                            this.view.FormToEntity();
                            int? flag = this.view.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID;
                            //Estatus Aprobada - EC
                            if (flag == 2 & flagold != 1)
                            {
                                this.view.HabilitarTallerInterno(true);
                                if (!this.view.Entity.OrdenServicio.TallerInterno)
                                {
                                    this.view.MostrarProveedor(true);
                                    this.view.MostrarMetodoPago(true);
                                    this.view.MostrarDetallePago(true);
                                    this.view.MostrarImpuestos(true);
                                    //Comentado, se trabaja en el evento del Check;
                                    //this.view.VisibleItemCostoServicio(true);
                                }
                            }
                            //Estatus Atendida
                            if (flag == 5)
                            {
                                //Eduardo Contreras
                                this.view.HabilitarTallerInterno(false);
                                this.view.bOCultarProveedor(true);
                                //Si es taller interno no muestra factura
                                if (!this.view.Entity.OrdenServicio.TallerInterno)
                                    this.view.MostrarFacturacion(true);
                                if (this.view.Entity.OrdenServicio.TallerInterno)
                                {
                                    this.view.MostrarProveedor(false);
                                    this.view.MostrarMetodoPago(false);
                                    this.view.MostrarDetallePago(false);
                                    this.view.MostrarImpuestos(false);
                                    //Comentado, se trabaja en el evento del Check;
                                    //this.view.VisibleItemCostoServicio(false);
                                }
                            }
                            //Estatus realizada.
                            else if (flag == 6)
                            {
                                //this.view.MostrarFacturaci�n(true);
                            }
                            else
                            {
                                this.view.bOCultarProveedor(false);
                                this.view.MostrarFacturacion(false);
                            }

                            //Combo proveedor - IVA
                            double? IVA = 0;
                            int? ProveedorID = this.view.Entity.OrdenServicio.ProveedorServicio.ProveedorID;

                            if (ProveedorID != 0)
                            {
                                IVA = this.view.Entity.GetIVAByProveedorID(ProveedorID);
                                this.view.SetIVA(IVA);
                            }
                        }
                        #endregion ComboEstatus

                      

                        #region BotonPDF
                        if (pVal.ItemUID == "btnPDF" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            if (this.view.ListaServiciosAgregados.Count > 0)
                                ExportarPDF(out FileName, true);
                            else
                            {
                                BubbleEvent = false;
                                oHerramientas.ColocarMensaje("Proporciona información antes de generar el PDF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                            }
                         
                        }
                        #endregion BotonPDF

                        #region determina gestion plantilla
                        if (pVal.ItemUID == "cmbSyscr")
                            this.view.FillComboComponenteSistema();


                        if (pVal.ItemUID == "cmbrutina")
                            //cargar datos de plantillas
                            this.view.FormLoadPlantillaMood();

                       

                        if (pVal.ItemUID == "cmbPla")
                            this.view.PlantillaToForm();

                        if (pVal.ItemUID == "cmbATipo")
                            this.view.DefineChooseCosto();

                        if (pVal.ItemUID == "cmbmeca")
                            this.view.cargaSalarioEmpleado();
                        
                        #endregion determina gestion plantilla

                        #region Guardar

                        if (pVal.ItemUID == "btnSaveOS" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            BubbleEvent = false;
                            //Se obtiene el status al momento de lanzar el formulario
                            this.EstatusActual = this.view.estatus;
                            this.view.FormToEntity();
                            this.EstatusSiguiente = this.view.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID;
                            int? flag = this.view.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID;
                            bool bContinuar = true;
                            #region ValidarOrdenEstatus
                            //Verificar seguimiento de estatus.
                            if (EstatusActual != null & EstatusSiguiente != null)
                            {
                                bContinuar = VerificarSiguienteEstatus();

                                //Lanza excepcion por mal definir el estatus.
                                if (bContinuar == false)
                                {
                                    throw new Exception("Orden de estatus incorrecta. ");
                                }
                                else
                                {
                                    DateTime FechaActual = DateTime.Now;
                                    this.view.Entity.InsertCambioEstatus(EstatusSiguiente, FechaActual, this.view.Entity.OrdenServicio.OrdenServicioID);
                                }
                            }
                            else
                            {
                                bContinuar = false;
                                throw new Exception("Error interno. Error al intentar obtener estatus. ");
                            }
                            #endregion ValidarOrdenEstatus

                            #region RecharzarOrdenServicio
                            //Cuando una orden rechazada
                            if (flag == 3)
                            {
                                this.view.Entity.EstatusRechazada(Convert.ToInt32(this.view.Entity.OrdenServicio.OrdenServicioID));
                                bContinuar = false;
                                this.view.CloseForm();

                                //Se limpia y se cargan nuevamente todas las ordenes para visualizar el nuevo estatus
                                this.viewlist.listordenes.Remove(
                                        this.viewlist.listordenes.FirstOrDefault(
                                            x => x.OrdenServicioID == this.view.Entity.OrdenServicio.OrdenServicioID));
                                this.viewlist.listordenes.Add((OrdenServicio)this.view.Entity.OrdenServicio.Clone());
                                this.viewlist.FillOrdenesGridFromList();

                                return;
                            }
                            #endregion RecharzarOrdenServicio

                            #region EliminarOrdenServicio
                            //Cuando una orden es eliminada
                            if (flag == 4)
                            {
                                this.view.Entity.EstatusEliminar(Convert.ToInt32(this.view.Entity.OrdenServicio.OrdenServicioID));
                                bContinuar = false;
                                this.view.CloseForm();

                                //Se limpia y se cargan nuevamente todas las ordenes para visualizar el nuevo estatus
                                this.viewlist.listordenes.Remove(
                                        this.viewlist.listordenes.FirstOrDefault(
                                            x => x.OrdenServicioID == this.view.Entity.OrdenServicio.OrdenServicioID));
                                this.viewlist.listordenes.Add((OrdenServicio)this.view.Entity.OrdenServicio.Clone());
                                this.viewlist.FillOrdenesGridFromList();

                                return;
                            }
                            #endregion EliminarOrdenServicio

                            #region Facturar Estatus 5
                            if (flag == 5)
                                /*Se asigna la fecha de entrada real al taller*/
                                this.view.Entity.OrdenServicio.FechaRecepcionReal = DateTime.Today;
                            #endregion Facturar Estatus 5

                            #region Salida inventario Estatus 6
                            //Salida de Inventario - Eduardo Contreras
                            if (flag == 6)
                            {
                                /*Se asigna la fecha de salida real del taller*/
                                this.view.Entity.OrdenServicio.FechaLiberacionReal = DateTime.Today;
                                /*if (flag == 6)
                                {
                                    if (bContinuar & this.view.Refacciones != null)
                                        if (bContinuar & this.view.Refacciones.Count > 0)
                                            SalidaInventarioRefacciones(this.view.Entity.OrdenServicio, this.view.Refacciones);
                                }*/
                            }
                            #endregion Salida inventario Estatus 6

                            #region ActualizarOrdenServicio
                            if (bContinuar)
                            {

                                if (this.view.Entity.OrdenServicio.OrdenServicioID != null || this.view.Entity.OrdenServicio.OrdenServicioID <= 0)
                                {
                                    this.ActualizarOrdenes();
                                    this.view.CloseForm();
                                    this.viewlist.listordenes.Remove(
                                        this.viewlist.listordenes.FirstOrDefault(
                                            x => x.OrdenServicioID == this.view.Entity.OrdenServicio.OrdenServicioID));
                                    this.viewlist.listordenes.Add((OrdenServicio)this.view.Entity.OrdenServicio.Clone());
                                    this.viewlist.FillOrdenesGridFromList();
                                    //this.viewlist.AplyFilter();
                                }
                                else
                                {
                                    if (this.ValidaOrdenServicio())
                                    {
                                        if (this.InsertarOrdenes())
                                        {                                            
                                            if (flag == 1)
                                                //condicion que determina que es una solicitud de orden de servicio
                                                this.EnviarNotificacion(this.view.Entity.OrdenServicio.AprobadorOrdenServicio.EmpleadoID);
                                        }
                                        this.view.EmptyEntityToForm(); ///linea que limpia formulario de orden de servicio
                                        this.view.ConsumeSiguienteFolioWS();
                                    }
                                }

                                #region Actualizando par�metros de servicios
                                //M�todo para validar actualizar par�metros
                                if (this.view.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID == 5)
                                {
                                    #region InstanciandoObjetos
                                    int? VHID;
                                    int? SVID;
                                    int? TVHID;
                                    double? valor;
                                    double? alerta;
                                    int? valorTiempo;
                                    int? alertaTiempo;
                                    decimal? Odometro;
                                    string Sentencia = "";
                                    VehiculoWS vhWS = new VehiculoWS(Guid.Parse(this.configurations.UserFull.Usuario.PublicKey), this.configurations.UserFull.Usuario.PrivateKey);
                                    AlertaMantenimiento alMto = new AlertaMantenimiento();
                                    List<AlertaMantenimiento> ListalMto = new List<AlertaMantenimiento>();
                                    AlertaMantenimientoSAP alCtl = new AlertaMantenimientoSAP(this.Company);

                                    DetalleOrden detalleOrden = new DetalleOrden();
                                    List<DetalleOrden> ListDO = new List<DetalleOrden>();

                                    ParametroServicio PS = new ParametroServicio() { Servicio = new Servicio() };
                                    List<ParametroServicio> ListPS = new List<ParametroServicio>() { };
                                    ParametroServicioData PSCtl = new ParametroServicioData(this.Company);

                                    ParametroMantenimiento PM = new ParametroMantenimiento();
                                    List<ParametroMantenimiento> ListPM = new List<ParametroMantenimiento>();
                                    ParametroMantenimientoData PMCtl = new ParametroMantenimientoData(this.Company);
                                    #endregion

                                    //ListDO = this.view.EntityDetalle.RecordSetToListDetalleOrden(rs, detalleOrden);
                                    for (int i = 0; i < this.view.Entity.OrdenServicio.DetalleOrden.Count; i++)
                                    {
                                        #region Obteniendo identificadores
                                        VHID = this.view.Entity.OrdenServicio.DetalleOrden[i].Mantenible.MantenibleID != null ? VHID = this.view.Entity.OrdenServicio.DetalleOrden[i].Mantenible.MantenibleID : VHID = null;
                                        TVHID = this.view.Entity.OrdenServicio.DetalleOrden[i].TipoMantenibleID != null ? TVHID = this.view.Entity.OrdenServicio.DetalleOrden[i].TipoMantenibleID : TVHID = null;
                                        SVID = this.view.Entity.OrdenServicio.DetalleOrden[i].Servicio.ServicioID != null ? SVID = this.view.Entity.OrdenServicio.DetalleOrden[i].Servicio.ServicioID : SVID = null;
                                        string OdoTemp = vhWS.GetOdometro(Convert.ToInt32(VHID));
                                        Odometro = Convert.ToDecimal(OdoTemp);
                                        #endregion

                                        #region DesactivarAlertas
                                        //Se eliminan las alertas en caso de que la alerta conincida con el servicio, el vehiculo y el tipo de veh�culo que
                                        //se registr� en la orden de servicio.
                                        Sentencia = string.Format(@"select * from ""@VSKF_ALERTAMTTO"" where U_MantenibleID = '{0}' and U_TipoMantenibleID = '{1}' and U_ServicioID = '{2}'", VHID, TVHID, SVID);
                                        rs.DoQuery(Sentencia);
                                        alCtl.AlertaMantenimiento = new AlertaMantenimiento() { Mantenible = new Vehiculo() };
                                        ListalMto = alCtl.HashoAlertaMto(rs) ? ListalMto = alCtl.RecordSetToListAlertaMantenimiento(rs) : ListalMto = new List<AlertaMantenimiento>();

                                        //Se verigica que si existe una alerta para este veh�culo con ese servicio y se elimina
                                        //  en caso de que exista por ser aprobada la solicitud de orden de servicio.
                                        for (int h = 0; h < ListalMto.Count; h++)
                                        {
                                            if (ListalMto[h].Mantenible.MantenibleID == VHID && ListalMto[h].servicio.ServicioID == SVID)
                                            {
                                                string SentenciaEliminarAlerta;
                                                //Se desactiva registro padre.
                                                SentenciaEliminarAlerta = string.Format(@"update ""@VSKF_ALERTA"" set U_EstaActivo = '{0}' where U_IAlertaID = '{1}'", 0, ListalMto[h].AlertaMantenimientoID);
                                                rs.DoQuery(SentenciaEliminarAlerta);
                                                //Se desactiva registro hijo.
                                                /*SentenciaEliminarAlerta = string.Format("update from [@VSKF_ALERTAMTTO] where U_IAlertaID = '{0}'", ListalMto[h].AlertaMantenimientoID);
                                                rs.DoQuery(SentenciaEliminarAlerta);*/
                                            }
                                        }
                                        #endregion

                                        #region Actualizar par�metros
                                        Sentencia = null;
                                        Sentencia = string.Format(@"select * from ""@VSKF_PMTROSERVICIO"" where U_MantbleID = '{0}' and U_ServicioID = '{1}' and U_TMantbleID = '{2}'", VHID, SVID, TVHID);
                                        rs.DoQuery(Sentencia);
                                        ListPS = PSCtl.HashParametroServicio(rs) ? ListPS = PSCtl.RecordSetToListOS(rs) : ListPS = new List<ParametroServicio>();

                                        Sentencia = null;
                                        Sentencia = string.Format(@"select * from ""@VSKF_PMTROMTTO"" where U_MantbleID = '{0}' and U_ServicioID = '{1}' and U_TMantbleID = '{2}'", VHID, SVID, TVHID);
                                        rs.DoQuery(Sentencia);
                                        ListPM = PMCtl.HasParametroMantenimiento(rs) ? ListPM = PMCtl.RecordSetToListOS(rs) : ListPM = new List<ParametroMantenimiento>();

                                        for (int n = 0; n < ListPM.Count; n++)
                                        {
                                            string Sentenciainsertar = null;
                                            DateTime? UltServiTiemp = this.view.Entity.OrdenServicio.Fecha;
                                            decimal? proxserviKM = Odometro + Convert.ToDecimal(ListPS[n].Valor);
                                            TimeSpan? intervaloTiempo = ListPM[n].ProximoServicioTiempo.Value - ListPM[n].UltimoServicioTiempo.Value;
                                            int IntervaloDias = intervaloTiempo.Value.Days;
                                            DateTime? proxservTiemp = DateTime.Today.AddDays(IntervaloDias);

                                            //Actualizando par�metros por distancia
                                            //Se verifica que exista definici�n de par�metros por Kilometraje.
                                            if (ListPS[n].Valor > 0)
                                            {
                                                Sentenciainsertar = null;
                                                Sentenciainsertar = string.Format(@"update ""@VSKF_PMTROMTTO"" set U_UltServ = '{0}', U_ProxServ = '{1}' where U_PMtoID = '{2}'", Odometro, proxserviKM, ListPM[n].ParametroMantenimientoID);
                                                rs.DoQuery(Sentenciainsertar);
                                            }

                                            //Actualizando par�metros tiempo
                                            //Se verifica que exista definici�n de par�metros por tiempo.
                                            if (IntervaloDias > 0)
                                            {
                                                Sentenciainsertar = null;
                                                Sentenciainsertar = string.Format(@"update ""@VSKF_PMTROMTTO"" set U_UlSvTiem = '{0}', U_ProxSvTiem = '{1}' where U_PMtoID = '{2}'", DateTime.Today.ToString("yyyy-MM-dd HH:mm:ss"), proxservTiemp.Value.ToString("yyyy-MM-dd HH:mm:ss"), ListPM[n].ParametroMantenimientoID);
                                                rs.DoQuery(Sentenciainsertar);
                                            }

                                            //Se pregunta si existe alg�n veh�culo relacionado con el servicio registrado en la roden de servicio.
                                            // Sentencia = string.Format("update from [@VSKF_PMTROSERVICIO] where ");
                                            //rs.DoQuery(Sentencia);
                                            //ListPS = PSCtl.HashParametroServicio(rs) ? ListPS = PSCtl.RecordSetToList(rs) : ListPS = new List<ParametroServicio>();
                                        }
                                        #endregion

                                        #region Comentado
                                        //Se actualizan los par�metros de servicio con ese veh�culo especifico.
                                        /*Sentencia = string.Format("select * from [@VSKF_PMTROMTTO] where U_MantbleID = '{0}' and U_ServicioID = '{1}' and U_TMantbleID = '{2}'", VHID, SVID, TVHID);
                                        rs.DoQuery(Sentencia);
                                        ListPM = PMCtl.HasParametroMantenimiento(rs) ? ListPM = PMCtl.RecordSetToList(rs) : ListPM = new List<ParametroMantenimiento>();

                                        for (int k = 0; k < ListPS.Count; k++ )
                                        {
                                            //Si es un vehiculo se actualizan los par�metros.
                                            if (VHID != null && TVHID == 1)
                                            {
                                                //int
                                            }
                                            Odometro = vhWS.GetOdometro(Convert.ToInt32(VHID));
                                        }*/
                                        #endregion

                                    }
                                }
                                #endregion
                            }
                            else throw new Exception("Favor de agreagar refacciones a la orden de servicio");
                            #endregion ActualizarOrdenServicio
                        }
                        #endregion Guardar

                        #region BotonEnviarNotificaci�n
                        if (pVal.ItemUID == "btnNotif" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            BubbleEvent = false;
                            //Se obtiene el status al momento de lanzar el formulario
                            int? estatusActual = this.view.estatus;
                            this.view.FormToEntity();
                            int? estatusSiguiente = this.view.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID;
                            int? flag = this.view.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID;

                            if (estatusActual != null & estatusSiguiente != null)
                            {
                                if (estatusActual == 1 && estatusSiguiente == 1)
                                {
                                    this.ReenviarNotificacion(this.view.Entity.OrdenServicio.AprobadorOrdenServicio.EmpleadoID);
                                }
                            }
                        }
                        #endregion BotonEnviarNotificaci�n

                        #region BotonCancelar
                        if (pVal.ItemUID == "btnCanOS" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            BubbleEvent = false;
                            this.SBO_Application.Forms.Item("frmAddOS").Close();
                            this.viewlist.OSRowIndex = null;
                        }
                        #endregion BotonCancelar

                        #region DetalleRefacciones
                        if (pVal.ItemUID == "btnRefn" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            String CadenaRef = string.Empty;
                            BubbleEvent = this.view.FormValidaRefaccion(out CadenaRef);
                            if (BubbleEvent)
                            {
                                this.view.FormToListRefaccion();
                                BubbleEvent = this.view.ListToMatrix();
                                this.view.SetCostoTotal();

                                if (BubbleEvent)
                                    this.view.LimpiarObjctRefacc();
                            }

                            if (!string.IsNullOrEmpty(CadenaRef))
                                oHerramientas.ColocarMensaje(CadenaRef, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);

                        }
                        #endregion

                        if (pVal.ItemUID == "btnAddCto" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                            this.view.FormtoEntityToCostoList();

                        if (pVal.ItemUID == "btnaddtool" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                            this.view.FormToEntityHerramientas();

                        if (pVal.ItemUID == "btnaddmec" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                            this.view.FormToEntityMecanico();

                        #region DetalleOrden
                        if (pVal.ItemUID == "btnOSvDone" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            if (this.view.ExistDetalleOrden())
                            {
                                this.view.FormToDetalleOrden();
                                int index = view.ListaServiciosAgregados.FindIndex(
                                        x => x.DetalleOrdenID == view.EntityDetalle.oDetalleOrden.DetalleOrdenID);
                                view.ListaServiciosAgregados[index] = (DetalleOrden)view.EntityDetalle.oDetalleOrden.Clone();
                                //view.ListaServiciosEditados.Add((DetalleOrden)view.EntityDetalle.oDetalleOrden.Clone());
                                view.FillGridServiciosFromList();
                                this.view.SetCostoTotal();

                                //No entra debido a que el boton de indicacion es incorrecto.
                                /*view.FillGridRefaccionesFromList();
                                view.SetCostoTotal();*/
                            }
                            else
                            {
                                this.view.NewDetalleOrdenToGrid();
                            }
                            this.view.EmptyServicioToForm();
                            BubbleEvent = false;
                        }
                        if (pVal.ItemUID == "btnDelRf" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            if (!pVal.BeforeAction)
                            {
                                if (view.Refacciones.Count > 0)
                                {
                                    BubbleEvent = view.EliminarLineaRefaccion();
                                    if (!BubbleEvent)
                                        throw new Exception("Refacción no eliminada asegurte de haber seleccionado el registro");
                                }
                                else throw new Exception("Agrega refacciones para poder eliminar");
                            }
                        }
                        if (pVal.ItemUID == "btnDelOSv" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.view.GetDetalleOrdenFromGrid();
                            if (this.view.RowIndex == null) throw new Exception("Indice de objeto no establecido");
                            int index = (int)this.view.RowIndex;
                            if (view.ListaServiciosAgregados != null)
                            {
                                view.ListaServiciosAgregados.RemoveAt(index);
                                view.FillGridServiciosFromList();
                                //view.ListaServiciosEliminados.Add((DetalleOrden)view.EntityDetalle.oDetalleOrden.Clone());
                            }
                            BubbleEvent = false;
                        }

                        if (pVal.ItemUID == "btnDelcto" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            //this.view.GetDetalleOrdenFromGrid();
                            if (this.view.RowIndex == null) throw new Exception("Indice de objeto no establecido para costos");
                            int index = (int)this.view.RowIndex;
                            if (view.ListaCostosAdicionales != null )
                            {
                                view.ListaCostosAdicionales.RemoveAt(index);
                                view.FillGastosAdicionales();   
                            }
                            BubbleEvent = false;
                        }

                        if (pVal.ItemUID == "btndeltool" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            if (this.view.RowIndex == null) throw new Exception("Indice de objeto no establecido para herramientas");
                            int index = (int)this.view.RowIndex;
                            if (view.ListaHerramientas != null)
                            {
                                view.ListaHerramientas.RemoveAt(index);
                                view.FillHerramientas();
                            }
                            BubbleEvent = false;
                        }

                        if (pVal.ItemUID == "btndelmeca" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            if (this.view.RowIndex == null) throw new Exception("Indice de objeto no establecido para el mecanico");
                            int index = (int)this.view.RowIndex;
                            if (view.ListaMecanicos != null)
                            {
                                view.ListaMecanicos.RemoveAt(index);
                                view.FillMecanicos();
                            }
                            BubbleEvent = false;
                        }

                        if (pVal.ItemUID == "btnEdtOSv" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.view.GetDetalleOrdenFromGrid();
                            this.view.DetalleOrdenToForm();
                            BubbleEvent = false;
                        }
                        #endregion
                    }
                }
                #endregion

                #region ListadoOrdenes
                if (pVal.FormTypeEx == "AdminOServ")
                {
                    #region Comentado
                    //if (Modal)
                    //{
                    //    //userview.ShowForm();
                    //    BubbleEvent = false;
                    //    return;
                    //}
                    #endregion Comentado

                    #region beforeaction

                    if (pVal.BeforeAction)
                    {
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "grdorden")
                        {
                            //Se establece el ID de la orden para cunsultar datos.
                            this.viewlist.SetOSRowIndex(pVal.Row);
                            //Se guarda ID de respaldo para evitar recargado de Orden.
                            this.viewlist.IDOSActual = pVal.Row;
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnfiltro")
                        {
                            this.viewlist.AplyFilter();
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnorden")
                        {
                            int? OrdenServicioID = this.viewlist.Entity.OrdenServicio.OrdenServicioID;
                            this.viewlist.GetOrdenFromGrid();
                            this.view.Entity.oUDT.GetByKey(this.viewlist.Entity.OrdenServicio.OrdenServicioID.ToString());                            
                            this.view.Entity.UDTToOrdenServicio();
                            this.viewlist.Entity.OrdenServicio.OrdenServicioID = OrdenServicioID;
                            this.view.Entity.GetDetalleOrden(Convert.ToInt32(this.viewlist.Entity.OrdenServicio.OrdenServicioID));
                            this.view.FolioOrdenSercio = this.viewlist.Entity.OrdenServicio.OrdenServicioID.ToString();
                            switch (viewlist.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID)
                            {
                                case 0:
                                case 1://Solicitada
                                    this.view.ShowForm(1, true);//Prepara form para orden solicitada
                                    //Carga los datos antes de mostrar el formulario
                                    BubbleEvent = false;
                                    this.LoadData();
                                    break;
                                case 2://Aprobada
                                    this.view.ShowForm(2, true);//Prepara form para orden aprobada
                                    //Carga los datos antes de mostrar el formulario
                                    BubbleEvent = false;
                                    this.LoadData();
                                    break;
                                case 3://Eliminada
                                    this.view.ShowForm(3, true);//Prepara form para visualizar sin modificar
                                    //Carga los datos antes de mostrar el formulario
                                    BubbleEvent = false;
                                    this.LoadData();
                                    break;
                                case 4://Eliminada
                                    this.view.ShowForm(4, false);//Muestra un mensaje de registro eliminado
                                    //Carga los datos antes de mostrar el formulario
                                    BubbleEvent = false;
                                    this.LoadData();
                                    break;
                                case 5://Atendida
                                    this.view.ShowForm(5, true);//Prepara form para orden atendida
                                    //Carga los datos antes de mostrar el formulario
                                    BubbleEvent = false;
                                    this.LoadData();
                                    break;
                                case 6: //Realizada
                                    this.view.ShowForm(6, true);//Prepara form para visualizar sin modificar
                                    //Carga los datos antes de mostrar el formulario
                                    BubbleEvent = false;
                                    this.LoadData();
                                    break;
                            }

                            //Se realiza en el LoadData();
                            /*this.view.EntityToForm();
                            this.view.GetDetalleOrdenInfo();
                            this.view.FillGridServiciosFromList();
                            this.view.FillGridRefaccionesFromList();
                            this.view.SetCostoTotal();*/
                            //this.view.AsignarFocoEntity();
                        }
                    }
                    #endregion

                    #region After Action
                    #endregion
                }
                #endregion
            }
            catch (Exception ex)
            {
                BubbleEvent = false;
                KananSAP.Helper.KananSAPHerramientas.LogError("OrdenServicioSinAlertaPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
                oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            }
        }

        #region LoadData
        public void LoadData()
        {
            try
            {
                if (!this.view.ExistDetalleOrden())
                {
                    /*Llenando Servicios...*/
                    if (viewlist.OSRowIndex > -1)
                    {
                        this.viewlist.GetOrdenFromGrid();
                        this.view.Entity.oUDT.GetByKey(this.viewlist.Entity.OrdenServicio.OrdenServicioID.ToString());
                        this.view.Entity.UDTToOrdenServicio();                       
                        this.view.EntityToForm();
                        this.view.GetDetalleOrdenInfo();
                        this.view.GetOtrosGastosDetalle();
                        /*Env�a el coso total de los servicios a la variable
                         est�tica*/
                        this.view.ListaServiciosAgregados = this.view.Entity.GetDetalleOrden(Convert.ToInt32(this.viewlist.Entity.OrdenServicio.OrdenServicioID));
                        this.view.cargaGridEditList();
                        this.view.FillGridServiciosFromList();
                    }
                    /*Llenando refacci�nes...*/
                    if (viewlist.OSRowIndex > -1 && (this.view.Refacciones == null || this.view.Refacciones.Count == 0))
                    {
                        this.view.GestionInventario();
                        this.view.EntityRefaccion = new DetalleRefaccionesSAP(this.Company);
                        this.view.EntityRefaccion.oDetalleRefaccion = new DetalleRefaccion();

                        this.view.EntityRefaccion.oDetalleRefaccion.OrdenServicio = new OrdenServicio();
                        this.view.EntityRefaccion.oDetalleRefaccion.OrdenServicio.OrdenServicioID = this.viewlist.Entity.OrdenServicio.OrdenServicioID;
                        this.view.Refacciones = this.view.EntityRefaccion.RecuperaLista();
                        /*Env�a el coso total de las refacci�nes a la variable
                         est�tica*/
                        //this.view.ListToMatrix(); --- Equivalente en FillGridRefaccionesFromList();
                        this.view.FillGridRefaccionesFromList();
                    }
                }
                this.view.SetCostoTotal();
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("OrdenServicioSinAlertaPresenter.cs->LoadData()", ex.Message);
                throw new Exception("Error al migrar informacion del Grid Administrar ordenes a laventana Ordenes Servicio" + ex.Message);
            }
        }
        #endregion LoadData

        #region Metodos SAP

        private string SalidaInventarioRefacciones(OrdenServicio OSerivicio, List<DetalleRefaccion> refacciones)
        {
            string sresponse = string.Empty, consultaSql = string.Empty;
            SBO_KF_Configuracion_INFO KnSttings = new SBO_KF_Configuracion_INFO();
            SBO_KF_Configuracion_AD oControl = new SBO_KF_Configuracion_AD();
            sresponse = oControl.recuperaConfiguracion(ref Company, out consultaSql, ref KnSttings);
            if (sresponse == "OK")
            {
                sresponse = "ERROR";
                if (KnSttings.cSBO == 'Y' && KnSttings.cAlmacen == 'Y')
                {
                    Documents GIssue = (SAPbobsCOM.Documents)this.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenExit);
                    string fecha = Convert.ToDateTime(OSerivicio.Fecha.ToString()).ToString("dd/MM/yyyy");
                    string FechaCaptura = Convert.ToDateTime(OSerivicio.FechaCaptura.ToString()).ToString("dd/MM/yyyy");
                    GIssue.TaxDate = Convert.ToDateTime(FechaCaptura);
                    GIssue.DocDate = Convert.ToDateTime(FechaCaptura);
                    GIssue.Reference2 = string.Format(@"{0}/{1}", OSerivicio.FolioOrden.Folio, OSerivicio.FolioOrden.AnioFolio);
                    GIssue.Comments = string.Format(@"Salida de mercancia realizada desde Addon Kanan Fleet en fecha {0} ", DateTime.Now.ToString("dd/MM/yyyy"));

                    foreach (DetalleRefaccion data in refacciones)
                    {
                        GIssue.Lines.ItemCode = data.Articulo;
                        GIssue.Lines.Quantity = Convert.ToDouble(data.Cantidad);
                        string CentroCosto = data.CodigoAlmacen;
                        switch (KnSttings.DimCode)
                        {
                            case 0:
                            case 1:
                                GIssue.Lines.CostingCode = CentroCosto;
                                break;
                            case 2:
                                GIssue.Lines.CostingCode2 = CentroCosto;
                                break;

                            case 3:
                                GIssue.Lines.CostingCode3 = CentroCosto;
                                break;

                            case 4:
                                GIssue.Lines.CostingCode4 = CentroCosto;
                                break;
                            case 5:
                                GIssue.Lines.CostingCode5 = CentroCosto;
                                break;

                        }
                        GIssue.Lines.Add();
                    }
                    int iSave = GIssue.Add();
                    if (iSave != 0)
                    {
                        int iCode = 0;
                        this.Company.GetLastError(out iCode, out sresponse);
                        sresponse = string.Format("Salida de mercancía no creada. ERROR {0}", sresponse);
                    }
                    else
                    {
                        sresponse = "OK";
                        this.SBO_Application.StatusBar.SetText("Salida de mercancia registrada, folio de referencia " + OSerivicio.FolioOrden.Folio.ToString(), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    }

                }
                else sresponse = "OK";
            }

            return sresponse;
        }

        private void InsertSAP()
        {
            string sresponse = string.Empty;
            try
            {
                this.Company.StartTransaction();
                string sNombrePlantilla = string.Empty;
                long iSiguienteFolio = -1;
                //OrdenServicioData ordenData = new OrdenServicioData(this.Company);
                //this.view.EntityRefaccion = new DetalleRefaccionesSAP(this.Company);
                this.view.FormToEntity();
                this.ItemTemp = KananSAP.Helper.KananSAPHelpers.GetCodeTemporal();
                this.view.Entity.ItemCode = this.ItemTemp.ToString();
                this.view.Entity.Sincronizado = 1;
                this.view.Entity.UUID = Guid.NewGuid();
                iSiguienteFolio = new Add(ref Company, null).recuperIDMaximoTabla("VSKF_ORDENSERVICIO");
                this.view.Entity.OrdenServicio.OrdenServicioID = Convert.ToInt32(iSiguienteFolio);
                sresponse = this.view.Entity.InsertUpdaterdenServicio();
                this.InsertaBitacora(iSiguienteFolio.ToString(), true);
               if (sresponse != "OK")
                   throw new Exception("No se ha podido registrar la información de la Orden de Servicio");
                //this.Company.StartTransaction();                
                if (this.view.DeterminaSavePlantilla(out sNombrePlantilla))
                {
                    //this.Company.StartTransaction();
                    iSiguienteFolio = new Add(ref Company, null).recuperIDMaximoTabla("VSKF_OSPLANTILLA");
                    bool bInsertado = this.view.Entity.InsertaPlantilla(iSiguienteFolio.ToString(), sNombrePlantilla);
                    if (!bInsertado)
                        throw new Exception("No se ha podido insertar la plantilla para la OS");
                    long iNextdetFol = new Add(ref Company, null).recuperIDMaximoTabla("VSKF_OSPLANTILLADET");
                    foreach (DetalleOrden DetOs in this.view.ListaServiciosAgregados)
                    {
                        DetalleOrden oTemp = DetOs;
                       bInsertado =  this.view.EntityDetalle.InsertaPlantillaDetalle(ref oTemp, iNextdetFol.ToString(), iSiguienteFolio.ToString());
                       if (!bInsertado)
                           throw new Exception("No se ha podido insertar la plantilla para la OS");
                        iNextdetFol++;
                    }

                    iNextdetFol = new Add(ref Company, null).recuperIDMaximoTabla("VSKF_PLANTREFACCION");
                    foreach (DetalleRefaccion oRefaccion in this.view.Refacciones)
                    {
                        DetalleRefaccion oTemp = oRefaccion;
                        bInsertado = this.view.EntityDetalle.InsertaPlantillaRefacciones(ref oTemp, iNextdetFol.ToString(), iSiguienteFolio.ToString());
                        if (!bInsertado)
                            throw new Exception("No se ha podido insertar la plantilla para la OS");
                        iNextdetFol++;
                    }
                    //this.Company.EndTransaction(BoWfTransOpt.wf_Commit);
                }
                //this.view.Entity.Insert();
                /*if (this.Company.InTransaction)
                    this.Company.EndTransaction(BoWfTransOpt.wf_Commit);*/
                //this.view.Entity.InsertDetalleOrden();
                //InsertaDetalleOrden
                //this.view.ActualizarFolio();

                if (this.view.ListaServiciosAgregados != null)
                {
                    //DetalleOrdenSAP dorden = new DetalleOrdenSAP(ref this.oCompany);
                    iSiguienteFolio = new Add(ref Company, null).recuperIDMaximoTabla("VSKF_DETALLEORDEN");
                    foreach (DetalleOrden sc in this.view.ListaServiciosAgregados)
                    {
                        sc.OrdenServicio = new OrdenServicio();
                        sc.OrdenServicio.OrdenServicioID = this.view.Entity.OrdenServicio.OrdenServicioID;
                        sresponse = this.view.EntityDetalle.InsertaDetalleOrden(sc, iSiguienteFolio.ToString());
                        if (sresponse != "OK")
                            throw new Exception("No se ha podido agregar el detalle de la Orden de Servicio");
                        iSiguienteFolio++;
                    }
                }

                if (this.view.ListaCostosAdicionales != null)
                {
                    iSiguienteFolio = new Add(ref Company, null).recuperIDMaximoTabla("VSKF_OSCOSTOSADICIO");
                    foreach (SBOKF_CL_CostosAdicional_INFO oCostoa in this.view.ListaCostosAdicionales)
                    {
                        oCostoa.Code = this.view.Entity.OrdenServicio.OrdenServicioID.ToString();
                        sresponse = this.view.EntityDetalle.InsertaCostoAdicional(oCostoa, iSiguienteFolio.ToString());
                        if (sresponse != "OK")
                            throw new Exception("No se ha podido agregar el detalle de la Orden de Servicio");
                        iSiguienteFolio++;
                    }
                }

                /*if (this.view.OrdenServicio.DetalleOrden != null && this.OrdenServicio.DetalleOrden.Count > 0)
                {
                    DetalleOrdenSAP dorden = new DetalleOrdenSAP(ref this.oCompany);
                    ItemsDetalle = new List<string>();
                    string sItemCodeAsignado = "";
                    foreach (DetalleOrden d in this.OrdenServicio.DetalleOrden)
                    {
                        d.OrdenServicio.OrdenServicioID = Convert.ToInt32(this.ItemCode);
                        dorden.oDetalleOrden = d;
                        dorden.ItemCode = DateTime.Now.Ticks.ToString();
                        dorden.Sincronizado = 1;
                        dorden.UUID = Guid.NewGuid();
                        sItemCodeAsignado = dorden.DetalleOrdenToUDT();
                        this.oCompany.StartTransaction();
                        dorden.Insert();
                        if (this.oCompany.InTransaction)
                            this.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                        //ItemsDetalle.Add(dorden.ItemCode);
                        ItemsDetalle.Add(sItemCodeAsignado);
                    }
                }*/


                ///------------
                if (this.view.Refacciones != null)
                {
                    iSiguienteFolio = new Add(ref Company, null).recuperIDMaximoTabla("VSKF_DTLLEREFACCION");
                    this.view.EntityRefaccion.LimipiarTabla();
                    List<DetalleRefaccion> lstRefacciones = this.view.Refacciones;
                    foreach (DetalleRefaccion oDetalleSinc in lstRefacciones)
                    {
                        DetalleRefaccion oTemp = oDetalleSinc;                        
                        oTemp.Servicio  = new Servicio();
                        oTemp.Mantenible = new Vehiculo();
                        oTemp.Servicio.ServicioID = this.view.Entity.OrdenServicio.OrdenServicioID;
                        oTemp.OrdenServicio= new OrdenServicio();
                        oTemp.OrdenServicio.OrdenServicioID = this.view.Entity.OrdenServicio.OrdenServicioID;
                        sresponse = this.view.EntityRefaccion.InsertaRefaccion(iSiguienteFolio.ToString(), oTemp, 1);
                        if (sresponse != "OK")
                            throw new Exception("No se ha podido asiganr la refaccion a la solicitud");
                        iSiguienteFolio++;
                        /*this.view.EntityRefaccion.oDetalleRefaccion.iLineaID = data.iLineaID;
                        this.view.EntityRefaccion.oDetalleRefaccion.Almacen = data.Almacen;
                        this.view.EntityRefaccion.oDetalleRefaccion.Articulo = data.Articulo;
                        this.view.EntityRefaccion.oDetalleRefaccion.Cantidad = data.Cantidad;
                        this.view.EntityRefaccion.oDetalleRefaccion.CodigoAlmacen = data.CodigoAlmacen;
                        this.view.EntityRefaccion.oDetalleRefaccion.Costo = data.Costo;
                        this.view.EntityRefaccion.oDetalleRefaccion.Total = data.Total;
                        this.view.EntityRefaccion.oDetalleRefaccion.Almacen = data.Almacen;
                        this.view.EntityRefaccion.oDetalleRefaccion.CodigoAlmacen = data.CodigoAlmacen;*/
                        //this.Company.StartTransaction();
                        //this.view.EntityRefaccion.Insert();
                        /*if (this.Company.InTransaction)
                            this.Company.EndTransaction(BoWfTransOpt.wf_Commit);*/
                    }
                }


                if (this.view.ListaMecanicos != null)
                {
                    if (this.view.ListaMecanicos.Count > 0)
                    {
                        iSiguienteFolio = new Add(ref Company, null).recuperIDMaximoTabla("VSKF_MECANICOSOS");
                        foreach (Kanan.Mantenimiento.BO2.SBO_KF_OHEM_INFO oMecanico in this.view.ListaMecanicos)
                        {
                            oMecanico.OrdeServicioID = this.view.Entity.OrdenServicio.OrdenServicioID.ToString();
                            sresponse = this.view.EntityDetalle.InsertaMecanicos(oMecanico, iSiguienteFolio.ToString());
                            if (sresponse != "OK")
                                throw new Exception("No se ha podido agregar la lista de mecanicos");
                            iSiguienteFolio++;
                        }
                    }
                }

                if (this.view.ListaHerramientas != null)
                {
                    if (this.view.ListaHerramientas.Count > 0)
                    {
                        iSiguienteFolio = new Add(ref Company, null).recuperIDMaximoTabla("VSKF_HERRAMIENTAOS");
                        foreach (Kanan.Mantenimiento.BO2.SBO_KF_Herramientas_INFO oTool in this.view.ListaHerramientas)
                        {
                            oTool.OrdenServicioID = this.view.Entity.OrdenServicio.OrdenServicioID.ToString();
                            sresponse = this.view.EntityDetalle.InsertaHerramientas(oTool, iSiguienteFolio.ToString());
                            if (sresponse != "OK")
                                throw new Exception("No se ha podido agregar la lista de herramientas");
                            iSiguienteFolio++;
                        }
                    }
                }

                #region Comentado
                //ordenData

                //if (this.Ordenes != null)
                //{
                //    if (this.Ordenes.Count > 0)
                //    {
                //        foreach (OrdenServicio ods in this.Ordenes)
                //        {
                //            if (ods.alertas.Mantenible.GetType() != typeof(Caja))
                //            {
                //                ordenData.OrdenServicio = ods;
                //                ordenData.OrdenServicio.EncargadoOrdenServicio = this.configurations.UserFull;
                //                //ordenData.OrdenServicio.alertas.empresa = this.configurations.UserFull.Dependencia;
                //                ordenData.OrdenServicio.SucursalOrden = this.configurations.UserFull.EmpleadoSucursal;
                //                ordenData.OrdenServicio.EncargadoOrdenServicio = this.configurations.UserFull;
                //                ordenData.OrdenServicioToUDT();
                //                ordenData.Insert();
                //                this.UpdateParametroMantenimiento(ods);
                //                //ordenData.OrdenServicio.alertas.AlertaMantenimientoID = this
                //            }
                //        }
                //        this.oHerramientas.ColocarMensaje("�Registro Guardardo!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                //        this.view.ClearFields();
                //        //this.SBO_Application.Forms.Item("frmAddOS").Close();
                //    }
                //}


                //if (listServicios != null)
                //{

                //    foreach (serviciocosto servicios in listServicios)
                //    {
                //        OrdenServicio ordenesServicios = this.view.OrdenServicio;
                //        if (ordenesServicios.alertas != null)
                //        {
                //            if (servicios.tipoMantenibleID == 1)
                //            {
                //                ordenesServicios.alertas.Mantenible = null;
                //                ordenesServicios.alertas.Mantenible = new Vehiculo();
                //                ordenesServicios.alertas.Mantenible.MantenibleID = servicios.vehiculoid;
                //                ordenesServicios.alertas.servicio.ServicioID = servicios.servicioid;
                //                ordenesServicios.TipoDePago.TipoPagoID = servicios.tipoPagoID;
                //                ordenesServicios.TipoDePago.DetallePago = servicios.detallePago;
                //            }
                //            ordenesServicios.Impuesto = servicios.impuesto;
                //            ordenesServicios.Costo = servicios.costo;
                //            ordenData.OrdenServicio = ordenesServicios;
                //            ordenData.OrdenServicioToUDT();
                //            ordenData.Insert();
                //            this.UpdateParametroMantenimiento(ordenesServicios);
                //        }
                //    }

                //    listServicios.Clear();
                //    this.oHerramientas.ColocarMensaje("�Registro Guardardo!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                //    this.SBO_Application.Forms.Item("frmAddOS").Close();
                //}
                #endregion Comentado
                this.Company.EndTransaction(BoWfTransOpt.wf_Commit);
            }
            catch (Exception ex)
            {
                if (this.Company.InTransaction)
                    this.Company.EndTransaction(BoWfTransOpt.wf_RollBack);
                this.oHerramientas.ColocarMensaje(string.Format("No se ha podido registrar la orden. ERROR {0}",ex.Message), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                KananSAP.Helper.KananSAPHerramientas.LogError("OrdenServicioSinAlertaPresenter.cs->InsertarSAP()", ex.Message);
                /**/
                //throw new Exception(ex.Message);
            }
        }

        private void UpdateSAP()
        {
            try
            {
                //OrdenServicioData ordenData = new OrdenServicioData(this.Company);
                this.view.EntityRefaccion = new DetalleRefaccionesSAP(this.Company);
                //OrdenServicioData ordenData = new OrdenServicioData(this.Company);
                this.view.FormToEntity();
                //this.ItemTemp = KananSAP.Helper.KananSAPHelpers.GetCodeTemporal();
                //ordenData.ItemCode = this.ItemTemp.ToString();
                this.view.Entity.Sincronizado = 0;// cambiar a 2 para implementar revision
                //ordenData.UUID = Guid.NewGuid();
                //this.view.Entity.OrdenServicio = this.view.OrdenServicio;
                this.view.Entity.ItemCode = null;
                //this.view.Entity.OrdenServicioToUDT();
                //this.Company.StartTransaction();
                this.view.Entity.Actualizar();

                long iSiguienteFolio = -1; string sresponse = string.Empty;
                Add oRecord = new Add(ref Company, null);
                if (this.view.ListaServiciosAgregados != null)
                {
                    if (this.view.ListaServiciosAgregados.Count > 0)
                    {
                        //DetalleOrdenSAP dorden = new DetalleOrdenSAP(ref this.oCompany);
                        oRecord.LimpiaTablaCode("VSKF_DETALLEORDEN", this.view.Entity.OrdenServicio.OrdenServicioID.ToString(), "U_OrdenServicioID");
                        iSiguienteFolio = oRecord.recuperIDMaximoTabla("VSKF_DETALLEORDEN");
                        foreach (DetalleOrden sc in this.view.ListaServiciosAgregados)
                        {
                            sc.OrdenServicio = new OrdenServicio();
                            sc.OrdenServicio.OrdenServicioID = this.view.Entity.OrdenServicio.OrdenServicioID;
                            sresponse = this.view.EntityDetalle.InsertaDetalleOrden(sc, iSiguienteFolio.ToString());
                            if (sresponse != "OK")
                                throw new Exception("No se ha podido agregar el detalle de la Orden de Servicio");
                            iSiguienteFolio++;
                        }
                    }
                }

                if (this.view.ListaCostosAdicionales != null)
                {
                    if (this.view.ListaCostosAdicionales.Count > 0)
                    {
                        oRecord.LimpiaTablaCode("VSKF_OSCOSTOSADICIO", this.view.Entity.OrdenServicio.OrdenServicioID.ToString(), "U_Ordenservico");
                        iSiguienteFolio = oRecord.recuperIDMaximoTabla("VSKF_OSCOSTOSADICIO");
                        foreach (SBOKF_CL_CostosAdicional_INFO oCostoa in this.view.ListaCostosAdicionales)
                        {
                            oCostoa.Code = this.view.Entity.OrdenServicio.OrdenServicioID.ToString();
                            sresponse = this.view.EntityDetalle.InsertaCostoAdicional(oCostoa, iSiguienteFolio.ToString());
                            if (sresponse != "OK")
                                throw new Exception("No se ha podido agregar el detalle de la Orden de Servicio");
                            iSiguienteFolio++;
                        }
                    }
                }


                ///------------
                if (this.view.Refacciones != null)
                {
                    if (this.view.Refacciones.Count > 0)
                    {
                        oRecord.LimpiaTablaCode("VSKF_DTLLEREFACCION", this.view.Entity.OrdenServicio.OrdenServicioID.ToString(), "U_ServicioID");
                        iSiguienteFolio = oRecord.recuperIDMaximoTabla("VSKF_DTLLEREFACCION");
                        this.view.EntityRefaccion.LimipiarTabla();
                        List<DetalleRefaccion> lstRefacciones = this.view.Refacciones;
                        foreach (DetalleRefaccion oDetalleSinc in lstRefacciones)
                        {
                            DetalleRefaccion oTemp = oDetalleSinc;
                            oTemp.Servicio = new Servicio();
                            oTemp.Mantenible = new Vehiculo();
                            oTemp.Servicio.ServicioID = this.view.Entity.OrdenServicio.OrdenServicioID;
                            oTemp.OrdenServicio = new OrdenServicio();
                            oTemp.OrdenServicio.OrdenServicioID = this.view.Entity.OrdenServicio.OrdenServicioID;
                            sresponse = this.view.EntityRefaccion.InsertaRefaccion(iSiguienteFolio.ToString(), oTemp, 1);
                            if (sresponse != "OK")
                                throw new Exception("No se ha podido asiganr la refaccion a la solicitud");
                            iSiguienteFolio++;
                        }

                    }
                }

                if (this.view.ListaMecanicos != null)
                {
                    if (this.view.ListaMecanicos.Count > 0)
                    {
                        oRecord.LimpiaTablaCode("VSKF_MECANICOSOS", this.view.Entity.OrdenServicio.OrdenServicioID.ToString(), "U_OrdenServicioID");
                        iSiguienteFolio = oRecord.recuperIDMaximoTabla("VSKF_MECANICOSOS");
                        foreach (Kanan.Mantenimiento.BO2.SBO_KF_OHEM_INFO oMecanico in this.view.ListaMecanicos)
                        {
                            oMecanico.OrdeServicioID = this.view.Entity.OrdenServicio.OrdenServicioID.ToString();
                            sresponse = this.view.EntityDetalle.InsertaMecanicos(oMecanico, iSiguienteFolio.ToString());
                            if (sresponse != "OK")
                                throw new Exception("No se ha podido agregar la lista de mecanicos");
                            iSiguienteFolio++;
                        }
                    }
                }

                if (this.view.ListaHerramientas != null)
                {
                    if (this.view.ListaHerramientas.Count > 0)
                    {
                        oRecord.LimpiaTablaCode("VSKF_HERRAMIENTAOS", this.view.Entity.OrdenServicio.OrdenServicioID.ToString(), "U_OrdenServicioID");
                        iSiguienteFolio = oRecord.recuperIDMaximoTabla("VSKF_HERRAMIENTAOS");
                        foreach (Kanan.Mantenimiento.BO2.SBO_KF_Herramientas_INFO oTool in this.view.ListaHerramientas)
                        {
                            oTool.OrdenServicioID = this.view.Entity.OrdenServicio.OrdenServicioID.ToString();
                            sresponse = this.view.EntityDetalle.InsertaHerramientas(oTool, iSiguienteFolio.ToString());
                            if (sresponse != "OK")
                                throw new Exception("No se ha podido agregar la lista de herramientas");
                            iSiguienteFolio++;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("OrdenServicioSinAlertaPresenter.cs->UpdateSAP()", ex.Message);
                /*if (this.Company.InTransaction)
                    this.Company.EndTransaction(BoWfTransOpt.wf_RollBack);*/
                throw new Exception(ex.Message);
            }
        }

        private void ActualizarSAP()
        {
            //OrdenServicioData ordenData = new OrdenServicioData(this.Company);
            this.view.Entity.ItemCode = this.OrdenServicioReturned.Code.ToString();
            if (this.view.Entity.Existe())
            {
                this.view.Entity.Sincronizado = 1;
                this.view.Entity.ActualizarCode(this.OrdenServicioReturned);

                if (this.OrdenServicioReturned.DetalleOrden.Any())
                {
                    //Eliminar toda el contenido de la tabla para insertar con los datos de WS
                    if (this.view.ListaServiciosAgregados != null)
                    {
                        //DetalleOrdenSAP dorden = new DetalleOrdenSAP(ref this.oCompany);
                        new Add(ref Company, null).LimpiaTablaCode("VSKF_DETALLEORDEN", this.OrdenServicioReturned.Code.ToString(), "U_OrdenServicioID");
                        long iSiguienteFolio = new Add(ref Company, null).recuperIDMaximoTabla("VSKF_DETALLEORDEN");
                        foreach (DetalleOrden sc in this.OrdenServicioReturned.DetalleOrden)
                        {
                            sc.OrdenServicio = new OrdenServicio();
                            sc.OrdenServicio.OrdenServicioID = this.OrdenServicioReturned.Code;
                            this.view.EntityDetalle.InsertaDetalleOrden(sc, iSiguienteFolio.ToString());
                            iSiguienteFolio++;
                        }
                    }
                }
            }
        }

        private void UpdateParametroMantenimiento()
        {
            try
            {
                if (this.view.ListaServiciosAgregados != null && this.view.ListaServiciosAgregados.Count > 0)
                {
                    ParametroMantenimientoHelperData helpData = new ParametroMantenimientoHelperData(this.Company);
                    ParametroMantenimientoData pmData = new ParametroMantenimientoData(this.Company);
                    ParametroServicioData psData = new ParametroServicioData(this.Company);
                    DesplazamientoData dsData = new DesplazamientoData(this.Company);

                    foreach (DetalleOrden sc in this.view.ListaServiciosAgregados)
                    {
                        ParametroMantenimiento parametro = new ParametroMantenimiento();
                        ParametroMantenimiento nuevoParametro = new ParametroMantenimiento();
                        Desplazamiento desplazamiento = new Desplazamiento();
                        parametro.ParametroServicio = new ParametroServicio();
                        parametro.ParametroServicio.Servicio = new Servicio();
                        if (sc.TipoMantenibleID == 1)
                            parametro.Mantenible = new Vehiculo();
                        else if (sc.TipoMantenibleID == 2)
                            parametro.Mantenible = new Caja();
                        parametro.ParametroServicio.Servicio.ServicioID = sc.Servicio.ServicioID;
                        parametro.Mantenible.MantenibleID = sc.Mantenible.MantenibleID;
                        SAPbobsCOM.Recordset rs = pmData.Consultar(parametro);
                        if (pmData.HasParametroMantenimiento(rs))
                        {
                            parametro = pmData.LastRecordSetToParametroMantenimiento(rs);
                            parametro = pmData.ConsultarCompleto(parametro.ParametroMantenimientoID);
                            nuevoParametro = (ParametroMantenimiento)parametro.Clone();
                            nuevoParametro.UltimoServicio = desplazamiento.DistanciaRecorrida;
                            nuevoParametro.ProximoServicio = nuevoParametro.ParametroServicio.Valor + nuevoParametro.UltimoServicio;
                            nuevoParametro.AlertaProximoServicio = nuevoParametro.ProximoServicio - nuevoParametro.ParametroServicio.Alerta;
                            /// Se calcula el parametro por tiempo
                            int intervaloTiempo = 0;
                            int alertaTiempoTemp = 0;
                            if (parametro.ProximoServicioTiempo != null && parametro.UltimoServicioTiempo != null)
                            {
                                TimeSpan res = (TimeSpan)((DateTime)parametro.ProximoServicioTiempo - (DateTime)parametro.UltimoServicioTiempo);
                                intervaloTiempo = res.Days;
                                /// valid type of parameter for calculate de value of range alert for time
                                if (nuevoParametro.ParametroServicio.Mantenible.GetType() == typeof(Vehiculo))
                                {

                                    if (nuevoParametro.ParametroServicio.AlertaTiempo != null && intervaloTiempo > 0)
                                    {
                                        alertaTiempoTemp = (int)nuevoParametro.ParametroServicio.AlertaTiempo;
                                        nuevoParametro.ParametroServicio.ValorTiempo = intervaloTiempo;
                                        nuevoParametro.ParametroServicio.AlertaTiempo = alertaTiempoTemp;
                                    }
                                }
                                else
                                {
                                    if (nuevoParametro.ParametroServicio.AlertaTiempo != null && intervaloTiempo > 0)
                                    {
                                        alertaTiempoTemp = (int)nuevoParametro.ParametroServicio.AlertaTiempo;
                                    }
                                }
                                if (intervaloTiempo > 0 && alertaTiempoTemp > 0)
                                {
                                    /// Take the utc now for get date and replace the last service time
                                    DateTime lastService = DateTime.Now;
                                    nuevoParametro.UltimoServicioTiempo = lastService;
                                    nuevoParametro.ProximoServicioTiempo = nuevoParametro.UltimoServicioTiempo.Value.AddDays(intervaloTiempo);
                                    nuevoParametro.AlertaProximoServicioTiempo = nuevoParametro.ProximoServicioTiempo.Value.AddDays(-alertaTiempoTemp);
                                    ///Se actualiza el parametro de mantenimiento
                                }
                            }
                            helpData.oParametroMantenimiento = nuevoParametro;
                            helpData.ActualizarCompleto();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("OrdenServicioSinAlertaPresenter.cs->UpdateParametroMantenimiento()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        #region Comentado
        //private void UpdateParametroMantenimiento(OrdenServicio orden)
        //{
        //    try
        //    {
        //        ParametroMantenimientoHelperData helpData = new ParametroMantenimientoHelperData(this.Company);
        //        ParametroMantenimientoData pmData = new ParametroMantenimientoData(this.Company);
        //        ParametroServicioData psData = new ParametroServicioData(this.Company);
        //        DesplazamientoData dsData = new DesplazamientoData(this.Company);
        //        Desplazamiento desplazamiento = new Desplazamiento();
        //        ParametroMantenimiento parametro = new ParametroMantenimiento();
        //        ParametroMantenimiento nuevoParametro = new ParametroMantenimiento();
        //        parametro.ParametroServicio = new ParametroServicio();
        //        parametro.Mantenible = new Vehiculo();
        //        //if (orden.alertas.Mantenible.GetType() == typeof(Vehiculo))
        //        //    parametro.Mantenible = new Vehiculo();
        //        //else if (orden.alertas.Mantenible.GetType() == typeof(Caja))
        //        //    parametro.Mantenible = new Caja();

        //        //parametro.ParametroServicio.Servicio = orden.alertas.servicio;
        //        //parametro.Mantenible = orden.alertas.Mantenible;
        //        SAPbobsCOM.Recordset rs = pmData.Consultar(parametro);
        //        if(pmData.HasParametroMantenimiento(rs))
        //        {
        //            parametro = pmData.LastRecordSetToParametroMantenimiento(rs);
        //            parametro = pmData.ConsultarCompleto(parametro.ParametroMantenimientoID);
        //            nuevoParametro = (ParametroMantenimiento)parametro.Clone();
        //            //if(dsData.SetUDTByVehiculoID(orden.alertas.vehiculo.VehiculoID))
        //            //{
        //            //    dsData.UDTToDesplazamiento();
        //            //    desplazamiento = dsData.Desplazamiento;
        //            //}
        //            //rs = dsData.Consultar(desplazamiento, orden.alertas.vehiculo);
        //            //if(dsData.HashDesplazamiento(rs))
        //            //{
        //            //  desplazamiento = dsData.LastRecordSetToDesplazamiento(rs);
        //            //}

        //            nuevoParametro.UltimoServicio = desplazamiento.DistanciaRecorrida;
        //            nuevoParametro.ProximoServicio = nuevoParametro.ParametroServicio.Valor + nuevoParametro.UltimoServicio;
        //            nuevoParametro.AlertaProximoServicio = nuevoParametro.ProximoServicio - nuevoParametro.ParametroServicio.Alerta;
        //            /// Se calcula el parametro por tiempo
        //            int intervaloTiempo = 0;
        //            int alertaTiempoTemp = 0;
        //            if (parametro.ProximoServicioTiempo != null && parametro.UltimoServicioTiempo != null)
        //            {
        //                TimeSpan res = (TimeSpan)((DateTime)parametro.ProximoServicioTiempo - (DateTime)parametro.UltimoServicioTiempo);
        //                intervaloTiempo = res.Days;
        //                /// valid type of parameter for calculate de value of range alert for time
        //                if (nuevoParametro.ParametroServicio.Mantenible.GetType() == typeof(Vehiculo))
        //                {

        //                    if (nuevoParametro.ParametroServicio.AlertaTiempo != null && intervaloTiempo > 0)
        //                    {
        //                        alertaTiempoTemp = (int)nuevoParametro.ParametroServicio.AlertaTiempo;
        //                        nuevoParametro.ParametroServicio.ValorTiempo = intervaloTiempo;
        //                        nuevoParametro.ParametroServicio.AlertaTiempo = alertaTiempoTemp;
        //                    }

        //                }
        //                else
        //                {

        //                    if (nuevoParametro.ParametroServicio.AlertaTiempo != null && intervaloTiempo > 0)
        //                    {
        //                        alertaTiempoTemp = (int)nuevoParametro.ParametroServicio.AlertaTiempo;
        //                    }

        //                }
        //                if (intervaloTiempo > 0 && alertaTiempoTemp > 0)
        //                {
        //                    /// Take the utc now for get date and replace the last service time
        //                    DateTime lastService = DateTime.Now;
        //                    nuevoParametro.UltimoServicioTiempo = lastService;
        //                    nuevoParametro.ProximoServicioTiempo = nuevoParametro.UltimoServicioTiempo.Value.AddDays(intervaloTiempo);
        //                    nuevoParametro.AlertaProximoServicioTiempo = nuevoParametro.ProximoServicioTiempo.Value.AddDays(-alertaTiempoTemp);
        //                    ///Se actualiza el parametro de mantenimiento
        //                }
        //            }
        //            helpData.oParametroMantenimiento = nuevoParametro;
        //            helpData.ActualizarCompleto();
        //        }
        //    }
        //    catch (Exception ex) { throw new Exception(ex.Message); }
        //}
        #endregion Comentado

        #endregion

        #region Metodos WS
        private void InsertWS()
        {
            try
            {
                //this.view.FormToData();
                //this.Ordenes = this.ordenWS.InsertarSinAlerta(this.view.OrdenServicio, this.view.ListaServiciosAgregados);
                this.view.Entity.OrdenServicio.CostosAdicionalesDto = this.view.ListaCostosAdicionales;
                this.view.Entity.OrdenServicio.EmpleadosDto = this.view.ListaMecanicos;
                this.view.Entity.OrdenServicio.HerramientasDto = this.view.ListaHerramientas;
                this.OrdenServicioReturned = this.ordenWS.InsertarSinAlerta(this.view.Entity.OrdenServicio);
                if (this.OrdenServicioReturned == null)
                    throw new Exception("Es posible que no se haya guardado la información");
                if (this.OrdenServicioReturned.OrdenServicioID == null)
                    throw new Exception("Es posible que no se haya guardado la información");
                //if (this.OrdenServicioReturned.DetalleOrden == null || this.OrdenServicioReturned.DetalleOrden.Count <= 0)
                //    throw new Exception("Es posible que no se haya guardado la informaci�n");
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("OrdenServicioSinAlertaPresenter.cs->InsertarWS()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void UpdateWS()
        {
            try
            {
                //this.view.FormToData();
                //this.Ordenes = this.ordenWS.InsertarSinAlerta(this.view.OrdenServicio, this.view.ListaServiciosAgregados);
                //this.view.Entity.OrdenServicio.DetalleOrden = null;

                //this.view.Entity.OrdenServicio.DetalleOrden = this.view.Refacciones;                
                this.view.Entity.OrdenServicio.CostosAdicionalesDto = this.view.ListaCostosAdicionales;
                this.view.Entity.OrdenServicio = this.ordenWS.ActualizarSinAlerta(this.view.Entity.OrdenServicio);
                try
                {
                    if (this.view.Entity.OrdenServicio == null)
                        throw new Exception("Es posible que no se haya guardado la información");
                    if (this.view.Entity.OrdenServicio.OrdenServicioID == null)
                        throw new Exception("Es posible que no se haya guardado la información");
                }
                catch { }
                //if (this.OrdenServicioReturned.DetalleOrden == null || this.OrdenServicioReturned.DetalleOrden.Count <= 0)
                //    throw new Exception("Es posible que no se haya guardado la informaci�n");
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("OrdenServicioSinAlertaPresenter.cs->UpdateWS()", ex.Message);
                throw new Exception(ex.Message);
            }
        }
        #endregion

      
        private Boolean InsertarOrdenes()
        {
            #region Comentarios
            /*
             * Comentarios: Metodo encargado de realizar la insercion de una orden de servicio.
             *
             * Actualizaciones:
             * Se modifica el tipo de dato devuelto a boleano, para saber si la orden ha sido o no
             * creada, con el fin de utilizar la respuesta para desencadenar acciones.
             * Se crearon campos nuevos para saber cuando el veh�culo/activo ingresa o sale del
             * taller.
             */
            #endregion

            bool bContinuar = false;
            try
            {
                
                this.InsertSAP();
                if (this.view.Entity.OrdenServicio.SucursalOrden.SucursalID == 0)
                    this.view.Entity.OrdenServicio.SucursalOrden.SucursalID = null;
                try
                {
                    this.InsertWS();
                    this.ActualizarSAP();
                }
                catch (Exception oex)
                {
                    KananSAP.Helper.KananSAPHerramientas.LogError("OrdenServicioSinAlertaPresenter.cs->InsertarOrdenes->InsertWS()", oex.Message);
                }
                this.oHerramientas.ColocarMensaje("¡Registro Guardado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                bContinuar = true;
                //this.view.EmptyEntityToForm();
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("OrdenServicioSinAlertaPresenter.cs->InsertarOrdenes()", ex.Message);
                this.view.EmptyEntityToForm();
                //this.SBO_Application.Forms.ActiveForm.Close();
                throw new Exception(ex.Message);
            }
            return bContinuar;
        }

        private void ActualizarOrdenes()
        {
            try
            {
                bool bContinua = false;
                string sresponse = string.Empty, sDocEntry = string.Empty, consultaSql = string.Empty;
                SBO_KF_Configuracion_AD oConfig = new SBO_KF_Configuracion_AD();
                SBO_KF_Configuracion_INFO oSetting = new SBO_KF_Configuracion_INFO();
                sresponse = oConfig.recuperaConfiguracion(ref Company, out consultaSql, ref oSetting);
                if (sresponse == "OK")
                {

                    this.Company.StartTransaction();
                    #region Creacion de documentos SAP Factura y Salida de refacciones cuando se gestiona inventario por SBO

                    if (this.view.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID == 6)
                    {
                        bContinua = this.ValidaDocumentoB1(this.view.Entity.OrdenServicio, out sDocEntry, false, "OIGE");
                        if (!bContinua)
                        {
                            sresponse = this.SalidaInventarioRefacciones(this.view.Entity.OrdenServicio, this.view.Refacciones);
                            if (sresponse != "OK")
                                throw new Exception(sresponse);
                        }

                        if (this.view.Entity.OrdenServicio.FacturableCliente == 1)
                        {
                            bContinua = this.ValidaDocumentoB1(this.view.Entity.OrdenServicio, out sDocEntry, false, "OINV");
                            if (bContinua)
                            {
                                bContinua = this.CreaFacturaCliente(this.view.Entity.OrdenServicio, oSetting);
                                if (!bContinua)
                                    throw new Exception("No se ha podido crear la factura de cliente");
                            }
                        }
                    }

                    if (oSetting.EstatusOS == this.view.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID)
                    {
                        bool bDrafmood = oSetting.DraftOS == 1;
                        bContinua = this.ValidaDocumentoB1(this.view.Entity.OrdenServicio, out sDocEntry, bDrafmood, "OPCH");
                        if (!bContinua)
                        {
                            bContinua = CrearFactura(this.view.Entity.OrdenServicio, bDrafmood);
                            if (!bContinua)
                                throw new Exception("No se ha podido crear la factura");
                        }
                    }                   

                    #endregion Creacion de documentos SAP Factura y Salida de refacciones cuando se gestiona inventario por SBO
                    this.InsertaBitacora(this.view.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID.ToString());
                    this.UpdateSAP();
                    this.UpdateWS();
                    this.oHerramientas.ColocarMensaje("¡Registro Guardado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

                    this.view.EmptyEntityToForm();
                    this.SBO_Application.Forms.ActiveForm.Close();

                    if (this.Company.InTransaction)
                        this.Company.EndTransaction(BoWfTransOpt.wf_Commit);
                }
                else throw new Exception("No se ha encontrado la configuración para el AddOn");
            }
            catch (Exception ex)
            {
                if (this.Company.InTransaction)
                    this.Company.EndTransaction(BoWfTransOpt.wf_RollBack);
                KananSAP.Helper.KananSAPHerramientas.LogError("OrdenServicioSinAlertaPresenter.cs->ActualizarOrdenes()", ex.Message);
                //this.view.EmptyEntityToForm();
                //this.SBO_Application.Forms.ActiveForm.Close();
                throw new Exception(ex.Message);
            }
        }

        void InsertaBitacora(string OrdenServicioID, bool bNuevo = false)
        {
            string sresponse = string.Empty;
            bool bContinua = bNuevo;

            if (!bContinua)
            {
                if (this.view.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID != this.view.statusInicial)
                    bContinua = true;
            }

            if (bContinua)
            {
                SBO_KF_BITACORA_INFO oLog = new SBO_KF_BITACORA_INFO();
                SBO_KF_BITACORA_PD oControlLog = new SBO_KF_BITACORA_PD();
                long iCode = new Add(ref Company, null).recuperIDMaximoTabla("VSKF_OSCAMESTADO");
                if (iCode > 0)
                {
                    oLog.fechaHora = DateTime.Now;
                    oLog.sincronizado = false;
                    oLog.origen = "S";
                    oLog.estatusAnt = string.Empty;
                    oLog.estatusAntID = -1;
                    oLog.Code = iCode.ToString();
                    oLog.Name = iCode.ToString();
                    oLog.usuario = this.Company.UserName;
                    oLog.OrdenServicioSAPID = OrdenServicioID;
                    oLog.OrdenServicioWebID = OrdenServicioID;
                    oLog.estatusActID = Convert.ToInt32(this.view.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID);
                    oLog.estatusAct = this.GetStatusbyID(oLog.estatusActID); 
                    if (!bNuevo)
                    {
                        oLog.estatusAntID = Convert.ToInt32(this.view.statusInicial);
                        oLog.estatusAnt = this.GetStatusbyID(oLog.estatusAntID);
                    }                    
                    sresponse = oControlLog.CreateBitacora(ref this.Company, oLog);
                    if (sresponse != "OK")
                        throw new Exception("No se ha podido crear el registro de bitacora");
                }
                else throw new Exception("No se ha podido asignar el folio para la bitacora");
            }
        }

        string GetStatusbyID(int? EstatusOS)
        {
            string sestatus = string.Empty;
            try
            {
                string query = string.Format(@"select ""Name"" from ""@VSKF_ESTATUSORDEN"" where U_Activo = 1 AND ""Code"" = '{0}' ", EstatusOS);
                SAPbobsCOM.Recordset rs = this.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    rs.MoveFirst();
                    sestatus =  Convert.ToString(rs.Fields.Item("Name").Value);
                }
                else sestatus = "No registrado";
            }
            catch { }

            return sestatus;
        }

        #region GetPDF
        private String GetPDF()
        {
            String Ruta = String.Empty;

            String pathFile = Directorio();

            if (string.IsNullOrEmpty(pathFile))
                throw new Exception("No se ha determinado el directorio de PDF");

            pathFile = string.Format(@"{0}\OrdenServicio", pathFile);
            if (!Directory.Exists(pathFile))
                Directory.CreateDirectory(pathFile);
            this.view.FormToEntity();

            string NameFilePDF = string.Format(@"{0}\{1}_{2}.pdf", pathFile, this.view.Entity.OrdenServicio.FolioOrden.Folio, this.view.Entity.OrdenServicio.FolioOrden.AnioFolio);

            //D:\\\\OrdenServicio\\36_2019.pdf
            Ruta = NameFilePDF;//.Replace("\\\\", "/").Replace("\\", "/");

            return Ruta;
        }
        #endregion GetPDF

        #region ExportarPDF
        private Boolean ExportarPDF(out string PDF, bool bStart = true)
        {
            Boolean bContinuar = false;
            String pathFile = Directorio();

            if (string.IsNullOrEmpty(pathFile))
                throw new Exception("No se ha determinado el directorio de PDF");

            pathFile = string.Format(@"{0}\OrdenServicio", pathFile);
            if (!Directory.Exists(pathFile))
                Directory.CreateDirectory(pathFile);
            this.view.FormToEntity();
            string NameFilePDF = string.Format(@"{0}\{1}{2}.pdf", pathFile, this.view.Entity.OrdenServicio.FolioOrden.Folio, this.view.Entity.OrdenServicio.FolioOrden.AnioFolio);

            String FechActual = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
            PdfWriter oPDFWrite;
            PdfContentByte oPDFcb;
            PDF = NameFilePDF;
            if (File.Exists(NameFilePDF))
                File.Delete(NameFilePDF);

            iTextSharp.text.Font oFontConcepto = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 11, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            iTextSharp.text.Font oFontTittle = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font oFontSubtittle = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 11, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font oespacios = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 1, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            try
            {
                #region Formato
                Double dCosto = Convert.ToDouble(this.view.Entity.OrdenServicio.Costo);
                Double dImpuesto = Convert.ToDouble(this.view.Entity.OrdenServicio.Impuesto);
                Double dSubtotal = dCosto - dImpuesto;
                Double dTotal = dSubtotal + dImpuesto;

                Document oDocumento;
                oDocumento = new Document(PageSize.A4, 20, 20, 25, 10);
                oPDFWrite = PdfWriter.GetInstance(oDocumento, new FileStream(NameFilePDF, FileMode.Create));
                oDocumento.Open();
                oPDFcb = oPDFWrite.DirectContent;
                oPDFcb.BeginText();

                float cellHeight = oDocumento.TopMargin;
                iTextSharp.text.Rectangle oPage = oDocumento.PageSize;
                oPDFcb.EndText();

                PdfPTable oTitulo = new PdfPTable(2);
                oTitulo.WidthPercentage = 100;
                float[] widthHeader = { 400f, 400f };
                oTitulo.SetWidths(widthHeader);
                oTitulo.HorizontalAlignment = 0;

                PdfPCell oCelda = new PdfPCell();
                oCelda.Colspan = 2;
                oCelda.FixedHeight = 18f;

                estiloSBorde(ref oCelda, "REPORTE DETALLE DE ORDEN DE SERVICIO", oFontTittle);
                oCelda.VerticalAlignment = Element.ALIGN_LEFT;
                oCelda.HorizontalAlignment = Element.ALIGN_LEFT;
                oTitulo.AddCell(oCelda);

                estiloSBorde(ref oCelda, string.Format("Fecha:   {0}", FechActual), oFontConcepto);
                oCelda.VerticalAlignment = Element.ALIGN_RIGHT;
                oCelda.HorizontalAlignment = Element.ALIGN_RIGHT;
                oTitulo.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontSubtittle);
                oCelda.Colspan = 2;
                oCelda.FixedHeight = 5f;
                oCelda.BorderWidthBottom = .5f;
                oTitulo.AddCell(oCelda);
                oDocumento.Add(oTitulo);

                PdfPTable oConceptos = new PdfPTable(4);
                oConceptos.WidthPercentage = 100;
                float[] widthHeader2 = { 200f, 20f, 400f, 400f };
                oConceptos.SetWidths(widthHeader2);
                oConceptos.HorizontalAlignment = 0;

                estiloSBorde(ref oCelda, "", oFontSubtittle);
                oCelda.Colspan = 2;
                oCelda.FixedHeight = 15f;
                oConceptos.AddCell(oCelda);

                estiloSBorde(ref oCelda, "", oFontSubtittle);
                oCelda.Colspan = 2;
                oCelda.FixedHeight = 15f;
                oConceptos.AddCell(oCelda);
                oCelda = new PdfPCell();


                oDocumento.Add(oConceptos);
                PdfPTable oDetalle = new PdfPTable(4);
                oDetalle.WidthPercentage = 100;
                float[] widthHeaders = { 60f, 5f, 140f, 120f };
                oDetalle.SetWidths(widthHeaders);
                oDetalle.HorizontalAlignment = 0;

                oCelda = new PdfPCell();
                oCelda.Colspan = 2;
                oCelda.FixedHeight = 18f;
                String cardCode = "";

                #region CONCEPTOS
                estiloCeldaColor(ref oCelda, "Estatus:", oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                estiloCeldaColor(ref oCelda, DevolverServicio(this.view.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID), oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);

                EspacioBlanco(ref oCelda, oespacios, oDetalle);

                estiloCeldaColor(ref oCelda, "Proveedor:", oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                this.view.Entity.RecuperaProveedor(this.view.Entity.OrdenServicio.ProveedorServicio.ProveedorID, GestionGlobales.bSqlConnection, ref cardCode, false);
                estiloCeldaColor(ref oCelda, cardCode, oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                EspacioBlanco(ref oCelda, oespacios, oDetalle);

                estiloCeldaColor(ref oCelda, "Fecha:", oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                estiloCeldaColor(ref oCelda, this.view.Entity.OrdenServicio.Fecha.Value.ToString("dd/MM/yyyy"), oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);

                EspacioBlanco(ref oCelda, oespacios, oDetalle);


                estiloCeldaColor(ref oCelda, "Folio:", oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                estiloCeldaColor(ref oCelda, this.view.Entity.OrdenServicio.FolioOrden.Folio == 0 ? "Desconocido" : this.view.Entity.OrdenServicio.FolioOrden.Folio.ToString(), oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);

                EspacioBlanco(ref oCelda, oespacios, oDetalle);


                estiloCeldaColor(ref oCelda, "Descripción:", oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                estiloCeldaColor(ref oCelda, this.view.Entity.OrdenServicio.Descripcion, oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                EspacioBlanco(ref oCelda, oespacios, oDetalle);


                estiloCeldaColor(ref oCelda, "Método de pago:", oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                String TipoPago = this.view.Entity.GetNameTipoPago(this.view.Entity.OrdenServicio.TipoDePago.TipoPagoID);
                estiloCeldaColor(ref oCelda, this.view.Entity.OrdenServicio.TipoDePago.TipoPago, oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                EspacioBlanco(ref oCelda, oespacios, oDetalle);


                estiloCeldaColor(ref oCelda, "Detalle de pago:", oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                estiloCeldaColor(ref oCelda, this.view.Entity.OrdenServicio.TipoDePago.DetallePago, oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                EspacioBlanco(ref oCelda, oespacios, oDetalle);

                estiloCeldaColor(ref oCelda, "Responsable:", oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                estiloCeldaColor(ref oCelda, FindEmpleado(this.view.Entity.OrdenServicio.ResponsableOrdenServicio.EmpleadoID), oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                EspacioBlanco(ref oCelda, oespacios, oDetalle);

                estiloCeldaColor(ref oCelda, "Aprobador:", oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                estiloCeldaColor(ref oCelda, FindEmpleado(this.view.Entity.OrdenServicio.AprobadorOrdenServicio.EmpleadoID), oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                EspacioBlanco(ref oCelda, oespacios, oDetalle);

                estiloCeldaColor(ref oCelda, "Subtotal:", oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                estiloCeldaColor(ref oCelda, string.Format("$ {0}", dSubtotal), oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                EspacioBlanco(ref oCelda, oespacios, oDetalle);

                estiloCeldaColor(ref oCelda, "Impuesto:", oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);

                estiloCeldaColor(ref oCelda, string.Format("$ {0}", dImpuesto), oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                EspacioBlanco(ref oCelda, oespacios, oDetalle);

                estiloCeldaColor(ref oCelda, "Total:", oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                estiloCeldaColor(ref oCelda, string.Format("$ {0}", dTotal), oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                EspacioBlanco(ref oCelda, oespacios, oDetalle);

                oDocumento.Add(oDetalle);
                #endregion

                PdfPTable Orden = new PdfPTable(1);
                Orden.WidthPercentage = 100;
                Orden.HorizontalAlignment = 0;
                oCelda = new PdfPCell();
                oCelda.FixedHeight = 18f;
                estiloCeldaColor(ref oCelda, "Detalle de la orden", oFontSubtittle, new BaseColor(231, 231, 233), false);
                Orden.AddCell(oCelda);


                oDocumento.Add(Orden);

                PdfPTable lstConcepto = new PdfPTable(3);
                lstConcepto.WidthPercentage = 100;
                float[] widthConcepto = { 400f, 400f, 400f };
                lstConcepto.SetWidths(widthConcepto);
                lstConcepto.HorizontalAlignment = 0;

                oCelda = new PdfPCell();
                oCelda.Colspan = 2;
                oCelda.FixedHeight = 18f;

                estiloCeldaColor(ref oCelda, "Vehículo", oFontSubtittle, new BaseColor(174, 174, 174), false);
                lstConcepto.AddCell(oCelda);

                estiloCeldaColor(ref oCelda, "Servicio", oFontSubtittle, new BaseColor(174, 174, 174), false);
                lstConcepto.AddCell(oCelda);

                estiloCeldaColor(ref oCelda, "Costo", oFontSubtittle, new BaseColor(174, 174, 174), false);
                lstConcepto.AddCell(oCelda);

                int iLine = 0;
                if (this.view.ListaServiciosAgregados.Count == 0)
                    throw new Exception("No se han encontrado servicios agregados");

                foreach (DetalleOrden svc in this.view.ListaServiciosAgregados)
                {
                    iLine++;
                    var BaseOcolor = new BaseColor(231, 231, 231);
                    if (iLine % 2 == 0)
                    {
                        BaseOcolor = new BaseColor(207, 207, 207);
                    }

                    estiloCeldaColor(ref oCelda, Vehiculo(svc.Mantenible.MantenibleID), oFontConcepto, BaseOcolor);
                    lstConcepto.AddCell(oCelda);

                    estiloCeldaColor(ref oCelda, svc.Servicio.Nombre, oFontConcepto, BaseOcolor, false);
                    lstConcepto.AddCell(oCelda);

                    estiloCeldaColor(ref oCelda, string.Format("$ {0}", svc.Costo.ToString("0.00")), oFontConcepto, BaseOcolor, false);
                    lstConcepto.AddCell(oCelda);



                }

                PdfPTable oBarraFirma = new PdfPTable(1);
                oBarraFirma.WidthPercentage = 60;
                oBarraFirma.HorizontalAlignment = Element.ALIGN_CENTER;
                estiloSBorde(ref oCelda, "      ", oFontSubtittle);
                oCelda.VerticalAlignment = Element.ALIGN_CENTER;
                oCelda.HorizontalAlignment = Element.ALIGN_CENTER;
                oCelda.PaddingTop += 100;
                oCelda.BorderWidthBottom = .2f;
                oBarraFirma.AddCell(oCelda);

                /*PdfPTable oBarraFirma = new PdfPTable(1);
                oBarraFirma.WidthPercentage = 60;
                oBarraFirma.HorizontalAlignment = Element.ALIGN_CENTER;
                estiloSBorde(ref oCelda, "", oFontSubtittle);
                //oCelda.FixedHeight = 5f;
                oCelda.BorderWidthBottom = .2f;
                oCelda.VerticalAlignment = Element.ALIGN_CENTER;
                oCelda.HorizontalAlignment = Element.ALIGN_CENTER;
                oCelda.PaddingTop += 100;
                oBarraFirma.AddCell(oCelda);*/


                PdfPTable oFirma = new PdfPTable(1);
                oFirma.WidthPercentage = 60;
                oFirma.HorizontalAlignment = Element.ALIGN_CENTER;
                estiloSBorde(ref oCelda, "NORMA & FIRMA", oFontSubtittle);
                oCelda.VerticalAlignment = Element.ALIGN_CENTER;
                oCelda.HorizontalAlignment = Element.ALIGN_CENTER;
                oCelda.PaddingTop += 2;
                oFirma.AddCell(oCelda);
                //oCelda = new PdfPCell();
               /* estiloSBorde(ref oCelda, "", oFontSubtittle);
                
                //oCelda.Colspan = 2;
                oCelda.FixedHeight = 5f;
                oCelda.BorderWidthBottom = .2f;
                oCelda.VerticalAlignment = Element.ALIGN_CENTER;
                oCelda.HorizontalAlignment = Element.ALIGN_CENTER;
                oCelda.PaddingTop = 350;
                oFirma.AddCell(oCelda);
                oDocumento.Add(oFirma);

                oFirma = new PdfPTable(1);
                oFirma.WidthPercentage = 60;
                oFirma.HorizontalAlignment = Element.ALIGN_CENTER;

                //oCelda.FixedHeight = 18f;                
                estiloSBorde(ref oCelda, "FIRMA", oFontSubtittle);
                oCelda.VerticalAlignment = Element.ALIGN_CENTER;
                oCelda.HorizontalAlignment = Element.ALIGN_CENTER;
                oCelda.PaddingTop += 2;
                oFirma.AddCell(oCelda);*/
                
                /*estiloSBorde(ref oCelda, "FIRMA", oFontTittle);
                
                oTitulo.AddCell(oCelda);*/

                oDocumento.Add(lstConcepto);
                oDocumento.Add(oBarraFirma);
                oDocumento.Add(oFirma);
                oDocumento.Close();
                oPDFWrite.Close();
                #endregion Formato

                if (bStart)
                    System.Diagnostics.Process.Start(NameFilePDF);

            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("OrdenServicioSinAlertaPresenter.cs->ExportarPDF()", ex.Message);
                bContinuar = false;
                oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            }

            return bContinuar;
        }
        #endregion ExportarPDF

        private void EspacioBlanco(ref PdfPCell oCelda, iTextSharp.text.Font oFuente, PdfPTable oDetalle)
        {
            for (int i = 0; i < 4; i++)
            {
                estiloSBorde(ref oCelda, "\n", oFuente);
                oDetalle.AddCell(oCelda);
            }
        }

        private void estiloSBorde(ref PdfPCell oCelda, string sTextoAgregar, iTextSharp.text.Font oFuente)
        {
            oCelda = new PdfPCell(new Phrase(sTextoAgregar, oFuente));
            oCelda.Border = PdfPCell.NO_BORDER;
        }

        private void estiloCeldaColor(ref PdfPCell oCelda, string sTextoAgregar, iTextSharp.text.Font oFuente, BaseColor oColor, bool bAling = true)
        {
            oCelda = new PdfPCell(new Phrase(sTextoAgregar, oFuente));
            oCelda.Border = PdfPCell.NO_BORDER;
            oCelda.BackgroundColor = oColor;
            oCelda.Border = PdfPCell.NO_BORDER;
            if (bAling)
            {
                oCelda.VerticalAlignment = Element.ALIGN_LEFT;
                oCelda.HorizontalAlignment = Element.ALIGN_LEFT;
            }
            else
            {
                oCelda.VerticalAlignment = Element.ALIGN_CENTER;
                oCelda.HorizontalAlignment = Element.ALIGN_CENTER;
            }

        }

        public string DevolverServicio(int? EstatusOrdenID)
        {
            string Servicio = "";

            switch (EstatusOrdenID)
            {
                case 1:
                    Servicio = "Servicio solicitado";
                    break;
                case 2:
                    Servicio = "Solicitud aprobada";
                    break;
                case 3:
                    Servicio = "Solicitud eliminada";
                    break;
                case 5:
                    Servicio = "Solicitud atendida";
                    break;
                case 6:
                    Servicio = "Solicitud realizada";
                    break;
            }

            return Servicio;
        }

        private String Directorio()
        {
            String Path = "";
            String pathAttach = string.Format(@"SELECT ""AttachPath"" FROM OADP");
            Recordset oRecordQuery = Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            oRecordQuery.DoQuery(pathAttach);

            if (oRecordQuery.RecordCount > 0)
            {
                oRecordQuery.MoveFirst();
                Path = Convert.ToString(oRecordQuery.Fields.Item("AttachPath").Value);
            }

            return Path;
        }

        private String Vehiculo(int? MantenibleID)
        {
            String NombreVehiculo = "";
            String Vehiculo = string.Format(@"select ""Name"",U_Descripcion from ""@VSKF_VEHICULO"" WHERE ""U_VEHICULOID"" = '{0}'", MantenibleID);
            Recordset oRecordQuery = Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            oRecordQuery.DoQuery(Vehiculo);

            if (oRecordQuery.RecordCount > 0)
            {
                oRecordQuery.MoveFirst();
                NombreVehiculo = string.Format("{0} - {1}", Convert.ToString(oRecordQuery.Fields.Item("Name").Value), Convert.ToString(oRecordQuery.Fields.Item("U_Descripcion").Value));
            }
            else NombreVehiculo = "Desconocido";
            return NombreVehiculo;
        }

        private String FindEmpleado(int? EmpleadoID)
        {

            String NombreEmpleado = "";
            String Empleado = @"select * from ""@VSKF_RELEMPLEADO"" WHERE U_KFEmpleadoID = " + EmpleadoID;
            Recordset oRecordQuery = Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            oRecordQuery.DoQuery(Empleado);

            if (oRecordQuery.RecordCount > 0)
            {
                oRecordQuery.MoveFirst();
                NombreEmpleado = Convert.ToString(oRecordQuery.Fields.Item("Name").Value);
            }
            else NombreEmpleado = "Desconocido";
            return NombreEmpleado;
        }

        private void ReenviarNotificacion(int? EmpleadoID)
        {
            String Attachment = string.Empty, Asunto = string.Empty, resultado = string.Empty, correoPara = string.Empty, Nombre = string.Empty, consultaSql = string.Empty;
            SBO_KF_Configuracion_INFO KnSttings = new SBO_KF_Configuracion_INFO();
            SBO_KF_Configuracion_AD oControl = new SBO_KF_Configuracion_AD();
            Attachment = this.GetPDF();
            resultado = oControl.recuperaConfiguracion(ref Company, out consultaSql, ref KnSttings);
            if (resultado == "OK")
            {
                resultado = "ERROR";
                if (RecuperarCorreoNombre(EmpleadoID, out Nombre, out correoPara))
                {
                    String HTML = @"<html><head><title></title></head><body>" +
                                   "<p>Solicitud de orden de servicio</p>  " +
                                    "<h2>" + Nombre + " </h2>" +
                                    "<p> Se ha creado una solicitud de orden de servicio se envia documento de detalle de la solicitud como adjunto. </p>" +
                                    "<p>*Este es un correo enviado a través de un servicio de forma automatica, favor de no responderlo.*</p>" +
                                    "</body></html>";
                    resultado = GestionGlobales.Notificarcorreo(KnSttings, correoPara, "Solicitud de orden de servicio", HTML, Attachment);
                    if (resultado != "OK")
                        oHerramientas.ColocarMensaje(resultado, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                    else oHerramientas.ColocarMensaje("Se ha enviado la notificación de la orden de servicio al usuario " + Nombre, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                }

                else oHerramientas.ColocarMensaje("No se encontró el correo del empleado", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            }
        }

        private void EnviarNotificacion(int? EmpleadoID)
        {
            String Attachment = string.Empty, Asunto = string.Empty, resultado = string.Empty, correoPara = string.Empty, Nombre = string.Empty, consultaSql = string.Empty;
            SBO_KF_Configuracion_INFO KnSttings = new SBO_KF_Configuracion_INFO();
            SBO_KF_Configuracion_AD oControl = new SBO_KF_Configuracion_AD();
            resultado = oControl.recuperaConfiguracion(ref Company, out consultaSql, ref KnSttings);
            if (resultado == "OK")
            {
                resultado = "ERROR";
                this.ExportarPDF(out Attachment, false);
                if (RecuperarCorreoNombre(EmpleadoID, out Nombre, out correoPara))
                {
                    String HTML = @"<html><head><title></title></head><body>" +
                                   "<p>Solicitud de orden de servicio</p>  " +
                                    "<h2>" + Nombre + " </h2>" +
                                    "<p> Se ha creado una solicitud de orden de servicio se envia documento de detalle de la solicitud como adjunto. </p>" +
                                    "<p>*Este es un correo enviado a través de un servicio de forma automatica, favor de no responderlo.*</p>" +
                                    "</body></html>";

                    resultado = GestionGlobales.Notificarcorreo(KnSttings, correoPara, "Solicitud de orden de servicio", HTML, Attachment);
                    if (resultado != "OK")
                        oHerramientas.ColocarMensaje(resultado, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                    else oHerramientas.ColocarMensaje("Se ha enviado la notificación de la orden de servicio al usuario " + Nombre, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                }

                else oHerramientas.ColocarMensaje("No se encontró el correo del empleado", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
        }

        private Boolean RecuperarCorreoNombre(int? EmpleadoID, out String Nombre, out String Correo)
        {
            Boolean bContinuar = false;
            Nombre = string.Empty;
            Correo = string.Empty;

            try
            {
                String Empleado = @"SELECT U_Nombre, U_Correo FROM ""@VSKF_OPERADOR"" WHERE U_EmpleadoID = " + EmpleadoID;
                Recordset oRecordQuery = Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                oRecordQuery.DoQuery(Empleado);

                if (oRecordQuery.RecordCount > 0)
                {
                    oRecordQuery.MoveFirst();
                    Nombre = Convert.ToString(oRecordQuery.Fields.Item("U_Nombre").Value);
                    Correo = Convert.ToString(oRecordQuery.Fields.Item("U_Correo").Value);
                    bContinuar = true;
                }
                else bContinuar = false;

            }
            catch (Exception ex)
            {
                bContinuar = false;
                KananSAPHerramientas.LogError("OrdenServicioSinAlertaPresenter.cs->RecuperarCorreoNombre()", ex.Message);
            }


            return bContinuar;
        }
        private Boolean CreaFacturaCliente(OrdenServicio Orden, SBO_KF_Configuracion_INFO oSetting)
        {

            bool bContinua = false;
            string sresponse = "ERROR", consultaSql = string.Empty;
            int icommand = 0, iTransacc = -1;
            Documents oInvoices = (SAPbobsCOM.Documents)Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices);
            oInvoices.DocType = BoDocumentTypes.dDocument_Items;
            string comments = string.Format("Factura de cliente para orden de servicio, creada desde AddON KF en fecha {0}", DateTime.Now.ToString("dd/MM/yyyy"));
            oInvoices.CardCode = Orden.socioCliente;
            oInvoices.NumAtCard = string.Format("{0}/{1}", Orden.FolioOrden.Folio, Orden.FolioOrden.AnioFolio);
            oInvoices.DocDate = DateTime.Now;
            oInvoices.DocDueDate = DateTime.Now;
            oInvoices.TaxDate = DateTime.Now;
            oInvoices.Comments = comments.Length > 254 ? comments.Substring(0, 254) : comments;

            foreach (DetalleOrden oServicio in this.view.ListaServiciosAgregados)
            {
                oInvoices.Lines.ItemCode = oServicio.ItemCode;
                oInvoices.Lines.UnitPrice = Convert.ToDouble(oServicio.Costo);
                oInvoices.Lines.Quantity = 1;
                SBO_KF_SUCURSALES_AD oSucursal = new SBO_KF_SUCURSALES_AD();
                List<SBO_KF_SUCURSAL_INFO> lstsucursales = new List<SBO_KF_SUCURSAL_INFO>();
                sresponse = oSucursal.listaSucursales(ref this.Company, out consultaSql, ref lstsucursales, Convert.ToInt32(Orden.SucursalOrden.SucursalID));
                if (sresponse != "OK")
                    throw new Exception("No se encuentra el almacen para la factura de cliente");
                oInvoices.Lines.WarehouseCode = lstsucursales[0].U_ALMACEN;
                oInvoices.Lines.Add();
            }

            #region determina si la gestion es por SAP B1 y se da salida a refacciones
            if (oSetting.cSBO == 'Y' && oSetting.cAlmacen == 'Y')
            {
                foreach (DetalleRefaccion oTool in this.view.Refacciones)
                {
                    oInvoices.Lines.ItemCode = oTool.Articulo;
                    oInvoices.Lines.Quantity = Convert.ToDouble(oTool.Cantidad);
                    oInvoices.Lines.UnitPrice = Convert.ToDouble(oTool.Costo);
                    oInvoices.Lines.LineTotal = Convert.ToDouble(oTool.Total);
                    oInvoices.Lines.WarehouseCode = oTool.CodigoAlmacen;
                    oInvoices.Lines.Add();
                }
            }
            #endregion determina si la gestion es por SAP B1 y se da salida a refacciones
            iTransacc = oInvoices.Add();
            if (iTransacc != 0)
                Company.GetLastError(out icommand, out sresponse);
            else bContinua = true;

            return bContinua;
        }
        private Boolean CrearFactura(OrdenServicio Orden, bool bDraft)
        {
            #region Comentarios
            /*Metodo que genera la factura de la orden de servicio*/
            #endregion

            String sCentrocosto = string.Empty, consultaSql = string.Empty, response = string.Empty;
            Recordset oRecordQuery = Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            SBO_KF_Configuracion_INFO KnSttings = new SBO_KF_Configuracion_INFO();
            SBO_KF_Configuracion_AD oControl = new SBO_KF_Configuracion_AD();
            response = oControl.recuperaConfiguracion(ref Company, out consultaSql, ref KnSttings);
            Boolean bContinuar = false;

            if (response == "OK")
            {

                string CardCode = "";
                if (this.view.Entity.RecuperaProveedor(Orden.ProveedorServicio.ProveedorID, GestionGlobales.bSqlConnection, ref CardCode) != "OK")
                    throw new Exception("No se ha encontrado el proveedor");

                if (KnSttings.cSucursal == 'Y')
                {
                    oRecordQuery.DoQuery(@"SELECT ""Name"",U_CENTRO FROM ""@VSKF_SUCURSAL"" WHERE U_SucursalID = " + Orden.SucursalOrden.SucursalID);
                    //recuperar sucursal de lista sucursales activas
                    //si no se recupera sucursal o no existe enviar sucursal guardada en configuraciones
                    //Orden.SucursalOrden.SucursalID--->valor para obtener sucursal
                    if (oRecordQuery.RecordCount > 0)
                    {
                        oRecordQuery.MoveFirst();
                        sCentrocosto = Convert.ToString(oRecordQuery.Fields.Item("U_CENTRO").Value);
                    }
                    else
                        sCentrocosto = KnSttings.Sucursal;

                }
                else
                {
                    //sCentrocosto =
                    oRecordQuery.DoQuery(@"SELECT ""Name"" FROM ""@VSKF_VEHICULO"" WHERE U_VehiculoID =" + Orden.DetalleOrden[0].Mantenible.MantenibleID);
                    if (oRecordQuery.RecordCount > 0)
                    {
                        oRecordQuery.MoveFirst();
                        sCentrocosto = Convert.ToString(oRecordQuery.Fields.Item("Name").Value);
                    }
                    else sCentrocosto = KnSttings.Vehiculo;

                }

                #region validación para recuperar centro de costo en cliente
                if (KnSttings.cCliente == 'Y')
                {
                    try
                    {
                        string sql = string.Format(GestionGlobales.bSqlConnection ? "SELECT U_KF_CostCentro FROM OCRD WHERE CardCode = '{0}'" : @"SELECT U_KF_CostCentro FROM ""OCRD"" WHERE ""CardCode"" = '{0}' ", CardCode);
                        oRecordQuery.DoQuery(sql);
                        if (oRecordQuery.RecordCount == 0)
                        {
                            oRecordQuery.MoveFirst();
                            string sCostCenter = Convert.ToString(oRecordQuery.Fields.Item("U_KF_CostCentro").Value);
                            if (!string.IsNullOrEmpty(sCostCenter))
                                sCentrocosto = sCostCenter;
                        }
                    }
                    catch { }
                }
                #endregion validación para recuperar centro de costo en cliente


                //KnSttings.cVehiculo == 'Y' ? KnSttings.Vehiculo : KnSttings.Sucursal;
                try
                {
                    //Se obtiene el costo del formulario
                    //Decimal Costo = this.view.GetCostoForm();
                    bContinuar = this.view.Entity.CreateInvoice(Orden, GestionGlobales.bSqlConnection, sCentrocosto, KnSttings.DimCode, bDraft);
                    //oHerramientas.ColocarMensaje("Factura de proveedor creada exitosamente", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                }
                catch (Exception ex)
                {
                    // oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                    KananSAPHerramientas.LogError("OrdenServicioSinAlertaPresenter.cs->CrearFactura()", ex.Message);
                    bContinuar = false;
                }
            }
            return bContinuar;

        }

        #region DetalleOrden
        public void ManageDetalleOrden()
        {
            try
            {
                foreach (var svc in view.ListaServiciosAgregados)
                {
                    if (svc.DetalleOrdenID == null || svc.DetalleOrdenID <= 0)
                    {
                        this.view.EntityDetalle.oDetalleOrden = (DetalleOrden)svc.Clone();
                        this.view.EntityDetalle.oDetalleOrden.OrdenServicio.OrdenServicioID =
                            this.view.Entity.OrdenServicio.OrdenServicioID;
                        InsertDetalleOrden();
                    }
                    else
                    {
                        this.view.EntityDetalle.oUDT.GetByKey(svc.DetalleOrdenID.ToString());
                        this.view.EntityDetalle.UDTTooDetalleOrden();
                        this.view.EntityDetalle.oDetalleOrden = (DetalleOrden)svc.Clone();
                        UpdateDetalleOrden();
                    }
                    this.view.Entity.OrdenServicio.DetalleOrden.Add((DetalleOrden)this.view.EntityDetalle.oDetalleOrden.Clone());
                }
                //foreach (var svc in view.ListaServiciosAgregados)
                //{

                //}
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("OrdenServicioSinAlertaPresenter.cs->MergeDetalleOrden()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public void InsertDetalleOrden()
        {
            try
            {

                this.view.EntityDetalle.UUID = Guid.NewGuid();
                this.view.EntityDetalle.Sincronizado = 1;
                sIDTempAsignado = this.view.EntityDetalle.DetalleOrdenToUDT();
                this.view.EntityDetalle.Insert();

                this.view.EntityDetalle.oDetalleOrden = this.ordenWS.AddDetalleOrden(view.EntityDetalle.oDetalleOrden);


                this.view.EntityDetalle.Sincronizado = 0;
                this.view.EntityDetalle.ActualizarCode(view.EntityDetalle.oDetalleOrden, sIDTempAsignado);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("OrdenServicioSinAlertaPresenter.cs->InsertarDetalleOrden()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public void UpdateDetalleOrden()
        {
            try
            {

                this.view.EntityDetalle.DetalleOrdenToUDT();
                this.view.EntityDetalle.Actualizar();

                this.view.EntityDetalle.oDetalleOrden = this.ordenWS.UpdateDetalleOrden(view.EntityDetalle.oDetalleOrden);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("OrdenServicioSinAlertaPresenter.cs->UpdateDetalleOrden()", ex.Message);
                throw new Exception(ex.Message);
            }
        }
        #endregion

      

        #region Validaciones

        private bool ValidaDocumentoB1(OrdenServicio Orden, out string sDocEntry , bool bDraft, string TablaSAP)
        {
            sDocEntry = string.Empty;
            bool bContinuar = false;
            string sql = string.Format(@"SELECT ""DocEntry"" FROM ""{0}"" WHERE ""NumAtCard"" = '{1}/{2}' ",TablaSAP, Orden.FolioOrden.Folio, Orden.FolioOrden.AnioFolio);
            Recordset rs = Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            rs.DoQuery(sql);
            if (rs.RecordCount > 0)
            {
                bContinuar = true;
                sDocEntry = rs.Fields.Item("DocEntry").Value;
            }
            else bContinuar = false;
            return bContinuar;
        }

        private bool ValidaOrdenServicio()
        {
            string err = string.Empty;

            switch (this.view.estatus)
            {
                case 1:
                    err = this.view.ValidaSolicitudOrden();
                    break;
            }
            if (string.IsNullOrEmpty(err)) return true;
            this.SBO_Application.MessageBox(err, 1, "", "", "");
            return false;
        }

        private bool VerificarSiguienteEstatus()
        {
            bool Aprobado = true;

            if (EstatusActual == 1) //Solicitada
                if (EstatusSiguiente != 2 & EstatusSiguiente != 1 & EstatusSiguiente != 3 & EstatusSiguiente != 4)
                    Aprobado = false;

            if (EstatusActual == 2) //Aprobada
                if (EstatusSiguiente != 5 & EstatusSiguiente != 2)
                    Aprobado = false;

            if (EstatusActual == 3) //Rechazada
                if (EstatusSiguiente != 3 & EstatusSiguiente != 4)
                    Aprobado = false;

            if (EstatusActual == 4) //Eliminada
                if (EstatusSiguiente != 4)
                    Aprobado = false;

            if (EstatusActual == 5) //Aprobada
                if (EstatusSiguiente != 6 & EstatusSiguiente != 5)
                    Aprobado = false;

            if (EstatusActual == 6) //Atentida
                if (EstatusSiguiente != 6)
                    Aprobado = false;

            return Aprobado;
        }

        #endregion Validaciones

    }
}
