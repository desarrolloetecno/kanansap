﻿using System;
using KananSAP.GestionAlertas.Presenter;
using KananSAP.GestionUsuarios.Autentificacion;
using KananSAP.GestionVehiculos.CatalogoVehiculo;
using KananSAP.MenuItems.Presenter;
using KananSAP.SociosDeNegocios.GestionSociosNegocios.Presenter;
using KananWS.Interface;
using SAPbouiCOM;
using SAPbobsCOM;
using KananSAP.GestionVehiculos.CatalogoVehiculo.Presenter;
using KananSAP.GestionActivosFijos.Presenter;
/*Prueba módulo gestoría*/
using KananSAP.GestionVehiculos.GestoriaVehiculo.Presenter;
/*Migrador de llantas*/
using KananSAP.GestionDatos.MigradorDatos.Presenter;
using KananSAP.Helper;

using KananSAP.UnidadesMedida.Presenter;

namespace KananSAP
{
    public class MainKananFleet
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        public SAPbobsCOM.Company oCom;
        private EventFilter Filter;
        private Configurations configurations;
        
        ItemEvent qVal { get; set; }

        private CatalogoVehiculosPresenter veh_presenter;
        private ActivoFijoPresenter af_presenter;
        private GestionSociosNegociosPresenter bpartner_presenter;
        private UnidadesMedidaPresenter unid_presenter;
        private MenuPresenter menu_presenter;
        private AutentificacionPresenter auten_presenter;
        private AlertaPresenter alerta_presenter;
        private static VisorAlertaPesenter visal_presenter;
        //private OrdenServicioConAlertaPresenter OrSerConAl_Presenter;
        private GestoriaVehiculoPresenter ges_vehi;
        //private MigradorPresenter mig_lnt;
        public bool IsLogged;
        Helper.KananSAPHerramientas oHerramientas = new Helper.KananSAPHerramientas(null);
        private MainController controller_gestion;
        private GestionGlobales oGlobales = null;
        #endregion

        #region Constructor

        public MainKananFleet()
        {
            try
            {
                //System.Windows.Forms.MessageBox.Show("IsLogged");
                this.IsLogged = false;

                //System.Windows.Forms.MessageBox.Show("New Configuration");
                this.configurations = new Configurations();

                //System.Windows.Forms.MessageBox.Show("SetAplication()");
                SetApplication();

                //System.Windows.Forms.MessageBox.Show("SetCompany()");
                SetCompany();

                //System.Windows.Forms.MessageBox.Show("SetConfiguration");
                SetConfiguration();

                //System.Windows.Forms.MessageBox.Show("SetFilter");
                SetFilter();

                //System.Windows.Forms.MessageBox.Show("oCom = this.Company");
                this.oCom = this.Company;
                //SetTimeZone();
            }
            catch (Exception oError)
            {
                KananSAPHerramientas.LogError("MainKananFleet.cs->MainKananFleet()", oError.Message);
                System.Windows.Forms.MessageBox.Show(oError.Message);
                System.Environment.Exit(0);
            }

            //SBO_Application.MenuEvent += new SAPbouiCOM._IApplicationEvents_MenuEventEventHandler(SBO_Application_MenuEvent);
        }

        #endregion

        #region Eventos
        private void oAplicacionSBO_EventoControl(String FormUID, ref ItemEvent pVal, out bool BubbleEvent)
        {
            #region Comentarios
            /* Método:              Método que maneja eventos de controles de aplicación
             */
            #endregion Comentarios
            BubbleEvent = true;
            try
            {
                if (pVal.FormTypeEx == null)
                {
                    return;
                }

                if (this.configurations.UserFull == null && pVal.FormTypeEx != null && pVal.FormTypeEx != "")
                {
                    //KananSAP.Helper.KananSAPHerramientas.LogError("MenuPresenter.cs->SBO_Application_MenuEvent()", "No se obtuvieron valores de inicio de sesión. Es posible que la conexión a Internet sea mala o que no sea una instalación oficial del addonkananfleet.");
                    //oHerramientas.ColocarMensaje("No fue poible iniciar sesión, valide su conexión de Internet. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                    //return;
                }

                switch (pVal.FormTypeEx)
                {
                    //Formas de sistema  
                    case "LogginKF":
                        this.auten_presenter.SBO_Application_ItemEvent(FormUID, ref pVal, out BubbleEvent);
                        break;
                    case "10003":
                    case "GestCarga":
                    case "RepDet":
                    case "ConfSvForm":
                    case "LstServ":
                    case "frmScAdmon":
                        if (af_presenter.Foco)
                            this.af_presenter.SBO_Application_ItemEvent(FormUID, ref pVal, out BubbleEvent);
                        if (veh_presenter.Foco)
                            this.veh_presenter.SBO_Application_ItemEvent(FormUID, ref pVal, out BubbleEvent);
                        break;
                    //case "migrador":
                     //   this.mig_lnt.SBO_Application_ItemEvent(FormUID, ref pVal, out BubbleEvent);
                     //   break;
                    case "Gestoria":
                        if (veh_presenter.gestoria.view.Foco)
                            this.veh_presenter.gestoria.SBO_Application_ItemEvent(FormUID, ref pVal, out BubbleEvent);
                        break;
                    case "1473000075":
                        this.af_presenter.SBO_Application_ItemEvent(FormUID, ref pVal, out BubbleEvent);
                        this.veh_presenter.Foco = false;
                        break;
                    case "150":
                        this.veh_presenter.SBO_Application_ItemEvent(FormUID, ref pVal, out BubbleEvent);
                        this.af_presenter.Foco = false;
                        break;
                    case "188":
                    case "198":
                        this.alerta_presenter.SBO_Application_ItemEvent(FormUID, ref pVal, out BubbleEvent);
                        break;
                    case "frmConOS":
                    case "FrmAlerta":
                    case "frmAddOS":
                        visal_presenter.SBO_Application_ItemEvent(FormUID, ref pVal, out BubbleEvent);
                        break;
                    case "13000005":
                        this.unid_presenter.SBO_Application_ItemEvent(FormUID, ref pVal, out BubbleEvent);
                        break;
                }

                switch (pVal.FormType)
                {
                    case 134:
                        this.bpartner_presenter.SBO_Application_ItemEvent(FormUID, ref pVal, out BubbleEvent);
                        break;

                }
            }
            catch (Exception oError)
            {
                KananSAPHerramientas.LogError("MainKananFleet.cs->oAplicacionSBO_EventoControl()", oError.Message);
                this.oHerramientas.ColocarMensaje(oError.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error, false);
                
            }
        }

        /*
        private void DatosMaestrosKananFleet(String FormUIDD, ref ItemEvent pVall, out bool BubbleEventt)
        {
            BubbleEventt = true;
            if (pVall.FormTypeEx == null)
                return;

            switch (pVall.FormTypeEx)
            {
                //Formas de sistema  
                case "150":
                    this.veh_presenter.SBO_Application_ItemEvent(FormUIDD, ref pVall, out BubbleEventt);
                    break;
            }
        }*/

        private void oAplicacionSBO_EventoMenu(ref MenuEvent pVal, out bool BubbleEvent)
        {
            #region Comentarios
            /* Método:              Método que maneja eventos de menú
             */
            #endregion Comentarios

            Boolean bExito;

            Form oFormaActiva;
            ItemEvent fVal = qVal;

            String FormUID = "";

            BubbleEvent = true;

            try
            {
                if (pVal.BeforeAction)
                {
                    this.menu_presenter.SBO_Application_MenuEvent(ref pVal, out BubbleEvent);
                    if (SBO_Application.Forms.ActiveForm.Type == 150)
                    {
                        this.veh_presenter.SBO_Application_MenuEvent(ref pVal, out BubbleEvent);
                    }
                    /*if (pVal.MenuUID == "migrador")
                    {
                        this.mig_lnt.view.LoadForm();
                        this.mig_lnt.view.ShowForm();
                    }*/

                    /*Comentado debido a que se movió a botón en DMA. */
                    //if (pVal.MenuUID == "GVehiculo")
                    //    this.ges_vehi.view.ShowForm();
                    
                    /*
                    if (pVal.MenuUID == "150")
                    {
                        this.veh_presenter.SBO_Application_ItemEvent(FormUID, ref fVal, out BubbleEvent);
                    }
                    */
                }
                else
                {
                    try
                    {
                        oFormaActiva = this.SBO_Application.Forms.ActiveForm;
                        if (SBO_Application.Forms.ActiveForm.Type == 150)
                        {
                            this.veh_presenter.SBO_Application_MenuEvent(ref pVal, out BubbleEvent);
                        }
                    }
                    catch
                    {
                        return;
                    }
                }
            }
            catch (Exception oError)
            {
                KananSAPHerramientas.LogError("MainKananFleet.cs->oAplicacionSBO_EventoMenu()", oError.Message);
                this.oHerramientas.ColocarMensaje(oError.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error, false);
            }
        }
        private void oAplicacionSBO_EventoDatosForma(ref BusinessObjectInfo pVal, out bool BubbleEvent)
        {

            #region Comentarios
            /* Método:              Método que maneja eventos de form
             */
            #endregion Comentarios
            BubbleEvent = true;
            try
            {
                
                if (!pVal.BeforeAction)
                {
                    //switch (pVal.FormTypeEx)
                    //{
                        
                    //}
                   
                    //this.oHerramientas.ColocarMensaje("BEFORE" + pVal.EventType.ToString(), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error, false);
                }
                else
                {
                    //this.oHerramientas.ColocarMensaje("AFTER" + pVal.EventType.ToString(), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error, false);
                }
                
                
            }
            catch (Exception oError)
            {
                KananSAPHerramientas.LogError("MainKananFleet.cs->oAplicacionSBO_EventoDatosForma()", oError.Message);
                this.oHerramientas.ColocarMensaje(oError.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error, false);
            }

        }
        #endregion Eventos

        #region Metodos

        #region Conexion
        private void SetApplication()
        {
            /*Comentarios para sincronizar*/

            // *******************************************************************
            // Use an SboGuiApi object to establish the connection
            // with the application and return an initialized appliction object
            // *******************************************************************
            
                SAPbouiCOM.SboGuiApi SboGuiApi = null;
                string sConnectionString = null;

                SboGuiApi = new SAPbouiCOM.SboGuiApi();

                // by following the steps specified above, the following
                // statment should be suficient for either development or run mode
                try
                {
                    //System.Windows.Forms.MessageBox.Show("GetCommandLineArg().GetValue(1)");
                    sConnectionString = System.Convert.ToString(Environment.GetCommandLineArgs().GetValue(1));
                }
                catch 
                {
                    //System.Windows.Forms.MessageBox.Show("sConnectionString");
                   // sConnectionString = "0030002C0030002C00530041005000420044005F00440061007400650076002C0050004C006F006D0056004900490056";
                    sConnectionString = "0030002C0030002C00530041005000420044005F00440061007400650076002C0050004C006F006D0056004900490056";
                }

                // connect to a running SBO Application

                //System.Windows.Forms.MessageBox.Show("Connect");
                SboGuiApi.Connect(sConnectionString);

                // get an initialized application object

                SBO_Application = SboGuiApi.GetApplication(-1);
           
        
        }

        private void SetCompany()
        {
            try
            {
                //System.Windows.Forms.MessageBox.Show("SAPbobsCOM.Company()");
                Company = new SAPbobsCOM.Company();

                //System.Windows.Forms.MessageBox.Show("GetDICompany()");
                Company = SBO_Application.Company.GetDICompany();

                //System.Windows.Forms.MessageBox.Show("DBType()");
                bool bSQLConnection = Company.DbServerType.ToString().Contains("MSSQL") ? true : false;

                //System.Windows.Forms.MessageBox.Show("GestionGlobales");
                oGlobales = new GestionGlobales(bSQLConnection, Company, SBO_Application,this.configurations);
            }
            catch (Exception ex)
            {
                //System.Windows.Forms.MessageBox.Show("Catch - SetCompany");
                KananSAPHerramientas.LogError("MainKananFleet.cs->SetCompany()", ex.Message);
                Company = SBO_Application.Company.GetDICompany();
                bool bSQLConnection = Company.DbServerType.ToString().Contains("MSSQL") ? true : false;
                oGlobales = new GestionGlobales(bSQLConnection, Company, SBO_Application, this.configurations);
            }
        }

        private void SetFilter()
        {
            //System.Windows.Forms.MessageBox.Show("New EventFilter");
            this.Filter = new EventFilter(this.SBO_Application);
            //System.Windows.Forms.MessageBox.Show("Filter.SetFilter()");
            this.Filter.SetFilter();
        }
        #endregion

        #region LogIn

        private void LogIn()
        {
            try
            {
                configurations = this.auten_presenter.LogInCurrentSAPUserToKF();
                if (configurations.UserFull.Usuario.UsuarioID.HasValue)
                    this.IsLogged = true;
                else
                    throw new Exception("El usuario SAP no se ha podido autentificar como usuario Kananfleet");
            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError("MainKananFleet.cs->LogIn()", ex.Message);
                this.SBO_Application.StatusBar.SetText(ex.Message + " - Los módulos de Kananfleet estarán deshabilitados", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
            }
        }

        #endregion

        #region Configuration

        private void SetConfiguration()
        {
            try
            {
                this.auten_presenter = new AutentificacionPresenter(this.SBO_Application, this.Company);

                this.LogIn();

                bool isSQL = Company.DbServerType.ToString().Contains("MSSQL") ? true : false;

                this.menu_presenter = new MenuPresenter(this.SBO_Application, this.Company, this.configurations, IsLogged, this.oGlobales)//, isSQL)
                {
                    autentificacionView = this.auten_presenter
                };

                this.controller_gestion = new MainController(this.configurations, oGlobales);

                this.veh_presenter = new CatalogoVehiculosPresenter(this.oGlobales,this.SBO_Application, this.Company, this.configurations) {IsLogged = IsLogged};
                this.af_presenter = new ActivoFijoPresenter(this.SBO_Application, this.Company, this.configurations, this.oGlobales) { IsLogged = IsLogged };

                /*Prueba módulo gestoría*/
                this.ges_vehi = new GestoriaVehiculoPresenter(this.SBO_Application, this.Company, this.configurations, new Kanan.Vehiculos.BO2.Vehiculo()) { IsLogged = IsLogged};

                /*Migrador de llantas*/
                //this.mig_lnt = new MigradorPresenter(this.SBO_Application, this.Company, this.configurations);

                this.bpartner_presenter = new GestionSociosNegociosPresenter(this.SBO_Application, this.Company, this.configurations) { IsLogged = IsLogged };

                this.alerta_presenter = new AlertaPresenter(this.SBO_Application, this.Company, this.configurations) { IsLogged = IsLogged };

                visal_presenter = new VisorAlertaPesenter(this.SBO_Application, this.Company, this.configurations, this.IsLogged, this.oGlobales);

                //OrSerConAl_Presenter = new OrdenServicioConAlertaPresenter(this.SBO_Application, this.Company, this.configurations);

                this.unid_presenter = new UnidadesMedidaPresenter(this.SBO_Application, this.Company, this.configurations) { IsLogged = IsLogged };

                this.oHerramientas = new Helper.KananSAPHerramientas(this.SBO_Application);

                //Se agregan los eventos

                this.SBO_Application.MenuEvent += new _IApplicationEvents_MenuEventEventHandler(oAplicacionSBO_EventoMenu);
                this.SBO_Application.ItemEvent += new _IApplicationEvents_ItemEventEventHandler(oAplicacionSBO_EventoControl);
                //this.SBO_Application.ItemEvent += new _IApplicationEvents_ItemEventEventHandler(DatosMaestrosKananFleet);
                this.SBO_Application.FormDataEvent += new _IApplicationEvents_FormDataEventEventHandler(oAplicacionSBO_EventoDatosForma);
                
            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError("MainKananFleet.cs->SetConfiguration()", ex.Message);
                new KananSAPHerramientas(this.SBO_Application).ColocarMensaje(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            }

        }
        #endregion

        /*#region TimeZone
        private void SetTimeZone()
        {
            NodaTimeHelper.Services.DateTimeHelper Utc = new NodaTimeHelper.Services.DateTimeHelper(configurations.UserFull.Configs["TimeZone"]);
        }
        #endregion*/

        #endregion
    }
}
