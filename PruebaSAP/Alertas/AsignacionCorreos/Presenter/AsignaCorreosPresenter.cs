﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KananSAP.AsignacionCorreos.Views;
using KananWS.Interface;
using SAPbouiCOM;

namespace KananSAP.AsignacionCorreos.Presenter
{
    public class AsignaCorreosPresenter
    {
        #region Atributo
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private SAPbouiCOM.Item oItem;
        public AsignaCorreosView view { get; set; }
        private Configurations configs;
        private NotificacionesWS notiws;
        private bool Modal;
        private Helper.KananSAPHerramientas oHerramientas;
        #endregion

        #region Constructor
        public AsignaCorreosPresenter(SAPbouiCOM.Application aplication, SAPbobsCOM.Company company, Configurations c)
        {
            this.configs = c;
            this.SBO_Application = aplication;
            this.Company = company;
            this.view = new AsignaCorreosView(this.SBO_Application, this.Company, c);
            this.notiws = new NotificacionesWS(Guid.Parse(this.configs.UserFull.Usuario.PublicKey), this.configs.UserFull.Usuario.PrivateKey);
            this.Modal = false;
            SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
            this.oHerramientas = new Helper.KananSAPHerramientas(this.SBO_Application);
        }
        #endregion

        #region Eventos
        private void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (pVal.FormTypeEx == "AsignMail")
                {
                    #region beforeaction
                    if (pVal.BeforeAction)
                    {
                        #region Empresa
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "rademp")
                        {
                            this.view.GridShowOrHide("grdsuc", false);
                            this.view.GridShowOrHide("grdveh", false);
                            this.view.GridEnabled("grdsuc", false);
                            this.view.GridEnabled("grdveh", false);
                            this.view.GetNotificacion(1);
                            this.view.EntityToForm();
                            BubbleEvent = false;
                        }
                        #endregion

                        #region Sucursal
                        if (pVal.ItemUID == "grdsuc" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.view.SetSucRowIndex(pVal.Row);
                            this.view.GetNotificacion(3);
                            this.view.EntityToForm();
                            BubbleEvent = false;
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "radsuc")
                        {
                            this.view.GridShowOrHide("grdsuc", true);
                            this.view.GridShowOrHide("grdveh", false);
                            this.view.GridEnabled("grdsuc", true);
                            this.view.GridEnabled("grdveh", false);
                            this.view.EmptyEntityToForm();
                            BubbleEvent = false;
                        }
                        #endregion

                        #region Vehiculo

                        if (pVal.ItemUID == "grdveh" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.view.SetVehRowIndex(pVal.Row);
                            this.view.GetNotificacion(2);
                            this.view.EntityToForm();
                            BubbleEvent = false;
                        }
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "radveh")
                        {
                            this.view.GridShowOrHide("grdsuc", false);
                            this.view.GridShowOrHide("grdveh", true);
                            this.view.GridEnabled("grdsuc", false);
                            this.view.GridEnabled("grdveh", true);
                            this.view.EmptyEntityToForm();
                            BubbleEvent = false;
                        }
                        #endregion

                        #region Asignar
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnasigna")
                        {
                            this.view.ValidateData();
                            if (this.view.Entity.Notificacion.NotificacionID == null)
                            {
                                this.Insertar();
                            }
                            else
                            {
                                this.Update();
                            }
                            BubbleEvent = false;
                        }
                        #endregion
                    }
                    #endregion

                    #region After Action
                    else
                    {
                        //if (Modal && pVal.EventType == BoEventTypes.et_FORM_ACTIVATE)
                        //{
                        //    userview.ShowForm();
                        //    BubbleEvent = false;
                        //    return;
                        //}
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                BubbleEvent = false;
                KananSAP.Helper.KananSAPHerramientas.LogError("AsignaCorreosPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
                oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            }
        }
        #endregion

        #region Metodos

        #region Acciones
        private void GuardarCargaCombustible()
        {
            try
            {
                //if (string.IsNullOrEmpty(cargaview.GetCargaID().Trim()))
                //{
                //     Insertar();
                //}
                //else
                //{
                //    Update();
                //}
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AsignaCorreosPresenter.cs->GuardarCargaCombustible()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void Update()
        {
            try
            {
                this.view.Entity.Sincronizado = 2;
                this.view.Entity.ActualizarCode(view.Entity.Notificacion);

                this.view.Entity.Notificacion = this.notiws.Actualizar(view.Entity.Notificacion);

                this.view.Entity.Sincronizado = 0;
                this.view.Entity.NotificacionToUDT();
                this.view.Entity.UpdateUDT();

                this.oHerramientas.ColocarMensaje("Operación exitosa", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AsignaCorreosPresenter.cs->Update()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void Insertar()
        {
            try
            {
                this.view.Entity.Notificacion.NotificacionID = null;
                this.view.Entity.UUID = Guid.NewGuid();
                this.view.Entity.Sincronizado = 1;
                this.view.Entity.NotificacionToUDT();
                this.view.Entity.InsertUDT();

                this.view.Entity.Notificacion = this.notiws.Insertar(this.view.Entity.Notificacion);

                this.view.Entity.Sincronizado = 0;
                this.view.Entity.ActualizarCode(view.Entity.Notificacion);
                this.SBO_Application.MessageBox("Configuración guardada correctamente");
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("AsignaCorreosPresenter.cs->Insertar()", ex.Message);
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Metodos Auxiliares
        #endregion
        #endregion
    }
}