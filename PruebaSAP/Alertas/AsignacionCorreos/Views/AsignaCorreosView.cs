﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kanan.Comun.BO2;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using KananSAP.Alertas.Data;
using KananSAP.Operaciones.Data;
using KananSAP.Vehiculos.Data;
using KananWS.Interface;
using SAPbobsCOM;
using SAPbouiCOM;
using Application = System.Windows.Forms.Application;

namespace KananSAP.AsignacionCorreos.Views
{
    public class AsignaCorreosView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.OptionBtn oOptionBtn;
        private SAPbouiCOM.Button oButton;
        private SAPbouiCOM.Grid oGrid;
        private string name;
        public NotificacionesSAP Entity { get; set; }
        public SucursalSAP ESucursal { get; set; }
        public VehiculoSAP EVehiculo { get; set; }
        public string FormUniqueID { get; set; }
        private XMLPerformance XmlApplication;
        public List<Sucursal> listSucursal;
        public List<Vehiculo> listVehiculo;
        public int? SucRowIndex { get; set; }
        public int? VehRowIndex { get; set; }
        public Configurations configs;

        #endregion

        #region Constructor
        public AsignaCorreosView(SAPbouiCOM.Application Application, SAPbobsCOM.Company company, Configurations c)
        {
            this.Entity = new NotificacionesSAP(company);
            this.ESucursal = new SucursalSAP(company);
            this.EVehiculo = new VehiculoSAP(company);
            this.XmlApplication = new XMLPerformance(Application);
            this.SBO_Application = Application;
            this.oItem = null;
            this.oEditText = null;
            this.oGrid = null;
            this.oOptionBtn = null;
            this.oForm = null;
            listSucursal = new List<Sucursal>();
            listVehiculo = new List<Vehiculo>();
            this.configs = c;
        }
        #endregion

        #region Metodos

        #region FormMethods
        public void SetForm(string type, int count)
        {
            try
            {
                if (count > 1)
                {
                    this.oForm = SBO_Application.Forms.GetForm(type, count);
                    this.oForm.Close();
                    this.oForm = SBO_Application.Forms.GetForm(type, 1);
                    this.oForm.Select();
                    this.FormUniqueID = this.oForm.UniqueID;
                }
                else
                {
                    this.oForm = SBO_Application.Forms.GetForm(type, count);
                    this.FormUniqueID = this.oForm.UniqueID;
                }
            }
            catch (Exception ex)
            {
                this.oForm = null;
                throw new Exception(ex.Message);
            }
        }

        public void LoadForm()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("AsignMail");
            }
            catch
            {
                string sPath = Application.StartupPath;                
                this.XmlApplication.LoadFromXML(sPath + "\\XML","AsignaMailAlertas.xml");
                this.oForm = SBO_Application.Forms.Item("AsignMail");

                oItem = this.oForm.Items.Item("radsuc");
                oOptionBtn = (SAPbouiCOM.OptionBtn)(oItem.Specific);
                oOptionBtn.GroupWith("rademp");

                oItem = this.oForm.Items.Item("radveh");
                oOptionBtn = (SAPbouiCOM.OptionBtn)(oItem.Specific);
                oOptionBtn.GroupWith("radsuc");

                oItem = this.oForm.Items.Item("rademp");
                oOptionBtn = (SAPbouiCOM.OptionBtn)(oItem.Specific);
                oOptionBtn.Selected = true;

                GetListSucursales();
                GetListVehiculos();
                FillSucursalesGridFromList();
                FillVehiculosGridFromList();

                GridShowOrHide("grdsuc", false);
                GridShowOrHide("grdveh", false);
                GridEnabled("grdsuc", false);
                GridEnabled("grdveh", false);

                //this.GetNotificacion(1);
                //this.EntityToForm();

                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
            }
        }

        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
            }
            catch (Exception)
            {
                this.LoadForm();
                this.oForm.Visible = true;
                this.oForm.Select();
            }
        }

        public void CloseForm()
        {
            if (this.oForm.Visible & this.oForm.Selected)
                this.oForm.Close();
        }

        public void GridShowOrHide(string gridname, bool showorhide)
        {
            oGrid = this.oForm.Items.Item(gridname).Specific;
            oGrid.Item.Visible = showorhide;
        }

        public void GridEnabled(string gridname, bool enable)
        {
            oGrid = this.oForm.Items.Item(gridname).Specific;
            oGrid.Item.Enabled = enable;
            oGrid.SelectionMode = enable ? BoMatrixSelect.ms_Single : BoMatrixSelect.ms_None;
        }

        #region Entity
        public void FormToEntity()
        {
            this.oForm = SBO_Application.Forms.Item("AsignMail");

            oItem = this.oForm.Items.Item("txtmail");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.Notificacion.Correos = oEditText.String;

            oItem = this.oForm.Items.Item("rademp");
            oOptionBtn = (SAPbouiCOM.OptionBtn)(oItem.Specific);
            if (oOptionBtn.Selected)
            {
                this.Entity.Notificacion.Notificador = new Empresa { EmpresaID = configs.UserFull.Dependencia.EmpresaID };
            }

            oItem = this.oForm.Items.Item("radsuc");
            oOptionBtn = (SAPbouiCOM.OptionBtn)(oItem.Specific);
            if (oOptionBtn.Selected)
            {
                if (this.SucRowIndex == null)
                {
                    throw new Exception("Se debe seleccionar una sucursal");
                }
                GetSucursalFromGrid();
                this.Entity.Notificacion.Notificador = ESucursal.Sucursal;
            }

            oItem = this.oForm.Items.Item("radveh");
            oOptionBtn = (SAPbouiCOM.OptionBtn)(oItem.Specific);
            if (oOptionBtn.Selected)
            {
                if (this.VehRowIndex == null)
                {
                    throw new Exception("Se debe seleccionar un Vehículo");
                }
                GetVehiculoFromGrid();
                this.Entity.Notificacion.Notificador = EVehiculo.vehiculo;
            }
        }

        public void EntityToForm()
        {
            this.oForm = SBO_Application.Forms.Item("AsignMail");

            oItem = this.oForm.Items.Item("txtmail");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = this.Entity.Notificacion.Correos;
        }

        public void EmptyEntityToForm()
        {
            oItem = this.oForm.Items.Item("txtmail");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;
        }

        public void ValidateData()
        {
            try
            {
                this.FormToEntity();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            if (this.Entity.Notificacion.Notificador.NotificableID == null)
            {
                throw new Exception("No se seleccionó objeto al cual asignar destinatarios");
            }
            if (string.IsNullOrEmpty(this.Entity.Notificacion.Correos))
            {
                throw new Exception("El valor 'Destinatarios' es obligatorio");
            }
            if(VerificarCorreo(this.Entity.Notificacion.Correos) == false)
            {
                throw new Exception("El valor 'Destinatarios' debe ser un correo");
            }
        }

        public void GetNotificacion(int Tipo)
        {
            try
            {
                this.Entity.Sincronizado = null;
                this.Entity.UUID = null;
                Recordset rs;
                switch (Tipo)
                {
                    case 1:
                        {
                            rs = this.Entity.Consultar(new Notificacion { Notificador = new Empresa { EmpresaID = this.configs.UserFull.Dependencia.EmpresaID } });
                            break;
                        }
                    case 2:
                        {
                            this.GetVehiculoFromGrid();
                            rs = this.Entity.Consultar(new Notificacion { Notificador = new Vehiculo() { VehiculoID = this.EVehiculo.vehiculo.VehiculoID } });
                            break;
                        }
                    case 3:
                        {
                            this.GetSucursalFromGrid();
                            rs = this.Entity.Consultar(new Notificacion { Notificador = new Sucursal { SucursalID = this.ESucursal.Sucursal.SucursalID } });
                            break;
                        }
                    default:
                        throw new Exception("Tipo de notificación incorrecto");
                        break;
                }
                this.Entity.Notificacion = rs.RecordCount > 0 ? this.Entity.RecordSetToListNotificacion(rs).FirstOrDefault() : new Notificacion();
                if (this.Entity.oUDT.GetByKey(this.Entity.Notificacion.NotificacionID.ToString()))
                {
                    this.Entity.UDTToNotificacion();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region Sucursales
        public void FillSucursalesGridFromList()
        {
            try
            {
                DataTable tab = oForm.DataSources.DataTables.Item("DT_Suc");
                oGrid = this.oForm.Items.Item("grdsuc").Specific;
                tab.Rows.Clear();
                int index = 0;
                foreach (Sucursal suc in listSucursal)
                {
                    tab.Rows.Add();
                    tab.SetValue("colsucid", index, suc.SucursalID ?? 0);
                    tab.SetValue("colnombre", index, suc.Descripcion);
                    tab.SetValue("colestado", index, suc.Estado);
                    tab.SetValue("colciudad", index, suc.Ciudad);
                    index++;
                }
                oGrid.DataTable = tab;
                oGrid.Columns.Item("colsucid").Visible = false;

                var title = oGrid.Columns.Item("colnombre").TitleObject;
                title.Caption = "Nombre";
                title = oGrid.Columns.Item("colestado").TitleObject;
                title.Caption = "Estado";
                title = oGrid.Columns.Item("colciudad").TitleObject;
                title.Caption = "Ciudad";

                oGrid.SelectionMode = BoMatrixSelect.ms_Single;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void GetListSucursales()
        {
            ESucursal.Sincronizado = null;
            ESucursal.UUID = null;
            var rs = ESucursal.Consultar(new Sucursal());
            this.listSucursal = ESucursal.RecordSetToListSucursal(rs);
        }

        public void GetSucursalFromGrid()
        {
            try
            {
                if (this.SucRowIndex != null && this.SucRowIndex > -1)
                {
                    oGrid = this.oForm.Items.Item("grdsuc").Specific;
                    int sucid = Convert.ToInt32(oGrid.DataTable.Columns.Item("colsucid").Cells.Item((int)this.SucRowIndex).Value);
                    //if (listobservacion != null && listobservacion.Count > 0 && obsid != 0)
                    if (listSucursal != null && listSucursal.Count > 0)
                    {
                        bool exist = listSucursal.Exists(svc => svc.SucursalID == sucid);
                        if (exist)
                        {
                            this.ESucursal.Sucursal = this.listSucursal.FirstOrDefault(svc => svc.SucursalID == sucid);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SetSucRowIndex(int rowIndex)
        {
            try
            {
                this.SucRowIndex = rowIndex;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region Vehiculos
        public void FillVehiculosGridFromList()
        {
            try
            {
                DataTable tab = oForm.DataSources.DataTables.Item("DT_Veh");
                oGrid = this.oForm.Items.Item("grdveh").Specific;
                tab.Rows.Clear();
                int index = 0;
                foreach (Vehiculo veh in listVehiculo)
                {
                    tab.Rows.Add();
                    tab.SetValue("colvehid", index, veh.VehiculoID ?? 0);
                    tab.SetValue("colname", index, veh.Nombre);
                    tab.SetValue("colmarca", index, veh.Marca);
                    tab.SetValue("colmodelo", index, veh.Modelo);
                    tab.SetValue("colplaca", index, veh.Placa);
                    index++;
                }
                oGrid.DataTable = tab;

                oGrid.Columns.Item("colvehid").Visible = false;

                var title = oGrid.Columns.Item("colname").TitleObject;
                title.Caption = "No. Económico";
                title = oGrid.Columns.Item("colmarca").TitleObject;
                title.Caption = "Marca";
                title = oGrid.Columns.Item("colmodelo").TitleObject;
                title.Caption = "Modelo";
                title = oGrid.Columns.Item("colplaca").TitleObject;
                title.Caption = "Placa";

                oGrid.SelectionMode = BoMatrixSelect.ms_Single;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void GetListVehiculos()
        {
            EVehiculo.Sincronizado = null;
            EVehiculo.UUID = null;
            EVehiculo.ItemCode = null;
            var rs = EVehiculo.ConsultarSimple(new Vehiculo());
            this.listVehiculo = EVehiculo.RecordSetToListVehiculos(rs);
        }

        public void GetVehiculoFromGrid()
        {
            try
            {
                if (this.VehRowIndex != null && this.VehRowIndex > -1)
                {
                    oGrid = this.oForm.Items.Item("grdveh").Specific;
                    int vehid = Convert.ToInt32(oGrid.DataTable.Columns.Item("colvehid").Cells.Item((int)this.VehRowIndex).Value);
                    //if (listobservacion != null && listobservacion.Count > 0 && obsid != 0)
                    if (listVehiculo != null && listVehiculo.Count > 0)
                    {
                        bool exist = listVehiculo.Exists(svc => svc.VehiculoID == vehid);
                        if (exist)
                        {
                            this.EVehiculo.vehiculo = this.listVehiculo.FirstOrDefault(svc => svc.VehiculoID == vehid);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void SetVehRowIndex(int rowIndex)
        {
            try
            {
                this.VehRowIndex = rowIndex;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #endregion
        #endregion

        #region ItemsMethods
        #endregion

        #region Metodos Auxiliares

        public bool VerificarCorreo(String correo)
        {
            bool estado = false;
            char [] arreglo = correo.ToCharArray();

            for (int i = 0; i < correo.Count(); i++ )
            {
                if (arreglo[i] == '@')
                    estado = true;
            }
            return estado;
        }

        #endregion

        #endregion
    }
}