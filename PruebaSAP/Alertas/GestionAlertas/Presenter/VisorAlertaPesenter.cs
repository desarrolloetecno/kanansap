using Kanan.Mantenimiento.BO2;
using Kanan.Vehiculos.BO2;
using KananSAP.Alertas.Data;
using KananSAP.GestionAlertas.View;
using KananSAP.Helpers;
using KananSAP.Mantenimiento.Data;
using KananWS.Interface;

using KananSAP.AdministrarOrdenesServicio.Views;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;
using KananSAP.Configuracion.Configuraciones.Objects;
using KananSAP.Helpers;
using KananSAP.Helper;

using SAPbouiCOM;
using SAPbobsCOM;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPinterface.Data.INFO.Tablas;
using SAPinterface.Data.AD;
using KananFleet.OrdenesServicio.SolicitarOrdenServicio.Views;

namespace KananSAP.GestionAlertas.Presenter
{
    public class VisorAlertaPesenter
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private EventFilter Filter;
        private Configurations configurations;
        public GestionAlertas.View.VisorAlertaView view;
        public bool IsLogged {get; set;}
        private bool isModal { get; set; }

        public OrdenServicioConAlertaView viewOS;
        public AdministraOSView viewlist { get; set; }
        Helper.KananSAPHerramientas oHerramientas;
        private KananSAP.Mantenimiento.Data.MotorMantenimientoData motorData;
        private SBO_KF_SolicitaOrdenesView oSolicitaOS = null;
        private OrdenServicio OrdenServicioReturned { get; set; }
        private KananWS.Interface.OrdenServicioWS ordenWS;
        private int _itemTemp;
        private int ItemTemp { get { return this._itemTemp; } set { this._itemTemp = value; } }
        private bool ModalOrden;
        private GestionGlobales oGlobal = null;
        string sIDTempAsignado = "";
        Recordset rs;
        
        //Se crean variables est�ticas de Estatus para validar que no se puedan saltar procesos.
        private int? EstatusActual;
        private int? EstatusSiguiente;

        #endregion

        #region Constructor
        public VisorAlertaPesenter(SAPbouiCOM.Application sApplication, SAPbobsCOM.Company Cmpny, Configurations c, bool isloggin, GestionGlobales oGlobales)
        {
            try
            {
                this.oGlobal = oGlobales;
                this.IsLogged = isloggin;
                if (!IsLogged)
                    return;
                this.SBO_Application = sApplication;
                this.Company = Cmpny;
                this.configurations = c;
                this.view = new GestionAlertas.View.VisorAlertaView(this.SBO_Application, this.Company);
                this.view.Configs = this.configurations;
                this.isModal = false;
                this.viewOS = new OrdenServicioConAlertaView(this.SBO_Application, this.Company, this.configurations);
                this.viewlist = new AdministraOSView(this.SBO_Application, this.Company);
                this.oHerramientas = new Helper.KananSAPHerramientas(this.SBO_Application);
                ModalOrden = false;
                motorData = new Mantenimiento.Data.MotorMantenimientoData(this.Company);

                this.ordenWS = new OrdenServicioWS(Guid.Parse(this.configurations.UserFull.Usuario.PublicKey), this.configurations.UserFull.Usuario.PrivateKey);
                
                rs = (Recordset)Company.GetBusinessObject(BoObjectTypes.BoRecordset);

            }
            catch(Exception ex)
            { throw new Exception(ex.Message); }
        }

        #endregion

        #region ItemEvent
        public void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            if (!IsLogged) return;
            string FileName = string.Empty;
            string group = string.Empty;
            try
            {
                #region Visor de Alerta
                if (pVal.FormTypeEx == "FrmAlerta")
                {
                    if (pVal.BeforeAction)
                    {
                        if (pVal.EventType == BoEventTypes.et_DOUBLE_CLICK && pVal.ItemUID == "mtxAlertas")
                        {
                            if (this.view.HasRowValue(pVal.Row))
                                oSolicitaOS = new SBO_KF_SolicitaOrdenesView(this.oGlobal, this.configurations, ref this.Company, "beep", "alerta", AlertaID: this.view.DevuelveAlertaID());
                        }

                        if (pVal.EventType == BoEventTypes.et_MATRIX_LINK_PRESSED && pVal.ItemUID == "mtxAlertas")
                        {
                            if (this.view.HasRowValue(pVal.Row))
                                oSolicitaOS = new SBO_KF_SolicitaOrdenesView(this.oGlobal, this.configurations, ref this.Company, "beep", "alerta", AlertaID: this.view.DevuelveAlertaID());
                            /*if (this.view.HasRowValue(pVal.Row))
                            {
                                int VHID = this.view.GetVehiculoID();
                                int SVID = this.view.GetServicioID();

                                #region Validacones
                                bool ExisteVehiculo = this.view.ExisteVehiculo(VHID);
                                if (ExisteVehiculo != true)
                                {
                                    oHerramientas.ColocarMensaje("Veh�culo no existe. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                    this.view.DesactivarAlerta();
                                    return;
                                }

                                bool ExisteServicio = this.view.ExisteServicio(SVID);
                                if (ExisteServicio != true)
                                {
                                    oHerramientas.ColocarMensaje("Servicio no existe. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                    this.view.DesactivarAlerta();
                                    return;
                                }
                                #endregion Validaciones

                                viewOS.ShowForm(1, false);

                                this.viewOS.EntityDetalle.oDetalleOrden = new DetalleOrden() { Servicio = new Servicio(), OrdenServicio = new OrdenServicio() };
                                this.viewOS.EntityDetalle.oDetalleOrden.TipoMantenibleID = 1;
                                this.viewOS.EntityDetalle.oDetalleOrden.Mantenible = new Vehiculo();
                                (this.viewOS.EntityDetalle.oDetalleOrden.Mantenible as Vehiculo).VehiculoID = VHID;
                                this.viewOS.EntityDetalle.oDetalleOrden.Servicio.ServicioID = SVID;
                                this.viewOS.EntityDetalle.oDetalleOrden.Costo = 0;
                                this.viewOS.EntityDetalle.oDetalleOrden.OrdenServicio.OrdenServicioID = null;
                                this.viewOS.ListaServiciosAgregados.Add((DetalleOrden)this.viewOS.EntityDetalle.oDetalleOrden.Clone());
                                this.viewOS.GetDetalleOrdenInfo();

                                if (VHID > 0 & SVID > 0)
                                {
                                    this.viewOS.FillGridServiciosFromList();
                                }
                                else
                                {
                                    oHerramientas.ColocarMensaje("Detalle de alerta no encontrada. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                }
                                return;
                            }*/
                        }
                    }
                }
                #endregion

                #region
                if (pVal.FormTypeEx == "frmConOS")
                {
                    #region BeforeAction

                    #region OrdenServicio
                    if (pVal.BeforeAction)
                    {
                        #region ChooseFromList
                        ///se agrega evento de seleccion de datos por lista al componente refacciones
                        if (pVal.EventType == BoEventTypes.et_CHOOSE_FROM_LIST)
                        {
                            SAPbouiCOM.IChooseFromListEvent oCFLEvento = null;
                            oCFLEvento = ((SAPbouiCOM.IChooseFromListEvent)(pVal));
                            string sCFL_ID = null;
                            sCFL_ID = oCFLEvento.ChooseFromListUID;
                            SAPbouiCOM.Form oForm = null;
                            oForm = SBO_Application.Forms.Item(FormUID);
                            SAPbouiCOM.ChooseFromList oCFL = null;
                            oCFL = oForm.ChooseFromLists.Item(sCFL_ID);
                            if (oCFLEvento.BeforeAction == false)
                            {
                                SAPbouiCOM.DataTable oDataTable = null;
                                oDataTable = oCFLEvento.SelectedObjects;
                                string val = null;
                                try
                                {
                                    val = System.Convert.ToString(oDataTable.GetValue(0, 0));
                                    oForm.DataSources.UserDataSources.Item("EditDS").ValueEx = val;
                                }
                                catch (Exception ex)
                                {
                                    throw new Exception("Error en evento selecci�n de datos por lista al componente refacci�nes. " + ex.Message);
                                }
                            }
                        }
                        #endregion ChooseFromList

                        #region comboMPOSv
                        if (pVal.EventType == BoEventTypes.et_COMBO_SELECT && pVal.ItemUID == "comboMPOSv")
                        {
                            if (this.viewOS.isEfectivo())
                                this.viewOS.HiddeDetallePago();
                            else
                                this.viewOS.ShowDetallePago();
                        }
                        #endregion comboMPOSv

                        #region gdOS
                        if ((pVal.ItemUID == "gdOS" || pVal.ItemUID == "gdRFa") && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            BubbleEvent = false;
                            this.viewOS.SetRowIndex(pVal.Row, pVal.ItemUID);
                        }
                        #endregion gdOS

                        #region cmbestatus
                        if (pVal.ItemUID == "cmbestatus" && pVal.EventType == BoEventTypes.et_COMBO_SELECT)
                        {
                            this.viewOS.FormToEntity();
                            int? flag = this.viewOS.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID;
                            if (flag == 5)
                            {
                                this.viewOS.bOCultarProveedor(true);
                                this.viewOS.MostrarFacturaci�n(true);
                            }
                            else if (flag == 6)
                            {
                                //this.viewOS.MostrarFacturaci�n(true);
                            }
                            else
                            {
                                this.viewOS.bOCultarProveedor(false);
                                this.viewOS.MostrarFacturaci�n(false);
                            }
                        }
                        #endregion cmbestatus

                        #region btnPDF
                        if (pVal.ItemUID == "btnPDF" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            if (this.viewOS.ListaServiciosAgregados.Count > 0)
                            {
                                #region Cuardar
                                BubbleEvent = false;
                                //Se obtiene el status al momento de lanzar el formulario
                                this.EstatusActual = this.viewOS.estatus;
                                this.viewOS.FormToEntity();
                                this.EstatusSiguiente = this.viewOS.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID;
                                int? flag = this.viewOS.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID;
                                bool bContinuar = true;

                                #region ValidarOrdenEstatus
                                //Verificar seguimiento de estatus.
                                if (EstatusActual != null & EstatusSiguiente != null)
                                {
                                    bContinuar = VerificarSiguienteEstatus();

                                    //Lanza excepcion por mal definir el estatus.
                                    if (bContinuar == false)
                                    {
                                        throw new Exception("Orden de estatus incorrecta. ");
                                    }
                                    else
                                    {
                                        DateTime FechaActual = DateTime.Now;
                                        this.viewOS.Entity.InsertCambioEstatus(EstatusSiguiente, FechaActual, this.viewOS.Entity.OrdenServicio.OrdenServicioID);
                                    }
                                }
                                else
                                {
                                    bContinuar = false;
                                    throw new Exception("Error interno. Error al intentar obtener estatus. ");
                                }
                                #endregion ValidarOrdenEstatus

                                #region RecharzarOrdenServicio
                                //Cuando una orden rechazada
                                if (flag == 3)
                                {
                                    this.viewOS.Entity.EstatusRechazada(Convert.ToInt32(this.viewOS.Entity.OrdenServicio.OrdenServicioID));
                                    bContinuar = false;
                                    this.viewOS.CloseForm();

                                    //Se limpia y se cargan nuevamente todas las ordenes para visualizar el nuevo estatus
                                    this.viewlist.listordenes.Remove(
                                            this.viewlist.listordenes.FirstOrDefault(
                                                x => x.OrdenServicioID == this.viewOS.Entity.OrdenServicio.OrdenServicioID));
                                    this.viewlist.listordenes.Add((OrdenServicio)this.viewOS.Entity.OrdenServicio.Clone());
                                    this.viewlist.FillOrdenesGridFromList();

                                    return;
                                }
                                #endregion RecharzarOrdenServicio

                                #region EliminarOrdenServicio
                                //Cuando una orden es eliminada
                                if (flag == 4)
                                {
                                    this.viewOS.Entity.EstatusEliminar(Convert.ToInt32(this.viewOS.Entity.OrdenServicio.OrdenServicioID));
                                    bContinuar = false;
                                    this.viewOS.CloseForm();

                                    //Se limpia y se cargan nuevamente todas las ordenes para visualizar el nuevo estatus
                                    this.viewlist.listordenes.Remove(
                                            this.viewlist.listordenes.FirstOrDefault(
                                                x => x.OrdenServicioID == this.viewOS.Entity.OrdenServicio.OrdenServicioID));
                                    this.viewlist.listordenes.Add((OrdenServicio)this.viewOS.Entity.OrdenServicio.Clone());
                                    this.viewlist.FillOrdenesGridFromList();

                                    return;
                                }
                                #endregion EliminarOrdenServicio

                                #region Facturar Estatus 5
                                if (flag == 5)
                                {
                                    bContinuar = this.viewOS.Entity.ExisteFactura(Convert.ToInt32(this.viewOS.Entity.OrdenServicio.OrdenServicioID));

                                    if (bContinuar == false)
                                    {
                                        oHerramientas.ColocarMensaje("La orden de servicio debe facturarse antes. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                        return;
                                    }

                                    /*Se asigna la fecha de entrada real al taller*/
                                    this.viewOS.Entity.OrdenServicio.FechaRecepcionReal = DateTime.Today;
                                }
                                #endregion Facturar Estatus 5

                                #region Fecha SalidaRealTaller
                                if (flag == 6)
                                {
                                    /*Se asigna la fecha de salida real del taller*/
                                    this.viewOS.Entity.OrdenServicio.FechaLiberacionReal = DateTime.Today;
                                }
                                #endregion Fecha SalidaRealTaller

                                #region RecumperaPDF
                                if (bContinuar)
                                {
                                    String pathFile = Directorio();

                                    if (string.IsNullOrEmpty(pathFile))
                                        oHerramientas.ColocarMensaje("No se ha determinado el directorio de PDF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);

                                    pathFile = string.Format(@"{0}\OrdenServicio", pathFile);
                                    if (!Directory.Exists(pathFile))
                                    {
                                        /*Pedir al usuario definir el direcctorio en caso de no existir el directorio definido. */
                                        //Directory.CreateDirectory(pathFile);
                                        oHerramientas.ColocarMensaje("Ruta no existe en el equipo actual, favor de definir una ruta existente para todos los equipos que comparten el servidor. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                        return;
                                    }
                                    else
                                    {
                                        Directory.CreateDirectory(pathFile);
                                    }

                                    string Ubicaci�_PDF = string.Format(@"{0}\{1}_{2}.pdf", pathFile, this.viewOS.Entity.OrdenServicio.FolioOrden.Folio, this.viewOS.Entity.OrdenServicio.FolioOrden.AnioFolio);

                                    if (File.Exists(Ubicaci�_PDF))
                                    {
                                        /*Se elimina anterior para poder mostrar el nuevo reporte con los datos actualizados. */
                                        File.Delete(Ubicaci�_PDF);
                                    }

                                    /*Se exporte el PDF. */
                                    ExportarPDF(out FileName, true);
                                }
                                #endregion RecumperaPDF

                                #region ActualizarOrdenServicio
                                if (bContinuar)
                                {
                                    if (this.viewOS.Entity.OrdenServicio.OrdenServicioID != null || this.viewOS.Entity.OrdenServicio.OrdenServicioID <= 0)
                                    {
                                        this.ActualizarOrdenes();
                                        this.viewOS.CloseForm();
                                        this.viewlist.listordenes.Remove(
                                            this.viewlist.listordenes.FirstOrDefault(
                                                x => x.OrdenServicioID == this.viewOS.Entity.OrdenServicio.OrdenServicioID));
                                        this.viewlist.listordenes.Add((OrdenServicio)this.viewOS.Entity.OrdenServicio.Clone());
                                        /*Se maneja el try-catch para evitar problemas de limpiado de IDs.*/
                                        try
                                        {
                                            this.viewlist.FillOrdenesGridFromList();
                                        }
                                        catch
                                        {

                                        }
                                        //this.viewlist.AplyFilter();
                                    }
                                    else
                                    {
                                        if (this.ValidaOrdenServicio())
                                        {
                                            if (this.InsertarOrdenes())
                                            {
                                                if (flag == 1)
                                                    //condicion que determina que es una solicitud de orden de servicio
                                                    this.EnviarNotificacion(this.viewOS.Entity.OrdenServicio.AprobadorOrdenServicio.EmpleadoID);
                                            }
                                            this.viewOS.EmptyEntityToForm(); ///linea que limpia formulario de orden de servicio
                                        }
                                    }

                                    #region Actualizando par�metros de servicios
                                    //M�todo para validar actualizar par�metros
                                    if (this.viewOS.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID == 5)
                                    {
                                        #region InstanciandoObjetos
                                        int? VHID;
                                        int? SVID;
                                        int? TVHID;
                                        double? valor;
                                        double? alerta;
                                        int? valorTiempo;
                                        int? alertaTiempo;
                                        decimal? Odometro;
                                        string Sentencia = "";
                                        VehiculoWS vhWS = new VehiculoWS(Guid.Parse(this.configurations.UserFull.Usuario.PublicKey), this.configurations.UserFull.Usuario.PrivateKey);
                                        AlertaMantenimiento alMto = new AlertaMantenimiento();
                                        List<AlertaMantenimiento> ListalMto = new List<AlertaMantenimiento>();
                                        AlertaMantenimientoSAP alCtl = new AlertaMantenimientoSAP(this.Company);

                                        DetalleOrden detalleOrden = new DetalleOrden();
                                        List<DetalleOrden> ListDO = new List<DetalleOrden>();

                                        ParametroServicio PS = new ParametroServicio()
                                        {
                                            Servicio = new Servicio()
                                        };
                                        List<ParametroServicio> ListPS = new List<ParametroServicio>()
                                        {
                                        };
                                        ParametroServicioData PSCtl = new ParametroServicioData(this.Company);

                                        ParametroMantenimiento PM = new ParametroMantenimiento();
                                        List<ParametroMantenimiento> ListPM = new List<ParametroMantenimiento>();
                                        ParametroMantenimientoData PMCtl = new ParametroMantenimientoData(this.Company);
                                        #endregion

                                        //ListDO = this.viewOS.EntityDetalle.RecordSetToListDetalleOrden(rs, detalleOrden);
                                        for (int i = 0; i < this.viewOS.Entity.OrdenServicio.DetalleOrden.Count; i++)
                                        {
                                            #region Obteniendo identificadores
                                            VHID = this.viewOS.Entity.OrdenServicio.DetalleOrden[i].Mantenible.MantenibleID != null ? VHID = this.viewOS.Entity.OrdenServicio.DetalleOrden[i].Mantenible.MantenibleID : VHID = null;
                                            TVHID = this.viewOS.Entity.OrdenServicio.DetalleOrden[i].TipoMantenibleID != null ? TVHID = this.viewOS.Entity.OrdenServicio.DetalleOrden[i].TipoMantenibleID : TVHID = null;
                                            SVID = this.viewOS.Entity.OrdenServicio.DetalleOrden[i].Servicio.ServicioID != null ? SVID = this.viewOS.Entity.OrdenServicio.DetalleOrden[i].Servicio.ServicioID : SVID = null;
                                            string OdoTemp = vhWS.GetOdometro(Convert.ToInt32(VHID));
                                            Odometro = Convert.ToDecimal(OdoTemp);
                                            #endregion

                                            #region DesactivarAlertas
                                            //Se eliminan las alertas en caso de que la alerta conincida con el servicio, el vehiculo y el tipo de veh�culo que
                                            //se registr� en la orden de servicio.
                                            Sentencia = string.Format(@"select * from ""@VSKF_ALERTAMTTO"" where ""U_MANTENIBLEID"" = '{0}' and ""U_TIPOMANTENIBLEID"" = '{1}' and ""U_SERVICIOID"" = '{2}'", VHID, TVHID, SVID);
                                            rs.DoQuery(Sentencia);
                                            alCtl.AlertaMantenimiento = new AlertaMantenimiento()
                                            {
                                                Mantenible = new Vehiculo()
                                            };
                                            ListalMto = alCtl.HashoAlertaMto(rs) ? ListalMto = alCtl.RecordSetToListAlertaMantenimiento(rs) : ListalMto = new List<AlertaMantenimiento>();

                                            //Se verigica que si existe una alerta para este veh�culo con ese servicio y se elimina
                                            //  en caso de que exista por ser aprobada la solicitud de orden de servicio.
                                            for (int h = 0; h < ListalMto.Count; h++)
                                            {
                                                if (ListalMto[h].Mantenible.MantenibleID == VHID && ListalMto[h].servicio.ServicioID == SVID)
                                                {
                                                    string SentenciaEliminarAlerta;
                                                    //Se desactiva registro padre.
                                                    SentenciaEliminarAlerta = string.Format(@"update ""@VSKF_ALERTA"" set ""U_ESTAACTIVO"" = '{0}' where ""U_IALERTAID"" = '{1}'", 0, ListalMto[h].AlertaMantenimientoID);
                                                    rs.DoQuery(SentenciaEliminarAlerta);
                                                    //Se desactiva registro hijo.
                                                    /*SentenciaEliminarAlerta = string.Format("update from [@VSKF_ALERTAMTTO] where U_IAlertaID = '{0}'", ListalMto[h].AlertaMantenimientoID);
                                                    rs.DoQuery(SentenciaEliminarAlerta);*/
                                                }
                                            }
                                            #endregion

                                            #region Actualizar par�metros
                                            Sentencia = null;
                                            Sentencia = string.Format(@"select * from ""@VSKF_PMTROSERVICIO"" where ""U_MANTBLEID"" = '{0}' and ""U_SERVICIOID"" = '{1}' and U_TMANTBLEID = '{2}'", VHID, SVID, TVHID);
                                            rs.DoQuery(Sentencia);
                                            ListPS = PSCtl.HashParametroServicio(rs) ? ListPS = PSCtl.RecordSetToListOS(rs) : ListPS = new List<ParametroServicio>();

                                            Sentencia = null;
                                            Sentencia = string.Format(@"select * from ""@VSKF_PMTROMTTO"" where ""U_MANTBLEID"" = '{0}' and ""U_SERVICIOID"" = '{1}' and ""U_MANTBLEID"" = '{2}'", VHID, SVID, TVHID);
                                            rs.DoQuery(Sentencia);
                                            ListPM = PMCtl.HasParametroMantenimiento(rs) ? ListPM = PMCtl.RecordSetToListOS(rs) : ListPM = new List<ParametroMantenimiento>();

                                            for (int n = 0; n < ListPM.Count; n++)
                                            {
                                                string Sentenciainsertar = null;
                                                DateTime? UltServiTiemp = this.viewOS.Entity.OrdenServicio.Fecha;
                                                decimal? proxserviKM = Odometro + Convert.ToDecimal(ListPS[n].Valor);
                                                TimeSpan? intervaloTiempo = ListPM[n].ProximoServicioTiempo.Value - ListPM[n].UltimoServicioTiempo.Value;
                                                int IntervaloDias = intervaloTiempo.Value.Days;
                                                DateTime? proxservTiemp = DateTime.Today.AddDays(IntervaloDias);

                                                //Actualizando par�metros por distancia
                                                //Se verifica que exista definici�n de par�metros por Kilometraje.
                                                if (ListPS[n].Valor > 0)
                                                {
                                                    Sentenciainsertar = null;
                                                    Sentenciainsertar = string.Format(@"update ""@VSKF_PMTROMTTO"" set ""U_ULTSERV"" = '{0}', ""U_PROXSERV"" = '{1}' where ""U_PMTOID"" = '{2}'", Odometro, proxserviKM, ListPM[n].ParametroMantenimientoID);
                                                    rs.DoQuery(Sentenciainsertar);
                                                }

                                                //Actualizando par�metros tiempo
                                                //Se verifica que exista definici�n de par�metros por tiempo.
                                                if (IntervaloDias > 0)
                                                {
                                                    Sentenciainsertar = null;
                                                    Sentenciainsertar = string.Format(@"update ""@VSKF_PMTROMTTO"" set ""U_ULSVTIEM"" = '{0}', ""U_PROXSVTIEM"" = '{1}' where ""U_PMTOID"" = '{2}'", DateTime.Today.ToString("yyyy-MM-dd HH:mm:ss"), proxservTiemp.Value.ToString("yyyy-MM-dd HH:mm:ss"), ListPM[n].ParametroMantenimientoID);
                                                    rs.DoQuery(Sentenciainsertar);
                                                }

                                                //Se pregunta si existe alg�n veh�culo relacionado con el servicio registrado en la roden de servicio.
                                                // Sentencia = string.Format("update from [@VSKF_PMTROSERVICIO] where ");
                                                //rs.DoQuery(Sentencia);
                                                //ListPS = PSCtl.HashParametroServicio(rs) ? ListPS = PSCtl.RecordSetToList(rs) : ListPS = new List<ParametroServicio>();
                                            }
                                            #endregion
                                        }
                                    }
                                    #endregion
                                }
                                else
                                    throw new Exception("Favor de agreagar refacciones a la orden de servicio");
                                #endregion ActualizarOrdenServicio
                                #endregion Guardar
                            }
                            else
                            {
                                BubbleEvent = false;
                                oHerramientas.ColocarMensaje("Proporciona informaci�n antes de generar el PDF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                            }
                        }
                        #endregion btnPDF

                        #region Facturacion
                        if (pVal.ItemUID == "oInvoice" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            /*Preparar facturaci�n*/
                            bool run = true;
                            BubbleEvent = false;
                            this.EstatusActual = this.viewOS.estatus;
                            this.viewOS.FormToEntity();
                            this.EstatusSiguiente = this.viewOS.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID;

                            /*Validar estatus de la orden*/
                            #region ValidarOrdenEstatus
                            //Verificar seguimiento de estatus.
                            if (EstatusActual != null & EstatusSiguiente != null)
                            {
                                run = VerificarSiguienteEstatus();

                                //Lanza excepcion por mal definir el estatus.
                                if (run == false)
                                {
                                    oHerramientas.ColocarMensaje("Orden de estatus incorrecta. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                    return;
                                }
                            }
                            else
                            {
                                run = false;
                                oHerramientas.ColocarMensaje("Error interno. Error al intentar obtener estatus. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                return;
                            }
                            #endregion ValidarOrdenEstatus

                            /*Validar que la factura no se haya creado anteriormente*/
                            run = this.ValidarFacturacion(Convert.ToInt32(this.viewOS.Entity.OrdenServicio.OrdenServicioID));
                            if (run == false)
                            {
                                oHerramientas.ColocarMensaje("Factura se ha creado anteriormente. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                return;
                            }
                            /*Se valida que el estatus de la orden sea de Atendida*/
                            int? flag = this.viewOS.GetEstatus();
                            if (flag == 5)
                            {
                                /*Validar que la Orden de Servicio tenga servicios agregados.
                                 Las refacciones no son obligatorias.*/

                                //run = this.viewOS.Refacciones.Count > 0 ? true : false;
                                run = this.viewOS.ListaServiciosAgregados.Count > 0 ? true : false;
                                if (run)
                                {
                                    /*Se solicita confirmaci�n de facturaci�n*/
                                    System.Windows.Forms.DialogResult respuestaUsuario;
                                    respuestaUsuario = System.Windows.Forms.MessageBox.Show("La factura no puede ser modifinada/eliminada posterior a ser creada. ", "�Confirmar facturaci�n? ", System.Windows.Forms.MessageBoxButtons.YesNo);
                                    if (respuestaUsuario == System.Windows.Forms.DialogResult.Yes)
                                    {
                                       // run = CrearFactura(this.viewOS.Entity.OrdenServicio, );
                                    }

                                    if (!run)
                                    {
                                        oHerramientas.ColocarMensaje("Error al crear la factura, validar datos. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                                        return;
                                    }
                                    else
                                    {
                                        /*Se actualiza el estatus de la orden*/
                                        this.viewOS.Entity.EstatusRealizada(Convert.ToInt32(this.viewOS.Entity.OrdenServicio.OrdenServicioID));

                                        /*Se selecciona la opcion "Atendida" del combo*/
                                        //this.viewOS.ComboEstatusRealizada();

                                        /*Se recarga la Orden de Servicio*/
                                        //this.viewOS.LoadForm(this.viewOS.GetEstatus(), true);

                                        /*Se notifica al usuario que la factura se ha creado*/
                                        oHerramientas.ColocarMensaje("Factura creada. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

                                        //this.viewOS.CloseForm();
                                    }
                                }
                                else
                                {
                                    oHerramientas.ColocarMensaje("Debe existir al menos un servicio relacinado con la Orden de Servicio para poder facturar. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                }
                            }
                            else
                            {
                                BubbleEvent = false;
                                oHerramientas.ColocarMensaje("La Orden de Servicio debe tener estatus Atendida. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                            }
                        }
                        #endregion Facturacion

                        #region btnSaveOS
                        if (pVal.ItemUID == "btnSaveOS" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            BubbleEvent = false;
                            //Se obtiene el status al momento de lanzar el formulario
                            this.EstatusActual = this.viewOS.estatus;
                            this.viewOS.FormToEntity();
                            this.EstatusSiguiente = this.viewOS.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID;
                            int? flag = this.viewOS.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID;
                            bool bContinuar = true;

                            #region ValidarOrdenEstatus
                            //Verificar seguimiento de estatus.
                            if (EstatusActual != null & EstatusSiguiente != null)
                            {
                                bContinuar = VerificarSiguienteEstatus();

                                //Lanza excepcion por mal definir el estatus.
                                if (bContinuar == false)
                                {
                                    throw new Exception("Orden de estatus incorrecta. ");
                                }
                                else
                                {
                                    DateTime FechaActual = DateTime.Now;
                                    this.viewOS.Entity.InsertCambioEstatus(EstatusSiguiente, FechaActual, this.viewOS.Entity.OrdenServicio.OrdenServicioID);
                                }
                            }
                            else
                            {
                                bContinuar = false;
                                throw new Exception("Error interno. Error al intentar obtener estatus. ");
                            }
                            #endregion ValidarOrdenEstatus

                            #region RecharzarOrdenServicio
                            //Cuando una orden rechazada
                            if (flag == 3)
                            {
                                this.viewOS.Entity.EstatusRechazada(Convert.ToInt32(this.viewOS.Entity.OrdenServicio.OrdenServicioID));
                                bContinuar = false;
                                this.viewOS.CloseForm();

                                //Se limpia y se cargan nuevamente todas las ordenes para visualizar el nuevo estatus
                                this.viewlist.listordenes.Remove(
                                        this.viewlist.listordenes.FirstOrDefault(
                                            x => x.OrdenServicioID == this.viewOS.Entity.OrdenServicio.OrdenServicioID));
                                this.viewlist.listordenes.Add((OrdenServicio)this.viewOS.Entity.OrdenServicio.Clone());
                                this.viewlist.FillOrdenesGridFromList();

                                return;
                            }
                            #endregion RecharzarOrdenServicio

                            #region EliminarOrdenServicio
                            //Cuando una orden es eliminada
                            if (flag == 4)
                            {
                                this.viewOS.Entity.EstatusEliminar(Convert.ToInt32(this.viewOS.Entity.OrdenServicio.OrdenServicioID));
                                bContinuar = false;
                                this.viewOS.CloseForm();

                                //Se limpia y se cargan nuevamente todas las ordenes para visualizar el nuevo estatus
                                this.viewlist.listordenes.Remove(
                                        this.viewlist.listordenes.FirstOrDefault(
                                            x => x.OrdenServicioID == this.viewOS.Entity.OrdenServicio.OrdenServicioID));
                                this.viewlist.listordenes.Add((OrdenServicio)this.viewOS.Entity.OrdenServicio.Clone());
                                this.viewlist.FillOrdenesGridFromList();

                                return;
                            }
                            #endregion EliminarOrdenServicio

                            #region Facturar Estatus 5
                            if (flag == 5)
                            {
                                bContinuar = this.viewOS.Entity.ExisteFactura(Convert.ToInt32(this.viewOS.Entity.OrdenServicio.OrdenServicioID));

                                if (bContinuar == false)
                                {
                                    oHerramientas.ColocarMensaje("La orden de servicio debe facturarse antes. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                    return;
                                }

                                /*Se asigna la fecha de entrada real al taller*/
                                this.viewOS.Entity.OrdenServicio.FechaRecepcionReal = DateTime.Today;
                            }
                            #endregion Facturar Estatus 5

                            #region Fecha SalidaRealTaller
                            if (flag == 6)
                            {
                                /*Se asigna la fecha de salida real del taller*/
                                this.viewOS.Entity.OrdenServicio.FechaLiberacionReal = DateTime.Today;
                            }
                            #endregion Fecha SalidaRealTaller

                            //Se maneja facturaci�n en boton oInvoice
                            #region Comentado
                            //if (flag == 5)
                            //{
                            //guardar entrada de proveedor
                            /*bContinuar = this.viewOS.Refacciones.Count > 0 ? true : false;
                            if (bContinuar)
                            {
                                bContinuar = CrearFactura(this.viewOS.Entity.OrdenServicio);
                                if (!bContinuar)
                                    return;
                            }*/
                            //}
                            #endregion Comentado

                            #region ActualizarOrdenServicio
                            if (bContinuar)
                            {
                                if (this.viewOS.Entity.OrdenServicio.OrdenServicioID != null || this.viewOS.Entity.OrdenServicio.OrdenServicioID <= 0)
                                {
                                    this.ActualizarOrdenes();
                                    this.viewOS.CloseForm();
                                    this.viewlist.listordenes.Remove(
                                        this.viewlist.listordenes.FirstOrDefault(
                                            x => x.OrdenServicioID == this.viewOS.Entity.OrdenServicio.OrdenServicioID));
                                    this.viewlist.listordenes.Add((OrdenServicio)this.viewOS.Entity.OrdenServicio.Clone());
                                    this.viewlist.FillOrdenesGridFromList();
                                    //this.viewlist.AplyFilter();
                                }
                                else
                                {
                                    if (this.ValidaOrdenServicio())
                                    {
                                        if (this.InsertarOrdenes())
                                        {
                                            if (flag == 1)
                                                //condicion que determina que es una solicitud de orden de servicio
                                                this.EnviarNotificacion(this.viewOS.Entity.OrdenServicio.AprobadorOrdenServicio.EmpleadoID);
                                        }
                                        this.viewOS.EmptyEntityToForm(); ///linea que limpia formulario de orden de servicio
                                    }
                                }

                                #region Actualizando par�metros de servicios
                                //M�todo para validar actualizar par�metros
                                if (this.viewOS.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID == 5)
                                {
                                    #region InstanciandoObjetos
                                    int? VHID;
                                    int? SVID;
                                    int? TVHID;
                                    double? valor;
                                    double? alerta;
                                    int? valorTiempo;
                                    int? alertaTiempo;
                                    decimal? Odometro;
                                    string Sentencia = "";
                                    VehiculoWS vhWS = new VehiculoWS(Guid.Parse(this.configurations.UserFull.Usuario.PublicKey), this.configurations.UserFull.Usuario.PrivateKey);
                                    AlertaMantenimiento alMto = new AlertaMantenimiento();
                                    List<AlertaMantenimiento> ListalMto = new List<AlertaMantenimiento>();
                                    AlertaMantenimientoSAP alCtl = new AlertaMantenimientoSAP(this.Company);

                                    DetalleOrden detalleOrden = new DetalleOrden();
                                    List<DetalleOrden> ListDO = new List<DetalleOrden>();

                                    ParametroServicio PS = new ParametroServicio()
                                    {
                                        Servicio = new Servicio()
                                    };
                                    List<ParametroServicio> ListPS = new List<ParametroServicio>()
                                    {
                                    };
                                    ParametroServicioData PSCtl = new ParametroServicioData(this.Company);

                                    ParametroMantenimiento PM = new ParametroMantenimiento();
                                    List<ParametroMantenimiento> ListPM = new List<ParametroMantenimiento>();
                                    ParametroMantenimientoData PMCtl = new ParametroMantenimientoData(this.Company);
                                    #endregion

                                    //ListDO = this.viewOS.EntityDetalle.RecordSetToListDetalleOrden(rs, detalleOrden);
                                    for (int i = 0; i < this.viewOS.Entity.OrdenServicio.DetalleOrden.Count; i++)
                                    {
                                        #region Obteniendo identificadores
                                        VHID = this.viewOS.Entity.OrdenServicio.DetalleOrden[i].Mantenible.MantenibleID != null ? VHID = this.viewOS.Entity.OrdenServicio.DetalleOrden[i].Mantenible.MantenibleID : VHID = null;
                                        TVHID = this.viewOS.Entity.OrdenServicio.DetalleOrden[i].TipoMantenibleID != null ? TVHID = this.viewOS.Entity.OrdenServicio.DetalleOrden[i].TipoMantenibleID : TVHID = null;
                                        SVID = this.viewOS.Entity.OrdenServicio.DetalleOrden[i].Servicio.ServicioID != null ? SVID = this.viewOS.Entity.OrdenServicio.DetalleOrden[i].Servicio.ServicioID : SVID = null;
                                        string OdoTemp = vhWS.GetOdometro(Convert.ToInt32(VHID));
                                        Odometro = Convert.ToDecimal(OdoTemp);
                                        #endregion

                                        #region DesactivarAlertas
                                        //Se eliminan las alertas en caso de que la alerta conincida con el servicio, el vehiculo y el tipo de veh�culo que
                                        //se registr� en la orden de servicio.
                                        Sentencia = string.Format(@"select * from ""@VSKF_ALERTAMTTO"" where ""U_MANTENIBLEID"" = '{0}' and ""U_TIPOMANTENIBLEID"" = '{1}' and ""U_SERVICIOID"" = '{2}'", VHID, TVHID, SVID);
                                        rs.DoQuery(Sentencia);
                                        alCtl.AlertaMantenimiento = new AlertaMantenimiento()
                                        {
                                            Mantenible = new Vehiculo()
                                        };
                                        ListalMto = alCtl.HashoAlertaMto(rs) ? ListalMto = alCtl.RecordSetToListAlertaMantenimiento(rs) : ListalMto = new List<AlertaMantenimiento>();

                                        //Se verigica que si existe una alerta para este veh�culo con ese servicio y se elimina
                                        //  en caso de que exista por ser aprobada la solicitud de orden de servicio.
                                        for (int h = 0; h < ListalMto.Count; h++)
                                        {
                                            if (ListalMto[h].Mantenible.MantenibleID == VHID && ListalMto[h].servicio.ServicioID == SVID)
                                            {
                                                string SentenciaEliminarAlerta;
                                                //Se desactiva registro padre.
                                                SentenciaEliminarAlerta = string.Format(@"update ""@VSKF_ALERTA"" set ""U_ESTAACTIVO"" = '{0}' where ""U_IALERTAID"" = '{1}'", 0, ListalMto[h].AlertaMantenimientoID);
                                                rs.DoQuery(SentenciaEliminarAlerta);
                                                //Se desactiva registro hijo.
                                                /*SentenciaEliminarAlerta = string.Format("update from [@VSKF_ALERTAMTTO] where U_IAlertaID = '{0}'", ListalMto[h].AlertaMantenimientoID);
                                                rs.DoQuery(SentenciaEliminarAlerta);*/
                                            }
                                        }
                                        #endregion

                                        #region Actualizar par�metros
                                        Sentencia = null;
                                        Sentencia = string.Format("select * from [@VSKF_PMTROSERVICIO] where U_MantbleID = '{0}' and U_ServicioID = '{1}' and U_TMantbleID = '{2}'", VHID, SVID, TVHID);
                                        rs.DoQuery(Sentencia);
                                        ListPS = PSCtl.HashParametroServicio(rs) ? ListPS = PSCtl.RecordSetToListOS(rs) : ListPS = new List<ParametroServicio>();

                                        Sentencia = null;
                                        Sentencia = string.Format("select * from [@VSKF_PMTROMTTO] where U_MantbleID = '{0}' and U_ServicioID = '{1}' and U_TMantbleID = '{2}'", VHID, SVID, TVHID);
                                        rs.DoQuery(Sentencia);
                                        ListPM = PMCtl.HasParametroMantenimiento(rs) ? ListPM = PMCtl.RecordSetToListOS(rs) : ListPM = new List<ParametroMantenimiento>();

                                        for (int n = 0; n < ListPM.Count; n++)
                                        {
                                            string Sentenciainsertar = null;
                                            DateTime? UltServiTiemp = this.viewOS.Entity.OrdenServicio.Fecha;
                                            decimal? proxserviKM = Odometro + Convert.ToDecimal(ListPS[n].Valor);
                                            TimeSpan? intervaloTiempo = ListPM[n].ProximoServicioTiempo.Value - ListPM[n].UltimoServicioTiempo.Value;
                                            int IntervaloDias = intervaloTiempo.Value.Days;
                                            DateTime? proxservTiemp = DateTime.Today.AddDays(IntervaloDias);

                                            //Actualizando par�metros por distancia
                                            //Se verifica que exista definici�n de par�metros por Kilometraje.
                                            if (ListPS[n].Valor > 0)
                                            {
                                                Sentenciainsertar = null;
                                                Sentenciainsertar = string.Format("update [@VSKF_PMTROMTTO] set U_UltServ = '{0}', U_ProxServ = '{1}' where U_PMtoID = '{2}'", Odometro, proxserviKM, ListPM[n].ParametroMantenimientoID);
                                                rs.DoQuery(Sentenciainsertar);
                                            }

                                            //Actualizando par�metros tiempo
                                            //Se verifica que exista definici�n de par�metros por tiempo.
                                            if (IntervaloDias > 0)
                                            {
                                                Sentenciainsertar = null;
                                                Sentenciainsertar = string.Format("update [@VSKF_PMTROMTTO] set U_UlSvTiem = '{0}', U_ProxSvTiem = '{1}' where U_PMtoID = '{2}'", DateTime.Today.ToString("yyyy-MM-dd HH:mm:ss"), proxservTiemp.Value.ToString("yyyy-MM-dd HH:mm:ss"), ListPM[n].ParametroMantenimientoID);
                                                rs.DoQuery(Sentenciainsertar);
                                            }
                                        }
                                        #endregion
                                    }
                                }
                                #endregion
                            }
                            else
                                throw new Exception("Favor de agreagar refacciones a la orden de servicio");
                            #endregion ActualizarOrdenServicio

                            this.viewOS.CloseForm();
                        }
                        #endregion btnSaveOS

                        #region btnCanOS
                        if (pVal.ItemUID == "btnCanOS" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            BubbleEvent = false;
                            this.SBO_Application.Forms.Item("frmConOS").Close();
                            this.viewlist.OSRowIndex = null;
                        }
                        #endregion btnCanOS

                        #region DetalleRefacciones
                        if (pVal.ItemUID == "btnRefn" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            String CadenaRef = string.Empty;
                            BubbleEvent = this.viewOS.FormValidaRefaccion(out CadenaRef);
                            if (BubbleEvent)
                            {
                                this.viewOS.FormToListRefaccion();
                                BubbleEvent = this.viewOS.ListToMatrix();
                                this.viewOS.SetCostoTotal();

                                if (BubbleEvent)
                                    this.viewOS.LimpiarObjctRefacc();
                            }

                            if (!string.IsNullOrEmpty(CadenaRef))
                                oHerramientas.ColocarMensaje(CadenaRef, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);

                        }
                        #endregion

                        #region DetalleOrden
                        if (pVal.ItemUID == "btnOSvDone" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            if (this.viewOS.ExistDetalleOrden())
                            {
                                this.viewOS.FormToDetalleOrden();
                                int index = viewOS.ListaServiciosAgregados.FindIndex(
                                        x => x.DetalleOrdenID == viewOS.EntityDetalle.oDetalleOrden.DetalleOrdenID);
                                viewOS.ListaServiciosAgregados[index] = (DetalleOrden)viewOS.EntityDetalle.oDetalleOrden.Clone();
                                //viewOS.ListaServiciosEditados.Add((DetalleOrden)viewOS.EntityDetalle.oDetalleOrden.Clone());
                                viewOS.FillGridServiciosFromList();
                                this.viewOS.SetCostoTotal();

                                //No entra debido a que el boton de indicacion es incorrecto.
                                /*viewOS.FillGridRefaccionesFromList();
                                viewOS.SetCostoTotal();*/
                            }
                            else
                            {
                                this.viewOS.NewDetalleOrdenToGrid();
                            }
                            this.viewOS.EmptyServicioToForm();
                            BubbleEvent = false;
                        }
                        if (pVal.ItemUID == "btnDelRf" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            if (!pVal.BeforeAction)
                            {
                                if (viewOS.Refacciones.Count > 0)
                                {
                                    BubbleEvent = viewOS.EliminarLineaRefaccion();
                                    if (!BubbleEvent)
                                        throw new Exception("Refacci�n no eliminada asegurte de haber seleccionado el registro");
                                }
                                else
                                    throw new Exception("Agrega refacciones para poder eliminar");
                            }
                        }
                        if (pVal.ItemUID == "btnDelOSv" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.viewOS.GetDetalleOrdenFromGrid();
                            if (this.viewOS.RowIndex == null)
                                throw new Exception("Indice de objeto no establecido");
                            int index = (int)this.viewOS.RowIndex;
                            if (viewOS.EntityDetalle.oDetalleOrden.DetalleOrdenID == null || viewOS.EntityDetalle.oDetalleOrden.DetalleOrdenID <= 0)
                            {
                                viewOS.ListaServiciosAgregados.RemoveAt(index);
                                //viewOS.ListaServiciosEliminados.Add((DetalleOrden)viewOS.EntityDetalle.oDetalleOrden.Clone());
                            }
                            viewOS.FillGridServiciosFromList();
                            BubbleEvent = false;
                        }
                        if (pVal.ItemUID == "btnEdtOSv" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.viewOS.GetDetalleOrdenFromGrid();
                            this.viewOS.DetalleOrdenToForm();
                            BubbleEvent = false;
                        }
                        #endregion
                    }
                    #endregion OrdenServicio

                    #endregion BeforeAction

                    #region AfterAction
                    else
                    {

                    }
                    #endregion AfterAction
                }
                #endregion

                if (oSolicitaOS != null)
                {
                    try
                    {
                        oSolicitaOS.SBO_Application_ItemEvent(FormUID, ref pVal, out BubbleEvent);
                    }
                    catch { }
                }

            }
            catch (Exception ex)
            {
                BubbleEvent = false;
                SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
            }
        }
        #endregion

        #region LoadData
        public void LoadData()
        {
            try
            {
                if (!this.viewOS.ExistDetalleOrden())
                {
                    /*Llenando Servicios...*/
                    if (viewlist.OSRowIndex > -1)
                    {
                        this.viewlist.GetOrdenFromGrid();
                        this.viewOS.Entity.oUDT.GetByKey(this.viewlist.Entity.OrdenServicio.OrdenServicioID.ToString());
                        this.viewOS.Entity.UDTToOrdenServicio();
                        this.viewOS.Entity.GetDetalleOrden(Convert.ToInt32(this.viewlist.Entity.OrdenServicio.OrdenServicioID));
                        this.viewOS.EntityToForm();
                        this.viewOS.GetDetalleOrdenInfo();
                        /*Env�a el coso total de los servicios a la variable
                         est�tica*/
                        this.viewOS.FillGridServiciosFromList();
                    }
                    /*Llenando refacci�nes...*/
                    if (viewlist.OSRowIndex > -1 && (this.viewOS.Refacciones == null || this.viewOS.Refacciones.Count == 0))
                    {
                        this.viewOS.GestionInventario();
                        this.viewOS.EntityRefaccion = new DetalleRefaccionesSAP(this.Company);
                        this.viewOS.EntityRefaccion.oDetalleRefaccion = new DetalleRefaccion();

                        this.viewOS.EntityRefaccion.oDetalleRefaccion.OrdenServicio = new OrdenServicio();
                        this.viewOS.EntityRefaccion.oDetalleRefaccion.OrdenServicio.OrdenServicioID = this.viewlist.Entity.OrdenServicio.OrdenServicioID;
                        this.viewOS.Refacciones = this.viewOS.EntityRefaccion.RecuperaLista();
                        /*Env�a el coso total de las refacci�nes a la variable
                         est�tica*/
                        //this.view.ListToMatrix(); --- Equivalente en FillGridRefaccionesFromList();
                        this.viewOS.FillGridRefaccionesFromList();
                    }
                }
                this.viewOS.SetCostoTotal();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al migrar informacion del Grid Administrar ordenes a laventana Ordenes Servicio" + ex.Message);
            }
        }
        #endregion LoadData

        #region Metodos SAP

        private void SalidaInventarioRefacciones()
        {
            try
            {

            }
            catch (Exception ex)
            {

            }
        }

        private void InsertSAP()
        {
            /*try
            {
                //OrdenServicioData ordenData = new OrdenServicioData(this.Company);
                this.viewOS.EntityRefaccion = new DetalleRefaccionesSAP(this.Company);
                this.viewOS.FormToEntity();
                this.ItemTemp = KananSAP.Helper.KananSAPHelpers.GetCodeTemporal();
                this.viewOS.Entity.ItemCode = this.ItemTemp.ToString();
                this.viewOS.Entity.Sincronizado = 1;
                this.viewOS.Entity.UUID = Guid.NewGuid();
                this.viewOS.Entity.OrdenServicioToUDT();
                this.Company.StartTransaction();
                this.viewOS.Entity.Insert();
                if (this.Company.InTransaction)
                    this.Company.EndTransaction(BoWfTransOpt.wf_Commit);

                this.viewOS.Entity.InsertDetalleOrden();

                this.viewOS.ActualizarFolio();


                ///------------
                if (this.viewOS.Refacciones != null)
                {
                    this.viewOS.EntityRefaccion.LimipiarTabla();
                    foreach (DetalleRefaccion data in this.viewOS.Refacciones)
                    {
                        this.viewOS.EntityRefaccion.oDetalleRefaccion.iLineaID = data.iLineaID;
                        this.viewOS.EntityRefaccion.oDetalleRefaccion.Almacen = data.Almacen;
                        this.viewOS.EntityRefaccion.oDetalleRefaccion.Articulo = data.Articulo;
                        this.viewOS.EntityRefaccion.oDetalleRefaccion.Cantidad = data.Cantidad;
                        this.viewOS.EntityRefaccion.oDetalleRefaccion.CodigoAlmacen = data.CodigoAlmacen;
                        this.viewOS.EntityRefaccion.oDetalleRefaccion.Costo = data.Costo;
                        this.viewOS.EntityRefaccion.oDetalleRefaccion.Total = data.Total;
                        this.viewOS.EntityRefaccion.oDetalleRefaccion.Almacen = data.Almacen;
                        this.viewOS.EntityRefaccion.oDetalleRefaccion.CodigoAlmacen = data.CodigoAlmacen;
                        this.viewOS.EntityRefaccion.DetalleRefaccionToUDT();
                        
                        this.Company.StartTransaction();
                        this.viewOS.EntityRefaccion.Insert();
                        if (this.Company.InTransaction)
                            this.Company.EndTransaction(BoWfTransOpt.wf_Commit);
                    }
                }
            }
            catch (Exception ex)
            {
                if (this.Company.InTransaction)
                    this.Company.EndTransaction(BoWfTransOpt.wf_RollBack);
                throw new Exception(ex.Message);
            }*/
        }

        private void UpdateSAP()
        {
            try
            {
                //OrdenServicioData ordenData = new OrdenServicioData(this.Company);
                this.viewOS.EntityRefaccion = new DetalleRefaccionesSAP(this.Company);
                //OrdenServicioData ordenData = new OrdenServicioData(this.Company);
                this.viewOS.FormToEntity();
                //this.ItemTemp = KananSAP.Helper.KananSAPHelpers.GetCodeTemporal();
                //ordenData.ItemCode = this.ItemTemp.ToString();
                this.viewOS.Entity.Sincronizado = 0;// cambiar a 2 para implementar revision
                //ordenData.UUID = Guid.NewGuid();
                //this.view.Entity.OrdenServicio = this.view.OrdenServicio;
                this.viewOS.Entity.ItemCode = null;
                //this.viewOS.Entity.OrdenServicioToUDT();

                this.Company.StartTransaction();
                this.viewOS.Entity.Actualizar();
                if (this.Company.InTransaction)
                    this.Company.EndTransaction(BoWfTransOpt.wf_Commit);

                if (this.viewOS.Refacciones != null)
                {
                    this.viewOS.EntityRefaccion.LimipiarTabla();
                    foreach (DetalleRefaccion data in this.viewOS.Refacciones)
                    {

                        this.viewOS.EntityRefaccion.oDetalleRefaccion.iLineaID = data.iLineaID;
                        this.viewOS.EntityRefaccion.oDetalleRefaccion.Almacen = data.Almacen;
                        this.viewOS.EntityRefaccion.oDetalleRefaccion.Articulo = data.Articulo;
                        this.viewOS.EntityRefaccion.oDetalleRefaccion.Cantidad = data.Cantidad;
                        this.viewOS.EntityRefaccion.oDetalleRefaccion.CodigoAlmacen = data.CodigoAlmacen;
                        this.viewOS.EntityRefaccion.oDetalleRefaccion.Costo = data.Costo;
                        this.viewOS.EntityRefaccion.oDetalleRefaccion.Total = data.Total;
                        this.viewOS.EntityRefaccion.oDetalleRefaccion.Almacen = data.Almacen;
                        this.viewOS.EntityRefaccion.oDetalleRefaccion.CodigoAlmacen = data.CodigoAlmacen;
                       // this.viewOS.EntityRefaccion.DetalleRefaccionToUDT();

                        this.Company.StartTransaction();
                        this.viewOS.EntityRefaccion.Insert();
                        if (this.Company.InTransaction)
                            this.Company.EndTransaction(BoWfTransOpt.wf_Commit);

                    }
                }
            }
            catch (Exception ex)
            {
                if (this.Company.InTransaction)
                    this.Company.EndTransaction(BoWfTransOpt.wf_RollBack);
                throw new Exception(ex.Message);
            }
        }

        private void ActualizarSAP()
        {
            //OrdenServicioData ordenData = new OrdenServicioData(this.Company);
            this.viewOS.Entity.ItemCode = this.ItemTemp.ToString();
            if (this.viewOS.Entity.Existe())
            {
                this.viewOS.Entity.UDTToOrdenServicio();
                this.viewOS.Entity.Sincronizado = 0;
                
                this.Company.StartTransaction();
                this.viewOS.Entity.ActualizarCode(this.OrdenServicioReturned);
                if (this.Company.InTransaction)
                    this.Company.EndTransaction(BoWfTransOpt.wf_Commit);

                if (this.OrdenServicioReturned.DetalleOrden.Any())
                {
                    this.Company.StartTransaction();
                    this.viewOS.Entity.ActualizarCodeDetalle(this.OrdenServicioReturned.DetalleOrden);
                    if (this.Company.InTransaction)
                        this.Company.EndTransaction(BoWfTransOpt.wf_Commit);
                }

                this.viewOS.Entity.ActualizarCambioEstarusOSID(this.OrdenServicioReturned.OrdenServicioID);
            }
            //view.EFolio.folio.FolioOrdenID = OrdenServicioReturned.FolioOrden.FolioOrdenID;
            //view.EFolio.Sincronizado = 0;
            //this.view.EFolio.ActualizarCode(view.EFolio.folio);
        }

        #endregion

        #region Metodos WS
        private void InsertWS()
        {
            try
            {
                //this.view.FormToData();
                //this.Ordenes = this.ordenWS.InsertarSinAlerta(this.view.OrdenServicio, this.view.ListaServiciosAgregados);
                this.OrdenServicioReturned = this.ordenWS.InsertarSinAlerta(this.viewOS.Entity.OrdenServicio);
                if (this.OrdenServicioReturned == null)
                    throw new Exception("Es posible que no se haya guardado la informaci�n");
                if (this.OrdenServicioReturned.OrdenServicioID == null)
                    throw new Exception("Es posible que no se haya guardado la informaci�n");
                //if (this.OrdenServicioReturned.DetalleOrden == null || this.OrdenServicioReturned.DetalleOrden.Count <= 0)
                //    throw new Exception("Es posible que no se haya guardado la informaci�n");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void UpdateWS()
        {
            try
            {
                //this.view.FormToData();
                //this.Ordenes = this.ordenWS.InsertarSinAlerta(this.view.OrdenServicio, this.view.ListaServiciosAgregados);
                this.viewOS.Entity.OrdenServicio.DetalleOrden = null;
                this.viewOS.Entity.OrdenServicio = this.ordenWS.ActualizarSinAlerta(this.viewOS.Entity.OrdenServicio);
                if (this.viewOS.Entity.OrdenServicio == null)
                    throw new Exception("Es posible que no se haya guardado la informaci�n");
                if (this.viewOS.Entity.OrdenServicio.OrdenServicioID == null)
                    throw new Exception("Es posible que no se haya guardado la informaci�n");
                //if (this.OrdenServicioReturned.DetalleOrden == null || this.OrdenServicioReturned.DetalleOrden.Count <= 0)
                //    throw new Exception("Es posible que no se haya guardado la informaci�n");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        private Boolean InsertarOrdenes()
        {
            #region Comentarios
            /*
             * Comentarios: Metodo encargado de realizar la insercion de una orden de servicio.
             *
             * Actualizaciones:
             * Se modifica el tipo de dato devuelto a boleano, para saber si la orden ha sido o no
             * creada, con el fin de utilizar la respuesta para desencadenar acciones.
             * Se crearon campos nuevos para saber cuando el veh�culo/activo ingresa o sale del
             * taller.
             */
            #endregion

            bool bContinuar = false;
            try
            {
                this.InsertSAP();
                if (this.viewOS.Entity.OrdenServicio.SucursalOrden.SucursalID == 0)
                    this.viewOS.Entity.OrdenServicio.SucursalOrden.SucursalID = null;
                this.InsertWS();
                this.ActualizarSAP();
                this.oHerramientas.ColocarMensaje("�Registro Guardado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                bContinuar = true;

            }
            catch (Exception ex)
            {
                this.viewOS.EmptyEntityToForm();
                this.SBO_Application.Forms.ActiveForm.Close();
                throw new Exception(ex.Message);
            }

            return bContinuar;
        }

        private void ActualizarOrdenes()
        {
            try
            {
                this.Company.StartTransaction();
                this.UpdateSAP();
                this.UpdateWS();
                ManageDetalleOrden();
                if (this.Company.InTransaction)
                    this.Company.EndTransaction(BoWfTransOpt.wf_Commit);
                this.oHerramientas.ColocarMensaje("�Registro Guardado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                this.viewOS.EmptyEntityToForm();

            }
            catch (Exception ex)
            {
                if (this.Company.InTransaction)
                    this.Company.EndTransaction(BoWfTransOpt.wf_RollBack);
                this.viewOS.EmptyEntityToForm();
                this.SBO_Application.Forms.ActiveForm.Close();
                throw new Exception(ex.Message);
            }
        }

        #region ExportarPDF
        private Boolean ExportarPDF(out string PDF, bool bStart = true)
        {
            Boolean bContinuar = false;
            String pathFile = Directorio();

            if (string.IsNullOrEmpty(pathFile))
                throw new Exception("No se ha determinado el directorio de PDF");

            pathFile = string.Format(@"{0}\OrdenServicio", pathFile);
            if (!Directory.Exists(pathFile))
                Directory.CreateDirectory(pathFile);
            this.viewOS.FormToEntity();
            string NameFilePDF = string.Format(@"{0}\{1}_{2}.pdf", pathFile, this.viewOS.Entity.OrdenServicio.FolioOrden.Folio, this.viewOS.Entity.OrdenServicio.FolioOrden.AnioFolio);

            String FechActual = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
            PdfWriter oPDFWrite;
            PdfContentByte oPDFcb;
            PDF = NameFilePDF;
            if (File.Exists(NameFilePDF))
                File.Delete(NameFilePDF);

            iTextSharp.text.Font oFontConcepto = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 11, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            iTextSharp.text.Font oFontTittle = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font oFontSubtittle = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 11, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font oespacios = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 1, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            try
            {
                Double dCosto = Convert.ToDouble(this.viewOS.Entity.OrdenServicio.Costo);
                Double dImpuesto = Convert.ToDouble(this.viewOS.Entity.OrdenServicio.Impuesto);
                Double dSubtotal = dCosto - dImpuesto;
                Double dTotal = dSubtotal + dImpuesto;

                Document oDocumento;
                oDocumento = new Document(PageSize.A4, 20, 20, 25, 10);
                oPDFWrite = PdfWriter.GetInstance(oDocumento, new FileStream(NameFilePDF, FileMode.Create));
                oDocumento.Open();
                oPDFcb = oPDFWrite.DirectContent;
                oPDFcb.BeginText();

                float cellHeight = oDocumento.TopMargin;
                iTextSharp.text.Rectangle oPage = oDocumento.PageSize;
                oPDFcb.EndText();

                PdfPTable oTitulo = new PdfPTable(2);
                oTitulo.WidthPercentage = 100;
                float[] widthHeader = { 400f, 400f };
                oTitulo.SetWidths(widthHeader);
                oTitulo.HorizontalAlignment = 0;

                PdfPCell oCelda = new PdfPCell();
                oCelda.Colspan = 2;
                oCelda.FixedHeight = 18f;

                estiloSBorde(ref oCelda, "REPORTE DETALLE DE ORDEN DE SERVICIO", oFontTittle);
                oCelda.VerticalAlignment = Element.ALIGN_LEFT;
                oCelda.HorizontalAlignment = Element.ALIGN_LEFT;
                oTitulo.AddCell(oCelda);

                estiloSBorde(ref oCelda, string.Format("Fecha:   {0}", FechActual), oFontConcepto);
                oCelda.VerticalAlignment = Element.ALIGN_RIGHT;
                oCelda.HorizontalAlignment = Element.ALIGN_RIGHT;
                oTitulo.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontSubtittle);
                oCelda.Colspan = 2;
                oCelda.FixedHeight = 5f;
                oCelda.BorderWidthBottom = .5f;
                oTitulo.AddCell(oCelda);
                oDocumento.Add(oTitulo);

                PdfPTable oConceptos = new PdfPTable(4);
                oConceptos.WidthPercentage = 100;
                float[] widthHeader2 = { 200f, 20f, 400f, 400f };
                oConceptos.SetWidths(widthHeader2);
                oConceptos.HorizontalAlignment = 0;

                estiloSBorde(ref oCelda, "", oFontSubtittle);
                oCelda.Colspan = 2;
                oCelda.FixedHeight = 15f;
                oConceptos.AddCell(oCelda);

                estiloSBorde(ref oCelda, "", oFontSubtittle);
                oCelda.Colspan = 2;
                oCelda.FixedHeight = 15f;
                oConceptos.AddCell(oCelda);
                oCelda = new PdfPCell();


                oDocumento.Add(oConceptos);
                PdfPTable oDetalle = new PdfPTable(4);
                oDetalle.WidthPercentage = 100;
                float[] widthHeaders = { 60f, 5f, 140f, 120f };
                oDetalle.SetWidths(widthHeaders);
                oDetalle.HorizontalAlignment = 0;

                oCelda = new PdfPCell();
                oCelda.Colspan = 2;
                oCelda.FixedHeight = 18f;
                String cardCode = "";

                #region CONCEPTOS
                estiloCeldaColor(ref oCelda, "Estatus:", oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                estiloCeldaColor(ref oCelda, DevolverServicio(this.viewOS.Entity.OrdenServicio.EstatusOrden.EstatusOrdenID), oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);

                EspacioBlanco(ref oCelda, oespacios, oDetalle);

                estiloCeldaColor(ref oCelda, "Proveedor:", oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                this.viewOS.Entity.RecuperaProveedor(this.viewOS.Entity.OrdenServicio.ProveedorServicio.ProveedorID, GestionGlobales.bSqlConnection, ref cardCode, false);
                estiloCeldaColor(ref oCelda, cardCode, oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                EspacioBlanco(ref oCelda, oespacios, oDetalle);

                estiloCeldaColor(ref oCelda, "Fecha:", oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                estiloCeldaColor(ref oCelda, this.viewOS.Entity.OrdenServicio.Fecha.ToString(), oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);

                EspacioBlanco(ref oCelda, oespacios, oDetalle);


                estiloCeldaColor(ref oCelda, "Folio:", oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                estiloCeldaColor(ref oCelda, this.viewOS.Entity.OrdenServicio.FolioOrden.Folio == 0 ? "Desconocido" : this.viewOS.Entity.OrdenServicio.FolioOrden.Folio.ToString(), oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);

                EspacioBlanco(ref oCelda, oespacios, oDetalle);


                estiloCeldaColor(ref oCelda, "Descripci�n:", oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                estiloCeldaColor(ref oCelda, this.viewOS.Entity.OrdenServicio.Descripcion, oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                EspacioBlanco(ref oCelda, oespacios, oDetalle);


                estiloCeldaColor(ref oCelda, "M�todo de pago:", oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                estiloCeldaColor(ref oCelda, this.viewOS.Entity.OrdenServicio.TipoDePago.TipoPago, oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                EspacioBlanco(ref oCelda, oespacios, oDetalle);


                estiloCeldaColor(ref oCelda, "Detalle de pago:", oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                estiloCeldaColor(ref oCelda, this.viewOS.Entity.OrdenServicio.TipoDePago.DetallePago, oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                EspacioBlanco(ref oCelda, oespacios, oDetalle);

                estiloCeldaColor(ref oCelda, "Responsable:", oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                estiloCeldaColor(ref oCelda, FindEmpleado(this.viewOS.Entity.OrdenServicio.ResponsableOrdenServicio.EmpleadoID), oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                EspacioBlanco(ref oCelda, oespacios, oDetalle);

                estiloCeldaColor(ref oCelda, "Aprobador:", oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                estiloCeldaColor(ref oCelda, FindEmpleado(this.viewOS.Entity.OrdenServicio.AprobadorOrdenServicio.EmpleadoID), oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                EspacioBlanco(ref oCelda, oespacios, oDetalle);

                estiloCeldaColor(ref oCelda, "Subtotal:", oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                estiloCeldaColor(ref oCelda, string.Format("$ {0}", dSubtotal), oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                EspacioBlanco(ref oCelda, oespacios, oDetalle);

                estiloCeldaColor(ref oCelda, "Impuesto:", oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);

                estiloCeldaColor(ref oCelda, string.Format("$ {0}", dImpuesto), oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                EspacioBlanco(ref oCelda, oespacios, oDetalle);

                estiloCeldaColor(ref oCelda, "Total:", oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                estiloCeldaColor(ref oCelda, string.Format("$ {0}", dTotal), oFontConcepto, new BaseColor(231, 231, 233));
                oDetalle.AddCell(oCelda);
                estiloSBorde(ref oCelda, "", oFontConcepto);
                oDetalle.AddCell(oCelda);
                EspacioBlanco(ref oCelda, oespacios, oDetalle);

                oDocumento.Add(oDetalle);
                #endregion

                PdfPTable Orden = new PdfPTable(1);
                Orden.WidthPercentage = 100;
                Orden.HorizontalAlignment = 0;
                oCelda = new PdfPCell();
                oCelda.FixedHeight = 18f;
                estiloCeldaColor(ref oCelda, "Detalle de la orden", oFontSubtittle, new BaseColor(231, 231, 233), false);
                Orden.AddCell(oCelda);


                oDocumento.Add(Orden);

                PdfPTable lstConcepto = new PdfPTable(3);
                lstConcepto.WidthPercentage = 100;
                float[] widthConcepto = { 400f, 400f, 400f };
                lstConcepto.SetWidths(widthConcepto);
                lstConcepto.HorizontalAlignment = 0;

                oCelda = new PdfPCell();
                oCelda.Colspan = 2;
                oCelda.FixedHeight = 18f;

                estiloCeldaColor(ref oCelda, "Veh�culo", oFontSubtittle, new BaseColor(174, 174, 174), false);
                lstConcepto.AddCell(oCelda);

                estiloCeldaColor(ref oCelda, "Servicio", oFontSubtittle, new BaseColor(174, 174, 174), false);
                lstConcepto.AddCell(oCelda);

                estiloCeldaColor(ref oCelda, "Costo", oFontSubtittle, new BaseColor(174, 174, 174), false);
                lstConcepto.AddCell(oCelda);

                int iLine = 0;
                if (this.viewOS.ListaServiciosAgregados.Count == 0)
                    throw new Exception("No se han encontrado servicios agregados");

                foreach (DetalleOrden svc in this.viewOS.ListaServiciosAgregados)
                {
                    iLine++;
                    var BaseOcolor = new BaseColor(231, 231, 231);
                    if (iLine % 2 == 0)
                    {
                        BaseOcolor = new BaseColor(207, 207, 207);
                    }

                    estiloCeldaColor(ref oCelda, Vehiculo(svc.Mantenible.MantenibleID), oFontConcepto, BaseOcolor);
                    lstConcepto.AddCell(oCelda);

                    estiloCeldaColor(ref oCelda, svc.Servicio.Nombre, oFontConcepto, BaseOcolor, false);
                    lstConcepto.AddCell(oCelda);

                    estiloCeldaColor(ref oCelda, string.Format("$ {0}", svc.Costo.ToString("0.00")), oFontConcepto, BaseOcolor, false);
                    lstConcepto.AddCell(oCelda);



                }
                oDocumento.Add(lstConcepto);
                oDocumento.Close();
                oPDFWrite.Close();

                if (bStart)
                    System.Diagnostics.Process.Start(NameFilePDF);

            }
            catch (Exception ex)
            {
                bContinuar = false;
                oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            }

            return bContinuar;
        }
        #endregion ExportarPDF

        private void EspacioBlanco(ref PdfPCell oCelda, iTextSharp.text.Font oFuente, PdfPTable oDetalle)
        {
            for (int i = 0; i < 4; i++)
            {
                estiloSBorde(ref oCelda, "\n", oFuente);
                oDetalle.AddCell(oCelda);
            }
        }

        private void estiloSBorde(ref PdfPCell oCelda, string sTextoAgregar, iTextSharp.text.Font oFuente)
        {
            oCelda = new PdfPCell(new Phrase(sTextoAgregar, oFuente));
            oCelda.Border = PdfPCell.NO_BORDER;
        }

        private void estiloCeldaColor(ref PdfPCell oCelda, string sTextoAgregar, iTextSharp.text.Font oFuente, BaseColor oColor, bool bAling = true)
        {
            oCelda = new PdfPCell(new Phrase(sTextoAgregar, oFuente));
            oCelda.Border = PdfPCell.NO_BORDER;
            oCelda.BackgroundColor = oColor;
            oCelda.Border = PdfPCell.NO_BORDER;
            if (bAling)
            {
                oCelda.VerticalAlignment = Element.ALIGN_LEFT;
                oCelda.HorizontalAlignment = Element.ALIGN_LEFT;
            }
            else
            {
                oCelda.VerticalAlignment = Element.ALIGN_CENTER;
                oCelda.HorizontalAlignment = Element.ALIGN_CENTER;
            }

        }

        public string DevolverServicio(int? EstatusOrdenID)
        {
            string Servicio = "";

            switch (EstatusOrdenID)
            {
                case 1:
                    Servicio = "Servicio solicitado";
                    break;
                case 2:
                    Servicio = "Solicitud aprobada";
                    break;
                case 3:
                    Servicio = "Solicitud eliminada";
                    break;
                case 5:
                    Servicio = "Solicitud atendida";
                    break;
                case 6:
                    Servicio = "Solicitud realizada";
                    break;
            }

            return Servicio;
        }

        private String Directorio()
        {
            String Path = "";
            String pathAttach = string.Format(@"SELECT ""AttachPath"" FROM OADP");
            Recordset oRecordQuery = Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            oRecordQuery.DoQuery(pathAttach);

            if (oRecordQuery.RecordCount > 0)
            {
                oRecordQuery.MoveFirst();
                Path = Convert.ToString(oRecordQuery.Fields.Item("AttachPath").Value);
            }

            return Path;
        }

        private String Vehiculo(int? MantenibleID)
        {
            String NombreVehiculo = "";
            String Vehiculo = string.Format(@"select Name,U_Descripcion from ""@VSKF_VEHICULO"" WHERE ""U_VEHICULOID"" = '{0}'", MantenibleID);
            Recordset oRecordQuery = Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            oRecordQuery.DoQuery(Vehiculo);

            if (oRecordQuery.RecordCount > 0)
            {
                oRecordQuery.MoveFirst();
                NombreVehiculo = string.Format("{0} - {1}", Convert.ToString(oRecordQuery.Fields.Item("Name").Value), Convert.ToString(oRecordQuery.Fields.Item("U_Descripcion").Value));
            }
            else
                NombreVehiculo = "Desconocido";
            return NombreVehiculo;
        }

        private String FindEmpleado(int? EmpleadoID)
        {

            String NombreEmpleado = "";
            String Empleado = @"select * from [@VSKF_RELEMPLEADO] WHERE U_KFEmpleadoID = " + EmpleadoID;
            Recordset oRecordQuery = Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            oRecordQuery.DoQuery(Empleado);

            if (oRecordQuery.RecordCount > 0)
            {
                oRecordQuery.MoveFirst();
                NombreEmpleado = Convert.ToString(oRecordQuery.Fields.Item("Name").Value);
            }
            else
                NombreEmpleado = "Desconocido";
            return NombreEmpleado;
        }

        private void EnviarNotificacion(int? EmpleadoID)
        {
            String Attachment = string.Empty, Asunto = string.Empty, resultado = string.Empty, correoPara = string.Empty, Nombre = string.Empty, sconsultaSql = string.Empty;

            SBO_KF_Configuracion_INFO oSetting = new SBO_KF_Configuracion_INFO();
            SBO_KF_Configuracion_AD oConfiguracion = new SBO_KF_Configuracion_AD();

            resultado = oConfiguracion.recuperaConfiguracion(ref Company, out sconsultaSql, ref oSetting);
            if (resultado == "OK")
            {
                resultado = "ERROR";
                this.ExportarPDF(out Attachment, false);

                if (RecuperarCorreoNombre(EmpleadoID, out Nombre, out correoPara))
                {
                    String HTML = @"<html><head><title></title></head><body>" +
                                   "<p>Solicitud de orden de servicio</p>  " +
                                    "<h2>" + Nombre + " </h2>" +
                                    "<p> Se ha creado una solicitud de orden de servicio se envia documento de detalle de la solicitud como adjunto. </p>" +
                                    "<p>*Este es un correo enviado a trav�s de un servicio de forma automatica, favor de no responderlo.*</p>" +
                                    "</body></html>";

                    resultado = GestionGlobales.Notificarcorreo(oSetting, correoPara, "Solicitud de orden de servicio", HTML, Attachment);
                    if (resultado != "OK")
                        oHerramientas.ColocarMensaje(resultado, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                    else
                        oHerramientas.ColocarMensaje("Se ha enviado la notificaci�n de la orden de servicio al usuario " + Nombre, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                }

                else
                    oHerramientas.ColocarMensaje("No se encontr� el correo del empleado", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            }
            else resultado = "No se ha podido enviar la notificacion";
        }

        private Boolean RecuperarCorreoNombre(int? EmpleadoID, out String Nombre, out String Correo)
        {
            Boolean bContinuar = false;
            Nombre = string.Empty;
            Correo = string.Empty;

            try
            {
                String Empleado = @"SELECT U_Nombre, U_Correo FROM [@VSKF_OPERADOR] WHERE U_EmpleadoID = " + EmpleadoID;
                Recordset oRecordQuery = Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                oRecordQuery.DoQuery(Empleado);

                if (oRecordQuery.RecordCount > 0)
                {
                    oRecordQuery.MoveFirst();
                    Nombre = Convert.ToString(oRecordQuery.Fields.Item("U_Nombre").Value);
                    Correo = Convert.ToString(oRecordQuery.Fields.Item("U_Correo").Value);
                    bContinuar = true;
                }
                else
                    bContinuar = false;

            }
            catch (Exception ex)
            {
                bContinuar = false;
                KananSAPHerramientas.LogError("OrdenServicioSinAlertaPresenter.cs->RecuperarCorreoNombre()", ex.Message);
            }


            return bContinuar;
        }

        private Boolean CrearFactura(OrdenServicio Orden, bool bDraft)
        {
            #region Comentarios
            /*Metodo que genera la factura de la orden de servicio*/
            #endregion

            Recordset oRecordQuery = Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            ConfiguracionKananINFO KnSttings = null; //this.Settings();
            String sCentrocosto = string.Empty;

            string CardCode = "";
            if (this.viewOS.Entity.RecuperaProveedor(Orden.ProveedorServicio.ProveedorID, GestionGlobales.bSqlConnection, ref CardCode) != "OK")
                throw new Exception("No se ha encontrado el proveedor");

            if (KnSttings.cSucursal == 'Y')
            {
                oRecordQuery.DoQuery(@"SELECT Name,U_CENTRO FROM [@VSKF_SUCURSAL] WHERE U_SucursalID = " + Orden.SucursalOrden.SucursalID);
                //recuperar sucursal de lista sucursales activas
                //si no se recupera sucursal o no existe enviar sucursal guardada en configuraciones
                //Orden.SucursalOrden.SucursalID--->valor para obtener sucursal
                if (oRecordQuery.RecordCount > 0)
                {
                    oRecordQuery.MoveFirst();
                    sCentrocosto = Convert.ToString(oRecordQuery.Fields.Item("U_CENTRO").Value);
                }
                else
                    sCentrocosto = KnSttings.Sucursal;

            }
            else
            {
                //sCentrocosto =
                oRecordQuery.DoQuery(@"SELECT Name FROM [@VSKF_VEHICULO] WHERE U_VehiculoID =" + Orden.DetalleOrden[0].Mantenible.MantenibleID);
                if (oRecordQuery.RecordCount > 0)
                {
                    oRecordQuery.MoveFirst();
                    sCentrocosto = Convert.ToString(oRecordQuery.Fields.Item("Name").Value);
                }
                else
                    sCentrocosto = KnSttings.Vehiculo;

            }

            #region validaci�n para recuperar centro de costo en cliente
            if (KnSttings.cCliente == 'Y')
            {
                try
                {
                    string sql = string.Format(GestionGlobales.bSqlConnection ? "SELECT U_KF_CostCentro FROM OCRD WHERE CardCode = '{0}'" : @"SELECT U_KF_CostCentro FROM ""OCRD"" WHERE ""CardCode"" = '{0}' ", CardCode);
                    oRecordQuery.DoQuery(sql);
                    if (oRecordQuery.RecordCount == 0)
                    {
                        oRecordQuery.MoveFirst();
                        string sCostCenter = Convert.ToString(oRecordQuery.Fields.Item("U_KF_CostCentro").Value);
                        if (!string.IsNullOrEmpty(sCostCenter))
                            sCentrocosto = sCostCenter;
                    }
                }
                catch { }
            }
            #endregion validaci�n para recuperar centro de costo en cliente

            //KnSttings.cVehiculo == 'Y' ? KnSttings.Vehiculo : KnSttings.Sucursal;

            Boolean bContinuar = false;
            try
            {
                //Se obtiene el costo del formulario
                //Decimal Costo = this.view.GetCostoForm();
                //bContinuar = this.viewOS.Entity.CreateInvoice(Orden, GestionGlobales.bSqlConnection, sCentrocosto, KnSttings.DimCode, bDraft);
                oHerramientas.ColocarMensaje("Factura de proveedor creada exitosamente", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            }
            catch (Exception ex)
            {
                oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                KananSAPHerramientas.LogError("OrdenServicioSinAlertaPresenter.cs->CrearFactura()", ex.Message);
                bContinuar = false;
            }
            return bContinuar;

        }

        #region DetalleOrden
        public void ManageDetalleOrden()
        {
            try
            {
                foreach (var svc in viewOS.ListaServiciosAgregados)
                {
                    if (svc.DetalleOrdenID == null || svc.DetalleOrdenID <= 0)
                    {
                        this.viewOS.EntityDetalle.oDetalleOrden = (DetalleOrden)svc.Clone();
                        this.viewOS.EntityDetalle.oDetalleOrden.OrdenServicio.OrdenServicioID =
                            this.viewOS.Entity.OrdenServicio.OrdenServicioID;
                        InsertDetalleOrden();
                    }
                    else
                    {
                        this.viewOS.EntityDetalle.oUDT.GetByKey(svc.DetalleOrdenID.ToString());
                        this.viewOS.EntityDetalle.UDTTooDetalleOrden();
                        this.viewOS.EntityDetalle.oDetalleOrden = (DetalleOrden)svc.Clone();
                        UpdateDetalleOrden();
                    }
                    this.viewOS.Entity.OrdenServicio.DetalleOrden.Add((DetalleOrden)this.viewOS.EntityDetalle.oDetalleOrden.Clone());
                }
                //foreach (var svc in view.ListaServiciosAgregados)
                //{

                //}
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void InsertDetalleOrden()
        {
            try
            {

                this.viewOS.EntityDetalle.UUID = Guid.NewGuid();
                this.viewOS.EntityDetalle.Sincronizado = 1;
                sIDTempAsignado = this.viewOS.EntityDetalle.DetalleOrdenToUDT();
                this.viewOS.EntityDetalle.Insert();

                this.viewOS.EntityDetalle.oDetalleOrden = this.ordenWS.AddDetalleOrden(viewOS.EntityDetalle.oDetalleOrden);


                this.viewOS.EntityDetalle.Sincronizado = 0;
                this.viewOS.EntityDetalle.ActualizarCode(viewOS.EntityDetalle.oDetalleOrden, sIDTempAsignado);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void UpdateDetalleOrden()
        {
            try
            {

                this.viewOS.EntityDetalle.DetalleOrdenToUDT();
                this.viewOS.EntityDetalle.Actualizar();

                this.viewOS.EntityDetalle.oDetalleOrden = this.ordenWS.UpdateDetalleOrden(viewOS.EntityDetalle.oDetalleOrden);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion


        #region Validaciones

        private bool ValidarFacturacion(Int32 IDOS)
        {
            bool run = false;
            try
            {
                run = this.viewOS.Entity.ExisteFactura(IDOS) == false ? true : false;
                return run;
            }
            catch (Exception ex)
            {
                run = false;
                throw new Exception("Error: " + ex.Message);
            }
        }

        private bool ValidaOrdenServicio()
        {
            string err = string.Empty;

            switch (this.viewOS.estatus)
            {
                case 1:
                    err = this.viewOS.ValidaSolicitudOrden();
                    break;
            }
            if (string.IsNullOrEmpty(err))
                return true;
            this.SBO_Application.MessageBox(err, 1, "", "", "");
            return false;
        }

        private bool VerificarSiguienteEstatus()
        {
            bool Aprobado = true;

            if (EstatusActual == 1) //Solicitada
                if (EstatusSiguiente != 2 & EstatusSiguiente != 1 & EstatusSiguiente != 3 & EstatusSiguiente != 4)
                    Aprobado = false;

            if (EstatusActual == 2) //Aprobada
                if (EstatusSiguiente != 5 & EstatusSiguiente != 2)
                    Aprobado = false;

            if (EstatusActual == 3) //Rechazada
                if (EstatusSiguiente != 3 & EstatusSiguiente != 4)
                    Aprobado = false;

            if (EstatusActual == 4) //Eliminada
                if (EstatusSiguiente != 4)
                    Aprobado = false;

            if (EstatusActual == 5) //Aprobada
                if (EstatusSiguiente != 6 & EstatusSiguiente != 5)
                    Aprobado = false;

            if (EstatusActual == 6) //Atentida
                if (EstatusSiguiente != 6)
                    Aprobado = false;

            return Aprobado;
        }

        #endregion Validaciones

    }
}
