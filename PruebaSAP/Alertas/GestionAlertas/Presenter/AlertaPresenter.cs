﻿using KananWS.Interface;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.GestionAlertas.Presenter
{
    public class AlertaPresenter
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private EventFilter Filter;
        private Configurations configurations;
        private bool ModalOrden;
        private GestionAlertas.View.AlertaView view;
        //private GestionAlertas.Presenter.OrdenServicioConAlertaPresenter ordenPresenter;
        //private CatalogoVehiculosPresenter veh_presenter;
        //private GestionSociosNegociosPresenter bpartner_presenter;
        //private MenuPresenter menu_presenter;
        //private AutentificacionPresenter auten_presenter;
        public bool IsLogged { get; set; }
        private Helper.KananSAPHerramientas oHerramientas;
        #endregion

        #region Constructor
        public AlertaPresenter(SAPbouiCOM.Application sApplication, SAPbobsCOM.Company Cmpny, Configurations c)
        {
            try
            {
                this.SBO_Application = sApplication;
                this.Company = Cmpny;
                this.configurations = c;
                #region Comentado. La aplicación principal maneja los eventos
                //SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
                #endregion Comentado. La aplicación principal maneja los eventos
                ModalOrden = false;
                this.oHerramientas = new Helper.KananSAPHerramientas(this.SBO_Application);
            }
            catch(Exception ex)
            { throw new Exception(ex.Message); }
        }

        #endregion

        #region ItemEvent
        public void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            if (!IsLogged) return;
            string group = string.Empty;
            try
            {

                #region Orden de servicio

                if (pVal.Before_Action)
                {
                    //if (pVal.FormType == 188)
                    //{
                    //    if (this.view == null)
                    //        this.view = new View.AlertaView(this.SBO_Application, this.Company, pVal.FormTypeEx, pVal.FormTypeCount);
                    //}
                    if (pVal.FormTypeEx == "198" && pVal.EventType == BoEventTypes.et_DOUBLE_CLICK && pVal.ItemUID == "6")
                    {
                        if (this.view == null)
                            this.view = new View.AlertaView(this.SBO_Application, this.Company, pVal.FormTypeEx, pVal.FormTypeCount);

                        int index = pVal.Row;
                        if (this.view.ColumHasValue(index))//, pVal.FormTypeEx, pVal.FormTypeCount))
                        {

                            /*this.ordenPresenter = null;
                            this.ordenPresenter = new OrdenServicioConAlertaPresenter(this.SBO_Application, this.Company, this.configurations);
                            this.ordenPresenter.IsLogged = this.IsLogged;
                            this.ordenPresenter.Initialize(this.view.AlertaID);
                            ///Prueba
                            //this.ordenPresenter.view.LoadForm();
                            //this.ordenPresenter.view.ShowForm();
                            //this.ordenPresenter.view.DataToForm();
                            ///Produccion
                            if (this.ordenPresenter.view.Exist)
                            {
                                this.ordenPresenter.view.LoadForm();
                                this.ordenPresenter.view.ShowForm();
                                this.ordenPresenter.view.DataToForm();
                            }
                            else
                            {
                                this.ordenPresenter = null;
                                this.SBO_Application.MessageBox("Alerta atendida o eliminada", 1, "Ok", "", "");
                            }*/
                                
                        }
                    }

                    if (pVal.FormTypeEx == "188" && pVal.EventType == BoEventTypes.et_DOUBLE_CLICK && pVal.ItemUID == "10")
                    {
                        if (this.view == null)
                            this.view = new View.AlertaView(this.SBO_Application, this.Company, pVal.FormTypeEx, pVal.FormTypeCount);

                        int index = pVal.Row;
                        if (this.view.ColumHasValue2(index))//, pVal.FormTypeEx, pVal.FormTypeCount))
                        {

                            /*this.ordenPresenter = null;
                            this.ordenPresenter = new OrdenServicioConAlertaPresenter(this.SBO_Application, this.Company, this.configurations);
                            this.ordenPresenter.IsLogged = this.IsLogged;
                            this.ordenPresenter.Initialize(this.view.AlertaID);
                            ///Prueba
                            //this.ordenPresenter.view.LoadForm();
                            //this.ordenPresenter.view.ShowForm();
                            //this.ordenPresenter.view.DataToForm();
                            ///Produccion
                            if (this.ordenPresenter.view.Exist)
                            {
                                this.ordenPresenter.view.LoadForm();
                                this.ordenPresenter.view.ShowForm();
                                this.ordenPresenter.view.DataToForm();
                            }
                            else
                            {
                                this.ordenPresenter = null;
                                this.SBO_Application.MessageBox("Alerta atendida o eliminada", 1, "Ok", "", "");
                            }*/

                        }
                    }
                }

                #endregion

            }
            catch (Exception ex)
            {
                BubbleEvent = false;
                KananSAP.Helper.KananSAPHerramientas.LogError("AlertasPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
                oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            }
        }
        #endregion
        

    }
}
