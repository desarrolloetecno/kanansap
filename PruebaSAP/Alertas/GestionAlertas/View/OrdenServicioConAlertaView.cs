﻿﻿using Kanan.Comun.BO2;
using Kanan.Costos.BO2;
using Kanan.Mantenimiento.BO2;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using KananSAP.Configuracion.Configuraciones.Objects;
using KananSAP.GestionFacturaClientes.Objects;
using KananSAP.Helper;
using KananSAP.Mantenimiento.Data;
using KananWS.Interface;
using SAPbobsCOM;
using SAPbouiCOM;
using SAPinterface.Data.AD;
using SAPinterface.Data.INFO.Tablas;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KananSAP.GestionAlertas.View
{
    public class OrdenServicioConAlertaView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private static SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.Button oButton;
        private SAPbouiCOM.StaticText oStaticText;
        private SAPbouiCOM.ComboBox oCombo;
        private XMLPerformance XmlApplication;
        public Kanan.Mantenimiento.BO2.AlertaMantenimiento AlertaMantenimiento { get; set; }
        //public OrdenServicio OrdenServicio { get; set; }
        public OrdenServicioSAP Entity { get; set; }
        public DetalleRefaccionesSAP EntityRefaccion { get; set; }
        public RefaccionesWS ReffWS = null;
        public DetalleOrdenSAP EntityDetalle { get; set; }
        public List<DetalleRefaccion> Refacciones { get; set; }
        public RefaccionesView RefaccionVista{ get; set; }
        public int iLineaRefaccion { get; set; }
        public int? ServicioID { get; set; }
        private SAPbobsCOM.Company oCompany;
        private const string INDEFINIDO = "No definido";
        public bool Exist { get; set; }
        private Configurations Config;
        private NodaTimeHelper.Services.DateTimeHelper dateHelper;
        public List<DetalleOrden> ListaServiciosAgregados { get; set; }
        public List<DetalleOrden> ListaServiciosEliminados { get; set; }
        public List<DetalleOrden> ListaServiciosEditados { get; set; }

        private SAPbouiCOM.Grid oGrid;
        public int? RowIndex { get; set; }
        private SAPbouiCOM.Folder oFolder;
        public int? estatus;
        public string FolioOrdenSercio { get; set; }
        private bool EsSQL;

        public FolioOrdenServicioSAP EFolio { get; set; }

        public decimal? TotalServicios;
        public decimal? TotalRefacciones;
        public decimal? TotalOrdenServicio;

        #endregion

        #region Constructor
        public OrdenServicioConAlertaView(SAPbouiCOM.Application Application, SAPbobsCOM.Company company, Configurations c)
        {
            try
            {
                this.SBO_Application = Application;
                this.oCompany = company;
                this.oItem = null;
                oCombo = null;
                oStaticText = null;
                oEditText = null;
                this.Config = c;
                this.XmlApplication = new XMLPerformance(this.SBO_Application);
                this.PrepareObjects();
                this.oGrid = null;
                this.GetUserConfigs();
                this.ListaServiciosAgregados = new List<DetalleOrden>();
                this.EFolio = new FolioOrdenServicioSAP(this.oCompany);
                this.Entity = new OrdenServicioSAP(ref this.oCompany);
                this.EntityDetalle = new DetalleOrdenSAP(ref this.oCompany);
                this.EsSQL = oCompany.DbServerType.ToString().Contains("MSSQL") ? true : false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region FORM

        public void LoadForm(int? step, bool exist)
        {
            try
            {
                oForm = SBO_Application.Forms.Item("frmConOS");
                this.SetBoundsToControl();
                this.FillCombos();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Invalid Form"))
                {
                    string sPath = System.Windows.Forms.Application.StartupPath;                   
                    this.XmlApplication.LoadFromXML(sPath + "\\XML", "OrdenServicioConAlerta.xml");
                    oForm = SBO_Application.Forms.Item("frmConOS");
                    this.SetBoundsToControl();
                    this.FillCombos();
                    this.SetCurrentFolioOrden();
                    this.ListaServiciosAgregados = new List<DetalleOrden>();
                    this.Refacciones = new List<DetalleRefaccion>();
                    this.RefaccionVista = new RefaccionesView();
                    this.estatus = step;

                    switch (step)
                    {
                        case 1:
                            PrepareOrdenSolicitada();
                            HideCosto();
                            if (exist)
                            {
                                //ShowCosto();
                                oForm.Title = "Actualizar Orden de Servicio";
                                oItem = oForm.Items.Item("cmbestatus");
                                oItem.Enabled = true;
                            }
                            break;
                        case 2:
                            PrepareOrdenAprobada();
                            if (exist)
                            {
                                oForm.Title = "Actualizar Orden de Servicio";
                            }
                            break;
                        case 5:
                            PrepareOrdenAtendida();
                            if (exist)
                            {
                                oForm.Title = "Detalle de Orden de Servicio";
                            }
                            break;
                        case 6:
                            PrepareOrdenRealizada();
                            if (exist)
                            {
                                oForm.Title = "Detalle de Orden de Servicio";
                            }
                            break;
                        case 4:
                            if (!exist)
                            {
                                PrepareOrdenEliminada();
                                SBO_Application.SetStatusBarMessage("Este registro ha sido eliminado, sólo es posible consultar. ", BoMessageTime.bmt_Short, false);
                                return;
                            }
                            break;
                        case 3:
                            PrepareOrdenRechazada();
                            if (exist)
                            {
                                oForm.Title = "Detalle de Orden de Servicio";
                            }
                            break;
                    }
                    //this.EnableEstatus(false);
                    //Establece la ventana centrada
                    oForm.Left = (SBO_Application.Desktop.Width - oForm.Width) / 2;
                    oForm.Top = (SBO_Application.Desktop.Height - oForm.Height) / 2;
                }
                else
                {
                    SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                }
            }
        }

        public void GestionInventario()
        {
            try
            {
                
                string sresponse = string.Empty, sconsultaSql = string.Empty;
                SBO_KF_Configuracion_INFO oSetting = new SBO_KF_Configuracion_INFO();                
                SBO_KF_Configuracion_AD oConfiguracion = new SBO_KF_Configuracion_AD();
                sresponse = oConfiguracion.recuperaConfiguracion(ref oCompany, out sconsultaSql, ref oSetting);
                if (sresponse == "OK")
                {

                    GestionGlobales oGlobales = new GestionGlobales();
                    oGlobales.FillCombo("Ninguno", "WhsCode", "WhsName", "cmbAlmacn", GestionGlobales.AlmacenRecarga(), oForm, this.oCompany);

                    if (oSetting.cKANAN == 'N')
                    {
                        oItem = oForm.Items.Item("txtRefSAP");
                        oItem.Visible = true;
                        oItem = oForm.Items.Item("cmbRefKF");
                        oItem.Visible = false;
                        this.AddItemsCFL();
                    }
                    else
                    {

                        oItem = oForm.Items.Item("txtRefSAP");
                        oItem.Visible = false;
                        oItem = oForm.Items.Item("cmbRefKF");
                        oItem.Visible = false;
                        ReffWS = new RefaccionesWS();
                        List<DetalleRefaccion> dataRef = ReffWS.SolicitarRefacciones(this.Config.UserFull.Dependencia.EmpresaID);
                        ComboBox oCombo = oItem.Specific;
                        KananSAP.Helper.KananSAPHelpers.CleanComboBox(oCombo);
                        oCombo.ValidValues.Add(" ", "Selecciona la refacción");
                        foreach (DetalleRefaccion rfaccion in dataRef)
                        {
                            string value = rfaccion.ArticuloID.ToString();
                            string description = string.Format("{0} - {1}", value, rfaccion.NombreArticulo.ToString().Trim());
                            oCombo.ValidValues.Add(rfaccion.NombreArticulo.ToString().Trim(), description.Trim());
                        }
                        oCombo.Item.DisplayDesc = true;
                    }
                }
            }
            catch
            {
            }
        
        }
        public void AddItemsCFL()
        {
            try
            {


                Item oItem = null;
                oForm.DataSources.UserDataSources.Add("EditDS", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);
                SAPbouiCOM.ChooseFromListCollection oCFLs = null;
                oCFLs = oForm.ChooseFromLists;
                SAPbouiCOM.ChooseFromList oCFL = null;
                SAPbouiCOM.ChooseFromListCreationParams oCFLCreationParams = null;
                oCFLCreationParams = ((SAPbouiCOM.ChooseFromListCreationParams)(SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)));
                oCFLCreationParams.MultiSelection = false;
                oCFLCreationParams.ObjectType = "4";
                oCFLCreationParams.UniqueID = "CFL10";

                oCFL = oCFLs.Add(oCFLCreationParams);
                SAPbouiCOM.Conditions oConditions = this.SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_Conditions);
                oConditions = oCFL.GetConditions();
                SAPbouiCOM.Condition oCondition = oConditions.Add();
                oCondition.Alias = "ItmsGrpCod";
                oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
                oCondition.CondVal = new GestionGlobales().iFiltroGrupo(this.oCompany, "REFACCIONES KF");
                oCFL.SetConditions(oConditions);
                oItem = oForm.Items.Item("txtRefSAP");
                EditText oEditText = (EditText)(oItem.Specific);
                oEditText.DataBind.SetBound(true, "", "EditDS");
                oEditText.ChooseFromListUID = "CFL10";
                oEditText.ChooseFromListAlias = "ItemCode";

            }
            catch (Exception ex)
            {
                KananSAPHerramientas.LogError("OrdenServicioSinAlertaView.cs->AddItemsCFL()", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public void ShowForm(int? step, bool exist)
        {
            try
            {
                //Se quitó el if para poder visualizar una orden eliminada pero no podermodificarla.
                //if (step == 4)
                //return;
                oForm.Visible = true;
                oForm.Select();
            }
            catch (Exception ex)
            {
                //Se quitó el if para poder visualizar una orden eliminada pero no podermodificarla.
                //if (step == 4)
                //return;
                this.LoadForm(step, exist);
                oForm.Left = (SBO_Application.Desktop.Width - oForm.Width) / 2;
                oForm.Top = (SBO_Application.Desktop.Height - oForm.Height) / 2;
                oForm.Visible = true;
                oForm.Select();

            }
        }

        public void CloseForm()
        {
            if (oForm.Visible & oForm.Selected)
                oForm.Close();
        }

        /// <summary>
        /// Establece los UserDataSource para los controles que lo requieran
        /// </summary>
        private void SetBoundsToControl()
        {
            try
            {
                oEditText = oForm.Items.Item("txtFechaOS").Specific as EditText;
                oEditText.DataBind.SetBound(true, "", "UD_DateOS");
                oEditText = oForm.Items.Item("txtCostoOS").Specific as EditText;
                oEditText.DataBind.SetBound(true, "", "UD_Costo");
                oEditText = oForm.Items.Item("txtImpOSv").Specific as EditText;
                oEditText.DataBind.SetBound(true, "", "UD_IVA");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Establece los campos de la ventana para solicitud de orden de servicio o orden solicitada
        /// </summary>
        private void PrepareOrdenSolicitada()
        {
            try
            {
                oItem = oForm.Items.Item("cmbestatus");
                oItem.Visible = true;
                oItem.Enabled = false;
                SelectComboBoxValue("cmbestatus", "1");

                oItem = oForm.Items.Item("lblProvOS");
                oItem.Visible = false;
                oItem = oForm.Items.Item("comboProOS");
                oItem.Visible = false;

                oItem = oForm.Items.Item("lblFoliOS");
                oItem.Visible = false;
                oItem = oForm.Items.Item("txtFolioOS");
                oItem.Visible = false;

                oItem = oForm.Items.Item("lblMPOS");
                oItem.Visible = false;
                oItem = oForm.Items.Item("comboMPOSv");
                oItem.Visible = false;

                oItem = oForm.Items.Item("lblDetOS");
                oItem.Visible = false;
                oItem = oForm.Items.Item("txtDetOSv");
                oItem.Visible = false;

                oItem = oForm.Items.Item("lblCost");
                oItem.Visible = false;
                oItem = oForm.Items.Item("Item_1");
                oItem.Visible = false;
                oItem = oForm.Items.Item("lblCostV");
                oItem.Visible = false;
                oItem = oForm.Items.Item("Item_22");
                oItem.Visible = false;

                oItem = oForm.Items.Item("lblImpOS");
                oItem.Visible = false;
                oItem = oForm.Items.Item("txtImpOSv");
                oItem.Visible = false;

                oItem = oForm.Items.Item("lblCostOS");
                oItem.Visible = false;

                oItem = oForm.Items.Item("txtCostoOS");
                oItem.Visible = false;
                
                oFolder = oForm.Items.Item("tabDataRef").Specific;
                oFolder.Item.Visible = false;

                oItem = oForm.Items.Item("txtEnPro");
                oItem.Enabled = true;

                oItem = oForm.Items.Item("txtSalPro");
                oItem.Enabled = true;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Oculta los campos de ingreso de costos.
        /// </summary>
        public void HideCosto()
        {
            try
            {
                oItem = oForm.Items.Item("lblCostOS");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                //oItem.Visible = false;
                oItem.Enabled = false;

                oItem = oForm.Items.Item("txtCostoOS");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                //oItem.Visible = false;
                oItem.Enabled = false;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al ocultar campos de costo. Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Muestra los campos de ingreso de costos.
        /// </summary>
        public void ShowCosto()
        {
            try
            {
                oItem = oForm.Items.Item("lblCostOS");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                //oItem.Visible = false;
                oItem.Enabled = true;

                oItem = oForm.Items.Item("txtCostoOS");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                //oItem.Visible = false;
                oItem.Enabled = true;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al mostrar campos de costo. Error: " + ex.Message);
            }
        }

        /// <summary>
        /// Establece los campos de la ventana para orden de servicio aprobada
        /// </summary>
        private void PrepareOrdenAprobada()
        {
            try
            {
                oItem = oForm.Items.Item("Item_3");
                oItem.Enabled = false;
                oItem = oForm.Items.Item("cmbsuc");
                oItem.Enabled = false;

                oItem = oForm.Items.Item("lblProvOS");
                oItem.Enabled = false;
                oItem = oForm.Items.Item("comboProOS");
                oItem.Enabled = false;

                oItem = oForm.Items.Item("lblFchOS");
                oItem.Enabled = false;
                oItem = oForm.Items.Item("txtFechaOS");
                oItem.Enabled = false;

                oItem = oForm.Items.Item("lblDesOS");
                oItem.Enabled = false;
                oItem = oForm.Items.Item("txtDescOs");
                oItem.Enabled = false;

                oItem = oForm.Items.Item("lblRsp");
                oItem.Enabled = false;
                oItem = oForm.Items.Item("cmbResp");
                oItem.Enabled = false;

                oItem = oForm.Items.Item("lblAprob");
                oItem.Enabled = false;
                oItem = oForm.Items.Item("cmbAprob");
                oItem.Enabled = false;

                oFolder = oForm.Items.Item("tabDataRef").Specific;
                oFolder.Item.Visible = true;
                oFolder.Item.Enabled = false;

                SetEstatusAprobada();
                SelectComboBoxValue("cmbestatus", "2");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Establece los campos de la ventana para orden de servicio atendida
        /// </summary>
        private void PrepareOrdenAtendida()
        {
            try
            {
                oEditText = oForm.Items.Item("txtFechaOS").Specific;//Fecha
                oEditText.Item.Enabled = false;
                oEditText = oForm.Items.Item("txtFolioOS").Specific;//Folio
                oEditText.Item.Enabled = false;
                oEditText = oForm.Items.Item("txtDescOs").Specific;//Descripcion
                oEditText.Item.Enabled = false;

                oCombo = oForm.Items.Item("comboMPOSv").Specific;//Combo Metodo de pago
                oCombo.Item.Enabled = false;
                oItem = oForm.Items.Item("txtDetOSv");//DetallePago
                oItem.Enabled = false;
                oEditText = oForm.Items.Item("txtImpOSv").Specific;//Impuesto
                oEditText.Item.Enabled = false;

                oStaticText = oForm.Items.Item("lblCostV").Specific;
                oStaticText.Item.Enabled = false;

                oCombo = oForm.Items.Item("cmbResp").Specific;//Combo Responsable
                oCombo.Item.Enabled = false;
                oCombo = oForm.Items.Item("cmbsuc").Specific;//Combo Sucursal
                oCombo.Item.Enabled = false;
                oCombo = oForm.Items.Item("cmbAprob").Specific;//Combo Aprobador
                oCombo.Item.Enabled = false;
                oCombo = oForm.Items.Item("comboProOS").Specific;//Combo Proveedor
                oCombo.Item.Enabled = false;

                oEditText = oForm.Items.Item("txtCostoOS").Specific;//Costo
                oEditText.Item.Enabled = false;
                oCombo = oForm.Items.Item("comboVehOS").Specific;//Combo Vehiculo
                oCombo.Item.Enabled = false;
                oCombo = oForm.Items.Item("comoServOS").Specific;//Combo Servicio
                oCombo.Item.Enabled = false;
                oCombo = oForm.Items.Item("cmbestatus").Specific;//Combo Estatus
                oCombo.Item.Enabled = true;

                //oFolder = oForm.Items.Item("tabDataOrd").Specific;
                //oFolder.Select();
                SelectComboBoxValue("cmbestatus", "5");
                oButton = oForm.Items.Item("btnOSvDone").Specific;
                oButton.Item.Enabled = false;

                oButton = oForm.Items.Item("btnDelOSv").Specific;
                oButton.Item.Enabled = false;

                oButton = oForm.Items.Item("btnEdtOSv").Specific;
                oButton.Item.Enabled = false;
                oButton = oForm.Items.Item("btnSaveOS").Specific;
                oButton.Item.Enabled = true;

                oButton = oForm.Items.Item("btnDelRf").Specific;
                oButton.Item.Enabled = false;

                oButton = oForm.Items.Item("btnRefn").Specific;
                oButton.Item.Enabled = false;

                oFolder = oForm.Items.Item("tabDataRef").Specific;
                oFolder.Item.Visible = true;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Establece los campos de la ventana para orden de servicio realizada
        /// </summary>
        private void PrepareOrdenRealizada()
        {
            try
            {
                oEditText = oForm.Items.Item("txtFechaOS").Specific;//Fecha
                oEditText.Item.Enabled = false;
                oEditText = oForm.Items.Item("txtFolioOS").Specific;//Folio
                oEditText.Item.Enabled = false;
                oEditText = oForm.Items.Item("txtDescOs").Specific;//Descripcion
                oEditText.Item.Enabled = false;
                oEditText = oForm.Items.Item("txtQntyRf").Specific;//Catidad refacciones
                oEditText.Item.Enabled = false;
                oEditText = oForm.Items.Item("txtCostoRf").Specific;//Costo refacciones
                oEditText.Item.Enabled = false;
                oEditText = oForm.Items.Item("txtCostoOS").Specific;//Costo
                oEditText.Item.Enabled = false;
                oEditText = oForm.Items.Item("txtImpOSv").Specific;//Impuesto
                oEditText.Item.Enabled = false;

                //oFolder = oForm.Items.Item("tabDataOrd").Specific;
                //oFolder.Select();
                oFolder.Item.Enabled = true;
                oFolder = oForm.Items.Item("tabDataSer").Specific;
                oFolder.Item.Enabled = true;
                oFolder = oForm.Items.Item("tabDataRef").Specific;
                oFolder.Item.Enabled = true;

                oItem = oForm.Items.Item("txtDetOSv");//DetallePago
                oItem.Enabled = false;

                oStaticText = oForm.Items.Item("lblCostV").Specific;
                oStaticText.Item.Enabled = false;

                oCombo = oForm.Items.Item("cmbResp").Specific;//Combo Responsable
                oCombo.Item.Enabled = false;
                oCombo = oForm.Items.Item("cmbsuc").Specific;//Combo Sucursal
                oCombo.Item.Enabled = false;
                oCombo = oForm.Items.Item("cmbAprob").Specific;//Combo Aprobador
                oCombo.Item.Enabled = false;
                oCombo = oForm.Items.Item("comboProOS").Specific;//Combo Proveedor
                oCombo.Item.Enabled = false;
                oCombo = oForm.Items.Item("comboMPOSv").Specific;//Combo Metodo de pago
                oCombo.Item.Enabled = false;
                oCombo = oForm.Items.Item("comboVehOS").Specific;//Combo Vehiculo
                oCombo.Item.Enabled = false;
                oCombo = oForm.Items.Item("comoServOS").Specific;//Combo Servicio
                oCombo.Item.Enabled = false;
                oCombo = oForm.Items.Item("cmbestatus").Specific;//Combo Estatus
                oCombo.Item.Enabled = false;
                SelectComboBoxValue("cmbestatus", "6");
                oCombo = oForm.Items.Item("cmbAlmacn").Specific;//Combo Almacen refacciones
                oCombo.Item.Enabled = false;
                oCombo = oForm.Items.Item("cmbRefKF").Specific;//Combo refacciónes
                oCombo.Item.Enabled = false;

                oButton = oForm.Items.Item("btnOSvDone").Specific;
                oButton.Item.Enabled = false;
                oButton = oForm.Items.Item("btnDelOSv").Specific;
                oButton.Item.Enabled = false;
                oButton = oForm.Items.Item("btnEdtOSv").Specific;
                oButton.Item.Enabled = false;
                oButton = oForm.Items.Item("btnSaveOS").Specific;
                oButton.Item.Enabled = false;
                oButton = oForm.Items.Item("btnRefn").Specific;/*Botón agregar refacción*/
                oButton.Item.Enabled = false;
                oButton = oForm.Items.Item("btnDelRf").Specific;/*Botón borrar refacción*/
                oButton.Item.Enabled = false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Establece los campos de la ventana de una Orden de Servicio rechazada
        /// </summary>
        private void PrepareOrdenRechazada()
        {
            try
            {
                oEditText = oForm.Items.Item("txtFechaOS").Specific;//Fecha
                oEditText.Item.Enabled = false;
                oEditText = oForm.Items.Item("txtFolioOS").Specific;//Folio
                oEditText.Item.Enabled = false;
                oEditText = oForm.Items.Item("txtDescOs").Specific;//Descripcion
                oEditText.Item.Enabled = false;

                oCombo = oForm.Items.Item("comboMPOSv").Specific;//Combo Metodo de pago
                oCombo.Item.Enabled = false;
                oItem = oForm.Items.Item("txtDetOSv");//DetallePago
                oItem.Enabled = false;
                oEditText = oForm.Items.Item("txtImpOSv").Specific;//Impuesto
                oEditText.Item.Enabled = false;

                oStaticText = oForm.Items.Item("lblCostV").Specific;
                oStaticText.Item.Enabled = false;

                oCombo = oForm.Items.Item("cmbResp").Specific;//Combo Responsable
                oCombo.Item.Enabled = false;
                oCombo = oForm.Items.Item("cmbsuc").Specific;//Combo Sucursal
                oCombo.Item.Enabled = false;
                oCombo = oForm.Items.Item("cmbAprob").Specific;//Combo Aprobador
                oCombo.Item.Enabled = false;
                oCombo = oForm.Items.Item("comboProOS").Specific;//Combo Proveedor
                oCombo.Item.Enabled = false;

                oEditText = oForm.Items.Item("txtCostoOS").Specific;//Costo
                oEditText.Item.Enabled = false;
                oCombo = oForm.Items.Item("comboVehOS").Specific;//Combo Vehiculo
                oCombo.Item.Enabled = false;
                oCombo = oForm.Items.Item("comoServOS").Specific;//Combo Servicio
                oCombo.Item.Enabled = false;
                oCombo = oForm.Items.Item("cmbestatus").Specific;//Combo Estatus
                oCombo.Item.Enabled = false;

                //oFolder = oForm.Items.Item("tabDataOrd").Specific;
                //oFolder.Select();
                SelectComboBoxValue("cmbestatus", "3");

                oButton = oForm.Items.Item("btnOSvDone").Specific;
                oButton.Item.Enabled = false;

                oButton = oForm.Items.Item("btnDelOSv").Specific;
                oButton.Item.Enabled = false;

                oButton = oForm.Items.Item("btnEdtOSv").Specific;
                oButton.Item.Enabled = false;
                oButton = oForm.Items.Item("btnSaveOS").Specific;
                oButton.Item.Enabled = false;

                oButton = oForm.Items.Item("btnDelRf").Specific;
                oButton.Item.Enabled = false;

                oButton = oForm.Items.Item("btnRefn").Specific;
                oButton.Item.Enabled = false;

                oFolder = oForm.Items.Item("tabDataRef").Specific;
                oFolder.Item.Visible = false;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Establece los campos de la ventana de ua Orden de Servicio Eliminada
        /// </summary>
        private void PrepareOrdenEliminada()
        {
            try
            {
                oEditText = oForm.Items.Item("txtFechaOS").Specific;//Fecha
                oEditText.Item.Enabled = false;
                oEditText = oForm.Items.Item("txtFolioOS").Specific;//Folio
                oEditText.Item.Enabled = false;
                oEditText = oForm.Items.Item("txtDescOs").Specific;//Descripcion
                oEditText.Item.Enabled = false;

                oCombo = oForm.Items.Item("comboMPOSv").Specific;//Combo Metodo de pago
                oCombo.Item.Enabled = false;
                oItem = oForm.Items.Item("txtDetOSv");//DetallePago
                oItem.Enabled = false;
                oEditText = oForm.Items.Item("txtImpOSv").Specific;//Impuesto
                oEditText.Item.Enabled = false;

                oStaticText = oForm.Items.Item("lblCostV").Specific;
                oStaticText.Item.Enabled = false;

                oCombo = oForm.Items.Item("cmbResp").Specific;//Combo Responsable
                oCombo.Item.Enabled = false;
                oCombo = oForm.Items.Item("cmbsuc").Specific;//Combo Sucursal
                oCombo.Item.Enabled = false;
                oCombo = oForm.Items.Item("cmbAprob").Specific;//Combo Aprobador
                oCombo.Item.Enabled = false;
                oCombo = oForm.Items.Item("comboProOS").Specific;//Combo Proveedor
                oCombo.Item.Enabled = false;

                oEditText = oForm.Items.Item("txtCostoOS").Specific;//Costo
                oEditText.Item.Enabled = false;
                oCombo = oForm.Items.Item("comboVehOS").Specific;//Combo Vehiculo
                oCombo.Item.Enabled = false;
                oCombo = oForm.Items.Item("comoServOS").Specific;//Combo Servicio
                oCombo.Item.Enabled = false;
                oCombo = oForm.Items.Item("cmbestatus").Specific;//Combo Estatus
                oCombo.Item.Enabled = false;

                //oFolder = oForm.Items.Item("tabDataOrd").Specific;
                //oFolder.Select();
                SelectComboBoxValue("cmbestatus", "4");

                oButton = oForm.Items.Item("btnOSvDone").Specific;
                oButton.Item.Enabled = false;

                oButton = oForm.Items.Item("btnDelOSv").Specific;
                oButton.Item.Enabled = false;

                oButton = oForm.Items.Item("btnEdtOSv").Specific;
                oButton.Item.Enabled = false;
                oButton = oForm.Items.Item("btnSaveOS").Specific;
                oButton.Item.Enabled = false;

                oButton = oForm.Items.Item("btnDelRf").Specific;
                oButton.Item.Enabled = false;

                oButton = oForm.Items.Item("btnRefn").Specific;
                oButton.Item.Enabled = false;

                oFolder = oForm.Items.Item("tabDataRef").Specific;
                oFolder.Item.Visible = false;

                oEditText = oForm.Items.Item("txtImpOSv").Specific;
                oEditText.Item.Enabled = false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region FILL COMBO
        private void FillComboTipoPago()
        {
            try
            {
                string query = "select * from [@VSKF_TIPODEPAGO] where U_Estatus = 1";
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    oItem = oForm.Items.Item("comboMPOSv");
                    oCombo = oItem.Specific;
                    Helper.KananSAPHelpers.CleanComboBox(oCombo);//this.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Ninguno");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("Code").Value);
                        string description = rs.Fields.Item("Name").Value;
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }
                    oCombo.Item.DisplayDesc = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void FillComboSucursales()
        {
            try
            {
                string query = "select * from [@VSKF_SUCURSAL]";
                //query += string.Format(" where U_Propietario = {0} ", this.Config.UserFull.Dependencia.EmpresaID);
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    oItem = oForm.Items.Item("cmbsuc");
                    oCombo = oItem.Specific;
                    Helper.KananSAPHelpers.CleanComboBox(oCombo);//this.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Ninguno");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("U_SucursalID").Value);
                        string description = rs.Fields.Item("Name").Value;
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }
                    oCombo.Item.DisplayDesc = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void FillComboProveedores()
        {
            try
            {
                string query = @"select * from ""@VSKF_PROVEEDOR"" ";
                //query += string.Format(" where U_Propietario = {0} ", this.Config.UserFull.Dependencia.EmpresaID);
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    oItem = oForm.Items.Item("comboProOS");
                    oCombo = oItem.Specific;
                    Helper.KananSAPHelpers.CleanComboBox(oCombo);//this.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Ninguno");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("U_ProveedorID").Value);
                        string description = rs.Fields.Item("U_Nombre").Value;
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }
                    oCombo.Item.DisplayDesc = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void FillComboServicios()
        {
            try
            {
                ServicioSAP svData = new ServicioSAP(this.oCompany);
                Servicio sv = new Servicio();
                sv.Propietario = new Kanan.Operaciones.BO2.Empresa();
                sv.Propietario.EmpresaID = this.Config.UserFull.Dependencia.EmpresaID;
                SAPbobsCOM.Recordset rs = svData.Consultar(sv);
                if (svData.HashServicios(rs))
                {
                    oItem = oForm.Items.Item("comoServOS");
                    oCombo = oItem.Specific;
                    Helper.KananSAPHelpers.CleanComboBox(oCombo);//this.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Ninguno");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("U_ServicioID").Value);
                        string description = rs.Fields.Item("Servicio").Value;
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }
                    oCombo.Item.DisplayDesc = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void FillComboVehiculos()
        {
            try
            {
                string query = string.Empty;
                query = " select vh.U_VehiculoID, vh.U_Nombre from [@VSKF_VEHICULO] vh ";
                if (this.Config.UserFull.Dependencia.EmpresaID.HasValue)
                    query += string.Format(" where vh.U_EmpresaID = {0} ", this.Config.UserFull.Dependencia.EmpresaID);
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    oItem = oForm.Items.Item("comboVehOS");
                    oCombo = oItem.Specific;
                    Helper.KananSAPHelpers.CleanComboBox(oCombo);//this.CleanComboBox(oCombo);
                    oCombo.ValidValues.Add("0", "Ninguno");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("U_VehiculoID").Value);
                        string description = rs.Fields.Item("U_Nombre").Value;
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }
                    oCombo.Item.DisplayDesc = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void FillComboEmpleados()
        {
            try
            {
                string query = string.Empty;
                query = " select e.Name, e.U_KFEmpleadoID from [@VSKF_RELEMPLEADO] e ";
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    oItem = oForm.Items.Item("cmbResp");
                    oCombo = oItem.Specific;
                    ComboBox cmb = oForm.Items.Item("cmbAprob").Specific;
                    Helper.KananSAPHelpers.CleanComboBox(oCombo);
                    Helper.KananSAPHelpers.CleanComboBox(cmb);
                    oCombo.ValidValues.Add("0", "Ninguno");
                    cmb.ValidValues.Add("0", "Ninguno");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("U_KFEmpleadoID").Value);
                        string description = rs.Fields.Item("Name").Value;
                        oCombo.ValidValues.Add(value, description);
                        cmb.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }
                    oCombo.Item.DisplayDesc = true;
                    cmb.Item.DisplayDesc = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void FillComboEstatus()
        {
            try
            {
                string query = "select * from [@VSKF_ESTATUSORDEN] where U_Activo = 1";
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    oItem = oForm.Items.Item("cmbestatus");
                    oCombo = oItem.Specific;
                    Helper.KananSAPHelpers.CleanComboBox(oCombo); //this.CleanComboBox(oCombo);
                    //oCombo.ValidValues.Add("0", "Ninguno");
                    rs.MoveFirst();
                    for (int i = 0; i < rs.RecordCount; i++)
                    {
                        string value = Convert.ToString(rs.Fields.Item("Code").Value);
                        string description = rs.Fields.Item("Name").Value;
                        oCombo.ValidValues.Add(value, description);
                        rs.MoveNext();
                    }
                    oCombo.Item.DisplayDesc = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Establece los valores de los combos
        /// </summary>
        private void FillCombos()
        {
            FillComboTipoPago();
            FillComboServicios();
            FillComboVehiculos();
            FillComboSucursales();
            FillComboProveedores();
            FillComboEmpleados();
            FillComboEstatus();
            //Folder(Tabs)
            /*oFolder = oForm.Items.Item("tabDataOrd").Specific;
            oFolder.DataBind.SetBound(true, "", "tabOrd");
            oFolder.Item.Width = 200;
            oFolder.Select();
            oFolder = oForm.Items.Item("tabDataSer").Specific;
            oFolder.DataBind.SetBound(true, "", "tabOrd");
            oFolder.Item.Width = 200;
            oFolder.GroupWith("tabDataOrd");*/
        }

        #endregion

        #region Complemento Form

        /// <summary>
        /// Obtiene el costo mostrado en el formulario de la Orden de servicio.
        /// </summary>
        /// <returns></returns>
        public Decimal GetCostoForm()
        {
            try
            {
                Decimal Costo;

                //Se obtiene el costo del formulario
                oStaticText = oForm.Items.Item("lblCostV").Specific;
                Costo = String.IsNullOrEmpty(oStaticText.Caption) ? Costo = Convert.ToDecimal(oStaticText.Caption) : Costo = 0;

                return Costo;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar obtener el costo actual en el formulario. " + ex.Message);
            }
        }

        /// <summary>
        /// Verifica si el tipo de pago seleccionado por el usuario es efectivo
        /// </summary>
        /// <returns></returns>
        public bool isEfectivo()
        {
            try
            {
                oItem = null;
                oItem = oForm.Items.Item("comboMPOSv");
                ComboBox cbx = oItem.Specific;
                string val = cbx.Value;
                if (!string.IsNullOrEmpty(val))
                {
                    if (val.Equals("4"))
                        return true;
                    else
                        return false;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Muestra control detalle de pago para capturar en caso de ser un tipo de pago diferente de efectivo
        /// </summary>
        public void ShowDetallePago()
        {
            try
            {
                oItem = oForm.Items.Item("lblDetOS");
                oItem.Visible = true;
                oItem = oForm.Items.Item("txtDetOSv");
                oEditText = oItem.Specific;
                oEditText.String = string.Empty;
                oItem.Visible = true;
                oItem = oForm.Items.Item("txtDetOSv");
                oItem.Click(BoCellClickType.ct_Regular);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Oculta el control detalle pago en caso de ser un tipo de pago en efectivo
        /// </summary>
        public void HiddeDetallePago()
        {
            try
            {
                oItem = oForm.Items.Item("lblDetOS");
                oItem.Visible = false;
                oItem = oForm.Items.Item("txtDetOSv");
                oItem.Visible = false;
                oItem = oForm.Items.Item("txtImpOSv");
                oItem.Click(BoCellClickType.ct_Regular);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Recupera el folio de sistema correspondiente a la siguiente orden
        /// </summary>
        private void SetCurrentFolioOrden()
        {
            try
            {
                String folio = String.Empty;
                String anio = String.Empty;
                /*Comentado para evitar no contar insercion por insercion erronea.*/
                //Recordset rs = this.EFolio.ConsultarTodas();

                Recordset CountFolio = this.Entity.ConsultarTodos();
                oStaticText = oForm.Items.Item("txtfolio").Specific;

                if (this.Entity.HashOrdenServicio(CountFolio))//(EFolio.HashFolioOrden(rs))
                {
                    #region Antiguo
                    //Forma antigua
                    /*CountFolio.MoveFirst();
                    EFolio.oUDT.GetByKey(CountFolio.Fields.Item("Code").Value);
                    EFolio.UDTToFolio();
                    EFolio.folio.Folio = ((EFolio.folio.Folio) + 1);
                    folio = EFolio.folio.Folio.ToString();
                    anio = EFolio.folio.AnioFolio.ToString();*/
                    #endregion Antiguo

                    //Forma nueva
                    this.EFolio.folio = new FolioOrden
                    {
                        Propietario = new Empresa
                        {
                            EmpresaID = this.Config.UserFull.Dependencia.EmpresaID
                        }
                    };

                    folio = (CountFolio.RecordCount + 1).ToString();
                    anio = DateTime.Today.Year.ToString();
                    this.EFolio.folio.Folio = Convert.ToInt32(folio);
                    this.EFolio.folio.AnioFolio = Convert.ToInt32(anio);
                    oStaticText.Caption = string.Format("{0}/{1}", folio, anio);
                }
                else
                {
                    oStaticText.Caption = "1/" + DateTime.UtcNow.Year;
                    int Anio = Convert.ToInt32(oStaticText.Caption.Split('/')[1]);
                    int Folio = Convert.ToInt32(oStaticText.Caption.Split('/')[0]);
                    this.EFolio.folio = new FolioOrden
                    {
                        AnioFolio = Anio,
                        Folio = Folio,
                        Propietario = new Empresa
                        {
                            EmpresaID = this.Config.UserFull.Dependencia.EmpresaID
                        }
                    };
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// habilita o desabilita el combo de estatus
        /// </summary>
        public void SetEstatusSolicitada()
        {
            oCombo = oForm.Items.Item("cmbestatus").Specific as ComboBox;
            //oCombo.ValidValues.Remove("1");
            oCombo.ValidValues.Remove("4");
            oCombo.ValidValues.Remove("5");
        }
        /// <summary>
        /// Prepara el combo estatus para atender o eliminar orden
        /// </summary>
        public void SetEstatusAprobada()
        {
            oCombo = oForm.Items.Item("cmbestatus").Specific as ComboBox;
            oCombo.ValidValues.Remove("1");
            oCombo.ValidValues.Remove("3");
            oCombo.ValidValues.Remove("4");
            //oCombo.ValidValues.Remove("6");
        }
        /// <summary>
        /// Prepara el combo estatus para atender o eliminar orden
        /// </summary>
        public void SetEstatusAtendida()
        {
            oCombo = oForm.Items.Item("cmbestatus").Specific as ComboBox;
            oCombo.ValidValues.Remove("1");
            //oCombo.ValidValues.Remove("2");
            oCombo.ValidValues.Remove("4");
        }
        /// <summary>
        /// Prepara el combo estatus para atender o eliminar orden
        /// </summary>
        public void ComboEstatusRealizada()
        {
            oCombo = oForm.Items.Item("cmbestatus").Specific as ComboBox;
            oCombo.Select("6", BoSearchKey.psk_ByDescription);
        }
        /// <summary>
        /// RSelecciona el value especificado en un combobox
        /// </summary>
        public void SelectComboBoxValue(string comboname, string value)
        {
            oCombo = oForm.Items.Item(comboname).Specific;
            oCombo.Select(value, BoSearchKey.psk_ByValue);
        }
        #endregion

        #region Servicios
        /// <summary>
        /// Pasa los datos de la interface al objeto DetalleOrden
        /// </summary>
        public void FormToDetalleOrden()
        {
            this.EntityDetalle.oDetalleOrden = new DetalleOrden();

            oStaticText = oForm.Items.Item("lblsvcid").Specific;
            EntityDetalle.oDetalleOrden.DetalleOrdenID = !string.IsNullOrEmpty(oStaticText.Caption) ? Convert.ToInt32(oStaticText.Caption.Trim()) : (int?)null;

            EntityDetalle.oDetalleOrden.TipoMantenibleID = 1;
            EntityDetalle.oDetalleOrden.Mantenible = new Vehiculo();
            //Vehiculo
            oCombo = oForm.Items.Item("comboVehOS").Specific;
            (EntityDetalle.oDetalleOrden.Mantenible as Vehiculo).VehiculoID = Convert.ToInt32(oCombo.Value);
            (EntityDetalle.oDetalleOrden.Mantenible as Vehiculo).Nombre = oCombo.Selected.Description;

            //Servicio
            oCombo = oForm.Items.Item("comoServOS").Specific;
            EntityDetalle.oDetalleOrden.Servicio.ServicioID = Convert.ToInt32(oCombo.Value);
            EntityDetalle.oDetalleOrden.Servicio.Nombre = oCombo.Selected.Description;
            //Costo
            oEditText = oForm.Items.Item("txtCostoOS").Specific;
            //Se agregó el Replace para evitar un error en la validación por las comas.
            oEditText.String = oEditText.String.Replace(",", "").Replace(".00", "");
            EntityDetalle.oDetalleOrden.Costo = !string.IsNullOrEmpty(oEditText.String) ? Convert.ToDecimal(oEditText.String.Trim()) : 0;
            //OrdenServicio
            oStaticText = oForm.Items.Item("lblid").Specific;
            EntityDetalle.oDetalleOrden.OrdenServicio.OrdenServicioID = !string.IsNullOrEmpty(oStaticText.Caption) ? Convert.ToInt32(oStaticText.Caption.Trim()) : (int?)null;

        }
        public Boolean FormToListRefaccion()
        {
            Boolean bContinuar = false;
            DetalleRefaccion oTemp = new DetalleRefaccion();
            if (Refacciones == null)
                Refacciones = new List<DetalleRefaccion>();
            Decimal Quantity = 0;
            string sresponse = string.Empty, sconsultaSql = string.Empty;
            SBO_KF_Configuracion_INFO oSetting = new SBO_KF_Configuracion_INFO();
            SBO_KF_Configuracion_AD oConfiguracion = new SBO_KF_Configuracion_AD();
            sresponse = oConfiguracion.recuperaConfiguracion(ref oCompany, out sconsultaSql, ref oSetting);
            if (sresponse == "OK")
            {

                if (oSetting.cKANAN == 'N')
                    oTemp.Articulo = RefaccionVista.txtRefSAP.String.Trim();
                else
                    oTemp.Articulo = RefaccionVista.cmbRefaccion.Value;

                Quantity = Convert.ToDecimal(RefaccionVista.txtQntyRf.String.Trim());
                oTemp.Cantidad = Quantity;
                Quantity = Convert.ToDecimal(RefaccionVista.txtCostoRf.String.Trim());
                oTemp.Costo = Quantity;
                oTemp.Almacen = RefaccionVista.cmbAlmacn.Selected.Description.Trim();
                oTemp.Almacen = oTemp.Almacen.Substring(oTemp.Almacen.LastIndexOf('-') + 1);
                oTemp.CodigoAlmacen = RefaccionVista.cmbAlmacn.Value;
                oTemp.Total = Math.Round(Convert.ToDecimal(oTemp.Cantidad * oTemp.Costo), 4);
                Refacciones.Add(oTemp);

                //Comentado debido a que una variable estática controla el total de las refacciones.
                /*Decimal CostoActual = Convert.ToDecimal(Entity.OrdenServicio.Costo);
                Decimal NuevoCosto = Convert.ToDecimal(CostoActual + oTemp.Total);
                Entity.OrdenServicio.Costo = NuevoCosto;*/

                /*Se modificó, ahora una variable estática controla el costo total
                 debido a que se sobre escribían valores.*/
                //Costototal
                /*oStaticText = oForm.Items.Item("lblCostV").Specific;
                oStaticText.Caption = Entity.OrdenServicio.Costo.ToString();*/
            }
            return bContinuar;
        }
        public bool EliminarLineaRefaccion()
        {
            try
            {
                var Refaccion = Refacciones.FirstOrDefault(x => x.iLineaID == iLineaRefaccion);
                if (Refaccion != null)
                {
                    Decimal CostoActual = Convert.ToDecimal(Entity.OrdenServicio.Costo);
                    Decimal NuevoCosto = Convert.ToDecimal(CostoActual - Refaccion.Total);
                    Entity.OrdenServicio.Costo = NuevoCosto;
                    oStaticText = oForm.Items.Item("lblCostV").Specific;
                    oStaticText.Caption = Entity.OrdenServicio.Costo.ToString();

                    iLineaRefaccion = -1;
                    Refacciones.Remove(Refaccion);
                    this.ListToMatrix();
                    return true;
                }
            }
            catch
            {
            }

            return false;
        }
        public void LimpiarObjctRefacc()
        {
            RefaccionVista.txtCostoRf.String = "0.00";
            RefaccionVista.txtQntyRf.String = "0.00";
             string sresponse = string.Empty, sconsultaSql = string.Empty;
            SBO_KF_Configuracion_INFO oSetting = new SBO_KF_Configuracion_INFO();
            SBO_KF_Configuracion_AD oConfiguracion = new SBO_KF_Configuracion_AD();
            sresponse = oConfiguracion.recuperaConfiguracion(ref oCompany, out sconsultaSql, ref oSetting);
            if (sresponse == "OK")
            {
                if (oSetting.cKANAN == 'N')
                    RefaccionVista.txtRefSAP.String = "";
                try
                {
                    RefaccionVista.ocmbAlmacen.Enabled = false;
                    RefaccionVista.cmbAlmacn.Select(" ", BoSearchKey.psk_ByValue);
                    RefaccionVista.ocmbAlmacen.Enabled = true;

                    if (oSetting.cKANAN == 'Y')
                    {
                        RefaccionVista.ocmbReffcion.Enabled = false;
                        RefaccionVista.cmbRefaccion.Select(" ", BoSearchKey.psk_ByValue);
                        RefaccionVista.ocmbReffcion.Enabled = true;
                    }
                }
                catch
                {
                }
            }
        }

        public Boolean ListToMatrix()
        {
            bool bContinuar = false;
            try
            {
                if (Refacciones != null)
                {
                    Matrix oRefacciones = (Matrix)oForm.Items.Item("gdRFa").Specific;
                    Columns oColum = (SAPbouiCOM.Columns)oRefacciones.Columns;
                    oRefacciones.Clear();
                    int iLine = 1;
                    foreach (DetalleRefaccion data in Refacciones)
                    {
                        data.iLineaID = iLine;
                        oForm.DataSources.UserDataSources.Item("oUMAT01").Value = iLine.ToString();
                        oForm.DataSources.UserDataSources.Item("oUMAT02").Value = data.Almacen;
                        oForm.DataSources.UserDataSources.Item("oUMAT03").Value = data.Articulo;
                        oForm.DataSources.UserDataSources.Item("oUMAT04").Value = Convert.ToDecimal(data.Costo).ToString("0.000");//string.Format("$ {0}", data.Costo);
                        oForm.DataSources.UserDataSources.Item("oUMAT05").Value = data.Cantidad.ToString();
                        oForm.DataSources.UserDataSources.Item("oUMAT06").Value = data.Total.ToString();
                        iLine++;
                        oRefacciones.AddRow(1, -1);
                    }
                }
                bContinuar = true;
                this.SetCostoRefacciones();
            }
            catch
            {

            }
            return bContinuar;
        }

        public Boolean FormValidaRefaccion(out String Cadena)
        {
            Cadena = string.Empty;
            Decimal Quantity = 0;

            if (RefaccionVista == null)
                RefaccionVista = new RefaccionesView();

            string sresponse = string.Empty, sconsultaSql = string.Empty;
            SBO_KF_Configuracion_INFO oSetting = new SBO_KF_Configuracion_INFO();
            SBO_KF_Configuracion_AD oConfiguracion = new SBO_KF_Configuracion_AD();
            sresponse = oConfiguracion.recuperaConfiguracion(ref oCompany, out sconsultaSql, ref oSetting);
            if (sresponse == "OK")
            {

                if (oSetting.cKANAN == 'Y')
                {
                    RefaccionVista.ocmbReffcion = oForm.Items.Item("cmbRefKF");
                    RefaccionVista.cmbRefaccion = RefaccionVista.ocmbReffcion.Specific;
                    if (string.IsNullOrEmpty(RefaccionVista.cmbRefaccion.Value.Trim()))
                        Cadena = "Selecciona la refacción gestionada por Kanan Fleet";
                }
                else
                {
                    RefaccionVista.txtRefSAP = oForm.Items.Item("txtRefSAP").Specific;
                    if (string.IsNullOrEmpty(RefaccionVista.txtRefSAP.String.Trim()))
                        Cadena = "Proporciona la refacción gestionada por SAP";
                }

                RefaccionVista.txtQntyRf = oForm.Items.Item("txtQntyRf").Specific;
                Quantity = Convert.ToDecimal(RefaccionVista.txtQntyRf.String.Trim());
                if (Quantity <= 0)
                    Cadena = "Proporciona la cantidad de refacciones";
                RefaccionVista.txtCostoRf = oForm.Items.Item("txtCostoRf").Specific;
                Quantity = Convert.ToDecimal(RefaccionVista.txtCostoRf.String.Trim());
                if (Quantity <= 0)
                    Cadena = "Proporciona el costo por unidad";

                RefaccionVista.ocmbAlmacen = oForm.Items.Item("cmbAlmacn");
                RefaccionVista.cmbAlmacn = RefaccionVista.ocmbAlmacen.Specific;
                if (string.IsNullOrEmpty(RefaccionVista.cmbAlmacn.Value.Trim()))
                    Cadena = "Selecciona el almacén de inventario";
            }

            return string.IsNullOrEmpty(Cadena) ? true : false;
        }

        /// <summary>
        /// Pasa los datos de la interface al objeto DetalleOrden
        /// </summary>
        public void DetalleOrdenToForm()
        {
            oStaticText = oForm.Items.Item("lblsvcid").Specific;
            oStaticText.Caption = EntityDetalle.oDetalleOrden.DetalleOrdenID != null ? EntityDetalle.oDetalleOrden.DetalleOrdenID.ToString() : string.Empty;

            //Vehiculo
            oCombo = oForm.Items.Item("comboVehOS").Specific;
            oCombo.Select((EntityDetalle.oDetalleOrden.Mantenible as Vehiculo).VehiculoID.ToString());

            //Servicio
            oCombo = oForm.Items.Item("comoServOS").Specific;
            oCombo.Select(EntityDetalle.oDetalleOrden.Servicio.ServicioID.ToString());
            //Costo
            oEditText = oForm.Items.Item("txtCostoOS").Specific;
            oEditText.String = EntityDetalle.oDetalleOrden.Costo.ToString();

        }
        /// <summary>
        /// Vacia los campos de los servicios para agregar uno nuevo
        /// </summary>
        public void EmptyServicioToForm()
        {
            #region Datos de los servicios Agregados
            oStaticText = oForm.Items.Item("lblsvcid").Specific;//Costo
            oStaticText.Caption = "";
            oEditText = oForm.Items.Item("txtCostoOS").Specific;//Costo
            oEditText.String = "";
            oCombo = oForm.Items.Item("comboVehOS").Specific;//Combo Vehiculo
            oCombo.Select("0", BoSearchKey.psk_ByValue);
            oCombo = oForm.Items.Item("comoServOS").Specific;//Combo Servicio
            oCombo.Select("0", BoSearchKey.psk_ByValue);

            //oCombo = oForm.Items.Item("comboMPOSv").Specific;//Combo Metodo de pago
            //oCombo.Select("psk_ByValue", BoSearchKey.psk_ByValue);
            //oItem = oForm.Items.Item("txtDetOSv");//DetallePago
            //oItem.Visible = true;
            //oEditText = oItem.Specific;
            //oEditText.String = "";
            //oEditText = oForm.Items.Item("txtImpOSv").Specific;//Impuesto
            //oEditText.String = "";
            #endregion
        }
        /// <summary>
        /// Valida si el servicio que se va agregar ya existe
        /// </summary>
        /// <param name="servicioID"></param>
        /// <returns></returns>
        private bool isServicioAgregado(int? servicioID, int? vehiculoID)
        {
            try
            {
                return this.ListaServiciosAgregados.Exists(s => s.Servicio.ServicioID == servicioID && s.Mantenible.MantenibleID == vehiculoID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Agrega un servicio a la orden de servicio
        /// </summary>
        public void NewDetalleOrdenToGrid()
        {
            try
            {
                string err = this.ValidarServicio();
                if (!string.IsNullOrEmpty(err))
                    this.SBO_Application.MessageBox(err, 1, "Ok", "", "");
                else
                {
                    this.FormToDetalleOrden();
                    if (this.ListaServiciosAgregados == null)
                        ListaServiciosAgregados = new List<DetalleOrden>();
                    if (!this.isServicioAgregado(EntityDetalle.oDetalleOrden.Servicio.ServicioID, EntityDetalle.oDetalleOrden.Mantenible.MantenibleID))
                    {
                        this.ListaServiciosAgregados.Add((DetalleOrden)EntityDetalle.oDetalleOrden.Clone());
                        this.FillGridServiciosFromList();
                        this.SetCosto();
                        this.EmptyServicioToForm();
                    }
                    else
                        this.SBO_Application.MessageBox("Ya se ha agregado este servicio", 1, "Ok");
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Carga la informacion de la lista de DetalleOrden al grid de servicios
        /// </summary>
        public void FillGridServiciosFromList()
        {
            try
            {
                #region Comentado
                /*if (this.OSRowIndex != null && this.OSRowIndex > -1)
                {
                    oGrid = oForm.Items.Item("grdorden").Specific;
                    int id = Convert.ToInt32(oGrid.DataTable.Columns.Item("colid").Cells.Item((int)this.OSRowIndex).Value);
                    //if (listobservacion != null && listobservacion.Count > 0 && obsid != 0)
                    if (listordenes != null && listordenes.Count > 0)
                    {
                        bool exist = listordenes.Exists(svc => svc.OrdenServicioID == id);
                        if (exist)
                        {
                            this.Entity.OrdenServicio = this.listordenes.FirstOrDefault(svc => svc.OrdenServicioID == id);
                            this.Entity.oUDT.GetByKey(this.Entity.OrdenServicio.OrdenServicioID.ToString());
                            this.Entity.UDTToOrdenServicio();
                        }
                    }
                }*/
                #endregion Comentado

                DataTable tab = oForm.DataSources.DataTables.Item("dtServSAG");
                oGrid = oForm.Items.Item("gdOS").Specific;
                tab.Rows.Clear();
                int index = 0;
                foreach (DetalleOrden svc in ListaServiciosAgregados)
                {
                    tab.Rows.Add();
                    tab.SetValue("id", index, svc.DetalleOrdenID ?? 0);
                    tab.SetValue("Vehiculo", index, (svc.Mantenible as Vehiculo).Nombre);
                    tab.SetValue("Servicio", index, svc.Servicio.Nombre);
                    tab.SetValue("Costo", index, svc.Costo.ToString());
                    tab.SetValue("IVA", index, 0);//impuesto.ToString());
                    tab.SetValue("ServicioID", index, svc.Servicio.ServicioID.ToString());
                    index++;
                }
                oGrid.DataTable = tab;
                oGrid.Columns.Item("Vehiculo").Width = 150;
                oGrid.Columns.Item("Servicio").Width = 150;
                oGrid.Columns.Item("Costo").Width = 100;
                oGrid.Columns.Item("IVA").Width = 80;
                oGrid.Columns.Item("id").Visible = false;
                oGrid.Columns.Item("ServicioID").Visible = false;
                oGrid.Columns.Item("Vehiculo").Editable = false;
                oGrid.Columns.Item("Servicio").Editable = false;
                oGrid.Columns.Item("Costo").Editable = false;
                //oGrid.Columns.Item("IVA").Editable = false;
                oGrid.Columns.Item("IVA").Visible = false;
                oGrid.SelectionMode = BoMatrixSelect.ms_Single;
                SetCosto();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Boolean FillGridRefaccionesFromList()
        {
            bool bContinuar = false;
            try
            {
                if (Refacciones != null)
                {
                    Matrix oRefacciones = (Matrix)oForm.Items.Item("gdRFa").Specific;
                    Columns oColum = (SAPbouiCOM.Columns)oRefacciones.Columns;
                    oRefacciones.Clear();
                    int iLine = 1;
                    foreach (DetalleRefaccion data in Refacciones)
                    {
                        data.iLineaID = iLine;
                        oForm.DataSources.UserDataSources.Item("oUMAT01").Value = iLine.ToString();
                        oForm.DataSources.UserDataSources.Item("oUMAT02").Value = data.Almacen;
                        oForm.DataSources.UserDataSources.Item("oUMAT03").Value = data.Articulo;
                        oForm.DataSources.UserDataSources.Item("oUMAT04").Value = Convert.ToDecimal(data.Costo).ToString("0.000");//string.Format("$ {0}", data.Costo);
                        oForm.DataSources.UserDataSources.Item("oUMAT05").Value = data.Cantidad.ToString();
                        oForm.DataSources.UserDataSources.Item("oUMAT06").Value = data.Total.ToString();
                        iLine++;
                        oRefacciones.AddRow(1, -1);
                    }
                }
                bContinuar = true;
                this.SetCostoRefacciones();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar llenar la tabla de refacciones" + ex.Message);
            }
            return bContinuar;
        }

        private void SetCosto()
        {
            //oStaticText = oForm.Items.Item("lblCostV").Specific;
            if (this.ListaServiciosAgregados != null && this.ListaServiciosAgregados.Count > 0)
            {
                TotalServicios = 0;
                foreach (DetalleOrden s in this.ListaServiciosAgregados)
                {
                    TotalServicios += s.Costo;
                }
                //Se manejan variables globales para obtener el total
                //tanto de servicios como para refacciones.
                //oStaticText.Caption = total.ToString();
            }
            else
                TotalServicios = 0;
        }

        private void SetCostoRefacciones()
        {
            if (this.Refacciones != null && this.Refacciones.Count > 0)
            {
                TotalRefacciones = 0;
                foreach (DetalleRefaccion s in this.Refacciones)
                {
                    TotalRefacciones += (s.Costo * s.Cantidad);
                }
            }
            else
                TotalRefacciones = 0;
        }

        public void SetCostoTotal()
        {
            try
            {
                decimal? total = this.TotalServicios + TotalRefacciones;
                oStaticText = oForm.Items.Item("lblCostV").Specific;
                oStaticText.Caption = total.ToString();
            }
            catch (Exception ex)
            {
                oStaticText.Caption = "0";
                throw new Exception("Error al mostrar el costo total. " + ex.Message);
            }
        }

        /// <summary>
        /// Actualiza la vista del grid que contiene los servicios agregados
        /// </summary>
        private void UpdateList()
        {
            try
            {
                DataTable tab = oForm.DataSources.DataTables.Item(0);
                if (this.ListaServiciosAgregados != null)
                {
                    if (this.ListaServiciosAgregados.Count > 0)
                    {
                        oGrid = null;
                        oGrid = oForm.Items.Item("gdOS").Specific;
                        if (oGrid.DataTable != null)
                            if (oGrid.DataTable.Rows.Count > 0)
                                oGrid.DataTable.Rows.Clear();
                        //int rows = oGrid.DataTable.Rows.Count;
                        int cont = 0, index = 0;
                        int nServicios = this.ListaServiciosAgregados.Count;
                        foreach (DetalleOrden svc in this.ListaServiciosAgregados)
                        {
                            cont++;
                            //decimal impuesto = (decimal)(svc.impuesto / 100);
                            tab.Rows.Add();
                            tab.SetValue("id", index, svc.DetalleOrdenID ?? 0);
                            tab.SetValue("Vehiculo", index, (svc.Mantenible as Vehiculo).Nombre);
                            tab.SetValue("Servicio", index, svc.Servicio.Descripcion);
                            tab.SetValue("Costo", index, svc.Costo.ToString());
                            tab.SetValue("IVA", index, 0);//impuesto.ToString());
                            tab.SetValue("ServicioID", index, svc.Servicio.ServicioID.ToString());
                            index++;
                        }
                        oGrid.DataTable = tab;
                        oGrid.Columns.Item("Vehiculo").Width = 150;
                        oGrid.Columns.Item("Servicio").Width = 150;
                        oGrid.Columns.Item("Costo").Width = 100;
                        oGrid.Columns.Item("IVA").Width = 80;
                        oGrid.Columns.Item("ServicioID").Visible = false;
                        oGrid.Columns.Item("Vehiculo").Editable = false;
                        oGrid.Columns.Item("Servicio").Editable = false;
                        oGrid.Columns.Item("Costo").Editable = false;
                        oGrid.Columns.Item("IVA").Editable = false;
                        oGrid.SelectionMode = BoMatrixSelect.ms_Single;
                        this.SetCosto();
                    }
                    else
                        oGrid.DataTable.Rows.Clear();
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Estable el número de la fila del grid
        /// </summary>
        /// <param name="rowIndex"></param>
        public void SetRowIndex(int rowIndex, string item)
        {
            try
            {
                if (item == "gdRFa")
                    this.iLineaRefaccion = rowIndex;
                else
                    this.RowIndex = rowIndex;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Elimina un servicio de la lista de servicios agregados a la orden
        /// </summary>
        public void EliminarServicio()
        {
            try
            {
                if (this.RowIndex != null && this.RowIndex != -1)
                {
                    oGrid = oForm.Items.Item("gdOS").Specific;
                    int servicioID = Convert.ToInt32(oGrid.DataTable.Columns.Item("ServicioID").Cells.Item((int)this.RowIndex).Value);
                    if (ListaServiciosAgregados != null && ListaServiciosAgregados.Count > 0)
                    {
                        bool exist = ListaServiciosAgregados.Exists(svc => svc.Servicio.ServicioID == servicioID);
                        if (exist)
                        {
                            int index = this.ListaServiciosAgregados.FindIndex(svc => svc.Servicio.ServicioID == servicioID);
                            this.ListaServiciosAgregados.RemoveAt(index);
                            this.UpdateList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Valida si el objeto en la interface existe en la lista de Servicios ya agregados
        /// </summary>
        public bool ExistDetalleOrden()
        {
            try
            {
                oItem = oForm.Items.Item("lblsvcid");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                return !string.IsNullOrEmpty(oStaticText.Caption);
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Obtiene el objeto DetalleOrden seleccionado en el Grid
        /// </summary>
        public void GetDetalleOrdenFromGrid()
        {
            try
            {
                if (this.RowIndex != null && this.RowIndex > -1)
                {
                    oGrid = oForm.Items.Item("gdOS").Specific;
                    int licid = Convert.ToInt32(oGrid.DataTable.Columns.Item("id").Cells.Item((int)this.RowIndex).Value);
                    //if (listobservacion != null && listobservacion.Count > 0 && obsid != 0)
                    if (ListaServiciosAgregados != null && ListaServiciosAgregados.Count > 0)
                    {
                        bool exist = ListaServiciosAgregados.Exists(svc => svc.DetalleOrdenID == licid);
                        if (exist)
                        {
                            this.EntityDetalle.oDetalleOrden = this.ListaServiciosAgregados.FirstOrDefault(svc => svc.DetalleOrdenID == licid);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Valida los datos que debe contener un servicio antes de agregarlo a la lista de la orden de servicio
        /// </summary>
        /// <returns></returns>
        private string ValidarServicio()
        {
            try
            {
                string err = string.Empty;
                //Validando Vehiculo
                oCombo = oForm.Items.Item("comboVehOS").Specific;
                if (string.IsNullOrEmpty(oCombo.Value) || oCombo.Value.ToString() == "0")
                    err += ", Vehículo";
                //Validando Servicio
                oCombo = oForm.Items.Item("comoServOS").Specific;
                if (string.IsNullOrEmpty(oCombo.Value) || oCombo.Value.ToString() == "0")
                    err += ", Servicio";
                //Validando Costo
                //oEditText = oForm.Items.Item("txtCostoOS").Specific;
                //if (string.IsNullOrEmpty(oEditText.String.Trim()))
                //    err += ", Costo";
                //else
                //{
                //    if(Convert.ToDouble(oEditText.String.Trim()) <= 0)
                //        err += ", Costo";
                //}
                //Validando Metodo de pago
                //oCombo = oForm.Items.Item("comboMPOSv").Specific;
                //if (string.IsNullOrEmpty(oCombo.Value) || oCombo.Value.ToString() == "0")
                //err += ", Método de pago";
                if (!string.IsNullOrEmpty(err))
                    if (err.StartsWith(","))
                        err = "Los siguientes campos son obligatorios:\n" + err.Substring(1);
                return err;
            }
            catch (Exception ex)
            {
                return String.Empty;
            }
        }
        #endregion

        #region Orden Servicio
        /// <summary>
        /// Obtine los datos de la vista hacia a la entidad
        /// </summary>
        public void FormToEntity()
        {
            try
            {
                oStaticText = oForm.Items.Item("lblid").Specific;
                if (!string.IsNullOrEmpty(oStaticText.Caption.Trim()))
                    Entity.OrdenServicio.OrdenServicioID = Convert.ToInt32(oStaticText.Caption);
                else
                    Entity.OrdenServicio.OrdenServicioID = null;

                int proveedorID;//Proveedor
                oCombo = oForm.Items.Item("comboProOS").Specific;
                if (int.TryParse(this.oCombo.Value, out proveedorID))
                {
                    Entity.OrdenServicio.ProveedorServicio.ProveedorID = proveedorID;
                }
                else
                {
                    Entity.OrdenServicio.ProveedorServicio = new Proveedor();
                }
                int sucursalID;//Sucursal
                oCombo = oForm.Items.Item("cmbsuc").Specific;
                if (int.TryParse(this.oCombo.Value, out sucursalID))
                {
                    Entity.OrdenServicio.SucursalOrden.SucursalID = sucursalID;
                }
                else
                {
                    Entity.OrdenServicio.SucursalOrden = new Sucursal();
                }
                DateTime utcTime;//Fecha de servicio
                oEditText = oForm.Items.Item("txtFechaOS").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                {
                    if (this.dateHelper == null)
                        this.GetUserConfigs();
                    utcTime = this.dateHelper.ToServerTime(Convert.ToDateTime(oEditText.String.Trim()));
                    Entity.OrdenServicio.Fecha = utcTime;
                }

                //DateTime EnPro;//Fecha de entrada programada
                oEditText = oForm.Items.Item("txtEnPro").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                {
                    //if (this.dateHelper == null)
                    //    this.GetUserConfigs();
                    //EnPro = this.dateHelper.ToServerTime(Convert.ToDateTime(oEditText.String.Trim()));
                    //Entity.OrdenServicio.FechaRecepcion = EnPro;

                    Entity.OrdenServicio.FechaRecepcion = Convert.ToDateTime(oEditText.String);
                }

                //DateTime SalPro;//Fecha de salida programada
                oEditText = oForm.Items.Item("txtSalPro").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                {
                    //if (this.dateHelper == null)
                    //    this.GetUserConfigs();
                    //SalPro = this.dateHelper.ToServerTime(Convert.ToDateTime(oEditText.String.Trim()));
                    //Entity.OrdenServicio.FechaLiberacion = SalPro;

                    Entity.OrdenServicio.FechaLiberacion = Convert.ToDateTime(oEditText.String);
                }

                //DateTime EnReal;//Fecha de entrada real
                oEditText = oForm.Items.Item("txtEnReal").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                {
                    //if (this.dateHelper == null)
                    //    this.GetUserConfigs();
                    //EnReal = this.dateHelper.ToServerTime(Convert.ToDateTime(oEditText.String.Trim()));
                    //Entity.OrdenServicio.FechaRecepcionReal = EnReal;

                    Entity.OrdenServicio.FechaRecepcionReal = Convert.ToDateTime(oEditText.String);
                }

                //DateTime SalReal;//Fecha de salida real
                oEditText = oForm.Items.Item("txtSalReal").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                {
                    //if (this.dateHelper == null)
                    //    this.GetUserConfigs();
                    //SalReal = this.dateHelper.ToServerTime(Convert.ToDateTime(oEditText.String.Trim()));
                    //Entity.OrdenServicio.FechaLiberacionReal = SalReal;

                    Entity.OrdenServicio.FechaLiberacionReal = Convert.ToDateTime(oEditText.String);
                }

                //Folio
                oEditText = oForm.Items.Item("txtFolioOS").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    Entity.OrdenServicio.Folio = oEditText.String.Trim();
                //Descripcion
                oEditText = oForm.Items.Item("txtDescOs").Specific;
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    Entity.OrdenServicio.Descripcion = oEditText.String.Trim();
                //Metodo de pago
                oCombo = oForm.Items.Item("comboMPOSv").Specific;
                int pagoID;
                if (int.TryParse(oCombo.Value, out pagoID))
                    Entity.OrdenServicio.TipoDePago.TipoPagoID = pagoID;
                //Detalle de pago
                oEditText = oForm.Items.Item("txtDetOSv").Specific;
                if (!string.IsNullOrEmpty(oEditText.String))
                    Entity.OrdenServicio.TipoDePago.DetallePago = oEditText.String.Trim();
                //Costo
                oStaticText = oForm.Items.Item("lblCostV").Specific;
                if (!string.IsNullOrEmpty(oStaticText.Caption.Trim()))
                    Entity.OrdenServicio.Costo = Convert.ToDecimal(oStaticText.Caption.Trim());


                //Impuesto
                oEditText = oForm.Items.Item("txtImpOSv").Specific;
                if (!string.IsNullOrEmpty(oEditText.String))
                    Entity.OrdenServicio.Impuesto = Convert.ToDecimal(oEditText.String.Trim());
                //Responsable dela orden de servicio
                oCombo = oForm.Items.Item("cmbResp").Specific;
                int respID;
                if (int.TryParse(oCombo.Value, out respID))
                    Entity.OrdenServicio.ResponsableOrdenServicio.EmpleadoID = respID;
                //Aprobador de la orden de servico
                oCombo = oForm.Items.Item("cmbAprob").Specific;
                int apID;
                if (int.TryParse(oCombo.Value, out apID))
                    Entity.OrdenServicio.AprobadorOrdenServicio.EmpleadoID = apID;
                Entity.OrdenServicio.FechaCaptura = DateTime.UtcNow;

                oCombo = oForm.Items.Item("cmbestatus").Specific;
                Entity.OrdenServicio.EstatusOrden.EstatusOrdenID = Convert.ToInt32(oCombo.Selected.Value);

                //OrdenServicio.alertas.empresa = this.Config.UserFull.Dependencia;
                //OrdenServicio.SucursalOrden = this.Config.UserFull.EmpleadoSucursal;
                Entity.OrdenServicio.EmpresaOrden = this.Config.UserFull.Dependencia;
                Entity.OrdenServicio.EncargadoOrdenServicio = this.Config.UserFull;
                Entity.OrdenServicio.DetalleOrden = this.ListaServiciosAgregados;
                Entity.OrdenServicio.FolioOrden = (FolioOrden)EFolio.folio.Clone();
                Entity.OrdenServicio.alertas = new AlertaMantenimiento();
                Entity.OrdenServicio.alertas.servicio = new Servicio();
                Entity.OrdenServicio.alertas.servicio.ServicioID = 0;
                //this.Entity.OrdenServicio = OrdenServicio;

                EntityRefaccion = new DetalleRefaccionesSAP(this.oCompany);
                EntityRefaccion.oDetalleRefaccion = new DetalleRefaccion();
                EntityRefaccion.oDetalleRefaccion.OrdenServicio = Entity.OrdenServicio;
                //Se validó que sea diferente de nulo para no devolver 0 en lugar de null y evitar que actualice una orden
                //en lugar de insertarla.
                if (!String.IsNullOrEmpty(this.FolioOrdenSercio))
                    EntityRefaccion.oDetalleRefaccion.OrdenServicio.OrdenServicioID = Convert.ToInt32(this.FolioOrdenSercio);
                EntityRefaccion.oDetalleRefaccion.Mantenible = new Vehiculo();
                EntityRefaccion.oDetalleRefaccion.Servicio = new Servicio();
                EntityRefaccion.oDetalleRefaccion.Servicio.ServicioID = Convert.ToInt32(oCombo.Value);
                EntityRefaccion.oDetalleRefaccion.TipoMantenibleID = 1;


                oEditText = oForm.Items.Item("txtHoroOs").Specific;
                Int32 horometro = (string.IsNullOrEmpty(oEditText.String)) ? 0 : Int32.Parse(oEditText.String);
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    Entity.OrdenServicio.Horometro = horometro;

                oEditText = oForm.Items.Item("txtOdomOs").Specific;
                Int32 odometro = (string.IsNullOrEmpty(oEditText.String)) ? 0 : Int32.Parse(oEditText.String);
                if (!string.IsNullOrEmpty(oEditText.String.Trim()))
                    Entity.OrdenServicio.Odometro = odometro;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Obtine los datos de la entidad hacia la vista
        /// </summary>
        public void EntityToForm()
        {
            try
            {


                oStaticText = oForm.Items.Item("lblid").Specific;
                if (Entity.OrdenServicio.OrdenServicioID != null && Entity.OrdenServicio.OrdenServicioID > 0)
                    oStaticText.Caption = Entity.OrdenServicio.OrdenServicioID.ToString();
                else
                {
                    oStaticText.Caption = String.Empty;
                }

                oCombo = oForm.Items.Item("comboProOS").Specific;
                if (Entity.OrdenServicio.ProveedorServicio != null && Entity.OrdenServicio.ProveedorServicio.ProveedorID != null)
                {
                    oCombo.Select(Entity.OrdenServicio.ProveedorServicio.ProveedorID.ToString());
                }

                oCombo = oForm.Items.Item("cmbsuc").Specific;
                if (Entity.OrdenServicio.SucursalOrden != null && Entity.OrdenServicio.SucursalOrden.SucursalID != null)
                {
                    oCombo.Select(Entity.OrdenServicio.SucursalOrden.SucursalID.ToString());
                }

                DateTime utcTime;//Fecha de servicio
                oEditText = oForm.Items.Item("txtFechaOS").Specific;
                if (Entity.OrdenServicio.Fecha != null)
                {
                    if (this.dateHelper == null)
                        this.GetUserConfigs();
                    utcTime = this.dateHelper.ToUserTime((DateTime)Entity.OrdenServicio.Fecha);
                    oEditText.String = utcTime.ToShortDateString();
                }

                //DateTime EnPro;//Fecha de entrada programada
                oEditText = oForm.Items.Item("txtEnPro").Specific;
                if (Entity.OrdenServicio.FechaRecepcion != null)
                {
                    //if (this.dateHelper == null)
                    //    this.GetUserConfigs();
                    //EnPro = this.dateHelper.ToUserTime((DateTime)Entity.OrdenServicio.FechaRecepcion);
                    //oEditText.String = EnPro.ToShortDateString();

                    oEditText.String = Entity.OrdenServicio.FechaRecepcion.Value.ToString("yyyy/MM/dd");
                }

                //DateTime SalPro;//Fecha de salida programada
                oEditText = oForm.Items.Item("txtSalPro").Specific;
                if (Entity.OrdenServicio.FechaLiberacion != null)
                {
                    //if (this.dateHelper == null)
                    //    this.GetUserConfigs();
                    //SalPro = this.dateHelper.ToUserTime((DateTime)Entity.OrdenServicio.FechaLiberacion);
                    //oEditText.String = SalPro.ToShortDateString();

                    oEditText.String = Entity.OrdenServicio.FechaLiberacion.Value.ToString("yyyy/MM/dd");
                }

                //DateTime EnReal;//Fecha de entrada real
                oEditText = oForm.Items.Item("txtEnReal").Specific;
                if (Entity.OrdenServicio.FechaRecepcionReal != null)
                {
                    //if (this.dateHelper == null)
                    //    this.GetUserConfigs();
                    //EnReal = this.dateHelper.ToUserTime((DateTime)Entity.OrdenServicio.FechaRecepcionReal);
                    //oEditText.String = EnReal.ToShortDateString();

                    oEditText.String = Entity.OrdenServicio.FechaRecepcionReal.Value.ToString("yyyy/MM/dd");
                }

                //DateTime SalReal;//Fecha de salida real
                oEditText = oForm.Items.Item("txtSalReal").Specific;
                if (Entity.OrdenServicio.FechaLiberacionReal != null)
                {
                    //if (this.dateHelper == null)
                    //    this.GetUserConfigs();
                    //SalReal = this.dateHelper.ToUserTime((DateTime)Entity.OrdenServicio.FechaLiberacionReal);
                    //oEditText.String = SalReal.ToShortDateString();

                    oEditText.String = Entity.OrdenServicio.FechaLiberacionReal.Value.ToString("yyyy/MM/dd");
                }

                //Folio
                oEditText = oForm.Items.Item("txtFolioOS").Specific;
                if (!string.IsNullOrEmpty(Entity.OrdenServicio.Folio))
                    oEditText.String = Entity.OrdenServicio.Folio;
                //Descripcion
                oEditText = oForm.Items.Item("txtDescOs").Specific;
                if (!string.IsNullOrEmpty(Entity.OrdenServicio.Descripcion))
                    oEditText.String = Entity.OrdenServicio.Descripcion;
                //Metodo de pago
                oCombo = oForm.Items.Item("comboMPOSv").Specific;
                if (Entity.OrdenServicio.TipoDePago != null && Entity.OrdenServicio.TipoDePago.TipoPagoID != null)
                    oCombo.Select(Entity.OrdenServicio.TipoDePago.TipoPagoID.ToString());
                //Detalle de pago
                oEditText = oForm.Items.Item("txtDetOSv").Specific;
                if (Entity.OrdenServicio.TipoDePago != null && !string.IsNullOrEmpty(Entity.OrdenServicio.TipoDePago.DetallePago))
                    oEditText.String = Entity.OrdenServicio.TipoDePago.DetallePago;
                //Costo
                oStaticText = oForm.Items.Item("lblCostV").Specific;
                if (Entity.OrdenServicio.Costo != null)
                    oStaticText.Caption = Entity.OrdenServicio.Costo.ToString();

                //Impuesto
                oEditText = oForm.Items.Item("txtImpOSv").Specific;
                if (Entity.OrdenServicio.Impuesto != null)
                {
                    //bool boolTextBoxImpuesto = oForm.Items.Item("txtImpOSv").Enabled;

                    oEditText.String = Entity.OrdenServicio.Impuesto.ToString();
                    //oForm.Items.Item("txtImpOSv").Enabled = boolTextBoxImpuesto;

                }
                //Responsable dela orden de servicio
                oCombo = oForm.Items.Item("cmbResp").Specific;
                if (Entity.OrdenServicio.ResponsableOrdenServicio != null && Entity.OrdenServicio.ResponsableOrdenServicio.EmpleadoID != null)
                    oCombo.Select(Entity.OrdenServicio.ResponsableOrdenServicio.EmpleadoID.ToString());
                //Aprobador de la orden de servico
                oCombo = oForm.Items.Item("cmbAprob").Specific;
                if (Entity.OrdenServicio.AprobadorOrdenServicio != null && Entity.OrdenServicio.AprobadorOrdenServicio.EmpleadoID != null)
                    oCombo.Select(Entity.OrdenServicio.AprobadorOrdenServicio.EmpleadoID.ToString());

                //oCombo = oForm.Items.Item("cmbestatus").Specific;
                //OrdenServicio.EstatusOrden.EstatusOrdenID = Convert.ToInt32(oCombo.Selected.Value);

                //OrdenServicio.alertas.empresa = this.Config.UserFull.Dependencia;
                //OrdenServicio.SucursalOrden = this.Config.UserFull.EmpleadoSucursal;
                //OrdenServicio.DetalleOrden = this.GetListDetalleOrdenFromListServicios();
                EFolio.folio = Entity.OrdenServicio.FolioOrden;
                oStaticText = oForm.Items.Item("txtfolio").Specific;
                oStaticText.Caption = string.Format("{0}/{1}", Entity.OrdenServicio.FolioOrden.Folio,
                   Entity.OrdenServicio.FolioOrden.AnioFolio);

                if (this.Entity.OrdenServicio.DetalleOrden.Any())
                {
                    this.ListaServiciosAgregados = this.Entity.OrdenServicio.DetalleOrden;
                    //this.FillGridServiciosFromList();
                }

                oEditText = oForm.Items.Item("txtHoroOs").Specific;
                if (Entity.OrdenServicio.Horometro != null)
                {
                    //bool boolTextBoxImpuesto = oForm.Items.Item("txtImpOSv").Enabled;

                    oEditText.String = Entity.OrdenServicio.Horometro.ToString();
                    //oForm.Items.Item("txtImpOSv").Enabled = boolTextBoxImpuesto;

                }

                oEditText = oForm.Items.Item("txtOdomOs").Specific;
                if (Entity.OrdenServicio.Odometro != null)
                {
                    //bool boolTextBoxImpuesto = oForm.Items.Item("txtImpOSv").Enabled;

                    oEditText.String = Entity.OrdenServicio.Odometro.ToString();
                    //oForm.Items.Item("txtImpOSv").Enabled = boolTextBoxImpuesto;

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Int32 GetEstatus()
        {
            Int32 Estatus;
            Int32? EstatusActual = 0;
            Int32? EstatusGuardado = null;

            oCombo = oForm.Items.Item("cmbestatus").Specific;
            EstatusGuardado = Entity.OrdenServicio.EstatusOrden.EstatusOrdenID;
            EstatusActual = Convert.ToInt32(oCombo.Selected.Value);

            if ((Int32)EstatusActual == (Int32)EstatusGuardado)
                Estatus = Convert.ToInt32(EstatusActual);
            else
                Estatus = 0;

            return Estatus;
        }

        public void AsignarFocoEntity()
        {
            oForm.Items.Item("tabDataOrd").Click();
        }
        /// <summary>
        /// Vacia todos los campos del formulario
        /// </summary>
        public void EmptyEntityToForm()
        {
            try
            {
                #region Tab Datos de la Orden
                oStaticText = oForm.Items.Item("lblid").Specific;
                oStaticText.Caption = String.Empty;

                oEditText = oForm.Items.Item("txtFechaOS").Specific;//Fecha
                oEditText.String = "";
                oEditText = oForm.Items.Item("txtFolioOS").Specific;//Folio
                oEditText.String = "";
                oEditText = oForm.Items.Item("txtDescOs").Specific;//Descripcion
                oEditText.String = "";

                oCombo = oForm.Items.Item("comboMPOSv").Specific;//Combo Metodo de pago
                oCombo.Select("0", BoSearchKey.psk_ByValue);
                oItem = oForm.Items.Item("txtDetOSv");//DetallePago
                //oItem.Visible = true;
                oEditText = oItem.Specific;
                oEditText.String = "";
                oEditText = oForm.Items.Item("txtImpOSv").Specific;//Impuesto
                oEditText.String = "";

                oStaticText = oForm.Items.Item("lblCostV").Specific;
                oStaticText.Caption = "0.00";

                oCombo = oForm.Items.Item("cmbResp").Specific;//Combo Responsable
                oCombo.Select("0", BoSearchKey.psk_ByValue);
                oCombo = oForm.Items.Item("cmbsuc").Specific;//Combo Sucursal
                oCombo.Select("0", BoSearchKey.psk_ByValue);
                oCombo = oForm.Items.Item("cmbAprob").Specific;//Combo Aprobador
                oCombo.Select("0", BoSearchKey.psk_ByValue);
                oCombo = oForm.Items.Item("comboProOS").Specific;//Combo Proveedor
                oCombo.Select("0", BoSearchKey.psk_ByValue);
                //oFolder = oForm.Items.Item("tabDataOrd").Specific;
                SetCurrentFolioOrden();
                //oFolder.Select();
                #endregion
                #region Datos de los servicios Agregados
                this.EmptyServicioToForm();
                //oCombo = oForm.Items.Item("comboVehOS").Specific;//Combo Vehiculo
                //oCombo.Select("", BoSearchKey.psk_ByDescription);
                //oCombo = oForm.Items.Item("comoServOS").Specific;//Combo Servicio
                //oCombo.Select("", BoSearchKey.psk_ByDescription);
                //oEditText = oForm.Items.Item("txtCostoOS").Specific;//Costo
                //oEditText.String = "";
                #endregion
                this.PrepareObjects();
                if (this.ListaServiciosAgregados != null && this.ListaServiciosAgregados.Count > 0)
                    this.ListaServiciosAgregados.Clear();
                oGrid.DataTable.Rows.Clear();
                this.Refacciones = new List<DetalleRefaccion>();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Obtiene la orden por medio de su id
        /// </summary>
        public void GetEntityByID()
        {
            try
            {
                var rs = this.Entity.Consultar(this.Entity.OrdenServicio);
                if (Entity.HashOrdenServicio(rs))
                {
                    this.Entity.oUDT.GetByKey(rs.Fields.Item("Code").Value.ToString());
                    this.Entity.UDTToOrdenServicio();
                    var rsd = this.EntityDetalle.ConsultarByOrdenID(this.Entity.OrdenServicio.OrdenServicioID);
                    if (EntityDetalle.HashoDetalleOrden(rsd))
                    {
                        Entity.OrdenServicio.DetalleOrden = EntityDetalle.RecordSetToListDetalleOrden(rs);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string ValidaSolicitudOrden()
        {
            string err = string.Empty;
            //Validando proveedor
            //oCombo = oForm.Items.Item("comboProOS").Specific;
            //if (string.IsNullOrEmpty(oCombo.Value) || oCombo.Value.ToString() == "0")
            //    err += ", Proveedor";
            //Validando la fecha de la orden
            oEditText = oForm.Items.Item("txtFechaOS").Specific;
            if (string.IsNullOrEmpty(oEditText.String.Trim()))
                err += ", Fecha";

            //Se valida que exista un servicio en la pestaña de "Agregar servicios". R. Santos.
            oGrid = oForm.Items.Item("gdOS").Specific;
            if (oGrid.DataTable != null)
            {
                if (oGrid.DataTable.Rows.Count < 1)
                {
                    err += ", Servicio";
                }
            }
            else
            {
                err += ", Servicio";
            }

            //oEditText = oForm.Items.Item("txtCostoOS").Specific;
            //if (String.IsNullOrEmpty(oEditText.String.Trim()))
            //err += ", Costo (Sin Impuesto)";
            if (!string.IsNullOrEmpty(err))
            {
                if (err.StartsWith(","))
                    err = "Los siguentes campos son requeridos:\n" + err.Substring(1);
            }
            return err;
        }

        public void bOCultarProveedor(bool bHide)
        {
            oItem = oForm.Items.Item("comboProOS");
            oItem.Enabled = bHide;

            oItem = oForm.Items.Item("tabDataRef");
            oItem.Enabled = bHide;
        }

        public void MostrarFacturación(bool show)
        {
            oItem = oForm.Items.Item("oInvoice");
            //oItem.Enabled = show;
            oItem.Visible = show;
        }

        #endregion

        /// <summary>
        /// Obtiene el detalle de la orden a partir de la lista de servicios agregados
        /// </summary>
        /// <returns></returns>
        //public List<DetalleOrden> GetListDetalleOrdenFromListServicios()
        //{
        //    try
        //    {
        //        List<DetalleOrden> detalle = new List<DetalleOrden>();
        //        if(this.ListaServiciosAgregados != null && this.ListaServiciosAgregados.Count > 0)
        //        {
        //            foreach(DetalleOrden sv in this.ListaServiciosAgregados)
        //            {
        //                DetalleOrden dtor = new DetalleOrden();
        //                dtor.Servicio = new Servicio();
        //                if (sv.tipoMantenibleID == 1)
        //                    dtor.Mantenible = new Vehiculo();
        //                else if (sv.tipoMantenibleID == 2)
        //                    dtor.Mantenible = new Caja();
        //                dtor.Mantenible.MantenibleID = sv.vehiculoid;
        //                dtor.Servicio.ServicioID = sv.servicioid;
        //                dtor.TipoMantenibleID = sv.tipoMantenibleID;
        //                dtor.Costo = Convert.ToDecimal(sv.costo);
        //                detalle.Add(dtor);
        //            }
        //        }
        //        return detalle;
        //    }
        //    catch(Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}

        /// <summary>
        /// Valida los datos de la orden de servicio
        /// </summary>
        /// <returns></returns>
        public string ValidarDatos()
        {
            try
            {
                string err = string.Empty;
                //Validando proveedor
                oCombo = oForm.Items.Item("comboProOS").Specific;
                if (string.IsNullOrEmpty(oCombo.Value) || oCombo.Value.ToString() == "0")
                    err += ", Proveedor";
                //Validando la fecha de la orden
                oEditText = oForm.Items.Item("txtFechaOS").Specific;
                if (string.IsNullOrEmpty(oEditText.String.Trim()))
                    err += ", Fecha";
                //Validando el folio
                oEditText = oForm.Items.Item("txtFolioOS").Specific;
                if (string.IsNullOrEmpty(oEditText.String.Trim()))
                    err += ", Folio";
                //Validando el metodo de pago
                oCombo = oForm.Items.Item("comboMPOSv").Specific;
                if (string.IsNullOrEmpty(oCombo.Value) || oCombo.Value == "0")
                    err += ", Método de pago";
                //Validando el impuesto
                oEditText = oForm.Items.Item("txtImpOSv").Specific;
                if (string.IsNullOrEmpty(oEditText.String.Trim()))
                    err += ", Impuesto";
                //Validando el responsable de la orden de servicio
                oCombo = oForm.Items.Item("cmbResp").Specific;
                if (string.IsNullOrEmpty(oCombo.Value) || oCombo.Value == "0")
                    err += ", Responsable de la orden de servicio";
                //Validando el aprobador de la orden de servicio
                oCombo = oForm.Items.Item("cmbAprob").Specific;
                if (string.IsNullOrEmpty(oCombo.Value) || oCombo.Value == "0")
                    err += ", Aprobador de la orden de servicio";
                //Validando la lista de servicios agregados
                if (this.ListaServiciosAgregados == null)
                    err += ", Agrege al menos un servicio";
                if (!string.IsNullOrEmpty(err))
                {
                    if (err.StartsWith(","))
                        err = "Los siguentes campos son requeridos:\n" + err.Substring(1);
                }
                return err;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region Auxiliares
        /// <summary>
        /// Crea las instancias de los objetos que intervienen en el modulo, preparandolos para realizar una nueva accion
        /// </summary>
        private void PrepareObjects()
        {
            #region Alertas de mantenimiento
            this.AlertaMantenimiento = new Kanan.Mantenimiento.BO2.AlertaMantenimiento();
            this.AlertaMantenimiento.empresa = new Kanan.Operaciones.BO2.Empresa();
            this.AlertaMantenimiento.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
            this.AlertaMantenimiento.servicio = new Kanan.Mantenimiento.BO2.Servicio();
            this.AlertaMantenimiento.servicio.TipoServicio = new Kanan.Mantenimiento.BO2.TipoServicio();
            this.AlertaMantenimiento.servicio.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
            #endregion

            #region Orden de servicio
            //Entity.OrdenServicio = new Kanan.Mantenimiento.BO2.OrdenServicio();
            //OrdenServicio.alertas = new AlertaMantenimiento();
            //OrdenServicio.alertas.vehiculo = new Vehiculo();
            //OrdenServicio.alertas.servicio = new Servicio();
            //OrdenServicio.TipoDePago = new TipoDePago();
            //OrdenServicio.ProveedorServicio = new Kanan.Costos.BO2.Proveedor();
            //OrdenServicio.alertas.empresa = new Empresa();
            //OrdenServicio.SucursalOrden = new Sucursal();
            //OrdenServicio.EncargadoOrdenServicio = new Empleado();
            #endregion
        }
        /// <summary>
        /// Obtiene las configuraciones de usuario
        /// </summary>
        private void GetUserConfigs()
        {
            string configs = this.Config.UserFull.ConfigurationToString();
            var array = configs.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (array.Length > 0)
            {
                //TimeZone=America/Mexico_City
                var timeZone = array[0].Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                int index = timeZone.Length - 1;
                string timeZoneValue = (string)timeZone.GetValue(index);
                //Lang=es-MX
                var lang = array[1].Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                //es-MX
                string language = lang[1].ToString();
                //es
                var shortLang = language.ToString().Split('-')[0];

                Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(language);
                Thread.CurrentThread.CurrentCulture = new CultureInfo(language);

                /// init noda
                this.dateHelper = new NodaTimeHelper.Services.DateTimeHelper(timeZoneValue);
                string[] formatsTime = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortTimePattern.Split(':');
                string[] formatEnd = formatsTime[1].Split(' ');
                string formatCurrent = string.Format("{0} {1}:{2}", Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern, formatsTime[0], formatEnd[0]);
                this.dateHelper.SetShortDate(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern);
                this.dateHelper.SetPattern(formatCurrent);
            }

        }
        /// <summary>
        /// obtiene la informacion de vehiculo y servicio por cada DetalleOrden
        /// </summary>
        public void GetDetalleOrdenInfo()
        {
            foreach (var dto in ListaServiciosAgregados)
            {
                #region Vehicullo
                string query = "select * from [@VSKF_VEHICULO] ";
                query += string.Format(" where U_VehiculoID = {0} ", dto.Mantenible.MantenibleID);
                SAPbobsCOM.Recordset rs = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                if (rs.RecordCount > 0)
                {
                    rs.MoveFirst();
                    string value = Convert.ToString(rs.Fields.Item("U_Nombre").Value);
                    (dto.Mantenible as Vehiculo).Nombre = value;
                }
                #endregion

                #region Servicio
                string query2 = "select * from [@VSKF_SERVICIOS] ";
                query2 += string.Format(" where U_ServicioID = {0} ", dto.Servicio.ServicioID);
                SAPbobsCOM.Recordset rs2 = this.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                rs2.DoQuery(query2);
                if (rs2.RecordCount > 0)
                {
                    rs2.MoveFirst();
                    string value = Convert.ToString(rs2.Fields.Item("U_Nombre").Value);
                    dto.Servicio.Nombre = value;
                }
                #endregion
            }
        }

        public void ActualizarFolio()
        {
            //this.EFolio.folio.Folio ++;
            if (EFolio.folio.FolioOrdenID != null)
            {
                this.EFolio.folio.Folio++;
                this.EFolio.FolioToUDT();
                if (EsSQL)
                {
                    this.oCompany.StartTransaction();
                    this.EFolio.Actualizar("[@VSKF_FOLIOORDEN]");
                    if (this.oCompany.InTransaction)
                        this.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                }
                else
                {
                    this.oCompany.StartTransaction();
                    this.EFolio.Actualizar(@"""@VSKF_FOLIOORDEN""");
                    if (this.oCompany.InTransaction)
                        this.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                }
            }
            else
            {
                oStaticText = oForm.Items.Item("txtfolio").Specific;
                this.EFolio.Sincronizado = 1;
                this.EFolio.UUID = Guid.NewGuid();
                this.EFolio.FolioToUDT();

                if (EsSQL)
                {
                    this.oCompany.StartTransaction();
                    this.EFolio.Insert("[@VSKF_FOLIOORDEN]");
                    if (this.oCompany.InTransaction)
                        this.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                }
                else
                {
                    this.oCompany.StartTransaction();
                    this.EFolio.Insert(@"""@VSKF_FOLIOORDEN""");
                    if (this.oCompany.InTransaction)
                        this.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                }
            }
        }
        #endregion
    }
}
