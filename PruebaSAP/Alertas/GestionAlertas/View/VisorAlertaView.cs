﻿using KananWS.Interface;
using SAPbouiCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.GestionAlertas.View
{
    public class VisorAlertaView
    {
         #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.Matrix oMatrix;
        private SAPbouiCOM.EditText oEditText;
        public string FormUniqueID { get; private set; }
        private SAPbouiCOM.DBDataSource oDBDataSource;
        public int? AlertaID { get; set; }
        private XMLPerformance XmlApplication;
        private KananSAP.Alertas.Data.AlertaSAP alertaData;
        private KananSAP.Mantenimiento.Data.ServicioSAP servicioData;
        private KananSAP.Vehiculos.Data.VehiculoSAP vehiculoData;
        public Configurations Configs { get; set; }
        #endregion

        #region Constructor
        public VisorAlertaView(SAPbouiCOM.Application Application, SAPbobsCOM.Company company) 
        {
            try
            {
                this.SBO_Application = Application;
                this.XmlApplication = new XMLPerformance(this.SBO_Application);
                this.oItem = null;
                this.oMatrix = null;
                oDBDataSource = null;
                this.alertaData = new Alertas.Data.AlertaSAP(company);
                this.servicioData = new Mantenimiento.Data.ServicioSAP(company);
                this.vehiculoData = new Vehiculos.Data.VehiculoSAP(company);
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region FORM

        public void LoadForm()
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("FrmAlerta");
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Invalid Form"))
                {
                    string sPath = System.Windows.Forms.Application.StartupPath;                   

                    this.XmlApplication.LoadFromXML(sPath + "\\XML",
                            "VisorAlertas.xml");
                    this.oForm = SBO_Application.Forms.Item("FrmAlerta");
                    
                    //Estabkece la ventana centrada
                    this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                    this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                }
                else
                {
                    SBO_Application.StatusBar.SetText(ex.Message, BoMessageTime.bmt_Short);
                }
            }
        }

        public void ShowForm()
        {
            try
            {
                this.oForm.Visible = true;
                this.oForm.Select();
            }
            catch (Exception ex)
            {
                this.LoadForm();
                this.oForm.Left = (SBO_Application.Desktop.Width - this.oForm.Width) / 2;
                this.oForm.Top = (SBO_Application.Desktop.Height - this.oForm.Height) / 2;
                this.oForm.Visible = true;
                this.oForm.Select();

            }
        }

        #endregion

        #region DataSource
        public void SetDataSource()
        {
            try
            {
                string query = this.alertaData.ConsultarTop10Alertas(this.Configs.UserFull.Dependencia.EmpresaID, null, true);
                if (this.oForm.DataSources.DataTables.Count > 0)
                {
                    oMatrix.LoadFromDataSource();
                }
                else
                {
                    this.oForm.DataSources.DataTables.Add("tabAlertas");
                    this.oForm.DataSources.DataTables.Item("tabAlertas").Clear();
                    this.oForm.DataSources.DataTables.Item("tabAlertas").ExecuteQuery(query);
                    oMatrix = this.oForm.Items.Item("mtxAlertas").Specific;
                    //oMatrix.Columns.Add("clTipoAl", BoFormItemTypes.it_EDIT);
                    //oMatrix.Columns.Item("clTipoAl").DataBind.Bind("tabAlertas", "U_TipoAlerta");
                    oMatrix.Columns.Item("#").DataBind.Bind("tabAlertas", "U_AlertaID");
                    oMatrix.Columns.Item("clDescrip").DataBind.Bind("tabAlertas", "U_Descripcion_Es");
                    oMatrix.LoadFromDataSource();
                }
            }
            catch (Exception ex) { 
                throw new Exception(ex.Message); }
        }
        #endregion

        public bool HasRowValue(int indexRow)
        {
            try
            {
                this.oForm = SBO_Application.Forms.Item("FrmAlerta");
                oMatrix = oForm.Items.Item("mtxAlertas").Specific;

                int rows = oMatrix.RowCount;
                if (indexRow <= 0 || indexRow > rows) return false;
                oEditText = (SAPbouiCOM.EditText)oMatrix.GetCellSpecific(0, indexRow);
                if(!string.IsNullOrEmpty(oEditText.String.Trim()))
                {
                    this.AlertaID = Convert.ToInt32(oEditText.String.Trim());
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public int DevuelveAlertaID()
        {
            return Convert.ToInt32(this.AlertaID);
        }
        public int GetVehiculoID()
        {
            try
            {
                int vehiculoID = 0;
                vehiculoID = this.alertaData.GetVehiculoID(Convert.ToInt32(this.AlertaID));

                return vehiculoID;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al obtener el VehiculoID. ");
            }
        }

        public int GetServicioID()
        {
            try
            {
                int servicioID = 0;
                servicioID = this.alertaData.GetServicioID(Convert.ToInt32(this.AlertaID));

                return servicioID;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al obtener el servicioID. ");
            }
        }

        public bool ExisteServicio(int ServicioID)
        {
            try
            {
                bool existe = false;

                existe = this.servicioData.ExisteServicio(ServicioID);

                return existe;
            }
            catch (Exception ex)
            {
                throw new Exception("Servicio no existe. " + ex.Message);
            }
        }

        public bool ExisteVehiculo(int VehiculoID)
        {
            try
            {
                bool existe;

                existe = this.vehiculoData.ExisteVehiculoID(VehiculoID);

                return existe;
            }
            catch (Exception ex)
            {
                throw new Exception("Vehículo no existe. " + ex.Message);
            }
        }

        public void DesactivarAlerta()
        {
            this.alertaData.DesactivarByAlertaID(this.AlertaID);
        }
    }
}
