﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.GestionAlertas.View
{
    public class AlertaView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.Matrix oMatrix;
        public string FormUniqueID { get; private set; }
        private SAPbouiCOM.DBDataSource oDBDataSource;
        public int? AlertaID { get; set; }
        #endregion

        #region Constructor
        public AlertaView(SAPbouiCOM.Application Application, SAPbobsCOM.Company company, string formtype, int formcount) 
        {
            try
            {
                this.SBO_Application = Application;
                this.oItem = null;
                this.oMatrix = null;
                this.SetForm(formtype, formcount);
                oDBDataSource = null;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion


        public void SetForm(string type, int count)
        {
            try
            {
                this.oForm = SBO_Application.Forms.GetForm(type, count);
                this.FormUniqueID = this.oForm.UniqueID;
            }
            catch (Exception ex)
            {
                this.oForm = null;
                throw new Exception(ex.Message);
            }
        }

        public bool ColumHasValue(int rowIndex)//, string type, int count)
        {
            try
            {
                //this.SetForm(type, count);
                this.oMatrix = oForm.Items.Item("6").Specific;
                SAPbouiCOM.EditText txt = oMatrix.Columns.Item("V_0").Cells.Item(rowIndex).Specific;
                if (!string.IsNullOrEmpty(txt.String))
                { this.AlertaID = Convert.ToInt32(txt.String); return true; }
                return false;
            }
            catch (Exception ex) { return false; }
        }

        public bool ColumHasValue2(int rowIndex)//, string type, int count)
        {
            try
            {
                //this.SetForm(type, count);
                this.oMatrix = oForm.Items.Item("10").Specific;
                SAPbouiCOM.EditText txt = oMatrix.Columns.Item("V_0").Cells.Item(rowIndex).Specific;
                if (!string.IsNullOrEmpty(txt.String))
                { this.AlertaID = Convert.ToInt32(txt.String); return true; }
                return false;
            }
            catch (Exception ex) { return false; }
        }

        public int GetDataID(int rowIndex)
        {
            try
            {
                int id = 0;
                this.oMatrix = oForm.Items.Item("10").Specific;
                SAPbouiCOM.EditText txt = oMatrix.Columns.Item("V_1").Cells.Item(rowIndex).Specific;
                if (!string.IsNullOrEmpty(txt.String))
                    id = Convert.ToInt32(txt.String);
                return id;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

    }
}

