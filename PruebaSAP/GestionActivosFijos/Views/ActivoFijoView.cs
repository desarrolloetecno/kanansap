﻿using Kanan.Vehiculos.BO2;
using Kanan.Operaciones.BO2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KananSAP.Vehiculos.Data;
using KananSAP.Operaciones.Data;
using SAPbobsCOM;
using SAPbouiCOM;

namespace KananSAP.GestionActivosFijos.Views
{
    public class ActivoFijoView
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Item oNewItem;
        private SAPbouiCOM.Item oItem;
        private SAPbouiCOM.Folder oFolderItem;
        private SAPbouiCOM.StaticText oStaticText;
        private SAPbouiCOM.EditText oEditText;
        private SAPbouiCOM.ComboBox oComboBox;
        private SAPbouiCOM.Button oButton;
        public ActivoSAP Entity { get; private set; }
        public string FormUniqueID { get; private set; }
        public SAPbobsCOM.Company company;
        #endregion

        #region Constructor
        public ActivoFijoView(SAPbouiCOM.Application Application, SAPbobsCOM.Company company, string formtype, int formcount) 
        {
            this.Entity = new ActivoSAP(company);
            this.SBO_Application = Application;
            this.company = company;
            this.SetForm(formtype, formcount);
            this.oNewItem = null;
            this.oItem = null;
            this.oFolderItem = null;
            this.oStaticText = null;
            this.oEditText = null;
            this.oComboBox = null;
        }
        #endregion

        #region Metodos

        public void SetForm(string type, int count)
        {
            try
            {
                this.oForm = SBO_Application.Forms.GetForm(type, count);
                this.FormUniqueID = this.oForm.UniqueID;
            }
            catch (Exception ex)
            {
                this.oForm = null;
                throw new Exception(ex.Message);
            }
        }

        public void SetFormMode(SAPbouiCOM.BoFormMode mode)
        {
            this.oForm.Mode = mode;
        }

        public void InsertFolder() 
        {
            oNewItem = oForm.Items.Add("activinfo", SAPbouiCOM.BoFormItemTypes.it_FOLDER);

            // use an existing folder item for grouping and setting the
            // items properties (such as location properties)
            // use the 'Display Debug Information' option (under 'Tools')
            // in the application to acquire the UID of the desired folder
            oItem = oForm.Items.Item("1470002136");

            oNewItem.Top = oItem.Top;
            oNewItem.Height = oItem.Height;
            oNewItem.Width = oItem.Width;
            oNewItem.Left = oItem.Left + oItem.Width;

            oFolderItem = ((SAPbouiCOM.Folder)(oNewItem.Specific));

            oFolderItem.Caption = "Activo Kananfleet";
            
            // group the folder with the desired folder item
            oFolderItem.GroupWith("1470002136");       
            oFolderItem.Pane = 20;
            // add your own items to the form
        }

        public void AddItemsToForm()
        {
            const int pane = 20;

            #region ComponentePadre

            oForm.DataSources.UserDataSources.Add("CombComp", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);

            oItem = oForm.Items.Item("95");
            oNewItem = oForm.Items.Add("lblcomp", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "cmbcomp";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Componente padre";

            oItem = oForm.Items.Item("lblcomp");
            oNewItem = oForm.Items.Add("cmbcomp", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 250;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.DisplayDesc = true;

            oComboBox = ((SAPbouiCOM.ComboBox)(oNewItem.Specific));

            // bind the Combo Box item to the defined used data source
            oComboBox.DataBind.SetBound(true, "", "CombComp");

            #region FillComponentesPadres
            var rs = this.Entity.GetComponentes();
            oComboBox = oForm.Items.Item("cmbcomp").Specific;
            this.CleanComboBox(oComboBox);
            oComboBox.ValidValues.Add("0", "--Ninguno--");
            rs.MoveFirst();
            for (int i = 0; i < rs.RecordCount; i++)
            {
                string nombre = rs.Fields.Item("U_NUMEROECONOMICO").Value.ToString();
                string value = (string)Convert.ChangeType(rs.Fields.Item("U_ACTIVOID").Value, typeof(string));
                oComboBox.ValidValues.Add(value, nombre);
                rs.MoveNext();
            }

            oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index);

            oComboBox.Item.DisplayDesc = true;
            #endregion FillComponentesPadres

            #endregion ComponentePadre

            #region Tipo Activo

            oForm.DataSources.UserDataSources.Add("CombSource", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);

            oItem = oForm.Items.Item("lblcomp");
            oNewItem = oForm.Items.Add("lbltipo", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;// + oItem.Width + 20;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "cmbtipo";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Tipo de Activo";

            oItem = oForm.Items.Item("lbltipo");
            oNewItem = oForm.Items.Add("cmbtipo", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 250;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.DisplayDesc = true;

            oComboBox = ((SAPbouiCOM.ComboBox)(oNewItem.Specific));

            // bind the Combo Box item to the defined used data source
            oComboBox.DataBind.SetBound(true, "", "CombSource");
            var tipoService = new TipoActivoSAP(company);
            var componentes = tipoService.Consultar(new TipoActivos());
            var lista = tipoService.RecordSetToListTipoActivos(componentes);

            if (lista != null && lista.Count > 0)
            {
                oComboBox.ValidValues.Add("0", "--Ninguno--");
                foreach (TipoActivos tipo in lista)
                {
                    oComboBox.ValidValues.Add(tipo.TipoActivoID.ToString().Trim(), tipo.Nombre);
                }
                oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index);
            }
            #endregion

            #region TipoChasis
            oForm.DataSources.UserDataSources.Add("cmbtipo", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);

            oItem = oForm.Items.Item("lbltipo");
            oNewItem = oForm.Items.Add("lbltpcss", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "cmbtpchss";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Tipo de chasís";

            oItem = oForm.Items.Item("lbltpcss");
            oNewItem = oForm.Items.Add("cmbtpchss", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 250;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.DisplayDesc = true;

            oComboBox = ((SAPbouiCOM.ComboBox)(oNewItem.Specific));

            // bind the Combo Box item to the defined used data source
            oComboBox.DataBind.SetBound(true, "", "cmbtipo");
            FillTipoChasis();

            oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index);
            #endregion TipoChasis

            #region Sucursal

            oForm.DataSources.UserDataSources.Add("CombBranch", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);

            oItem = oForm.Items.Item("lbltpcss");
            oNewItem = oForm.Items.Add("lblbrnch", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;// + oItem.Width + 20;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "cmbbrnch";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Sucursal";

            oItem = oForm.Items.Item("lblbrnch");
            oNewItem = oForm.Items.Add("cmbbrnch", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 250;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.DisplayDesc = true;

            oComboBox = ((SAPbouiCOM.ComboBox)(oNewItem.Specific));

            // bind the Combo Box item to the defined used data source
            oComboBox.DataBind.SetBound(true, "", "CombBranch");

            var branchService = new SucursalSAP(company);
            var rsb = branchService.Consultar(new Sucursal()
            {
                EsActivo = true,
                Base = new Empresa
                {
                    EmpresaID = this.Entity.activo.Propietario.EmpresaID
                }
            });

            if (rsb != null && rsb.RecordCount > 0)
            {
                oComboBox.ValidValues.Add("0", "--Ninguno--");
                rsb.MoveFirst();
                for (int i = 0; i < rsb.RecordCount; i++)
                {
                    oComboBox.ValidValues.Add(rsb.Fields.Item("U_SUCURSALID").Value.ToString(), rsb.Fields.Item("U_DIRECCION").Value.ToString());
                    rsb.MoveNext();
                }
                oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index);
            }
            #endregion
            
            #region No Economico
            oItem = oForm.Items.Item("lblbrnch");
            oNewItem = oForm.Items.Add("lblnoecon", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtnoecon";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "No. Económico";

            oItem = oForm.Items.Item("lblnoecon");
            oNewItem = oForm.Items.Add("txtnoecon", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            #endregion

            #region Odometro
            oItem = oForm.Items.Item("lblnoecon");
            oNewItem = oForm.Items.Add("lblodom", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtodom";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Odómetro";

            oItem = oForm.Items.Item("lblodom");
            oNewItem = oForm.Items.Add("txtodom", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));
            #endregion

            #region Marca
            oItem = oForm.Items.Item("lblodom");
            oNewItem = oForm.Items.Add("lblmarca", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtmarca";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Marca";

            oItem = oForm.Items.Item("lblmarca");
            oNewItem = oForm.Items.Add("txtmarca", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            #endregion

            #region Modelo
            oItem = oForm.Items.Item("lblmarca");
            oNewItem = oForm.Items.Add("lblmodelo", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtmodelo";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Modelo";

            oItem = oForm.Items.Item("lblmodelo");
            oNewItem = oForm.Items.Add("txtmodelo", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            #endregion

            #region Año
            oItem = oForm.Items.Item("lblmodelo");
            oNewItem = oForm.Items.Add("lblanio", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtanio";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Año";

            oItem = oForm.Items.Item("lblanio");
            oNewItem = oForm.Items.Add("txtanio", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            #endregion

            #region Placa
            oItem = oForm.Items.Item("lblanio");
            oNewItem = oForm.Items.Add("lblplaca", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtplaca";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Placa";

            oItem = oForm.Items.Item("lblplaca");
            oNewItem = oForm.Items.Add("txtplaca", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            #endregion

            #region Tipo
            oItem = oForm.Items.Item("lblplaca");
            oNewItem = oForm.Items.Add("lbltipoB", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txttipo";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Tipo";

            oItem = oForm.Items.Item("lbltipoB");
            oNewItem = oForm.Items.Add("txttipo", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            #endregion Tipo

            #region Posición
            oItem = oForm.Items.Item("lbltipoB");
            oNewItem = oForm.Items.Add("lblposi", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtposi";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Posición";

            oItem = oForm.Items.Item("lblposi");
            oNewItem = oForm.Items.Add("txtposi", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            #endregion

            #region Detalle
            oItem = oForm.Items.Item("lblposi");
            oNewItem = oForm.Items.Add("lbldet", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtdet";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Detalle";

            oItem = oForm.Items.Item("lbldet");
            oNewItem = oForm.Items.Add("txtdet", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            #endregion

            #region Póliza
            oItem = oForm.Items.Item("lbldet");
            oNewItem = oForm.Items.Add("lblpoliza", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtpoliza";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Póliza";

            oItem = oForm.Items.Item("lblpoliza");
            oNewItem = oForm.Items.Add("txtpoliza", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            #endregion

            #region Vigencia de Póliza
            oForm.DataSources.UserDataSources.Add("UDDate", SAPbouiCOM.BoDataType.dt_DATE, 254);
            oItem = oForm.Items.Item("lblpoliza");
            oNewItem = oForm.Items.Add("lblvigpol", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtvigepol";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Vigencia de la Póliza";

            oItem = oForm.Items.Item("lblvigpol");
            oNewItem = oForm.Items.Add("txtvigepol", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            oEditText.DataBind.SetBound(true, "", "UDDate");
            #endregion

            #region Avisar Vencimiento antes
            oItem = oForm.Items.Item("lblvigpol");
            oNewItem = oForm.Items.Add("lblaviso", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtaviso";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Avisar vencimiento antes de";

            oItem = oForm.Items.Item("lblaviso");
            oNewItem = oForm.Items.Add("txtaviso", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));

            oItem = oForm.Items.Item("txtaviso");
            oNewItem = oForm.Items.Add("lblaviso1", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left + oItem.Width + 10;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "días";

            #endregion

            #region Plazas
            //oItem = oForm.Items.Item("lblaviso");
            //oNewItem = oForm.Items.Add("lblplazas", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            //oNewItem.Left = oItem.Left;
            //oNewItem.Width = 150;
            //oNewItem.Top = oItem.Top + 16;
            //oNewItem.Height = 14;
            //oNewItem.FromPane = pane;
            //oNewItem.ToPane = pane;

            //oNewItem.LinkTo = "txtplazas";

            //oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            //oStaticText.Caption = "Plazas";

            //oItem = oForm.Items.Item("lblplazas");
            //oNewItem = oForm.Items.Add("txtplazas", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            //oNewItem.Left = oItem.Left + oItem.Width;
            //oNewItem.Width = 150;
            //oNewItem.Top = oItem.Top;
            //oNewItem.Height = 14;
            //oNewItem.FromPane = pane;
            //oNewItem.ToPane = pane;

            //oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));

            #endregion

            #region Rendimiento Combustible
            //oItem = oForm.Items.Item("lblplazas");
            //oNewItem = oForm.Items.Add("lblrcomb", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            //oNewItem.Left = oItem.Left;
            //oNewItem.Width = 150;
            //oNewItem.Top = oItem.Top + 16;
            //oNewItem.Height = 14;
            //oNewItem.FromPane = pane;
            //oNewItem.ToPane = pane;

            //oNewItem.LinkTo = "txtrcomb";

            //oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            //oStaticText.Caption = "Rendimiento de Combustible";

            //oItem = oForm.Items.Item("lblrcomb");
            //oNewItem = oForm.Items.Add("txtrcomb", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            //oNewItem.Left = oItem.Left + oItem.Width;
            //oNewItem.Width = 150;
            //oNewItem.Top = oItem.Top;
            //oNewItem.Height = 14;
            //oNewItem.FromPane = pane;
            //oNewItem.ToPane = pane;

            //oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));

            //oItem = oForm.Items.Item("txtrcomb");
            //oNewItem = oForm.Items.Add("lblrcomb1", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            //oNewItem.Left = oItem.Left + oItem.Width + 10;
            //oNewItem.Width = 150;
            //oNewItem.Top = oItem.Top;
            //oNewItem.Height = 14;
            //oNewItem.FromPane = pane;
            //oNewItem.ToPane = pane;

            //oNewItem.LinkTo = "txtrcomb";

            //oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            //oStaticText.Caption = "Kms/Ltr";


            #endregion

            #region Rendimiento de Carga
            //oItem = oForm.Items.Item("lblrcomb");
            //oNewItem = oForm.Items.Add("lblrcarga", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            //oNewItem.Left = oItem.Left;
            //oNewItem.Width = 150;
            //oNewItem.Top = oItem.Top + 16;
            //oNewItem.Height = 14;
            //oNewItem.FromPane = pane;
            //oNewItem.ToPane = pane;

            //oNewItem.LinkTo = "txtrcarga";

            //oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            //oStaticText.Caption = "Rendimiento con Carga";

            //oItem = oForm.Items.Item("lblrcarga");
            //oNewItem = oForm.Items.Add("txtrcarga", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            //oNewItem.Left = oItem.Left + oItem.Width;
            //oNewItem.Width = 150;
            //oNewItem.Top = oItem.Top;
            //oNewItem.Height = 14;
            //oNewItem.FromPane = pane;
            //oNewItem.ToPane = pane;

            //oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));

            //oItem = oForm.Items.Item("txtrcarga");
            //oNewItem = oForm.Items.Add("lblrcarga1", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            //oNewItem.Left = oItem.Left + oItem.Width + 10;
            //oNewItem.Width = 150;
            //oNewItem.Top = oItem.Top;
            //oNewItem.Height = 14;
            //oNewItem.FromPane = pane;
            //oNewItem.ToPane = pane;

            //oNewItem.LinkTo = "txtrcomb";

            //oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            //oStaticText.Caption = "Kms/Ltr";
            #endregion

            #region CostoCompra
            oItem = oForm.Items.Item("lblaviso");
            oNewItem = oForm.Items.Add("lblcoscom", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtcoscom";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Costo de compra";

            oItem = oForm.Items.Item("lblcoscom");
            oNewItem = oForm.Items.Add("txtcoscom", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            #endregion CostoCompra

            #region Descripcion
            oItem = oForm.Items.Item("lblcoscom");
            oNewItem = oForm.Items.Add("lbldesc", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtdesc";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Descripción";

            oItem = oForm.Items.Item("lbldesc");
            oNewItem = oForm.Items.Add("txtdesc", SAPbouiCOM.BoFormItemTypes.it_EXTEDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 42;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            #endregion

            #region Observaciones
            /*oItem = oForm.Items.Item("lbldesc");
            oNewItem = oForm.Items.Add("lblobsv", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top + 16 + 28;
            oNewItem.Height = 14;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oNewItem.LinkTo = "txtobsv";

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));

            oStaticText.Caption = "Observaciones";

            oItem = oForm.Items.Item("lblobsv");
            oNewItem = oForm.Items.Add("txtobsv", SAPbouiCOM.BoFormItemTypes.it_EXTEDIT);
            oNewItem.Left = oItem.Left + oItem.Width;
            oNewItem.Width = 150;
            oNewItem.Top = oItem.Top;
            oNewItem.Height = 42;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));*/
            #endregion

            #region Boton Recarga de Combustible
            //oItem = oForm.Items.Item("1470002217");
            //oNewItem = oForm.Items.Add("btnrcomb", SAPbouiCOM.BoFormItemTypes.it_BUTTON);
            //oNewItem.Left = oItem.Left;
            //oNewItem.Width = 200;
            //oNewItem.Top = oItem.Top;
            //oNewItem.Height = 20;
            //oNewItem.FromPane = pane;
            //oNewItem.ToPane = pane;

            //oButton = ((SAPbouiCOM.Button)(oNewItem.Specific));
            //oButton.Caption = "Agregar carga de combustible";
            #endregion

            #region Boton Para Configurar Servicio
            oItem = oForm.Items.Item("1470002217");
            oNewItem = oForm.Items.Add("btnaddsvc", SAPbouiCOM.BoFormItemTypes.it_BUTTON);
            oNewItem.Left = oItem.Left + 50;
            oNewItem.Width = 200;
            oNewItem.Top = oItem.Top - 21;
            oNewItem.Height = 20;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oButton = (SAPbouiCOM.Button)oNewItem.Specific;
            oButton.Caption = "Configurar servicio a Activo";
            #endregion

            #region Boton Para Asignar Llantas a Activo
            oItem = oForm.Items.Item("btnaddsvc");
            oNewItem = oForm.Items.Add("btnLlantas", SAPbouiCOM.BoFormItemTypes.it_BUTTON);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 200;
            oNewItem.Top = oItem.Top + 25;
            oNewItem.Height = 20;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;
            oButton = (SAPbouiCOM.Button)oNewItem.Specific;
            oButton.Caption = "Asignar Llantas";
            #endregion

            #region Boton Para Incidencia de vehiculos
            oItem = oForm.Items.Item("btnLlantas");
            oNewItem = oForm.Items.Add("btnInci", SAPbouiCOM.BoFormItemTypes.it_BUTTON);
            oNewItem.Visible = true;
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 200;
            oNewItem.Top = oItem.Top + 25;
            oNewItem.Height = 20;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oButton = (SAPbouiCOM.Button)oNewItem.Specific;
            oButton.Caption = "Incidencia en activo";
            //oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));
            #endregion

            #region Boton Reporte Detalle de Recarga de Combustible
            //oItem = oForm.Items.Item("btnInci");
            //oNewItem = oForm.Items.Add("btnreport", SAPbouiCOM.BoFormItemTypes.it_BUTTON);
            //oNewItem.Left = oItem.Left;
            //oNewItem.Width = 200;
            //oNewItem.Top = oItem.Top + 25;
            //oNewItem.Height = 20;
            //oNewItem.FromPane = pane;
            //oNewItem.ToPane = pane;

            //oButton = ((SAPbouiCOM.Button)(oNewItem.Specific));
            //oButton.Caption = "Reporte detalle de cargas de combustible";
            #endregion

            #region PanelDisponibilidadVehiculos
            oItem = oForm.Items.Item("btnInci");
            oNewItem = oForm.Items.Add("btnDisp", SAPbouiCOM.BoFormItemTypes.it_EDIT);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 200;
            oNewItem.Top = oItem.Top + 25;
            oNewItem.Height = 35;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oEditText = ((SAPbouiCOM.EditText)(oNewItem.Specific));
            oEditText.String = "Estatus de activo";
            oEditText.Item.Enabled = false; //Se establece como no editable.
            #endregion

            #region Simbologia
            oItem = oForm.Items.Item("btnDisp");
            oNewItem = oForm.Items.Add("lblSimb", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 200;
            oNewItem.Top = oItem.Top + 40;
            oNewItem.Height = 20;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));
            oStaticText.Caption = "Verde = Disponible";
            oStaticText.Item.Enabled = false; //Se establece como no editable.

            oItem = oForm.Items.Item("lblSimb");
            oNewItem = oForm.Items.Add("lblSimb2", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 200;
            oNewItem.Top = oItem.Top + 15;
            oNewItem.Height = 20;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));
            oStaticText.Caption = "Rojo = Incidencia";
            oStaticText.Item.Enabled = false; //Se establece como no editable.

            oItem = oForm.Items.Item("lblSimb2");
            oNewItem = oForm.Items.Add("lblSimb3", SAPbouiCOM.BoFormItemTypes.it_STATIC);
            oNewItem.Left = oItem.Left;
            oNewItem.Width = 200;
            oNewItem.Top = oItem.Top + 15;
            oNewItem.Height = 20;
            oNewItem.FromPane = pane;
            oNewItem.ToPane = pane;

            oStaticText = ((SAPbouiCOM.StaticText)(oNewItem.Specific));
            oStaticText.Caption = "Amarillo = Mantenimiento";
            oStaticText.Item.Enabled = false; //Se establece como no editable.
            #endregion Simbologia
        }

        private void FillTipoChasis()
        {
            oComboBox = oForm.Items.Item("cmbtpchss").Specific;
            this.CleanComboBox(oComboBox);
            oComboBox.ValidValues.Add("0", "--Ninguno--");
            //oComboBox.ValidValues.Add("1", "Auto");
            //oComboBox.ValidValues.Add("2", "Moto");
            //oComboBox.ValidValues.Add("4", "Monta Carga");
            //oComboBox.ValidValues.Add("6", "Tracto / Camión");
            oComboBox.ValidValues.Add("7", "Caja 1");
            oComboBox.ValidValues.Add("8", "Caja 2");
            oComboBox.ValidValues.Add("9", "Doly");
            oComboBox.Item.DisplayDesc = true;
        }

        private void CleanComboBox(ComboBox combo)
        {
            if (combo == null || combo.ValidValues == null || combo.ValidValues.Count <= 0)
                return;
            var f = combo.ValidValues.Count;
            for (int i = 0; i < f; i++)
            {
                combo.ValidValues.Remove(i, BoSearchKey.psk_Index);
                f--;
                i--;
            }
        }

        public string FormFrozen()
        {
            string strFrozendate = string.Empty;
            try
            {
                oItem = this.GetItemFromForm("1000020");//.oForm.Items.Item("frozeenFrom");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
                if (oEditText.Value != "")
                    strFrozendate = Convert.ToDateTime(oEditText.Value).ToString("yyyy-MM-ddTHH:mm:ss"); 

            }catch(Exception ex)
            {
            }
            return strFrozendate;
        }
        public void FormToEntity()
        {
            try
            {
                oItem = this.oForm.Items.Item("cmbcomp");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                this.Entity.activo.ComponenteID = oComboBox.Selected == null ? (int?)null : Convert.ToInt32(oComboBox.Selected.Value);
                if (this.Entity.activo.ComponenteID == null | this.Entity.activo.ComponenteID <= 0)
                    this.Entity.activo.ComponenteID = null;
            }
            catch (Exception)
            {
                this.Entity.activo.ComponenteID = null;
            }

            try
            {
                oItem = this.oForm.Items.Item("cmbtipo");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                this.Entity.activo.TipoActivos = new TipoActivos { TipoActivoID = oComboBox.Selected == null ? (int?)null : Convert.ToInt32(oComboBox.Selected.Value) };
            }
            catch (Exception)
            {
                this.Entity.activo.TipoActivos = null;
            }

            try
            {
                oItem = this.oForm.Items.Item("cmbbrnch");
                oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
                this.Entity.activo.SubPropietario = new Sucursal { SucursalID = oComboBox.Selected == null ? (int?)null : Convert.ToInt32(oComboBox.Selected.Value) };
            }
            catch (Exception)
            {
                this.Entity.activo.SubPropietario = null;
            }

            oItem = this.oForm.Items.Item("txtnoecon");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.activo.NumeroEconomico = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;

            oItem = this.oForm.Items.Item("txtdet");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            int detalle = !string.IsNullOrEmpty(oEditText.String) ? Convert.ToInt32(oEditText.String) : 0;
            if (detalle > 0 || detalle != null)
                this.Entity.activo.Detalle = detalle;
            /*Comentado para manejar devidamente el campo Tipo.*/
            //this.Entity.activo.Tipo = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;

            oItem = this.oForm.Items.Item("txtmarca");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.activo.Marca = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;

            oItem = this.oForm.Items.Item("txtmodelo");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.activo.Modelo = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;

            oItem = this.oForm.Items.Item("txtplaca");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.activo.Placa = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;

            oItem = this.oForm.Items.Item("txtanio");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (!string.IsNullOrEmpty(oEditText.String))
                this.Entity.activo.Anio = Convert.ToInt32(oEditText.String);

            oItem = this.oForm.Items.Item("txtpoliza");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (!string.IsNullOrEmpty(oEditText.String))
                this.Entity.activo.Poliza = Convert.ToInt32(oEditText.String);

            oItem = this.oForm.Items.Item("txtvigepol");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (!string.IsNullOrEmpty(oEditText.String))
                this.Entity.activo.VencimientoPoliza = Convert.ToDateTime(oEditText.String);

            oItem = this.oForm.Items.Item("txtaviso");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (!string.IsNullOrEmpty(oEditText.String))
                this.Entity.activo.AvisoPoliza = Convert.ToInt32(oEditText.String);

            //oItem = this.oForm.Items.Item("txtplazas");
            //oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            //if (!string.IsNullOrEmpty(oEditText.String))
            //    this.Entity.activo.Plazas = Convert.ToInt32(oEditText.String);

            //oItem = this.oForm.Items.Item("txtrcomb");
            //oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            //if (!string.IsNullOrEmpty(oEditText.String))
            //    this.Entity.activo.RendCarga = Convert.ToInt32(oEditText.String);

            //oItem = this.oForm.Items.Item("txtrcarga");
            //oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            //if (!string.IsNullOrEmpty(oEditText.String))
            //    this.Entity.activo.RendComb = Convert.ToInt32(oEditText.String);

            oItem = this.oForm.Items.Item("txttipo");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (!string.IsNullOrEmpty(oEditText.String))
                this.Entity.activo.Tipo = oEditText.String;

            oItem = this.oForm.Items.Item("txtposi");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (!string.IsNullOrEmpty(oEditText.String))
                this.Entity.activo.Posicion = Convert.ToInt32(oEditText.String);

            oItem = this.oForm.Items.Item("txtdesc");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            this.Entity.activo.Descripcion = !string.IsNullOrEmpty(oEditText.String) ? oEditText.String : null;
            this.Entity.activo.Observaciones = this.Entity.activo.Descripcion;
        }

        public void EntityToForm()
        {
            /*Se valida que contengan datos.*/
            if (this.Entity.activo.CajaID <= 0)
                this.Entity.UDTToActivo();
            if (this.Entity != null)
                if (this.Entity.activo == null)
                    return;

            oItem = this.oForm.Items.Item("cmbbrnch");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            if (this.Entity.activo.SubPropietario != null && this.Entity.activo.SubPropietario.SucursalID != null)
            {
                oComboBox.Select(this.Entity.activo.SubPropietario.SucursalID.ToString());
                oComboBox.Select(this.Entity.activo.SubPropietario.SucursalID.ToString());
            }
            else
            {
                oComboBox.Select("0");
                oComboBox.Select("0");
            }

            oItem = this.oForm.Items.Item("cmbcomp");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            if (this.Entity.activo.ComponenteID != null && this.Entity.activo.ComponenteID > 0)
            {
                oComboBox.Select(this.Entity.activo.ComponenteID.ToString(), BoSearchKey.psk_ByDescription);
                oComboBox.Select(this.Entity.activo.ComponenteID.ToString(), BoSearchKey.psk_ByDescription);
            }
            else
            {
                oComboBox.Select("0");
                oComboBox.Select("0");
            }

            oItem = this.oForm.Items.Item("cmbtipo");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            if (this.Entity.activo.TipoActivos != null && this.Entity.activo.TipoActivos.TipoActivoID != null)
            {
                oComboBox.Select(this.Entity.activo.TipoActivos.TipoActivoID.ToString());
                oComboBox.Select(this.Entity.activo.TipoActivos.TipoActivoID.ToString());
            }
            else
            {
                oComboBox.Select("0");
                oComboBox.Select("0");
            }

            oItem = this.oForm.Items.Item("cmbtpchss");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            if (this.Entity.activo != null && this.Entity.activo.TipoChasis != null)
            {
                oComboBox.Select(this.Entity.activo.TipoChasis.ToString());
                oComboBox.Select(this.Entity.activo.TipoChasis.ToString());
            }
            else
            {
                oComboBox.Select("0");
                oComboBox.Select("0");
            }

            oItem = this.oForm.Items.Item("txtnoecon");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = this.Entity.activo.NumeroEconomico;

            oItem = this.oForm.Items.Item("txtmarca");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = this.Entity.activo.Marca;

            oItem = this.oForm.Items.Item("txtmodelo");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = this.Entity.activo.Modelo;

            oItem = this.oForm.Items.Item("txtplaca");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = Convert.ToString(this.Entity.activo.Placa);

            oItem = this.oForm.Items.Item("txtanio");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = Convert.ToString(this.Entity.activo.Anio);

            oItem = this.oForm.Items.Item("txtdet");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = Convert.ToString(this.Entity.activo.Detalle);

            oItem = this.oForm.Items.Item("txtpoliza");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = Convert.ToString(this.Entity.activo.Poliza);

            oItem = this.oForm.Items.Item("txtvigepol");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            String VigePoliza = String.Empty;
            if (this.Entity.activo.VencimientoPoliza != null)
                VigePoliza = this.Entity.activo.VencimientoPoliza.Value.ToString("dd-MM-yyyy");
            oEditText.String = VigePoliza;

            oItem = this.oForm.Items.Item("txtaviso");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = Convert.ToString(this.Entity.activo.AvisoPoliza);

            //oItem = this.oForm.Items.Item("txtplazas");
            //oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            //oEditText.String = Convert.ToString(this.Entity.activo.Plazas);

            //oItem = this.oForm.Items.Item("txtrcomb");
            //oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            //oEditText.String = Convert.ToString(this.Entity.activo.RendComb);

            //oItem = this.oForm.Items.Item("txtrcarga");
            //oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            //oEditText.String = Convert.ToString(this.Entity.activo.RendCarga);

            oItem = this.oForm.Items.Item("txtposi");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = Convert.ToString(this.Entity.activo.Posicion);

            oItem = this.oForm.Items.Item("txttipo");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = Convert.ToString(this.Entity.activo.Tipo);

            oItem = this.oForm.Items.Item("txtdesc");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = this.Entity.activo.Descripcion;

            /*Comentado debido a que ahora se maneja en el campo Observación*/
            /*oItem = this.oForm.Items.Item("txtobsv");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = this.Entity.activo.Observaciones;*/

        }

        public void EmptyEntityToForm()
        {
            oItem = this.oForm.Items.Item("cmbcomp");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select("0");
            oComboBox.Select("0");

            oItem = this.oForm.Items.Item("cmbtipo");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select("0", SAPbouiCOM.BoSearchKey.psk_ByDescription);
            oComboBox.Select("0", SAPbouiCOM.BoSearchKey.psk_ByDescription);

            oItem = this.oForm.Items.Item("cmbtpchss");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select("0", SAPbouiCOM.BoSearchKey.psk_ByDescription);
            oComboBox.Select("0", SAPbouiCOM.BoSearchKey.psk_ByDescription);

            oItem = this.oForm.Items.Item("cmbbrnch");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            oComboBox.Select("0", SAPbouiCOM.BoSearchKey.psk_ByDescription);
            oComboBox.Select("0", SAPbouiCOM.BoSearchKey.psk_ByDescription);

            oItem = this.oForm.Items.Item("txtnoecon");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtmarca");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtmodelo");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtanio");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtplaca");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txttipo");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtposi");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtdet");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtpoliza");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtvigepol");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            oItem = this.oForm.Items.Item("txtaviso");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;

            //oItem = this.oForm.Items.Item("txtplazas");
            //oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            //oEditText.String = string.Empty;

            //oItem = this.oForm.Items.Item("txtrcomb");
            //oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            //oEditText.String = string.Empty;

            //oItem = this.oForm.Items.Item("txtrcarga");
            //oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            //oEditText.String = string.Empty;

            /*Se refleja el mismo valor en el campo descripción.*/
            /*oItem = this.oForm.Items.Item("txtobsv");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;*/

            oItem = this.oForm.Items.Item("txtdesc");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            oEditText.String = string.Empty;
        }

        public void ValidarDatos()
        {
            this.FormToEntity();

            oItem = this.oForm.Items.Item("txtnoecon");
            oEditText = (SAPbouiCOM.EditText)(oItem.Specific);
            if (string.IsNullOrEmpty(oEditText.String))
            {
                throw new Exception("El dato 'No. Economico' es obligatorio para el registro de vehiculos KananFleet");
            }

            oItem = this.oForm.Items.Item("cmbtipo");
            oComboBox = (SAPbouiCOM.ComboBox)(oItem.Specific);
            if (oComboBox.Selected == null)
            {
                throw new Exception("Seleccione un 'Tipo de Vehículo'");
            }
        }

        public bool IsActivoKf()
        {
            string group = string.Empty;
            if (this.oForm == null) return false;
            //var item = this.GetItemFromForm("1470002153");
            var item = this.GetItemFromForm("39");
            if (item == null) return false;
            oComboBox = (SAPbouiCOM.ComboBox)(item.Specific);
            @group = oComboBox.Selected.Description.ToString();
            return group.Trim().ToUpper() == "ACTIVOS KF";
        }

        #region SetOdometro
        public void SetOdometro(string kilometro)
        {
            try
            {
                oItem = this.oForm.Items.Item("txtodom");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                oStaticText.Caption = kilometro;
                //oItem.Enabled = false;
            }
            catch (Exception ex)
            {
                oItem = this.oForm.Items.Item("txtodom");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                oStaticText.Caption = "Error";
                throw new Exception("Error al intentar cargar el odómetro del vehículo. Error: " + ex.Message);
            }
        }
        #endregion SetOdometro

        #region SetEstatus
        public void SetEstatus(string EstatusActivo)
        {
            try
            {
                oItem = this.oForm.Items.Item("btnDisp");
                oEditText = (SAPbouiCOM.EditText)(oItem.Specific);

                switch (EstatusActivo)
                {
                    case "Disponible":
                        oEditText.String = "Disponible";
                        oEditText.FontSize = 25;
                        oEditText.ForeColor = 65280;
                        break;
                    case "Incidencia":
                        oEditText.String = "Incidencia";
                        oEditText.FontSize = 25;
                        oEditText.ForeColor = 255;
                        break;
                    case "Mantenimiento":
                        oEditText.String = "Mantenimiento";
                        oEditText.FontSize = 25;
                        oEditText.ForeColor = 65535;
                        break;
                    case "Rentado":
                        oEditText.String = "Rentado";
                        oEditText.FontSize = 25;
                        oEditText.ForeColor= 16776960;
                        break;
                    //case "Rentado":
                    //    oEditText.String = "Rentado";
                    //    oEditText.BackColor = 16776960;
                    //    break;

                    default:
                        oEditText.String = "Undefined";
                        break;
                }
            }
            catch (Exception ex)
            {
                oItem = this.oForm.Items.Item("btnDisp");
                oStaticText = (SAPbouiCOM.StaticText)(oItem.Specific);
                oStaticText.Caption = "Error";
                throw new Exception("Error al intentar cargar el estatus del activo. Error: " + ex.Message);
            }
        }
        #endregion SetEstatus

        #region  Metodos Auxiliares

        public Item GetItemFromForm(string ItemID)
        {
            try
            {
                oItem = this.oForm.Items.Item(ItemID);
                return oItem;
            }
            catch
            {
                return null;
            }
        }

        public void ChangeFolder(int panelview)
        {
            this.oForm.PaneLevel = panelview;
        }

        public BoFormMode GetFormMode(string FormUID)
        {
            this.oForm = SBO_Application.Forms.Item(FormUID);
            return this.oForm.Mode;
        }
        #endregion

        #endregion
    }
}
