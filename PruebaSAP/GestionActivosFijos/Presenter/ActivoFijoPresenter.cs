﻿using System;
using System.Collections.Generic;
using System.Linq;
using KananSAP.GestionActivosFijos.Views;
using System.Text;
using System.Threading.Tasks;
using KananSAP.Reportes.Presenter;
using Kanan.Vehiculos.BO2;
using KananSAP.GestionVehiculos.RecargaCombustible.Presenter;
using KananSAP.GestionVehiculos.IncidenciaVehiculo.Presenter;
using KananSAP.GestionServicios.CatalogoServicios.Presenter;
using KananWS.Interface;
using SAPbouiCOM;

namespace KananSAP.GestionActivosFijos.Presenter
{
    public class ActivoFijoPresenter
    {
        #region Atributos
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private SAPbouiCOM.Item oItem;
        private bool IsMessageBoxElimina = false;
        private bool Modal;
        private bool ModalRep;
        private bool ModalDisp;
        private bool ModalServicio;
        private bool ModalSucursal { get; set; }

        private bool ModalIncidencia;
        private bool ModalEditarIncidencia;
        private bool ModalPictureIncidencia;
        private bool ModalGeolocalizacionIncidencia;

        private bool ModalCarga;
        private bool ModalPictureCarga;
        private bool ModalGeolocalizacionCarga;

        private bool ModalConfigServicio { get; set; }
        private bool ShowConfigServVehi { get; set; }
        private bool GuardConfigServVehi { get; set; }

        //private List<ActivoFijoView> Views;
        private ActivoFijoView view;
        private VehiculoWS VehiculoService;
        private Configurations configs;
        private GestionGlobales oGlobales = null;
        private GestionCargasPresenter recarga;
        private InciVehiculoPresenter incidencia;
        private ListaServicioConfigPresenter lstServiciosConfigPresenter;
        private GestionOperaciones.AdminSucursal.Presenter.SucursalAdmonPresenter admonSucursalPresenter;

        public Reportes.Presenter.ReportePresenter ReportesPresenter { get; set; }
        private string EndPoint;
        private bool ejecuta;
        public bool Foco;
        private BoFormMode mode;
        Helper.KananSAPHerramientas oHerramientas;
        public bool IsLogged { get; set; }
        #endregion

        public ActivoFijoPresenter(SAPbouiCOM.Application sApplication, SAPbobsCOM.Company Cmpny, Configurations c, GestionGlobales oGlobales)
        {
            this.oGlobales = oGlobales;
            this.SBO_Application = sApplication;
            this.Company = Cmpny;
            this.configs = c;
            this.ejecuta = false;
            //this.Views = new List<ActivoFijoView>();
            try
            {
                this.VehiculoService = new VehiculoWS(Guid.Parse(this.configs.UserFull.Usuario.PublicKey), this.configs.UserFull.Usuario.PrivateKey);
            }
            catch
            {
                this.VehiculoService = new VehiculoWS(new Guid(), string.Empty);
            }

            this.Modal = false;
            this.ModalIncidencia = false;
            this.ModalRep = false;
            this.ModalDisp = false;
            this.ModalServicio = false;
            this.ModalSucursal = false;

            this.ModalIncidencia = false;
            this.ModalPictureIncidencia = false;
            this.ModalGeolocalizacionIncidencia = false;

            this.ModalCarga = false;
            this.ModalPictureCarga = false;
            this.ModalGeolocalizacionCarga = false;

            this.ModalConfigServicio = false;
            this.ShowConfigServVehi = false;
            this.GuardConfigServVehi = false;

            this.Foco = false;

            this.oHerramientas = new Helper.KananSAPHerramientas(this.SBO_Application);
            this.EndPoint = System.Configuration.ConfigurationManager.AppSettings.Get("EndPoint");

        }

        /*private void SetActiveView(string FormUID)
        {
            this.view = Views.First(x => x.FormUniqueID == FormUID);
        }*/

        #region Eventos

        #region Application Events

        public void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            if (!IsLogged) return;
            string group = string.Empty;
            try
            {
                #region FotoGeo EditarIncidencia en GestionIncidencia
                if (ModalEditarIncidencia)
                {
                    BubbleEvent = false;
                    ModalEditarIncidencia = false;
                    return;
                }

                if (ModalPictureIncidencia)
                {
                    BubbleEvent = false;
                    ModalPictureIncidencia = false;
                    this.incidencia.ReportesPresenter.view.ShowForm();
                    return;
                }

                if (ModalGeolocalizacionIncidencia)
                {
                    BubbleEvent = false;
                    ModalGeolocalizacionIncidencia = false;
                    this.incidencia.ReportesPresenter.view.ShowForm();
                    return;
                }

                if (pVal.FormTypeEx == "EInci")
                {
                    #region BeforeAction
                    if (pVal.BeforeAction)
                    {
                        if (pVal.ItemUID == "btnphoto" && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.incidencia.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepFotos", @EndPoint + "Vehiculos/ImagenesIncidenciaVehiculo.aspx?_modal=true&sap=true&IncidenciaId=" + this.incidencia.view.id);
                            BubbleEvent = false;
                            this.ModalPictureIncidencia = true;
                        }

                        if (pVal.ItemUID == "btngeo" && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.incidencia.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepGeopos", @EndPoint + "Vehiculos/GeoposicionRecarga.aspx?_modal=true&sap=true&incive=1&IncidenciaVehiculoID=" + this.incidencia.view.id);
                            BubbleEvent = false;
                            this.ModalGeolocalizacionIncidencia = true;
                        }
                    }
                    #endregion

                    #region AfterAction
                    else
                    {

                    }
                    #endregion
                }
                #endregion FotoGeo EditarIncidencia en GestionIncidencia

                #region GestionCargas
                if (ModalPictureCarga)
                {
                    BubbleEvent = false;
                    ModalPictureCarga = false;
                    this.recarga.ReportesPresenter.view.ShowForm();
                    return;
                }
                if (ModalGeolocalizacionCarga)
                {
                    BubbleEvent = false;
                    ModalGeolocalizacionCarga = false;
                    this.recarga.ReportesPresenter.view.ShowForm();
                    return;
                }
                if (pVal.FormTypeEx == "CargaCom")
                {
                    #region BeforeAction
                    if (pVal.BeforeAction)
                    {
                        if (pVal.ItemUID == "btnphoto" && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.recarga.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepFotos", @EndPoint + "Vehiculos/ImagenesRecarga.aspx?_modal=true&sap=true&CargaCombustibleID=" + this.recarga.cargaview.Entity.carga.CargaCombustibleID);
                            BubbleEvent = false;
                            this.ModalPictureCarga = true;
                        }
                        if (pVal.ItemUID == "btngeo" && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.recarga.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepGeopos", @EndPoint + "Vehiculos/GeoposicionRecarga.aspx?_modal=true&sap=true&CargaCombustibleID=" + this.recarga.cargaview.Entity.carga.CargaCombustibleID);
                            BubbleEvent = false;
                            this.ModalGeolocalizacionCarga = true;
                        }
                    }
                    #endregion BeforeAction

                    #region AfterAction
                    else
                    {

                    }
                    #endregion AfterAction
                }
                #endregion GestionCargas

                #region Servicios

                #region LstServ
                if (pVal.FormTypeEx == "LstServ")
                {
                    if (Modal)
                    {
                        lstServiciosConfigPresenter.listaServicioView.ShowForm();
                        BubbleEvent = false;
                        return;
                    }

                    if (ModalConfigServicio)
                    {
                        lstServiciosConfigPresenter.configServicioView.ShowForm();
                        ModalConfigServicio = false;
                        BubbleEvent = false;
                        return;
                    }

                    if (ShowConfigServVehi)
                    {
                        //BubbleEvent = false;
                        ShowConfigServVehi = false;
                        //Modal encargado de mostrar el formulario.
                        lstServiciosConfigPresenter.listaServicioView.InitializeIDS();
                        lstServiciosConfigPresenter.configServicioView.ShowForm();
                        lstServiciosConfigPresenter.configServicioView.oServicio = null;
                        lstServiciosConfigPresenter.configServicioView.EsActivo = true;
                        lstServiciosConfigPresenter.configServicioView.oActivo = lstServiciosConfigPresenter.listaServicioView.oActivo;
                        lstServiciosConfigPresenter.configServicioView.ClearAllFields();
                        lstServiciosConfigPresenter.configServicioView.PrepareNewEntity();
                        lstServiciosConfigPresenter.configServicioView.SetServiceVehicleData();
                        lstServiciosConfigPresenter.configServicioView.HideCheckBox();
                        lstServiciosConfigPresenter.configServicioView.HiddeDeleteButton();
                        return;
                    }

                    if (pVal.BeforeAction)
                    {
                        #region EditarParametroServicio
                        if (pVal.EventType == BoEventTypes.et_MATRIX_LINK_PRESSED)
                        {
                            lstServiciosConfigPresenter.listaServicioView.SetID(pVal.Row);
                            if (lstServiciosConfigPresenter.listaServicioView.oServicio != null && lstServiciosConfigPresenter.listaServicioView.oActivo != null)
                            {
                                if (lstServiciosConfigPresenter.listaServicioView.oServicio.ServicioID != null && lstServiciosConfigPresenter.listaServicioView.oActivo.CajaID!= null)
                                {
                                    ModalConfigServicio = true;
                                    lstServiciosConfigPresenter.configServicioView.ShowForm();
                                    lstServiciosConfigPresenter.configServicioView.oServicio = lstServiciosConfigPresenter.listaServicioView.oServicio;
                                    lstServiciosConfigPresenter.configServicioView.EsActivo = true;
                                    lstServiciosConfigPresenter.configServicioView.oActivo = lstServiciosConfigPresenter.listaServicioView.oActivo;
                                    lstServiciosConfigPresenter.configServicioView.ClearAllFields();
                                    lstServiciosConfigPresenter.configServicioView.PrepareNewEntity();
                                    lstServiciosConfigPresenter.configServicioView.SetServiceVehicleData();
                                    if (lstServiciosConfigPresenter.configServicioView.VerificaExistenciaParametro())
                                        lstServiciosConfigPresenter.configServicioView.ShowCheckBox();
                                    else
                                        lstServiciosConfigPresenter.configServicioView.HideCheckBox();
                                }
                            }
                            BubbleEvent = false;
                        }
                        #endregion EditarParametroServicio

                        #region EliminarParametroServicio
                        if (pVal.ItemUID == "btnDelLst" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            if (lstServiciosConfigPresenter.listaServicioView.PMantenimientoID.HasValue && lstServiciosConfigPresenter.listaServicioView.PServicioID.HasValue)
                            {
                                if (lstServiciosConfigPresenter.listaServicioView.EliminarParametros())
                                {
                                    this.SBO_Application.StatusBar.SetText("¡Parámetro de Mantenimiento Eliminado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                    lstServiciosConfigPresenter.listaServicioView.ShowParametrosMantenimiento();
                                }
                                else
                                    lstServiciosConfigPresenter.listaServicioView.InitializeIDS();
                            }
                        }
                        #endregion EliminarParametroServicio

                        #region AgregarParametroServicio
                        if (pVal.ItemUID == "btnAddPMto" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            ShowConfigServVehi = true;
                            return;
                            /*lstServiciosConfigPresenter.configServicioView.ShowForm();
                            lstServiciosConfigPresenter.configServicioView.oServicio = null;
                            lstServiciosConfigPresenter.configServicioView.oVehiculo = lstServiciosConfigPresenter.listaServicioView.oVehiculo;
                            lstServiciosConfigPresenter.configServicioView.ClearAllFields();
                            lstServiciosConfigPresenter.configServicioView.PrepareNewEntity();
                            lstServiciosConfigPresenter.configServicioView.SetServiceVehicleData();
                            lstServiciosConfigPresenter.configServicioView.HideCheckBox();
                            lstServiciosConfigPresenter.configServicioView.HiddeDeleteButton();*/
                        }
                        #endregion AgregarParametroServicio
                    }

                    #region FormClose
                    if (pVal.EventType == BoEventTypes.et_FORM_CLOSE && this.Modal)
                    {
                        this.Modal = false;
                        this.Foco = false;
                        lstServiciosConfigPresenter.configServicioView.TipoParametro = 0;
                    }
                    #endregion FormClose

                    #region UnloadForm
                    if (pVal.EventType == BoEventTypes.et_FORM_UNLOAD)
                    {
                        this.Modal = false;
                        this.Foco = false;
                        lstServiciosConfigPresenter.configServicioView.TipoParametro = 0;
                    }
                    #endregion UnloadForm
                }
                #endregion LstServ

                #region ConfSvForm
                if (pVal.FormTypeEx == "ConfSvForm")
                {
                    if (pVal.BeforeAction)
                    {
                        #region GuardarParametroServicio
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnAConfSv")
                        {
                            try
                            {
                                lstServiciosConfigPresenter.configServicioView.InsertOrUpdateParameters();
                                //Se utiliza el SBO_Application para generar los mensajes.
                                this.SBO_Application.StatusBar.SetText("Parámetros de mantenimiento guardados", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                lstServiciosConfigPresenter.listaServicioView.InitializeIDS();
                                lstServiciosConfigPresenter.listaServicioView.ShowParametrosMantenimiento();
                                BubbleEvent = false;
                                return;
                            }
                            catch (Exception ex)
                            {
                                lstServiciosConfigPresenter.listaServicioView.InitializeIDS();
                                lstServiciosConfigPresenter.listaServicioView.ShowParametrosMantenimiento();
                                KananSAP.Helper.KananSAPHerramientas.LogError("ActivoFijoPresenter.cs->ConfSvForm->GuardarParametroServicio()", ex.Message);
                                throw new Exception(ex.Message);
                            }

                        }
                        #endregion GuardarParametroServicio

                        #region ConfigurarParametroServicio
                        if (pVal.ItemUID == "btnCConfSv" && pVal.EventType == BoEventTypes.et_ITEM_PRESSED)
                        {
                            ModalConfigServicio = false;
                            lstServiciosConfigPresenter.listaServicioView.InitializeIDS();
                            lstServiciosConfigPresenter.configServicioView.ClearAllFields();
                            this.SBO_Application.Forms.ActiveForm.Close();
                        }
                        #endregion ConfigurarParametroServicio

                        #region EliminarParametroServicio
                        if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnDelPMS")
                        {
                            if (lstServiciosConfigPresenter.listaServicioView.PMantenimientoID.HasValue && lstServiciosConfigPresenter.listaServicioView.PServicioID.HasValue)
                            {
                                if (lstServiciosConfigPresenter.listaServicioView.EliminarParametros())
                                {
                                    this.SBO_Application.StatusBar.SetText("¡Parámetro de Mantenimiento Eliminado!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                    lstServiciosConfigPresenter.listaServicioView.ShowParametrosMantenimiento();
                                    ModalConfigServicio = false;
                                    this.SBO_Application.Forms.ActiveForm.Close();
                                }
                                else
                                    lstServiciosConfigPresenter.listaServicioView.InitializeIDS();
                            }
                        }
                        #endregion EliminarParametroServicio

                        #region CloseForm
                        if (pVal.EventType == BoEventTypes.et_FORM_CLOSE && this.ModalConfigServicio)
                        {
                            this.ModalConfigServicio = false;
                            lstServiciosConfigPresenter.configServicioView.oServicio = null;
                            lstServiciosConfigPresenter.configServicioView.TipoParametro = 0;
                        }
                        #endregion CloseForm

                        #region UnloadForm
                        if (pVal.EventType == BoEventTypes.et_FORM_UNLOAD)
                        {
                            lstServiciosConfigPresenter.configServicioView.oServicio = null;
                            this.ModalConfigServicio = false;
                            lstServiciosConfigPresenter.configServicioView.TipoParametro = 0;
                        }
                        #endregion UnloadForm
                    }
                }
                #endregion

                #endregion Servicios

                #region Form Datos Maestros de Activo Fijo
                if (pVal.FormTypeEx == "1473000075")
                {
                    if (this.view != null)
                    {
                        SAPbouiCOM.ComboBox oCombo;
                        var item = this.view.GetItemFromForm("39");
                        if (item != null)
                        {
                            oCombo = (SAPbouiCOM.ComboBox)(item.Specific);
                            @group = oCombo.Selected != null ? oCombo.Selected.Description : string.Empty;
                        }
                    }

                    #region Modales
                    if (ModalIncidencia)
                    {
                        BubbleEvent = false;
                        ModalIncidencia = false;
                        this.incidencia.view.LoadForm();
                        this.incidencia.view.ShowForm();
                        return;
                    }
                    if (ModalCarga)
                    {
                        BubbleEvent = false;
                        ModalCarga = false;
                        this.recarga.view.LoadForm();
                        this.recarga.view.ShowForm();
                        return;
                    }

                    if (ModalRep)
                    {
                        BubbleEvent = false;
                        ModalRep = false;
                        ReportesPresenter.view.ShowForm();
                        return;
                    }

                    if (ModalDisp)
                    {
                        BubbleEvent = false;
                        ModalDisp = false;
                        ReportesPresenter.view.ShowForm();
                        return;
                    }

                    if (ModalServicio)
                    {
                        ModalServicio = false;
                        BubbleEvent = false;
                        lstServiciosConfigPresenter.listaServicioView.ShowForm();
                        return;
                    }

                    if (ModalSucursal)
                    {
                        ModalSucursal = false;
                        BubbleEvent = false;
                        this.admonSucursalPresenter.view.ChangeForm();
                        this.admonSucursalPresenter.view.ShowForm();
                        return;
                    }
                    #endregion Modales

                    #region Before Action
                    if (!pVal.Before_Action)
                    {
                        #region Form Load
                        if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_LOAD)
                        {
                            this.view = new ActivoFijoView(this.SBO_Application, this.Company, pVal.FormTypeEx, pVal.FormTypeCount);
                            //this.SetActiveView(FormUID);
                            this.view.InsertFolder();
                            this.Foco = true;
                            var empresaId = 208; //this.configs.UserFull.Dependencia.EmpresaID;
                            this.view.Entity.activo.Propietario.EmpresaID = 208; //this.configs.UserFull.Dependencia.EmpresaID;
                            if (empresaId != null)
                                this.view.AddItemsToForm();
                            else
                            {
                                throw new Exception("KananFleet Configurations not found");
                            }
                            this.view.ChangeFolder(6);
                        }
                        #endregion

                        #region Cambio de Pestaña a sección Activo KF
                        if (pVal.ItemUID == "activinfo" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                        {
                            this.view.ChangeFolder(20);
                        }
                        #endregion

                        #region RecargaCombustible
                        if (pVal.ItemUID == "btnrcomb" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                        {
                            //this.view.FreezeForm(true);
                            if (view.IsActivoKf())
                            {
                                //this.view.FreezeForm(true);
                                if (this.view.Entity.activo.CajaID != null && this.view.Entity.activo.CajaID > 0)
                                {
                                    this.recarga = null;
                                    this.recarga = new GestionCargasPresenter(this.SBO_Application, this.Company, (int)this.view.Entity.activo.CajaID, this.view.Entity.activo.NumeroEconomico, this.configs, this.view.Entity.activo.SubPropietario.SucursalID);
                                    this.ModalCarga = true;
                                    BubbleEvent = false;
                                }
                                else
                                {
                                    oHerramientas.ColocarMensaje("Seleccione un Articulo de Tipo Activo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                }
                            }
                            else
                            {
                                oHerramientas.ColocarMensaje("Seleccione un Articulo de Tipo Activo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                            }
                        }
                        #endregion RecargaCombustible

                        #region Servicio
                        if (pVal.ItemUID == "btnaddsvc" && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                        {
                            ejecuta = @group.Trim().ToUpper() == "ACTIVOS KF";
                            if (ejecuta)
                            {
                                if (this.view.Entity.activo.CajaID != null && this.view.Entity.activo.CajaID > 0)
                                {
                                    this.lstServiciosConfigPresenter = null;
                                    this.lstServiciosConfigPresenter = new ListaServicioConfigPresenter(SBO_Application, Company, this.configs);
                                    this.lstServiciosConfigPresenter.listaServicioView.EsActivo = true;
                                    this.lstServiciosConfigPresenter.listaServicioView.LoadForm();
                                    this.lstServiciosConfigPresenter.listaServicioView.oActivo = this.view.Entity.activo;
                                    this.lstServiciosConfigPresenter.listaServicioView.ShowForm();
                                    this.lstServiciosConfigPresenter.listaServicioView.ShowParametrosMantenimiento();
                                    this.lstServiciosConfigPresenter.isConfig = true;
                                    this.ModalServicio = true;
                                    BubbleEvent = false;
                                }
                                else
                                {
                                    oHerramientas.ColocarMensaje("Seleccione un Articulo de Tipo Vehiculo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                }
                            }
                            else
                            {
                                oHerramientas.ColocarMensaje("Seleccione un Articulo de Tipo Vehiculo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                            }
                        }
                        #endregion Servicio

                        #region Asignación de Llnatas
                        if (pVal.ItemUID == "btnLlantas" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                        {
                            //this.view.FreezeForm(true);
                            if (view.IsActivoKf())
                            {
                                if (this.view.Entity.activo.CajaID != null && this.view.Entity.activo.CajaID > 0)
                                {
                                    this.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepDet", @"http://test.administraflotilla.com/PaginasSAP/ConfiguracionLLantasSAP.aspx?id=" + this.view.Entity.activo.CajaID + "&tipo=2&empid=" + configs.UserFull.Dependencia.EmpresaID);
                                    //this.ReportesPresenter = new ReportePresenter(SBO_Application, Company, true, "RepDet", @"http://test.administraflotilla.com/reportesSAP/DetalleRecargaVehiculos.aspx?lang=" + this.configs.UserFull.Configs["Lang"] + "&Tmz=" + "&long=Kms&vol=Lts&vh=" + this.view.Entity.vehiculo.VehiculoID);
                                    //this.ReportesPresenter.view.LoadForm();
                                    this.ReportesPresenter.view.ShowForm();
                                    //this.ModalRep = true;
                                }
                                else
                                {
                                    oHerramientas.ColocarMensaje("Seleccione un Articulo de Tipo Activo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                }
                            }
                            else
                            {
                                oHerramientas.ColocarMensaje("Seleccione un Articulo de Tipo Activo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                            }
                        }
                        #endregion

                        #region Incidencia
                        //Presionar botón "Incidencia en Vehículo"
                        if (pVal.ItemUID == "btnInci" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                        {
                            //this.view.FreezeForm(true);
                            if (view.IsActivoKf())
                            {
                                //this.view.FreezeForm(true);
                                if (this.view.Entity.activo.CajaID != null && this.view.Entity.activo.CajaID > 0)
                                {
                                    this.incidencia = null;
                                    this.incidencia = new InciVehiculoPresenter(this.SBO_Application, this.Company, (int)this.view.Entity.activo.CajaID, this.view.Entity.activo.NumeroEconomico, this.configs, this.oGlobales);
                                    this.ModalIncidencia = true;
                                    BubbleEvent = false;
                                }
                                else
                                {
                                    SBO_Application.StatusBar.SetText("Seleccione un Articulo de Tipo Vehiculo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                }
                            }
                            else
                            {
                                SBO_Application.StatusBar.SetText("Seleccione un Articulo de Tipo Vehiculo KF", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                            }
                        }
                        #endregion Incidencia

                        #region Acciones

                        if (pVal.ItemUID == "1" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                        {
                            mode = this.view.GetFormMode(pVal.FormUID);
                            ejecuta = this.view.IsActivoKf();

                            if (ejecuta)
                            {
                                switch (mode)
                                {
                                    case BoFormMode.fm_ADD_MODE:
                                        this.view.ValidarDatos();
                                        this.view.FormToEntity();
                                        break;
                                    case BoFormMode.fm_UPDATE_MODE:
                                        this.view.ValidarDatos();
                                        this.view.FormToEntity();
                                        break;
                                }
                            }
                        }
                        #endregion
                    }
                    #endregion

                    #region After Action
                    else
                    {
                        /*if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE)
                        {
                            this.SetActiveView(FormUID);
                        }*/

                        if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
                            this.view = null;

                        #region Form Unload
                        if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
                        {
                            this.Foco = false;
                        }
                        if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_CLOSE)
                        {
                            this.Foco = false;
                        }
                        #endregion Form Unload

                        #region Acciones

                        if (pVal.ItemUID == "1" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                        {
                            switch (mode)
                            {
                                case BoFormMode.fm_ADD_MODE:
                                    if (ejecuta)
                                    {
                                        Insertar();
                                    }
                                    else
                                    {
                                        this.view.EmptyEntityToForm();
                                    }
                                    break;
                                case BoFormMode.fm_UPDATE_MODE:
                                    if (ejecuta)
                                    {
                                        Actualizar();
                                    }
                                    Buscar();
                                    break;
                                case BoFormMode.fm_FIND_MODE:
                                    Buscar();
                                    break;
                            }
                            mode = BoFormMode.fm_OK_MODE;
                            ejecuta = false;
                            BubbleEvent = false;
                        }
                        #endregion
                    }
                    #endregion
                }
                #endregion

                #region Form Lista de Articulos

                if (pVal.FormType == 10003 && this.view != null)
                {
                    if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD & !pVal.Before_Action)
                    {
                        Buscar();
                    }
                }
                #endregion

                
                //Elimina y desactiva tanto de 
                #region Form Confirmar Eliminar
                if (pVal.FormType == 0)
                {
                    if (pVal.ItemUID == "1" & pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED & pVal.Before_Action)
                    {
                        if (IsMessageBoxElimina)
                        {
                            if (this.view != null && this.view.IsActivoKf())
                            {
                                Eliminar();
                                BubbleEvent = false;
                                SBO_Application.Forms.ActiveForm.Close();
                                this.view.SetFormMode(BoFormMode.fm_FIND_MODE);
                            }
                            IsMessageBoxElimina = false;
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                BubbleEvent = false;
                KananSAP.Helper.KananSAPHerramientas.LogError("ActivoFijoPresenter.cs->SBO_Application_ItemEvent()", ex.Message);
                oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            }
        } 
        #endregion

        #region Menu Events

        private void SBO_Application_MenuEvent(ref SAPbouiCOM.MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            if (!IsLogged) return;
            try
            {
                if (SBO_Application.Forms.ActiveForm.Type == 150)
                {
                    if (pVal.BeforeAction)
                    {
                        if (pVal.MenuUID == "1283") //Menu Eliminar Item
                        {
                            IsMessageBoxElimina = pVal.BeforeAction;
                        }
                    }
                    else
                    {
                        //Flechas de navegación de items: 1290 - Primer Item, 1289 - Item Anterior, 1288 - item siguiente, 1291 - Ultimo item
                        if (pVal.MenuUID == "1290" || pVal.MenuUID == "1289" || pVal.MenuUID == "1288" ||
                            pVal.MenuUID == "1291")
                        {
                            Buscar();

                        }
                        if (pVal.MenuUID == "1282") //Menu Crear Item
                        {
                            this.view.EmptyEntityToForm();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                BubbleEvent = false;
                KananSAP.Helper.KananSAPHerramientas.LogError("ActivoFijoPresenter.cs->SBO_Application_MenuEvent()", ex.Message);
                oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
            }
        } 
        #endregion 

        #endregion

        #region Metodos

        #region Find
        public void Buscar()
        {
            try
            {
                oItem = this.view.GetItemFromForm("5");
                var edittext = (SAPbouiCOM.EditText)(oItem.Specific);
                if (this.view.Entity.SAPToActivo(edittext.String.Trim()))
                {
                    Caja caja = new Caja() { TipoActivos = new TipoActivos(), Propietario = new Kanan.Operaciones.BO2.Empresa(), SubPropietario = new Kanan.Operaciones.BO2.Sucursal() };
                    caja = this.view.Entity.UDTToActivo();
                    this.view.EntityToForm();
                    this.GetOdometro();
                    this.GetEstatus();
                    this.view.SetFormMode(BoFormMode.fm_OK_MODE);
                    oHerramientas.ColocarMensaje("Búsqueda de activo KananFleet exitosa", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
                }
                else
                {
                    this.view.EmptyEntityToForm();
                    this.view.SetFormMode(BoFormMode.fm_OK_MODE);
                }
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("ActivoFijoPresenter.cs->Buscar()", ex.Message);
                throw new Exception(ex.Message);
            }
        }
        #endregion Find

        #region GetOdometro
        public void GetOdometro()
        {
            try
            {
                int ACID = Convert.ToInt32(this.view.Entity.activo.CajaID);
                /*Se toma del UDT debido a que Activo en ActivoSAP.cs no mantiene datos de Activo, es decir, Caja.*/
                int IDActivo = Convert.ToInt32(this.view.Entity.oUDT.UserFields.Fields.Item("U_ACTIVOD").Value);
                string kilometraje = VehiculoService.GetOdometro(IDActivo);
                this.view.SetOdometro(kilometraje);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("ActivoFijoPresenter.cs->GetOdometro()", ex.Message);
                throw new Exception("Error al obtener el odómetro" + ex.Message);
            }
        }
        #endregion GetOdometro

        #region GetEstatus
        public void GetEstatus()
        {
            try
            {
                int ActivoID = Convert.ToInt32(this.view.Entity.activo.CajaID);
                string Estatus = VehiculoService.GetEstatusActivo(ActivoID);
                this.view.SetEstatus(Estatus);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("ActivoFijoPresenter.cs->GetEstatus()", ex.Message);
                oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Warning);
            }
        }
        #endregion GetEstatus

        #region Insert
        public void Insertar()
        {
            bool bContinuar = true;
            try
            {
                /*Se limpia  el en caso de haber insertardo anteriormente algún activo.*/
                this.view.Entity.activo.CajaID = null;
                if (this.view.Entity.GetTotalActivos() > this.configs.UserFull.Dependencia.EKPermitidos)
                    bContinuar = false;
                if (bContinuar)
                {
                    this.view.FormToEntity();
                    this.view.Entity.Sincronizado = 0;
                    this.view.Entity.UUID = Guid.NewGuid();
                    this.view.Entity.activo.Propietario.EmpresaID = this.configs.UserFull.Dependencia.EmpresaID;
                    this.view.Entity.activo.FechaAlta = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
                    this.view.Entity.ActivoToUDT();

                    try //Se inserta en la nube antes de insertar en SAP.
                    {
                        this.view.Entity.activo = VehiculoService.AddActivo(this.view.Entity.activo);
                    }
                    catch (Exception ex)
                    {
                        oHerramientas.ColocarMensaje("No se há sincronizado el activo con la nube. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                        return;
                    }
                    //Se inserta en SAP posterior a la inserción en la nube permitiendo omitir el Actualizar Code y Name.
                    this.view.Entity.Insert();
                }
                else oHerramientas.ColocarMensaje("Ya ha excedido el máximo de activos permitidos para su cuenta", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                this.view.EmptyEntityToForm();
            }
            catch (Exception ex)
            {
                this.view.EmptyEntityToForm();
                KananSAP.Helper.KananSAPHerramientas.LogError("ActivoFijoPresenter.cs->Insertar()", ex.Message);
                oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                return;
            }
        }
        #endregion Insert

        #region Update
        public void Actualizar()
        {
            try
            {
                this.view.FormToEntity();
                this.view.Entity.Sincronizado = 2;
                this.view.Entity.activo.Propietario.EmpresaID = this.configs.UserFull.Dependencia.EmpresaID > 0 ? this.configs.UserFull.Dependencia.EmpresaID : 0;

                this.view.Entity.activo.FechaBaja = this.view.FormFrozen();
                this.view.Entity.ActivoToUDT();
                this.view.Entity.Update();

                this.view.Entity.activo = VehiculoService.UpdateActivo(this.view.Entity.activo);

                this.view.Entity.ActivoToUDT();
                this.view.Entity.Sincronizado = 0;
                this.view.Entity.ActualizarCode(this.view.Entity.activo);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("ActivoFijoPresenter.cs->Actualizar()", ex.Message);
                throw new Exception(ex.Message);
            }
        }
        #endregion Update

        #region Delete
        public void Eliminar()
        {
            try
            {
                this.view.Entity.activo = this.VehiculoService.DeleteActivo((int)this.view.Entity.activo.CajaID);
                this.view.Entity.ItemCode = this.view.Entity.activo.CajaID.ToString();
                this.view.Entity.Deactivate();
                this.view.EmptyEntityToForm();
                this.oHerramientas.ColocarMensaje("Operación exitosa", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("ActivoFijoPresenter.cs->Eliminar()", ex.Message);
                throw new Exception(ex.Message);
            }
        }
        #endregion Delete

        #endregion Metodos
    }
}
