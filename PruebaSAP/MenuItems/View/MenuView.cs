﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//Comentario 11/01/2018
namespace KananSAP.MenuItems.View
{
    public class MenuView
    {
        private SAPbouiCOM.Application SBO_Application;
        SAPbouiCOM.Menus oMenus = null;
        SAPbouiCOM.MenuItem oMenuItem = null;

        public MenuView(SAPbouiCOM.Application application)
        {
            this.SBO_Application = application;
            this.oMenus = null;
            this.oMenuItem = null;
        }

        public void AddMenuItems()
        {
            oMenus = SBO_Application.Menus;

            SAPbouiCOM.MenuCreationParams oCreationPackage = null;
            oCreationPackage = ((SAPbouiCOM.MenuCreationParams)(SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)));
            oMenuItem = SBO_Application.Menus.Item("43520"); // moudles'

            string sPath = null;

            sPath = Application.StartupPath;


            // find the place in wich you want to add your menu item
            // in this example I chose to add my menu item under
            // SAP Business One.
            oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
            oCreationPackage.UniqueID = "KFMenu";
            oCreationPackage.String = "KananFleet";
            oCreationPackage.Enabled = true;
            oCreationPackage.Image = sPath + "\\favicon.bmp";
            
            oCreationPackage.Position = oMenuItem.SubMenus.Count + 1;

            oMenus = oMenuItem.SubMenus;

            try
            {
                //  If the manu already exists this code will fail
                oMenus.AddEx(oCreationPackage);

                #region MENU PRINCIPAL KANANFLEET
                // Get the menu collection of the newly added pop-up item
                oMenuItem = SBO_Application.Menus.Item("KFMenu");
                oMenus = oMenuItem.SubMenus;

                //1
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "KFAdmin";
                oCreationPackage.String = "Administración - Catálogos";
                oCreationPackage.Image = null;
                oMenus.AddEx(oCreationPackage);

                //2
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "KFCartaLiq";
                oCreationPackage.String = "Carta de porte / Liquidaciones";
                oCreationPackage.Image = null;
                oMenus.AddEx(oCreationPackage);

                //3
                //oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                //oCreationPackage.UniqueID = "KFInvent";
                //oCreationPackage.String = "Inventario refacciónes";
                //oCreationPackage.Image = null;
                //oMenus.AddEx(oCreationPackage);

                //4
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "KFReportes";
                oCreationPackage.String = "Reportes / Consultas";
                oCreationPackage.Image = null;
                oMenus.AddEx(oCreationPackage);

                //5
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "KFDashBoard";
                oCreationPackage.String = "Dashboard";
                oCreationPackage.Image = null;
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "KFVersion";
                oCreationPackage.String = "Versión";
                oCreationPackage.Image = null;
                oMenus.AddEx(oCreationPackage);

                #endregion


                #region ADMINISTRACION

                #region MENU ADMINISTRACION KANANFLEET
                oMenuItem = SBO_Application.Menus.Item("KFAdmin");
                oMenus = oMenuItem.SubMenus;

                //Sucursales/Empresa
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "KFScEmp";
                oCreationPackage.String = "Sucursales / Empresas";
                oCreationPackage.Image = null;
                oMenus.AddEx(oCreationPackage);

                //Vehículos
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "KFVehiculos";
                oCreationPackage.String = "Vehículo";
                oCreationPackage.Image = null;
                oMenus.AddEx(oCreationPackage);

                //Sistema
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "KFSistema";
                oCreationPackage.String = "Sistema";
                oCreationPackage.Image = null;
                oMenus.AddEx(oCreationPackage);

                #endregion

                #region Empresa - Sucursal
                oMenuItem = SBO_Application.Menus.Item("KFScEmp");
                oMenus = oMenuItem.SubMenus;

                //Admon Sucursales/Empresa
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "AdmonScEmp";
                oCreationPackage.String = "Gestión sucursal / empresa";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region Vehículo

                #region MENÚ VEHÍCULOS
                //Vehículos
                oMenuItem = SBO_Application.Menus.Item("KFVehiculos");
                oMenus = oMenuItem.SubMenus;

                //Gestion Tipo de Vehiculo
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "TVehiculos";
                oCreationPackage.String = "Gestión tipo de vehículo";
                oMenus.AddEx(oCreationPackage);

                //Gestion Disponibilidad de Vehiculo
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "DVehiculos";
                oCreationPackage.String = "Panel de disponibilidad de vehículo";
                oMenus.AddEx(oCreationPackage);

                //Gestion de causa raiz vehiculos
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "GTCausar";
                oCreationPackage.String = "Panel de sistemas - componentes";
                oMenus.AddEx(oCreationPackage);

                //Operadores
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "KFOpera";
                oCreationPackage.String = "Operadores (Conductores)";
                oCreationPackage.Image = null;
                oMenus.AddEx(oCreationPackage);

                //Llantas
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "KFLlantas";
                oCreationPackage.String = "Llantas";
                oCreationPackage.Image = null;
                oMenus.AddEx(oCreationPackage);

                //Servicios
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "KFServicio";
                oCreationPackage.String = "Servicios";
                oCreationPackage.Image = null;
                oMenus.AddEx(oCreationPackage);

                //Configuración de alertas
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "KFAsCor";
                oCreationPackage.String = "Configuración de alertas";
                oCreationPackage.Image = null;
                oMenus.AddEx(oCreationPackage);

                //Orden de servicio
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "KFSerMa";
                oCreationPackage.String = "Orden de servicio";
                oCreationPackage.Image = null;
                oMenus.AddEx(oCreationPackage);


                //Recargas de combustibles
                /*oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "KFRComb";
                oCreationPackage.String = "Recargas de combustible";
                oCreationPackage.Image = null;
                oMenus.AddEx(oCreationPackage);*/
                #endregion MENÚ VEHÍCULOS

                #region Operadores (Conductores)
                oMenuItem = SBO_Application.Menus.Item("KFOpera");
                oMenus = oMenuItem.SubMenus;

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "KFSyncSec";
                oCreationPackage.String = "Gestión de operadores (Conductores)";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region Llantas
                oMenuItem = SBO_Application.Menus.Item("KFLlantas");
                oMenus = oMenuItem.SubMenus;

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "AdmLlanta";
                oCreationPackage.String = "Gestión de llantas";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "CfgLlanta";
                oCreationPackage.String = "Parametrización desgaste llanta";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "migrador";
                oCreationPackage.String = "Importar archivo ";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region Servicios
                oMenuItem = SBO_Application.Menus.Item("KFServicio");
                oMenus = oMenuItem.SubMenus;

                //Services module
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "AddServ";
                oCreationPackage.String = "Gestión de servicios";
                oMenus.AddEx(oCreationPackage);
                //Services type module
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "AddTServ";
                oCreationPackage.String = "Gestión de tipos de servicios";
                oMenus.AddEx(oCreationPackage);
                //Service for vehicle type
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "AddServicioTVehiculo";
                oCreationPackage.String = "Agregar parámetro para tipo de vehículo";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region Asignación de Correos
                oMenuItem = SBO_Application.Menus.Item("KFAsCor");
                oMenus = oMenuItem.SubMenus;

                //Asignación de correos
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "AsignMail";
                oCreationPackage.String = "Asignación de correos";
                oMenus.AddEx(oCreationPackage);
                #endregion



                #region Servicios de Mantenimiento
                oMenuItem = SBO_Application.Menus.Item("KFSerMa");
                oMenus = oMenuItem.SubMenus;

                //Agregar orden de servicio
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "AddOrdSv";
                oCreationPackage.String = "Solicitar orden de servicio";
                oMenus.AddEx(oCreationPackage);

                //Administrar Ordenes de Servicio
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "AdmiOrdSer";
                oCreationPackage.String = "Gestión de ordenes de servicio";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region Comentado
                //Prueba módulo gestoría.
                //oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                //oCreationPackage.UniqueID = "GVehiculo";
                //oCreationPackage.String = "Módulo gestoriía";
                //oMenus.AddEx(oCreationPackage);

                /*
                //Datos Maestros de Articulo
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "150";
                oCreationPackage.String = "Datos Maestros de Artículo";
                oMenus.AddEx(oCreationPackage);
                //Datos Maestros de Activo
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "";
                oCreationPackage.String = "Datos Maestros de Activo";
                oMenus.AddEx(oCreationPackage);
                */
                #endregion Comentado

                #endregion

                #region Sistema
                oMenuItem = SBO_Application.Menus.Item("KFSistema");
                oMenus = oMenuItem.SubMenus;

                //Centro de costos
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "ConfigLev";
                oCreationPackage.String = "Centro de costos";
                oMenus.AddEx(oCreationPackage);

                //Asociar empresa
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "KFSyncAdm";
                oCreationPackage.String = "Asociar empresa";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #endregion

                #region CARTA PORTE / LIQUIDACIONES

                //Adding reports menu
                oMenuItem = SBO_Application.Menus.Item("KFCartaLiq");
                oMenus = oMenuItem.SubMenus;

                // Create s sub menu
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "Client";
                oCreationPackage.String = "Clientes";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "CajaPorte";
                oCreationPackage.String = "Carta porte";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "Liqui";
                oCreationPackage.String = "Liquidación";
                oMenus.AddEx(oCreationPackage);


                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "CajChi";
                oCreationPackage.String = "Gestión de caja chica";
                oMenus.AddEx(oCreationPackage);


                #region Clientes
                // Create s sub menu
                oMenuItem = SBO_Application.Menus.Item("Client");
                oMenus = oMenuItem.SubMenus;

                //oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                //oCreationPackage.UniqueID = "NewClient";
                //oCreationPackage.String = "Agregar cliente";
                //oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "Admclient";
                oCreationPackage.String = "Gestión de clientes";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region Carta Porte
                // Create s sub menu
                oMenuItem = SBO_Application.Menus.Item("CajaPorte");
                oMenus = oMenuItem.SubMenus;

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "NewCarta";
                oCreationPackage.String = "Agregar carta porte";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "Admcarta";
                oCreationPackage.String = "Gestión de carta porte";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "ACAPUFE";
                oCreationPackage.String = "Cargar archivo CAPUFE";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "GesIten";
                oCreationPackage.String = "Gestión de itinerarios";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region Liquidaciones
                // Create s sub menu
                oMenuItem = SBO_Application.Menus.Item("Liqui");
                oMenus = oMenuItem.SubMenus;

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "NewLiqui";
                oCreationPackage.String = "Agregar liquidación";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "Admliquit";
                oCreationPackage.String = "Gestión de liquidaciones";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region CajaChica
                oMenuItem = SBO_Application.Menus.Item("CajChi");
                oMenus = oMenuItem.SubMenus;

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "NewCajChi";
                oCreationPackage.String = "Nueva caja chica";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #endregion

                #region Inventario
                /*
                //Adding reports menu
                oMenuItem = SBO_Application.Menus.Item("KFInvent");
                oMenus = oMenuItem.SubMenus;

                // Create s sub menu
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "ICatalog";
                oCreationPackage.String = "Catálogos";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "EntSal";
                oCreationPackage.String = "Entradas y salidas";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "Trasfer";
                oCreationPackage.String = "Transferencia de artículo";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "OrdComp";
                oCreationPackage.String = "Ordenes de compra";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "IxSuc";
                oCreationPackage.String = "Inventario por sucursal";
                oMenus.AddEx(oCreationPackage);

                //Inventario por artículo.

                //Configuración alertas de inventario.


                #region Catalogos

                //Adding reports menu
                oMenuItem = SBO_Application.Menus.Item("ICatalog");
                oMenus = oMenuItem.SubMenus;

                // Create s sub menu
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "IUnidad";
                oCreationPackage.String = "Unidades";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "IMoneda";
                oCreationPackage.String = "Moneda";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "IImpues";
                oCreationPackage.String = "Impuestos";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "Icateg";
                oCreationPackage.String = "Categorías";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "ISubcat";
                oCreationPackage.String = "Sub-categorías";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "IArtic";
                oCreationPackage.String = "Artículos";
                oMenus.AddEx(oCreationPackage);

                #region Unidades
                //Adding reports menu
                oMenuItem = SBO_Application.Menus.Item("IUnidad");
                oMenus = oMenuItem.SubMenus;

                // Create s sub menu
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "Inewunid";
                oCreationPackage.String = "Agregar unidades de medida";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "Iadmunid";
                oCreationPackage.String = "Gestionar unidades de medida";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region Monedas
                //Adding reports menu
                oMenuItem = SBO_Application.Menus.Item("IMoneda");
                oMenus = oMenuItem.SubMenus;

                // Create s sub menu
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "IAgrMon";
                oCreationPackage.String = "Agregar moneda";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "IAdmMon";
                oCreationPackage.String = "Gestionar monedas";
                oMenus.AddEx(oCreationPackage);
                #endregion Monedas

                #region Impuestos
                //Adding reports menu
                oMenuItem = SBO_Application.Menus.Item("IImpues");
                oMenus = oMenuItem.SubMenus;

                // Create s sub menu
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "Inewimp";
                oCreationPackage.String = "Agregar impuesto";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "Iadmimp";
                oCreationPackage.String = "Gestionar impuestos";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region Categorias
                //Adding reports menu
                oMenuItem = SBO_Application.Menus.Item("Icateg");
                oMenus = oMenuItem.SubMenus;

                // Create s sub menu
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "Inewcat";
                oCreationPackage.String = "Agregar categoría";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "Iadmcat";
                oCreationPackage.String = "Gestionar categorías";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region Sub-Categorias
                //Adding reports menu
                oMenuItem = SBO_Application.Menus.Item("ISubcat");
                oMenus = oMenuItem.SubMenus;

                // Create s sub menu
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "Inewscat";
                oCreationPackage.String = "Agregar sub-categoría";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "Iadmscat";
                oCreationPackage.String = "Gestionar sub-categoría";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region Articulos
                //Adding reports menu
                oMenuItem = SBO_Application.Menus.Item("IArtic");
                oMenus = oMenuItem.SubMenus;

                // Create s sub menu
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "Inewart";
                oCreationPackage.String = "Agregar artículo";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "Iadmart";
                oCreationPackage.String = "Gestionar artículos";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #endregion

                #region Transferencia de artículo
                //Adding reports menu
                oMenuItem = SBO_Application.Menus.Item("Trasfer");
                oMenus = oMenuItem.SubMenus;

                // Create s sub menu
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "Inewtrasf";
                oCreationPackage.String = "Agregar transferencia";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "Iadmtrasf";
                oCreationPackage.String = "Gestionar transferencias";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region Ordenes de Compra
                //Adding reports menu
                oMenuItem = SBO_Application.Menus.Item("OrdComp");
                oMenus = oMenuItem.SubMenus;

                // Create s sub menu
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "Inewoc";
                oCreationPackage.String = "Agregar orden de compra";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "Iadmoc";
                oCreationPackage.String = "Gestionar ordenes de compra";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region Entradas y Salidas
                //Adding reports menu
                oMenuItem = SBO_Application.Menus.Item("EntSal");
                oMenus = oMenuItem.SubMenus;

                // Create s sub menu
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "Inewes";
                oCreationPackage.String = "Agregar entrada - salida";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "Iadmes";
                oCreationPackage.String = "Gestionar entradas - salidas";
                oMenus.AddEx(oCreationPackage);
                #endregion
                */
                #endregion

                #region Reportes

                #region Menu Reportes

                //Adding reports menu
                oMenuItem = SBO_Application.Menus.Item("KFReportes");
                oMenus = oMenuItem.SubMenus;

                // Create s sub menu
                //Agregado
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "KFVisAl";
                oCreationPackage.String = "Visor de alertas";
                oCreationPackage.Image = null;
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "RepOrSe";
                oCreationPackage.String = "Ordenes de servicio";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "RepVeh";
                oCreationPackage.String = "Vehiculos";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "RepComb";
                oCreationPackage.String = "Combustible";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "RepAct";
                oCreationPackage.String = "Activos";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "RepSuc";
                oCreationPackage.String = "Sucursales";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "RepOper";
                oCreationPackage.String = "Operadores";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "RepLlantas";
                oCreationPackage.String = "Llantas";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "RepProg";
                oCreationPackage.String = "Reportes programables";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "RepPiz";
                oCreationPackage.String = "Pizarra Informe";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "RepOtro";
                oCreationPackage.String = "Indicadores MTTO";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region Visor de Alertas
                oMenuItem = SBO_Application.Menus.Item("KFVisAl");
                oMenus = oMenuItem.SubMenus;

                //Visor de alertas
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "VsAlertas";
                oCreationPackage.String = "Visor de alertas activas";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region Reportes Ordenes de servicio
                oMenuItem = SBO_Application.Menus.Item("RepOrSe");
                oMenus = oMenuItem.SubMenus;

                //Reporte de inventario de sucursal
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RepOS";
                oCreationPackage.String = "Rep. orden de servicio";
                oMenus.AddEx(oCreationPackage);
                #endregion Reportes Ordenes de servicio

                #region Reportes Vehiculo
                oMenuItem = SBO_Application.Menus.Item("RepVeh");
                oMenus = oMenuItem.SubMenus;

                //LanzaPlantillaCarta
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "LanPCOH";
                oCreationPackage.String = "Carga de Odometros";
                oMenus.AddEx(oCreationPackage);



                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RepIntVeh";
                oCreationPackage.String = "Inventario vehículos";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RepDistR";
                oCreationPackage.String = "Rep. dist. recorrida";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RepCVh";
                oCreationPackage.String = "Rep. costo por vehículo";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RepInVh";
                oCreationPackage.String = "Rep. incidencias en vehículo";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RepIVh";
                oCreationPackage.String = "Rep. Inversión por Vehículo";
                oMenus.AddEx(oCreationPackage);

                #endregion

                #region Reportes Combustible
                oMenuItem = SBO_Application.Menus.Item("RepComb");
                oMenus = oMenuItem.SubMenus;

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "CargaComb";
                oCreationPackage.String = "Rep. de cargas combustible";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region Reportes Activos
                oMenuItem = SBO_Application.Menus.Item("RepAct");
                oMenus = oMenuItem.SubMenus;

                //Reporte de inventario de activos
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RepActivos";
                oCreationPackage.String = "Rep. inv. activos";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region Reportes Sucursal
                oMenuItem = SBO_Application.Menus.Item("RepSuc");
                oMenus = oMenuItem.SubMenus;

                //Reporte de inventario de sucursal
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RepSucsal";
                oCreationPackage.String = "Rep. inv. sucursal";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region Reportes Operadores
                oMenuItem = SBO_Application.Menus.Item("RepOper");
                oMenus = oMenuItem.SubMenus;

                //Reporte de inventario de vehículos asignados a operador
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RepVhOpe";
                oCreationPackage.String = "Inv. vh. asignados a operador";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region Reportes Llantas
                oMenuItem = SBO_Application.Menus.Item("RepLlantas");
                oMenus = oMenuItem.SubMenus;

                //Reporte de inventario de uso de llantas
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RepUsLlant";
                oCreationPackage.String = "Rep. inv. uso llantas";
                oMenus.AddEx(oCreationPackage);

                //Reporte de inventario de uso de llantas por vehiculo
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RepVhLlant";
                oCreationPackage.String = "Uso de llantas por vehículo";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RepInLl";
                oCreationPackage.String = "Rep. incidencias en llantas";
                oMenus.AddEx(oCreationPackage);

                //Reporte de Costo de Llantas
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RepCtLlant";
                oCreationPackage.String = "Rep. Costo de Llantas";
                oMenus.AddEx(oCreationPackage);

                //Desgaste de llantas vs kilometraje
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RepLlVsKms";
                oCreationPackage.String = "Rep. Desgaste llanta Vs Km";
                oMenus.AddEx(oCreationPackage);

                #endregion                

                #region Reportes de pizarra 
                
                oMenuItem = SBO_Application.Menus.Item("RepPiz");
                oMenus = oMenuItem.SubMenus;

                 oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RptPiz001";
                oCreationPackage.String = "Pizarra empresa actual";
                oMenus.AddEx(oCreationPackage);

                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RptPiz002";
                oCreationPackage.String = "Pizarra todas las empresas";
                oMenus.AddEx(oCreationPackage);
                
                #endregion Reportes de pizarra 

                #region Otros reportes
                oMenuItem = SBO_Application.Menus.Item("RepOtro");
                oMenus = oMenuItem.SubMenus;
                
                //Reporte de consumo mensual de refacciones
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RptO001";
                oCreationPackage.String = "Consumo mensual de refacciones";
                oMenus.AddEx(oCreationPackage);

                //Reporte Bitácora de costos de mantenimiento
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RptO002";
                oCreationPackage.String = "Bitácora de costos de mantenimiento";
                oMenus.AddEx(oCreationPackage);

                //Formato de vale para trabajador
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RptO003";
                oCreationPackage.String = "Vale para trabajador";
                oMenus.AddEx(oCreationPackage);

                //Reporte disponibilidad activo
                //oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                //oCreationPackage.UniqueID = "RptO004";
                //oCreationPackage.String = "Disponibilidad activo";
                //oMenus.AddEx(oCreationPackage);

                //Reporte de localización por unidad productiva
                //oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                //oCreationPackage.UniqueID = "RptO005";
                //oCreationPackage.String = "Tiempo Promedio Para Reparación";
                //oMenus.AddEx(oCreationPackage);

                //Reporte próximos servicios
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RptO006";
                oCreationPackage.String = "Próximos servicios";
                oMenus.AddEx(oCreationPackage);

                //Reporte de rendimiento de combustible por unidad (horas/Litro-Km/Litro)
                //oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                //oCreationPackage.UniqueID = "RptO007";
                //oCreationPackage.String = "Rendimiento de combustible";
                //oMenus.AddEx(oCreationPackage);

                ////Reporte tiempo promedio entre fallas por equipo
                //oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                //oCreationPackage.UniqueID = "RptO008";
                //oCreationPackage.String = "Tiempo Promedio en Fallas por equipo";
                //oMenus.AddEx(oCreationPackage);

                //Tiempo promedio de reparación de acuerdo a rutinas
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RptO009";
                oCreationPackage.String = "Tiempo Promedio de reparaciones de acuerdo a rutinas";
                oMenus.AddEx(oCreationPackage);

                //Reporte de uso por hora mensual de los activos
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RptO010";
                oCreationPackage.String = "Hora mensual de activos";
                oMenus.AddEx(oCreationPackage);
                #endregion Otros reportes

                #region Reportes Programables
                oMenuItem = SBO_Application.Menus.Item("RepProg");
                oMenus = oMenuItem.SubMenus;

                //Reporte de inventario de vehículos asignados a operador
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "RptProgVeh";
                oCreationPackage.String = "Vehículos";
                oMenus.AddEx(oCreationPackage);
                //Reporte de inventario de vehículos asignados a operador
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "RptProgAct";
                oCreationPackage.String = "Activos";
                oMenus.AddEx(oCreationPackage);
                //Reporte de inventario de vehículos asignados a operador
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "RptProgOp";
                oCreationPackage.String = "Operadores";
                oMenus.AddEx(oCreationPackage);
                //Reporte de inventario de vehículos asignados a operador
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "RptProgLl";
                oCreationPackage.String = "Llantas";
                oMenus.AddEx(oCreationPackage);
                //Reporte de inventario de vehículos asignados a operador
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "RptProgOS";
                oCreationPackage.String = "Ordenes de servicio";
                oMenus.AddEx(oCreationPackage);
                //Reporte de inventario de vehículos asignados a operador
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "RptProgCom";
                oCreationPackage.String = "Combustible";
                oMenus.AddEx(oCreationPackage);

                #region Vehiculos
                oMenuItem = SBO_Application.Menus.Item("RptProgVeh");
                oMenus = oMenuItem.SubMenus;

                //Reporte de inventario de uso de llantas
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RPUsoLLanta";
                oCreationPackage.String = "Uso de llantas";
                oMenus.AddEx(oCreationPackage);
                //Reporte de inventario de uso de llantas
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RPHistAl";
                oCreationPackage.String = "Historico de alertas por vehículo";
                oMenus.AddEx(oCreationPackage);
                //Reporte de inventario de uso de llantas
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RPOpAsig";
                oCreationPackage.String = "Operadores asignados";
                oMenus.AddEx(oCreationPackage);
                //Reporte de inventario de uso de llantas
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RPUActAsig";
                oCreationPackage.String = "Activos asignados";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region Activos
                oMenuItem = SBO_Application.Menus.Item("RptProgAct");
                oMenus = oMenuItem.SubMenus;

                //Reporte de inventario de uso de llantas
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RPLlAsig";
                oCreationPackage.String = "Llantas asignadas";
                oMenus.AddEx(oCreationPackage);
                //Reporte de inventario de uso de llantas
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RPVehAsig";
                oCreationPackage.String = "Vehículos asignados";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region Operadores
                oMenuItem = SBO_Application.Menus.Item("RptProgOp");
                oMenus = oMenuItem.SubMenus;

                //Reporte de inventario de uso de llantas
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RPOpVehAs";
                oCreationPackage.String = "Vehículos asignados";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region Llantas
                oMenuItem = SBO_Application.Menus.Item("RptProgLl");
                oMenus = oMenuItem.SubMenus;

                //Reporte de inventario de uso de llantas
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RPUso";
                oCreationPackage.String = "Uso de llantas";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region OrdenesServicio
                oMenuItem = SBO_Application.Menus.Item("RptProgOS");
                oMenus = oMenuItem.SubMenus;

                //Reporte de inventario de uso de llantas
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RPHist";
                oCreationPackage.String = "Historico de ordenes de servicio";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #region Combustible
                oMenuItem = SBO_Application.Menus.Item("RptProgCom");
                oMenus = oMenuItem.SubMenus;

                //Reporte de inventario de uso de llantas
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "RPHistComb";
                oCreationPackage.String = "Recarga historica de combustible";
                oMenus.AddEx(oCreationPackage);
                #endregion

                #endregion


                //Comentado
                #region Reportes Incidencias
                //oMenuItem = SBO_Application.Menus.Item("RepInci");
                //oMenus = oMenuItem.SubMenus;

                /*oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "EInci";
                oCreationPackage.String = "Rep. Incidencias";
                oMenus.AddEx(oCreationPackage);*/
                #endregion

                #endregion

                #region Dashboard
                /*Se lanza desde el menú principal*/
                #endregion Dashboard
            }
            catch //(Exception er)
            { //  Menu already exists
                //SBO_Application.MessageBox("Menu Already Exists", 1, "Ok", "", "");
            }
        }
    }
}