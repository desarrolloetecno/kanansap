﻿﻿using System;
using KananSAP.AdministrarOrdenesServicio.Presenter;
using KananSAP.AsignacionCorreos.Presenter;
using KananSAP.GestionLlantas.Presenter;
//using KananSAP.GestionDatos.MigradorDatos.Presenter;
using KananSAP.GestionLlantas.ConfiguracionLlantas.Presenter;
using KananSAP.GestionUsuarios.AdministraUsuarios;
using KananSAP.GestionUsuarios.Autentificacion;
using KananSAP.GestionVehiculos.CatalogoVehiculo.Presenter;
 /*Preuba módulo gestoría*/
//using KananSAP.GestionVehiculos.GestoriaVehiculo.Presenter;
using KananSAP.MenuItems.View;
using KananSAP.Reportes.Presenter;
using KananWS.Interface;
using SAPbouiCOM;

namespace KananSAP.MenuItems.Presenter
{
    public class MenuPresenter
    {
        private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        private SAPbouiCOM.MenuItem oItem;
        private MenuView view;
        public bool Islogged { get; set; }
        private Configurations configs;
        private string EndPoint;
        private Helper.KananSAPHerramientas oHerramientas;

        #region Cuenta - Usuarios - Operadores
        public AutentificacionPresenter autentificacionView { get; set; }
        public AdministraUsuarioPresenter AdminUserPresenter { get; set; }
        public bool isSQL;
        #endregion

        #region Carta Porte / Liquidaciones

        #region Clientes
        public Reportes.Presenter.ReportePresenter PNuevoCliente { get; set; }
        public Reportes.Presenter.ReportePresenter PAdmiClientes { get; set; }
        private GestionGlobales oGlobales = null;
        #endregion

        #region Carta Porte
        public Reportes.Presenter.ReportePresenter PNuevoCartaPorte { get; set; }
        public Reportes.Presenter.ReportePresenter PAdmiCartaPorte { get; set; }
        public Reportes.Presenter.ReportePresenter PArchCAPUFE { get; set; }
        #endregion

        #region Liquidaciones
        public Reportes.Presenter.ReportePresenter PNuevaLiquidacion { get; set; }
        public Reportes.Presenter.ReportePresenter PAdmiLiquidacion { get; set; }
        #endregion

        #region CajaChica
        public Reportes.Presenter.ReportePresenter PCajaChicaNuevo { get; set; }
        #endregion

        #endregion

        #region Inventario
        /*
        #region Catalogos

        #region Unidades de Medida
        public Reportes.Presenter.ReportePresenter INuevoUM { get; set; }
        public Reportes.Presenter.ReportePresenter IAdmiUM { get; set; }
        #endregion

        #region Moneda
        public Reportes.Presenter.ReportePresenter INuevoMon { get; set; }
        public Reportes.Presenter.ReportePresenter IAdminMon {get ; set; }
        #endregion Moneda

        #region Impuestos
        public Reportes.Presenter.ReportePresenter INuevoImp { get; set; }
        public Reportes.Presenter.ReportePresenter IAdmiImp { get; set; }
        #endregion

        #region Categorias
        public Reportes.Presenter.ReportePresenter INuevoCateg { get; set; }
        public Reportes.Presenter.ReportePresenter IAdmiCateg { get; set; }
        #endregion

        #region Sub-Categoria
        public Reportes.Presenter.ReportePresenter INuevoSCat { get; set; }
        public Reportes.Presenter.ReportePresenter IAdmiSCat { get; set; }
        #endregion

        #region Articulos
        public Reportes.Presenter.ReportePresenter INuevoArtic { get; set; }
        public Reportes.Presenter.ReportePresenter IAdmiArtic { get; set; }
        #endregion

        #endregion

        #region Inventario por sucursal
        public Reportes.Presenter.ReportePresenter IInvPorSuc { get; set; }
        #endregion

        #region Transferencias
        public Reportes.Presenter.ReportePresenter INuevaTrasfer { get; set; }
        public Reportes.Presenter.ReportePresenter IAdmiTransfer { get; set; }
        #endregion

        #region Ordenes de Compra
        public Reportes.Presenter.ReportePresenter INuevaOrdComp { get; set; }
        public Reportes.Presenter.ReportePresenter IAdmiOrdComp { get; set; }
        #endregion

        #region Entradas y  Salidas
        public Reportes.Presenter.ReportePresenter INuevaEntrSal { get; set; }
        public Reportes.Presenter.ReportePresenter IAdmiEntrSal { get; set; }
        #endregion
        */
        #endregion

        #region Reportes
        public Reportes.Presenter.ReportePresenter ReportesPresenter1 { get; set; }
        public Reportes.Presenter.ReportePresenter ReportesPresenter2 { get; set; }
        public Reportes.Presenter.ReportePresenter ReportesPresenter3 { get; set; }
        /// <summary>
        /// Reporte de inventario de sucursal
        /// </summary>
        public Reportes.Presenter.ReportePresenter ReportesPresenter4 { get; set; }
        /// <summary>
        /// Reporte de inventario de activos
        /// </summary>
        public Reportes.Presenter.ReportePresenter ReportesPresenter5 { get; set; }
        /// <summary>
        /// Reporte de ubicación de llantas
        /// </summary>
        public Reportes.Presenter.ReportePresenter ReportesPresenter6 { get; set; }
        /// <summary>
        /// Reporte de historico de vehiculos asignados a operadores
        /// </summary>
        public Reportes.Presenter.ReportePresenter ReportesPresenter7 { get; set; }
        /// <summary>
        /// Reporte de historico de vehiculos asignados a llantas
        /// </summary>
        public Reportes.Presenter.ReportePresenter ReportesPresenter8 { get; set; }
        /// <summary>
        /// Reporte de Costo de llantas
        /// </summary>
        public Reportes.Presenter.ReportePresenter ReportesPresenter9 { get; set; }
        /// Reporte de desgaste de llantas vs km
        /// </summary>
        public Reportes.Presenter.ReportePresenter ReportesPresenter10 { get; set; }
        /// <summary>
        /// Reporte Inventario de Vehículos
        /// </summary>
        //public Reportes.Presenter.ReportePresenter ReportesPresenter9 { get; set; }

        /// <summary>
        /// Reporte de historico de inventario de vehículos
        /// </summary>
        public Reportes.Presenter.ReportePresenter oReporteVehiculos { get; set; }

        /// <summary>
        /// Reporte de historico de inventario de vehículos
        /// </summary>
        public Reportes.Presenter.ReportePresenter oReporteCostoXVehiculos { get; set; }
        /// <summary>
        /// Reporte de de inversion vehículos
        /// </summary>
        public Reportes.Presenter.ReportePresenter oReporteInverXVehiculos { get; set; }
        //RepInVh oReporteIncidenciasEnVehiculos
        public Reportes.Presenter.ReportePresenter oReporteIncidenciasEnVehiculos { get; set; }
        //RepInLl
        public Reportes.Presenter.ReportePresenter oReporteIncidenciasLlantas { get; set; }

        //RepOrSe
        public Reportes.Presenter.ReportePresenter oReporteOrdenesServicio { get; set; }

        public Reportes.Presenter.ReportePresenter oReporteDisponibilidadVehiculo { get; set; }

        #region Reportes Programables

        #region Combustible
        public Reportes.Presenter.ReportePresenter RPHistoricoCombustible { get; set; }
        #endregion

        #region Vehiculos
        public Reportes.Presenter.ReportePresenter RPVehiculoHistLlantas { get; set; }
        public Reportes.Presenter.ReportePresenter RPVehiculoAlertxVeh { get; set; }
        public Reportes.Presenter.ReportePresenter RPVehiculoOperadorxVeh { get; set; }
        public Reportes.Presenter.ReportePresenter RPVehiculoActivoxVeh { get; set; }
        #endregion

        #region Caja
        public Reportes.Presenter.ReportePresenter RPCaja_VehiculosAsignados { get; set; }
        public Reportes.Presenter.ReportePresenter RPCaja_LlantasAsignadas { get; set; }
        #endregion

        #region Llanta
        public Reportes.Presenter.ReportePresenter RPLlanta_UsoDeLlantas { get; set; }
        #endregion

        #region Operador
        public Reportes.Presenter.ReportePresenter RPOperadores_VehiculosAsignados { get; set; }
        #endregion

        #region Orden
        public Reportes.Presenter.ReportePresenter RPOrdenes_Historico { get; set; }
        #endregion

        #endregion


        #region PlantillaCarga
        public Reportes.Presenter.ReportePresenter LanzaPlantillaCarta { get; set; }
        #endregion
        /// <summary>
        /// Liga a DashBoard
        /// </summary>
        public Reportes.Presenter.ReportePresenter RptDashBoard { get; set; }



        #endregion

        #region Vehiculos
        //Catalogo de vehiculos
        public CatalogoTipoVehiculoPresenter ctVehiculoPresenter { get; set; }
        public CatalogoVehiculosPresenter DatosMaestrosArticulo { get; set; }
        
        /*Prueba módulo gestoría*/
        //public GestoriaVehiculoPresenter Gestoria { get; set; }
        #endregion

        #region Llantas
        //Catalogo de Llantas
        public GestionLlantasPresenter AdmnLlantasPresenter { get; set; }
        //Configuración de llantas
        public ConfiguracionLlantasPresenter CfgLlantasPresenter { get; set; }

        //MigDat
        //public MigradorPresenter MigDat { get; set; }
        #endregion

        #region Servicios
        //Administracion de servicios
        public GestionServicios.CatalogoServicios.Presenter.CatalogoServiciosPresenter ctlogServiciosPresenter { get; set; }
        //Administracion de tipos de servicios
        public GestionServicios.CatalogoServicios.Presenter.ListaTServiciosPresenter ctlogTServiciosPresenter { get; set; }
        //Configuracion de parametros para tipo de vehiculo
        public ListaConfigTipoVehiculoPresenter cParametroTVehiculoPresenter { get; set; }
        #endregion

        #region Alertas
        //Notificaciones
        public AsignaCorreosPresenter notificacionesPresenter { get; set; }
        //Visor de alertas
        public GestionAlertas.Presenter.VisorAlertaPesenter vsAlertaPresenter { get; set; }
        #endregion

        #region OrdenesDeServicio
        //Orden de servicio sin alerta generada
        public SolicitarOrdenServicio.Presenter.OrdenServicioSinAlertaPresenter odServicioPresenter { get; set; }
        //Administracion de Ordenes de Servicio
        public AdministrarOrdenesServicio.Presenter.AdministraOSPresenter AdmiOrdenesServicioPresenter { get; set; }
        #endregion

        #region Sucursales
        //Administracion de sucursales/empresas
        public GestionOperaciones.AdminSucursal.Presenter.SucursalAdmonPresenter admonSucursalPresenter { get; set; }
        #endregion

        public MenuPresenter(SAPbouiCOM.Application sApplication, SAPbobsCOM.Company Cmpny, Configurations c, bool islogged, GestionGlobales oGlobales)//, bool isSQL)
        {
            //this.isSQL = isSQL;
            this.oGlobales = oGlobales;
            this.configs = c;
            this.Islogged = islogged;
            this.SBO_Application = sApplication;
            this.Company = Cmpny;
            this.view = new MenuView(this.SBO_Application);
            this.AdminUserPresenter = new AdministraUsuarioPresenter(this.SBO_Application, this.Company, this.configs);//, this.isSQL);
            this.EndPoint = System.Configuration.ConfigurationManager.AppSettings.Get("EndPoint");

            if (Islogged && configs != null && configs.UserFull != null && configs.UserFull.Dependencia != null && configs.UserFull.Dependencia.EmpresaID != null)
            {
                this.LanzaPlantillaCarta = new ReportePresenter(SBO_Application, Company, Islogged, "LanPCOH", @EndPoint + "PaginasSAP/VehiculoActualizaKM.aspx?empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.ReportesPresenter1 = new ReportePresenter(SBO_Application, Company, Islogged, "RepVeh", @EndPoint + "ReportesSAP/ListadoCatalogoVehiculos.aspx?emp=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.ReportesPresenter2 = new ReportePresenter(SBO_Application, Company, Islogged, "CargaComb", @EndPoint + "ReportesSAP/SeleccionarVehiculosRecarga.aspx?long=Kms&emp=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.ReportesPresenter3 = new ReportePresenter(SBO_Application, Company, Islogged, "RepDistR", @EndPoint + "ReportesSAP/DistanciaRecorridaVehiculos.aspx?long=Kms&emp=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.ReportesPresenter4 = new ReportePresenter(SBO_Application, Company, Islogged, "RepSucsal", @EndPoint + "ReportesSAP/ReporteSucursal.aspx?lang=es-MX&emp=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.ReportesPresenter5 = new ReportePresenter(SBO_Application, Company, Islogged, "RepActivos", @EndPoint + "ReportesSAP/SeleccionarCajas.aspx?empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.ReportesPresenter6 = new ReportePresenter(SBO_Application, Company, Islogged, "RepUsLlant", @EndPoint + "ReportesSAP/SeleccionarLlantasHistorica.aspx?lang=es-MX&emp=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.ReportesPresenter7 = new ReportePresenter(SBO_Application, Company, Islogged, "RepVhOpe", @EndPoint + "ReportesSAP/SeleccionarVehiculoOperadores.aspx?lang=es-MX&emp=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.ReportesPresenter8 = new ReportePresenter(SBO_Application, Company, Islogged, "RepVhLlant", @EndPoint + "ReportesSAP/SeleccionarVehiculosXLlantas.aspx?lang=es-MX&emp=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.ReportesPresenter9 = new ReportePresenter(SBO_Application, Company, Islogged, "RepCtLlant", @EndPoint + "ReportesSAP/SeleccionarLlantaCosto.aspx?lang=es-MX&emp=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.ReportesPresenter10 = new ReportePresenter(SBO_Application, Company, Islogged, "RepLlVsKms", @EndPoint + "ReportesSAP/ReporteDesgasteLlantaVsKM.aspx?lang=es-MX&emp=" + this.configs.UserFull.Dependencia.EmpresaID);
                /*this.ReportesPresenter9 = new ReportePresenter(SBO_Application, Company, Islogged, "EInci", @EndPoint + "ReportesSAP/SeleccionarVehiculosXLlantas.aspx?lang=es-MX&emp=" + this.configs.UserFull.Dependencia.EmpresaID);*/

                this.oReporteVehiculos = new ReportePresenter(SBO_Application, Company, Islogged, "RepIntVeh", @EndPoint + "ReportesSAP/ListadoCatalogoVehiculos.aspx?lang=es-MX&task=in&emp=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.oReporteCostoXVehiculos = new ReportePresenter(SBO_Application, Company, Islogged, "RepCVh", @EndPoint + "ReportesSAP/SeleccionarVehiculosCosto.aspx?lang=es-MX&task=in&empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.oReporteInverXVehiculos = new ReportePresenter(SBO_Application, Company, Islogged, "RepIVh", @EndPoint + "ReportesSAP/ReporteInversionVehiculo.aspx?lang=es-MX&task=in&empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.oReporteIncidenciasEnVehiculos = new ReportePresenter(SBO_Application, Company, Islogged, "RepInVh", @EndPoint + "ReportesSAP/SeleccionarVehiculosIncidencias.aspx?lang=es-MX&task=in&empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.oReporteIncidenciasLlantas = new ReportePresenter(SBO_Application, Company, Islogged, "RepInLl", @EndPoint + "ReportesSAP/SeleccionarLlantasIncidencias.aspx?lang=es-MX&task=in&empid=" + this.configs.UserFull.Dependencia.EmpresaID);

                this.oReporteOrdenesServicio = new ReportePresenter(SBO_Application, Company, Islogged, "RepOrSe", @EndPoint + "ReportesSAP/SeleccionarOrdenes.aspx?lang=es-MX&task=in&empid=" + this.configs.UserFull.Dependencia.EmpresaID);

                this.oReporteDisponibilidadVehiculo = new ReportePresenter(SBO_Application, Company, Islogged, "DVehiculos", @EndPoint + "PaginasSAP/CatalogoDisponibilidadVehiculo.aspx?lang=es-MX&task=in&empid=" + this.configs.UserFull.Dependencia.EmpresaID);


                #region Reportes Programables

                #region Combustible
                this.RPHistoricoCombustible = new ReportePresenter(SBO_Application, Company, Islogged, "RepVhLlant", @EndPoint + "PaginasSAP/PReporteDetalleRecargas.aspx?r=hdr&empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                #endregion

                #region Vehiculo
                this.RPVehiculoHistLlantas = new ReportePresenter(SBO_Application, Company, Islogged, string.Empty, @EndPoint + "PaginasSAP/PReporteVehiculo.aspx?r=llant&empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.RPVehiculoAlertxVeh = new ReportePresenter(SBO_Application, Company, Islogged, string.Empty, @EndPoint + "PaginasSAP/PReporteVehiculo.aspx?r=alert&empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.RPVehiculoOperadorxVeh = new ReportePresenter(SBO_Application, Company, Islogged, string.Empty, @EndPoint + "PaginasSAP/PReporteVehiculo.aspx?r=oper&empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.RPVehiculoActivoxVeh = new ReportePresenter(SBO_Application, Company, Islogged, string.Empty, @EndPoint + "PaginasSAP/PReporteVehiculo.aspx?r=caj&empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                #endregion

                #region Caja
                this.RPCaja_LlantasAsignadas = new ReportePresenter(SBO_Application, Company, Islogged, string.Empty, @EndPoint + "PaginasSAP/PReporteCaja.aspx?r=vh&empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.RPCaja_VehiculosAsignados = new ReportePresenter(SBO_Application, Company, Islogged, string.Empty, @EndPoint + "PaginasSAP/PReporteCaja.aspx?r=llant&empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                #endregion

                #region Llantas
                this.RPLlanta_UsoDeLlantas = new ReportePresenter(SBO_Application, Company, Islogged, string.Empty, @EndPoint + "PaginasSAP/PReporteLlanta.aspx?r=uso&empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                #endregion

                #region Operadores
                this.RPOperadores_VehiculosAsignados = new ReportePresenter(SBO_Application, Company, Islogged, string.Empty, @EndPoint + "PaginasSAP/PReporteOperador.aspx?r=vh&empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                #endregion

                #region Ordenes
                this.RPOrdenes_Historico = new ReportePresenter(SBO_Application, Company, Islogged, string.Empty, @EndPoint + "PaginasSAP/PReporteOrden.aspx?r=hist&empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                #endregion

                #endregion

                #region Carta Porte / Liquidaciones

                #region Cliente
                this.PNuevoCliente = new ReportePresenter(SBO_Application, Company, Islogged, "PNuevoCliente", @EndPoint + "PaginasSAP/Clientes/ClienteNuevo.aspx?empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.PAdmiClientes = new ReportePresenter(SBO_Application, Company, Islogged, "PAdmiClientes", @EndPoint + "PaginasSAP/Clientes/CatalogoClientes.aspx?empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                #endregion

                #region Carta Porte
                this.PNuevoCartaPorte = new ReportePresenter(SBO_Application, Company, Islogged, "PNuevoCartaPorte", @EndPoint + "PaginasSAP/CartaPorte/CartaPorteNuevo.aspx?empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.PAdmiCartaPorte = new ReportePresenter(SBO_Application, Company, Islogged, "PAdmiCartaPorte", @EndPoint + "PaginasSAP/CartaPorte/CatalogoCartaPorte.aspx?r=hdr&empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.PArchCAPUFE = new ReportePresenter(SBO_Application, Company, Islogged, "PArchCAPUFE", @EndPoint + "PaginasSAP/CartaPorte/ArchivoIave.aspx?empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                #endregion

                #region Liquidaciones
                this.PNuevaLiquidacion = new ReportePresenter(SBO_Application, Company, Islogged, "PNuevaLiquidacion", @EndPoint + "PaginasSAP/Liquidaciones/LiquidacionNuevo.aspx?r=hdr&empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.PAdmiLiquidacion = new ReportePresenter(SBO_Application, Company, Islogged, "PAdmiLiquidacion", @EndPoint + "PaginasSAP/Liquidaciones/CatalogoLiquidacion.aspx?r=hdr&empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                #endregion

                #region Caja Chica
                this.PCajaChicaNuevo = new ReportePresenter(SBO_Application, Company, Islogged, "PCajaChicaNuevo", @EndPoint + "PaginasSAP/CajaChica/CajaChicaNuevo.aspx?r=hdr&empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                #endregion

                #endregion

                #region Inventario
                /*
                #region Catalogos

                #region Unidades de Medida
                this.INuevoUM = new ReportePresenter(SBO_Application, Company, Islogged, "INuevoUM", @EndPoint + "PaginasSAP/Inventarios/UnidadNuevo.aspx?empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.IAdmiUM = new ReportePresenter(SBO_Application, Company, Islogged, "IAdmiUM", @EndPoint + "PaginasSAP/Inventarios/UnidadAdministracion.aspx?empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                #endregion

                #region Moneda
                this.INuevoMon = new ReportePresenter(SBO_Application, Company, islogged, "IAgrMon", @EndPoint + "PaginasSAP/Inventarios/InventarioMonedaNuevo.aspx?empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.IAdminMon = new ReportePresenter(SBO_Application, Company, islogged, "IAdmMon", @EndPoint + "PaginasSAP/Inventarios/InventarioMonedaAdministracion.aspx?empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                #endregion Moneda

                #region Impuestos
                this.INuevoImp = new ReportePresenter(SBO_Application, Company, Islogged, "INuevoImp", @EndPoint + "PaginasSAP/Inventarios/ImpuestoNuevo.aspx?empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.IAdmiImp = new ReportePresenter(SBO_Application, Company, Islogged, "IAdmiImp", @EndPoint + "PaginasSAP/Inventarios/ImpuestoAdministracion.aspx?empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                #endregion

                #region Categorias
                this.INuevoCateg = new ReportePresenter(SBO_Application, Company, Islogged, "INuevoCateg", @EndPoint + "PaginasSAP/Inventarios/CategoriaNuevo.aspx?empid=" + this.configs.UserFull.Dependencia.EmpresaID + "&_modal=true");
                this.IAdmiCateg = new ReportePresenter(SBO_Application, Company, Islogged, "IAdmiCateg", @EndPoint + "PaginasSAP/Inventarios/CategoriaAdministracion.aspx?empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                #endregion

                #region Sub-Categoria
                this.INuevoSCat = new ReportePresenter(SBO_Application, Company, Islogged, "INuevoSCat", @EndPoint + "PaginasSAP/Inventarios/SubCategoriaNuevo.aspx?empid=" + this.configs.UserFull.Dependencia.EmpresaID + "&_modal=true");
                this.IAdmiSCat = new ReportePresenter(SBO_Application, Company, Islogged, "IAdmiSCat", @EndPoint + "PaginasSAP/Inventarios/SubCategoriaAdministracion.aspx?empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                #endregion

                #region Articulos
                this.INuevoArtic = new ReportePresenter(SBO_Application, Company, Islogged, "INuevoArtic", @EndPoint + "PaginasSAP/Inventarios/ArticuloNuevo.aspx?empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.IAdmiArtic = new ReportePresenter(SBO_Application, Company, Islogged, "IAdmiArtic", @EndPoint + "PaginasSAP/Inventarios/ArticuloAdministracion.aspx?empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                #endregion

                #endregion

                #region Inventario por sucursal
                this.IInvPorSuc = new ReportePresenter(SBO_Application, Company, Islogged, "IInvPorSuc", @EndPoint + "PaginasSAP/Inventarios/InventarioPorSucursal.aspx?empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                #endregion

                #region Transferencias
                this.INuevaTrasfer = new ReportePresenter(SBO_Application, Company, Islogged, "INuevaTrasfer", @EndPoint + "PaginasSAP/Inventarios/TransferenciaNuevo.aspx?usrid=" + this.configs.UserFull.EmpleadoID + "&empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.IAdmiTransfer = new ReportePresenter(SBO_Application, Company, Islogged, "IAdmiTransfer", @EndPoint + "PaginasSAP/Inventarios/TransferenciaAdministracion.aspx?usrid=" + this.configs.UserFull.EmpleadoID + "&empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                #endregion

                #region Ordenes de Compra
                this.INuevaOrdComp = new ReportePresenter(SBO_Application, Company, Islogged, "INuevaOrdComp", @EndPoint + "PaginasSAP/Inventarios/OrdenDeCompraNuevo.aspx?usrid=" + this.configs.UserFull.EmpleadoID + "&empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.IAdmiOrdComp = new ReportePresenter(SBO_Application, Company, Islogged, "IAdmiOrdComp", @EndPoint + "PaginasSAP/Inventarios/OrdenDeCompraAdministracion.aspx?usrid=" + this.configs.UserFull.EmpleadoID + "&empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                #endregion

                #region Entradas y  Salidas
                this.INuevaEntrSal = new ReportePresenter(SBO_Application, Company, Islogged, "INuevaEntrSal", @EndPoint + "PaginasSAP/Inventarios/EntradaSalidaNuevo.aspx?usrid=" + this.configs.UserFull.EmpleadoID + "&empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                this.IAdmiEntrSal = new ReportePresenter(SBO_Application, Company, Islogged, "IAdmiEntrSal", @EndPoint + "PaginasSAP/Inventarios/EntradaSalidaAdministracion.aspx?usrid=" + this.configs.UserFull.EmpleadoID + "&empid=" + this.configs.UserFull.Dependencia.EmpresaID);
                #endregion

                */
                #endregion

                #region DashBoard
                this.RptDashBoard = new ReportePresenter(SBO_Application, Company, Islogged, "RptDashBoard", @"http://dashboard.administraflotilla.com/dashboard/" + this.configs.UserFull.Dependencia.EmpresaID);
                #endregion DashBoard
            }
            if (Islogged && configs != null && configs.UserFull != null)
            {
                this.ctlogServiciosPresenter = new GestionServicios.CatalogoServicios.Presenter.CatalogoServiciosPresenter(this.SBO_Application, this.Company, this.configs);
                this.ctlogTServiciosPresenter = new GestionServicios.CatalogoServicios.Presenter.ListaTServiciosPresenter(this.SBO_Application, this.Company, this.configs);
                this.ctVehiculoPresenter = new CatalogoTipoVehiculoPresenter(this.SBO_Application, this.Company, this.configs);
                this.DatosMaestrosArticulo = new CatalogoVehiculosPresenter(this.oGlobales , this.SBO_Application, this.Company, this.configs);
                
                /*Prueba módulo gestoría*/
                //this.Gestoria = new GestoriaVehiculoPresenter(this.SBO_Application, this.Company, this.configs, new Kanan.Vehiculos.BO2.Vehiculo());

                this.cParametroTVehiculoPresenter = new ListaConfigTipoVehiculoPresenter(this.SBO_Application, this.Company, this.configs);
                this.admonSucursalPresenter = new GestionOperaciones.AdminSucursal.Presenter.SucursalAdmonPresenter(this.SBO_Application, this.Company, this.configs);
                this.AdmnLlantasPresenter = new GestionLlantasPresenter(this.SBO_Application, this.Company, this.configs);
                this.CfgLlantasPresenter = new ConfiguracionLlantasPresenter(this.SBO_Application, this.Company, this.configs);
                //this.MigDat = new MigradorPresenter(this.SBO_Application, this.Company, this.configs);

                #region Alertas
                this.vsAlertaPresenter = new GestionAlertas.Presenter.VisorAlertaPesenter(this.SBO_Application, this.Company, this.configs, this.Islogged, this.oGlobales);
                this.notificacionesPresenter = new AsignaCorreosPresenter(this.SBO_Application, this.Company, this.configs);
                #endregion

                #region Ordenes Servicio
                this.odServicioPresenter = new SolicitarOrdenServicio.Presenter.OrdenServicioSinAlertaPresenter(this.SBO_Application, this.Company, this.configs);
                //this.AdmiOrdenesServicioPresenter = new AdministraOSPresenter(this.SBO_Application, this.Company, this.configs);
                #endregion
            }
            //this.ReportesPresenter4 = new ReportePresenter(SBO_Application, Company, Islogged, "CargaComb", @"http://test.administraflotilla.com/ReportesSAP/SeleccionarVehiculosRecarga.aspx");
            //this.ReportesPresenter5 = new ReportePresenter(SBO_Application, Company, Islogged, "CargaComb", @"http://test.administraflotilla.com/ReportesSAP/SeleccionarVehiculosRecarga.aspx");
            this.view.AddMenuItems();
            // events handled by SBO_Application_MenuEvent

            #region Comentado. La aplicación principal maneja los eventos
            //SBO_Application.MenuEvent += new SAPbouiCOM._IApplicationEvents_MenuEventEventHandler(SBO_Application_MenuEvent);
            #endregion Comentado. La aplicación principal maneja los eventos

            this.oHerramientas = new Helper.KananSAPHerramientas(this.SBO_Application);
            this.isSQL = Company.DbServerType.ToString().Contains("MSSQL") ? true : false;
        }

        public void SBO_Application_MenuEvent(ref SAPbouiCOM.MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                /*if (this.configs.UserFull == null)
                {
                    KananSAP.Helper.KananSAPHerramientas.LogError("MenuPresenter.cs->SBO_Application_MenuEvent()","No se obtuvieron valores de inicio de sesión. Es posible que la conexión a Internet sea mala o que no sea una instalación oficial del addonkananfleet.");
                    oHerramientas.ColocarMensaje("No se obtuvieron valores de inicio de sesión, valide su conexión de Internet. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                    return;
                }*/

                #region Administracion

                #region Cuentas

                if (pVal.MenuUID == "KFSyncAdm" && pVal.BeforeAction)
                {
                    this.autentificacionView.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                if (pVal.MenuUID == "KFSyncSec" && pVal.BeforeAction && Islogged)
                {
                    this.AdminUserPresenter.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }

                #endregion

                #region Vehiculos

                if (pVal.MenuUID == "TVehiculos" && pVal.BeforeAction && Islogged)
                {
                    this.ctVehiculoPresenter.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }

                if (pVal.MenuUID == "DVehiculos" && pVal.BeforeAction && Islogged)
                {
                    this.oReporteDisponibilidadVehiculo.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }

                /*Prueba módulo gestoría*/
                //if (pVal.MenuUID == "GVehiculo" && pVal.BeforeAction && Islogged)
                //{
                //    this.Gestoria.view.ShowForm();
                //    BubbleEvent = false;
                //    return;
                //}

                /*
                if (pVal.MenuUID == "DaMaAr" && pVal.BeforeAction && Islogged)
                {
                    SAPbouiCOM.ItemEvent paVal = new ItemEvent();
                    DatosMaestrosArticulo.SBO_Application_ItemEvent("150", paVal, BubbleEvent);
                    BubbleEvent = false;
                    return;
                }
                */

                #endregion

                #region LLantas
                if (pVal.MenuUID == "AdmLlanta" && pVal.BeforeAction && Islogged)
                {
                    this.AdmnLlantasPresenter.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }

                if (pVal.MenuUID == "CfgLlanta" && pVal.BeforeAction && Islogged)
                {
                    this.CfgLlantasPresenter.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }

                //if (pVal.MenuUID == "MigDat" && pVal.BeforeAction && Islogged)
                //{
                //    this.MigDat.View.ShowForm();
                //    BubbleEvent = false;
                //    return;
                //}

                #endregion

                #region Servicios
                if (pVal.MenuUID == "AddServ" && pVal.BeforeAction && Islogged)
                {
                    this.ctlogServiciosPresenter.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                if (pVal.MenuUID == "AddTServ" && pVal.BeforeAction && Islogged)
                {
                    this.ctlogTServiciosPresenter.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                if (pVal.MenuUID == "AddServicioTVehiculo" && pVal.BeforeAction && Islogged)
                {
                    this.cParametroTVehiculoPresenter.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }

                #endregion

                #region Alertas
                if (pVal.MenuUID == "VsAlertas" && pVal.BeforeAction && Islogged)
                {
                    this.vsAlertaPresenter.view.ShowForm();
                    this.vsAlertaPresenter.view.SetDataSource();
                    //BubbleEvent = false;
                    //return;
                }

                if (pVal.MenuUID == "AsignMail" && pVal.BeforeAction && Islogged)
                {
                    this.notificacionesPresenter.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion

                #region Ordenes de Servicio
                /*if (pVal.MenuUID == "AddOrdSv" && pVal.BeforeAction && Islogged)
                {
                    this.odServicioPresenter.view.ShowForm(1, false);
                    this.odServicioPresenter.viewlist.OSRowIndex = -1;
                }
                if (pVal.MenuUID == "AdmiOrdSer" && pVal.BeforeAction && Islogged)
                {
                    this.odServicioPresenter.viewlist.ShowForm();
                    this.odServicioPresenter.viewlist.OSRowIndex = null;
                }*/
                #endregion

                #region Sucursales
                if (pVal.MenuUID == "AdmonScEmp" && pVal.BeforeAction && Islogged)
                    this.admonSucursalPresenter.view.ShowForm();
                #endregion

                #endregion

                #region Carta Porte / Liquidaciones

                #region Clientes
                if (pVal.MenuUID == "NewClient" && pVal.BeforeAction && Islogged)
                {
                    this.PNuevoCliente.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                if (pVal.MenuUID == "Admclient" && pVal.BeforeAction && Islogged)
                {
                    this.PAdmiClientes.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion

                #region Carta Porte
                if (pVal.MenuUID == "NewCarta" && pVal.BeforeAction && Islogged)
                {
                    this.PNuevoCartaPorte.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                if (pVal.MenuUID == "Admcarta" && pVal.BeforeAction && Islogged)
                {
                    this.PAdmiCartaPorte.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion

                #region Liquidaciones
                if (pVal.MenuUID == "NewLiqui" && pVal.BeforeAction && Islogged)
                {
                    this.PNuevaLiquidacion.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                if (pVal.MenuUID == "Admliquit" && pVal.BeforeAction && Islogged)
                {
                    this.PAdmiLiquidacion.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                if (pVal.MenuUID == "ACAPUFE" && pVal.BeforeAction && Islogged)
                {
                    this.PArchCAPUFE.view.ShowForm();
                    BubbleEvent = false;
                    return;
                } 
                #endregion

                #region CajaChica
                if (pVal.MenuUID == "NewCajChi" && pVal.BeforeAction && Islogged)
                {
                    this.PCajaChicaNuevo.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }

                #endregion

                #endregion

                #region Inventario
                /*
                #region Catalogos

                #region Unidades de Medida
                if (pVal.MenuUID == "Inewunid" && pVal.BeforeAction && Islogged)
                {
                    this.INuevoUM.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                if (pVal.MenuUID == "Iadmunid" && pVal.BeforeAction && Islogged)
                {
                    this.IAdmiUM.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion

                #region Monedas
                if (pVal.MenuUID == "IAgrMon" && pVal.BeforeAction && Islogged)
                {
                    this.INuevoMon.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                if (pVal.MenuUID == "IAdmMon" && pVal.BeforeAction && Islogged)
                {
                    this.IAdminMon.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion Monedas

                #region Impuestos
                if (pVal.MenuUID == "Inewimp" && pVal.BeforeAction && Islogged)
                {
                    this.INuevoImp.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                if (pVal.MenuUID == "Iadmimp" && pVal.BeforeAction && Islogged)
                {
                    this.IAdmiImp.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion

                #region Categorias
                if (pVal.MenuUID == "Inewcat" && pVal.BeforeAction && Islogged)
                {
                    this.INuevoCateg.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                if (pVal.MenuUID == "Iadmcat" && pVal.BeforeAction && Islogged)
                {
                    this.IAdmiCateg.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion

                #region Sub-Categoria
                if (pVal.MenuUID == "Inewscat" && pVal.BeforeAction && Islogged)
                {
                    this.INuevoSCat.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                if (pVal.MenuUID == "Iadmscat" && pVal.BeforeAction && Islogged)
                {
                    this.IAdmiSCat.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion

                #region Articulos
                if (pVal.MenuUID == "Inewart" && pVal.BeforeAction && Islogged)
                {
                    this.INuevoArtic.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                if (pVal.MenuUID == "Iadmart" && pVal.BeforeAction && Islogged)
                {
                    this.IAdmiArtic.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion

                #endregion

                #region Inventario por sucursal
                if (pVal.MenuUID == "IxSuc" && pVal.BeforeAction && Islogged)
                {
                    this.IInvPorSuc.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion

                #region Transferencias
                if (pVal.MenuUID == "Inewtrasf" && pVal.BeforeAction && Islogged)
                {
                    this.INuevaTrasfer.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                if (pVal.MenuUID == "Iadmtrasf" && pVal.BeforeAction && Islogged)
                {
                    this.IAdmiTransfer.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion

                #region Ordenes de Compra
                if (pVal.MenuUID == "Inewoc" && pVal.BeforeAction && Islogged)
                {
                    this.INuevaOrdComp.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                if (pVal.MenuUID == "Iadmoc" && pVal.BeforeAction && Islogged)
                {
                    this.IAdmiOrdComp.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion

                #region Entradas y  Salidas
                if (pVal.MenuUID == "Inewes" && pVal.BeforeAction && Islogged)
                {
                    this.INuevaEntrSal.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                if (pVal.MenuUID == "Iadmes" && pVal.BeforeAction && Islogged)
                {
                    this.IAdmiEntrSal.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion
                */
                #endregion

                #region Reportes
                if (pVal.MenuUID == "LanPCOH" && pVal.BeforeAction && Islogged)
                {
                    this.LanzaPlantillaCarta.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #region Vehiculos
                if (pVal.MenuUID == "RepVeh" && pVal.BeforeAction && Islogged)
                {
                    this.ReportesPresenter1.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                if (pVal.MenuUID == "RepDistR" && pVal.BeforeAction && Islogged)
                {
                    this.ReportesPresenter3.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }

                if (pVal.MenuUID == "RepOrSe" && pVal.BeforeAction && Islogged)
                {
                    this.oReporteOrdenesServicio.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }

                #endregion

                #region Combustible
                if (pVal.MenuUID == "CargaComb" && pVal.BeforeAction && Islogged)
                {
                    this.ReportesPresenter2.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion

                #region DetalleIncidencia
                /*if (pVal.MenuUID == "EInci" && pVal.BeforeAction && Islogged)
                {
                    this.ReportesPresenter9.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }*/
                #endregion

                #region Activos
                //Reporte de inventario de activos
                if (pVal.MenuUID == "RepActivos" && pVal.BeforeAction && Islogged)
                {
                    this.ReportesPresenter5.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion

                #region Sucursales
                //Reporte de inventario de sucursales
                if (pVal.MenuUID == "RepSucsal" && pVal.BeforeAction && Islogged)
                {
                    this.ReportesPresenter4.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion

                #region OrdenServicio
                //Reporte de ordenes de servicio
                if (pVal.MenuUID == "RepOS" && pVal.BeforeAction && Islogged)
                {
                    this.oReporteOrdenesServicio.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion OrdenServicio

                #region Llantas
                //Reporte de uso de llantas
                if (pVal.MenuUID == "RepUsLlant" && pVal.BeforeAction && Islogged)
                {
                    this.ReportesPresenter6.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }

                //Reporte de historial de uso de llantas por vehiculos
                if (pVal.MenuUID == "RepVhLlant" && pVal.BeforeAction && Islogged)
                {
                    this.ReportesPresenter8.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }

                //Reporte de costo llanta
                if (pVal.MenuUID == "RepCtLlant" && pVal.BeforeAction && Islogged)
                {
                    this.ReportesPresenter9.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }

                //Reporte de desgaste llanta vs km
                if (pVal.MenuUID == "RepLlVsKms" && pVal.BeforeAction && Islogged)
                {
                    this.ReportesPresenter10.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }

                //Reporte de inventario de vehículos
                if (pVal.MenuUID == "RepIntVeh" && pVal.BeforeAction && Islogged)
                {
                    this.oReporteVehiculos.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }

                if (pVal.MenuUID == "RepCVh")
                {
                    this.oReporteCostoXVehiculos.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }

                if (pVal.MenuUID == "RepIVh")
                {
                    this.oReporteInverXVehiculos.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }

                if (pVal.MenuUID == "RepInVh")
                {
                    this.oReporteIncidenciasEnVehiculos.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }

                if (pVal.MenuUID == "RepInLl")
                {
                    this.oReporteIncidenciasLlantas.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion

                #region Operadores
                //Reporte de historial de vehiculos asignados a operadores
                if (pVal.MenuUID == "RepVhOpe" && pVal.BeforeAction && Islogged)
                {
                    this.ReportesPresenter7.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion

                #region Reportes Programables

                #region Vehiculo
                //Reporte de historial de vehiculos asignados a operadores
                if (pVal.MenuUID == "RPUsoLLanta" && pVal.BeforeAction && Islogged)
                {
                    this.RPVehiculoHistLlantas.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                //Reporte de historial de vehiculos asignados a operadores
                if (pVal.MenuUID == "RPHistAl" && pVal.BeforeAction && Islogged)
                {
                    this.RPVehiculoAlertxVeh.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                //Reporte de historial de vehiculos asignados a operadores
                if (pVal.MenuUID == "RPOpAsig" && pVal.BeforeAction && Islogged)
                {
                    this.RPVehiculoOperadorxVeh.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                //Reporte de historial de vehiculos asignados a operadores
                if (pVal.MenuUID == "RPUActAsig" && pVal.BeforeAction && Islogged)
                {
                    this.RPVehiculoActivoxVeh.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion

                #region Combustible
                //Reporte de historial de vehiculos asignados a operadores
                if (pVal.MenuUID == "RPHistComb" && pVal.BeforeAction && Islogged)
                {
                    this.RPHistoricoCombustible.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion

                #region Cajas
                //Reporte Programable de llantas asignadas a activos
                if (pVal.MenuUID == "RPLlAsig" && pVal.BeforeAction && Islogged)
                {
                    this.RPCaja_LlantasAsignadas.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                //Reporte Programable de vehiculos asignados a activos
                if (pVal.MenuUID == "RPVehAsig" && pVal.BeforeAction && Islogged)
                {
                    this.RPCaja_VehiculosAsignados.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion

                #region Llantas
                //Reporte Programable de uso de llantas
                if (pVal.MenuUID == "RPUso" && pVal.BeforeAction && Islogged)
                {
                    this.RPLlanta_UsoDeLlantas.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion

                #region Operadores
                //Reporte Programable de vehiculos asignados a operadores
                if (pVal.MenuUID == "RPOpVehAs" && pVal.BeforeAction && Islogged)
                {
                    this.RPOperadores_VehiculosAsignados.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion

                #region Ordenes
                //Reporte de historial de ordenes de serivicio
                if (pVal.MenuUID == "RPHist" && pVal.BeforeAction && Islogged)
                {
                    this.RPOrdenes_Historico.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion

                #endregion

                #endregion

                #region DashBoard
                //Reporte de historial de vehiculos asignados a operadores
                if (pVal.MenuUID == "KFDashBoard" && pVal.BeforeAction && Islogged)
                {
                    this.RptDashBoard.view.ShowForm();
                    BubbleEvent = false;
                    return;
                }
                #endregion
            }
            catch (Exception ex)
            {
                KananSAP.Helper.KananSAPHerramientas.LogError("MenuPresenter.cs->SBO_Application_MenuEvent()", ex.Message);
                oHerramientas.ColocarMensaje(ex.Message, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            }
        }

    }
}