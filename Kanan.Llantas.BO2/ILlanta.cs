﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System;

namespace Kanan.Llantas.BO2
{
    [Serializable]
    [DataContract]
    public abstract class ILlanta
    {
        [DataMember]
        public int? ID { get; set; }
        [DataMember]
        public abstract int? ILlantaID { get; set; }
        [DataMember]
        public double? KilometrosRecorridos { get; set; }
        [DataMember]
        public string NumeroEconomico { get; set; }
        [DataMember]
        public List<Refaccion> Refacciones { get; set; }
        [DataMember]
        public Llanta ubicacion1 { get; set; }
        [DataMember]
        public Llanta ubicacion2 { get; set; }
        [DataMember]
        public Llanta ubicacion3 { get; set; }
        [DataMember]
        public Llanta ubicacion4 { get; set; }
        [DataMember]
        public Llanta ubicacion5 { get; set; }
        [DataMember]
        public Llanta ubicacion6 { get; set; }
        [DataMember]
        public Llanta ubicacion7 { get; set; }
        [DataMember]
        public Llanta ubicacion8 { get; set; }
        [DataMember]
        public Llanta ubicacion9 { get; set; }
        [DataMember]
        public Llanta ubicacion10 { get; set; }
        [DataMember]
        public Llanta ubicacion11 { get; set; }
        [DataMember]
        public Llanta ubicacion12 { get; set; }
        [DataMember]
        public Llanta ubicacion13 { get; set; }
        [DataMember]
        public Llanta ubicacion14 { get; set; }
        [DataMember]
        public Llanta ubicacion15 { get; set; }
        [DataMember]
        public Llanta ubicacion16 { get; set; }
        [DataMember]
        public Llanta ubicacion17 { get; set; }
        [DataMember]
        public Llanta ubicacion18 { get; set; }

        /// <summary>
        /// Obtiene una lista de Llantas Instaladas que representan las llantas instaladas con su respectiva ubicación
        /// </summary>
        /// <returns></returns>
        public List<LlantaInstalada> getLlantasInstaladas() 
        {
            List<LlantaInstalada> llantas = new List<LlantaInstalada>();
            if (this.ubicacion1 != null && this.ubicacion1.LlantaID != null)
            {

                llantas.Add(new LlantaInstalada() { Posicion = 1, Rueda = this.ubicacion1 });
            }
            if (this.ubicacion2 != null && this.ubicacion2.LlantaID != null)
            {
                llantas.Add(new LlantaInstalada() { Posicion = 2, Rueda = this.ubicacion2 });
            }

            if (this.ubicacion3 != null && this.ubicacion3.LlantaID != null)
            {
                llantas.Add(new LlantaInstalada() { Posicion = 3, Rueda = this.ubicacion3 });
            }

            if (this.ubicacion4 != null && this.ubicacion4.LlantaID != null)
            {
                llantas.Add(new LlantaInstalada() { Posicion = 4, Rueda = this.ubicacion4 });
            }

            if (this.ubicacion5 != null && this.ubicacion5.LlantaID != null)
            {
                llantas.Add(new LlantaInstalada() { Posicion = 5, Rueda = this.ubicacion5 });
            }

            if (this.ubicacion6 != null && this.ubicacion6.LlantaID != null)
            {
                llantas.Add(new LlantaInstalada() { Posicion = 6, Rueda = this.ubicacion6 });
            }

            if (this.ubicacion7 != null && this.ubicacion7.LlantaID != null)
            {
                llantas.Add(new LlantaInstalada() { Posicion = 7, Rueda = this.ubicacion7 });
            }

            if (this.ubicacion8 != null && this.ubicacion8.LlantaID != null)
            {
                llantas.Add(new LlantaInstalada() { Posicion = 8, Rueda = this.ubicacion8 });
            }

            if (this.ubicacion9 != null && this.ubicacion9.LlantaID != null)
            {
                llantas.Add(new LlantaInstalada() { Posicion = 9, Rueda = this.ubicacion9 });
            }

            if (this.ubicacion10 != null && this.ubicacion10.LlantaID != null)
            {
                llantas.Add(new LlantaInstalada() { Posicion = 10, Rueda = this.ubicacion10 });
            }

            if (this.ubicacion11 != null && this.ubicacion11.LlantaID != null)
            {
                llantas.Add(new LlantaInstalada() { Posicion = 11, Rueda = this.ubicacion11 });
            }

            if (this.ubicacion12 != null && this.ubicacion12.LlantaID != null)
            {
                llantas.Add(new LlantaInstalada() { Posicion = 12, Rueda = this.ubicacion12 });
            }

            if (this.ubicacion13 != null && this.ubicacion13.LlantaID != null)
            {
                llantas.Add(new LlantaInstalada() { Posicion = 13, Rueda = this.ubicacion13 });
            }

            if (this.ubicacion14 != null && this.ubicacion14.LlantaID != null)
            {
                llantas.Add(new LlantaInstalada() { Posicion = 14, Rueda = this.ubicacion14 });
            }

            if (this.ubicacion15 != null && this.ubicacion15.LlantaID != null)
            {
                llantas.Add(new LlantaInstalada() { Posicion = 15, Rueda = this.ubicacion15 });
            }

            if (this.ubicacion16 != null && this.ubicacion16.LlantaID != null)
            {
                llantas.Add(new LlantaInstalada() { Posicion = 16, Rueda = this.ubicacion16 });
            }

            if (this.ubicacion17 != null && this.ubicacion17.LlantaID != null)
            {
                llantas.Add(new LlantaInstalada() { Posicion = 17, Rueda = this.ubicacion17 });
            }
            if (this.ubicacion18 != null && this.ubicacion18.LlantaID != null)
            {
                llantas.Add(new LlantaInstalada() { Posicion = 18, Rueda = this.ubicacion18 });
            }
            return llantas;
        }
    }
}
