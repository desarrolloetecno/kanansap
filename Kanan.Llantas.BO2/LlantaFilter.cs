﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kanan.Llantas.BO2
{
    public class LlantaFilter:Llanta
    {
        /// <summary>
        /// Es la fecha final de la fecha de compra
        /// </summary>
        public DateTime? FechaCompraFn { get; set; }
        /// <summary>
        /// Es la fecha final normal
        /// </summary>
        public DateTime? FechaFin { get; set; }
    }
}
