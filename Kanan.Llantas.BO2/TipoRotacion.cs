﻿using System.Xml.Serialization;
using System;
namespace Kanan.Llantas.BO2
{
    [Serializable]
    public enum TipoRotacion:int
    {
        [XmlEnum("1")]
        TODA_POSICION=1,
        [XmlEnum("2")]
        TRACCION=2,
        [XmlEnum("3")]
        REMOLQUE=3
    }
}
