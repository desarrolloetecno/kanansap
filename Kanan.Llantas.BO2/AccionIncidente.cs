﻿
using System;
namespace Kanan.Llantas.BO2
{
    [Serializable]
    public class AccionIncidente
    {
        /// <summary>
        /// Indica una instalación que puede ser (Caja,Vehículo,Llanta).
        /// </summary>
        public static readonly string INSTALACION = "INSTALACION";
        /// <summary>
        /// Indica una desinstalación que puede ser (Caja,Vehículo,Llanta).
        /// </summary>
        public static readonly string DEINSTALACION = "DESINSTALACION";
        public static readonly string REFACCION = "0";
        /// <summary>
        /// Indica que (Caja,Vehículo,Llanta) ha sido dado de baja.
        /// </summary>
        public static readonly string BAJA = "BAJA";
    }
}
