﻿
using System;
namespace Kanan.Llantas.BO2
{
    [Serializable]
    // <summary>
    /// Clase que se utiliza par obtener una llanta con su respectiva ubicación que ha sido instalada al vehículo
    /// </summary>
    public class LlantaInstalada
    {
        public int Posicion { get; set; }
        public Llanta Rueda { get; set; }
    }
}
