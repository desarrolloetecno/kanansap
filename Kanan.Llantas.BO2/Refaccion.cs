﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kanan.Llantas.BO2
{
    [Serializable]
    public class Refaccion
    {

        public int? RefaccionID { get; set; }
        public Llanta Neumatico { get; set; }

    }
}
