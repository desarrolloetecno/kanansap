﻿using System;
using Kanan.Costos.BO2;
using Kanan.Operaciones.BO2;
using System.Runtime.Serialization;
namespace Kanan.Llantas.BO2
{
     
    [Serializable]
    [DataContract]
    public class Llanta:ICloneable
    {
        [DataMember]
        public int? LlantaID { get; set; }
        [DataMember]
        public string NumeroEconomico { get; set; }
        [DataMember]
        public int? NumeroRevestimientos { get; set; }
        [DataMember]
        public string SN { get; set; }
        [DataMember]
        public string Marca { get; set; }
        [DataMember]
        public string Modelo { get; set; }
        //public string Tamanio { get; set; }
        [DataMember]
        public int? NumeroCapas { get; set; }
        [DataMember]
        public decimal? Costo { get; set; }
        [DataMember]
        public string NumeroFactura { get; set; }
        [DataMember]
        public DateTime? FechaCompra { get; set; }
        [DataMember]
        public bool? EstaActivo { get; set; }
        [DataMember]
        public bool? EstaInstalado { get; set; }
        [DataMember]
        public string Observaciones { get; set; }
        [DataMember]
        public double? KilometrosRecorridos { get; set; }
        [DataMember]
        public int? Uso { get; set; }
        [DataMember]
        public string Ancho { get; set; }
        [DataMember]
        public string Aspecto { get; set; }
        [DataMember]
        public int? Construccion { get; set; }
        [DataMember]
        public string DiametroRin { get; set; }
        [DataMember]
        public string IndiceCarga { get; set; }
        [DataMember]
        public int? RangoVelocidad { get; set; }
        [DataMember]
        public int? Aplicacion { get; set; }
        [DataMember]
        public int? Traccion { get; set; }
        [DataMember]
        public string Treadwear { get; set; }
        [DataMember]
        public int? Temperatura { get; set; }
        [DataMember]
        public string MaximaPresionInflado { get; set; }
        [DataMember]
        public string MinimaPresionInflado { get; set; }
        [DataMember]
        public DateTime? FechaFabricacion { get; set; }
        [DataMember]
        public Empresa Propietario { get; set; }
        [DataMember]
        public Sucursal SubPropietario { get; set; }
        //public TipoRotacion? ClaseRotacion { get; set; }
        [DataMember]
        public Proveedor proveedorLlanta { get; set; }
        [DataMember]
        public decimal? KilometrosCaducidad { get; set; }
        [DataMember]
        public DateTime? FechaCaducidad { get; set; }
        [DataMember]
        public int Sincronizado { get; set; }
        [DataMember]
        public string CondicionGarantia { get; set; }
        [DataMember]
        public String CodigoAlmacen { get; set; }

        [DataMember]
        public string ItemCode { get; set; }
        [DataMember]
        public String ClaseRotacion { get; set; }
        [DataMember]
        public String SucursalID { get; set; }

        [DataMember]
        public String EstatusSinc { get; set; }

        [DataMember]
        public String ConsecutivoCode { get; set; }
        [DataMember]
        public String ConsecutivoName { get; set; }


        public Llanta()
        {
            this.EstaActivo = true;
        }



        public object Clone()
        {
            Llanta llanta = new Llanta();
            llanta.LlantaID = this.LlantaID;
            llanta.NumeroEconomico = this.NumeroEconomico;
            llanta.SN = this.SN;
            llanta.Marca = this.Marca;
            llanta.Modelo = this.Modelo;
            llanta.NumeroFactura = this.NumeroFactura;
            //llanta.Tamanio = this.Tamanio;
            llanta.NumeroCapas = this.NumeroCapas;
            llanta.Costo = this.Costo;
            //llanta.ClaseRotacion = this.ClaseRotacion;
            llanta.FechaCompra = this.FechaCompra;
            llanta.EstaActivo = this.EstaActivo;
            llanta.EstaInstalado = this.EstaInstalado;
            llanta.Observaciones = this.Observaciones;
            llanta.KilometrosRecorridos = this.KilometrosRecorridos;
            if (this.Propietario != null)
            {
                llanta.Propietario = this.Propietario;
            }
            if (this.proveedorLlanta != null)
            {
                llanta.proveedorLlanta = this.proveedorLlanta;
            }
            if (this.SubPropietario != null)
            {
                llanta.SubPropietario = (Sucursal)this.SubPropietario.Clone();
            }
            llanta.Sincronizado = this.Sincronizado;
            return llanta;
        }
    }
}
