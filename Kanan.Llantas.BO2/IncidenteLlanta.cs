﻿using System;
using System.Runtime.Serialization;

namespace Kanan.Llantas.BO2
{
    [Serializable]
    [DataContract]
    public class IncidenteLlanta:ICloneable
    {
        [DataMember]
        public int? IncidenteID { get; set; }
        [DataMember]
        public string Accion { get; set; }
        [DataMember]
        public string Ubicacion { get; set; }
        [DataMember]
        public string Observaciones { get; set; }
        //[DataMember]
        //public ILlanta Llantable { get; set; }
        [DataMember]
        public int? Neumatico { get; set; }
        [DataMember]
        public DateTime? Fecha { get; set; }
        [DataMember]
        public DateTime? FechaCaptura { get; set; }
        [DataMember]
        public string GrosorCapa { get; set; }
        [DataMember]
        public double? DistanciaRecorrida { get; set; }
        [DataMember]
        public int? Sincronizado { get; set; }
        [DataMember]
        public int? MantenibleID { get; set; }
        [DataMember]
        public int? TipoMantenible { get; set; }
        [DataMember]
        public string Referencia { get; set; }

        public object Clone()
        {
            IncidenteLlanta incidente = new IncidenteLlanta();
            incidente.Accion = this.Accion;
            incidente.Ubicacion = this.Ubicacion;
            incidente.Observaciones = this.Observaciones;
            incidente.GrosorCapa = this.GrosorCapa;
            //if (this.Llantable != null)
            //{
            //    incidente.Llantable = this.Llantable;
            //}
            //if (this.Neumatico != null)
            //{
            //    incidente.Neumatico = (Llanta)this.Neumatico.Clone();
            //}
            return incidente;
        }
    }
}
