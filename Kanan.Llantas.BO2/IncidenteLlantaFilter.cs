﻿using System;

namespace Kanan.Llantas.BO2
{
    [Serializable]
    public class IncidenteLlantaFilter:IncidenteLlanta
    {
        public DateTime? FechaFin { get; set; }
    }
}
