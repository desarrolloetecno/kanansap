﻿using System;
namespace Kanan.Operaciones.BO2
{
    [Serializable]
    public class Documento
    {
        public int? DocumentoID { get; set; }
        public string TipoArchivo { get; set; }
        public string NombreArchivo { get; set; }
        /// <summary>
        /// Descripción principal del archivo, de que se trata el documento.
        /// </summary>
        public string Descripcion { get; set; }
        public byte[] Archivo { get; set; }
    }
}
