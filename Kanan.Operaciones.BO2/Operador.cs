﻿using System;
using System.Collections.Generic;
using Kanan.Comun.BO2;
using System.Runtime.Serialization;

namespace Kanan.Operaciones.BO2
{
    [Serializable]
    [DataContract]
    public class Operador
    {
        [DataMember]
        public int? OperadorID { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public string Alias { get; set; }
        [DataMember]
        public string Apellido1 { get; set; }
        [DataMember]
        public string Apellido2 { get; set; }
        [DataMember]
        public string Direccion { get; set; }
        [DataMember]
        public string Telefono { get; set; }
        [DataMember]
        public string Celular { get; set; }
        [DataMember]
        public string Correo { get; set; }
        [DataMember]
        public string CorreosNotificacion { get; set; }
        [DataMember]
        public string Curp { get; set; }
        [DataMember]
        /// <summary>
        /// Indica el estado del operador en el sistema
        /// </summary>
        public bool? EsActivo { get; set;}
        [DataMember]
        /// <summary>
        /// Indica algun rasgo físico o descripción
        /// </summary>
        public string SeniasParticulares { get; set; }
        [DataMember]
        /// <summary>
        /// Imagen del operador
        /// </summary>
        public Imagen Picture { get; set;}
        [DataMember]
        public int? EmpleadoID { get; set; }
        [DataMember]
        public Empresa Propietario { get; set; }
        [DataMember]
        public Sucursal SubPropietario { get; set; }
        [DataMember]
        public DateTime? FechaNacimiento { get; set; }
        [DataMember]
        public string LugarNacimiento { get; set; }
        [DataMember]
        public List<Licencia> Licencias { get; set; }
        [DataMember]
        public List<Observacion> Observaciones { get; set; }
        [DataMember]
        public List<Curso> Cursos { get; set; }
        [DataMember]
        public List<Documento> Documentos { get; set; }
        [DataMember]
        public String sCadenaImagen { get; set; }

        [DataMember]
        public Double? FondoFijo { get; set; }

        [DataMember]
        public String sCardCode { get; set; }

        public object Clone()
        {
            Operador operador = new Operador
            {
                OperadorID = this.OperadorID,
                Nombre = this.Nombre,
                Alias = this.Alias,
                Apellido1 = this.Apellido1,
                Apellido2 = this.Apellido2,
                Direccion = this.Direccion,
                Telefono = this.Telefono,
                Celular = this.Celular,
                Correo = this.Correo,
                CorreosNotificacion = this.CorreosNotificacion,
                Curp = this.Curp,
                EsActivo = this.EsActivo,
                SeniasParticulares = SeniasParticulares,
                Picture = this.Picture,
                EmpleadoID = this.EmpleadoID,
                Propietario = this.Propietario,
                SubPropietario = this.SubPropietario,
                FechaNacimiento = this.FechaNacimiento,
                LugarNacimiento = this.LugarNacimiento,
                Licencias = this.Licencias,
                Observaciones = this.Observaciones,
                Cursos = this.Cursos,
                Documentos = this.Documentos,
                sCadenaImagen = this.sCadenaImagen
            };

            return operador;
        }
    }
}
