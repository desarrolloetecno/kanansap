﻿using System;
using System.Xml.Serialization;
namespace Kanan.Operaciones.BO2
{
    [Serializable]
    public enum TipoEmpleado:int 
    {
        [XmlEnum("1")]
        ORIGINAL=1,
        [XmlEnum("2")]
        GENERICO=2,
        [XmlEnum("3")]
        MOVIL = 3
    }
}
