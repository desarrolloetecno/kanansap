﻿using System;
using System.Runtime.Serialization;
using Kanan.Comun.BO2;
using Kanan.Operaciones.BO2;

namespace Kanan.Operaciones.BO2
{
    [Serializable]
    [DataContract]

    public class Empresa : ICloneable, Notificable
    {

        [DataMember]
        public int? EmpresaID { get; set; }

        [DataMember]
        public string Nombre { get; set; }

        [DataMember]
        public string Datos { get; set; }
        /// <summary>
        /// Propiedad generica para distinguir la alerta de tipo 1 o 2
        /// </summary>

        [DataMember]
        public int? TipoAlertAcc { get; set; }
        /// <summary>
        /// Indica si la cuenta del kanan esta inactiva
        /// </summary>

        [DataMember]
        public bool? EsActivo { get; set; }
        [DataMember]

        public Imagen ImagenEmpresa { get; set; }
        /// <summary>
        /// Fecha del alta de la empresa
        /// </summary>

        [DataMember]
        public DateTime? FechaAlta { get; set; }
        ///// <summary>
        ///// Fecha de la baja de la empresa
        ///// </summary>
        //public DateTime? FechaBaja { get; set; }
        /// <summary>
        /// Tipo del cliente
        /// </summary>

        [DataMember]
        public TipoCliente? Clasificacion { get; set; }
        /// <summary>
        /// Dato de facturación
        /// </summary>

        [DataMember]
        public string Facturacion { get; set; }
        /// <summary>
        /// Indica el estado actual de la empresa
        /// </summary>

        [DataMember]
        public TipoEstadoEmpresa? Estado { get; set; }
        /// <summary>
        /// Número de vehículos con remora instalado
        /// </summary>

        [DataMember]
        public int? ERPermitidos { get; set; }
        /// <summary>
        /// Número de vehiculos sin remora
        /// </summary>

        [DataMember]
        public int? EKPermitidos { get; set; }
        /// <summary>
        /// Cuando se desactivará la cuenta
        /// </summary>

        [DataMember]
        public DateTime? FechaDesactivacion { get; set; }
        /// <summary>
        /// Indica a los cuantos dias se avisa para la alerta
        /// </summary>

        [DataMember]
        public int? AvisarVencimiento { get; set; }
        /// <summary>
        /// Indica la fecha de inicio que se usara para filtrar datos de la empresa
        /// como reportes, o consultas con fechas especificas
        /// </summary>

        [DataMember]
        public DateTime? FechaFiltroInicio { get; set; }
        /// <summary>
        /// Indica la fecha final que se usara para filtrar datos de la empresa
        /// como reportes, o consultas con fechas especificas
        /// </summary>

        [DataMember]
        public DateTime? FechaFiltroFin { get; set; }

        public Empresa()
        {
            this.EsActivo = true;
        }

        public object Clone()
        {
            Empresa empresa = new Empresa();
            empresa.EKPermitidos = this.EKPermitidos;
            empresa.ERPermitidos = this.ERPermitidos;
            empresa.EmpresaID = this.EmpresaID;
            empresa.Nombre = this.Nombre;
            empresa.Datos = this.Datos;
            empresa.EsActivo = this.EsActivo;
            empresa.FechaAlta = this.FechaAlta;
            empresa.AvisarVencimiento = this.AvisarVencimiento;
            empresa.Clasificacion = this.Clasificacion;
            empresa.Facturacion = this.Facturacion;
            empresa.FechaFiltroInicio = this.FechaFiltroInicio;
            empresa.FechaFiltroFin = this.FechaFiltroFin;
            if (this.ImagenEmpresa != null)
            {
                empresa.ImagenEmpresa = (Imagen)this.ImagenEmpresa.Clone();
            }
            return empresa;
        }
        [DataMember]
        public int? NotificableID
        {
            get
            {
                return this.EmpresaID;
            }
            set
            {
                this.EmpresaID = value;
            }
        }
    }
}
