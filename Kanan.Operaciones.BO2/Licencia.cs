﻿using System;
using Kanan.Comun.BO2;

namespace Kanan.Operaciones.BO2
{
    [Serializable]
    public class Licencia:Notificable
    {
        public int? LicenciaID { get; set; }
        public TipoLicencia? ClaseLicencia { get; set; }
        public string Numero { get; set; }
        public DateTime? VigenciaLicencia { get; set; }
        public bool? Enabled { get; set; }
        public int? AvisoLicencia { get; set; }

        public int? NotificableID
        {
            get
            {
                return this.LicenciaID;
            }
            set
            {
                this.LicenciaID = value;
            }
        }

        public object Clone()
        {
            var clone = new Licencia
            {
                LicenciaID = this.LicenciaID,
                ClaseLicencia = this.ClaseLicencia,
                Numero = this.Numero,
                VigenciaLicencia = this.VigenciaLicencia,
                Enabled = this.Enabled,
                AvisoLicencia = this.AvisoLicencia
            };
            return clone;
        }
    }
}
