﻿using System;

namespace Kanan.Operaciones.BO2
{
    [Serializable]
    public class Observacion
    {
        public int? ObservacionID { get; set; }
        public string Descripcion { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public DateTime? FechaObservacion { get; set; }

        public object Clone()
        {
            var clone = new Observacion
            {
                ObservacionID = this.ObservacionID,
                Descripcion = this.Descripcion,
                FechaRegistro = this.FechaRegistro,
                FechaObservacion = this.FechaObservacion
            };
            return clone;
        }
    }
}
