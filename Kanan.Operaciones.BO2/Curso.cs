﻿using System;

namespace Kanan.Operaciones.BO2
{
    [Serializable]
    public class Curso
    {
        public int? CursoID { get; set; }
        public string Nombre { get; set; }
        public DateTime? Fecha { get; set; }
        public string EmpresaOtorga { get; set; }
        public string Lugar { get; set; }

        public object Clone()
        {
            var clone = new Curso
            {
                CursoID = this.CursoID,
                Nombre = this.Nombre,
                Fecha = this.Fecha,
                EmpresaOtorga = this.EmpresaOtorga,
                Lugar = this.Lugar
            };
            return clone;
        }
    }
}
