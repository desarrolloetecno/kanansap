﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
namespace Kanan.Operaciones.BO2
{
    [Serializable]
    public enum TipoCliente:int
    {
        [EnumMember]
        [XmlEnum("1")] /// Ponerlo por cada valor
        FIJO=1,
        [EnumMember]
        [XmlEnum("2")]
        PRUEBA=2,
        [EnumMember]
        [XmlEnum("3")]
        MOVIL = 3
    }
}
