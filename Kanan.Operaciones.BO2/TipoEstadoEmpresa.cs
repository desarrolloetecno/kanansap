﻿using System.Xml.Serialization;
using System;
using System.Runtime.Serialization;
namespace Kanan.Operaciones.BO2
{
    [Serializable]
    [DataContract]
    public enum TipoEstadoEmpresa : int
    {
        [EnumMember]
        [XmlEnum("1")]
        ACTIVADO = 1,
        [EnumMember]
        [XmlEnum("2")]
        SUSPENDIDO = 2,
        [EnumMember]
        [XmlEnum("2")]
        DESACTIVADO = 3
    }
}
