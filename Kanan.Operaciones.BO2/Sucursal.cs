﻿using System;
using System.Runtime.Serialization;
using Kanan.Comun.BO2;

namespace Kanan.Operaciones.BO2
{
    [Serializable]
    [DataContract]
    public class Sucursal : ICloneable, Notificable
    {
        /// <summary>
        /// Corresponde al Code que entrelaza a SAP con Kananfleet
        /// </summary>
        [DataMember]
        public string Code { get; set; }

        /// <summary>
        /// Corresponde al Name que entrelaza a SAP con Kananfleet
        /// </summary>
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Almacen { get; set; }

        [DataMember]
        public int? SucursalID { get; set; }
        /// <summary>
        /// Es el nombre principal de la sucursal
        /// </summary>
        [DataMember]
        public string Direccion { get; set; }
        [DataMember]
        public Empresa Base { get; set; }
        [DataMember]
        public bool? EsActivo { get; set; }
        /// <summary>
        /// Responsable general de la sucursal
        /// </summary>
        [DataMember]
        public string Responsable { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public string Contacto { get; set; }
        [DataMember]
        public string Ciudad { get; set; }
        [DataMember]
        public string Estado { get; set; }
        [DataMember]
        public string Telefono { get; set; }
        [DataMember]
        public int? CodigoPostal { get; set; }
        public int? BranchID { get; set; }
        /// <summary>
        /// Dirección de la sucursal
        /// </summary>
        [DataMember]
        public string DireccionReal { get; set; }
        [DataMember]
        public string CostCenter { get; set; }

        [DataMember]
        public string MailAlmacenista { get; set; }

        [DataMember]
        public Int32? HorasDisponibilidad { get; set; }

        public Sucursal()
        {
            this.EsActivo = true;
        }

        public object Clone()
        {
            Sucursal sucursal = new Sucursal();
            sucursal.Code = this.Code;
            sucursal.Name = this.Name;
            sucursal.Almacen = this.Almacen;
            sucursal.SucursalID = this.SucursalID;
            sucursal.Direccion = this.Direccion;
            sucursal.EsActivo = this.EsActivo;
            sucursal.Responsable = this.Responsable;
            sucursal.Descripcion = this.Descripcion;
            sucursal.Contacto = this.Contacto;
            sucursal.Ciudad = this.Ciudad;
            sucursal.Estado = this.Estado;
            sucursal.Telefono = this.Telefono;
            sucursal.CodigoPostal = this.CodigoPostal;
            sucursal.DireccionReal = this.DireccionReal;
            sucursal.MailAlmacenista = this.MailAlmacenista;
            sucursal.HorasDisponibilidad = this.HorasDisponibilidad;

            if (this.Base != null)
            {
                sucursal.Base = (Empresa)Base.Clone();
            }
            return sucursal;
        }
        [DataMember]
        public int? NotificableID
        {
            get
            {
                return this.SucursalID;
            }
            set
            {
                this.SucursalID = value;
            }
        }
    }
}
