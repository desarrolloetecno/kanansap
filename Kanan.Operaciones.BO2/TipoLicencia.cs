﻿
using System;
using System.Xml.Serialization;
namespace Kanan.Operaciones.BO2
{
    [Serializable]
    public enum TipoLicencia:int
    {
        [XmlEnum("1")]
        A=1,
        [XmlEnum("2")]
        B=2,
        [XmlEnum("3")]
        C=3,
        [XmlEnum("4")]
        D=4,
        [XmlEnum("5")]
        E=5
    }
}
