﻿using System;
using System.Runtime.Serialization;
using Etecno.Security2.BO;
using System.Collections.Generic;
using Kanan.Comun.BO2;

namespace Kanan.Operaciones.BO2
{
    [Serializable]
    [DataContract]
    public class Empleado:ICloneable
    {
        public Empleado() 
        {
            this.Configs = new Dictionary<string, string>();
        }
        [DataMember]
        public int? EmpleadoID { get; set; }
                [DataMember]
        public string Nombre { get; set; }
        [DataMember]

        public string Apellido1 { get; set; }
        [DataMember]

        public string Apellido2 { get; set; }
        [DataMember]

        public string Telefono { get; set; }
        [DataMember]

        public string Direccion { get; set; }
        [DataMember]

        public string Email { get; set; }
        [DataMember]

        public string Celular { get; set; }
        [DataMember]

        public Empresa Dependencia { get; set; }
        [DataMember]

        public Sucursal EmpleadoSucursal { get; set; }
        [DataMember]

        public Usuario Usuario { get; set; }

        [DataMember]
        public TipoEmpleado? Tipo { get; set; }
        ////Unidad Medida

        [DataMember]
        public UnidadLongitud longitud { get; set; }

        [DataMember]
        public UnidadVolumen volumen { get; set; }
        /// <summary>
        /// Datos de la configuración
        /// </summary>
        //private string configuraciones;

        [DataMember]
        public Dictionary<string, string> Configs { get; set; }
        //public List<OrdenServicio> OrdenesServicio { get; set; }
       
        public object Clone()
        {
            Empleado empleado = new Empleado();
            empleado.EmpleadoID = this.EmpleadoID;
            empleado.Nombre = this.Nombre;
            empleado.Apellido1 = this.Apellido1;
            empleado.Apellido2 = this.Apellido2;
            empleado.Telefono = this.Telefono;
            empleado.Direccion = this.Direccion;
            empleado.Celular = this.Celular;
            empleado.Configs = this.Configs;
            if (this.Dependencia != null)
            {
                empleado.Dependencia = (Empresa)this.Dependencia.Clone();
            }
            if (this.EmpleadoSucursal != null)
            {
                empleado.EmpleadoSucursal =(Sucursal) this.EmpleadoSucursal.Clone();
            }
            if (this.Usuario != null)
            {
                empleado.Usuario =(Usuario) this.Usuario;
            }
            if (this.Tipo != null)
            {
                empleado.Tipo = this.Tipo;
            }
            ////Unidad Medida
            empleado.longitud = this.longitud;
            empleado.volumen = this.volumen;
            return empleado;
        }

        public void ConfigurationFromString(string config) 
        {
            string[] configurations = config.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            /// Recorrer cada configuración 
            for (int i = 0; i < configurations.Length; i++)
            {
                /// Obtener la configuración para agregar al hashtable delimitando cada configuración por :
                string[] data = configurations[i].Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                /// Validar si el valor de la configuracion es == a 2
                if (data.Length == 2)
                    this.Configs.Add(data[0].Trim(), data[1].Trim()); /// Agregar configuración al hash
            }
        }
        public string ConfigurationToString() 
        {
            string s=string.Empty;
            if (this.Configs == null) return null;
            foreach(string key in this.Configs.Keys)
            {                
                string value=this.Configs[key];
                s+=string.Format("{0}={1};",key, value);
            }
            return s;
        }
    }
}
