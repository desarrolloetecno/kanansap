﻿using System;
using Kanan.Costos.BO2;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using Kanan.Comun.BO2;
using System.Runtime.Serialization;

namespace Kanan.Mantenimiento.BO2
{
    [Serializable]
    [DataContract]
    public class Refaccion : ICloneable
    {
        [DataMember]
        public int? RefaccionID { get; set; }
        [DataMember]
        public string Nombre { get; set; }


        public object Clone()
        {
            Refaccion refaccion = new Refaccion();
            refaccion.Nombre = this.Nombre;
            refaccion.RefaccionID = this.RefaccionID;

            return refaccion;
        }
    }
}
