﻿using Kanan.Vehiculos.BO2;
using Kanan.Operaciones.BO2;
using System;

namespace Kanan.Mantenimiento.BO2
{
    [Serializable]
    public class AlertaMantenimiento
    {
        public Vehiculo vehiculo { get; set; }
        public Servicio servicio { get; set; }
        public Empresa empresa { get; set; }
        public string Mensaje{ get; set; }
        public Sucursal SubPropietario { get; set; }
        public string DescripcionServicio { get; set; }

        public int? AlertaMantenimientoID { get; set; }
        /// <summary>
        /// Representa un objeto que puede ser utilizado para el modulo de mantenimiento
        /// </summary>
        public IMantenible Mantenible { get; set; }

        /// <summary>
        /// Representa el tipo de mantenible , 1=vehiculo, 2=caja
        /// </summary>
        public int? TypeObject { get; set; }
        public ParametroServicio ParametroServicio { get; set; }

        /// <summary>
        /// Indica el tipo de alerta en cuestion de la medición 
        /// esto es como 1=por distancia, 2=tiempo o por dia, entre otros
        /// </summary>
        public int? TypeOfType { get; set;}
        /// <summary>
        /// Indica el proximo servicio por tiempo
        /// </summary>
        public DateTime? ProximoServicioTiempo { get; set;}
        /// <summary>
        /// Cuando le toca el proximo servicio al vehiculo
        /// </summary>
        public double? ProximoServicio { get; set; }
        /// <summary>
        /// Distancia real que lleva recorrido el Vehiculo
        /// </summary>
        public double? DistanciaRecorrida { get; set; }
        /// <summary>
        /// Distancia a la cual se debe generar una alerta preventiva
        /// </summary>
        public decimal? AlertaServicio { get; set; }

        /// <summary>
        /// Identificador de la alerta, dejado de esta manera para la compativilidad 
        /// con el servicio de alerta que existia anteriormente, es alerta mantenimiento id
        /// </summary>
        //public int? AlertaAvisoID { get; set; }

        /// <summary>
        /// Identifica el tipo de alerta del que se trata 
        /// 1 para preventiva 
        /// 2 para alarma
        /// </summary>
        public int? TipoAlerta { get; set; }

        public DateTime? Fecha { get; set; }

        public bool? EstadoAlerta { get; set; }

        public object Clone()
        {
            AlertaMantenimiento alert = new AlertaMantenimiento();
            alert.AlertaMantenimientoID = AlertaMantenimientoID;
            alert.Mensaje = this.Mensaje;
            alert.DescripcionServicio = this.DescripcionServicio;
            alert.ProximoServicio = this.ProximoServicio;
            alert.DistanciaRecorrida = this.DistanciaRecorrida;
            alert.AlertaServicio = this.AlertaServicio;
            //alert.AlertaAvisoID = this.AlertaAvisoID;
            alert.TipoAlerta = this.TipoAlerta;
            alert.Fecha = this.Fecha;
            alert.EstadoAlerta = this.EstadoAlerta;
            alert.TypeOfType = this.TypeOfType;
            /// time
            alert.ProximoServicioTiempo = this.ProximoServicioTiempo;

            if (this.empresa != null)
                alert.empresa = (Empresa)this.empresa.Clone();
            if (this.SubPropietario != null)
                alert.SubPropietario = (Sucursal)this.SubPropietario.Clone();

            if (TypeObject != null)
            {
                alert.TypeObject = TypeObject;
                if (this.TypeObject == 1)
                    alert.Mantenible = (Vehiculo)this.Mantenible.Clone();
                else if (this.TypeObject == 2)
                    alert.Mantenible = (Caja)this.Mantenible.Clone();
            }
            if (this.ParametroServicio != null)
                alert.ParametroServicio = ParametroServicio;

            return alert;
        }

        //public object Clone()
        //{
        //    AlertaMantenimiento alert = new AlertaMantenimiento();
        //    if (this.vehiculo != null)
        //        alert.vehiculo = (Vehiculo)this.vehiculo.Clone();
        //    if (this.servicio != null)
        //        alert.servicio = (Servicio)this.servicio.Clone();
        //    if (this.empresa != null)
        //        alert.empresa = (Empresa)this.empresa.Clone();
        //    if (this.SubPropietario != null)
        //        alert.SubPropietario = (Sucursal)this.SubPropietario.Clone();

        //    alert.Mensaje = this.Mensaje;
        //    alert.DescripcionServicio = this.DescripcionServicio;
        //    alert.ProximoServicio = this.ProximoServicio;
        //    alert.DistanciaRecorrida = this.DistanciaRecorrida;
        //    alert.AlertaServicio = this.AlertaServicio;
        //    alert.AlertaAvisoID = this.AlertaAvisoID;
        //    alert.TipoAlerta = this.TipoAlerta;
        //    alert.Fecha = this.Fecha;
        //    alert.EstadoAlerta = this.EstadoAlerta;
        //    alert.TypeOfType = this.TypeOfType;
        //   /// time
        //    alert.ProximoServicioTiempo = this.ProximoServicioTiempo;
        //    return alert;
        //}
    }
}
