﻿using System;
using Kanan.Operaciones.BO2;
using System.Runtime.Serialization;

namespace Kanan.Mantenimiento.BO2
{
    [Serializable]
    [DataContract]
    public class TipoServicio : ICloneable
    {
        [DataMember]
        public int? TipoServicioID { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public Empresa Propietario { get; set; }
        [DataMember]
        public Sucursal SubPropietario { get; set; }
        [DataMember]
        public bool? EsActivo { get; set; }

        public TipoServicio()
        {
            this.EsActivo = true;
        }

        public object Clone()
        {
            TipoServicio tipoServicio = new TipoServicio();
            tipoServicio.TipoServicioID = this.TipoServicioID;
            tipoServicio.Nombre = this.Nombre;
            tipoServicio.Descripcion = this.Descripcion;
            tipoServicio.EsActivo = this.EsActivo;
            if (this.Propietario != null)
            {
                tipoServicio.Propietario = (Empresa)this.Propietario.Clone();
            }
            if (this.SubPropietario != null)
            {
                tipoServicio.SubPropietario = (Sucursal)this.SubPropietario.Clone();
            }
            return tipoServicio;
        }
    }
}
