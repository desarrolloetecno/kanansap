﻿using System;
using Kanan.Costos.BO2;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using Kanan.Comun.BO2;
using System.Collections.Generic;

using System.Collections.Generic;

namespace Kanan.Mantenimiento.BO2
{
    [Serializable]
    public class OrdenServicio:ICloneable
    {
        public int? OrdenServicioID { get; set; }
        /// <summary>
        /// Fecha que captura el usuario
        /// </summary>
        public DateTime? Fecha{ get; set; }
        /// <summary>
        /// Fecha que captura el sistema
        /// </summary>
        public DateTime? FechaCaptura { get; set; }
        public string Folio { get; set; }
        public string Lugar{ get; set; }
        public string Descripcion { get; set; }
        public Empleado ResponsableOrdenServicio { get; set; }
        public Empleado EncargadoOrdenServicio { get; set; }
        public Empleado AprobadorOrdenServicio { get; set; }
        public EstatusOrden EstatusOrden { get; set; }
        public Empresa EmpresaOrden { get; set; }
        public Sucursal SucursalOrden { get; set; }
        public Proveedor ProveedorServicio { get; set; }
        public TipoDePago TipoDePago { get; set; }
        public decimal? KilometrosRecorridos { get; set; }
        public decimal? Impuesto { get; set; }
        public decimal? Costo { get; set; }
        public string ImagenArchivo { get; set; }
        public List<AlertaMantenimiento> Alertas { get; set; }
        public List<DetalleOrden> DetalleOrden { get; set; }
        
        public AlertaMantenimiento alertas { get; set; }
        public FolioOrden FolioOrden { get; set; }

        public OrdenServicio()
        {
            ResponsableOrdenServicio = new Empleado();
            EncargadoOrdenServicio = new Empleado();
            AprobadorOrdenServicio = new Empleado();
            EmpresaOrden = new Empresa();
            SucursalOrden = new Sucursal();
            ProveedorServicio = new Proveedor();
            TipoDePago = new TipoDePago();
            Alertas = new List<AlertaMantenimiento>();
            DetalleOrden = new List<DetalleOrden>();
            EstatusOrden = new EstatusOrden();
            FolioOrden = new FolioOrden();
        }

        public object Clone()
        {
            OrdenServicio ordenServicio = new OrdenServicio();
            ordenServicio.OrdenServicioID = this.OrdenServicioID;
            ordenServicio.Fecha = this.Fecha;
            ordenServicio.FechaCaptura = this.FechaCaptura;
            ordenServicio.Folio = this.Folio;
            ordenServicio.Lugar = this.Lugar;
            ordenServicio.Descripcion = this.Descripcion;
            if (this.ResponsableOrdenServicio != null)
            {
                ordenServicio.ResponsableOrdenServicio = (Empleado)this.ResponsableOrdenServicio.Clone();
            }
            if (this.EncargadoOrdenServicio != null)
            {
                ordenServicio.EncargadoOrdenServicio = (Empleado)this.EncargadoOrdenServicio.Clone();
            }
            if (this.AprobadorOrdenServicio != null)
            {
                ordenServicio.AprobadorOrdenServicio = (Empleado)this.AprobadorOrdenServicio.Clone();
            }
            if (this.EstatusOrden != null)
            {
                ordenServicio.EstatusOrden = (EstatusOrden)this.EstatusOrden.Clone();
            }
            if (this.SucursalOrden != null)
            {
                ordenServicio.SucursalOrden = (Sucursal)this.SucursalOrden.Clone();
            }
            if (this.EmpresaOrden != null)
            {
                ordenServicio.EmpresaOrden = (Empresa)this.EmpresaOrden.Clone();
            }
            if (this.ProveedorServicio != null)
            {
                ordenServicio.ProveedorServicio = (Proveedor)this.ProveedorServicio.Clone();
            }
            if (this.TipoDePago != null)
            {
                ordenServicio.TipoDePago = (TipoDePago)this.TipoDePago.Clone();
            }
            ordenServicio.KilometrosRecorridos = this.KilometrosRecorridos;
            ordenServicio.Impuesto = this.Impuesto;
            ordenServicio.Costo = this.Costo;
            ordenServicio.ImagenArchivo = this.ImagenArchivo;



            if (this.Alertas != null)
            {
                foreach (AlertaMantenimiento a in this.Alertas)
                {
                    AlertaMantenimiento am = (AlertaMantenimiento)a.Clone();
                    ordenServicio.Alertas.Add(am);
                }
            }

            if (this.DetalleOrden != null)
            {
                foreach (DetalleOrden d in this.DetalleOrden)
                {
                    DetalleOrden detO = (DetalleOrden)d.Clone();
                    ordenServicio.DetalleOrden.Add(detO);
                }
            }
            if (this.FolioOrden != null)
                ordenServicio.FolioOrden = (FolioOrden)this.FolioOrden.Clone();

            return ordenServicio;
                
        }
    }
}
