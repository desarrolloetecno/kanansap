﻿using System;
using Kanan.Vehiculos.BO2;
using System.Runtime.Serialization;
using Kanan.Mantenimiento.BO2;

namespace Kanan.Mantenimiento.BO2
{
    [Serializable]
    [DataContract]
    public class ParametroServicio : ICloneable
    {
        [DataMember]
        public int? ParametroServicioID { get; set; }
        // Parámetro de servicio para Distancia
        [DataMember]
        public Double? Valor { get; set; }
        [DataMember]
        public Double? Alerta { get; set; }


        // Parámetro de servicio para Tiempo
        [DataMember]
        public int? ValorTiempo { get; set; }
        [DataMember]
        public int? AlertaTiempo { get; set; }
        /// <summary>
        /// La propiedad Mensaje debe tener maximo 140 Caracteres
        /// </summary>
        /// 
        [DataMember]
        public string Mensaje { get; set; }
        [DataMember]
        public Servicio Servicio { get; set; }
        [DataMember]
        public IMantenible Mantenible { get; set; }

        public object Clone()
        {
            ParametroServicio parametro = new ParametroServicio();
            parametro.ParametroServicioID = this.ParametroServicioID;
            parametro.Alerta = this.Alerta;
            parametro.Mantenible = this.Mantenible;
            parametro.Mensaje = this.Mensaje;
            parametro.Valor = this.Valor;
            parametro.ValorTiempo = this.ValorTiempo;
            parametro.AlertaTiempo = this.AlertaTiempo;
            if (this.Servicio != null)
            {
                parametro.Servicio = (Servicio)this.Servicio.Clone();
            }
            return parametro;
        }
    }

}
