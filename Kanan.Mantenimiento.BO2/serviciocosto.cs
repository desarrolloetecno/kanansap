﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Kanan.Mantenimiento.BO2
{
    [Serializable]
    [DataContract]
    public class serviciocosto
    {
        [DataMember]
        public int? servicioid { get; set; }
        [DataMember]
        public string servicio { get; set; }
        [DataMember]
        public decimal? costo { get; set; }
        /// <summary>
        /// main value for any task we need for venezuela company 01/07/2015 11:10am
        /// </summary>
        /// 
        [DataMember]
        public decimal? impuesto { get; set; }
        [DataMember]
        public decimal? ivaReal { get { return costo * impuesto; } }
        [DataMember]
        public string vehiculo_nombre { get; set; }
        [DataMember]
        public int? vehiculoid { get; set; }
        [DataMember]
        public int? tipoMantenibleID { get; set; }
        [DataMember]
        public string servicioMantenible { get; set; }
        [DataMember]
        public int? tipoPagoID { get; set; }
        [DataMember]
        public string detallePago { get; set; }
    }
}
