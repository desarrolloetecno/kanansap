﻿
using System;
using System.Xml.Serialization;
namespace Kanan.Mantenimiento.BO2
{
    [Serializable]
    public enum TipoFecha:int
    {
        [XmlEnum("365")]
        Anio=365,
        [XmlEnum("30")]
        Mes=30,
        [XmlEnum("1")]
        Dia=1
    }
}
