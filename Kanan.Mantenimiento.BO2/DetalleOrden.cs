﻿using System;
using Kanan.Costos.BO2;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using Kanan.Comun.BO2;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Kanan.Mantenimiento.BO2
{
    [Serializable]
    [DataContract]
    public class DetalleOrden : ICloneable
    {
        [DataMember]
        public int? DetalleOrdenID { get; set; }

        //[DataMember]
        //public Vehiculo Vehiculo { get; set; }

        [DataMember]
        public Servicio Servicio { get; set; }

        [DataMember]
        public IMantenible Mantenible { get; set; }

        [DataMember]
        public int? TipoMantenibleID { get; set; }

        [DataMember]
        public OrdenServicio OrdenServicio { get; set; }

        [DataMember]
        public decimal Costo{ get; set; }

        [DataMember]
        public List<DetalleRefaccion> DetalleRefacciones { get; set; }

        public  DetalleOrden()
        {
            OrdenServicio = new OrdenServicio();
            Servicio = new Servicio();
            DetalleRefacciones = new List<DetalleRefaccion>();
        }

        public object Clone()
        {
            DetalleOrden detalleOrden = new DetalleOrden();
            detalleOrden.DetalleOrdenID = this.DetalleOrdenID;

            switch(this.TipoMantenibleID)
            {
                case 1:
                    detalleOrden.Mantenible = (Vehiculo)this.Mantenible.Clone();
                    break;
                case 2:
                    detalleOrden.Mantenible = (Caja)this.Mantenible.Clone();
                    break;
            }            
            detalleOrden.Servicio = (Servicio)this.Servicio.Clone();
            detalleOrden.OrdenServicio.OrdenServicioID = this.OrdenServicio.OrdenServicioID;
            detalleOrden.TipoMantenibleID = this.TipoMantenibleID;
            detalleOrden.Costo = this.Costo;

            if (this.DetalleRefacciones.Count > 0)
            {
                foreach (DetalleRefaccion df in this.DetalleRefacciones)
                {
                    DetalleRefaccion dref = (DetalleRefaccion)df.Clone();
                    detalleOrden.DetalleRefacciones.Add(dref);
                }
            }
            return detalleOrden;
        }

        public string UUID { get; set; }
    }
}
