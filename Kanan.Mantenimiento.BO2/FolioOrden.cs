﻿using Kanan.Operaciones.BO2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kanan.Mantenimiento.BO2
{
    [Serializable]
    public class FolioOrden:ICloneable
    {
        public int? FolioOrdenID { get; set; }
        /// <summary>
        /// Estable el Numero de Folio
        /// </summary>
        public int? Folio { get; set; }
        /// <summary>
        /// Anio del Folio de la orden
        /// </summary>
        public int? AnioFolio { get; set; }
        /// <summary>
        /// Empresa relacionada al Folio de la Orden
        /// </summary>
        public Empresa Propietario { get; set; }

        public FolioOrden()
        {
            this.Propietario = new Empresa();
        }

        public object Clone()
        {
            FolioOrden folio = new FolioOrden();
            folio.FolioOrdenID = this.FolioOrdenID;
            folio.Folio = this.Folio;
            folio.AnioFolio = this.AnioFolio;
            if (this.Propietario != null)
                folio.Propietario = this.Propietario;
            return folio;
        }
        
    }
}
