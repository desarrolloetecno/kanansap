﻿using System;
using Kanan.Operaciones.BO2;

using System.Runtime.Serialization;
using Kanan.Mantenimiento.BO2;

namespace Kanan.Mantenimiento.BO2
{
    [Serializable]
    [DataContract]
    public class Servicio : ICloneable
    {
        [DataMember]
        public int? ServicioID { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public string CuentaContable { get; set; }
        
        [DataMember]
        public bool? EsActivo { get; set; }
        [DataMember]
        public string ImagenUrl { get; set; }
        [DataMember]
        public TipoServicio TipoServicio { get; set; }
        [DataMember]
        public Empresa Propietario { get; set; }
        [DataMember]
        public Sucursal SubPropietario { get; set; }
        [DataMember]
        public bool? IsForReport { get; set; }
        public Servicio()
        {
            this.EsActivo = true;
            this.IsForReport = false;
        }

        public object Clone()
        {
            Servicio servicio = new Servicio();
            servicio.ServicioID = this.ServicioID;
            servicio.Nombre = this.Nombre;
            servicio.Descripcion = this.Descripcion;
            servicio.EsActivo = this.EsActivo;
            servicio.ImagenUrl = this.ImagenUrl;
            servicio.IsForReport = this.IsForReport;
            if (this.TipoServicio != null)
            {
                servicio.TipoServicio = (TipoServicio)this.TipoServicio.Clone();
            }
            if (this.Propietario != null)
            {
                servicio.Propietario = (Empresa)this.Propietario.Clone();
            }
            if (this.SubPropietario != null)
            {
                servicio.SubPropietario = (Sucursal)this.SubPropietario.Clone();
            }
            return servicio;
        }
    }
}
