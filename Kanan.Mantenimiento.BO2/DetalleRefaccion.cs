﻿using System;
using Kanan.Costos.BO2;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using Kanan.Comun.BO2;
using System.Runtime.Serialization;
using Kanan.Llantas.BO2;

namespace Kanan.Mantenimiento.BO2
{
    [Serializable]
    [DataContract]
    public class DetalleRefaccion : ICloneable
    {
        [DataMember]
        public int? DetalleRefaccionID { get; set; }
        [DataMember]
        public Refaccion Refaccion { get; set; }
        [DataMember]
        public float? Costo { get; set; }

        public object Clone()
        {
            DetalleRefaccion detalleRefaccion = new DetalleRefaccion();
            detalleRefaccion.DetalleRefaccionID = this.DetalleRefaccionID;
            //detalleRefaccion.Refaccion = (Refaccion)this.Refaccion.Clone();
            detalleRefaccion.Costo = this.Costo;

            return detalleRefaccion;
        }
    }
}
