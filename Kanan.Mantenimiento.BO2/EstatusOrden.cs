﻿using System;
using Kanan.Costos.BO2;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using Kanan.Comun.BO2;
using System.Runtime.Serialization;

namespace Kanan.Mantenimiento.BO2
{
    [Serializable]
    [DataContract]
    public class EstatusOrden : ICloneable
    {
        [DataMember]
        public int? EstatusOrdenID { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public bool? EsActivo { get; set; }


        public object Clone()
        {
            EstatusOrden estatusOrden = new EstatusOrden();
            estatusOrden.EstatusOrdenID = this.EstatusOrdenID;
            estatusOrden.Nombre = this.Nombre;
            estatusOrden.EsActivo = this.EsActivo;

            return estatusOrden;

        }
    }
}
