﻿using System;
using Kanan.Vehiculos.BO2;
using System.Runtime.Serialization;
using Kanan.Vehiculos.BO2;

namespace Kanan.Mantenimiento.BO2
{
    [Serializable]
    [DataContract]
    public class ParametroMantenimiento : ICloneable
    {
        [DataMember]
        public int? ParametroMantenimientoID { get; set; }

        // Parámetros de mtto para distancia
        [DataMember]
        public Double? UltimoServicio { get; set; }
        [DataMember]
        public Double? ProximoServicio { get; set; }
        [DataMember]
        public Double? AlertaProximoServicio { get; set; }

        // Parámetros de mtto para Tiempo
        [DataMember]
        public DateTime? UltimoServicioTiempo { get; set; }
        [DataMember]
        public DateTime? ProximoServicioTiempo { get; set; }
        [DataMember]
        public DateTime? AlertaProximoServicioTiempo { get; set; }
        [DataMember]
        public DateTime? UltimaAlertaEnviada { get; set; }


        /// <summary>
        /// Tipo de parametro que se usara para el servicio 1 = distancia, 2 = tiempo, 3 = ambos
        /// </summary>
        /// 
        [DataMember]
        public int? TipoParametro { get; set; }

        /// <summary>
        /// Representa un objeto que puede ser utilizado para el modulo de mantenimiento
        /// </summary>
        /// 
        [DataMember]
        public IMantenible Mantenible { get; set; }
        /// <summary>
        /// Representa el tipo de mantenible , 1=vehiculo, 2=caja
        /// </summary>
        /// 
        [DataMember]
        public int? TypeObject { get; set; }
        [DataMember]
        public ParametroServicio ParametroServicio { get; set; }
        //public Vehiculo VehiculoMantenimiento { get; set; }
        //public Caja CajaMantenimiento { get; set; }

        public object Clone()
        {
            ParametroMantenimiento parametro = new ParametroMantenimiento();
            parametro.ParametroMantenimientoID = this.ParametroMantenimientoID;
            parametro.UltimoServicio = this.UltimoServicio;
            parametro.UltimaAlertaEnviada = this.UltimaAlertaEnviada;
            parametro.ProximoServicio = this.ProximoServicio;
            parametro.AlertaProximoServicio = this.AlertaProximoServicio;
            parametro.TipoParametro = this.TipoParametro;
            /// cloning values for parameters time
            parametro.UltimoServicioTiempo = this.UltimoServicioTiempo;
            parametro.ProximoServicioTiempo = this.ProximoServicioTiempo;
            parametro.UltimoServicioTiempo = this.UltimoServicioTiempo;
            /// valid all objects for cloning every ones
            if (this.TypeObject != null)
            {
                parametro.TypeObject = this.TypeObject;
                if (this.TypeObject == 1)
                    parametro.Mantenible = (Vehiculo)this.Mantenible.Clone();
                else if (this.TypeObject == 2)
                    parametro.Mantenible = (Caja)this.Mantenible.Clone();

            }
            if (this.ParametroServicio != null)
            {
                parametro.ParametroServicio = this.ParametroServicio;
            }
            //if (this.VehiculoMantenimiento != null)
            //    parametro.VehiculoMantenimiento = this.VehiculoMantenimiento;
            //if (this.CajaMantenimiento != null)
            //    parametro.CajaMantenimiento = this.CajaMantenimiento;
            return parametro;
        }
    }
}
