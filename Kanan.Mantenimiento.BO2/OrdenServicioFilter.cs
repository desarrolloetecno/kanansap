﻿using System;

namespace Kanan.Mantenimiento.BO2
{
    [Serializable]
    public class OrdenServicioFilter:OrdenServicio
    {
        public DateTime? FechaFinFilter { get; set; }
    }
}
