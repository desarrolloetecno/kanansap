﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kanan.Mantenimiento.BO2
{
    public class PartialAlertsOrdendto
    {
        public int? Total { get; set; }
        public List<OrdenServicio> Ordenes { get; set; }
        
        public PartialAlertsOrdendto()
        {
            Ordenes = new List<OrdenServicio>();
        }

    }
}
