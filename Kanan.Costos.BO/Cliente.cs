﻿using System;
using System.Runtime.Serialization;

namespace Kanan.Costos.BO2
{
    [Serializable]
    [DataContract]
    public class Cliente:ICloneable
    {
        /// <summary>
        /// ClienteID, identificador del cliente en kananfleet.
        /// </summary>
        [DataMember]
        public int? ClienteID { get; set; }
        /// <summary>
        /// CardCode (15 carácteres), identificador de cliente en SAP.
        /// </summary>
        [DataMember]
        public string CardCode { get; set; }
        /// <summary>
        /// CardName (100 carácteres), nombre de cliente SAP.
        /// </summary>
        [DataMember]
        public string Nombre { get; set; }

        [DataMember]
        public string sClienteID { get; set; }
        /// <summary>
        /// CardFName (100 carácteres), nombre extrajero de cliente SAP.
        /// </summary>
        [DataMember]
        public string NombreExtranjero { get; set; }
        /// <summary>
        /// LicTradNum (32 carácteres), RFC de cliente SAP.
        /// </summary>
        [DataMember]
        public string RFC { get; set; }
        /// <summary>
        /// Phone1 (20 carácteres), teléfono 1 de cliente SAP.
        /// </summary>
        [DataMember]
        public string Telefono { get; set; }
        /// <summary>
        /// Cellular (50 carácteres), teléfono móvil de cliente SAP.
        /// </summary>
        [DataMember]
        public string TelefonoMovil { get; set; }
        /// <summary>
        /// E_Mail (100 carácteres), correo electrónico de cliente SAP.
        /// </summary>
        [DataMember]
        public string Correo { get; set; }
        /// <summary>
        /// EmpresaID, corresponde a la empresa kananfleet ligada a la sociedad.
        /// </summary>
        [DataMember]
        public int? EmpresaID { get; set; }
        /// <summary>
        /// EsActivo (carácter únicos: 1, 0), indica si el cliente es "visible" o no.
        /// </summary>
        [DataMember]
        public bool EsActivo { get; set; }

        public Cliente()
        {
            this.EsActivo = true;
        }

        /// <summary>
        /// Método encargado de clonar el objeto cliente.
        /// </summary>
        public object Clone()
        {
            Cliente cliente = new Cliente();
            cliente.ClienteID = this.ClienteID;
            cliente.CardCode = this.CardCode;
            cliente.Nombre = this.Nombre;
            cliente.NombreExtranjero = this.NombreExtranjero;
            cliente.RFC = this.RFC;
            cliente.Telefono = this.Telefono;
            cliente.TelefonoMovil = this.TelefonoMovil;
            cliente.Correo = this.Correo;
            cliente.EmpresaID = this.EmpresaID;
            cliente.EsActivo = this.EsActivo;
            return cliente;
        }
    }
}
