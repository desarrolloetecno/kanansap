﻿using System;
using System.Runtime.Serialization;

namespace Kanan.Costos.BO2
{
    [Serializable]
    [DataContract]
    public class Proveedor:ICloneable
    {
        [DataMember]
        public int? ProveedorID { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public string Direccion { get; set; }
        [DataMember]
        public string Telefono { get; set; }
        [DataMember]
        public string Contactos { get; set; }
        [DataMember]
        public string Correos { get; set; }
        /// <summary>
        /// Clave unica del proveedor
        /// </summary>
        [DataMember]
        public string CUP { get; set; }

        public object Clone()
        {
            Proveedor proveedor = new Proveedor();
            proveedor.ProveedorID = this.ProveedorID;
            proveedor.Nombre = this.Nombre;
            proveedor.Telefono = this.Telefono;
            proveedor.Direccion = this.Direccion;
            proveedor.Contactos = this.Contactos;
            proveedor.Correos = this.Correos;
            proveedor.CUP = this.CUP;
            return proveedor;
        }
    }
}
