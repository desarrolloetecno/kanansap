﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kanan.Mantenimiento.BO2;
using Kanan.Vehiculos.BO2;
using SAPbobsCOM;
using KananSAP.Vehiculos.Data;


namespace KananSAP.Mantenimiento.Data
{
    public class ParametroMantenimientoData
    {
        #region Atributos
        private const string TABLE_MANTENIMIENTO = "VSKF_PMTROMTTO";
        public ParametroMantenimiento oParametroMantenimiento { get; set; }
        public bool isTypeVehicle { get; set; }
        public UserTable oUDT { get; set; }
        private Company oCompany;
        public string ItemCode { get; set; }
        
        private ParametroServicioData ParametroServicioData { get; set; }
        private ServicioSAP ServicioSAP { get; set; }

        //Migracion Hana
        Recordset Insertar;
        String Parametro;
        private String NombreTabla;
        bool EsSQL;

        //Se agregó para ser visuble desde cualquier parte
        public String NombreTablaParametroMantenimiento = String.Empty;

        #endregion

        #region Contructor
        public ParametroMantenimientoData(Company company)
        {
            this.oCompany = company;
            try
            {
                this.oUDT = this.oCompany.UserTables.Item(TABLE_MANTENIMIENTO);

                this.ParametroServicioData = new ParametroServicioData(this.oCompany);
                this.ServicioSAP = new ServicioSAP(this.oCompany);

                //Migración HANA
                this.Insertar = (Recordset)this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.EsSQL = oCompany.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.NombreTabla = EsSQL ? "[@VSKF_PMTROMTTO]" : @"""@VSKF_PMTROMTTO""";

                //Se le asigna el mismo nombre al nomnbre de la tabla pública
                this.NombreTablaParametroMantenimiento = this.NombreTabla;
            }
            catch (Exception ex)
            {
                this.oUDT = null;
            }
        }
        #endregion

        /// <summary>
        /// Verifica si existe el registro por medio del Code
        /// </summary>
        /// <returns></returns>
        public bool Existe()
        {
            return this.oUDT.GetByKey(this.ItemCode);
        }

        /// <summary>
        /// Asigna los valores del parametro de mantenimiento al objeto oUDT
        /// </summary>
        private void PrepareUserTable()
        {
            try
            {
                this.oUDT.Code = this.oParametroMantenimiento.ParametroMantenimientoID.ToString() ?? this.ItemCode;
                this.oUDT.Name = this.oParametroMantenimiento.ParametroMantenimientoID.ToString() ?? this.ItemCode;
                this.oUDT.UserFields.Fields.Item("U_PMTOID").Value = this.oParametroMantenimiento.ParametroMantenimientoID ?? 6;
                this.oUDT.UserFields.Fields.Item("U_ULTSERV").Value = this.oParametroMantenimiento.UltimoServicio ?? 0;
                this.oUDT.UserFields.Fields.Item("U_ProxServ").Value = this.oParametroMantenimiento.ProximoServicio ?? 0;
                this.oUDT.UserFields.Fields.Item("U_AlProxServ").Value = this.oParametroMantenimiento.AlertaProximoServicio ?? 0;
                this.oUDT.UserFields.Fields.Item("U_ServicioID").Value = this.oParametroMantenimiento.ParametroServicio.Servicio.ServicioID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_MantbleID").Value = this.oParametroMantenimiento.Mantenible.MantenibleID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_UlAlEnviad").Value = this.oParametroMantenimiento.UltimaAlertaEnviada ?? Convert.ToDateTime("01/01/1990");
                this.oUDT.UserFields.Fields.Item("U_PServID").Value = this.oParametroMantenimiento.ParametroServicio.ParametroServicioID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_UlSvTiem").Value = this.oParametroMantenimiento.UltimoServicioTiempo ?? Convert.ToDateTime("01/01/1990");
                this.oUDT.UserFields.Fields.Item("U_ProxSvTiem").Value = this.oParametroMantenimiento.ProximoServicioTiempo ?? Convert.ToDateTime("01/01/1990");
                this.oUDT.UserFields.Fields.Item("U_AlProSvTie").Value = this.oParametroMantenimiento.AlertaProximoServicioTiempo ?? Convert.ToDateTime("01/01/1990");
                if (oParametroMantenimiento.Mantenible != null)
                {
                    int? tipoMant = null;
                    if (oParametroMantenimiento.Mantenible.GetType() == typeof(Vehiculo))
                        tipoMant = 1;
                    else if (oParametroMantenimiento.Mantenible.GetType() == typeof(Caja))
                        tipoMant = 2;
                    else if (oParametroMantenimiento.Mantenible.GetType() == typeof(TipoVehiculo))
                        tipoMant = 3;
                    if (tipoMant != null)
                        this.oUDT.UserFields.Fields.Item("U_TMantbleID").Value = tipoMant;
                }
                this.oUDT.UserFields.Fields.Item("U_TipoPmtro").Value = oParametroMantenimiento.TipoParametro ?? 0;
                this.oUDT.UserFields.Fields.Item("U_Rutina").Value = oParametroMantenimiento.PlantillaID ?? 0;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Hace una llamada al metodo PrepareUserTable que se encarga de asignar los valores
        /// al UserTable para guardar o modificar en la base de datos
        /// </summary>
        public void ParametroMantenimientoToUDT()
        {
            ValidateData();
            PrepareUserTable();
            //this.oUDT = this.oCompany.UserTables.Item(TABLE_MANTENIMIENTO);
            //if (this.oParametroMantenimiento.ParametroMantenimientoID == null)
            //    this.oUDT.Code = "4";
            //if (this.oParametroMantenimiento.ParametroMantenimientoID == null)
            //    this.oUDT.Name = "4";
            //if (this.oParametroMantenimiento.ParametroMantenimientoID != null)
            //    this.oUDT.UserFields.Fields.Item("U_PMtoID").Value = this.oParametroMantenimiento.ParametroMantenimientoID;
            //if (this.oParametroMantenimiento.UltimoServicio != null)
            //    this.oUDT.UserFields.Fields.Item("U_UltServ").Value = this.oParametroMantenimiento.UltimoServicio;
            //if (this.oParametroMantenimiento.ProximoServicio != null)
            //    this.oUDT.UserFields.Fields.Item("U_ProxServ").Value = this.oParametroMantenimiento.ProximoServicio;
            //if (this.oParametroMantenimiento.AlertaProximoServicio != null)
            //    this.oUDT.UserFields.Fields.Item("U_AlProxServ").Value = this.oParametroMantenimiento.AlertaProximoServicio;
            //if (this.oParametroMantenimiento.ParametroServicio.Servicio.ServicioID != null)
            //    this.oUDT.UserFields.Fields.Item("U_ServicioID").Value = this.oParametroMantenimiento.ParametroServicio.Servicio.ServicioID;
            //this.oUDT.UserFields.Fields.Item("U_MantbleID").Value = this.oParametroMantenimiento.Mantenible.MantenibleID;
            //if (this.oParametroMantenimiento.UltimaAlertaEnviada != null)
            //    this.oUDT.UserFields.Fields.Item("U_UlAlEnviad").Value = this.oParametroMantenimiento.UltimaAlertaEnviada;
            //if (this.oParametroMantenimiento.ParametroServicio.ParametroServicioID != null)
            //    this.oUDT.UserFields.Fields.Item("U_PServID").Value = this.oParametroMantenimiento.ParametroServicio.ParametroServicioID;
            //if (this.oParametroMantenimiento.UltimoServicioTiempo != null)
            //    this.oUDT.UserFields.Fields.Item("U_UlSvTiem").Value = this.oParametroMantenimiento.UltimoServicioTiempo;
            //if (this.oParametroMantenimiento.ProximoServicioTiempo != null)
            //    this.oUDT.UserFields.Fields.Item("U_ProxSvTiem").Value = this.oParametroMantenimiento.ProximoServicioTiempo;
            //if (this.oParametroMantenimiento.AlertaProximoServicioTiempo != null)
            //    this.oUDT.UserFields.Fields.Item("U_AlProSvTie").Value = this.oParametroMantenimiento.AlertaProximoServicioTiempo;

            //if (oParametroMantenimiento.Mantenible != null)
            //{
            //    int? tipoMant = null;
            //    if (oParametroMantenimiento.Mantenible.GetType() == typeof(Vehiculo))
            //        tipoMant = 1;
            //    else if (oParametroMantenimiento.Mantenible.GetType() == typeof(Caja))
            //        tipoMant = 2;
            //    else if (oParametroMantenimiento.Mantenible.GetType() == typeof(TipoVehiculo))
            //        tipoMant = 3;
            //    if (tipoMant != null)
            //        this.oUDT.UserFields.Fields.Item("U_TMantbleID").Value = tipoMant;
            //}
            //if (this.oParametroMantenimiento.TipoParametro != null)
            //    this.oUDT.UserFields.Fields.Item("U_TipoPmtro").Value = oParametroMantenimiento.TipoParametro;
            
        }

        /// <summary>
        /// Valida que los campos obligatorios contengan informacion
        /// </summary>
        private void ValidateData()
        {
            try
            {
                if (this.oParametroMantenimiento == null) throw new Exception("ParametroMantenimiento no puede ser nulo");
                if (this.oParametroMantenimiento.Mantenible == null) throw new Exception("Mantenimiento no puede ser nulo");
                if (this.oParametroMantenimiento.Mantenible.MantenibleID == null) throw new Exception("MantenibleID no puede ser nulo");
                if (this.oParametroMantenimiento.ParametroServicio.Servicio == null) throw new Exception("Servicio no puede ser nulo");
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Guarda las configuraciones de los parámetros de mantenimiento
        /// </summary>
        public void Insert()
        {
            int result = -1;
            try
            {
                String TMantenibleID = String.Empty;
                TMantenibleID = this.oParametroMantenimiento.Mantenible.GetType() == typeof(Vehiculo) ? TMantenibleID = "1" :
                    this.oParametroMantenimiento.Mantenible.GetType() == typeof(Caja) ? TMantenibleID = "2" :
                    this.oParametroMantenimiento.Mantenible.GetType() == typeof(TipoVehiculo) ? TMantenibleID = "3" : TMantenibleID = String.Empty;
                if (String.IsNullOrEmpty(TMantenibleID))
                    throw new Exception("Error al intentar obtener tipo de vehículo. ");
                DateTime UltimaAlertaEnviada = this.oParametroMantenimiento.UltimaAlertaEnviada != null ? this.oParametroMantenimiento.UltimaAlertaEnviada.Value : Convert.ToDateTime("2000/01/01");

                DateTime ProximoServicioTiempo = this.oParametroMantenimiento.ProximoServicioTiempo != null ? this.oParametroMantenimiento.ProximoServicioTiempo.Value : Convert.ToDateTime("2000/01/01");

                DateTime AlertaProximoServicioTiempo = this.oParametroMantenimiento.AlertaProximoServicioTiempo != null ? this.oParametroMantenimiento.AlertaProximoServicioTiempo.Value : Convert.ToDateTime("2000/01/01");

                DateTime UltimoServicioTiempo = this.oParametroMantenimiento.UltimoServicioTiempo != null ? this.oParametroMantenimiento.UltimoServicioTiempo.Value : Convert.ToDateTime("2000/01/01");

                #region SQL || HANA
                //this.Parametro = String.Empty;
                //this.Parametro = String.Format("INSERT INTO {0} VALUES('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', '{19}', '{20}', '{21}', '{22}')", this.NombreTabla, this.oParametroMantenimiento.ParametroMantenimientoID, this.oParametroMantenimiento.ParametroMantenimientoID, this.oParametroMantenimiento.ParametroMantenimientoID, Convert.ToInt32(this.oParametroMantenimiento.UltimoServicio), Convert.ToInt32(this.oParametroMantenimiento.ProximoServicio), Convert.ToInt32(this.oParametroMantenimiento.AlertaProximoServicio), this.oParametroMantenimiento.ParametroServicio.Servicio.ServicioID, this.oParametroMantenimiento.Mantenible.MantenibleID, UltimaAlertaEnviada.ToString("yyyy/MM/dd"), this.oParametroMantenimiento.ParametroServicio.ParametroServicioID, UltimoServicioTiempo.ToString("yyyy/MM/dd"), ProximoServicioTiempo.ToString("yyyy/MM/dd"), AlertaProximoServicioTiempo.ToString("yyyy/MM/dd"), TMantenibleID, this.oParametroMantenimiento.TipoParametro, "0", null, this.oParametroMantenimiento.ValorHora, this.oParametroMantenimiento.AlertaHora, this.oParametroMantenimiento.UltimoServicioHora, this.oParametroMantenimiento.ProximoServicioHora, this.oParametroMantenimiento.AlertaProximoServicioHora);
                //this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || HANA

                #region SQL
                result = this.oUDT.Add();
                if (result != 0)
                {
                    int errornum;
                    string errormsj;
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar insertar parámetro mantenimiento. " + ex.Message);
            }
        }

        /// <summary>
        /// Actualiza el registro en la base de datos
        /// </summary>
        public void Actualizar()
        {
            //int result = -1;
            try
            {
                #region Comentado
                /*String TMantenibleID = String.Empty;
                TMantenibleID = this.oParametroMantenimiento.Mantenible.GetType() == typeof(Vehiculo) ? TMantenibleID = "1" :
                    this.oParametroMantenimiento.Mantenible.GetType() == typeof(Caja) ? TMantenibleID = "2" :
                    this.oParametroMantenimiento.Mantenible.GetType() == typeof(TipoVehiculo) ? TMantenibleID = "3" : TMantenibleID = String.Empty;
                if (String.IsNullOrEmpty(TMantenibleID))
                    throw new Exception("Error al intentar obtener tipo de vehículo. ");

                DateTime UltimaAlertaEnviada = this.oParametroMantenimiento.UltimaAlertaEnviada != null ? this.oParametroMantenimiento.UltimaAlertaEnviada.Value : Convert.ToDateTime("2000/01/01");*/
                #endregion Comentado
                String TMantenibleID = String.Empty;
                TMantenibleID = this.oParametroMantenimiento.Mantenible.GetType() == typeof(Vehiculo) ? TMantenibleID = "1" :
                    this.oParametroMantenimiento.Mantenible.GetType() == typeof(Caja) ? TMantenibleID = "2" :
                    this.oParametroMantenimiento.Mantenible.GetType() == typeof(TipoVehiculo) ? TMantenibleID = "3" : TMantenibleID = String.Empty;
                if (String.IsNullOrEmpty(TMantenibleID))
                    throw new Exception("Error al intentar obtener tipo de vehículo. ");
                DateTime UltimaAlertaEnviada = this.oParametroMantenimiento.UltimaAlertaEnviada != null ? this.oParametroMantenimiento.UltimaAlertaEnviada.Value : Convert.ToDateTime("2000/01/01");

                DateTime ProximoServicioTiempo = this.oParametroMantenimiento.ProximoServicioTiempo != null ? this.oParametroMantenimiento.ProximoServicioTiempo.Value : Convert.ToDateTime("2000/01/01");

                DateTime AlertaProximoServicioTiempo = this.oParametroMantenimiento.AlertaProximoServicioTiempo != null ? this.oParametroMantenimiento.AlertaProximoServicioTiempo.Value : Convert.ToDateTime("2000/01/01");

                DateTime UltimoServicioTiempo = this.oParametroMantenimiento.UltimoServicioTiempo != null ? this.oParametroMantenimiento.UltimoServicioTiempo.Value : Convert.ToDateTime("2000/01/01");

                #region SQL || HANA
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET ""Code"" = '{1}', ""Name"" = '{2}', U_PMtoID = '{3}', ""U_ULTSERV"" = '{4}', U_ProxServ = '{5}', U_AlProxServ = '{6}', U_ServicioID = '{7}', U_MantbleID = '{8}', U_UlAlEnviad = '{9}', U_PServID = '{10}', U_UlSvTiem = '{11}', U_ProxSvTiem = '{12}', U_AlProSvTie = '{13}', U_TMantbleID = '{14}', U_TipoPmtro = '{15}', U_ValHora = '{18}', U_AlHora = '{19}' , U_UltServHora = '{20}', U_ProxServHora = '{21}', U_AlProxServHora = '{22}' WHERE Code = '{1}'", this.NombreTabla, this.oParametroMantenimiento.ParametroMantenimientoID, this.oParametroMantenimiento.ParametroMantenimientoID, this.oParametroMantenimiento.ParametroMantenimientoID, Convert.ToInt32(this.oParametroMantenimiento.UltimoServicio), Convert.ToInt32(this.oParametroMantenimiento.ProximoServicio), Convert.ToInt32(this.oParametroMantenimiento.AlertaProximoServicio), this.oParametroMantenimiento.ParametroServicio.Servicio.ServicioID, this.oParametroMantenimiento.Mantenible.MantenibleID, UltimaAlertaEnviada.ToString("yyyy/MM/dd"), this.oParametroMantenimiento.ParametroServicio.ParametroServicioID, UltimoServicioTiempo.ToString("yyyy/MM/dd"), ProximoServicioTiempo.ToString("yyyy/MM/dd"), AlertaProximoServicioTiempo.ToString("yyyy/MM/dd"), TMantenibleID, this.oParametroMantenimiento.TipoParametro, "0", null, this.oParametroMantenimiento.ValorHora, this.oParametroMantenimiento.AlertaHora, this.oParametroMantenimiento.UltimoServicioHora, this.oParametroMantenimiento.ProximoServicioHora, this.oParametroMantenimiento.AlertaProximoServicioHora);
                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || HANA

                #region SQL
                /*int errornum;
                string errormsj;
                result = this.oUDT.Update();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }*/
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar()
        {
            //int result = -1;
            try
            {
                String ID = String.Empty;
                ID = !String.IsNullOrEmpty(oUDT.Code) ? ID = oUDT.Code : ID = String.Empty;
                if (String.IsNullOrEmpty(ID))
                    throw new Exception("Error al intentar eliminar, no se encontró identificador. ");


                #region SQL || HANA
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code"" = '{1}'", this.NombreTabla, ID);
                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || HANA

                #region SQL
                /*int errornum;
                string errormsj;
                result = this.oUDT.Remove();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }*/
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Realiza una consulta de acuerdo al parametro de mantenimiento proporcionado
        /// </summary>
        /// <param name="pm"></param>
        /// <returns></returns>
        public Recordset Consultar(ParametroMantenimiento pm)
        {
            Recordset rs = null;
            try
            {
                if (pm == null) pm = new ParametroMantenimiento();
                if (pm.ParametroServicio == null) pm.ParametroServicio = new ParametroServicio();
                if (pm.ParametroServicio.Servicio == null) pm.ParametroServicio.Servicio = new Servicio();

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                //query.Append(" select pm.U_PMtoID,sv.U_Nombre as Servicio,sv.U_ServicioID,ps.U_Valor as Parametro,pm.U_ProxServHora,pm.U_ValHora,  "); U_ValHora,U_AlHora,U_UltServHora,U_ProxServHora,U_AlProxServHora
                query.Append(@" select pm.""U_PMTOID"",sv.U_Nombre as Servicio,sv.U_ServicioID,ps.U_Valor as Parametro,pm.U_ValHora,pm.U_AlHora,pm.U_UltServHora,pm.U_ProxServHora,pm.U_AlProxServHora,  ");
                query.Append(" ps.U_Alerta, ps.U_ValTiempo, ps.U_AlTiempo, pm.U_UltServ,pm.U_ProxServ as Proximo,pm.U_AlProxServ as ProximaAlerta,pm.U_UlSvTiem, ");
                query.Append(" pm.U_ProxSvTiem,pm.U_AlProSvTie,pm.U_MantbleID,pm.U_TMantbleID,pm.U_UlAlEnviad,pm.U_PServID,pm.U_TipoPmtro ");
                query.Append(" from " + this.NombreTabla + " as pm inner join " + this.ParametroServicioData.NombreTablaParametroServicio + " as ps on pm.U_PServID = ps.U_PServID inner join " + this.ServicioSAP.NombreTablaServicioSAP + " as sv on ps.U_ServicioID = sv.U_ServicioID ");

                if (pm.ParametroMantenimientoID != null)
                    where.Append(string.Format(@"and pm.""U_PMTOID"" = {0} ", pm.ParametroMantenimientoID));
                if(pm.ParametroServicio.ParametroServicioID != null)
                    where.Append(string.Format("and pm.U_PServID = {0} ", pm.ParametroServicio.ParametroServicioID));
                if(pm.ParametroServicio.Servicio.ServicioID != null)
                    where.Append(string.Format("and pm.U_ServicioID = {0} ", pm.ParametroServicio.Servicio.ServicioID));
                if(pm.UltimoServicio != null)
                    where.Append(string.Format(@"and pm.""U_ULTSERV"" = {0} ", pm.UltimoServicio));
                if(pm.ProximoServicio != null)
                    where.Append(string.Format("and pm.U_ProxServ = {0} ", pm.ProximoServicio));
                if(pm.AlertaProximoServicio != null)
                    where.Append(string.Format("and pm.U_AlProxServ = {0} ", pm.AlertaProximoServicio));
                if(pm.UltimoServicioTiempo != null)
                    where.Append(string.Format("and pm.U_UlSvTiem = {0} ", pm.UltimoServicioTiempo));
                if(pm.ProximoServicioTiempo != null)
                    where.Append(string.Format("and pm.U_ProxSvTiem = {0} ", pm.ProximoServicioTiempo));
                if(pm.AlertaProximoServicioTiempo != null)
                    where.Append(string.Format("and pm.U_AlProSvTie = {0} ", pm.AlertaProximoServicioTiempo));
                if(pm.UltimaAlertaEnviada != null)
                    where.Append(string.Format("and pm.U_UlAlEnviad = '{0}' ", pm.UltimaAlertaEnviada));
                if(pm.TipoParametro != null)
                    where.Append(string.Format("and pm.U_TipoPmtro = {0} ", pm.TipoParametro));
                if (!string.IsNullOrEmpty(pm.ParametroServicio.Servicio.Nombre))
                    where.Append(string.Format("and sv.U_Nombre like '%{0}%' ", pm.ParametroServicio.Servicio.Nombre));
                if(pm.Mantenible != null)
                {
                    if(pm.Mantenible.GetType() == typeof(Vehiculo))
                        where.Append(string.Format("and pm.U_TMantbleID = {0} ", 1));
                    if (pm.Mantenible.MantenibleID != null)
                        where.Append(string.Format("and pm.U_MantbleID = {0} ", pm.Mantenible.MantenibleID));
                }
                if(where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith("and"))
                        filtro = filtro.Substring(3);
                    query.Append(" where " + filtro);
                }

                query.Append(" order by pm.U_PMtoID asc ");
                rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ParametroMantenimiento ConsultarCompleto(int? parametroMantenimientoID)
        {
            try
            {
                ParametroMantenimiento pm = new ParametroMantenimiento() { ParametroMantenimientoID = parametroMantenimientoID };
                Recordset rs = this.Consultar(pm);
                if(this.HasParametroMantenimiento(rs))
                {
                    pm = this.LastRecordSetToParametroMantenimiento(rs);
                    ParametroServicioData psData = new ParametroServicioData(this.oCompany);
                    rs = psData.Consultar(pm.ParametroServicio);
                    if(psData.HashParametroServicio(rs))
                    {
                        pm.ParametroServicio = psData.LastRecordSetToParametroServicio(rs);
                    }
                    else if(pm.Mantenible.GetType() == typeof(Vehiculo))
                    {
                        VehiculoSAP vhData = new VehiculoSAP(this.oCompany);
                        if(vhData.SetUDTByVehiculoID(Convert.ToInt32(pm.Mantenible.MantenibleID)))
                        {
                            vhData.UDTToVehiculo();
                            pm.Mantenible = vhData.vehiculo;
                        }
                    }
                    //else if Actuvos
                }
                return pm;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        /// <summary>
        /// Obtiene la ultima fila un recordset y lo convierte en un objeto parametro de mantenimiento
        /// </summary>
        /// <param name="rs"></param>
        /// <returns></returns>
        public ParametroMantenimiento LastRecordSetToParametroMantenimiento(Recordset rs)
        {
            try
            {
                ParametroMantenimiento pm =  new ParametroMantenimiento();
                pm.ParametroServicio = new ParametroServicio();
                pm.ParametroServicio.Servicio = new Servicio();
                rs.MoveFirst();
                if (rs.Fields.Item("U_PMTOID").Value != null || rs.Fields.Item("U_PMTOID").Value > 0)
                    pm.ParametroMantenimientoID = Convert.ToInt32(rs.Fields.Item("U_PMTOID").Value);
                if (rs.Fields.Item("Servicio").Value != null)
                    pm.ParametroServicio.Servicio.Nombre = rs.Fields.Item("Servicio").Value;
                if (rs.Fields.Item("U_ServicioID").Value != null || rs.Fields.Item("U_ServicioID").Value > 0)
                    pm.ParametroServicio.Servicio.ServicioID = Convert.ToInt32(rs.Fields.Item("U_ServicioID").Value);
                if (rs.Fields.Item("Parametro").Value != null || rs.Fields.Item("Parametro").Value > 0)
                    pm.ParametroServicio.Valor = Convert.ToDouble(rs.Fields.Item("Parametro").Value);
                if (rs.Fields.Item("U_Alerta").Value != null || rs.Fields.Item("U_Alerta").Value > 0)
                    pm.ParametroServicio.Alerta = Convert.ToDouble(rs.Fields.Item("U_Alerta").Value);
                if (rs.Fields.Item("U_ValTiempo").Value != null || rs.Fields.Item("U_ValTiempo").Value > 0)
                    pm.ParametroServicio.ValorTiempo = Convert.ToInt32(rs.Fields.Item("U_ValTiempo").Value);
                if (rs.Fields.Item("U_AlTiempo").Value != null || rs.Fields.Item("U_AlTiempo").Value > 0)
                    pm.ParametroServicio.AlertaTiempo = Convert.ToInt32(rs.Fields.Item("U_AlTiempo").Value);
                if (rs.Fields.Item("U_ULTSERV").Value != null || rs.Fields.Item("U_ULTSERV").Value > 0)
                    pm.UltimoServicio = Convert.ToDouble(rs.Fields.Item("U_ULTSERV").Value);
                //-----
                if (rs.Fields.Item("U_UltServ").Value != null || rs.Fields.Item("U_ValHora").Value > 0)
                    pm.ValorHora = Convert.ToDouble(rs.Fields.Item("U_ValHora").Value);

                if (rs.Fields.Item("U_AlHora").Value != null || rs.Fields.Item("U_AlHora").Value > 0)
                    pm.AlertaHora = Convert.ToDouble(rs.Fields.Item("U_AlHora").Value);

                if (rs.Fields.Item("U_UltServHora").Value != null || rs.Fields.Item("U_UltServHora").Value > 0)
                    pm.UltimoServicioHora = Convert.ToDouble(rs.Fields.Item("U_UltServHora").Value);

                if (rs.Fields.Item("U_ProxServHora").Value != null || rs.Fields.Item("U_ProxServHora").Value > 0)
                    pm.ProximoServicioHora = Convert.ToDouble(rs.Fields.Item("U_ProxServHora").Value);

                if (rs.Fields.Item("U_AlProxServHora").Value != null || rs.Fields.Item("U_AlProxServHora").Value > 0)
                    pm.AlertaProximoServicioHora = Convert.ToDouble(rs.Fields.Item("U_AlProxServHora").Value);
                //-----
                if (rs.Fields.Item("Proximo").Value != null || rs.Fields.Item("Proximo").Value > 0)
                    pm.ProximoServicio = Convert.ToDouble(rs.Fields.Item("Proximo").Value);
                if (rs.Fields.Item("ProximaAlerta").Value != null || rs.Fields.Item("ProximaAlerta").Value > 0)
                    pm.AlertaProximoServicio = Convert.ToDouble(rs.Fields.Item("ProximaAlerta").Value);
                if (rs.Fields.Item("U_UlSvTiem").Value != null || rs.Fields.Item("U_UlSvTiem").Value > 0)
                    pm.UltimoServicioTiempo = Convert.ToDateTime(rs.Fields.Item("U_UlSvTiem").Value);
                if (rs.Fields.Item("U_ProxSvTiem").Value != null || rs.Fields.Item("U_ProxSvTiem").Value > 0)
                    pm.ProximoServicioTiempo = Convert.ToDateTime(rs.Fields.Item("U_ProxSvTiem").Value);
                if (rs.Fields.Item("U_AlProSvTie").Value != null || rs.Fields.Item("U_AlProSvTie").Value > 0)
                    pm.AlertaProximoServicioTiempo = Convert.ToDateTime(rs.Fields.Item("U_AlProSvTie").Value);
                if (rs.Fields.Item("U_TMantbleID").Value != null || rs.Fields.Item("U_TMantbleID").Value > 0)
                {
                   if (rs.Fields.Item("U_MantbleID").Value != null || rs.Fields.Item("U_MantbleID").Value > 0)
                   {
                       int tipo = Convert.ToInt32(rs.Fields.Item("U_TMantbleID").Value);
                       if(tipo == 1)
                       {
                           pm.Mantenible = new Vehiculo();
                           pm.Mantenible.MantenibleID = Convert.ToInt32(rs.Fields.Item("U_MantbleID").Value);
                       }
                    }
                 }
                 if (rs.Fields.Item("U_UlAlEnviad").Value != null || rs.Fields.Item("U_UlAlEnviad").Value > 0)
                     pm.UltimaAlertaEnviada = Convert.ToDateTime(rs.Fields.Item("U_UlAlEnviad").Value);
                 if (rs.Fields.Item("U_PServID").Value != null || rs.Fields.Item("U_PServID").Value > 0)
                     pm.ParametroServicio.ParametroServicioID = Convert.ToInt32(rs.Fields.Item("U_PServID").Value);
                 if (rs.Fields.Item("U_TipoPmtro").Value != null || rs.Fields.Item("U_TipoPmtro").Value > 0)
                    pm.TipoParametro = Convert.ToInt32(rs.Fields.Item("U_TipoPmtro").Value);
                return pm;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ParametroMantenimiento RowRecordSetToParametroMantenimiento(Recordset rs)
        {
            try
            {
                ParametroMantenimiento pm = new ParametroMantenimiento();
                pm.ParametroServicio = new ParametroServicio();
                pm.ParametroServicio.Servicio = new Servicio();
                if (rs.Fields.Item("U_PMTOID").Value != null || rs.Fields.Item("U_PMTOID").Value > 0)
                    pm.ParametroMantenimientoID = Convert.ToInt32(rs.Fields.Item("U_PMTOID").Value);
                if (rs.Fields.Item("Servicio").Value != null)
                    pm.ParametroServicio.Servicio.Nombre = rs.Fields.Item("Servicio").Value;
                if (rs.Fields.Item("U_ServicioID").Value != null || rs.Fields.Item("U_ServicioID").Value > 0)
                    pm.ParametroServicio.Servicio.ServicioID = Convert.ToInt32(rs.Fields.Item("U_ServicioID").Value);
                if (rs.Fields.Item("Parametro").Value != null || rs.Fields.Item("Parametro").Value > 0)
                    pm.ParametroServicio.Valor = Convert.ToDouble(rs.Fields.Item("Parametro").Value);
                if (rs.Fields.Item("U_Alerta").Value != null || rs.Fields.Item("U_Alerta").Value > 0)
                    pm.ParametroServicio.Alerta = Convert.ToDouble(rs.Fields.Item("U_Alerta").Value);
                if (rs.Fields.Item("U_ValTiempo").Value != null || rs.Fields.Item("U_ValTiempo").Value > 0)
                    pm.ParametroServicio.ValorTiempo = Convert.ToInt32(rs.Fields.Item("U_ValTiempo").Value);
                if (rs.Fields.Item("U_AlTiempo").Value != null || rs.Fields.Item("U_AlTiempo").Value > 0)
                    pm.ParametroServicio.AlertaTiempo = Convert.ToInt32(rs.Fields.Item("U_AlTiempo").Value);
                if (rs.Fields.Item("U_ULTSERV").Value != null || rs.Fields.Item("U_ULTSERV").Value > 0)
                    pm.UltimoServicio = Convert.ToDouble(rs.Fields.Item("U_ULTSERV").Value);
                if (rs.Fields.Item("Proximo").Value != null || rs.Fields.Item("Proximo").Value > 0)
                    pm.ProximoServicio = Convert.ToDouble(rs.Fields.Item("Proximo").Value);
                if (rs.Fields.Item("ProximaAlerta").Value != null || rs.Fields.Item("ProximaAlerta").Value > 0)
                    pm.AlertaProximoServicio = Convert.ToDouble(rs.Fields.Item("ProximaAlerta").Value);
                if (rs.Fields.Item("U_UlSvTiem").Value != null || rs.Fields.Item("U_UlSvTiem").Value > 0)
                    pm.UltimoServicioTiempo = Convert.ToDateTime(rs.Fields.Item("U_UlSvTiem").Value);
                if (rs.Fields.Item("U_ProxSvTiem").Value != null || rs.Fields.Item("U_ProxSvTiem").Value > 0)
                    pm.ProximoServicioTiempo = Convert.ToDateTime(rs.Fields.Item("U_ProxSvTiem").Value);
                if (rs.Fields.Item("U_AlProSvTie").Value != null || rs.Fields.Item("U_AlProSvTie").Value > 0)
                    pm.AlertaProximoServicioTiempo = Convert.ToDateTime(rs.Fields.Item("U_AlProSvTie").Value);
                if (rs.Fields.Item("U_TMantbleID").Value != null || rs.Fields.Item("U_TMantbleID").Value > 0)
                {
                    if (rs.Fields.Item("U_MantbleID").Value != null || rs.Fields.Item("U_MantbleID").Value > 0)
                    {
                        int tipo = Convert.ToInt32(rs.Fields.Item("U_TMantbleID").Value);
                        if (tipo == 1)
                        {
                            pm.Mantenible = new Vehiculo();
                            pm.Mantenible.MantenibleID = Convert.ToInt32(rs.Fields.Item("U_MantbleID").Value);
                        }
                    }
                }
                if (rs.Fields.Item("U_UlAlEnviad").Value != null || rs.Fields.Item("U_UlAlEnviad").Value > 0)
                    pm.UltimaAlertaEnviada = Convert.ToDateTime(rs.Fields.Item("U_UlAlEnviad").Value);
                if (rs.Fields.Item("U_PServID").Value != null || rs.Fields.Item("U_PServID").Value > 0)
                    pm.ParametroServicio.ParametroServicioID = Convert.ToInt32(rs.Fields.Item("U_PServID").Value);
                if (rs.Fields.Item("U_TipoPmtro").Value != null || rs.Fields.Item("U_TipoPmtro").Value > 0)
                    pm.TipoParametro = Convert.ToInt32(rs.Fields.Item("U_TipoPmtro").Value);
                return pm;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ParametroMantenimiento RecordSetToParametroMantenimiento(Recordset rs)
        {
            try
            {
                ParametroMantenimiento pm = new ParametroMantenimiento();
                pm.ParametroServicio = new ParametroServicio();
                pm.ParametroServicio.Servicio = new Servicio();
                if (rs.Fields.Item("U_PMTOID").Value != null || rs.Fields.Item("U_PMTOID").Value > 0)
                    pm.ParametroMantenimientoID = Convert.ToInt32(rs.Fields.Item("U_PMTOID").Value);
                //if (rs.Fields.Item("Servicio").Value != null)
                    //pm.ParametroServicio.Servicio.Nombre = rs.Fields.Item("Servicio").Value;
                if (rs.Fields.Item("U_ServicioID").Value != null || rs.Fields.Item("U_ServicioID").Value > 0)
                    pm.ParametroServicio.Servicio.ServicioID = Convert.ToInt32(rs.Fields.Item("U_ServicioID").Value);
                //if (rs.Fields.Item("Parametro").Value != null || rs.Fields.Item("Parametro").Value > 0)
                    //pm.ParametroServicio.Valor = Convert.ToDouble(rs.Fields.Item("Parametro").Value);
                //if (rs.Fields.Item("U_Alerta").Value != null || rs.Fields.Item("U_Alerta").Value > 0)
                    //pm.ParametroServicio.Alerta = Convert.ToDouble(rs.Fields.Item("U_Alerta").Value);
                //if (rs.Fields.Item("U_ValTiempo").Value != null || rs.Fields.Item("U_ValTiempo").Value > 0)
                    //pm.ParametroServicio.ValorTiempo = Convert.ToInt32(rs.Fields.Item("U_ValTiempo").Value);
                //if (rs.Fields.Item("U_AlTiempo").Value != null || rs.Fields.Item("U_AlTiempo").Value > 0)
                    //pm.ParametroServicio.AlertaTiempo = Convert.ToInt32(rs.Fields.Item("U_AlTiempo").Value);
                if (rs.Fields.Item("U_ULTSERV").Value != null || rs.Fields.Item("U_ULTSERV").Value > 0)
                    pm.UltimoServicio = Convert.ToDouble(rs.Fields.Item("U_ULTSERV").Value);
                //if (rs.Fields.Item("Proximo").Value != null || rs.Fields.Item("Proximo").Value > 0)
                    //pm.ProximoServicio = Convert.ToDouble(rs.Fields.Item("Proximo").Value);
                //if (rs.Fields.Item("ProximaAlerta").Value != null || rs.Fields.Item("ProximaAlerta").Value > 0)
                    //pm.AlertaProximoServicio = Convert.ToDouble(rs.Fields.Item("ProximaAlerta").Value);
                if (rs.Fields.Item("U_UlSvTiem").Value != null || rs.Fields.Item("U_UlSvTiem").Value > 0)
                    pm.UltimoServicioTiempo = Convert.ToDateTime(rs.Fields.Item("U_UlSvTiem").Value);
                if (rs.Fields.Item("U_ProxSvTiem").Value != null || rs.Fields.Item("U_ProxSvTiem").Value > 0)
                    pm.ProximoServicioTiempo = Convert.ToDateTime(rs.Fields.Item("U_ProxSvTiem").Value);
                if (rs.Fields.Item("U_AlProSvTie").Value != null || rs.Fields.Item("U_AlProSvTie").Value > 0)
                    pm.AlertaProximoServicioTiempo = Convert.ToDateTime(rs.Fields.Item("U_AlProSvTie").Value);
                if (rs.Fields.Item("U_TMantbleID").Value != null || rs.Fields.Item("U_TMantbleID").Value > 0)
                {
                    if (rs.Fields.Item("U_MantbleID").Value != null || rs.Fields.Item("U_MantbleID").Value > 0)
                    {
                        int tipo = Convert.ToInt32(rs.Fields.Item("U_TMantbleID").Value);
                        if (tipo == 1)
                        {
                            pm.Mantenible = new Vehiculo();
                            pm.Mantenible.MantenibleID = Convert.ToInt32(rs.Fields.Item("U_MantbleID").Value);
                        }
                    }
                }
                if (rs.Fields.Item("U_UlAlEnviad").Value != null || rs.Fields.Item("U_UlAlEnviad").Value > 0)
                    pm.UltimaAlertaEnviada = Convert.ToDateTime(rs.Fields.Item("U_UlAlEnviad").Value);
                if (rs.Fields.Item("U_PServID").Value != null || rs.Fields.Item("U_PServID").Value > 0)
                    pm.ParametroServicio.ParametroServicioID = Convert.ToInt32(rs.Fields.Item("U_PServID").Value);
                if (rs.Fields.Item("U_TipoPmtro").Value != null || rs.Fields.Item("U_TipoPmtro").Value > 0)
                    pm.TipoParametro = Convert.ToInt32(rs.Fields.Item("U_TipoPmtro").Value);
                return pm;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<ParametroMantenimiento> RecordSetToList(Recordset rs)
        {
            try
            {
                List<ParametroMantenimiento> lst = new List<ParametroMantenimiento>();
                rs.MoveFirst();
                for(int i = 0; i < rs.RecordCount; i ++)
                {
                    lst.Add(RowRecordSetToParametroMantenimiento(rs));
                    rs.MoveNext();
                }
                return lst;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<ParametroMantenimiento> RecordSetToListOS(Recordset rs)
        {
            try
            {
                List<ParametroMantenimiento> lst = new List<ParametroMantenimiento>();
                rs.MoveFirst();
                for (int i = 0; i < rs.RecordCount; i++)
                {
                    lst.Add(RecordSetToParametroMantenimiento(rs));
                    rs.MoveNext();
                }
                return lst;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Verifica si un recordset contiene informacion de alguna consulta especifica
        /// </summary>
        /// <param name="rs"></param>
        /// <returns></returns>
        public bool HasParametroMantenimiento(Recordset rs)
        {
            if (rs.RecordCount < 1)
                return false;
            return true;
        }

        public void ActualizarPMantenimientoTipoVehiculo()
        {
            try
            {
                String Valor = Convert.ToString(this.oParametroMantenimiento.ParametroServicio.Valor) ?? "0";
                String Alerta = Convert.ToString(this.oParametroMantenimiento.ParametroServicio.Alerta) ?? "0";
                String ParametroServicioID = Convert.ToString(this.oParametroMantenimiento.ParametroServicio.ParametroServicioID) ?? "0";

                #region SQL || HANA
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET ""U_PROXSERV"" = '{1}', ""U_ALPROXSERV"" = '{2}', ""U_PSERVID"" = '{3}' WHERE ""Code"" = '{4}'", this.NombreTabla, Valor, Alerta, ParametroServicioID, oUDT.Code);
                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || HANA

                #region SQL
                /*oUDT.UserFields.Fields.Item("U_ProxServ").Value = this.oParametroMantenimiento.ParametroServicio.Valor ?? 0;
                oUDT.UserFields.Fields.Item("U_AlProxServ").Value = this.oParametroMantenimiento.ParametroServicio.Alerta ?? 0;
                oUDT.UserFields.Fields.Item("U_PServID").Value = this.oParametroMantenimiento.ParametroServicio.ParametroServicioID ?? 0;
                this.Actualizar();*/
                #endregion SQL
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public bool SetUDTPMantenimientoID(int? id)
        {
            try
            {
                var rs = this.ConsultarByID(id);
                if(HasParametroMantenimiento(rs))
                {
                    rs.MoveFirst();
                    int _id = rs.Fields.Item("U_PMTOID").Value;
                    if (_id == id)
                        return this.oUDT.GetByKey(rs.Fields.Item("Code").Value);
                }
                this.oUDT.Code = string.Empty;
                return false;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public Recordset ConsultarByID(int? id)
        {
            try
            {
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(string.Format("Select * from " + this.NombreTabla + @" where ""U_PMTOID"" = {0}", id));
                return rs;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        /// <summary>
        /// Elimina el parametro de mantenimiento y de servicio ligados al vehiculo
        /// </summary>
        public void EliminarCompleto()
        {
            try
            {
                this.ItemCode = this.oParametroMantenimiento.ParametroMantenimientoID.ToString();
                if (this.Existe())
                    this.Eliminar();
                ParametroServicioData psData = new ParametroServicioData(this.oCompany);
                psData.ItemCode = this.oParametroMantenimiento.ParametroServicio.ParametroServicioID.ToString();
                if (psData.Existe())
                    psData.Eliminar();
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
    }
}
