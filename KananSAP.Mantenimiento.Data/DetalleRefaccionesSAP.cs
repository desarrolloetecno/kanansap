﻿using Kanan.Mantenimiento.BO2;
using Kanan.Vehiculos.BO2;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.Mantenimiento.Data
{
    public class DetalleRefaccionesSAP
    {

        #region Atributos
        private const string TABLE_DETALLEREFACCION = "VSKF_DTLLEREFACCION";
        public DetalleRefaccion oDetalleRefaccion { get; set; }
        public UserTable oUDT { get; set; }
        private Company oCompany;
        public int? Sincronizado { get; set; }
        #endregion

        #region Constructor
        public DetalleRefaccionesSAP(Company company)
        {
            this.oCompany = company;
            try
            {
                this.oUDT = this.oCompany.UserTables.Item(TABLE_DETALLEREFACCION);
            }
            catch
            {
                this.oUDT = null;
            }
        }
        public DetalleRefaccionesSAP()
        {
        }
        #endregion

        public string InsertaRefaccion(string sCodigo, DetalleRefaccion oRefaccion, int Sinc)
        {
            string sResponse = "ERROR";
            int iCommand = 0, iCode = 0; 
            this.oUDT.Code = sCodigo;
            this.oUDT.Name = sCodigo;
            this.oUDT.UserFields.Fields.Item("U_RefaccionLinea").Value = oRefaccion.iLineaID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_MantenibleID").Value = oRefaccion.Mantenible.MantenibleID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_ServicioID").Value = oRefaccion.Servicio.ServicioID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_OrdenServicioID").Value = oRefaccion.OrdenServicio.OrdenServicioID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value = oRefaccion.TipoMantenibleID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_Costo").Value = (double?)oRefaccion.Costo ?? 0;
            this.oUDT.UserFields.Fields.Item("U_Articulo").Value = oRefaccion.Articulo;
            this.oUDT.UserFields.Fields.Item("U_Almacen").Value = oRefaccion.Almacen ?? "Almacen general";
            this.oUDT.UserFields.Fields.Item("U_CodAlmcn").Value = oRefaccion.CodigoAlmacen ?? "01";
            this.oUDT.UserFields.Fields.Item("U_Cantidad").Value = (double?)oRefaccion.Cantidad ?? 0;
            this.oUDT.UserFields.Fields.Item("U_Total").Value = (double?)oRefaccion.Total ?? 0;
            this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = Sinc;
            iCommand = this.oUDT.Add();
            if (iCommand != 0)
                this.oCompany.GetLastError(out iCode, out sResponse);
            else sResponse = "OK";
            return sResponse;
        }
        private void InitializeDetalleRefaccion()
        {
            this.oDetalleRefaccion = new DetalleRefaccion();
            this.oDetalleRefaccion.Servicio = new Servicio();
            this.oDetalleRefaccion.OrdenServicio = new OrdenServicio();

        }

        public void UDTTooDetalleOrden()
        {
            try
            {
                this.InitializeDetalleRefaccion();
                int id;
                if (int.TryParse(this.oUDT.Code, out id))
                    this.oDetalleRefaccion.DetalleRefaccionID = id;
                else
                    this.oDetalleRefaccion.DetalleRefaccionID = null;

                this.oDetalleRefaccion.Servicio.ServicioID = this.oUDT.UserFields.Fields.Item("U_ServicioID").Value ?? null;
                this.oDetalleRefaccion.OrdenServicio.OrdenServicioID = this.oUDT.UserFields.Fields.Item("U_OrdenServicioID").Value ?? null;
                this.oDetalleRefaccion.TipoMantenibleID = this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value ?? null;
                if (this.oDetalleRefaccion.TipoMantenibleID != null)
                {
                    if (this.oDetalleRefaccion.TipoMantenibleID == 1)
                        this.oDetalleRefaccion.Mantenible = new Vehiculo();
                    else if (this.oDetalleRefaccion.TipoMantenibleID == 2)
                        this.oDetalleRefaccion.Mantenible = new Caja();
                    else this.oDetalleRefaccion.Mantenible = new Vehiculo();
                    this.oDetalleRefaccion.Mantenible.MantenibleID = this.oUDT.UserFields.Fields.Item("U_MantenibleID").Value ?? null;
                }
                this.oDetalleRefaccion.Almacen = string.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_Almacen").Value) ? "Almacen general" : this.oUDT.UserFields.Fields.Item("U_Almacen").Value;
                this.oDetalleRefaccion.CodigoAlmacen = string.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_CodAlmcn").Value ) ? "01": this.oUDT.UserFields.Fields.Item("U_CodAlmcn").Value;
                this.oDetalleRefaccion.Costo = (decimal?)Convert.ChangeType(this.oUDT.UserFields.Fields.Item("U_Costo").Value, typeof(decimal)) ?? Convert.ToDecimal(0);
                this.Sincronizado = this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value ?? null;
                this.oDetalleRefaccion.iLineaID = this.oUDT.UserFields.Fields.Item("U_RefaccionLinea").Value ?? null;
                this.oDetalleRefaccion.Articulo = this.oUDT.UserFields.Fields.Item("U_Articulo").Value == "" ? "" : Convert.ToString(this.oUDT.UserFields.Fields.Item("U_Articulo").Value);

                this.oDetalleRefaccion.Cantidad = (decimal?)Convert.ChangeType(this.oUDT.UserFields.Fields.Item("U_Cantidad").Value, typeof(decimal)) ?? Convert.ToDecimal(0);
                this.oDetalleRefaccion.Total = (decimal?)Convert.ChangeType(this.oUDT.UserFields.Fields.Item("U_Total").Value, typeof(decimal)) ?? Convert.ToDecimal(0);


            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void LimipiarTabla()
        {
            try
            {
                string query = string.Format(@"DELETE FROM ""@{0}"" where U_OrdenServicioID = {1}", TABLE_DETALLEREFACCION, this.oDetalleRefaccion.OrdenServicio.OrdenServicioID);
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
            }
            catch { }
        }
        public Recordset ConsultarByID(int? id)
        {
            try
            {
                string query = string.Format(@"select * from ""@{0}"" where U_OrdenServicioID = {1}", TABLE_DETALLEREFACCION, id);
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                return rs;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public List<DetalleRefaccion> RecuperaLista()
        {
            var rs = this.ConsultarByID(this.oDetalleRefaccion.OrdenServicio.OrdenServicioID);
            if (rs.RecordCount > 0)
                return this.RecordSetToListRefacciones(rs);

            return null;
        }


        public bool HashoDetalleOrden(Recordset rs)
        {
            if (rs.RecordCount > 0)
                return true;
            return false;
        }

        public void Insert()
        {
            int result = -1;
            try
            {
                result = this.oUDT.Add();
                if (result != 0)
                {
                    int errornum;
                    string errormsj;
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Actualizar()
        {
            int result = -1;
            try
            {
                int errornum;
                string errormsj;
                result = this.oUDT.Update();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar()
        {
            int result = -1;
            try
            {
                int errornum;
                string errormsj;
                result = this.oUDT.Remove();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DetalleRefaccion> RecordSetToListRefacciones(Recordset rs)
        {
            var result = new List<DetalleRefaccion>();
            rs.MoveFirst();
            for (int i = 0; i < rs.RecordCount; i++)
            {
                string itemcode = rs.Fields.Item("Code").Value;
                this.oUDT.GetByKey(itemcode);
                UDTTooDetalleOrden();
                var temp = (DetalleRefaccion)this.oDetalleRefaccion.Clone();
                result.Add(temp);
                rs.MoveNext();
            }
            return result;
        }
    }
}
