﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kanan.Mantenimiento.BO2;
using SAPbobsCOM;
using Kanan.Operaciones.BO2;

//Agregado para ver sucursal y utilizar método de consulta en tabla sucursal DB Hana
using KananSAP.Operaciones.Data;

namespace KananSAP.Mantenimiento.Data
{
    public class ServicioSAP
    {
        #region Atributos
        private const string TABLE_SERVICE_NAME = "VSKF_SERVICIOS";
        public UserTable oUDT { get; set; }
        private Company Company;
        public string ItemCode { get; set; }
        public int? Sincronizado { get; set; }
        public Guid UUID { get; set; }
        private TipoServicioData TipoServicioData { get; set; }
        private SucursalSAP SucursalSAP { get; set; }
        

        public class servicioa
        {
            public Servicio oServicio { get; set; }
        }

        public Servicio oServicio { get; set; }

        //Migracion Hana
        Recordset Insert;
        String Parametro;
        private String NombreTabla;
        bool EsSQL;

        //Se agregó para ser visible desde otros proyectos y poder realizar consultas
        // con un recordser en DB Hana.
        public String NombreTablaServicioSAP = String.Empty;

        #endregion

        #region Constructor
        public ServicioSAP(Company company)
        {
            this.Company = company;
            this.oServicio = new Servicio
            {
                Propietario = new Empresa(),
                SubPropietario = new Sucursal(),
                TipoServicio = new TipoServicio(),
            };
            this.TipoServicioData = new TipoServicioData(this.Company);
            this.SucursalSAP = new SucursalSAP(this.Company);
            try
            {
                this.oUDT = company.UserTables.Item(TABLE_SERVICE_NAME);

                //Migración HANA
                this.Insert = (Recordset)this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.EsSQL = Company.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.NombreTabla = EsSQL ? "[@VSKF_SERVICIOS]" : @"""@VSKF_SERVICIOS""";

                this.NombreTablaServicioSAP = this.NombreTabla;
            }
            catch(Exception ex)
            {
                this.oUDT = null;
            }
        }
        #endregion

        public bool Existe()
        {
            return this.oUDT.GetByKey(this.ItemCode);
        }

        public Recordset GetSAPServicioList()
        {
            Recordset rs = (Recordset)this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            rs.DoQuery(string.Format(@"select * from ""@{0}"" ", TABLE_SERVICE_NAME));
            return rs;
        }

        /// <summary>
        /// Establece los valores de la interfaz de usuario a un objeto SAP UserTable
        /// </summary>
        public void ServicioToUDT()
        {
            try
            {
                if (this.oServicio.SubPropietario == null) this.oServicio.SubPropietario = new Sucursal();
                int? activo = 0;
                if (Convert.ToBoolean(this.oServicio.EsActivo))
                    activo = 1;
                this.oUDT.Code = ItemCode ?? "0"; //this.oServicio.ServicioID.ToString() ?? ItemCode;
                //this.oUDT.Name = this.oServicio.Nombre;
                if (oServicio.Nombre == null)
                {
                    this.oUDT.Name = "";
                    this.oServicio.Nombre = "";
                }
                else
                {
                    this.oUDT.Name = this.oServicio.Nombre.Length > 30 ? oServicio.Nombre.Substring(0, 30) : oServicio.Nombre;
                }
                this.oUDT.UserFields.Fields.Item("U_SERVICIOID").Value = this.oServicio.ServicioID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_NOMBRE").Value = this.oServicio.Nombre == null ? "" : this.oServicio.Nombre;
                this.oUDT.UserFields.Fields.Item("U_DESCRIPCION").Value = this.oServicio.Descripcion ?? string.Empty; ;
                this.oUDT.UserFields.Fields.Item("U_IMAGENURL").Value = this.oServicio.ImagenUrl ?? string.Empty; ;
                this.oUDT.UserFields.Fields.Item("U_SUCURSALID").Value = this.oServicio.SubPropietario.SucursalID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_TIPOSERVICIOID").Value = this.oServicio.TipoServicio.TipoServicioID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_EMPRESAID").Value = this.oServicio.Propietario.EmpresaID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_ESACTIVO").Value = activo;
                this.oUDT.UserFields.Fields.Item("U_SINCRONIZADO").Value = this.Sincronizado ?? 0;
                this.oUDT.UserFields.Fields.Item("U_UUID").Value = this.UUID.ToString() ?? string.Empty;
                this.oUDT.UserFields.Fields.Item("U_CUENTACONTABLE").Value = this.oServicio.CuentaContable != null ? this.oServicio.CuentaContable.Trim() : string.Empty;
                this.oUDT.UserFields.Fields.Item("U_ITEMCODE").Value = this.oServicio.ItemCode ?? "";
                this.oUDT.UserFields.Fields.Item("U_NOMBRECUENTA").Value = this.oServicio.NombreCuenta ?? "";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Insertar()
        {
            try
            {
                String Activo = String.Empty;
                Activo = "0";
                if (this.oServicio.EsActivo == true)
                    Activo = "1";

                #region SQL || HANA
                this.Parametro = String.Empty;
                this.Parametro = String.Format("INSERT INTO {0} VALUES ('{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}')", this.NombreTabla, this.oServicio.ServicioID, this.oServicio.Nombre, this.oServicio.ServicioID, this.oServicio.Nombre, this.oServicio.Descripcion, this.oServicio.ImagenUrl, this.oServicio.SubPropietario.SucursalID, this.oServicio.TipoServicio.TipoServicioID, this.oServicio.Propietario.EmpresaID, Activo, "0", this.oServicio.CuentaContable, this.UUID, this.oServicio.ItemCode, this.oServicio.NombreCuenta);
                this.Insert.DoQuery(this.Parametro);
                #endregion SQL || HANA
            }
            catch
            {
                #region SQL
                int i = oUDT.Add();
                if (i != 0)
                {
                    int errornum;
                    string errormns;
                    Company.GetLastError(out errornum, out errormns);
                    throw new Exception("Error: " + errornum + " - " + errormns);
                }
                #endregion SQL
            }
        }

        public void Actualizar()
        {
            try
            {
                String Activo = String.Empty;
                Activo = "0";
                if (this.oServicio.EsActivo == true)
                    Activo = "1";
                String Sincronizado = String.Empty;
                Sincronizado = String.IsNullOrEmpty(Convert.ToString(this.Sincronizado)) ? Sincronizado = "0" : Sincronizado = Convert.ToString(this.Sincronizado);

                #region SQL || HANA

                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET ""Code""='{1}', ""Name""='{2}', ""U_SERVICIOID""='{3}', ""U_NOMBRE""='{4}', ""U_DESCRIPCION""='{5}', ""U_IMAGENURL""='{6}', ""U_SUCURSALID""='{7}', ""U_TIPOSERVICIOID""='{8}', ""U_EMPRESAID""='{9}', ""U_ESACTIVO""='{10}', U_CuentaContable='{11}', U_ItemCode = '{12}', U_NOMBRECUENTA = '{13}' WHERE ""Code""='{1}'", this.NombreTabla, this.oServicio.ServicioID, this.oServicio.Nombre, this.oServicio.ServicioID, this.oServicio.Nombre, this.oServicio.Descripcion, this.oServicio.ImagenUrl, this.oServicio.SubPropietario.SucursalID, this.oServicio.TipoServicio.TipoServicioID, this.oServicio.Propietario.EmpresaID, Activo, this.oServicio.CuentaContable, this.oServicio.ItemCode, this.oServicio.NombreCuenta);

                this.Insert.DoQuery(this.Parametro);

                #endregion SQL || HANA

                #region SQL
                //SQL
                /*int i = oUDT.Update();
                if (i != 0)
                {
                    int errornum;
                    string errormns;
                    Company.GetLastError(out errornum, out errormns);
                    throw new Exception("Error: " + errornum + " - " + errormns);
                }*/
                #endregion SQL
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar()
        {
            try
            {
                String ID = String.Empty;
                if (!String.IsNullOrEmpty(Convert.ToString(this.oUDT.UserFields.Fields.Item("U_SERVICIOID").Value)))
                    ID = Convert.ToString(this.oUDT.UserFields.Fields.Item("U_SERVICIOID").Value);
                else
                {
                    throw new Exception("Falta identificador de registro.");
                }
                #region SQL || HANA
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code"" = '{1}'", this.NombreTabla, ID);
                this.Insert.DoQuery(this.Parametro);
                #endregion

                #region SQL
                //SQL
                /*int i = oUDT.Remove();
                if (i != 0)
                {
                    int errornum;
                    string errormns;
                    Company.GetLastError(out errornum, out errormns);
                    throw new Exception("Error: " + errornum + " - " + errormns);
                }*/
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar eliminar un registro. " + ex.Message);
            }
        }

        private void EliminarParametroServicio()
        {
            try
            {
                ParametroServicioData psData = new ParametroServicioData(this.Company);
                List<ParametroServicio> lstParametroServicio = new List<ParametroServicio>();
                ParametroServicio ps = new ParametroServicio();
                ps.Servicio = new Servicio() { ServicioID = this.oServicio.ServicioID };
                Recordset rs = psData.Consultar(ps);
                if(psData.HashParametroServicio(rs))
                {
                    lstParametroServicio = psData.RecordSetToList(rs);
                    foreach(ParametroServicio data in lstParametroServicio)
                    {
                        psData.ItemCode = data.ParametroServicioID.ToString();
                        if(psData.Existe())
                        {
                            psData.oParametroServicio = data;
                            psData.ParametroServicioToUDT();
                            psData.EliminarCompleto();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void EliminarCompleto()
        {
            try
            {
                //Se comentó "this.EliminarParametroServicio" debido a que no se inserta un valor al momento de registrar un servicio y por lo tanto impedía
                //eliminar de manera correcta los datos en SAP que a su vez causaba problemas en la los registros de nombres identicos a los eliminados.
                //Rair Santos.
                //this.EliminarParametroServicio();
                this.Deactivate();
                //this.Eliminar();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Recordset Consultar(Servicio sv)
        {
            try
            {
                Recordset rs = this.Consultar(sv, false);
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Recordset ConsultarSoloServicio (Servicio ssv)
        {
            try
            {
                Recordset rs = this.SoloServicio(ssv);
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Recordset SoloServicio(Servicio ssv)
        {
            try
            {
                Recordset rs = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                if (ssv == null) ssv = new Servicio();
                if (ssv.SubPropietario == null) ssv.SubPropietario = new Sucursal();
                if (ssv.Propietario == null) ssv.Propietario = new Empresa();
                if (ssv.TipoServicio == null) ssv.TipoServicio = new TipoServicio();
                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                //query.Append(" SELECT sv.U_ServicioID,sv.U_Nombre as Servicio,sv.U_Descripcion,sv.U_ImagenUrl, ");
                //query.Append(" sv.U_SucursalID,sv.U_TipoServicioID,sv.U_EmpresaID,tps.U_Nombre as TipoServicio ");//, sv.U_Sincronizado, sv.U_UUID ");
                //query.Append(" FROM [@VSKF_SERVICIOS] as sv inner join [@VSKF_TIPOSERVICIO] tps on sv.U_TipoServicioID = tps.U_TipoServicioID ");
                query.Append(" SELECT * ");
                //Se sustituyó para permitir consultar en caso de tener DB Hana.
                query.Append(" FROM " + this.NombreTabla + " ");
                if (ssv.ServicioID != null)
                    where.Append(string.Format(@"and ""U_SERVICIOID"" = {0} ", ssv.ServicioID));
                if (ssv.Nombre != null)
                    where.Append(string.Format(@"and ""U_NOMBRE"" = '{0}' ", ssv.Nombre));
                if (ssv.Descripcion != null)
                    where.Append(string.Format(@"and ""U_DESCRIPCION"" = '{0}' ", ssv.Descripcion));
                if (ssv.ImagenUrl != null)
                    where.Append(string.Format(@"and ""U_IMAGENURL"" = '{0}' ", ssv.ImagenUrl));
                if (ssv.TipoServicio.TipoServicioID != null)
                    where.Append(string.Format(@"and ""U_TIPOSERVICIOID"" = {0} ", ssv.TipoServicio.TipoServicioID));
                if (ssv.Propietario.EmpresaID != null)
                    where.Append(string.Format(@"and ""U_EMPRESAID"" = {0} ", ssv.Propietario.EmpresaID));
                //if (ssv.TipoServicio.Nombre != null)
                    //where.Append(string.Format("and tps.U_Nombre = '{0}' ", ssv.TipoServicio.Nombre));
                if (ssv.EsActivo != null)
                    where.Append(@"and ""U_ESACTIVO"" = 1 ");
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith("and"))
                        filtro = filtro.Substring(3);
                    query.Append(" where " + filtro);
                }
                query.Append(@" order by ""U_SERVICIOID"" desc ");
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Recordset Consultar(Servicio sv, bool? isConsiderable)
        {
            try
            {
                Recordset rs = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                if (sv == null) sv = new Servicio();
                if (sv.SubPropietario == null) sv.SubPropietario = new Sucursal();
                if (sv.Propietario == null) sv.Propietario = new Empresa();
                if(sv.TipoServicio == null) sv.TipoServicio = new TipoServicio();
                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                //query.Append(" SELECT sv.U_ServicioID,sv.U_Nombre as Servicio,sv.U_Descripcion,sv.U_ImagenUrl, ");
                //query.Append(" sv.U_SucursalID,sv.U_TipoServicioID,sv.U_EmpresaID,tps.U_Nombre as TipoServicio ");//, sv.U_Sincronizado, sv.U_UUID ");
                //query.Append(" FROM [@VSKF_SERVICIOS] as sv inner join [@VSKF_TIPOSERVICIO] tps on sv.U_TipoServicioID = tps.U_TipoServicioID ");
                query.Append(@" SELECT sv.""U_SERVICIOID"",sv.""U_NOMBRE"" as Servicio,sv.""U_DESCRIPCION"",sv.""U_ESACTIVO"", sv.""U_EMPRESAID"",sv.""U_SUCURSALID"",sv.""U_IMAGENURL"",sv.""U_TIPOSERVICIOID"", sc.""U_DIRECCION"" as ""Sucursal"",tps.""U_NOMBRE"" as ""TipoServicio"",""U_CUENTACONTABLE"", ""U_ITEMCODE"", ""U_NOMBRECUENTA"" ");
                query.Append(" FROM " + this.NombreTabla + " as sv left outer join " + this.SucursalSAP.NombreTablaSucursalSAP + @" sc on sc.""U_SUCURSALID"" = sv.""U_SUCURSALID"" left outer join " + this.TipoServicioData.NombreTablaTipoServicioData + @" tps on sv.""U_TIPOSERVICIOID"" = tps.""U_TIPOSERVICIOID"" ");
                if (sv.ServicioID != null)
                    where.Append(string.Format(@"and sv.""U_SERVICIOID"" = {0} ", sv.ServicioID));
                if(sv.Nombre != null)
                    where.Append(string.Format(@"and sv.""U_NOMBRE"" = '{0}' ", sv.Nombre));
                if(sv.Descripcion != null)
                    where.Append(string.Format(@"and sv.""U_DESCRIPCION"" = '{0}' ", sv.Descripcion));
                if(sv.ImagenUrl != null)
                    where.Append(string.Format(@"and sv.""U_IMAGENURL"" = '{0}' ", sv.ImagenUrl));
                if (sv.TipoServicio.TipoServicioID != null)
                    where.Append(string.Format(@"and sv.""U_TIPOSERVICIOID"" = {0} ", sv.TipoServicio.TipoServicioID));
                if (sv.Propietario.EmpresaID != null)
                    where.Append(string.Format(@"and sv.""U_EMPRESAID"" = {0} ", sv.Propietario.EmpresaID));
                if(!(bool)isConsiderable)
                {
                    if (sv.SubPropietario.SucursalID != null)
                        where.Append(string.Format(@"and sv.""U_SUCURSALID"" = {0} ", sv.SubPropietario.SucursalID));
                }
                if (sv.TipoServicio.Nombre != null)
                    where.Append(string.Format(@"and tps.""U_NOMBRE"" = '{0}' ", sv.TipoServicio.Nombre));
                if(sv.EsActivo != null)
                    where.Append(@"and sv.""U_ESACTIVO"" = 1 ");
                if(where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith("and"))
                        filtro = filtro.Substring(3);
                    query.Append(" where " + filtro);
                }
                query.Append(@" order by sv.""U_SERVICIOID"" desc ");
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Servicio LastRecordSetToServicio(Recordset rs)
        {
            try
            {
                Servicio sv = new Servicio();
                sv.SubPropietario = new Sucursal();
                sv.Propietario = new Empresa();
                sv.TipoServicio = new TipoServicio();
                rs.MoveFirst();
                if (rs.Fields.Item("U_SERVICIOID").Value != null || rs.Fields.Item("U_SERVICIOID").Value > 0)
                    sv.ServicioID = Convert.ToInt32(rs.Fields.Item("U_SERVICIOID").Value);
                if (rs.Fields.Item("Servicio").Value != null)
                    sv.Nombre = rs.Fields.Item("Servicio").Value;
                if (rs.Fields.Item("U_DESCRIPCION").Value != null)
                    sv.Descripcion = rs.Fields.Item("U_DESCRIPCION").Value;
                if (rs.Fields.Item("U_ESACTIVO").Value != null)
                    sv.EsActivo = Convert.ToBoolean(rs.Fields.Item("U_ESACTIVO").Value);
                if (rs.Fields.Item("U_EMPRESAID").Value != null || rs.Fields.Item("U_EMPRESAID").Value > 0)
                    sv.Propietario.EmpresaID = Convert.ToInt32(rs.Fields.Item("U_EMPRESAID").Value);
                if (rs.Fields.Item("U_SUCURSALID").Value != null || rs.Fields.Item("U_SUCURSALID").Value > 0)
                    sv.SubPropietario.SucursalID = Convert.ToInt32(rs.Fields.Item("U_SUCURSALID").Value);
                if (rs.Fields.Item("U_IMAGENURL").Value != null)
                    sv.ImagenUrl = rs.Fields.Item("U_IMAGENURL").Value;
                if (rs.Fields.Item("U_TIPOSERVICIOID").Value != null || rs.Fields.Item("U_TIPOSERVICIOID").Value > 0)
                    sv.TipoServicio.TipoServicioID = Convert.ToInt32(rs.Fields.Item("U_TIPOSERVICIOID").Value);
                if (rs.Fields.Item("Sucursal").Value != null)
                    sv.SubPropietario.Direccion = rs.Fields.Item("Sucursal").Value;
                if (rs.Fields.Item("TipoServicio").Value != null)
                    sv.TipoServicio.Nombre = rs.Fields.Item("TipoServicio").Value;

                if (rs.Fields.Item("U_CUENTACONTABLE").Value != null)
                    sv.CuentaContable = rs.Fields.Item("U_CUENTACONTABLE").Value;

                if (!string.IsNullOrEmpty(rs.Fields.Item("U_ItemCode").Value))
                    sv.ItemCode = rs.Fields.Item("U_ItemCode").Value;

                if (!string.IsNullOrEmpty(rs.Fields.Item("U_NOMBRECUENTA").Value))
                    sv.NombreCuenta = rs.Fields.Item("U_NOMBRECUENTA").Value;
                //if (rs.Fields.Item("U_Sincronizado").Value != null)
                //    sv.Sincronizacion = (TipoSync)rs.Fields.Item("U_Sincronizado").Value;
                //if (rs.Fields.Item("U_UUID").Value != null)
                //    sv.UUID = rs.Fields.Item("U_UUID").Value;

                return sv;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Servicio LastRecordSetToSoloServicio(Recordset rs)
        {
            try
            {
                Servicio sv = new Servicio();
                sv.SubPropietario = new Sucursal();
                sv.Propietario = new Empresa();
                sv.TipoServicio = new TipoServicio();
                rs.MoveFirst();
                if (rs.Fields.Item("U_SERVICIOID").Value != null || rs.Fields.Item("U_SERVICIOID").Value > 0)
                    sv.ServicioID = Convert.ToInt32(rs.Fields.Item("U_SERVICIOID").Value);
                //if (rs.Fields.Item("Servicio").Value != null)
                    //sv.Nombre = rs.Fields.Item("Servicio").Value;
                if (rs.Fields.Item("U_DESCRIPCION").Value != null)
                    sv.Descripcion = rs.Fields.Item("U_DESCRIPCION").Value;
                if (rs.Fields.Item("U_ESACTIVO").Value != null)
                    sv.EsActivo = Convert.ToBoolean(rs.Fields.Item("U_ESACTIVO").Value);
                if (rs.Fields.Item("U_EMPRESAID").Value != null || rs.Fields.Item("U_EMPRESAID").Value > 0)
                    sv.Propietario.EmpresaID = Convert.ToInt32(rs.Fields.Item("U_EMPRESAID").Value);
                if (rs.Fields.Item("U_SUCURSALID").Value != null || rs.Fields.Item("U_SUCURSALID").Value > 0)
                    sv.SubPropietario.SucursalID = Convert.ToInt32(rs.Fields.Item("U_SUCURSALID").Value);
                if (rs.Fields.Item("U_IMAGENURL").Value != null)
                    sv.ImagenUrl = rs.Fields.Item("U_IMAGENURL").Value;
                if (rs.Fields.Item("U_TIPOSERVICIOID").Value != null || rs.Fields.Item("U_TIPOSERVICIOID").Value > 0)
                    sv.TipoServicio.TipoServicioID = Convert.ToInt32(rs.Fields.Item("U_TIPOSERVICIOID").Value);
                //if (rs.Fields.Item("Sucursal").Value != null)
                    //sv.SubPropietario.Direccion = rs.Fields.Item("Sucursal").Value;
                //if (rs.Fields.Item("TipoServicio").Value != null)
                    //sv.TipoServicio.Nombre = rs.Fields.Item("TipoServicio").Value;

                if (rs.Fields.Item("U_CUENTACONTABLE").Value != null)
                    sv.CuentaContable = rs.Fields.Item("U_CUENTACONTABLE").Value;
                //if (rs.Fields.Item("U_Sincronizado").Value != null)
                //    sv.Sincronizacion = (TipoSync)rs.Fields.Item("U_Sincronizado").Value;
                //if (rs.Fields.Item("U_UUID").Value != null)
                //    sv.UUID = rs.Fields.Item("U_UUID").Value;

                return sv;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Servicio RowRecordSetToServicio(Recordset rs)
        {
            try
            {
                Servicio sv = new Servicio();
                sv.SubPropietario = new Sucursal();
                sv.Propietario = new Empresa();
                sv.TipoServicio = new TipoServicio();
                //rs.MoveFirst();
                if (rs.Fields.Item("U_SERVICIOID").Value != null || rs.Fields.Item("U_SERVICIOID").Value > 0)
                    sv.ServicioID = Convert.ToInt32(rs.Fields.Item("U_SERVICIOID").Value);
                if (rs.Fields.Item("Servicio").Value != null)
                    sv.Nombre = rs.Fields.Item("Servicio").Value;
                if (rs.Fields.Item("U_DESCRIPCION").Value != null)
                    sv.Descripcion = rs.Fields.Item("U_DESCRIPCION").Value;
                if (rs.Fields.Item("U_ESACTIVO").Value != null)
                    sv.EsActivo = Convert.ToBoolean(rs.Fields.Item("U_ESACTIVO").Value);
                if (rs.Fields.Item("U_EMPRESAID").Value != null || rs.Fields.Item("U_EMPRESAID").Value > 0)
                    sv.Propietario.EmpresaID = Convert.ToInt32(rs.Fields.Item("U_EMPRESAID").Value);
                if (rs.Fields.Item("U_SUCURSALID").Value != null || rs.Fields.Item("U_SUCURSALID").Value > 0)
                    sv.SubPropietario.SucursalID = Convert.ToInt32(rs.Fields.Item("U_SUCURSALID").Value);
                if (rs.Fields.Item("U_IMAGENURL").Value != null)
                    sv.ImagenUrl = rs.Fields.Item("U_IMAGENURL").Value;
                if (rs.Fields.Item("U_TIPOSERVICIOID").Value != null || rs.Fields.Item("U_TIPOSERVICIOID").Value > 0)
                    sv.TipoServicio.TipoServicioID = Convert.ToInt32(rs.Fields.Item("U_TIPOSERVICIOID").Value);
                if (rs.Fields.Item("Sucursal").Value != null)
                    sv.SubPropietario.Direccion = rs.Fields.Item("Sucursal").Value;
                if (rs.Fields.Item("TipoServicio").Value != null)
                    sv.TipoServicio.Nombre = rs.Fields.Item("TipoServicio").Value;

                return sv;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Servicio> RecordSetToListServicio(Recordset rs)
        {
            try
            {
                List<Servicio> lstRet = new List<Servicio>();
                rs.MoveFirst();
                for(int i = 0; i < rs.RecordCount; i++)
                {
                    lstRet.Add(RowRecordSetToServicio(rs));
                    rs.MoveNext();
                }
                return lstRet;
            }
            catch (Exception ex) { throw new Exception("Error al convertir RecordSet a List<Servicio>: " + ex.Message); }
        }

        public bool HashServicios(Recordset rs)
        {
            if (rs.RecordCount < 1)
                return false;
            return true;
        }

        public bool SetUDTByServicioID(int? id)
        {
            try
            {
                var rs = this.ConsultarByID(id);
                int _id = 0;
                if (HashServicios(rs))
                {
                    rs.MoveFirst();
                    _id = rs.Fields.Item("U_SERVICIOID").Value;
                    if (_id == id)
                        return this.oUDT.GetByKey(rs.Fields.Item("Code").Value.ToString());

                }
                this.oUDT.Code = string.Empty;
                return false;
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }

        public Recordset ConsultarByID(int? id)
        {
            try
            {
                Recordset rs = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(string.Format("select * from " + this.NombreTabla + @" where ""U_SERVICIOID"" = {0} and ""U_ESACTIVO"" = 1", id));
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool ExisteServicio(int svid)
        {
            Recordset sv = ConsultarByID(svid);

            return HashServicios(sv);
        }

        public void Deactivate()
        {
            String ID = String.Empty;
            ID = Convert.ToString(oUDT.Code);

            #region SQL || HANA
            this.Parametro = String.Empty;
            this.Parametro = String.Format(@"UPDATE {0} SET ""U_ESACTIVO"" = 0 WHERE ""Code"" = '{1}'", this.NombreTabla, ID);
            this.Insert.DoQuery(this.Parametro);
            #endregion SQL || HANA

            #region SQL
            //SQL
            /*this.oUDT.UserFields.Fields.Item("U_EsActivo").Value = 0;
            Actualizar();*/
            //Probar cambio sincronización con función eliminar
            //Eliminar();
            #endregion SQL
        }

        public void UDTToServicio()
        {
            try
            {
                oServicio = new Servicio { Propietario = new Empresa(), SubPropietario = new Sucursal(), TipoServicio = new TipoServicio() };
                oServicio.ServicioID = (int)this.oUDT.UserFields.Fields.Item("U_SERVICIOID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_ServicioID").Value;
                oServicio.Nombre = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_NOMBRE").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Nombre").Value;
                oServicio.Descripcion = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_DESCRIPCION").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Descripcion").Value;
                oServicio.ImagenUrl = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_IMAGENURL").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Descripcion").Value;
                oServicio.SubPropietario.SucursalID = (int)this.oUDT.UserFields.Fields.Item("U_SUCURSALID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_SucursalID").Value;
                oServicio.TipoServicio.TipoServicioID = (int)this.oUDT.UserFields.Fields.Item("U_TIPOSERVICIOID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_TipoServicioID").Value;
                oServicio.Propietario.EmpresaID = (int)this.oUDT.UserFields.Fields.Item("U_EMPRESAID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value;
                oServicio.EsActivo = this.oUDT.UserFields.Fields.Item("U_ESACTIVO").Value == 1;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Sincronizar()
        {
            try
            {
                //Se obtiene el campo U_Sincronizado
                String Sinc = String.Empty;
                if (!String.IsNullOrEmpty(Convert.ToString(this.oUDT.UserFields.Fields.Item("U_SINCRONIZADO").Value)))
                    Sinc = Convert.ToString(this.oUDT.UserFields.Fields.Item("U_SINCRONIZADO").Value);

                //Se obtiene el campo Code 
                String code = String.Empty;
                code = oUDT.Code.ToString();

                #region SQL || HANA
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET ""U_SINCRONIZADO"" = '{1}' WHERE ""Code"" = '{2}'", this.NombreTabla, Sinc, code);
                this.Insert.DoQuery(this.Parametro);
                #endregion SQL || HANA

                #region SQL
                //SQL
                /*this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Sincronizado == null ? 0 : this.Sincronizado;
                this.Actualizar();*/
                #endregion SQL
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public Recordset ActualizarCode(Servicio sv)
        {
            try
            {
                Recordset rs = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(@" UPDATE " + this.NombreTabla + @" SET ""Code"" = '" + sv.ServicioID.ToString() +
                    @"', U_SERVICIOID = " + sv.ServicioID + @", ""U_SINCRONIZADO"" = " + Sincronizado);

                if (UUID != null)
                    where.Append(string.Format(@" and ""U_UUID"" like '%{0}%' ", UUID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void GetSyncUUID()
        {
            try
            {
                if (!string.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_UUID").Value))
                    this.UUID = Guid.Parse(this.oUDT.UserFields.Fields.Item("U_UUID").Value);
                this.Sincronizado = this.oUDT.UserFields.Fields.Item("U_SINCRONIZADO").Value;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
    }
}
