﻿using Kanan.Mantenimiento.BO2;
using KananSAP.Operaciones.Data;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KananSAP.Mantenimiento.Data
{
    public class TipoServicioData
    {

        #region Atributos
        private Company oCompany;
        public UserTable oUDT { get; set; }
        public Kanan.Mantenimiento.BO2.TipoServicio oTipoServicio { get; set; }
        private const string TABLE_TSERVICIO = "VSKF_TIPOSERVICIO";
        public string ItemCode { get; set; }
        public int? Sincronizado { get; set; }
        public Guid UUID { get; set; }
        private SucursalSAP SucursalSAP { get; set; }


        //Migracion Hana
        Recordset Insert;
        String Parametro;
        private String NombreTabla;
        bool EsSQL;

        //Se agregó para ser visible desde ServicioSAP.cs en KananSAP.Mantenimiento.Data.
        public String NombreTablaTipoServicioData = String.Empty;

        #endregion

        #region Constructor
        public TipoServicioData(Company company)
        {
            this.oCompany = company;
            this.oTipoServicio = new Kanan.Mantenimiento.BO2.TipoServicio();
            this.oTipoServicio.Propietario = new Kanan.Operaciones.BO2.Empresa();
            this.oTipoServicio.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
            try
            {
                this.oUDT = this.oCompany.UserTables.Item(TABLE_TSERVICIO);

                this.SucursalSAP = new SucursalSAP(this.oCompany);

                //Migración HANA
                this.Insert = (Recordset)this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.EsSQL = oCompany.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.NombreTabla = EsSQL ? "[@VSKF_TIPOSERVICIO]" : @"""@VSKF_TIPOSERVICIO""";
                //Se maneja en una variable pública para poder utilizar el método "Consultar" en ServicioSAP.cs
                // en KananSAP.Mantenimiento.Data.
                this.NombreTablaTipoServicioData = this.NombreTabla;
            }
            catch(Exception ex)
            {
                this.oUDT = null;
            }
        }
        #endregion

        public bool Existe()
        {
            return this.oUDT.GetByKey(this.ItemCode);
        }

        public void TipoServicioToUDT()
        {
            try
            {
                if (this.oTipoServicio.SubPropietario == null) this.oTipoServicio.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
                int activo = 0;
                if (Convert.ToBoolean(oTipoServicio.EsActivo))
                    activo = 1;
                oUDT.Code = this.ItemCode ?? oTipoServicio.TipoServicioID.ToString();// oTipoServicio.TipoServicioID.ToString() ?? this.ItemCode;
                oUDT.Name =  this.ItemCode ?? oTipoServicio.TipoServicioID.ToString() ;
                oUDT.UserFields.Fields.Item("U_TIPOSERVICIOID").Value = oTipoServicio.TipoServicioID ?? 0;
                oUDT.UserFields.Fields.Item("U_NOMBRE").Value = oTipoServicio.Nombre ?? string.Empty;
                oUDT.UserFields.Fields.Item("U_DESCRIPCION").Value = oTipoServicio.Descripcion ?? string.Empty;
                oUDT.UserFields.Fields.Item("U_EMPRESAID").Value = oTipoServicio.Propietario.EmpresaID ?? 0;
                oUDT.UserFields.Fields.Item("U_SUCURSALID").Value = oTipoServicio.SubPropietario.SucursalID ?? 0;
                oUDT.UserFields.Fields.Item("U_ESACTIVO").Value = activo;
                oUDT.UserFields.Fields.Item("U_SINCRONIZADO").Value = this.Sincronizado ?? 0;
                oUDT.UserFields.Fields.Item("U_UUID").Value = this.UUID.ToString() ?? string.Empty;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Inserta()
        {
            try
            {
                String Activo = "0";
                if (this.oTipoServicio.EsActivo == true)
                    Activo = "1";

                #region SQL || HANA 
                this.Parametro = String.Empty;
                this.Parametro = String.Format("INSERT INTO {0} VALUES('{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}')", this.NombreTabla, this.oTipoServicio.TipoServicioID, this.oTipoServicio.TipoServicioID, this.oTipoServicio.TipoServicioID, this.oTipoServicio.Nombre, this.oTipoServicio.Descripcion, this.oTipoServicio.Propietario.EmpresaID, this.oTipoServicio.SubPropietario.SucursalID, Activo, "0", this.UUID);
                this.Insert.DoQuery(this.Parametro);
                #endregion SQL || HANA
            }
            catch(Exception ex)
            {
                #region SQL
                string err;
                int code;
                int result = -1;
                result = this.oUDT.Add();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out code, out err);
                    throw new Exception(string.Format("ErrorCode: {0}, ErrorMessage:{1}.", code, err));
                }
                #endregion SQL
            }
        }

        public void Actualizar()
        {
            try
            {
                String Activo = "0";

                if (this.oTipoServicio.EsActivo == true)
                    Activo = "1";

                #region SQL || HANA

                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET ""Code"" = '{1}', ""Name"" = '{2}', ""U_TIPOSERVICIOID"" = '{3}', ""U_NOMBRE"" = '{4}', ""U_DESCRIPCION"" = '{5}', ""U_EMPRESAID"" = '{6}', ""U_SUCURSALID"" = '{7}', ""U_ESACTIVO""= '{8}' WHERE ""Code"" = '{1}'", this.NombreTabla, this.oTipoServicio.TipoServicioID, this.oTipoServicio.TipoServicioID, this.oTipoServicio.TipoServicioID, this.oTipoServicio.Nombre, this.oTipoServicio.Descripcion, this.oTipoServicio.Propietario.EmpresaID, this.oTipoServicio.SubPropietario.SucursalID, Activo);
                this.Insert.DoQuery(this.Parametro);

                #endregion SQL || HANA
            }
            catch (Exception ex)
            {
                #region SQL
                string err;
                int code;
                int result = -1;
                result = this.oUDT.Update();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out code, out err);
                    throw new Exception(string.Format("ErrorCode: {0}, ErrorMessage:{1}.", code, err));
                }
                #endregion SQL
            }
        }

        public void ActualizarCompleto()
        {
            try
            {
                ServicioSAP svData = new ServicioSAP(this.oCompany);
                List<Servicio> servicios = new List<Servicio>();
                Servicio sv = new Servicio();
                sv.TipoServicio = this.oTipoServicio;
                //sv.Propietario = this.oTipoServicio.Propietario;
                //sv.SubPropietario = this.oTipoServicio.SubPropietario;
                Recordset rs = svData.Consultar(sv);
                if(svData.HashServicios(rs))
                {
                    servicios = svData.RecordSetToListServicio(rs);
                    foreach (Servicio data in servicios)
                    {
                        if(svData.SetUDTByServicioID(data.ServicioID))
                        {
                            //svData.oServicio = data;
                            //svData.ServicioToUDT();
                            svData.UDTToServicio();
                            svData.EliminarCompleto();
                        }
                    }
                }
                this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Sincronizado ?? 0;
                this.Deactivate();
            }
            catch (Exception ex)
            {
                throw new Exception("ActualizarCompleto: " + ex.Message);
            }
        }

        public void Eliminar()
        {
            try
            {
                string ID = String.Empty;
                if (!String.IsNullOrEmpty(oUDT.Code))
                    ID = Convert.ToString(oUDT.Code);
                else
                {
                    throw new Exception("Erron al intentar obtener identificador. ");
                }

                #region SQL || HANA
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code"" = '{1}'", this.NombreTabla, ID);
                this.Insert.DoQuery(this.Parametro);
                #endregion
            }
            catch (Exception ex)
            {
                #region SQL
                string err;
                int code;
                int result = -1;
                result = this.oUDT.Remove();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out code, out err);
                    throw new Exception(string.Format("ErrorCode: {0}, ErrorMessage:{1}.", code, err));
                }
                #endregion SQL
            }
        }
        public Recordset Cuentascontable(bool bConnSql)
        {
            String stQuery = "";
            Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            try
            {
                if (bConnSql)
                    stQuery = "SELECT AcctCode, AcctName FROM OACT WHERE Postable = 'Y' AND FrozenFor ='N'";
                else
                    stQuery = @"SELECT ""AcctCode"", ""AcctName"" FROM ""OACT"" WHERE ""Postable"" = 'Y' AND ""FrozenFor"" = 'N'";


                rs.DoQuery(stQuery);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return rs;
        }

        public Recordset Consultar(TipoServicio ts)
        {
            try
            {
                if (ts == null) ts = new TipoServicio();
                if (ts.Propietario == null) ts.Propietario = new Kanan.Operaciones.BO2.Empresa();
                if (ts.SubPropietario == null) ts.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(@" SELECT ts.""U_TIPOSERVICIOID"", ts.""U_NOMBRE"", ts.""U_DESCRIPCION"",ts.""U_EMPRESAID"", ts.""U_SUCURSALID"", ts.""U_ESACTIVO"",sc.""U_DIRECCION"" as ""Sucursal"" from " + this.NombreTabla + " ts left outer  join " + this.SucursalSAP.NombreTablaSucursalSAP + @" sc on sc.""U_SUCURSALID""= ts.""U_SUCURSALID""  ");
                if (ts.TipoServicioID != null)
                    where.Append(string.Format(@"and ts.""U_TIPOSERVICIOID"" = {0} ", ts.TipoServicioID));
                if (ts.Nombre != null)
                    where.Append(string.Format(@"and ts.""U_NOMBRE"" = '{0}' ", ts.Nombre));
                if (ts.Descripcion != null)
                    where.Append(string.Format(@"and ts.""U_DESCRIPCION"" like '%{0}%' ", ts.Descripcion));
                if (ts.Propietario.EmpresaID != null)
                    where.Append(string.Format(@"and ts.""U_EMPRESAID"" = {0} ", ts.Propietario.EmpresaID));
                if (ts.SubPropietario.SucursalID != null)
                    where.Append(string.Format(@"and ts.""U_SUCURSALID"" = {0} ", ts.SubPropietario.SucursalID));
                if (ts.EsActivo != null)
                    where.Append(string.Format(@"and ts.""U_ESACTIVO"" = {0} ", Convert.ToInt32(ts.EsActivo)));
                
                if(where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith("and"))
                        filtro = filtro.Substring(3);
                    query.Append(" where " + filtro);
                }
                query.Append(@" order by ts.""U_TIPOSERVICIOID"" asc ");
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool HasTipoServicio(Recordset rs)
        {
            if (rs.RecordCount < 1)
                return false;
            return true;
        }

        public TipoServicio LastRecordSetToTipoServicio(Recordset rs)
        {
            try
            {
                TipoServicio ts = new TipoServicio();
                ts.Propietario = new Kanan.Operaciones.BO2.Empresa();
                ts.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
                rs.MoveFirst();
                if (rs.Fields.Item("U_TIPOSERVICIOID").Value != null && rs.Fields.Item("U_TIPOSERVICIOID").Value > 0)
                    ts.TipoServicioID = Convert.ToInt32(rs.Fields.Item("U_TIPOSERVICIOID").Value);
                if (!string.IsNullOrEmpty(rs.Fields.Item("U_NOMBRE").Value))
                    ts.Nombre = rs.Fields.Item("U_NOMBRE").Value;
                if (!string.IsNullOrEmpty(rs.Fields.Item("U_DESCRIPCION").Value))
                    ts.Descripcion = rs.Fields.Item("U_DESCRIPCION").Value;
                if (rs.Fields.Item("U_EMPRESAID").Value != null && rs.Fields.Item("U_EMPRESAID").Value > 0)
                    ts.Propietario.EmpresaID = Convert.ToInt32(rs.Fields.Item("U_EmpresaID").Value);
                if (rs.Fields.Item("U_SUCURSALID").Value != null && rs.Fields.Item("U_SUCURSALID").Value > 0)
                    ts.SubPropietario.SucursalID = Convert.ToInt32(rs.Fields.Item("U_SUCURSALID").Value);
                if (rs.Fields.Item("U_ESACTIVO").Value != null && rs.Fields.Item("U_ESACTIVO").Value > 0)
                    ts.EsActivo = Convert.ToBoolean(rs.Fields.Item("U_ESACTIVO").Value);
                if (rs.Fields.Item("Sucursal").Value != null)
                    ts.SubPropietario.Direccion = rs.Fields.Item("Sucursal").Value;
                return ts;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public TipoServicio RowRecordSetToTipoServicio(Recordset rs)
        {
            try
            {
                TipoServicio ts = new TipoServicio();
                ts.Propietario = new Kanan.Operaciones.BO2.Empresa();
                ts.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
                if (rs.Fields.Item("U_TIPOSERVICIOID").Value != null && rs.Fields.Item("U_TIPOSERVICIOID").Value > 0)
                    ts.TipoServicioID = Convert.ToInt32(rs.Fields.Item("U_TIPOSERVICIOID").Value);
                if (!string.IsNullOrEmpty(rs.Fields.Item("U_NOMBRE").Value))
                    ts.Nombre = rs.Fields.Item("U_NOMBRE").Value;
                if (!string.IsNullOrEmpty(rs.Fields.Item("U_DESCRIPCION").Value))
                    ts.Descripcion = rs.Fields.Item("U_DESCRIPCION").Value;
                if (rs.Fields.Item("U_EMPRESAID").Value != null && rs.Fields.Item("U_EMPRESAID").Value > 0)
                    ts.Propietario.EmpresaID = Convert.ToInt32(rs.Fields.Item("U_EMPRESAID").Value);
                if (rs.Fields.Item("U_SUCURSALID").Value != null && rs.Fields.Item("U_SUCURSALID").Value > 0)
                    ts.SubPropietario.SucursalID = Convert.ToInt32(rs.Fields.Item("U_SUCURSALID").Value);
                if (rs.Fields.Item("U_ESACTIVO").Value != null && rs.Fields.Item("U_ESACTIVO").Value > 0)
                    ts.EsActivo = Convert.ToBoolean(rs.Fields.Item("U_ESACTIVO").Value);
                if (rs.Fields.Item("Sucursal").Value != null)
                    ts.SubPropietario.Direccion = rs.Fields.Item("Sucursal").Value;
                return ts;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<TipoServicio> RecordSetToListTipoServicio(Recordset rs)
        {
            try
            {
                List<TipoServicio> list = new List<TipoServicio>();
                rs.MoveFirst();
                for(int i = 0; i < rs.RecordCount; i++)
                {
                    list.Add(this.RowRecordSetToTipoServicio(rs));
                    rs.MoveNext();
                }
                return list;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public bool SetUDTByServicioID(int? id)
        {
            try
            {
                var rs = this.ConsultarByID(id);
                int _id = 0;
                if (HasTipoServicio(rs))
                {
                    rs.MoveFirst();
                    _id = rs.Fields.Item("U_TIPOSERVICIOID").Value;
                    if (_id == id)
                        return this.oUDT.GetByKey(rs.Fields.Item("Code").Value.ToString());

                }
                this.oUDT.Code = string.Empty;
                return false;
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }

        public Recordset ConsultarByID(int? id)
        {
            try
            {
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(string.Format(@"select * from " + this.NombreTabla + @" where ""U_TIPOSERVICIOID"" = {0} and ""U_ESACTIVO"" = 1", id));
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Deactivate()
        {
            string ID = String.Empty;
            if (!String.IsNullOrEmpty(oUDT.Code))
                ID = oUDT.Code;
            else
            {
                throw new Exception("Erron al intentar obtener identificador.");
            }

            #region SQL || HANA
            this.Parametro = String.Empty;
            this.Parametro = String.Format(@"UPDATE {0} SET ""U_ESACTIVO"" = 0 WHERE ""Code"" = '{1}'", this.NombreTabla, ID);
            this.Insert.DoQuery(this.Parametro);
            #endregion SQL || HANA

            #region SQL
            //SQL
            /*this.oUDT.UserFields.Fields.Item("U_EsActivo").Value = 0;
            Actualizar();*/
            #endregion SQL
        }

        public void UDTToTipoServicio()
        {
            try
            {
                oTipoServicio = new TipoServicio { Propietario = new Kanan.Operaciones.BO2.Empresa(), SubPropietario = new Kanan.Operaciones.BO2.Sucursal() };
                oTipoServicio.TipoServicioID = (int)this.oUDT.UserFields.Fields.Item("U_TIPOSERVICIOID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_TipoServicioID").Value;
                oTipoServicio.Nombre = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_NOMBRE").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Nombre").Value;
                oTipoServicio.Descripcion = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_DESCRIPCION").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Descripcion").Value;
                oTipoServicio.Propietario.EmpresaID = (int)this.oUDT.UserFields.Fields.Item("U_EMPRESAID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value;
                oTipoServicio.SubPropietario.SucursalID = (int)this.oUDT.UserFields.Fields.Item("U_SUCURSALID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_SucursalID").Value;
                oTipoServicio.EsActivo = this.oUDT.UserFields.Fields.Item("U_ESACTIVO").Value == 1;

            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public Recordset ActualizarCode(TipoServicio ts)
        {
            try
            {
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(@" UPDATE " + this.NombreTabla + @" SET ""Code"" = '" + ts.TipoServicioID.ToString() + @"' , ""Name"" = '" + ts.TipoServicioID.ToString() +
                    @"' , ""U_TIPOSERVICIOID"" = " + ts.TipoServicioID + @", ""U_SINCRONIZADO"" = " + Sincronizado);

                if (UUID != null)
                    where.Append(string.Format(@" and ""U_UUID"" like '%{0}%' ", UUID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void GetSyncUUID()
        {
            try
            {
                if (!string.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_UUID").Value))
                    this.UUID = Guid.Parse(this.oUDT.UserFields.Fields.Item("U_UUID").Value);
                this.Sincronizado = this.oUDT.UserFields.Fields.Item("U_SINCRONIZADO").Value;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void Sincronizar()
        {
            try
            {
                String Sinc = String.Empty;
                Sinc = this.Sincronizado == null ? "0" : Convert.ToString(this.Sincronizado);

                string ID = String.Empty;
                if (!String.IsNullOrEmpty(oUDT.Code))
                    ID = Convert.ToString(oUDT.Code);
                else
                {
                    throw new Exception("Erron al intentar obtener identificador.");
                }

                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET ""U_SINCRONIZADO"" = '{1}' WHERE ""Code"" = '{2}'", this.NombreTabla, Sinc, ID);
                this.Insert.DoQuery(this.Parametro);
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
    }
}
