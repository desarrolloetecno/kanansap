using Kanan.Mantenimiento.BO2;
using Kanan.Vehiculos.BO2;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KananSAP.Mantenimiento.Data
{
    public class DetalleOrdenSAP
    {
        #region Atributos
        private const string TABLE_DETALLEORDEN = "VSKF_DETALLEORDEN";
        public DetalleOrden oDetalleOrden { get; set; }
        public UserTable oUDT { get; set; }
        private Company oCompany;
        public string ItemCode { get; set; }
        /// <summary>
        /// 0 = Sincronizado, 1 = Insertando, 2 = Actualizando, 3 = Eliminando
        /// </summary>
        public int? Sincronizado { get; set; }
        /// <summary>
        /// Guid que se tomará para la sincronización SAP <-> Kanan Fleet
        /// </summary>
        public Guid UUID { get; set; }
        #endregion

        #region Contructor
        public DetalleOrdenSAP(ref Company company)
        {
            this.oCompany = company;
            try
            {
                this.oUDT = this.oCompany.UserTables.Item(TABLE_DETALLEORDEN);
            }
            catch
            {
                this.oUDT = null;
            }
        }
        #endregion

        public bool Existe()
        {
            return this.oUDT.GetByKey(this.ItemCode);
        }

        public string DetalleOrdenToUDT()
        {
            string tks = DateTime.Now.Ticks.ToString();
            try
            {
                //this.oUDT.Code = this.ItemCode ?? this.oDetalleOrden.DetalleOrdenID.ToString();
                //this.oUDT.Name = this.ItemCode ?? this.oDetalleOrden.DetalleOrdenID.ToString();

                oUDT.Code = this.oDetalleOrden.DetalleOrdenID != null ? this.oDetalleOrden.DetalleOrdenID.ToString() : tks;
                oUDT.Name = this.oDetalleOrden.DetalleOrdenID != null ? this.oDetalleOrden.DetalleOrdenID.ToString() : tks;
                this.oUDT.UserFields.Fields.Item("U_MantenibleID").Value = this.oDetalleOrden.Mantenible.MantenibleID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_ServicioID").Value = this.oDetalleOrden.Servicio.ServicioID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_OrdenServicioID").Value = this.oDetalleOrden.OrdenServicio.OrdenServicioID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value = this.oDetalleOrden.TipoMantenibleID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_Costo").Value = (double?)this.oDetalleOrden.Costo ?? 0;
                this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Sincronizado == null ? 0 : this.Sincronizado;
                this.oUDT.UserFields.Fields.Item("U_UUID").Value = this.UUID == null ? "" : this.UUID.ToString();
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
            return tks;
        }

        public string InsertaDetalleOrden(DetalleOrden oDetalle, string sCodigo)
        {
            string sResponse = "ERROR";
            int iCommand = 0, iCode = 0;
            this.oUDT.Code = sCodigo;
            this.oUDT.Name = sCodigo;
            this.oUDT.UserFields.Fields.Item("U_MantenibleID").Value = oDetalle.Mantenible.MantenibleID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_ServicioID").Value = oDetalle.Servicio.ServicioID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_OrdenServicioID").Value = oDetalle.OrdenServicio.OrdenServicioID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value = oDetalle.TipoMantenibleID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_Costo").Value = (double?)oDetalle.Costo ?? 0;
            this.oUDT.UserFields.Fields.Item("U_DetalleOrdenID").Value = oDetalle.DetalleOrdenID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Sincronizado == null ? 0 : this.Sincronizado;
            this.oUDT.UserFields.Fields.Item("U_UUID").Value = this.UUID == null ? string.Empty : this.UUID.ToString();
            iCommand = this.oUDT.Add();
            if (iCommand != 0)
                this.oCompany.GetLastError(out iCode, out sResponse);
            else sResponse = "OK";
            return sResponse;
        }

        public string InsertaCostoAdicional(SBOKF_CL_CostosAdicional_INFO oCosto, string sCodigo)
        {
            string sResponse = "ERROR";
            int iCommand = 0, iCode = 0;
            UserTable oTabla = this.oCompany.UserTables.Item("VSKF_OSCOSTOSADICIO");
            oTabla.Code = sCodigo;
            oTabla.Name = sCodigo;
            oTabla.UserFields.Fields.Item("U_OrdenServico").Value = oCosto.Code;
            oTabla.UserFields.Fields.Item("U_Descripcion").Value = oCosto.U_DESCRIPCION;
            oTabla.UserFields.Fields.Item("U_Costo").Value = oCosto.U_COSTO;
            iCommand = oTabla.Add();
            if (iCommand != 0)
                this.oCompany.GetLastError(out iCode, out sResponse);
            else sResponse = "OK";
            return sResponse;
        }

        public string InsertaHerramientas(SBO_KF_Herramientas_INFO oTool, string sCodigo)
        {
            string sResponse = "ERROR";
            int iCommand = 0, iCode = 0;
            UserTable oTabla = this.oCompany.UserTables.Item("VSKF_HERRAMIENTAOS");
            oTabla.Code = sCodigo;
            oTabla.Name = sCodigo;
            oTabla.UserFields.Fields.Item("U_Folio").Value = oTool.ItemCode;
            oTabla.UserFields.Fields.Item("U_Descripcion").Value = oTool.ItemName;
            oTabla.UserFields.Fields.Item("U_Cantidad").Value = oTool.Quantity;
            oTabla.UserFields.Fields.Item("U_OrdenServicioID").Value = oTool.OrdenServicioID;
            iCommand = oTabla.Add();
            if (iCommand != 0)
                this.oCompany.GetLastError(out iCode, out sResponse);
            else sResponse = "OK";
            return sResponse;
        }

        public string InsertaMecanicos(SBO_KF_OHEM_INFO oEmpleado, string sCodigo)
        {
            string sResponse = "ERROR";
            int iCommand = 0, iCode = 0;
            UserTable oTabla = this.oCompany.UserTables.Item("VSKF_MECANICOSOS");
            oTabla.Code = sCodigo;
            oTabla.Name = sCodigo;
            oTabla.UserFields.Fields.Item("U_Empleadoid").Value = oEmpleado.empID.ToString();
            oTabla.UserFields.Fields.Item("U_Nombre").Value = oEmpleado.firstName;
            oTabla.UserFields.Fields.Item("U_Sueldo").Value = oEmpleado.salary;
            oTabla.UserFields.Fields.Item("U_OrdenServicioID").Value = oEmpleado.OrdeServicioID;
            iCommand = oTabla.Add();
            if (iCommand != 0)
                this.oCompany.GetLastError(out iCode, out sResponse);
            else sResponse = "OK";
            return sResponse;
        }

        public bool InsertaPlantillaDetalle(ref DetalleOrden DetOs , string sCode, string sCodePadre)
        {
            bool bContinuar = false;
            string sResponse = "ERROR";
            int iCommand = 0, iCode = 0;
            UserTable oTabla = this.oCompany.UserTables.Item("VSKF_OSPLANTILLADET");
            oTabla.Code = sCode;
            oTabla.Name = sCode;
            oTabla.UserFields.Fields.Item("U_PlantillaPadre").Value = sCodePadre;
            oTabla.UserFields.Fields.Item("U_MantenibleID").Value = DetOs.Mantenible.MantenibleID;
            oTabla.UserFields.Fields.Item("U_VehiculoNombre").Value = (DetOs.Mantenible as Vehiculo).Nombre;
            oTabla.UserFields.Fields.Item("U_ServicioID").Value = DetOs.Servicio.ServicioID;
            oTabla.UserFields.Fields.Item("U_NombreServicio").Value = DetOs.Servicio.Nombre;
            oTabla.UserFields.Fields.Item("U_Costo").Value = Convert.ToDouble(DetOs.Costo);
            iCommand = oTabla.Add();
            if (iCommand != 0)
                this.oCompany.GetLastError(out iCode, out sResponse);
            else bContinuar = true;
            return bContinuar;
        }

        public bool InsertaPlantillaRefacciones(ref DetalleRefaccion oRefaccion, string sCode, string sCodePadre)
        {
            bool bContinuar = false;
            string sResponse = "ERROR";
            int iCommand = 0, iCode = 0;
            UserTable oUDTRFCC = this.oCompany.UserTables.Item("VSKF_PLANTREFACCION");
            oUDTRFCC.Code = sCode;
            oUDTRFCC.Name = sCode;
            oUDTRFCC.UserFields.Fields.Item("U_PlantillaPadre").Value = sCodePadre;
            oUDTRFCC.UserFields.Fields.Item("U_Costo").Value = Convert.ToDouble( oRefaccion.Costo);
            oUDTRFCC.UserFields.Fields.Item("U_Articulo").Value = oRefaccion.Articulo;
            oUDTRFCC.UserFields.Fields.Item("U_Cantidad").Value = Convert.ToDouble( oRefaccion.Cantidad);
            oUDTRFCC.UserFields.Fields.Item("U_Total").Value = Convert.ToDouble(oRefaccion.Total);
            oUDTRFCC.UserFields.Fields.Item("U_Almacen").Value = oRefaccion.Almacen;
            oUDTRFCC.UserFields.Fields.Item("U_CodAlmcn").Value = oRefaccion.CodigoAlmacen;
            iCommand = oUDTRFCC.Add();
            if (iCommand != 0)
                this.oCompany.GetLastError(out iCode, out sResponse);
            else bContinuar = true;
            return bContinuar;
        }
        private void InitializeDetalleOrden()
        {
            this.oDetalleOrden = new DetalleOrden();
        }

        public void UDTTooDetalleOrden()
        {
            try
            {
                this.InitializeDetalleOrden();
                int id;
                if (int.TryParse(this.oUDT.Code, out id))
                    this.oDetalleOrden.DetalleOrdenID = id;
                else
                    this.oDetalleOrden.DetalleOrdenID = null;
                this.oDetalleOrden.Servicio.ServicioID = this.oUDT.UserFields.Fields.Item("U_ServicioID").Value ?? null;
                this.oDetalleOrden.OrdenServicio.OrdenServicioID = this.oUDT.UserFields.Fields.Item("U_OrdenServicioID").Value ?? null;
                this.oDetalleOrden.TipoMantenibleID = this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value ?? null;
                if(this.oDetalleOrden.TipoMantenibleID !=null)
                {
                    if (this.oDetalleOrden.TipoMantenibleID == 1)
                    {
                        this.oDetalleOrden.Mantenible = new Vehiculo();
                        (this.oDetalleOrden.Mantenible as Vehiculo).Nombre = Convert.ToString(this.oUDT.UserFields.Fields.Item("U_MantenibleID").Value);
                    }
                    else if (this.oDetalleOrden.TipoMantenibleID == 2)
                    {
                        this.oDetalleOrden.Mantenible = new Caja();
                        this.oDetalleOrden.Mantenible.MantenibleID = this.oUDT.UserFields.Fields.Item("U_MantenibleID").Value ?? null;
                    }
                    
                }
                this.oDetalleOrden.Costo = (decimal?)Convert.ChangeType(this.oUDT.UserFields.Fields.Item("U_Costo").Value, typeof(decimal)) ?? Convert.ToDecimal(0);
                this.Sincronizado = this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value ?? null;
                this.UUID = Guid.Parse(this.oUDT.UserFields.Fields.Item("U_UUID").Value) ?? null;

            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public bool SetUDTDetalleOrdenID(int? id)
        {
            try
            {
                var rs = this.ConsultarByID(id);
                if (HashoDetalleOrden(rs))
                {
                    rs.MoveFirst();
                    int _id = rs.Fields.Item("Code").Value;
                    if (_id == id)
                        return this.oUDT.GetByKey(rs.Fields.Item("Code").Value);
                }
                this.oUDT.Code = string.Empty;
                return false;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public Recordset ConsultarByID(int? id)
        {
            try
            {
                string query = string.Format(@"select * from ""@{0}"" where ""Code"" = {1}", TABLE_DETALLEORDEN, id);
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                return rs;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public Recordset ConsultarByOrdenID(int? id)
        {
            try
            {
                string query = string.Format(@"select * from ""@{0}"" where U_OrdenServicioID = {1}", TABLE_DETALLEORDEN, id);
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                return rs;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public bool HashoDetalleOrden(Recordset rs)
        {
            if (rs.RecordCount > 0)
                return true;
            return false;
        }

        public void Insert()
        {
            int result = -1;
            try
            {
                result = this.oUDT.Add();
                if (result != 0)
                {
                    int errornum;
                    string errormsj;
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Actualizar()
        {
            int result = -1;
            try
            {
                int errornum;
                string errormsj;
                result = this.oUDT.Update();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar()
        {
            int result = -1;
            try
            {
                int errornum;
                string errormsj;
                result = this.oUDT.Remove();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Recordset ActualizarCode(DetalleOrden detalle, string sIDItem)
        {
            try
            {
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                string strUpdate = string.Format(@"UPDATE ""@VSKF_DETALLEORDEN"" SET ""Code"" = '{0}', ""Name"" = '{0}', U_Sincronizado = {1}, U_OrdenServicioID = {2} ",
                    detalle.DetalleOrdenID, this.Sincronizado, detalle.OrdenServicio.OrdenServicioID);
                query.Append(strUpdate);
                //query.Append(" UPDATE [@VSKF_ORDENSERVICIO] SET Code = '" + detalle.DetalleOrdenID + "', U_Sincronizado = " + Sincronizado + ", Name = ' " + detalle.DetalleOrdenID + "'");
                //if (detalle.OrdenServicio != null && detalle.OrdenServicio.OrdenServicioID != null)
                //    query.Append(string.Format(", U_OrdenServicioID = {0} ", detalle.OrdenServicio.OrdenServicioID));
                //if (uuid != null)
                //    where.append(string.format(" and u_uuid like '%{0}%' ", uuid));
                //if (detalle.Mantenible != null && detalle.Mantenible.MantenibleID != null)
                //    where.Append(string.Format(" and U_MantenibleID = {0} ", detalle.Mantenible.MantenibleID));
                //if(detalle.Servicio != null && detalle.Servicio.ServicioID != null)
                //    where.Append(string.Format(" and U_ServicioID = {0} ", detalle.Servicio.ServicioID));
                //if(detalle.TipoMantenibleID != null)
                //    where.Append(string.Format(" and U_TipoMantenibleID = {0} ", detalle.TipoMantenibleID));
                //if (detalle.Costo != null)
                //    where.Append(string.Format(" and U_Costo = {0} ", detalle.Costo));
                if (!string.IsNullOrEmpty(sIDItem))
                    where.Append(string.Format(" and Code = '{0}' ", sIDItem));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<DetalleOrden> RecordSetToListDetalleOrden(Recordset rs)
        {
            var result = new List<DetalleOrden>();
            rs.MoveFirst();
            for (int i = 0; i < rs.RecordCount; i++)
            {
                string itemcode = rs.Fields.Item("Code").Value;
                this.oUDT.GetByKey(itemcode);
                UDTTooDetalleOrden();
                var temp = (DetalleOrden)this.oDetalleOrden.Clone();
                result.Add(temp);
                rs.MoveNext();
            }
            return result;
        }

        //Se agregó con un nuevo método para evitar crusar información en la actualización de los parámetros y las alertas
        public List <DetalleOrden> RecordSetToListDetalleOrden (Recordset rs, DetalleOrden DetOr)
        {
            // Se guardan valores en temporales.
            DetOr = this.oDetalleOrden.DetalleOrdenID != null ? DetOr = this.oDetalleOrden : DetOr = new DetalleOrden();
            UserTable oUT = oUDT != null ? oUT = oUDT : oUT = new UserTable();
            string ic = !string.IsNullOrEmpty(this.ItemCode) ? ic = this.ItemCode : ic = "";
            // Se migran datos de un RecordSet a una la Lista de tipo DetalleOrden.
            var list = new List<DetalleOrden>();
            rs.MoveFirst();
            for (int i = 0; i < rs.RecordCount; i++)
            {
                string itemcode = rs.Fields.Item("Code").Value;
                this.oUDT.GetByKey(itemcode);
                UDTTooDetalleOrden();
                var temp = (DetalleOrden)this.oDetalleOrden.Clone();
                list.Add(temp);
                rs.MoveNext();
            }
            // Se devuelven los valores temporales.
            this.oDetalleOrden = DetOr.DetalleOrdenID != null ? this.oDetalleOrden = DetOr : DetOr = new DetalleOrden();
            oUT = oUT != null ? oUDT = oUDT : oUDT = new UserTable();
            this.ItemCode = !string.IsNullOrEmpty(ic) ? this.ItemCode = ic : this.ItemCode = string.Empty;
            // Se retorna la lista.
            return list;
        }
    }
}
