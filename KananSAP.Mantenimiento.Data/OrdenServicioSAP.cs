using Kanan.Mantenimiento.BO2;
using KananSAP.Vehiculos.Data;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KananSAP.Mantenimiento.Data
{
    public class OrdenServicioSAP
    {
        #region Atributos
        private const string TABLE_ORDENSERVICIO = "VSKF_ORDENSERVICIO";
        public OrdenServicio OrdenServicio { get; set; }
        public UserTable oUDT { get; set; }
        private Company oCompany;
        public string ItemCode { get; set; }
        public int? Sincronizado { get; set; }
        public Guid? UUID { get; set; }
        private SAPbouiCOM.Application SBO_Aplication;
        private SAPbobsCOM.Documents oInvoices = null;
        private int index;
        //Migracion Hana
        Recordset Insertar;
        String Parametro;
        private String NombreTabla;
        bool EsSQL;

        //Se creó para ser visible desde otros proyectos y poder realizar consultas
        // con un recordser en DB Hana
        public String NombreTablaOrdenServicioSAP = String.Empty;
        public String NombreTablaCambioEstatus = String.Empty;
        public String NombreEstatusTabla = String.Empty;
        public VehiculoSAP vehiculosap;
        #endregion

        #region Contructor
        public OrdenServicioSAP(ref Company company)
        {
            this.oCompany = company;
            try
            {
                this.oUDT = this.oCompany.UserTables.Item(TABLE_ORDENSERVICIO);

                //Migración HANA
                this.Insertar = (Recordset)this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.EsSQL = oCompany.DbServerType == BoDataServerTypes.dst_HANADB ? false : true; 
                this.NombreTabla = EsSQL ? "[@VSKF_ORDENSERVICIO]" : @"""@VSKF_ORDENSERVICIO""";
                this.NombreTablaCambioEstatus = EsSQL ? "[@VSKF_CAMBESTATUS]" : @"""@VSKF_CAMBESTATUS""";
                this.NombreEstatusTabla = EsSQL ? "[@VSKF_ESTATUSORDEN]" : @"""@VSKF_ESTATUSORDEN""";


                this.NombreTablaOrdenServicioSAP = this.NombreTabla;
            }
            catch
            {
                this.oUDT = null;
            }
            this.OrdenServicio = new OrdenServicio();
        }
        #endregion

        #region Métodos de Orden de Servicio
        public bool Existe()
        {
            return this.oUDT.GetByKey(this.ItemCode);
        }

        #region Mercancia
        public String GetCostoMercancia(String ItemCode, String WhsCode)
        {
            String Precio = String.Empty;
            try
            {
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"select ""AvgPrice"" from ""OITW"" where ""ItemCode"" = '{0}' and ""WhsCode"" = '{1}'", ItemCode, WhsCode);
                this.Insertar.DoQuery(this.Parametro);
                if (this.Insertar.RecordCount <= 0)
                    throw new Exception("No se encontró en existencia");
                try
                {
                    Precio = String.IsNullOrEmpty(this.Insertar.Fields.Item("AvgPrice").Value.ToString()) ? "0" : Convert.ToString(this.Insertar.Fields.Item("AvgPrice").Value);
                    Precio = Precio.Replace(".00", "");
                }
                catch (Exception ex)
                {
                    throw new Exception("No se recuperó el precio del artículo. " + ex.Message);
                }
                return Precio;
            }
            catch
            {
                throw new Exception("Error: existencia de factura. ");
            }
        }
        #endregion Mercancia

        #region ExisteMercancia
        public bool ExisteMercancia(String ItemCode, decimal? cantidad)
        {
            bool Existe = false;
            
            try
            {
                int? Stock = 0;
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"select ""OnHand"" from OITW where ""ItemCode"" = '{0}'", ItemCode);
                this.Insertar.DoQuery(this.Parametro);
                if (this.Insertar.RecordCount <= 0)
                    throw new Exception("No se encontró en existencia");
                try
                {
                    Stock = String.IsNullOrEmpty(this.Insertar.Fields.Item("OnHand").Value.ToString()) ? 0 : Convert.ToInt32(this.Insertar.Fields.Item("OnHand").Value);
                }
                catch (Exception ex)
                {
                    Stock = 0;
                    Existe = false;
                }

                if (Stock >=  cantidad)
                    Existe = true;
                return Existe;
            }
            catch
            {
                Existe = false;
                throw new Exception("Error: existencia de factura. ");
            }
        }
        #endregion ExisteMercancia

        #region ExisteFactura
        public bool ExisteFactura(Int32 OSID)
        {
            bool Existe = false;
            try
            {
                int? FacturaID = 0;
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"select U_DocEntry from {0} where U_OrdenServicioID = '{1}'", this.NombreTabla, OSID);
                this.Insertar.DoQuery(this.Parametro);
                try
                {
                    FacturaID = String.IsNullOrEmpty(this.Insertar.Fields.Item("U_DocEntry").Value.ToString()) ? 0 : Convert.ToInt32(this.Insertar.Fields.Item("U_DocEntry").Value);
                }
                catch
                {
                    FacturaID = 0;
                    Existe = false;
                }

                if (FacturaID > 0)
                {
                    Existe = true;
                    this.OrdenServicio.DocEntry = FacturaID;
                }
                return Existe;
            }
            catch (Exception ex)
            {
                Existe = false;
                throw new Exception("Error: existencia de factura. ");
            }
        }
        #endregion ExisteFactura

        #region GetIVAByProveedorID
        public  double? GetIVAByProveedorID(int? ProveedorID)
        {
            double? IVA = null;
            String CardCode = String.Empty;

            if (ProveedorID == null)
                return null;

            try
            {
                this.Parametro = String.Empty;
                 this.Parametro = String.Format(@"SELECT ""Code"" FROM ""@VSKF_PROVEEDOR""  WHERE U_ProveedorID = {0}", ProveedorID);

                this.Insertar.DoQuery(Parametro);
                if (this.Insertar.RecordCount > 0)
                    CardCode = this.Insertar.Fields.Item("Code").Value;

                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"SELECT ""Rate"" FROM OSTA WHERE ""Code"" = (SELECT ""VatGroup"" FROM OCRD WHERE ""CardCode"" = '{0}')", CardCode);
                
                this.Insertar.DoQuery(Parametro);
                if(this.Insertar.RecordCount > 0)
                    IVA = this.Insertar.Fields.Item("Rate").Value;

                return IVA;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al recuerar el IVA del proveedor. " + ex.Message);
            }
        }
        #endregion GetIVAByProveedorID

        private int? GetTipoMantenibleID(Type mantenible)
        {
            try
            {
                if (mantenible == typeof(Kanan.Vehiculos.BO2.Vehiculo))
                    return 1;
                if (mantenible == typeof(Kanan.Vehiculos.BO2.Caja))
                    return 2;
                if (mantenible == typeof(Kanan.Vehiculos.BO2.TipoVehiculo))
                    return 1;
                return null;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public string InsertUpdaterdenServicio(bool bInsert = true) //OrdenServicioToUDT()
        {
            string sResponse = "ERROR";
            // try
            // {
            int iCommand = 0, iCode = 0;
            //if (this.OrdenServicio.alertas.SubPropietario == null) this.OrdenServicio.alertas.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
            this.oUDT.Code = this.OrdenServicio.OrdenServicioID.ToString(); //this.ItemCode ?? this.OrdenServicio.OrdenServicioID.ToString();
            this.oUDT.Name = this.OrdenServicio.OrdenServicioID.ToString(); //this.ItemCode ?? this.OrdenServicio.OrdenServicioID.ToString();
            this.oUDT.UserFields.Fields.Item("U_OrdenServicioID").Value = this.OrdenServicio.OrdenServicioID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_EmpleadoID").Value = this.OrdenServicio.EncargadoOrdenServicio.EmpleadoID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_ServicioID").Value = this.OrdenServicio.OrdenServicioID ?? 0;
            if (this.OrdenServicio.Fecha != null)
                this.oUDT.UserFields.Fields.Item("U_Fecha").Value = this.OrdenServicio.Fecha;
            this.oUDT.UserFields.Fields.Item("U_Folio").Value = string.Format(@"{0}/{1}", this.OrdenServicio.FolioOrden.Folio, this.OrdenServicio.FolioOrden.AnioFolio);
            this.oUDT.UserFields.Fields.Item("U_Lugar").Value = this.OrdenServicio.Lugar ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_Descripcion").Value = this.OrdenServicio.Descripcion ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_SucursalID").Value = this.OrdenServicio.SucursalOrden.SucursalID ?? 0;//.alertas.SubPropietario.SucursalID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value = this.OrdenServicio.EmpresaOrden.EmpresaID ?? 0;//.alertas.empresa.EmpresaID ?? 0;
            if (this.OrdenServicio.FechaCaptura != null)
                this.oUDT.UserFields.Fields.Item("U_FechaCaptura").Value = this.OrdenServicio.FechaCaptura;
            this.oUDT.UserFields.Fields.Item("U_ProveedorID").Value = this.OrdenServicio.ProveedorServicio.ProveedorID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_AlertaAvisoID").Value = 0;//this.OrdenServicio.alertas.AlertaMantenimientoID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_KMRecorridos").Value = (double?)this.OrdenServicio.KilometrosRecorridos ?? 0;
            this.oUDT.UserFields.Fields.Item("U_Costo").Value = (double?)this.OrdenServicio.Costo ?? 0;
            //this.oUDT.UserFields.Fields.Item("U_Archivo").Value = this.OrdenServicio.ImagenArchivo ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_IVA").Value = (double?)this.OrdenServicio.Impuesto ?? 0;
            this.oUDT.UserFields.Fields.Item("U_TipoPagoID").Value = this.OrdenServicio.TipoDePago.TipoPagoID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_DetallePago").Value = this.OrdenServicio.TipoDePago.DetallePago ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_MantenibleID").Value = 0;// this.OrdenServicio.alertas.Mantenible.MantenibleID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value = 0;// (int?)this.GetTipoMantenibleID(this.OrdenServicio.alertas.Mantenible.GetType()) ?? 0;
            this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Sincronizado == null ? 0 : this.Sincronizado;
            this.oUDT.UserFields.Fields.Item("U_UUID").Value = this.UUID == null ? string.Empty : this.UUID.ToString();
            this.oUDT.UserFields.Fields.Item("U_ResponsableID").Value = this.OrdenServicio.ResponsableOrdenServicio.EmpleadoID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_AprobadorID").Value = this.OrdenServicio.AprobadorOrdenServicio.EmpleadoID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_EstatusOrdenID").Value = this.OrdenServicio.EstatusOrden.EstatusOrdenID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_FolioOrden").Value = this.OrdenServicio.FolioOrden.Folio ?? 0;
            this.oUDT.UserFields.Fields.Item("U_AnioFolio").Value = this.OrdenServicio.FolioOrden.AnioFolio ?? 0;
            this.oUDT.UserFields.Fields.Item("U_DocEntry").Value = this.OrdenServicio.DocEntry ?? 0;

            if (this.OrdenServicio.FechaRecepcion != null)
                this.oUDT.UserFields.Fields.Item("U_EnPro").Value = this.OrdenServicio.FechaRecepcion;
            if (this.OrdenServicio.FechaLiberacion != null)
                this.oUDT.UserFields.Fields.Item("U_SalPro").Value = this.OrdenServicio.FechaLiberacion;
            if (this.OrdenServicio.FechaRecepcionReal != null)
                this.oUDT.UserFields.Fields.Item("U_EnReal").Value = this.OrdenServicio.FechaRecepcionReal;
            if (this.OrdenServicio.FechaLiberacionReal != null)
                this.oUDT.UserFields.Fields.Item("U_SalReal").Value = this.OrdenServicio.FechaLiberacionReal;

            if (this.OrdenServicio.SistemaID != null)
                this.oUDT.UserFields.Fields.Item("U_SistemaID").Value = Convert.ToInt32(this.OrdenServicio.SistemaID);

            if (this.OrdenServicio.ComponenteID != null)
                this.oUDT.UserFields.Fields.Item("U_ComponenteID").Value = Convert.ToInt32(this.OrdenServicio.ComponenteID);

            if (this.OrdenServicio.PrioridadID != null)
                this.oUDT.UserFields.Fields.Item("U_PrioridadID").Value = Convert.ToInt32(this.OrdenServicio.PrioridadID);

            if (!string.IsNullOrEmpty(this.OrdenServicio.socioCliente))
                this.oUDT.UserFields.Fields.Item("U_ClienteSN").Value = this.OrdenServicio.socioCliente;

            if (!string.IsNullOrEmpty(this.OrdenServicio.inspeccionID))
                this.oUDT.UserFields.Fields.Item("U_inspeccionID").Value = this.OrdenServicio.inspeccionID;

            if (!string.IsNullOrEmpty(this.OrdenServicio.HoraEnProgramada))
                this.oUDT.UserFields.Fields.Item("U_HoraEnP").Value = this.OrdenServicio.HoraEnProgramada;

            if (!string.IsNullOrEmpty(this.OrdenServicio.HoraSalProgramada))
                this.oUDT.UserFields.Fields.Item("U_HoraSalP").Value = this.OrdenServicio.HoraSalProgramada;

            if (!string.IsNullOrEmpty(this.OrdenServicio.HoraEnReal))
                this.oUDT.UserFields.Fields.Item("U_HoraEnR").Value = this.OrdenServicio.HoraEnReal;

            if (!string.IsNullOrEmpty(this.OrdenServicio.HoraSalReal))
                this.oUDT.UserFields.Fields.Item("U_HoraSalR").Value = this.OrdenServicio.HoraSalReal;

            //if (this.OrdenServicio.FacturableCliente)
                this.oUDT.UserFields.Fields.Item("U_Facturable").Value = Convert.ToInt32(this.OrdenServicio.FacturableCliente);

            iCommand = bInsert ? this.oUDT.Add() : this.oUDT.Update();
            if (iCommand != 0)
                this.oCompany.GetLastError(out iCode, out sResponse);
            else sResponse = "OK";
            //}
            //catch (Exception ex) { throw new Exception(ex.Message); }
            return sResponse;
        }

        public void UDTToOrdenServicio()
        {
            try
            {
                this.OrdenServicio = new OrdenServicio();

                this.OrdenServicio.OrdenServicioID = Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_servicioID").Value) ?? null; 
                this.OrdenServicio.EncargadoOrdenServicio.EmpleadoID = this.oUDT.UserFields.Fields.Item("U_EmpleadoID").Value ?? null;
                //this.OrdenServicio.alertas.servicio.ServicioID = this.oUDT.UserFields.Fields.Item("U_ServicioID").Value ?? null;
                this.OrdenServicio.Fecha = this.oUDT.UserFields.Fields.Item("U_Fecha").Value ?? null;
                this.OrdenServicio.Folio = this.oUDT.UserFields.Fields.Item("U_Folio").Value ?? null;
                this.OrdenServicio.Lugar = this.oUDT.UserFields.Fields.Item("U_Lugar").Value ?? null;
                this.OrdenServicio.Descripcion = this.oUDT.UserFields.Fields.Item("U_Descripcion").Value ?? null;
                this.OrdenServicio.SucursalOrden.SucursalID = this.oUDT.UserFields.Fields.Item("U_SucursalID").Value ?? null;
                //this.OrdenServicio.alertas.empresa.EmpresaID  = this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value ?? null;
                this.OrdenServicio.FechaCaptura = this.oUDT.UserFields.Fields.Item("U_FechaCaptura").Value ?? null;
                this.OrdenServicio.ProveedorServicio.ProveedorID  = this.oUDT.UserFields.Fields.Item("U_ProveedorID").Value ?? null;
                //this.OrdenServicio.alertas.AlertaMantenimientoID = this.oUDT.UserFields.Fields.Item("U_AlertaAvisoID").Value ?? null;
                this.OrdenServicio.KilometrosRecorridos = (decimal?)Convert.ChangeType(this.oUDT.UserFields.Fields.Item("U_KMRecorridos").Value, typeof(decimal)) ?? null;
                this.OrdenServicio.Costo = (decimal?)Convert.ChangeType(this.oUDT.UserFields.Fields.Item("U_Costo").Value, typeof(decimal)) ?? null;
                //this.OrdenServicio.ImagenArchivo = null;
                this.OrdenServicio.Impuesto = (decimal?)Convert.ChangeType(this.oUDT.UserFields.Fields.Item("U_IVA").Value, typeof(decimal)) ?? null;
                this.OrdenServicio.TipoDePago.TipoPagoID = this.oUDT.UserFields.Fields.Item("U_TipoPagoID").Value ?? null;
                this.OrdenServicio.TipoDePago.DetallePago = this.oUDT.UserFields.Fields.Item("U_DetallePago").Value ?? null;
                //this.OrdenServicio.alertas.Mantenible.MantenibleID  = this.oUDT.UserFields.Fields.Item("U_MantenibleID").Value ?? null;
                //this.OrdenServicio.alertas.Mantenible.MantenibleID = this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value ?? null;
                this.Sincronizado = this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value ?? null;
                if (!string.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_UUID").Value))
                    this.UUID = Guid.Parse(this.oUDT.UserFields.Fields.Item("U_UUID").Value);
                this.OrdenServicio.ResponsableOrdenServicio.EmpleadoID = this.oUDT.UserFields.Fields.Item("U_ResponsableID").Value ?? null;
                this.OrdenServicio.AprobadorOrdenServicio.EmpleadoID = this.oUDT.UserFields.Fields.Item("U_AprobadorID").Value ?? null;
                this.OrdenServicio.EstatusOrden.EstatusOrdenID = this.oUDT.UserFields.Fields.Item("U_EstatusOrdenID").Value ?? null;
                this.OrdenServicio.FolioOrden.Folio = this.oUDT.UserFields.Fields.Item("U_FolioOrden").Value ?? null;
                this.OrdenServicio.FolioOrden.AnioFolio = this.oUDT.UserFields.Fields.Item("U_AnioFolio").Value ?? null;
                this.OrdenServicio.DocEntry = this.oUDT.UserFields.Fields.Item("U_DocEntry").Value ?? null;
                this.OrdenServicio.Code = this.oUDT.UserFields.Fields.Item("U_OrdenServicioID").Value ?? null;
                this.OrdenServicio.SistemaID = Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_SistemaID").Value) ?? 0;
                this.OrdenServicio.ComponenteID = Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_ComponenteID").Value) ?? 0;                
                this.OrdenServicio.PrioridadID = Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_PrioridadID").Value) ?? 0;
                this.OrdenServicio.socioCliente = this.oUDT.UserFields.Fields.Item("U_ClienteSN").Value ?? null;
                this.OrdenServicio.inspeccionID = this.oUDT.UserFields.Fields.Item("U_inspeccionID").Value ?? null;
                this.OrdenServicio.HoraEnProgramada = this.oUDT.UserFields.Fields.Item("U_HoraEnP").Value ?? null;
                this.OrdenServicio.HoraSalProgramada = this.oUDT.UserFields.Fields.Item("U_HoraSalP").Value ?? null;
                this.OrdenServicio.HoraEnReal = this.oUDT.UserFields.Fields.Item("U_HoraEnR").Value ?? null;
                this.OrdenServicio.HoraSalReal = this.oUDT.UserFields.Fields.Item("U_HoraSalR").Value ?? null;
                this.OrdenServicio.FacturableCliente = Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_Facturable").Value) ?? 0;

                try
                {                    
                    this.OrdenServicio.FechaRecepcion = Convert.ToDateTime(this.oUDT.UserFields.Fields.Item("U_EnPro").Value) ?? null;                    
                    this.OrdenServicio.FechaLiberacion = Convert.ToDateTime(this.oUDT.UserFields.Fields.Item("U_SalPro").Value) ?? null;                    
                    this.OrdenServicio.FechaRecepcionReal = Convert.ToDateTime(this.oUDT.UserFields.Fields.Item("U_EnReal").Value) ?? null;                    
                    this.OrdenServicio.FechaLiberacionReal = Convert.ToDateTime(this.oUDT.UserFields.Fields.Item("U_SalReal").Value) ?? null;
                }
                catch { }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void EstatusEliminar(Int32 e)
        {
            Int32 ID = e;
            this.Parametro = String.Empty;
            this.Parametro = String.Format(@"UPDATE {0} SET U_EstatusOrdenID = 4 WHERE ""Code"" = '{1}'", this.NombreTabla, ID);

            this.Insertar.DoQuery(this.Parametro);
        }

        public void EstatusRechazada(Int32 r)
        {
            Int32 ID = r;
            this.Parametro = String.Empty;
            this.Parametro = String.Format(@"UPDATE {0} SET U_EstatusOrdenID = 3 WHERE ""Code"" = '{1}'", this.NombreTabla, ID);

            this.Insertar.DoQuery(this.Parametro);
        }

        public void EstatusRealizada(Int32 r)
        {
            Int32 ID = r;
            Int32 FacturaID = 0;
            this.Parametro = String.Empty;
            this.Parametro = String.Format(@"SELECT U_DocEntry FROM {0} WHERE ""Code"" = '{1}'", this.NombreTabla, ID);
            this.Insertar.DoQuery(this.Parametro);
            FacturaID = Convert.ToInt32(this.Insertar.Fields.Item("U_DocEntry").Value);
            if (FacturaID > 0 )
            {
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET U_EstatusOrdenID = 6 WHERE ""Code"" = '{1}'", this.NombreTabla, ID);
                this.Insertar.DoQuery(this.Parametro);
            }
            else
            {
                throw new Exception("No es posible realizar una orden si no está facturada. ");
            }
        }

        public bool SetUDTPMantenimientoID(int? id)
        {
            try
            {
                var rs = this.ConsultarByID(id);
                if (HashOrdenServicio(rs))
                {
                    rs.MoveFirst();
                    int _id = rs.Fields.Item("U_OrdenServicioID").Value;
                    if (_id == id)
                        return this.oUDT.GetByKey(rs.Fields.Item("Code").Value);
                }
                this.oUDT.Code = string.Empty;
                return false;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public Boolean CreateInvoice(OrdenServicio Orden, bool bSQLConnection, string sCentroCosto, int iCcosto, bool bDraft)
        {
            Boolean bContinuar = true;
            try
            {

                int DocEntry = 0;
                if (!bDraft)
                {
                    oInvoices = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseInvoices);
                    oInvoices.DocType = BoDocumentTypes.dDocument_Service;
                }
                else
                {
                    oInvoices = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts);
                    oInvoices.DocObjectCode = SAPbobsCOM.BoObjectTypes.oPurchaseInvoices;
                    oInvoices.DocType = BoDocumentTypes.dDocument_Service;
                }
                string CardCode = "";
                if (this.RecuperaProveedor(Orden.ProveedorServicio.ProveedorID, bSQLConnection, ref CardCode) != "OK")
                    throw new Exception("No se ha encontrado el proveedor");

                oInvoices.CardCode = CardCode;
                string sComments = string.Format("Creado por addon Kanan Fleet {0}", Orden.Descripcion.Trim().ToString());
                string fecha = Convert.ToDateTime(Orden.Fecha.ToString()).ToString("dd/MM/yyyy");
                string FechaCaptura = Convert.ToDateTime(Orden.FechaCaptura.ToString()).ToString("dd/MM/yyyy");
                oInvoices.DocDueDate = Convert.ToDateTime(fecha);
                oInvoices.TaxDate = Convert.ToDateTime(FechaCaptura);
                oInvoices.DocDate = Convert.ToDateTime(FechaCaptura);
                oInvoices.NumAtCard = string.Format("{0}/{1}", Orden.FolioOrden.Folio, Orden.FolioOrden.AnioFolio);
                oInvoices.Comments = sComments.Length > 254 ? sComments.Substring(0, 254) : sComments;
                foreach (DetalleOrden data in Orden.DetalleOrden)
                {
                    //RECUPERAR CUENTA CONTABLE SERVICIO
                    string AcctCode = "";
                    if (this.RecuperaCuenta(data.Servicio.ServicioID, bSQLConnection, ref AcctCode) != "OK")
                        throw new Exception(string.Format("No se ha encontrado la cuenta contable para el servicio {0}", data.Servicio.Nombre));

                    //Se obtiene el mantenible.
                    int MantenibleID = Convert.ToInt32(data.Mantenible.MantenibleID);

                    //Recuperar placa del vehículo
                    string Placa = string.Empty, sVehiculo = string.Empty;
                    RecuperaPlacaVehiculo(MantenibleID, out Placa, sVehiculo);
                    if (!bDraft)
                    {
                        if (!string.IsNullOrEmpty(Placa))
                            oInvoices.Lines.UserFields.Fields.Item("U_VSKF_VH").Value = Placa;
                        //Recuperar código de artículo

                        if (!string.IsNullOrEmpty(sVehiculo))
                            oInvoices.Lines.UserFields.Fields.Item("U_VSKF_VHNoEco").Value = sVehiculo;
                    }

                    //Recuperar centro de costos
                    sCentroCosto = String.Empty;
                    sCentroCosto = RecuperaCentroCostos(MantenibleID);
                    oInvoices.Lines.AccountCode = AcctCode;
                    oInvoices.Lines.ItemDescription = data.Servicio.Nombre;

                    switch (iCcosto)
                    {
                        case 1:
                        case 0:
                            oInvoices.Lines.CostingCode = sCentroCosto;
                            break;
                        case 2:
                            oInvoices.Lines.CostingCode2 = sCentroCosto;
                            break;
                        case 3:
                            oInvoices.Lines.CostingCode3 = sCentroCosto;
                            break;
                        case 4:
                            oInvoices.Lines.CostingCode4 = sCentroCosto;
                            break;
                        case 5:
                            oInvoices.Lines.CostingCode5 = sCentroCosto;
                            break;
                    }

                    
                    //El costo se toma del valor actual que semuestra en el formulario.
                    oInvoices.Lines.LineTotal = Convert.ToDouble(Orden.Costo);
                    oInvoices.Lines.Quantity = 1;
                    oInvoices.Lines.Add();
                }

                int iSave = oInvoices.Add();
                if (iSave != 0)
                {
                    int iCode = 0; string sResponse = string.Empty;
                    oCompany.GetLastError(out iCode, out sResponse);
                    throw new Exception("No se ha podido crear la factura de proveedor " + sResponse);
                }
                DocEntry = this.RecuperaDocEntryFactura();
                if (DocEntry > 0)
                {
                    this.OrdenServicio.DocEntry = DocEntry;
                    this.LigarFactura(Convert.ToInt32(Orden.OrdenServicioID), DocEntry);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                bContinuar = false;
            }
            return bContinuar;
        }

        public int RecuperaDocEntryFactura()
        {
            int DocEntry = 0;
            try
            {
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"SELECT TOP 1 ""DocEntry"" FROM OPCH ORDER BY ""DocEntry"" DESC");
                this.Insertar.DoQuery(this.Parametro);
                DocEntry = this.Insertar.Fields.Item("DocEntry").Value;
                return DocEntry;
            }
            catch(Exception ex)
            {
                throw new Exception("Error al intentar obtener el identificador de la factura. " + ex.Message);
            }
        }

        public void RecuperaPlacaVehiculo(int mantenible, out string sPlaca, string sVehiculo)
        {
            
            String Sentencia = String.Empty;
            sPlaca = string.Empty;
            sVehiculo = string.Empty;
            Sentencia = string.Format(@"SELECT U_PLACA, ""Code"" FROM ""@VSKF_VEHICULO""  WHERE U_VehiculoID = {0}", mantenible);
            Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            rs.DoQuery(Sentencia);
            if (rs.RecordCount > 0)
            {
                sPlaca = rs.Fields.Item("U_PLACA").Value;
                sVehiculo = rs.Fields.Item("Code").Value;
            }
            
        }

        public String RecuperaItemCode(int? mantenibleid)
        {
            String Placa = String.Empty;
            String Sentencia = String.Empty;

            Sentencia = string.Format(@"SELECT U_PLACA FROM ""@VSKF_VEHICULO""  WHERE U_VehiculoID = {0}", mantenibleid);
            Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            rs.DoQuery(Sentencia);
            if (rs.RecordCount > 0)
                Placa = rs.Fields.Item("U_PLACA").Value;

            return Placa;
        }

        public String RecuperaCentroCostos(int? mantenibleid)
        {
            String CentroCostos = String.Empty;
            vehiculosap = new VehiculoSAP(this.oCompany);
            CentroCostos = vehiculosap.GetCentroCostosByID(mantenibleid);
            return CentroCostos;
        }

        private String RecuperaCuenta(int? servicioID, Boolean bSQLConnection, ref  string oAcctCode)
        {
            String oResponse = "OK";
            String strQuery = "";
            try
            {
                strQuery = string.Format(@"SELECT U_CuentaContable FROM ""@VSKF_SERVICIOS""  WHERE U_ServicioID = {0}", servicioID);
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(strQuery);
                if (rs.RecordCount > 0)
                    oAcctCode = rs.Fields.Item("U_CuentaContable").Value;
                else oResponse = "ERROR";
            }
            catch (Exception ex)
            {
                oResponse = ex.Message;
            }

            return oResponse;
        }

        public String RecuperaProveedor(int? ProveedorID, Boolean bSQLConnection, ref  string CardCode, bool bOrdenes = true)
        {
            String oResponse = "OK";
            String strQuery = "";
            try
            {
                strQuery = string.Format(@"SELECT ""Code"" FROM ""@VSKF_PROVEEDOR""  WHERE U_ProveedorID = {0}", ProveedorID);
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(strQuery);
                if (rs.RecordCount > 0)
                    CardCode = rs.Fields.Item("Code").Value;

                else
                {
                    if (bOrdenes)
                        oResponse = "ERROR";
                    else
                        oResponse = "Desconocido";
                }
            }
            catch (Exception ex)
            {
                oResponse = ex.Message;
            }

            return oResponse;
        }

        public String GetNameTipoPago(int? TipoPagoID)
        {
            String Nombre = String.Empty;
            try
            {
                if (TipoPagoID == null)
                    return String.Empty;

                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"SELECT ""Name"" FROM {0} WHERE ""Code"" = {1}", this.NombreTabla, TipoPagoID);
                this.Insertar.DoQuery(Parametro);
                if (this.Insertar.RecordCount > 0)
                    Nombre = this.Insertar.Fields.Item("Name").Value;

                return Nombre;
            }
            catch (Exception ex)
            { throw new Exception("Error al obtener el nombre del tipo pago. " + ex.Message); }
        }

        public Recordset ConsultarByID(int? id)
        {
            try
            {
                string query = string.Format("select * from " + this.NombreTabla + " where U_OrdenServicioID = {0}", id);
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                return rs;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public bool HashOrdenServicio(Recordset rs)
        {
            if (rs.RecordCount > 0)
                return true;
            return false;
        }

        public void LigarFactura(Int32 OrdenServicioID, Int32 IDFactura)
        {
            try
            {
                this.Parametro = String.Empty;
                this.Parametro = String.Format("update {0} set U_DocEntry = '{1}' where U_OrdenServicioID = '{2}'", this.NombreTabla, IDFactura, OrdenServicioID);
                this.Insertar.DoQuery(this.Parametro);
            }
            catch (Exception ex)
            {
                throw new Exception("Erro al intentar ligar la orden de servicio con la factura. " + ex.Message);
            }
        }

        public void InsertCambioEstatus(int? estatus, DateTime fecha, int? OrdenServicioID)
        {
            /*Valida el número de estatus.*/
            if (estatus <= 0 | estatus > 6)
                throw new Exception("Error al insertar en CAMBIOESTATUD.");

            String Hora, Min, Seg, sQuery;
            Hora = String.Empty;
            Min = String.Empty;
            Seg = String.Empty;

            Hora = fecha.Hour.ToString().Count() < 2 ? "0" + fecha.Hour.ToString() : fecha.Hour.ToString();
            Min = fecha.Minute.ToString().Count() < 2 ? "0" + fecha.Minute.ToString() : fecha.Minute.ToString();
            Seg = fecha.Second.ToString().Count() < 2 ? "0" + fecha.Second.ToString() : fecha.Second.ToString();

            String HrMinSegString = Hora + Min + Seg;
            int horaminutosegundo = Convert.ToInt32(HrMinSegString);
            if (OrdenServicioID == null)
                OrdenServicioID = 0;

            /*Comprueba si existen registros.*/
            sQuery = String.Format("SELECT * FROM {0} WHERE U_IDorSer = {1} ORDER BY U_FechaCambio, U_HoraMinuto DESC", this.NombreTablaCambioEstatus, OrdenServicioID);
            Insertar.DoQuery(sQuery);
            if (Insertar.RecordCount > 0)
            {
                /*Movemos al registro más actual*/
                Insertar.MoveFirst();

                DateTime FechaRegistro = Insertar.Fields.Item("U_FechaCambio").Value;
                int HoraMinutoSegundoRegistro = Insertar.Fields.Item("U_HoraMinuto").Value;

                /*Se valida que la fecha sea mayor o igual con horaminutos diferente*/
                if (fecha >= FechaRegistro)
                {
                    /*Se valida que sea en minutosegundo mayor*/
                    if (horaminutosegundo > HoraMinutoSegundoRegistro)
                    {
                        /*Se valida que el estatus sea el mayor*/
                        int estatusRegistro = Insertar.Fields.Item("U_Estatus").Value;
                        if (estatus > estatusRegistro)
                        {
                            /*Se valida contador*/
                            Insertar.DoQuery(String.Format("SELECT * FROM " + this.NombreTablaCambioEstatus));
                            index = Insertar.RecordCount + 1;

                            this.Parametro = String.Format("INSERT INTO {0} VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}')", this.NombreTablaCambioEstatus, index, index, estatus, fecha.ToString("yyyy-MM-dd"), horaminutosegundo, OrdenServicioID);

                            try
                            {
                                this.oCompany.StartTransaction();
                                this.Insertar.DoQuery(this.Parametro);
                                if (this.oCompany.InTransaction)
                                    this.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                            }
                            catch
                            {
                                if (this.oCompany.InTransaction)
                                    this.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                            }
                        }
                    }
                }
            }
            else
            {
                Insertar.DoQuery(String.Format("SELECT * FROM " + this.NombreTablaCambioEstatus));
                index = Insertar.RecordCount + 1;

                this.Parametro = String.Format("INSERT INTO {0} VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}')", this.NombreTablaCambioEstatus, index, index, estatus, fecha.ToString("yyyy-MM-dd"), horaminutosegundo, OrdenServicioID);

                this.Insertar.DoQuery(this.Parametro);
            }
        }

        /*public void Insert()
        {
            try
            {
                String Fecha, FechaCaptura, EnPro, SalPro, EnReal, SalReal = String.Empty;
                this.Parametro = String.Empty;
                int? DocEntry = null;

                #region Validaciones
                if (this.OrdenServicio.FechaRecepcion != null)
                    EnPro = this.OrdenServicio.FechaRecepcion.Value.ToString("yyyy-MM-dd");
                else
                    EnPro = String.Empty;
                if (this.OrdenServicio.FechaLiberacion != null)
                    SalPro = this.OrdenServicio.FechaLiberacion.Value.ToString("yyyy-MM-dd");
                else
                    SalPro = String.Empty;
                if (this.OrdenServicio.FechaRecepcionReal != null)
                    EnReal = this.OrdenServicio.FechaRecepcionReal.Value.ToString("yyyy-MM-dd");
                else
                    EnReal = String.Empty;
                if (this.OrdenServicio.FechaLiberacionReal != null)
                    SalReal = this.OrdenServicio.FechaLiberacionReal.Value.ToString("yyyy-MM-dd");
                else
                    SalReal = String.Empty;
                if (this.OrdenServicio.DocEntry > 0)
                    DocEntry = Convert.ToInt32(this.OrdenServicio.DocEntry);
                if (this.OrdenServicio.Fecha != null)
                    Fecha = this.OrdenServicio.Fecha.Value.ToString("yyyy-MM-dd");
                else
                    Fecha = String.Empty;
                if (this.OrdenServicio.FechaCaptura != null)
                    FechaCaptura = this.OrdenServicio.FechaCaptura.Value.ToString("yyyy-MM-dd");
                else
                    FechaCaptura = String.Empty;
                #endregion Validaciones

                #region Hana
                this.Parametro = String.Format("INSERT INTO {0} VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', '{19}', '{20}', '{21}', '{22}', '{23}', '{24}', '{25}', '{26}', '{27}', '{28}', '{29}', '{30}', {31}, {32}, {33}, {34})", this.NombreTabla, this.oUDT.Code, this.oUDT.Name, this.OrdenServicio.OrdenServicioID, this.OrdenServicio.EncargadoOrdenServicio.EmpleadoID, /ServicioID = / 0, Fecha, this.OrdenServicio.Folio, "", this.OrdenServicio.Descripcion, this.OrdenServicio.SucursalOrden.SucursalID, this.OrdenServicio.EmpresaOrden.EmpresaID, FechaCaptura, this.OrdenServicio.ProveedorServicio.ProveedorID, 0, Convert.ToDouble(this.OrdenServicio.KilometrosRecorridos), Convert.ToDouble(this.OrdenServicio.Costo), null, Convert.ToDouble(this.OrdenServicio.Impuesto), this.OrdenServicio.TipoDePago.TipoPagoID, this.OrdenServicio.TipoDePago.DetallePago, 0, 0, this.OrdenServicio.ResponsableOrdenServicio.EmpleadoID, this.OrdenServicio.AprobadorOrdenServicio.EmpleadoID, this.OrdenServicio.EstatusOrden.EstatusOrdenID, this.OrdenServicio.FolioOrden.Folio, this.OrdenServicio.FolioOrden.AnioFolio, Convert.ToInt32(this.Sincronizado), Convert.ToString(this.UUID), DocEntry, !String.IsNullOrEmpty(EnPro) ? "'" + EnPro + "'" : "NULL", !String.IsNullOrEmpty(SalPro) ? "'" + SalPro + "'" : "NULL", !String.IsNullOrEmpty(EnReal) ? "'" + EnReal + "'" : "NULL", !String.IsNullOrEmpty(SalReal) ? "'" + SalReal + "'" : "NULL");
                this.Insertar.DoQuery(this.Parametro);
                #endregion Hana
            }
            catch (Exception ex)
            {
                #region SQL
                int result = -1;
                result = this.oUDT.Add();
                if (result != 0)
                {
                    int errornum;
                    string errormsj;
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    //throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                SBO_Aplication.StatusBar.SetText("Error al insertar orden. " + ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                return;
                #endregion SQL
            }
        }*/

        public bool InsertaPlantilla(string sCode, string sName)
        {
            bool bContinuar = false;
            string sResponse = "ERROR";
            int iCommand = 0, iCode = 0;
            UserTable oTabla = this.oCompany.UserTables.Item("VSKF_OSPLANTILLA");
            oTabla.Code = sCode;
            oTabla.Name = sName;
            oTabla.UserFields.Fields.Item("U_SucursalID").Value = this.OrdenServicio.SucursalOrden.SucursalID;
            oTabla.UserFields.Fields.Item("U_TallerInterno").Value = this.OrdenServicio.TallerInterno ? "Y" : "N";
            oTabla.UserFields.Fields.Item("U_EntradaProgramada").Value = Convert.ToDateTime( this.OrdenServicio.FechaRecepcion);
            oTabla.UserFields.Fields.Item("U_SalidaProgramada").Value = Convert.ToDateTime(this.OrdenServicio.FechaLiberacion);
            oTabla.UserFields.Fields.Item("U_FechaRevision").Value = Convert.ToDateTime(this.OrdenServicio.Fecha);
            oTabla.UserFields.Fields.Item("U_Descripcion").Value = this.OrdenServicio.Descripcion;
            //oTabla.UserFields.Fields.Item("U_NumCapas").Value = this.OrdenServicio.;
            oTabla.UserFields.Fields.Item("U_ResponsableID").Value = this.OrdenServicio.ResponsableOrdenServicio.EmpleadoID;
            oTabla.UserFields.Fields.Item("U_AprobadorID").Value = this.OrdenServicio.AprobadorOrdenServicio.EmpleadoID;
            iCommand = oTabla.Add();
            if (iCommand != 0)
                this.oCompany.GetLastError(out iCode, out sResponse);
            else bContinuar = true;
            return bContinuar;
        }
        public void Actualizar()
        {
            try
            {

                bool bContinuar = false;
                string sResponse = "ERROR";
                int iCommand = 0, iCode = 0;
                UserTable oTabla = this.oCompany.UserTables.Item("VSKF_ORDENSERVICIO");
                oTabla.GetByKey(this.oUDT.Code.ToString());

                if (this.OrdenServicio.EncargadoOrdenServicio.EmpleadoID != null)
                {
                    if (this.OrdenServicio.EncargadoOrdenServicio.EmpleadoID > 0)
                        oTabla.UserFields.Fields.Item("U_EmpleadoID").Value = Convert.ToInt32(this.OrdenServicio.EncargadoOrdenServicio.EmpleadoID);
                }

                if (!string.IsNullOrEmpty(this.OrdenServicio.Descripcion))
                    oTabla.UserFields.Fields.Item("U_Descripcion").Value = this.OrdenServicio.Descripcion;

                if (this.OrdenServicio.SucursalOrden.SucursalID != null)
                {
                    if (this.OrdenServicio.SucursalOrden.SucursalID > 0)
                        oTabla.UserFields.Fields.Item("U_SucursalID").Value = Convert.ToInt32(this.OrdenServicio.SucursalOrden.SucursalID);
                }

                if (this.OrdenServicio.FechaCaptura != null)
                {
                    if (this.OrdenServicio.FechaCaptura != DateTime.MinValue)
                        oTabla.UserFields.Fields.Item("U_FechaCaptura").Value = Convert.ToDateTime(this.OrdenServicio.FechaCaptura);
                }

                if (this.OrdenServicio.ProveedorServicio.ProveedorID != null)
                {
                    if (this.OrdenServicio.ProveedorServicio.ProveedorID > 0)
                        oTabla.UserFields.Fields.Item("U_ProveedorID").Value = Convert.ToInt32(this.OrdenServicio.ProveedorServicio.ProveedorID);
                }

                if (this.OrdenServicio.KilometrosRecorridos != null)
                {
                    if (this.OrdenServicio.KilometrosRecorridos > 0)
                        oTabla.UserFields.Fields.Item("U_KMRecorridos").Value = Convert.ToDouble(this.OrdenServicio.KilometrosRecorridos);
                }

                if (this.OrdenServicio.Costo != null)
                {
                    if (this.OrdenServicio.Costo > 0)
                        oTabla.UserFields.Fields.Item("U_Costo").Value = Convert.ToDouble(this.OrdenServicio.Costo);
                }

                if (this.OrdenServicio.ResponsableOrdenServicio.EmpleadoID != null)
                {
                    if (this.OrdenServicio.ResponsableOrdenServicio.EmpleadoID > 0)
                        oTabla.UserFields.Fields.Item("U_ResponsableID").Value = Convert.ToInt32(this.OrdenServicio.ResponsableOrdenServicio.EmpleadoID);
                }

                if (this.OrdenServicio.AprobadorOrdenServicio.EmpleadoID != null)
                {
                    if (this.OrdenServicio.AprobadorOrdenServicio.EmpleadoID > 0)
                        oTabla.UserFields.Fields.Item("U_AprobadorID").Value = Convert.ToInt32(this.OrdenServicio.AprobadorOrdenServicio.EmpleadoID);
                }

                if (this.OrdenServicio.Fecha.Value != null)
                {
                    if (this.OrdenServicio.Fecha.Value != DateTime.MinValue)
                        oTabla.UserFields.Fields.Item("U_Fecha").Value = Convert.ToDateTime(this.OrdenServicio.Fecha.Value);
                }

                if (this.OrdenServicio.EstatusOrden.EstatusOrdenID != null)
                {
                    if (this.OrdenServicio.EstatusOrden.EstatusOrdenID > 0)
                        oTabla.UserFields.Fields.Item("U_EstatusOrdenID").Value = Convert.ToInt32(this.OrdenServicio.EstatusOrden.EstatusOrdenID);
                }

                if (this.OrdenServicio.FechaLiberacion != null)
                {
                    if (this.OrdenServicio.Fecha.Value != DateTime.MinValue)
                        oTabla.UserFields.Fields.Item("U_SalPro").Value = Convert.ToDateTime(this.OrdenServicio.Fecha.Value);
                }
                if (this.OrdenServicio.FechaRecepcionReal != null)
                {
                    if (this.OrdenServicio.Fecha.Value != DateTime.MinValue)
                        oTabla.UserFields.Fields.Item("U_EnReal").Value = Convert.ToDateTime(this.OrdenServicio.Fecha.Value);
                }
                if (this.OrdenServicio.FechaLiberacionReal != null)
                {
                    if (this.OrdenServicio.Fecha.Value != DateTime.MinValue)
                        oTabla.UserFields.Fields.Item("U_SalReal").Value = Convert.ToDateTime(this.OrdenServicio.Fecha.Value);
                }

                if (this.OrdenServicio.FechaLiberacionReal != null)
                {
                    if (this.OrdenServicio.Fecha.Value != DateTime.MinValue)
                        oTabla.UserFields.Fields.Item("U_SalReal").Value = Convert.ToDateTime(this.OrdenServicio.Fecha.Value);
                }

                if (this.OrdenServicio.FolioOrden.AnioFolio != null)
                {
                    try
                    {
                        if (this.OrdenServicio.FolioOrden.AnioFolio > 0)
                            oTabla.UserFields.Fields.Item("U_Folio").Value = string.Format(@"{0}/{1}", this.OrdenServicio.FolioOrden.Folio, this.OrdenServicio.FolioOrden.AnioFolio);
                    }
                    catch { }
                }

                if (this.OrdenServicio.TipoDePago.TipoPagoID != null)
                {
                    if (this.OrdenServicio.TipoDePago.TipoPagoID > 0)
                        oTabla.UserFields.Fields.Item("U_TipoPagoID").Value = Convert.ToString(this.OrdenServicio.TipoDePago.TipoPagoID);
                }

                if (!string.IsNullOrEmpty(this.OrdenServicio.TipoDePago.DetallePago))
                    oTabla.UserFields.Fields.Item("U_DetallePago").Value = this.OrdenServicio.TipoDePago.DetallePago;

                if (this.OrdenServicio.DocEntry != null)
                {
                    if (this.OrdenServicio.DocEntry > 0)
                        oTabla.UserFields.Fields.Item("U_DocEntry").Value = Convert.ToInt32(this.OrdenServicio.DocEntry);
                }

                if (this.OrdenServicio.SistemaID != null)
                    oTabla.UserFields.Fields.Item("U_SistemaID").Value = Convert.ToInt32(this.OrdenServicio.SistemaID);

                if (this.OrdenServicio.ComponenteID != null)
                    oTabla.UserFields.Fields.Item("U_ComponenteID").Value = Convert.ToInt32(this.OrdenServicio.ComponenteID);

                if (this.OrdenServicio.PrioridadID != null)
                    oTabla.UserFields.Fields.Item("U_PrioridadID").Value = Convert.ToInt32(this.OrdenServicio.PrioridadID);

                if (!string.IsNullOrEmpty(this.OrdenServicio.socioCliente))
                    oTabla.UserFields.Fields.Item("U_ClienteSN").Value = this.OrdenServicio.socioCliente;

                if (!string.IsNullOrEmpty(this.OrdenServicio.inspeccionID))
                    oTabla.UserFields.Fields.Item("U_inspeccionID").Value = this.OrdenServicio.inspeccionID;

                if (!string.IsNullOrEmpty(this.OrdenServicio.HoraEnProgramada))
                    oTabla.UserFields.Fields.Item("U_HoraEnP").Value = this.OrdenServicio.HoraEnProgramada;

                if (!string.IsNullOrEmpty(this.OrdenServicio.HoraSalProgramada))
                    oTabla.UserFields.Fields.Item("U_HoraSalP").Value = this.OrdenServicio.HoraSalProgramada;

                if (!string.IsNullOrEmpty(this.OrdenServicio.HoraEnReal))
                    oTabla.UserFields.Fields.Item("U_HoraEnR").Value = this.OrdenServicio.HoraEnReal;

                if (!string.IsNullOrEmpty(this.OrdenServicio.HoraSalReal))
                    oTabla.UserFields.Fields.Item("U_HoraSalR").Value = this.OrdenServicio.HoraSalReal;

                if (this.OrdenServicio.FacturableCliente != null)
                    oTabla.UserFields.Fields.Item("U_Facturable").Value = Convert.ToInt32(this.OrdenServicio.FacturableCliente);
                

                iCommand = oTabla.Update();
                if (iCommand != 0)
                    this.oCompany.GetLastError(out iCode, out sResponse);
                else bContinuar = true;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar()
        {
            try
            {
                String OrdenServicioID;
                this.Parametro = String.Empty;

                #region Hana
                if (this.oUDT.Code != null)
                    OrdenServicioID = this.oUDT.Code;

                this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code"" = '{1}'", this.NombreTabla, this.oUDT.Code);

                this.Insertar.DoQuery(this.Parametro);
                #endregion Hana
            }
            catch (Exception ex)
            {
                #region SQL
                int result = -1;
                int errornum;
                string errormsj;
                result = this.oUDT.Remove();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    SBO_Aplication.StatusBar.SetText("Error al eliminar orden. " + ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                    return;
                }
                #endregion SQL
            }
        }

        public Recordset ConsultarTodos()
        {
            Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            rs.DoQuery("SELECT * FROM " + this.NombreTabla + " ");
            return rs;
        }

        public Recordset Consultar(OrdenServicio orden)
        {
            Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            try
            {
                
                string sConsultaSQl = string.Empty, sCampoSql = string.Empty;
                bool bAnd = false;
                sConsultaSQl = string.Format("SELECT * FROM {0}", this.NombreTabla);

                #region filtros and where
                if (orden.EstatusOrden.EstatusOrdenID != null)
                {
                    if (orden.EstatusOrden.EstatusOrdenID > 0)
                    {
                        sCampoSql = "U_EstatusOrdenID";
                        sConsultaSQl += string.Format(" {0} {1} = {2}", bAnd ? " AND " : " WHERE ", sCampoSql, orden.EstatusOrden.EstatusOrdenID);
                        bAnd = true;
                    }
                }

                if (orden.ResponsableOrdenServicio.EmpleadoID != null)
                {
                    if (orden.ResponsableOrdenServicio.EmpleadoID > 0)
                    {
                        sCampoSql =  "U_ResponsableID" ;
                        sConsultaSQl += string.Format(" {0} {1} = {2}", bAnd ? " AND " : " WHERE ", sCampoSql, orden.ResponsableOrdenServicio.EmpleadoID);
                        bAnd = true;
                    }
                }

                if (!string.IsNullOrEmpty(orden.Folio))
                {
                    sCampoSql = "U_Folio";
                    sConsultaSQl += string.Format(" {0} {1} LIKE '%{2}%'", bAnd ? " AND " : " WHERE ", sCampoSql, orden.Folio);
                    bAnd = true;
                }
                if (!string.IsNullOrEmpty(orden.sFechaInicio) && !string.IsNullOrEmpty(orden.sFechaFin))
                {
                    sCampoSql = "U_Fecha" ;
                    sConsultaSQl += string.Format(" {0} {1} BETWEEN '{2}' AND '{3}'", bAnd ? " AND " : " WHERE ", sCampoSql, orden.sFechaInicio, orden.sFechaFin);
                    bAnd = true;
                }
                #endregion filtros and where
                
                #region query de Rair con bugs
                /* StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(string.Format(" SELECT * FROM "+ this.NombreTabla +" as os "));
                //query.Append(" UPDATE [@VSKF_ORDENSERVICIO] SET Code = '" + orden.OrdenServicioID + "', U_Sincronizado = " + Sincronizado);

                if (OrdenServicio.OrdenServicioID != null)
                    where.Append(string.Format(" and os.U_OrdenServicioID = {0} ", orden.OrdenServicioID));
                if (OrdenServicio.EncargadoOrdenServicio != null && OrdenServicio.EncargadoOrdenServicio.EmpleadoID != null)
                    where.Append(string.Format(" and U_Empleado = {0} ", OrdenServicio.EncargadoOrdenServicio.EmpleadoID));
                if (orden.GetType() == typeof(OrdenServicioFilter))
                {
                    OrdenServicioFilter x = (OrdenServicioFilter)orden;
                    if (orden.FechaCaptura != null && x.FechaFinFilter != null)
                    {
                        where.Append(string.Format(" and os.U_FechaCaptura BETWEEN '{0}' and '{1}' ", DateToSQLDate((DateTime)x.FechaCaptura), DateToSQLDate((DateTime)x.FechaFinFilter)));
                    }
                    else if (orden.FechaCaptura != null && x.FechaFinFilter == null)
                    {
                        where.Append(" and os.U_FechaCaptura >= " + DateToSQLDate((DateTime)x.FechaCaptura) + " ");
                    }
                    else if (orden.FechaCaptura == null && x.FechaFinFilter != null)
                    {
                        where.Append(" and os.U_FechaCaptura <= " + x.FechaFinFilter + " ");
                    }
                }
                else if (orden.FechaCaptura != null)
                {
                    where.Append(" and os.Fecha = " + DateToSQLDate((DateTime) orden.FechaCaptura) + " ");
                }

                if (!string.IsNullOrEmpty(orden.Folio))
                    where.Append(string.Format(" and U_Folio like '%{0}%' ", orden.Folio));
                if (orden.EmpresaOrden != null && orden.EmpresaOrden.EmpresaID != null)
                    where.Append(string.Format(" and U_EmpresaID = {0} ", orden.EmpresaOrden.EmpresaID));
                if (orden.ProveedorServicio != null && orden.ProveedorServicio.ProveedorID != null)
                    where.Append(string.Format(" and U_ProveedorID = {0} ", orden.ProveedorServicio.ProveedorID));
                if (orden.ResponsableOrdenServicio != null && orden.ResponsableOrdenServicio.EmpleadoID != null)
                    where.Append(string.Format(" and U_ResponsableID = {0} ", orden.ResponsableOrdenServicio.EmpleadoID));
                if (orden.EstatusOrden != null && orden.EstatusOrden.EstatusOrdenID != null)
                    where.Append(string.Format(" and U_EstatusOrdenID = {0} ", orden.EstatusOrden.EstatusOrdenID));
                if (orden.FolioOrden != null && orden.FolioOrden.Folio != null)
                    where.Append(string.Format(" and U_FolioOrden = {0} ", orden.FolioOrden.Folio));
                if (orden.FolioOrden != null && orden.FolioOrden.AnioFolio != null)
                    where.Append(string.Format(" and U_AnioFolio = {0} ", orden.FolioOrden.AnioFolio));
                if (Sincronizado != null)
                    where.Append(string.Format(" and U_Sincronizado = {0} ", Sincronizado));
                if (UUID != null)
                    where.Append(string.Format(" and U_UUID like '%{0}%' ", UUID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                rs.DoQuery(query.ToString());*/
                #endregion #region query de Rair con bugs
                rs.DoQuery(sConsultaSQl);
                
            }
            catch (Exception ex)
            {
               // throw new Exception(ex.Message);
            }
            return rs;
        }

        private string DateToSQLDate(DateTime date)
        {
            //return string.Format("{0}-{1}-{2}", date.Month, date.Day, date.Year);
            return string.Format("{0}{1}{2}", date.Year, date.Month, date.Day);
        }

        public void ActualizarCambioEstarusOSID(int? ID)
        {
            try
            {
                if (index <= 0)
                {
                    throw new Exception("Index no definido");
                }

                String sentencia = String.Format(@"UPDATE {0} SET U_IDorSer = '{1}' WHERE  ""Code"" = '{2}'", this.NombreTablaCambioEstatus, ID, this.index);

                this.oCompany.StartTransaction();
                this.Insertar.DoQuery(sentencia);
                if (this.oCompany.InTransaction)
                    this.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
            }
            catch
            {
                if (this.oCompany.InTransaction)
                    this.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                throw new Exception("No se actualizó el ID de CambioEstatus. ");
            }
        }

        public Recordset ActualizarCode(OrdenServicio orden)
        {
            try
            {
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                string sql = string.Format(@" UPDATE " + this.NombreTabla + @" SET U_OrdenservicioID = {0}, U_Sincronizado = {1}", orden.OrdenServicioID, this.Sincronizado);
                sql += string.Format(@" WHERE ""Code"" = '{0}' ", orden.Code);
                rs.DoQuery(sql);
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<OrdenServicio> RecordSetToListOrdenesServicio(Recordset rs)
        {
            var result = new List<OrdenServicio>();
            rs.MoveFirst();
            for (int i = 0; i < rs.RecordCount; i++)
            {
                string itemcode = rs.Fields.Item("Code").Value;
                this.oUDT.GetByKey(itemcode);
                UDTToOrdenServicio();
                var temp = (OrdenServicio)OrdenServicio.Clone();
                result.Add(temp);
                rs.MoveNext();
            }
            return result;
        }

        #endregion

        #region Métodos Detalle de la Orden de Servicio
        private List<string> ItemsDetalle { get; set; }

        public List<DetalleOrden> GetDetalleOrden(int ordenServicioID)
        {
            DetalleOrdenSAP dorden = new DetalleOrdenSAP(ref this.oCompany);
            this.OrdenServicio.DetalleOrden = new List<DetalleOrden>();
            var rs = dorden.ConsultarByOrdenID(ordenServicioID);
            if (dorden.HashoDetalleOrden(rs))
                this.OrdenServicio.DetalleOrden = dorden.RecordSetToListDetalleOrden(rs);
            else this.OrdenServicio.DetalleOrden = new List<DetalleOrden>();

            return this.OrdenServicio.DetalleOrden;
        }

        public void InsertDetalleOrden()
        {
            try
            {
                if (!this.Existe())
                    return;
                if (this.OrdenServicio.DetalleOrden != null && this.OrdenServicio.DetalleOrden.Count > 0)
                {
                    DetalleOrdenSAP dorden = new DetalleOrdenSAP(ref this.oCompany);
                    ItemsDetalle = new List<string>();
                    string sItemCodeAsignado = "";
                    foreach(DetalleOrden d in this.OrdenServicio.DetalleOrden)
                    {
                        d.OrdenServicio.OrdenServicioID = Convert.ToInt32(this.ItemCode);
                        dorden.oDetalleOrden = d;
                        dorden.ItemCode = DateTime.Now.Ticks.ToString();
                        dorden.Sincronizado = 1;
                        dorden.UUID = Guid.NewGuid();
                        sItemCodeAsignado = dorden.DetalleOrdenToUDT();
                        this.oCompany.StartTransaction();
                        dorden.Insert();
                        if (this.oCompany.InTransaction)
                            this.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                        //ItemsDetalle.Add(dorden.ItemCode);
                        ItemsDetalle.Add(sItemCodeAsignado);
                    }
                }
            }
            catch(Exception ex)
            {
                if (this.oCompany.InTransaction)
                    this.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);

                throw new Exception("OrdenServicioData.InsetDetalleOrden: " + ex.Message);
            }

        }

        private void ActualizarOrdenDetalle()
        {
            try
            {
                if (this.OrdenServicio.DetalleOrden != null && this.OrdenServicio.DetalleOrden.Count > 0)
                {
                    DetalleOrdenSAP dorden = new DetalleOrdenSAP(ref this.oCompany);
                    foreach (DetalleOrden d in this.OrdenServicio.DetalleOrden)
                    {
                        if (d.DetalleOrdenID != 0)
                        {
                            dorden.oUDT.GetByKey(d.DetalleOrdenID.ToString());
                            dorden.oDetalleOrden = d;
                            dorden.DetalleOrdenToUDT();
                            dorden.Actualizar();
                        }
                        else if (d.DetalleOrdenID == 0)
                        {
                            d.OrdenServicio.OrdenServicioID = Convert.ToInt32(this.ItemCode);
                            dorden.oDetalleOrden = d;
                            dorden.ItemCode = DateTime.Now.Ticks.ToString();
                            dorden.Sincronizado = 1;
                            dorden.UUID = Guid.NewGuid();
                            dorden.DetalleOrdenToUDT();
                            dorden.Insert();
                            ItemsDetalle.Add(dorden.ItemCode);
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                throw new Exception("OrdenServicioServices.ActualizarOrdenDetalle: " + ex.Message);
            }
        }

        public void ActualizarCodeDetalle(List<DetalleOrden> detalle)
        {
            try
            {
                if (detalle != null && detalle.Count > 0)
                {
                    DetalleOrdenSAP dtsap = new DetalleOrdenSAP(ref this.oCompany);
                    int cont = 0;
                    foreach (DetalleOrden d in detalle)
                    {
                        string i = this.ItemsDetalle[cont].ToString();
                        dtsap.ItemCode = i;
                        if (dtsap.Existe())
                        {
                            dtsap.UDTTooDetalleOrden();
                            dtsap.Sincronizado = 0;
                            dtsap.ActualizarCode(d, i);
                        }
                        cont++;
                    }
                }
                if (this.ItemsDetalle != null)
                    this.ItemsDetalle.Clear();
            }
            catch(Exception ex)
            {
                this.ItemsDetalle.Clear();
                throw new Exception("OrdenServicioData.ActualizarCodeDetalle: " + ex.Message);
            }

        }
        #endregion
    }
}
