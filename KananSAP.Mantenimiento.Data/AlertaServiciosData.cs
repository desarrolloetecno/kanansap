﻿using Kanan.Mantenimiento.BO2;
using KananSAP.Vehiculos.Data;
using KananSAP.Alertas.Data;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KananSAP.Mantenimiento.Data
{
    public class AlertaServiciosData
    {
        #region Atributos
        private const string TABLE_ALERTA = "VSKF_ALERTA";
        //public Alerta Alerta { get; set; }
        public AlertaMantenimientoSAP AlertaMantenimientoSAP { get; set; }
        public VehiculoSAP VehiculoSAP { get; set; }
        public ServicioSAP ServicioSAP { get; set; }
        public UserTable oUDT { get; set; }
        private Company oCompany;
        public string ItemCode { get; set; }
        #endregion

        #region Contructor
        public AlertaServiciosData(Company company)
        {
            this.oCompany = company;
            try
            {
                this.oUDT = this.oCompany.UserTables.Item(TABLE_ALERTA);

                AlertaMantenimientoSAP = new AlertaMantenimientoSAP(oCompany);
                ServicioSAP = new ServicioSAP(oCompany);
            }
            catch
            {
                this.oUDT = null;
            }
        }
        #endregion

        public Recordset Consultar(AlertaMantenimiento alerta)
        {
            #region  Validación de parámetros

            //Validando Objetos
            if (alerta == null) alerta = new AlertaMantenimiento();
            if (alerta.empresa == null) alerta.empresa = new Empresa();
            if (alerta.vehiculo == null) alerta.vehiculo = new Vehiculo();
            if (alerta.servicio == null) alerta.servicio = new Servicio();

            #endregion

            #region Construyendo el query
            StringBuilder sCmd = new StringBuilder();
            StringBuilder s_VarWHERE = new StringBuilder();

            if (alerta.AlertaMantenimientoID != null)
            {
                s_VarWHERE.Append(string.Format("AND alm.U_AlertaMttoID = {0} ", alerta.AlertaMantenimientoID));
            }
            if (alerta.Mantenible != null && alerta.Mantenible.MantenibleID != null)
            {
                if (alerta.Mantenible.GetType() == typeof(Vehiculo))
                {
                    sCmd.Append(" select alm.U_AlertaMttoID,ISNULL(alm.U_MantenibleID,vh.U_VehiculoID) as MantenibleID, ISNULL(alm.U_TipoMantenibleID,1) as TipoMantenibleID,vh.U_EmpresaID, serv.U_Nombre as ser_Nombre, vh.U_Nombre as veh_Nombre, vh.U_VehiculoID as VehiculoID, serv.U_ServicioID,alm.U_TipoAlerta,alm.U_TypeOfType from " + AlertaMantenimientoSAP.NombreTablaAlertaMantenimiento + " alm join " + ServicioSAP.NombreTablaServicioSAP + " serv on serv.U_ServicioID=alm.U_ServicioID join " + VehiculoSAP.NombreTablaVehiculoSAP + " vh on vh.U_VehiculoID=alm.U_MantenibleID ");
                    s_VarWHERE.Append(string.Format("AND alm.U_MantenibleID = {0} ", alerta.Mantenible.MantenibleID));
                    s_VarWHERE.Append(string.Format("AND alm.U_TipoMantenibleID = {0} ", 1));
                }
                //else if (alerta.Mantenible.GetType() == typeof(Caja))
                //{
                //    sCmd.Append(" select AlertaMantenimientoID,ISNULL(MantenibleID,cj.CajaID) as MantenibleID,ISNULL(TipoMantenibleID,2) as TipoMantenibleID,cj.PropietarioID as EmpresaID, serv.Nombre as ser_Nombre, cj.NumeroEconomico  as veh_Nombre, cj.CajaID as VehiculoID, serv.ServicioID,alm.TipoAlerta,alm.TypeOfType from alertamantenimiento alm join Servicio serv on serv.ServicioID=alm.ServicioID join Caja cj on cj.CajaID = alm.MantenibleID ");
                //    s_VarWHERE.Append("AND MantenibleID = @MantenibleID ");
                //    s_VarWHERE.Append("AND TipoMantenibleID = @TipoMantenibleID ");
                //}
                //else if (alerta.Mantenible.GetType() == typeof(TipoVehiculo))
                //{
                //    sCmd.Append(" select alm.U_AlertaMttoID,ISNULL(alm.U_MantenibleID,vh.U_VehiculoID) as MantenibleID,ISNULL(alm.U_TipoMantenibleID,1) as TipoMantenibleID,vh.U_EmpresaID, serv.U_Nombre as ser_Nombre, vh.U_Nombre as veh_Nombre, vh.U_VehiculoID as VehiculoID, serv.U_ServicioID,alm.U_TipoAlerta,alm.U_TypeOfType from [@VSKF_ALERTAMTTO] alm join [@VSKF_SERVICIOS] serv on serv.U_ServicioID=alm.U_ServicioID join [@VSKF_VEHICULO] vh on vh.U_VehiculoID=alm.U_MantenibleID ");
                //    s_VarWHERE.Append("AND MantenibleID = @MantenibleID ");
                //    s_VarWHERE.Append("AND TipoMantenibleID = @TipoMantenibleID ");
                //}
            }
            if (alerta.servicio.ServicioID != null)
            {
                s_VarWHERE.Append(string.Format("AND alm.U_ServicioID = {0} ", alerta.servicio.ServicioID));
            }
            if (alerta.empresa.EmpresaID != null)
            {
                s_VarWHERE.Append(string.Format("AND alm.U_EmpresaID = {0} ", alerta.empresa.EmpresaID));
            }
            string filtros = s_VarWHERE.ToString().Trim();
            if (filtros.Length > 0)
            {
                if (filtros.StartsWith("AND "))
                    filtros = filtros.Substring(4);
                else if (filtros.StartsWith("OR "))
                    filtros = filtros.Substring(3);
                else if (filtros.StartsWith(","))
                    filtros = filtros.Substring(1);
                sCmd.Append(" WHERE " + filtros);
            }

            #endregion

            sCmd.Append(" ORDER BY alm.U_AlertaMttoID ASC ");

            #region Ejecutar consulta
            try
            {
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(sCmd.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            #endregion
        }

    }
}
