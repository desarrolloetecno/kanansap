﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kanan.Mantenimiento.BO2;
using Kanan.Vehiculos.BO2;

namespace KananSAP.Mantenimiento.Data
{
    public class MotorMantenimientoData
    {
        #region Atributos
        private Company oCompany;
        public UserTable oUDT { get; set; }
        private const string TABLE_TSERVICIO = "VSKF_TIPOSERVICIO";
        public string ItemCode { get; set; }
        #endregion

        #region  Constructor
        public MotorMantenimientoData(Company company)
        {
            this.oCompany = company;
            try
            {
                this.oUDT = this.oCompany.UserTables.Item(TABLE_TSERVICIO);
            }
            catch (Exception ex)
            {
                this.oUDT = null;
            }
        }
        #endregion

        /// <summary>
        /// Realiza consulta de las alertas activas
        /// </summary>
        /// <param name="empresaID">EmpresaID</param>
        /// <param name="mantenibleID">mantenibleID</param>
        /// <param name="servicioID">ServicioID</param>
        /// <param name="alertaID">AlertaID</param>
        /// <param name="activa">Es activa</param>
        /// <returns></returns>
        public Recordset ConsultarAlertasActivas(int? mantenibleID, int? servicioID, int? alertaID, Boolean? activa)
        {
            try
            {
                #region Construyendo el query MANTENIMIENTO

                StringBuilder sCmd = new StringBuilder();
                StringBuilder s_VarWHERE = new StringBuilder();
                sCmd.Append("SELECT * FROM (");
                //VEHICULOS
                sCmd.Append(" SELECT a.U_AlertaID as DetalleID ,a.U_AlertaID,a.U_IAlertaID, a.U_Descripcion_En, a.U_Descripcion_Es, a.U_Descripcion_Fr, a.U_Descripcion_Pt, a.U_EmpresaID, a.U_TipoAlerta, a.U_EstaActivo, am.U_AlertaMttoID, am.U_TipoAlerta as EstadoAlerta, s.U_ServicioID, s.U_Nombre as Servicio, s.U_Descripcion as DescripcionServico, v.U_VehiculoID, v.U_Nombre as Vehiculo, v.U_Placa, t.U_TipoServicioID, t.U_Nombre as TipoServicio, t.U_Descripcion as DescripcionTipoServicio,  d.U_Distancia as DistanciaRecorrida ,am.U_MantenibleID,am.U_TipoMantenibleID ,suc.U_SucursalID as SucursalID, suc.U_Direccion as Sucursal ");
                sCmd.Append(@" FROM ""@VSKF_ALERTA""c a JOIN ""@VSKF_ALERTAMTTO"" am ON am.U_AlertaMttoID = a.U_IAlertaID JOIN ""@VSKF_SERVICIOS"" s ON s.U_ServicioID = am.U_ServicioID JOIN ""@VSKF_VEHICULO"" v ON v.U_VehiculoID = am.U_MantenibleID JOIN ""@VSKF_TIPOSERVICIO"" t ON t.U_TipoServicioID = s.U_TipoServicioID LEFT OUTER JOIN ""@VSKF_SUCURSAL""  suc ON suc.U_SucursalID = v.U_SucursalID LEFT OUTER JOIN ""@VSKF_DESPLAZAMIENTO"" d ON d.U_VehiculoID = v.U_VehiculoID   ");

                s_VarWHERE.Append(" AND a.U_TipoAlerta = 1 ");
                s_VarWHERE.Append(" AND am.U_TipoMantenibleID = 1 ");

                if (mantenibleID != null)
                    s_VarWHERE.Append(string.Format(" AND  am.U_MantenibleID = {0} ", mantenibleID));
                if (servicioID != null)
                    s_VarWHERE.Append(string.Format(" AND s.U_ServicioID = {0} ", servicioID));
                if (alertaID != null)
                    s_VarWHERE.Append(string.Format(" AND a.U_AlertaID = {0} ", alertaID));
                if (activa != null)
                {
                    s_VarWHERE.Append(string.Format(" AND a.U_EstaActivo = {0} ", Convert.ToInt32(activa))); 
                }
                string filtros = s_VarWHERE.ToString().Trim();
                if (filtros.Length > 0)
                {
                    if (filtros.StartsWith("AND "))
                        filtros = filtros.Substring(4);
                    else if (filtros.StartsWith("OR "))
                        filtros = filtros.Substring(3);
                    else if (filtros.StartsWith(","))
                        filtros = filtros.Substring(1);
                    sCmd.Append(" WHERE " + filtros);
                }

                #endregion

                sCmd.Append(") AS Alertas order by U_AlertaID Desc");

                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(sCmd.ToString());
                return rs;
            }
            catch(Exception ex)
            { throw new Exception(ex.Message); }
        }

        public Recordset ConsultarViewAlertaServicio(int? mantenibleID, int? servicioID, int? tipoMantenibleID)
        {
            try
            {
                #region Construyendo el Query
                StringBuilder sCmd = new StringBuilder();
                StringBuilder s_VarWHERE = new StringBuilder();
                sCmd.Append(this.AlertaServiciosView());
                if(tipoMantenibleID == 1)
                {
                    s_VarWHERE.Append(string.Format(@" ""@VSKF_PMTROMTTO"".U_TMantbleID = {0} ", tipoMantenibleID));
                    if(mantenibleID != null)
                        s_VarWHERE.Append(string.Format(@" AND ""@VSKF_PMTROMTTO"".U_MantbleID = {0} ", mantenibleID));
                    if (servicioID != null)
                        s_VarWHERE.Append(string.Format(@" AND ""@VSKF_SERVICIOS"".U_ServicioID = {0} ", servicioID));

                }
                
                #endregion

                string filtros = s_VarWHERE.ToString().Trim();
                if (filtros.Length > 0)
                {
                    if (filtros.StartsWith("AND "))
                        filtros = filtros.Substring(4);
                    else if (filtros.StartsWith("OR "))
                        filtros = filtros.Substring(3);
                    else if (filtros.StartsWith(","))
                        filtros = filtros.Substring(1);
                    sCmd.Append(" WHERE " + filtros);
                }
                sCmd.Append(@" ORDER BY ""@VSKF_SERVICIOS"".U_ServicioID ASC ");
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(sCmd.ToString());
                return rs;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public bool HasAlertas(Recordset rs)
        {
            if (rs.RecordCount > 0)
                return true;
            return false;
        }

        public AlertaMantenimiento LastRecordSetToAlertaMantenimiento(Recordset rs)
        {
            try
            {
                AlertaMantenimiento alerta = new AlertaMantenimiento();
                alerta.empresa = new Kanan.Operaciones.BO2.Empresa();
                alerta.servicio = new Servicio();
                alerta.servicio.TipoServicio = new TipoServicio();
                alerta.servicio.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
                rs.MoveFirst();
                if (rs.Fields.Item("U_AlertaID").Value != null)
                    alerta.AlertaMantenimientoID = rs.Fields.Item("U_AlertaID").Value;
                if (rs.Fields.Item("U_TipoMantenibleID").Value != null)
                {
                    int tipo = rs.Fields.Item("U_TipoMantenibleID").Value;
                    if(tipo == 1)
                    {
                        Vehiculo vh = new Vehiculo();
                        if (rs.Fields.Item("U_MantenibleID").Value != null)
                            vh.VehiculoID = rs.Fields.Item("U_MantenibleID").Value;
                        if (rs.Fields.Item("Vehiculo").Value != null)
                            vh.Nombre = rs.Fields.Item("Vehiculo").Value;
                        if (rs.Fields.Item("U_Placa").Value != null)
                            vh.Placa = rs.Fields.Item("U_Placa").Value;
                        alerta.Mantenible = vh;
                    }
                }
                if (rs.Fields.Item("U_TipoAlerta").Value != null)
                    alerta.TipoAlerta = rs.Fields.Item("U_TipoAlerta").Value;
                if (rs.Fields.Item("U_ServicioID").Value != null)
                    alerta.servicio.ServicioID = rs.Fields.Item("U_ServicioID").Value;
                if (rs.Fields.Item("Servicio").Value != null)
                    alerta.servicio.Nombre = rs.Fields.Item("Servicio").Value;
                if (rs.Fields.Item("DescripcionServico").Value != null)
                    alerta.servicio.Descripcion = rs.Fields.Item("DescripcionServico").Value;
                if (rs.Fields.Item("U_TipoServicioID").Value != null)
                    alerta.servicio.TipoServicio.TipoServicioID = rs.Fields.Item("U_TipoServicioID").Value;
                if (rs.Fields.Item("TipoServicio").Value != null)
                    alerta.servicio.TipoServicio.Nombre = rs.Fields.Item("TipoServicio").Value;
                if (rs.Fields.Item("DistanciaRecorrida").Value != null)
                    alerta.DistanciaRecorrida = rs.Fields.Item("DistanciaRecorrida").Value;
                if (rs.Fields.Item("U_EmpresaID").Value != null)
                    alerta.empresa.EmpresaID = rs.Fields.Item("U_EmpresaID").Value;
                if (rs.Fields.Item("SucursalID").Value != null)
                    alerta.servicio.SubPropietario.SucursalID = rs.Fields.Item("SucursalID").Value;
                if (rs.Fields.Item("Sucursal").Value != null)
                    alerta.servicio.SubPropietario.Direccion = rs.Fields.Item("Sucursal").Value;

                return alerta;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        /// <summary>
        /// En un for, mientras se recorre, obtiene los datos contenidos en el RecordSert
        /// </summary>
        /// <param name="rs">SAPbobsCOM.RecordSet</param>
        /// <returns></returns>
        public AlertaMantenimiento RecordSetToAlertaMantenimiento(Recordset rs)
        {
            try
            {
                AlertaMantenimiento alerta = new AlertaMantenimiento();
                alerta.empresa = new Kanan.Operaciones.BO2.Empresa();
                alerta.servicio = new Servicio();
                alerta.servicio.TipoServicio = new TipoServicio();
                alerta.servicio.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();

                if (rs.Fields.Item("U_AlertaID").Value != null)
                    alerta.AlertaMantenimientoID = rs.Fields.Item("U_AlertaID").Value;
                if (rs.Fields.Item("U_TipoMantenibleID").Value != null)
                {
                    int tipo = rs.Fields.Item("U_TipoMantenibleID").Value;
                    if (tipo == 1)
                    {
                        Vehiculo vh = new Vehiculo();
                        if (rs.Fields.Item("U_MantenibleID").Value != null)
                            vh.VehiculoID = rs.Fields.Item("U_MantenibleID").Value;
                        if (rs.Fields.Item("Vehiculo").Value != null)
                            vh.Nombre = rs.Fields.Item("Vehiculo").Value;
                        if (rs.Fields.Item("U_Placa").Value != null)
                            vh.Placa = rs.Fields.Item("U_Placa").Value;
                        alerta.Mantenible = vh;
                    }
                }
                if (rs.Fields.Item("U_TipoAlerta").Value != null)
                    alerta.TipoAlerta = rs.Fields.Item("U_TipoAlerta").Value;
                if (rs.Fields.Item("U_ServicioID").Value != null)
                    alerta.servicio.ServicioID = rs.Fields.Item("U_ServicioID").Value;
                if (rs.Fields.Item("Servicio").Value != null)
                    alerta.servicio.Nombre = rs.Fields.Item("Servicio").Value;
                if (rs.Fields.Item("DescripcionServico").Value != null)
                    alerta.servicio.Descripcion = rs.Fields.Item("DescripcionServico").Value;
                if (rs.Fields.Item("U_TipoServicioID").Value != null)
                    alerta.servicio.TipoServicio.TipoServicioID = rs.Fields.Item("U_TipoServicioID").Value;
                if (rs.Fields.Item("TipoServicio").Value != null)
                    alerta.servicio.TipoServicio.Nombre = rs.Fields.Item("TipoServicio").Value;
                if (rs.Fields.Item("DistanciaRecorrida").Value != null)
                    alerta.DistanciaRecorrida = rs.Fields.Item("DistanciaRecorrida").Value;
                if (rs.Fields.Item("U_EmpresaID").Value != null)
                    alerta.empresa.EmpresaID = rs.Fields.Item("U_EmpresaID").Value;
                if (rs.Fields.Item("SucursalID").Value != null)
                    alerta.servicio.SubPropietario.SucursalID = rs.Fields.Item("SucursalID").Value;
                if (rs.Fields.Item("Sucursal").Value != null)
                    alerta.servicio.SubPropietario.Direccion = rs.Fields.Item("Sucursal").Value;

                return alerta;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        private IMantenible GetMantenible(int tipoId)
        {
            switch(tipoId)
            {
                case 1:
                    return new Vehiculo();
                case 2:
                    return new Caja();
                case 3:
                    return new TipoVehiculo();
                default:
                    return new Vehiculo();
            }
        }

        private string AlertaServiciosView()
        {
            string query = string.Empty;
            query += @" SELECT ""@VSKF_VEHICULO"".U_VehiculoID as vehiculo, ""@VSKF_VEHICULO"".U_Placa AS veh_Placa, ""@VSKF_VEHICULO"".U_Modelo AS veh_Modelo, ""@VSKF_VEHICULO"".U_Nombre AS veh_Nombre, ""@VSKF_VEHICULO"".U_Descripcion AS veh_Descripcion, ""@VSKF_SERVICIOS"".U_ServicioID as servicioID, ";
            query += @" ""@VSKF_SERVICIOS"".U_Nombre AS ser_Nombre, ""@VSKF_SERVICIOS"".U_Descripcion AS ser_Descripcion, ""@VSKF_SERVICIOS"".U_EmpresaID,""@VSKF_SERVICIOS"".U_TipoServicioID as tipoServicioID, ""@VSKF_TIPOSERVICIO"".U_Nombre AS tser_Nombre, ""@VSKF_PMTROMTTO"".U_AlProxServ as alertaProximoServicio, ";
            query += @" ""@VSKF_PMTROMTTO"".U_ProxServ as proximoServicio, ""@VSKF_PMTROMTTO"".U_UltServ as ultimoServicio, ""@VSKF_PMTROMTTO"".U_UlAlEnviad as ultimaAlertaEnviada, ""@VSKF_DESPLAZAMIENTO"".U_Distancia AS DistanciaRecorrida, ""@VSKF_DESPLAZAMIENTO"".U_Fecha as fecha, ""@VSKF_SUCURSAL"".U_SucursalID as sucursalID, ";
            query += @" ""@VSKF_PMTROMTTO"".U_PMtoID as parametroMantenimientoID, ""@VSKF_PMTROSERVICIO"".U_PServID as parametroServicioID,  ""@VSKF_SUCURSAL"".U_Direccion as sucursal, ""@VSKF_VEHICULO"".U_EsActivo AS vehiculoEstado, ""@VSKF_SERVICIOS"".U_EsActivo AS servicioEstado, ""@VSKF_TIPOSERVICIO"".U_EsActivo AS tipoServicioEstado, ";
            query += @" ""@VSKF_SUCURSAL"".U_EsActivo AS sucursalEstado, ""@VSKF_VEHICULO"".U_NombreVehiculo AS veh_NombreVehiculo, ""@VSKF_PMTROMTTO"".U_MantbleID as mantenibleID, ""@VSKF_PMTROMTTO"".U_TMantbleID as tipoMatenibleID ";
            query += @" FROM ""@VSKF_SERVICIOS"" INNER JOIN ""@VSKF_TIPOSERVICIO"" ON ""@VSKF_SERVICIOS"".U_TipoServicioID = ""@VSKF_TIPOSERVICIO"".U_TipoServicioID INNER JOIN ""@VSKF_PMTROSERVICIO"" ON ""@VSKF_SERVICIOS"".U_ServicioID = ""@VSKF_PMTROSERVICIO"".U_ServicioID INNER JOIN ""@VSKF_DESPLAZAMIENTO"" INNER JOIN ""@VSKF_PMTROMTTO"" INNER JOIN ";
            query += @" ""@VSKF_VEHICULO"" ON ""@VSKF_PMTROMTTO"".U_MantbleID = ""@VSKF_VEHICULO"".U_VehiculoID ON ""@VSKF_DESPLAZAMIENTO"".U_VehiculoID = ""@VSKF_VEHICULO"".U_VehiculoID LEFT OUTER JOIN ""@VSKF_SUCURSAL"" ON ""@VSKF_VEHICULO"".U_SucursalID = ""@VSKF_SUCURSAL"".U_SucursalID ON ""@VSKF_PMTROSERVICIO"".U_PServID = ""@VSKF_PMTROMTTO"".U_PServID ";
            //query += " [@VSKF_VEHICULO] ON [@VSKF_PMTROMTTO].U_MantbleID = [@VSKF_VEHICULO].U_VehiculoID ON [@VSKF_DESPLAZAMIENTO].U_VehiculoID = [@VSKF_VEHICULO].U_VehiculoID LEFT OUTER JOIN [@VSKF_SUCURSAL] ON [@VSKF_VEHICULO].U_SucursalID = [@VSKF_SUCURSAL].U_SucursalID ON [@VSKF_PMTROSERVICIO].U_PServID = [@VSKF_PMTROMTTO].U_PServID  ";
            return query;
        }

    }
}
