﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kanan.Operaciones.BO2;
using Kanan.Mantenimiento.BO2;
using Kanan.Llantas.BO2;
using KananSAP.Llantas.Data;
using SAPbobsCOM;

namespace KananSAP.Mantenimiento.Data
{
    public class ParametroDesgasteData
    {
        #region Atributos
        private const string TABLE_DESGASTE = "VSKF_PMTRODESGASTE";
        public ParametroDesgaste oParametroDesgaste { get; set; }
        public bool isTypeVehicle { get; set; }
        public UserTable oUDT { get; set; }
        private Company oCompany;
        public string ItemCode { get; set; }

        //Migracion Hana
        Recordset Insertar;
        String Parametro;
        private String NombreTabla;
        bool EsSQL;

        //Se agregó para ser visible desde cualquier lugar consulta.
        public String NombreTablaParametroDesgasteData = String.Empty;

        #endregion

        #region Constructor
        public ParametroDesgasteData(Company company)
        {
            this.oCompany = company;
            try
            {
                this.oUDT = this.oCompany.UserTables.Item(TABLE_DESGASTE);

                //Migración HANA
                this.Insertar = (Recordset)this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.EsSQL = oCompany.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.NombreTabla = EsSQL ? "[@VSKF_PMTRODESGASTE]" : @"""@VSKF_PMTRODESGASTE""";
                //Se maneja en una variable pública para poder utilizar desde cualquier método.
                this.NombreTablaParametroDesgasteData = this.NombreTabla;
            }
            catch (Exception ex)
            {
                this.oUDT = null;
            }
        }
        #endregion

        #region Existe
        public bool Existe()
        {
            return this.oUDT.GetByKey(this.ItemCode);
        }
        #endregion

        #region Preparando Tabla de usuario
        private void PrepareUserTable()
        {
            try
            {
                this.oUDT.Code = oParametroDesgaste.ParametroDesgasteID == null ? Convert.ToString(0) : Convert.ToString(oParametroDesgaste.ParametroDesgasteID);
                this.oUDT.Name = oParametroDesgaste.ParametroDesgasteID == null ? Convert.ToString(0) : Convert.ToString(oParametroDesgaste.ParametroDesgasteID);
                this.oUDT.UserFields.Fields.Item("U_PmDesID").Value = this.oParametroDesgaste.ParametroDesgasteID == null ? 0 : this.oParametroDesgaste.ParametroDesgasteID;
                this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value = this.oParametroDesgaste.Empresa.EmpresaID == null ? 0 : this.oParametroDesgaste.Empresa.EmpresaID;
                this.oUDT.UserFields.Fields.Item("U_SucursalID").Value = this.oParametroDesgaste.Sucursal.SucursalID == null ? 0 : this.oParametroDesgaste.Sucursal.SucursalID;
                this.oUDT.UserFields.Fields.Item("U_Uso").Value = this.oParametroDesgaste.Uso == null ? 0 : this.oParametroDesgaste.Uso;
                this.oUDT.UserFields.Fields.Item("U_LlantaID").Value = this.oParametroDesgaste.Llanta.LlantaID == null ? 0 : this.oParametroDesgaste.Llanta.LlantaID;
                this.oUDT.UserFields.Fields.Item("U_LimPorc").Value = this.oParametroDesgaste.LimPorc == null ? 0.000 : this.oParametroDesgaste.LimPorc;
                this.oUDT.UserFields.Fields.Item("U_LimProf").Value = this.oParametroDesgaste.LimProf == null ? 0.000 : this.oParametroDesgaste.LimProf;
                this.oUDT.UserFields.Fields.Item("U_AvisoLimPorc").Value = this.oParametroDesgaste.AvisoLimPorc == null ? 0.000 : this.oParametroDesgaste.AvisoLimPorc;
                this.oUDT.UserFields.Fields.Item("U_AvisoLimProf").Value = this.oParametroDesgaste.AvisoLimProf == null ? 0.000 : this.oParametroDesgaste.AvisoLimProf;
                this.oUDT.UserFields.Fields.Item("U_TiempActTread").Value = this.oParametroDesgaste.TiempActTread == null ? 0 : Convert.ToInt32(this.oParametroDesgaste.TiempActTread);
                this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.oParametroDesgaste.Sincronizado == null ? 0 : Convert.ToInt32(this.oParametroDesgaste.Sincronizado);

                #region TipoParametro
                //if (oParametroDesgaste.Mantenible != null)
                //{
                //    int? tipoMant = null;
                //    if (oParametroDesgaste.Mantenible.GetType() == typeof(Vehiculo))
                //        tipoMant = 1;
                //    else if (oParametroDesgaste.Mantenible.GetType() == typeof(Caja))
                //        tipoMant = 2;
                //    else if (oParametroDesgaste.Mantenible.GetType() == typeof(TipoVehiculo))
                //        tipoMant = 3;
                //    if (tipoMant != null)
                //        this.oUDT.UserFields.Fields.Item("U_TMantbleID").Value = tipoMant;
                //}
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Validación de datos
        private void ValidateData()
        {
            try
            {
                //Se valida que el objeto exista y en caso de tener instancias de otros objetos; que estos existan. Rair Santos
                if (this.oParametroDesgaste == null) throw new Exception("Error en la validación de datos, ParametroDesgaste no puede ser nulo");
                if (this.oParametroDesgaste.Empresa == null) throw new Exception("Error en la validación, el objeto Empersa no puede ser nulo");
                if (this.oParametroDesgaste.Sucursal == null) throw new Exception("Error en la validación, el objeto Sucursal no puede ser nulo");
                if (this.oParametroDesgaste.Llanta == null) throw new Exception("Error en la validación, el objeto Llanta no puede ser nulo");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ParametroDesgaste a Tabla de usuario
        public void ParametroDesgasteToUDT()
        {
            ValidateData();
            PrepareUserTable();
        }
        #endregion

        #region GetNewCode
        public void GetNewCode()
        {
            ParametroDesgaste pd = new ParametroDesgaste() { Empresa = new Empresa(), Sucursal = new Sucursal(), Llanta = new Llanta() };
            pd = LastRecordSetToParametroDesgaste(this.Consultar(pd));
            int? nVal = pd.ParametroDesgasteID + 1;
            oParametroDesgaste.ParametroDesgasteID = nVal;
        }
        #endregion

        #region Acciones SAP
        public void Insert()
        {
            try
            {
                #region SQL || Hana
                //this.Parametro = String.Empty;
                //this.Parametro = String.Format("INSERT INTO {0} VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}')", this.NombreTabla, this.oParametroDesgaste.ParametroDesgasteID, this.oParametroDesgaste.ParametroDesgasteID, this.oParametroDesgaste.ParametroDesgasteID, this.oParametroDesgaste.Empresa.EmpresaID, this.oParametroDesgaste.Sucursal.SucursalID, this.oParametroDesgaste.Uso, this.oParametroDesgaste.Llanta.LlantaID, this.oParametroDesgaste.LimPorc, this.oParametroDesgaste.LimProf, this.oParametroDesgaste.AvisoLimPorc, this.oParametroDesgaste.AvisoLimProf, Convert.ToInt32(this.oParametroDesgaste.TiempActTread), "0");

                //this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana

                #region SQL
                this.oUDT.Code = this.oParametroDesgaste.ParametroDesgasteID.ToString();
                this.oUDT.Name = this.oParametroDesgaste.ParametroDesgasteID.ToString();
                int result = this.oUDT.Add();
                if (result != 0)
                {
                    int errornum;
                    string errormsj;
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar insertar un parámetro en SAP. " + ex.Message);
            }
        }

        public void Actualizar()
        {
            try
            {
                #region SQL || Hana
                //this.Parametro = String.Empty;
                //this.Parametro = String.Format(@"UPDATE {0} SET ""Code"" = '{1}', ""Name"" = '{2}', U_PmDesID = '{3}', U_EmpresaID = '{4}', U_SucursalID = '{5}', U_Uso = '{6}', U_LlantaID = '{7}', U_LimPorc = '{8}', U_LimProf = '{9}', U_AvisoLimPorc = '{10}', U_AvisoLimProf = '{11}', U_TiempActTread = '{12}' WHERE Code = '{1}'", this.NombreTabla, this.oParametroDesgaste.ParametroDesgasteID, this.oParametroDesgaste.ParametroDesgasteID, this.oParametroDesgaste.ParametroDesgasteID, this.oParametroDesgaste.Empresa.EmpresaID, this.oParametroDesgaste.Sucursal.SucursalID, this.oParametroDesgaste.Uso, this.oParametroDesgaste.Llanta.LlantaID, this.oParametroDesgaste.LimPorc, this.oParametroDesgaste.LimProf, this.oParametroDesgaste.AvisoLimPorc, this.oParametroDesgaste.AvisoLimProf, Convert.ToInt32(this.oParametroDesgaste.TiempActTread), this.oParametroDesgaste.Sincronizado);

                //this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana

                #region SQL
                int errornum;
                string errormsj;
                int result = this.oUDT.Update();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar()
        {
            try
            {
                string ID = String.Empty;
                if (!String.IsNullOrEmpty(this.oUDT.Code))
                    ID = Convert.ToString(this.oUDT.Code);
                else
                {
                    throw new Exception("Erron al intentar obtener identificador. ");
                }

                #region SQL || Hana
                //this.Parametro = String.Empty;
                //this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code"" = '{1}'", this.NombreTabla, oUDT.Code);

                //this.Insertar.DoQuery(this.Parametro);
                #endregion SQL  || Hana

                #region SQL
                int errornum;
                int result = -1;
                string errormsj;
                result = this.oUDT.Remove();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Consultar
        public Recordset Consultar(ParametroDesgaste pd)
        {
            Recordset rs = null;
            try
            {
                if (pd == null) pd = new ParametroDesgaste();
                if (pd.Empresa == null) pd.Empresa = new Empresa();
                if (pd.Sucursal == null) pd.Sucursal = new Sucursal();
                if (pd.Llanta == null) pd.Llanta = new Llanta();

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();

                query.Append(" select * ");
                query.Append(" from " + this.NombreTabla + " ");

                if (pd.ParametroDesgasteID != null & pd.ParametroDesgasteID > 0)
                    where.Append(string.Format("and U_PmDesID = {0} ", pd.ParametroDesgasteID));
                if (pd.Empresa.EmpresaID != null & pd.Empresa.EmpresaID > 0)
                    where.Append(string.Format("and U_EmpresaID = {0} ", pd.Empresa.EmpresaID));
                if (pd.Sucursal.SucursalID != null & pd.Sucursal.SucursalID > 0)
                    where.Append(string.Format("and U_SucursalID = {0} ", pd.Sucursal.SucursalID));
                if (pd.Uso != null)
                    where.Append(string.Format("and U_Uso = {0} ", pd.Uso));
                if (pd.Llanta.LlantaID != null & pd.Llanta.LlantaID > 0)
                    where.Append(string.Format("and U_LlantaID = {0} ", pd.Llanta.LlantaID));
                if (pd.LimPorc != null & pd.LimPorc > 0)
                    where.Append(string.Format("and U_LimPorc = {0} ", pd.LimPorc));
                if (pd.LimProf != null & pd.LimProf > 0)
                    where.Append(string.Format("and U_LimProf = {0} ", pd.LimProf));
                if (pd.AvisoLimPorc != null & pd.AvisoLimPorc > 0)
                    where.Append(string.Format("and U_AvisoLimPorc = {0} ", pd.AvisoLimPorc));
                if (pd.AvisoLimProf != null & pd.AvisoLimProf > 0)
                    where.Append(string.Format("and U_AvisoLimProf = {0} ", pd.AvisoLimProf));
                if (pd.TiempActTread != null & pd.TiempActTread > 0)
                    where.Append(string.Format("and U_TiempActTread = {0} ", Convert.ToInt32(pd.TiempActTread)));
                if (pd.Sincronizado != null & pd.Sincronizado > 0)
                    where.Append(string.Format("and U_Sincronizado = {0}", Convert.ToInt32(pd.Sincronizado)));
                #region Comentado
                //if (pd.UltimaAlertaEnviada != null)
                //    where.Append(string.Format("and pm.U_UlAlEnviad = '{0}' ", pd.UltimaAlertaEnviada));
                //if (pd.TipoParametro != null)
                //    where.Append(string.Format("and pm.U_TipoPmtro = {0} ", pd.TipoParametro));
                //if (!string.IsNullOrEmpty(pd.ParametroServicio.Servicio.Nombre))
                //    where.Append(string.Format("and sv.U_Nombre like '%{0}%' ", pd.ParametroServicio.Servicio.Nombre));
                //if (pd.Mantenible != null)
                //{
                //    if (pd.Mantenible.GetType() == typeof(Vehiculo))
                //        where.Append(string.Format("and pm.U_TMantbleID = {0} ", 1));
                //    if (pd.Mantenible.MantenibleID != null)
                //        where.Append(string.Format("and pm.U_MantbleID = {0} ", pd.Mantenible.MantenibleID));
                //}
                #endregion

                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith("and"))
                        filtro = filtro.Substring(3);
                    query.Append(" where " + filtro);
                }

                //Corroborar llamada a campo Code o U_Code
                query.Append(@" order by ""Code"" desc ");
                rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(query.ToString());
                return rs;
            }
            #region Catch
            catch (Exception ex)
            {
                
                throw new Exception("Error al intentar realizar la consulta. " + ex.Message);
            }
            #endregion
        }
        #endregion

        #region Actualizar
        public void Actualizar(ParametroDesgaste pd)
        {
            //Realizar el query para el UPDATE
            Recordset rs = null;
            if (pd == null) pd = new ParametroDesgaste();
            if (pd.Empresa == null) pd.Empresa = new Empresa();
            if (pd.Sucursal == null) pd.Sucursal = new Sucursal();

            StringBuilder query = new StringBuilder();
            StringBuilder where = new StringBuilder();

            query.Append(" UPDATE " + this.NombreTabla + " ");

            if (pd.Empresa.EmpresaID != null & pd.Empresa.EmpresaID >= 0)
                query.Append(string.Format("SET U_EmpresaID = {0}, ", pd.Empresa.EmpresaID));
            if (pd.Sucursal.SucursalID != null & pd.Sucursal.SucursalID >= 0)
                query.Append(string.Format(" U_SucursalID = {0}, ", pd.Sucursal.SucursalID));
            if (pd.Uso != null)
                query.Append(string.Format(" U_Uso = {0}, ", pd.Uso));
            if (pd.Llanta.LlantaID != null & pd.Llanta.LlantaID >= 0)
                query.Append(string.Format(" U_LlantaID = {0}, ", pd.Llanta.LlantaID));
            if (pd.LimPorc != null & pd.LimPorc >= 0)
                query.Append(string.Format(" U_LimPorc = {0}, ", pd.LimPorc));
            if (pd.LimProf != null & pd.LimProf >= 0)
                query.Append(string.Format(" U_LimProf = {0}, ", pd.LimProf));
            if (pd.AvisoLimPorc != null & pd.AvisoLimPorc >= 0)
                query.Append(string.Format(" U_AvisoLimPorc = {0}, ", pd.AvisoLimPorc));
            if (pd.AvisoLimProf != null & pd.AvisoLimProf >= 0)
                query.Append(string.Format(" U_AvisoLimProf = {0}, ", pd.AvisoLimProf));
            if (pd.TiempActTread != null & pd.TiempActTread >= 0)
                query.Append(string.Format(" U_TiempActTread = {0} ", Convert.ToInt32(pd.TiempActTread)));
            if (pd.ParametroDesgasteID != null & pd.ParametroDesgasteID >= 0)
                where.Append(string.Format(" U_PmDesID = {0} ", pd.ParametroDesgasteID));
            if (where.Length > 0)
            {
                string filtro = where.ToString();
                if (filtro.StartsWith("and"))
                    filtro = filtro.Substring(3);
                query.Append(" where " + filtro);
            }
            rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            rs.DoQuery(query.ToString());
        }
        #endregion

        #region Ultimo Recordser a ParametroDesgaste
        public ParametroDesgaste LastRecordSetToParametroDesgaste(Recordset rs)
        {
            try
            {
                ParametroDesgaste pd = new ParametroDesgaste();
                if (pd.Empresa == null) pd.Empresa = new Empresa();
                if (pd.Sucursal == null) pd.Sucursal = new Sucursal();
                if (pd.Llanta == null) pd.Llanta = new Llanta();
                rs.MoveFirst();
                if (rs.Fields.Item("U_PmDesID").Value != null || rs.Fields.Item("U_PmDesID").Value > 0)
                    pd.ParametroDesgasteID = Convert.ToInt32(rs.Fields.Item("U_PmDesID").Value);
                if (rs.Fields.Item("U_EmpresaID").Value != null || rs.Fields.Item("U_EmpresaID").Value > 0)
                    pd.Empresa.EmpresaID = Convert.ToInt32(rs.Fields.Item("U_EmpresaID").Value);
                if (rs.Fields.Item("U_SucursalID").Value != null || rs.Fields.Item("U_SucursalID").Value > 0)
                    pd.Sucursal.SucursalID = Convert.ToInt32(rs.Fields.Item("U_SucursalID").Value);
                if (rs.Fields.Item("U_Uso").Value != null || rs.Fields.Item("U_Uso").Value > 0)
                    pd.Uso = Convert.ToInt32(rs.Fields.Item("U_Uso").Value);
                if (rs.Fields.Item("U_LlantaID").Value != null || rs.Fields.Item("U_LlantaID").Value > 0)
                    pd.Llanta.LlantaID = Convert.ToInt32(rs.Fields.Item("U_LlantaID").Value);
                if (rs.Fields.Item("U_LimPorc").Value != null || rs.Fields.Item("U_LimPorc").Value > 0)
                    pd.LimPorc = Convert.ToDouble(rs.Fields.Item("U_LimPorc").Value);
                if (rs.Fields.Item("U_LimProf").Value != null || rs.Fields.Item("U_LimProf").Value > 0)
                    pd.LimProf = Convert.ToDouble(rs.Fields.Item("U_LimProf").Value);
                if (rs.Fields.Item("U_AvisoLimPorc").Value != null || rs.Fields.Item("U_AvisoLimPorc").Value > 0)
                    pd.AvisoLimPorc = Convert.ToDouble(rs.Fields.Item("U_AvisoLimPorc").Value);
                if (rs.Fields.Item("U_AvisoLimProf").Value != null || rs.Fields.Item("U_AvisoLimProf").Value > 0)
                    pd.AvisoLimProf = Convert.ToDouble(rs.Fields.Item("U_AvisoLimProf").Value);
                if (rs.Fields.Item("U_TiempActTread").Value != null || rs.Fields.Item("U_TiempActTread").Value > 0)
                    pd.TiempActTread = Convert.ToDouble(rs.Fields.Item("U_TiempActTread").Value);
                #region Comentado
                //if (rs.Fields.Item("ProximaAlerta").Value != null || rs.Fields.Item("ProximaAlerta").Value > 0)
                //    pm.AlertaProximoServicio = Convert.ToDouble(rs.Fields.Item("ProximaAlerta").Value);
                //if (rs.Fields.Item("U_UlSvTiem").Value != null || rs.Fields.Item("U_UlSvTiem").Value > 0)
                //    pm.UltimoServicioTiempo = Convert.ToDateTime(rs.Fields.Item("U_UlSvTiem").Value);
                //if (rs.Fields.Item("U_ProxSvTiem").Value != null || rs.Fields.Item("U_ProxSvTiem").Value > 0)
                //    pm.ProximoServicioTiempo = Convert.ToDateTime(rs.Fields.Item("U_ProxSvTiem").Value);
                //if (rs.Fields.Item("U_AlProSvTie").Value != null || rs.Fields.Item("U_AlProSvTie").Value > 0)
                //    pm.AlertaProximoServicioTiempo = Convert.ToDateTime(rs.Fields.Item("U_AlProSvTie").Value);
                //if (rs.Fields.Item("U_TMantbleID").Value != null || rs.Fields.Item("U_TMantbleID").Value > 0)
                //{
                //    if (rs.Fields.Item("U_MantbleID").Value != null || rs.Fields.Item("U_MantbleID").Value > 0)
                //    {
                //        int tipo = Convert.ToInt32(rs.Fields.Item("U_TMantbleID").Value);
                //        if (tipo == 1)
                //        {
                //            pm.Mantenible = new Vehiculo();
                //            pm.Mantenible.MantenibleID = Convert.ToInt32(rs.Fields.Item("U_MantbleID").Value);
                //        }
                //    }
                //}
                //if (rs.Fields.Item("U_UlAlEnviad").Value != null || rs.Fields.Item("U_UlAlEnviad").Value > 0)
                //    pm.UltimaAlertaEnviada = Convert.ToDateTime(rs.Fields.Item("U_UlAlEnviad").Value);
                //if (rs.Fields.Item("U_PServID").Value != null || rs.Fields.Item("U_PServID").Value > 0)
                //    pm.ParametroServicio.ParametroServicioID = Convert.ToInt32(rs.Fields.Item("U_PServID").Value);
                //if (rs.Fields.Item("U_TipoPmtro").Value != null || rs.Fields.Item("U_TipoPmtro").Value > 0)
                //    pm.TipoParametro = Convert.ToInt32(rs.Fields.Item("U_TipoPmtro").Value);
                #endregion
                return pd;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region FIla Recordset a ParametroLista
        public ParametroDesgaste RowRecordSetToParametroDesgaste(Recordset rs)
        {
            try
            {
                ParametroDesgaste pd = new ParametroDesgaste();
                if (pd.Empresa == null) pd.Empresa = new Empresa();
                if (pd.Sucursal == null) pd.Sucursal = new Sucursal();
                if (pd.Llanta == null) pd.Llanta = new Llanta();

                if (rs.Fields.Item("U_PmDesID").Value != null || rs.Fields.Item("U_PmDesID").Value > 0)
                    pd.ParametroDesgasteID = Convert.ToInt32(rs.Fields.Item("U_PmDesID").Value);
                if (rs.Fields.Item("U_EmpresaID").Value != null || rs.Fields.Item("U_EmpresaID").Value > 0)
                    pd.Empresa.EmpresaID = Convert.ToInt32(rs.Fields.Item("U_EmpresaID").Value);
                if (rs.Fields.Item("U_SucursalID").Value != null || rs.Fields.Item("U_SucursalID").Value > 0)
                    pd.Sucursal.SucursalID = Convert.ToInt32(rs.Fields.Item("U_SucursalID").Value);
                if (rs.Fields.Item("U_Uso").Value != null || rs.Fields.Item("U_Uso").Value > 0)
                    pd.Uso = Convert.ToInt32(rs.Fields.Item("U_Uso").Value);
                if (rs.Fields.Item("U_LlantaID").Value != null || rs.Fields.Item("U_LlantaID").Value > 0)
                    pd.Llanta.LlantaID = Convert.ToInt32(rs.Fields.Item("U_LlantaID").Value);
                if (rs.Fields.Item("U_LimPorc").Value != null || rs.Fields.Item("U_LimPorc").Value > 0)
                    pd.LimPorc = Convert.ToDouble(rs.Fields.Item("U_LimPorc").Value);
                if (rs.Fields.Item("U_LimProf").Value != null || rs.Fields.Item("U_LimProf").Value > 0)
                    pd.LimProf = Convert.ToDouble(rs.Fields.Item("U_LimProf").Value);
                if (rs.Fields.Item("U_AvisoLimPorc").Value != null || rs.Fields.Item("U_AvisoLimPorc").Value > 0)
                    pd.AvisoLimPorc = Convert.ToDouble(rs.Fields.Item("U_AvisoLimPorc").Value);
                if (rs.Fields.Item("U_AvisoLimProf").Value != null || rs.Fields.Item("U_AvisoLimProf").Value > 0)
                    pd.AvisoLimProf = Convert.ToDouble(rs.Fields.Item("U_AvisoLimProf").Value);
                if (rs.Fields.Item("U_TiempActTread").Value != null || rs.Fields.Item("U_TiempActTread").Value > 0)
                    pd.TiempActTread = Convert.ToDouble(rs.Fields.Item("U_TiempActTread").Value);

                #region Comentado
                //if (rs.Fields.Item("ProximaAlerta").Value != null || rs.Fields.Item("ProximaAlerta").Value > 0)
                //    pd.AlertaProximoServicio = Convert.ToDouble(rs.Fields.Item("ProximaAlerta").Value);
                //if (rs.Fields.Item("U_UlSvTiem").Value != null || rs.Fields.Item("U_UlSvTiem").Value > 0)
                //    pd.UltimoServicioTiempo = Convert.ToDateTime(rs.Fields.Item("U_UlSvTiem").Value);
                //if (rs.Fields.Item("U_ProxSvTiem").Value != null || rs.Fields.Item("U_ProxSvTiem").Value > 0)
                //    pd.ProximoServicioTiempo = Convert.ToDateTime(rs.Fields.Item("U_ProxSvTiem").Value);
                //if (rs.Fields.Item("U_AlProSvTie").Value != null || rs.Fields.Item("U_AlProSvTie").Value > 0)
                //    pd.AlertaProximoServicioTiempo = Convert.ToDateTime(rs.Fields.Item("U_AlProSvTie").Value);
                //if (rs.Fields.Item("U_TMantbleID").Value != null || rs.Fields.Item("U_TMantbleID").Value > 0)
                //{
                //    if (rs.Fields.Item("U_MantbleID").Value != null || rs.Fields.Item("U_MantbleID").Value > 0)
                //    {
                //        int tipo = Convert.ToInt32(rs.Fields.Item("U_TMantbleID").Value);
                //        if (tipo == 1)
                //        {
                //            pm.Mantenible = new Vehiculo();
                //            pm.Mantenible.MantenibleID = Convert.ToInt32(rs.Fields.Item("U_MantbleID").Value);
                //        }
                //    }
                //}
                //if (rs.Fields.Item("U_UlAlEnviad").Value != null || rs.Fields.Item("U_UlAlEnviad").Value > 0)
                //    pm.UltimaAlertaEnviada = Convert.ToDateTime(rs.Fields.Item("U_UlAlEnviad").Value);
                //if (rs.Fields.Item("U_PServID").Value != null || rs.Fields.Item("U_PServID").Value > 0)
                //    pm.ParametroServicio.ParametroServicioID = Convert.ToInt32(rs.Fields.Item("U_PServID").Value);
                //if (rs.Fields.Item("U_TipoPmtro").Value != null || rs.Fields.Item("U_TipoPmtro").Value > 0)
                //    pm.TipoParametro = Convert.ToInt32(rs.Fields.Item("U_TipoPmtro").Value);
                #endregion
                return pd;
            }
            #region catch
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            #endregion
        }
        #endregion

        #region Recordset a lista
        public List<ParametroDesgaste> RecordSetToList(Recordset rs)
        {
            try
            {
                List<ParametroDesgaste> lst = new List<ParametroDesgaste>();
                rs.MoveFirst();
                for (int i = 0; i < rs.RecordCount; i++)
                {
                    lst.Add(RowRecordSetToParametroDesgaste(rs));
                    rs.MoveNext();
                }
                return lst;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Hash
        public bool HasParametroDesgaste(Recordset rs)
        {
            if (rs.RecordCount < 1)
                return false;
            return true;
        }
        #endregion

        #region Actualizar ParametroDesgaste de tipo de vehículo
        public void ActualizarPDesgasteTipoVehiculo()
        {
            try
            {
                /*oUDT.UserFields.Fields.Item("U_ProxServ").Value = this.oParametroDesgaste.ParametroServicio.Valor ?? 0;
                oUDT.UserFields.Fields.Item("U_AlProxServ").Value = this.oParametroDesgaste.ParametroServicio.Alerta ?? 0;
                oUDT.UserFields.Fields.Item("U_PServID").Value = this.oParametroDesgaste.ParametroServicio.ParametroServicioID ?? 0;*/
                this.Actualizar();
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region Enviar Tabla de usuario ParametroMantenimiento
        public bool SetUDTPDesgasteID(int? id)
        {
            try
            {
                var rs = this.ConsultarByID(id);
                if (HasParametroDesgaste(rs))
                {
                    rs.MoveFirst();
                    int _id = rs.Fields.Item("U_PmDesID").Value;
                    if (_id == id)
                        return this.oUDT.GetByKey(rs.Fields.Item("Code").Value);
                }
                this.oUDT.Code = string.Empty;
                return false;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region Consultar por ID
        public Recordset ConsultarByID(int? id)
        {
            try
            {
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(string.Format("Select * from " + this.NombreTabla + " where U_PmDesID = {0}", id));
                return rs;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region Eliminar completo
        public void EliminarCompleto()
        {
            try
            {
                this.ItemCode = this.oParametroDesgaste.ParametroDesgasteID.ToString();
                if (this.Existe())
                    this.Eliminar();
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion
    }
}