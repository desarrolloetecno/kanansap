﻿using KananSAP.WebApiServices.Models;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using KananSAP.Alertas.Data;
using Kanan.Mantenimiento.BO2;
using Kanan.Alertas.BO2;
using Kanan.Vehiculos.BO2;

namespace KananSAP.WebApiServices.Controllers
{
    public class AlertasController : ApiController
    {
        #region Atributos
        private globals_Renamed global = new globals_Renamed();
        private SAPbobsCOM.Recordset oRecordSet;
        #endregion

        #region Conexion
        private void SetConnection()
        {
            global.InitializeCompany();
            //GetCompanyList();

            global.oCompany.DbServerType = (BoDataServerTypes)(Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("DBType")));

            global.oCompany.UseTrusted = false;
            global.oCompany.DbUserName = System.Configuration.ConfigurationManager.AppSettings.Get("ServerUser");
            global.oCompany.DbPassword = System.Configuration.ConfigurationManager.AppSettings.Get("ServerPass");
            global.oCompany.CompanyDB = System.Configuration.ConfigurationManager.AppSettings.Get("ConmpanyDataBase");
            global.oCompany.UserName = System.Configuration.ConfigurationManager.AppSettings.Get("SAPUser");
            global.oCompany.Password = System.Configuration.ConfigurationManager.AppSettings.Get("SAPPass");
            // Connecting to a company DB
            global.lRetCode = global.oCompany.Connect();

            if (global.lRetCode != 0)
            {
                int temp_int = global.lErrCode;
                string temp_string = global.sErrMsg;
                global.oCompany.GetLastError(out temp_int, out temp_string);
                throw new Exception(global.sErrMsg + "Error: " + temp_int + " - " + temp_string);
            }
        }

        private void CloseConnection()
        {
            if (global.oCompany.Connected)
            {
                global.oCompany.Disconnect();
            }
        }

        #endregion

        #region Alertas Mantenimiento
        public class AlertaMantenimientoDto 
        {
            public AlertaMantenimiento AlertaMantenimiento { get; set; }
            public int? MantenibleID { get; set; }
            public int? TipoMantenibleID { get; set; }
            /// <summary>
            /// Indica el tipo de parámetro por el cual se genera la alerta
            /// Distancia = 1, Tiempo = 2;
            /// </summary>
            public int? TypeOfType { get; set; }
        }

        private IMantenible GetMantenible(int? tipoMantenibleID)
        {
            try
            {
                if (tipoMantenibleID == 1)
                    return new Vehiculo();
                else if (tipoMantenibleID == 2)
                    return new Caja();
                else if (tipoMantenibleID == 3)
                    return new TipoVehiculo();
                return null;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        private void ValidaAlertaMantenimientDto(AlertaMantenimientoDto alertaMto)
        {
            try
            {
                if (alertaMto == null)
                    throw new Exception("AlertaMantenimientoDto no puede ser nulo");
                if (alertaMto.AlertaMantenimiento == null)
                    throw new Exception("AlertaMantenimiento no puede ser nulo");
                if (alertaMto.AlertaMantenimiento.AlertaMantenimientoID == null)
                    throw new Exception("AlertaMantenimiento.AlertaMantenimientoID no puede ser nulo");
                if (alertaMto.AlertaMantenimiento.empresa == null)
                    throw new Exception("AlertaMantenimiento.empresa no puede ser nulo");
                if (alertaMto.AlertaMantenimiento.empresa.EmpresaID == null)
                    throw new Exception("AlertaMantenimiento.empresa.EmpresaID no puede ser nulo");
                if (alertaMto.AlertaMantenimiento.servicio == null)
                    throw new Exception("AlertaMantenimiento.servicio no puede ser nulo");
                if (alertaMto.AlertaMantenimiento.servicio.ServicioID == null)
                    throw new Exception("AlertaMantenimiento.servicio.ServicioID no puede ser nulo");
                if (alertaMto.MantenibleID == null)
                    throw new Exception("AlertaMantenimientoDto.MantenibleID no puede ser nulo");
                if (alertaMto.TipoMantenibleID == null)
                    throw new Exception("AlertaMantenimientoDto.TipoMantenibleID no puede ser nulo");
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        [HttpPost]//api/Alertas/InsertAlertaMantenimiento
        public bool InsertAlertaMantenimiento(AlertaMantenimientoDto alertaMto)
        {
            try
            {
                this.ValidaAlertaMantenimientDto(alertaMto);
                this.SetConnection();
                AlertaMantenimientoData alMantData = new AlertaMantenimientoData(this.global.oCompany);
                AlertaData alData = new AlertaData(this.global.oCompany);
                alMantData.AlertaMantenimiento = alertaMto.AlertaMantenimiento;
                alMantData.AlertaMantenimiento.Mantenible = this.GetMantenible(alertaMto.TipoMantenibleID);
                alertaMto.AlertaMantenimiento.Mantenible.MantenibleID = alertaMto.MantenibleID;
                alMantData.AlertaMantenimientoToUDT();
                alMantData.Insert();
                return true;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
            finally { this.CloseConnection(); }
        }

        [HttpPost]//api/Alertas/UpdateAlertaMantenimiento
        public bool UpdateAlertaMantenimiento(AlertaMantenimientoDto alertaMto)
        {
            try
            {
                this.ValidaAlertaMantenimientDto(alertaMto);
                this.SetConnection();
                AlertaMantenimientoData alMantData = new AlertaMantenimientoData(this.global.oCompany);
                alMantData.ItemCode = alertaMto.AlertaMantenimiento.AlertaMantenimientoID.ToString();
                if(alMantData.Existe())
                {
                    alMantData.AlertaMantenimiento = alertaMto.AlertaMantenimiento;
                    alMantData.AlertaMantenimiento.Mantenible = this.GetMantenible(alertaMto.TipoMantenibleID);
                    alertaMto.AlertaMantenimiento.Mantenible.MantenibleID = alertaMto.MantenibleID;
                    //Indica que esta en estado critico
                    alMantData.AlertaMantenimiento.TipoAlerta = 2;
                    alMantData.AlertaMantenimientoToUDT();
                    alMantData.Actualizar();
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
            finally { this.CloseConnection(); }
        }

        #endregion

        #region Alertas

        [HttpPost]//api/Alertas/InsertAlerta
        public bool InsertAlerta(Alerta alerta)
        {
            try
            {
                this.SetConnection();
                AlertaData alData = new AlertaData(this.global.oCompany);
                alData.Alerta = alerta;
                alData.AlertaToUDT();
                alData.Insert();
                return true;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
            finally { this.CloseConnection(); }
        }

        [HttpPost]//api/Alertas/UpdateAlerta
        public bool UpdateAlerta(Alerta alerta)
        {
            try
            {
                this.SetConnection();
                AlertaData alData = new AlertaData(this.global.oCompany);
                alData.ItemCode = alerta.AlertaID.ToString();
                if(alData.Existe())
                {
                    alData.Alerta = alerta;
                    alData.AlertaToUDT();
                    alData.Actualizar();
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
            finally { this.CloseConnection(); }
        }

        #endregion

    }
}
