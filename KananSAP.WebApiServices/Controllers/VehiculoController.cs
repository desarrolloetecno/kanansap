﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Kanan.Costos.BO2;
using KananSAP.Costos.Data;
using KananSAP.WebApiServices.Models;
using SAPbobsCOM;

namespace KananSAP.WebApiServices.Controllers
{
    public class VehiculoController : ApiController
    {
        #region Atributos
        private globals_Renamed global = new globals_Renamed();
        private SAPbobsCOM.Recordset oRecordSet;
        #endregion

        #region Conexion
        private void SetConnection()
        {
            global.InitializeCompany();
            //GetCompanyList();

            global.oCompany.DbServerType = (BoDataServerTypes)(Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("DBType")));
            
            global.oCompany.UseTrusted = false;
            global.oCompany.DbUserName = System.Configuration.ConfigurationManager.AppSettings.Get("ServerUser");
            global.oCompany.DbPassword = System.Configuration.ConfigurationManager.AppSettings.Get("ServerPass");
            global.oCompany.CompanyDB = System.Configuration.ConfigurationManager.AppSettings.Get("ConmpanyDataBase");
            global.oCompany.UserName = System.Configuration.ConfigurationManager.AppSettings.Get("SAPUser");
            global.oCompany.Password = System.Configuration.ConfigurationManager.AppSettings.Get("SAPPass");
            // Connecting to a company DB
            global.lRetCode = global.oCompany.Connect();

            if (global.lRetCode != 0)
            {
                int temp_int = global.lErrCode;
                string temp_string = global.sErrMsg;
                global.oCompany.GetLastError(out temp_int, out temp_string);
                throw new Exception(global.sErrMsg + "Error: " + temp_int + " - " + temp_string);
            }
        }

        private void CloseConnection()
        {
            if (global.oCompany.Connected)
            {
                global.oCompany.Disconnect();
            }
        }
        
        #endregion

        #region Vehiculosr
        // GET api/vehiculo/GetVehiculo
        [HttpGet]
        public IHttpActionResult GetVehiculo(int id)
        {
            try
            {
                SetConnection();
                KananSAP.Vehiculos.Data.VehiculoSAP sapvehicle = new KananSAP.Vehiculos.Data.VehiculoSAP(global.oCompany);
                if (!sapvehicle.SetUDTByVehiculoID(id))
                {
                    throw new Exception("Identificador Invalido");
                }
                sapvehicle.SAPToVehiculo();
                var vehiculo = sapvehicle.vehiculo;
                return Ok(vehiculo);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally
            {
                CloseConnection();
            }
        }

        // Post api/vehiculo/Insert
        [HttpPost]
        public IHttpActionResult Insert(Vehiculo vehiculo)
        {
            try
            {
                SetConnection();
                VehiculoSAP sapvehicle = new VehiculoSAP(global.oCompany);
                sapvehicle.vehiculo = vehiculo;
                sapvehicle.VehiculoToSAP();
                //sapvehicle.Insert();
                sapvehicle.Update();
                sapvehicle.SetUDTByVehiculoID((int) vehiculo.VehiculoID);
                sapvehicle.UDTToVehiculo();
                return Ok(sapvehicle.vehiculo);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally
            {
                CloseConnection();
            }
        }

        // Post api/vehiculo/Update
        [HttpPost]
        public IHttpActionResult Update(Vehiculo vehiculo)
        {
            try
            {
                SetConnection();
                VehiculoSAP sapvehicle = new VehiculoSAP(global.oCompany);
                if (vehiculo.VehiculoID != null) sapvehicle.SetUDTByVehiculoID((int) vehiculo.VehiculoID);
                else throw new Exception("Dato Invalido");
                sapvehicle.oItem.GetByKey(sapvehicle.oUDT.Code);
                sapvehicle.ItemCode = sapvehicle.oUDT.Code;
                sapvehicle.vehiculo = vehiculo;
                sapvehicle.VehiculoToUDT();
                sapvehicle.Update();
                sapvehicle.SAPToVehiculo();
                return Ok(sapvehicle.vehiculo);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally
            {
                CloseConnection();
            }
        }

        // Post api/vehiculo/Delete
        [HttpGet]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                SetConnection();
                VehiculoSAP sapvehicle = new VehiculoSAP(global.oCompany);
                sapvehicle.SetUDTByVehiculoID(id);
                sapvehicle.Deactivate();
                sapvehicle.SAPToVehiculo();
                return Ok(sapvehicle.vehiculo);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally
            {
                CloseConnection();
            }
        }
        #endregion

        #region Carga de Combustible

        //GET api/vehiculo/GetCargaByVehiculoID
        [HttpGet]
        public IHttpActionResult GetCargaByVehiculoID(int id)
        {
            try
            {
                SetConnection();
                CargaCombustibleSAP cargasap = new CargaCombustibleSAP(global.oCompany);
                var listresult = cargasap.Consultar(new CargaCombustible{Movil = new Vehiculo{VehiculoID = id}});
                return Ok(listresult);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally
            {
                CloseConnection();
            }
        }

        // GET api/vehiculo/GetCargaByID
        [HttpGet]
        public IHttpActionResult GetCargaByID(int id)
        {
            try
            {
                SetConnection();
                CargaCombustibleSAP cargasap = new CargaCombustibleSAP(global.oCompany);
                if (!cargasap.oUDT.GetByKey(id.ToString()))
                {
                    throw new Exception("Identificador Invalido");
                }
                cargasap.SAPToCarga();
                var carga = cargasap.carga;
                return Ok(carga);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally
            {
                CloseConnection();
            }
        }

        // GET api/vehiculo/GetCarga
        [HttpPost]
        public IHttpActionResult GetCarga(CargaCombustible carga)
        {
            try
            {
                SetConnection();
                CargaCombustibleSAP cargasap = new CargaCombustibleSAP(global.oCompany);
                Recordset rs = cargasap.Consultar(carga);
                var result = cargasap.RecordSetToListCargaCombustible(rs);
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally
            {
                CloseConnection();
            }
        }

        // Post api/vehiculo/AddCarga
        [HttpPost]
        public IHttpActionResult AddCarga(CargaCombustible carga)
        {
            try
            {
                SetConnection();
                CargaCombustibleSAP cargasap = new CargaCombustibleSAP(global.oCompany);
                cargasap.carga = carga;
                cargasap.CargaToSAP();
                cargasap.Insert();
                return Ok(cargasap.carga);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally
            {
                CloseConnection();
            }
        }

        // Post api/vehiculo/Update
        [HttpPost]
        public IHttpActionResult UpdateCarga(CargaCombustible carga)
        {
            try
            {
                SetConnection();
                CargaCombustibleSAP cargasap = new CargaCombustibleSAP(global.oCompany);
                if (carga.CargaCombustibleID != null && carga.CargaCombustibleID > 0 &&
                    cargasap.oUDT.GetByKey(carga.CargaCombustibleID.ToString()))
                {
                    cargasap.ItemCode = carga.CargaCombustibleID.ToString();
                    cargasap.carga = carga;
                    cargasap.CargaToSAP();
                    cargasap.Update();
                    cargasap.oUDT.GetByKey(cargasap.ItemCode);
                    cargasap.SAPToCarga();
                }
                else throw new Exception("Dato Invalido");
                return Ok(cargasap.carga);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally
            {
                CloseConnection();
            }
        }

        // Post api/vehiculo/Delete
        [HttpGet]
        public IHttpActionResult DeleteCarga(int id)
        {
            try
            {
                SetConnection();
                CargaCombustibleSAP cargasap = new CargaCombustibleSAP(global.oCompany);
                if (cargasap.oUDT.GetByKey(id.ToString()))
                {
                    cargasap.SAPToCarga();
                    cargasap.Delete();
                    return Ok(cargasap.carga);
                }
                throw new Exception("Dato invalido");
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally
            {
                CloseConnection();
            }
        }
        #endregion

    }
}
