﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using KananSAP.Operaciones.Data;
using KananSAP.Vehiculos.Data;
using KananSAP.WebApiServices.Models;
using SAPbobsCOM;

namespace KananSAP.WebApiServices.Controllers
{
    public class OperacionesController : ApiController
    {
        #region Atributos
        private globals_Renamed global = new globals_Renamed();
        private SAPbobsCOM.Recordset oRecordSet;
        #endregion

        #region Conexion
        private void SetConnection()
        {
            global.InitializeCompany();

            global.oCompany.DbServerType = (BoDataServerTypes)(Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("DBType")));

            global.oCompany.UseTrusted = false;
            global.oCompany.DbUserName = System.Configuration.ConfigurationManager.AppSettings.Get("ServerUser");
            global.oCompany.DbPassword = System.Configuration.ConfigurationManager.AppSettings.Get("ServerPass");
            global.oCompany.CompanyDB = System.Configuration.ConfigurationManager.AppSettings.Get("ConmpanyDataBase");
            global.oCompany.UserName = System.Configuration.ConfigurationManager.AppSettings.Get("SAPUser");
            global.oCompany.Password = System.Configuration.ConfigurationManager.AppSettings.Get("SAPPass");
            // Connecting to a company DB
            global.lRetCode = global.oCompany.Connect();

            if (global.lRetCode == 0) return;
            int temp_int = global.lErrCode;
            string temp_string = global.sErrMsg;
            global.oCompany.GetLastError(out temp_int, out temp_string);
            throw new Exception(global.sErrMsg + "Error: " + temp_int + " - " + temp_string);
        }

        private void CloseConnection()
        {
            if (global.oCompany.Connected)
            {
                global.oCompany.Disconnect();
            }
        }

        #endregion

        #region Operadores

        // GET api/Operaciones/GetOperador
        [HttpGet]
        public IHttpActionResult GetOperador(int id)
        {
            try
            {
                SetConnection();
                OperadorSAP sapOperador = new OperadorSAP(global.oCompany);
                if (!sapOperador.oUDT.GetByKey(id.ToString()))
                {
                    throw new Exception("Identificador Invalido");
                }
                sapOperador.UDTToOperador();
                var Operador = sapOperador.operador;
                return Ok(Operador);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally
            {
                CloseConnection();
            }
        }

        // Post api/Operaciones/InsertOperador
        [HttpPost]
        public IHttpActionResult InsertOperador(Operador Operador)
        {
            try
            {
                SetConnection();
                OperadorSAP sapOperador = new OperadorSAP(global.oCompany);
                sapOperador.operador = Operador;
                sapOperador.OperadorToUDT();
                sapOperador.Insert();
                sapOperador.oUDT.GetByKey(Operador.OperadorID.ToString());
                sapOperador.UDTToOperador();
                return Ok(sapOperador.operador);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally
            {
                CloseConnection();
            }
        }

        // Post api/Operaciones/Update
        [HttpPost]
        public IHttpActionResult UpdateOperador(Operador Operador)
        {
            try
            {
                SetConnection();
                OperadorSAP sapOperador = new OperadorSAP(global.oCompany);
                if (Operador.OperadorID != null) sapOperador.oUDT.GetByKey(Operador.OperadorID.ToString());
                else throw new Exception("Dato Invalido");
                sapOperador.ItemCode = sapOperador.oUDT.Code;
                sapOperador.operador = Operador;
                sapOperador.OperadorToUDT();
                sapOperador.Update();
                sapOperador.UDTToOperador();
                return Ok(sapOperador.operador);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally
            {
                CloseConnection();
            }
        }

        // Post api/Operaciones/Delete
        [HttpGet]
        public IHttpActionResult DeleteOperador(int id)
        {
            try
            {
                SetConnection();
                OperadorSAP sapOperador = new OperadorSAP(global.oCompany);
                sapOperador.oUDT.GetByKey(id.ToString());
                sapOperador.UDTToOperador();
                sapOperador.Deactivate();
                return Ok(sapOperador.operador);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally
            {
                CloseConnection();
            }
        }

        #region AsignarOperador
        // Post api/Operaciones/AsignaOperador
        [HttpPost]
        public IHttpActionResult AsignaOperador(OperadorVehiculo Operadorv)
        {
            try
            {
                SetConnection();
                OperadorVehiculoSAP sapOperadorv = new OperadorVehiculoSAP(global.oCompany);
                sapOperadorv.operadorv = Operadorv;
                sapOperadorv.OperadorToUDT();
                sapOperadorv.Insert();
                sapOperadorv.oUDT.GetByKey(Operadorv.OperadorAsignado.OperadorID.ToString());
                sapOperadorv.UDTToOperador();
                return Ok(sapOperadorv.operadorv);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally
            {
                CloseConnection();
            }
        }


        // Post api/Operaciones/LiberaOperador
        [HttpPost]
        public IHttpActionResult LiberaOperador(OperadorVehiculo Operadorv)
        {
            try
            {
                SetConnection();
                OperadorVehiculoSAP sapOperadorv = new OperadorVehiculoSAP(global.oCompany);
                if (Operadorv.OperadorVehiculoID != null) sapOperadorv.oUDT.GetByKey(Operadorv.OperadorVehiculoID.ToString());
                else throw new Exception("Dato Invalido");
                sapOperadorv.ItemCode = sapOperadorv.oUDT.Code;
                sapOperadorv.operadorv = Operadorv;
                sapOperadorv.OperadorToUDT();
                sapOperadorv.Update();
                sapOperadorv.UDTToOperador();
                return Ok(sapOperadorv.operadorv);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally
            {
                CloseConnection();
            }
        }
        #endregion

        #endregion
    }
}
