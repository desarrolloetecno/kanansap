﻿using Kanan.Mantenimiento.BO2;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using KananSAP.Alertas.Data;
using KananSAP.Helpers;
using KananSAP.Mantenimiento.Data;
using KananSAP.Vehiculos.Data;
using KananSAP.WebApiServices.Models;
using SAPbobsCOM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace KananSAP.WebApiServices.Controllers
{
    public class ServiciosController : ApiController
    {
        #region Atributos
        private globals_Renamed global = new globals_Renamed();
        private SAPbobsCOM.Recordset oRecordSet;
        #endregion

        #region Conexion
        private void SetConnection()
        {
            global.InitializeCompany();
            //GetCompanyList();

            global.oCompany.DbServerType = (BoDataServerTypes)(Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("DBType")));

            global.oCompany.UseTrusted = false;
            global.oCompany.DbUserName = System.Configuration.ConfigurationManager.AppSettings.Get("ServerUser");
            global.oCompany.DbPassword = System.Configuration.ConfigurationManager.AppSettings.Get("ServerPass");
            global.oCompany.CompanyDB = System.Configuration.ConfigurationManager.AppSettings.Get("ConmpanyDataBase");
            global.oCompany.UserName = System.Configuration.ConfigurationManager.AppSettings.Get("SAPUser");
            global.oCompany.Password = System.Configuration.ConfigurationManager.AppSettings.Get("SAPPass");
            // Connecting to a company DB
            global.lRetCode = global.oCompany.Connect();

            if (global.lRetCode != 0)
            {
                int temp_int = global.lErrCode;
                string temp_string = global.sErrMsg;
                global.oCompany.GetLastError(out temp_int, out temp_string);
                throw new Exception(global.sErrMsg + "Error: " + temp_int + " - " + temp_string);
            }
        }

        private void CloseConnection()
        {
            if (global.oCompany.Connected)
            {
                global.oCompany.Disconnect();
            }
        }

        #endregion

        #region Servicios
        [HttpGet]// api/Servicios/GetServicioByEmpresaID/
        public IEnumerable GetServicioByEmpresaID(int? id)
        {
            try
            {
                if (!id.HasValue && id <= 0)
                    throw new Exception("Proporcione un identificador válido");
                SetConnection();
                List<Servicio> listaServicio = new List<Servicio>();
                Servicio sv = new Servicio();
                sv.Propietario = new Empresa();
                sv.Propietario.EmpresaID = id;
                ServicioSAP svSAP = new ServicioSAP(global.oCompany);
                Recordset rs = svSAP.Consultar(sv);
                if(svSAP.HashServicios(rs))
                {
                    rs.MoveFirst();
                    for(int i = 0; i < rs.RecordCount; i++)
                    {
                        listaServicio.Add(svSAP.RowRecordSetToServicio(rs));
                        rs.MoveNext();
                    }
                }
                return listaServicio;
            }
            catch(Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally { CloseConnection(); }
        }

        [HttpGet] // api/Servicios/GetServicioByID/
        public IHttpActionResult GetServicioByID(int? id)
        {
            try
            {
                if (!id.HasValue && id <= 0)
                    throw new Exception("Proporcione un identificador válido");
                SetConnection();
                Servicio sv = new Servicio() { ServicioID = id };
                ServicioSAP svData = new ServicioSAP(global.oCompany);
                if (!svData.SetUDTByServicioID(id))
                    throw new Exception("Identificador inválido");
                svData.UDTToServicio();
                return Ok(svData.oServicio);
                //Recordset rs = svData.Consultar(sv);
                //if (svData.HashServicios(rs))
                //    sv = svData.LastRecordSetToServicio(rs);
                //else
                //    throw new Exception("El identificador proporcionado no existe o ha sido eliminado");
                //return Ok(sv);
            }
            catch(Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally { CloseConnection(); }
        }

        [HttpPost]// api/Servicios/AddServicio
        public bool AddServicio(Servicio sv)
        {
            try
            {
                if (sv == null)
                    throw new Exception("Servicio no puede ser nulo");
                if (sv.Propietario == null)
                    throw new Exception("Propietario no puede ser nulo");
                SetConnection();
                ServicioSAP svData = new ServicioSAP(global.oCompany);
                svData.oServicio = sv;
                svData.ServicioToUDT();
                svData.Insertar();
                return true;
            }
            catch(Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally { CloseConnection(); }
        }

        [HttpPost]// api/Servicios/UpdateServicio
        public bool UpdateServicio(Servicio sv)
        {
            try
            {
                if (sv == null)
                    throw new Exception("Servicio no puede ser nulo");
                if (sv.ServicioID == null)
                    throw new Exception("ServicioID no puede ser nulo");
                if (sv.Propietario == null)
                    throw new Exception("Propietario no puede ser nulo");
                SetConnection();
                ServicioSAP svData = new ServicioSAP(global.oCompany);
                svData.ItemCode = sv.ServicioID.ToString();
                if (!svData.Existe())
                    return false;//throw new Exception("La información que intenta modificar no existe o ha sido eliminada");
                svData.oServicio = sv;
                svData.ServicioToUDT();
                svData.Actualizar();
                return true;
            }
            catch(Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally { CloseConnection(); }
        }

        [HttpGet]// api/Servicios/DeleteServicio/
        public bool DeleteServicio(int? id)
        {
            try
            {
                if (!id.HasValue)
                    throw new Exception("Proporcione un identificador válido");
                SetConnection();
                ServicioSAP svData = new ServicioSAP(global.oCompany);
                
                if (svData.SetUDTByServicioID(id))
                {
                    svData.UDTToServicio();
                    svData.EliminarCompleto(); 
                    return true; 
                }
                return false;
                //svData.ItemCode = id.ToString();
                //if (!svData.Existe())
                //    throw new Exception("El registro que intenta eliminar no existe o ha sido eliminada");
                //Servicio sv = new Servicio() { ServicioID = id };
                //Recordset rs = svData.Consultar(sv);
                //if(!svData.HashServicios(rs))
                //    throw new Exception("El registro que intenta eliminar no existe o ha sido eliminada");
                //sv = svData.LastRecordSetToServicio(rs);
                //sv.EsActivo = false;
                //svData.oServicio = sv;
                //svData.ServicioToUDT();
                //svData.EliminarCompleto();//.Actualizar();
                //return true;
            }
            catch(Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally { CloseConnection(); }
        }

        #endregion

        #region Tipo Servicios

        [HttpGet]// api/Servicios/GetTServicioByEmpresaID/
        public IEnumerable GetTServicioByEmpresaID(int? id)
        {
            try
            {
                if (!id.HasValue)
                    throw new Exception("Proporcione un identificador válido");
                SetConnection();
                List<TipoServicio> listaTServicio = new List<TipoServicio>();
                TipoServicioData tsData = new TipoServicioData(global.oCompany);
                TipoServicio ts = new TipoServicio();
                ts.Propietario = new Empresa();
                ts.Propietario.EmpresaID = id;
                Recordset rs = tsData.Consultar(ts);
                if(tsData.HasTipoServicio(rs))
                {
                    rs.MoveFirst();
                    for(int i = 0; i < rs.RecordCount; i++)
                    {
                        listaTServicio.Add(tsData.RowRecordSetToTipoServicio(rs));
                        rs.MoveNext();
                    }
                }
                return listaTServicio;
            }
            catch(Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally { CloseConnection(); }
        }

        [HttpGet]// api/Servicios/GetTServicioByID/
        public IHttpActionResult GetTServicioByID(int? id)
        {
            try
            {
                if (!id.HasValue || id <= 0)
                    throw new Exception("Proporcione un identificador válido");
                SetConnection();
                TipoServicioData tsData = new TipoServicioData(global.oCompany);
                tsData.SetUDTByServicioID(id);
                tsData.UDTToTipoServicio();
                return Ok(tsData.oTipoServicio);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally { CloseConnection(); }
        }

        [HttpPost]// api/Servicios/AddTServicio
        public bool AddTServicio(TipoServicio ts)
        {
            try
            {
                if (ts == null)
                    throw new Exception("TipoServicio no puede ser nulo");
                if (ts.Propietario == null)
                    throw new Exception("Propietario no puede ser nulo");
                SetConnection();
                TipoServicioData tsData = new TipoServicioData(global.oCompany);
                tsData.oTipoServicio = ts;
                tsData.TipoServicioToUDT();
                tsData.Inserta();
                return true;
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally { CloseConnection(); }
        }

        [HttpPost]// api/Servicios/UpdateTServicio
        public bool UpdateTServicio(TipoServicio ts)
        {
            try
            {
                if (ts == null)
                    throw new Exception("TipoServicio no puede ser nulo");
                if (ts.Propietario == null)
                    throw new Exception("Propietario no puede ser nulo");
                SetConnection();
                TipoServicioData tsData = new TipoServicioData(global.oCompany);
                tsData.ItemCode = ts.TipoServicioID.ToString();
                if (!tsData.Existe())
                    return false;//throw new Exception("El registro que intanta actualizar no existe o ha sido eliminado");
                tsData.oTipoServicio = ts;
                tsData.TipoServicioToUDT();
                tsData.Actualizar();
                return true;
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally { CloseConnection(); }
        }

        [HttpGet]// api/Servicios/DeleteTServicio/
        public bool DeleteTServicio(int? id)
        {
            try
            {
                if (!id.HasValue)
                    throw new Exception("Proporcione un identificador válido"); ;
                SetConnection();
                TipoServicioData tsData = new TipoServicioData(global.oCompany);
                if (tsData.SetUDTByServicioID(id))
                {
                    tsData.UDTToTipoServicio();
                    tsData.ActualizarCompleto(); 
                    return true; 
                }
                return false;
                //TipoServicio ts = new TipoServicio() { TipoServicioID = id };
                //tsData.ItemCode = id.ToString();
                //if(!tsData.Existe())
                //    throw new Exception("El registro no existe o ha sido eliminado");
                //Recordset rs = tsData.Consultar(ts);
                //if (!tsData.HasTipoServicio(rs))
                //    throw new Exception("El registro no existe o ha sido eliminado");
                //ts = tsData.LastRecordSetToTipoServicio(rs);
                //ts.EsActivo = false;
                //tsData.oTipoServicio = ts;
                //tsData.TipoServicioToUDT();
                //tsData.ActualizarCompleto();//.Actualizar();
                
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally { CloseConnection(); }
        }

        #endregion

        #region Parametros de mantenimiento y servicio para vehiculos y activos

        #region Auxiliares

        public class ParametroMantenimientoDto
        {
            public ParametroMantenimiento ParametroMantenimiento { get; set; }
            public int? TipoMantenibleID { get; set; }
            public int? MantenibleID { get; set; }
        }

        private IMantenible GetMantenible(int? tipoMantenibleID)
        {
            try
            {
                switch (tipoMantenibleID)
                {
                    case 1:
                        return new Vehiculo();
                    case 2:
                        return new Caja();
                    case 3:
                        return new TipoVehiculo();
                    default:
                        throw new Exception("No se encuantra definido el tipo de mantenible solicitado");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al obtener el tipo de mantenible: " + ex.Message);
            }
        }

        private int? GetTipoMantenibleID(Type mantenible)
        {
            if (mantenible == typeof(Vehiculo))
                return 1;
            else if (mantenible == typeof(Caja))
                return 2;
            else if (mantenible == typeof(TipoVehiculo))
                return 3;
            return null;
        }

        private bool EliminarParamServicio(int? id)
        {
            try
            {
                if (!id.HasValue || id <= 0)
                    throw new Exception("Proporcione un identificador válido");
                ParametroServicioData psData = new ParametroServicioData(global.oCompany);
                Recordset rs = psData.Consultar(new ParametroServicio() { ParametroServicioID = id });
                if (psData.HashParametroServicio(rs))
                {
                    ParametroServicio ps = psData.LastRecordSetToParametroServicio(rs);
                    psData.oParametroServicio = ps;
                    psData.ItemCode = ps.ParametroServicioID.ToString();
                    psData.Existe();
                    psData.ParametroServicioToUDT();
                    psData.Eliminar();
                }
                return true;
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }

        private bool EliminarParamMantenimiento(int? id)
        {
            try
            {
                if (!id.HasValue || id <= 0)
                    throw new Exception("Proporcione un identificador válido");
                ParametroMantenimientoData pmData = new ParametroMantenimientoData(global.oCompany);
                Recordset rs = pmData.Consultar(new ParametroMantenimiento() { ParametroMantenimientoID = id });
                if (pmData.HasParametroMantenimiento(rs))
                {
                    ParametroMantenimiento pm = pmData.LastRecordSetToParametroMantenimiento(rs);
                    pmData.oParametroMantenimiento = pm;
                    pmData.ItemCode = pm.ParametroMantenimientoID.ToString();
                    pmData.Existe();
                    pmData.ParametroMantenimientoToUDT();
                    pmData.Eliminar();
                }
                return true;
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }

        #endregion

        #region Metodos para Vehiculo y Activos
        /// <summary>
        /// Guarda los parametros de mantenimiento
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]// api/Servicios/InsertarCompleto
        public bool InsertarCompleto(ParametroMantenimientoDto dto)
        {
            try
            {
                if (dto == null)
                    throw new Exception("ParametroMantenimientoDto no puede ser nulo");
                if (dto.ParametroMantenimiento == null)
                    throw new Exception("ParametroMantenimiento no puede ser nulo");
                if (dto.TipoMantenibleID == null)
                    throw new Exception("TipoMantenibleID no puede ser nulo");
                if (dto.MantenibleID == null)
                    throw new Exception("MantenibleID no puede ser nulo");
                ParametroMantenimiento pm = new ParametroMantenimiento();
                pm = dto.ParametroMantenimiento;
                pm.Mantenible = this.GetMantenible(dto.TipoMantenibleID);
                pm.Mantenible.MantenibleID = dto.MantenibleID;
                pm.ParametroServicio.Mantenible = pm.Mantenible;
                SetConnection();
                ParametroMantenimientoHelperData helpData = new ParametroMantenimientoHelperData(global.oCompany);
                helpData.oParametroMantenimiento = pm;
                helpData.InsertaCompleto();
                return true;
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally { CloseConnection(); }
        }

        /// <summary>
        /// Guarda o actualiza los parametros de mantenimiento
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]// api/Servicios/InsertarOActualizar
        public bool InsertarOActualizar(ParametroMantenimientoDto dto)
        {
            try
            {
                if (dto == null)
                    throw new Exception("ParametroMantenimientoDto no puede ser nulo");
                if (dto.ParametroMantenimiento == null)
                    throw new Exception("ParametroMantenimiento no puede ser nulo");
                if (dto.TipoMantenibleID == null)
                    throw new Exception("TipoMantenibleID no puede ser nulo");
                if (dto.MantenibleID == null)
                    throw new Exception("MantenibleID no puede ser nulo");
                ParametroMantenimiento pm = new ParametroMantenimiento();
                pm = dto.ParametroMantenimiento;
                pm.Mantenible = this.GetMantenible(dto.TipoMantenibleID);
                pm.Mantenible.MantenibleID = dto.MantenibleID;
                pm.ParametroServicio.Mantenible = pm.Mantenible;
                SetConnection();
                ParametroMantenimientoHelperData helpData = new ParametroMantenimientoHelperData(global.oCompany);
                helpData.oParametroMantenimiento = pm;
                helpData.InsertaOActualizarParametros();
                return true;
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally { CloseConnection(); }
        }

        /// <summary>
        /// Elimina todos los parametros de mantenimiento y de servicio ligados a él
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]// api/Servicios/EliminarCompleto
        public bool EliminarCompleto(ParametroMantenimientoDto dto)
        {
            try
            {
                if (dto == null)
                    throw new Exception("ParametroMantenimientoDto no puede ser nulo");
                if (dto.ParametroMantenimiento == null)
                    throw new Exception("ParametroMantenimiento no puede ser nulo");
                if (dto.ParametroMantenimiento.ParametroMantenimientoID == null)
                    throw new Exception("ParametroMantenimientoID no puede ser nulo");
                if (dto.ParametroMantenimiento.ParametroServicio == null)
                    throw new Exception("ParametroServicio no puede ser nulo");
                if (dto.ParametroMantenimiento.ParametroServicio.ParametroServicioID == null)
                    throw new Exception("ParametroServicioID no puede ser nulo");
                this.SetConnection();
                this.EliminarParamMantenimiento(dto.ParametroMantenimiento.ParametroMantenimientoID);
                this.EliminarParamServicio(dto.ParametroMantenimiento.ParametroServicio.ParametroServicioID);
                return true;
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally { CloseConnection(); }
        }

        #endregion

        #region Parametro de mantenimiento como tipo de vehiculo
        [HttpPost]// api/Servicios/ActualizarParametroMantenimiento
        public bool ActualizarParametroMantenimiento(ParametroMantenimientoDto dto)
        {
            try
            {
                if (dto == null)
                    throw new Exception("ParametroMantenimientoDto no puede ser nulo");
                if (dto.ParametroMantenimiento == null)
                    throw new Exception("ParametroMantenimiento no puede ser nulo");
                if (dto.ParametroMantenimiento.ParametroMantenimientoID == null)
                    throw new Exception("ParametroMantenimientoID no puede ser nulo");
                if (dto.TipoMantenibleID == null)
                    throw new Exception("TipoMantenibleID no puede ser nulo");
                if (dto.MantenibleID == null)
                    throw new Exception("MantenibleID no puede ser nulo");
                ParametroMantenimiento pm = new ParametroMantenimiento();
                pm = dto.ParametroMantenimiento;
                pm.Mantenible = this.GetMantenible(dto.TipoMantenibleID);
                pm.Mantenible.MantenibleID = dto.MantenibleID;
                pm.ParametroServicio.Mantenible = pm.Mantenible;
                SetConnection();
                ParametroMantenimientoData pmData = new ParametroMantenimientoData(global.oCompany);
                pmData.ItemCode = pm.ParametroMantenimientoID.ToString();
                if (!pmData.Existe())
                    return false;//throw new Exception("El registro que intenta modificar no existe o ha sido eliminado");
                pmData.oParametroMantenimiento = pm;
                pmData.ParametroMantenimientoToUDT();
                pmData.Actualizar();
                return true;
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally { CloseConnection(); }
        }

        [HttpGet]// api/Servicios/EliminarPMantenimiento/
        public bool EliminarPMantenimiento(int? id)
        {
            try
            {
                if (!id.HasValue || id <= 0)
                    throw new Exception("Proporcione un identificador válido");
                ParametroMantenimientoData pmData = new ParametroMantenimientoData(global.oCompany);
                Recordset rs = pmData.Consultar(new ParametroMantenimiento() { ParametroMantenimientoID = id });
                if(pmData.HasParametroMantenimiento(rs))
                {
                    ParametroMantenimiento pm = pmData.LastRecordSetToParametroMantenimiento(rs);
                    pmData.oParametroMantenimiento = pm;
                    pmData.ItemCode = pm.ParametroMantenimientoID.ToString();
                    pmData.Existe();
                    pmData.ParametroMantenimientoToUDT();
                    pmData.Eliminar();
                    return true;
                }
                return false;
            }
            catch(Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
        }

        #endregion


        #endregion

        #region Parametros de servicio para Tipo de vehiculos
        
        #region Auxiliares
        public class ParametroServicioDto
        {
            public ParametroServicio ParametroServicio { get; set; }
            public int? TipoMantenibleID { get; set; }
            public int? MantenibleID { get; set; }
        }

        private void ValidatePmtroServicio(ParametroServicioDto psDto)
        {
            try
            {
                if (psDto == null)
                    throw new Exception("ParametroServicioDto no puede ser nulo");
                if (psDto.ParametroServicio == null)
                    throw new Exception("ParametroServicioDto.ParametroServicio no puede ser nulo");
                if (psDto.TipoMantenibleID == null)
                    throw new Exception("ParametroServicioDto.TipoMantenibleID no puede ser nulo");
                if (psDto.MantenibleID == null)
                    throw new Exception("ParametroServicioDto.MantenibleID no puede ser nulo");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Metodos
        /// <summary>
        /// Guarda los parametros de servicio para tipos de vehiculo
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]// api/Servicios/InsertParametroServicio
        public bool InsertParametroServicio(ParametroServicioDto dto)
        {
            try
            {
                this.ValidatePmtroServicio(dto);
                ParametroServicio ps = new ParametroServicio();
                ps = dto.ParametroServicio;
                ps.Mantenible = this.GetMantenible(dto.TipoMantenibleID);
                ps.Mantenible.MantenibleID = dto.MantenibleID;
                SetConnection();
                ParametroServicioData psData = new ParametroServicioData(global.oCompany);
                psData.oParametroServicio = ps;
                psData.ParametroServicioToUDT();
                psData.Insert();
                return true;
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally { CloseConnection(); }
        }

        /// <summary>
        /// Actualiza unicamente el parametro de servicio seleccionado
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]// api/Servicios/ActualizarParametroServicio
        public bool ActualizarParametroServicio(ParametroServicioDto dto)
        {
            try
            {
                this.ValidatePmtroServicio(dto);
                ParametroServicio ps = new ParametroServicio();
                ps = dto.ParametroServicio;
                ps.Mantenible = this.GetMantenible(dto.TipoMantenibleID);
                ps.Mantenible.MantenibleID = dto.MantenibleID;
                SetConnection();
                ParametroServicioData psData = new ParametroServicioData(global.oCompany);
                psData.ItemCode = ps.ParametroServicioID.ToString();
                if (!psData.Existe())
                    return false;//throw new Exception("El registro que intenta modificar no existe o ha sido eliminado");
                psData.oParametroServicio = ps;
                psData.ParametroServicioToUDT();
                psData.Actualizar();
                return true;
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally { CloseConnection(); }

        }

        /// <summary>
        /// Actualiza los parametros de servicio y de mantenimiento de todos los tipos de vehiculos que utilizen el parametro de servicio seleccionado
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]// api/Servicios/ActualizarTodosParametrosServicio
        public bool ActualizarTodosParametrosServicio(ParametroServicioDto dto)
        {
            try
            {
                this.ValidatePmtroServicio(dto);
                ParametroServicio ps = new ParametroServicio();
                ps = dto.ParametroServicio;
                ps.Mantenible = this.GetMantenible(dto.TipoMantenibleID);
                ps.Mantenible.MantenibleID = dto.MantenibleID;
                SetConnection();
                ParametroServicioData psData = new ParametroServicioData(global.oCompany);
                psData.ItemCode = ps.ParametroServicioID.ToString();
                if (!psData.Existe())
                    return false;//throw new Exception("El registro que intenta modificar no existe o ha sido eliminado");
                psData.oParametroServicio = ps;
                psData.ParametroServicioToUDT();
                psData.Actualizar();

                ParametroMantenimientoData pmData = new ParametroMantenimientoData(global.oCompany);
                ParametroMantenimiento pm = new ParametroMantenimiento();
                pm.ParametroServicio = dto.ParametroServicio;
                Recordset rs = pmData.Consultar(pm);
                if(pmData.HasParametroMantenimiento(rs))
                {
                    pm = pmData.LastRecordSetToParametroMantenimiento(rs);
                    if(pmData.SetUDTPMantenimientoID(pm.ParametroMantenimientoID))
                    {
                        pmData.oParametroMantenimiento = pm;
                        pmData.ActualizarPMantenimientoTipoVehiculo();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally { CloseConnection(); }

        }

        /// <summary>
        /// Elimina el parametro de servicio y todos los parametros de mantenimiento ligados a él
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]// api/Servicios/EliminarCompletoPServicio
        public bool EliminarCompletoPServicio(int? id)
        {
            try
            {
                if (!id.HasValue || id <= 0)
                    throw new Exception("Proporcione un identificador válido");
                SetConnection();
                ParametroServicioData psData = new ParametroServicioData(global.oCompany);
                Recordset rs = psData.Consultar(new ParametroServicio() { ParametroServicioID = id });
                if(psData.HashParametroServicio(rs))
                {
                    ParametroServicio temp = psData.LastRecordSetToParametroServicio(rs);
                    if (psData.SetUDTToPServicio(id))
                    {
                        psData.oParametroServicio = temp;
                        psData.EliminarCompleto();
                        return true;
                    }
                }
                return false; ;
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally { CloseConnection(); }
        }

        #endregion

        #endregion


        #region Ordenes De Servicio

        public class OrdenServicioDto
        {
            public List<serviciocosto> listServicioCosto { get; set; }
            public OrdenServicio ordenServicio { get; set; }
            //public int? AlertaID { get; set; }
            //public int? EmpresaID { get; set; }
            public int? MantenibleID { get; set; }
            public int? TipoMantenibleID { get; set; }
            public List<OrdenServicio> ListaOrdenes { get; set; }
            public List<OrdenesDto> OrdenesDto { get; set; }
        }

        public class OrdenesDto
        {
            public OrdenServicio OrdenServicio { get; set; }
            public int? MantenibleID { get; set; }
            public int? TipoMantenibleID { get; set; }
        }

        #region Ordenes De Servicio Con Alerta Generada
        private void ValidateOrden(OrdenServicioDto dto)
        {
            try
            {
                if (dto == null)
                    throw new Exception("OrdenServicioDto no puede ser nulo");
                if (dto.ordenServicio == null)
                    throw new Exception("OrdenServicio no puede ser nulo");
                //if (dto.listServicioCosto == null)
                    //throw new Exception("ServicioCosto no puede ser nulo");
                if (dto.OrdenesDto == null)
                    throw new Exception("Lista OrdeneServicio no puede ser nulo");
                if(dto.MantenibleID == null)
                    throw new Exception("MantenibleID no puede ser nulo");
                if (dto.TipoMantenibleID == null)
                    throw new Exception("TipoMantenibleID no puede ser nulo");
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        [HttpPost]// api/Servicios/InsertOrdenServicioConAlerta
        public bool InsertOrdenServicioConAlerta(OrdenServicioDto dto)
        {
            try
            {
                //this.ValidateOrden(dto);
                if (dto == null)
                    return false;
                if (dto.OrdenesDto == null)
                    return false;
                if (dto.OrdenesDto.Count <= 0)
                    return false;
                if (dto.listServicioCosto == null)
                    return false;
                if (dto.listServicioCosto.Count <= 0)
                    return false;
                
                //List<serviciocosto> listServicios = dto.listServicioCosto;
                //OrdenServicio ordenesServicios = new OrdenServicio();
                //ordenesServicios = dto.ordenServicio;
                //ordenesServicios.alertas.Mantenible = this.GetMantenible(dto.TipoMantenibleID);
                //ordenesServicios.alertas.Mantenible.MantenibleID = dto.MantenibleID;
                this.SetConnection();
                OrdenServicioData ordenData = new OrdenServicioData(this.global.oCompany);
                //foreach(serviciocosto servicios in dto.listServicioCosto)
                //{
                //    if (ordenesServicios.alertas != null)
                //    {
                //        ordenesServicios.Costo = servicios.costo;
                //        ordenesServicios.Impuesto = servicios.impuesto;
                //        if (servicios.tipoMantenibleID == 1)
                //        {
                //            ordenesServicios.alertas.Mantenible = null;
                //            ordenesServicios.alertas.Mantenible = new Vehiculo();
                //            ordenesServicios.alertas.Mantenible.MantenibleID = servicios.vehiculoid;
                //            ordenesServicios.alertas.servicio.ServicioID = servicios.servicioid;
                //            ordenesServicios.TipoDePago.TipoPagoID = servicios.tipoPagoID;
                //            ordenesServicios.TipoDePago.DetallePago = servicios.detallePago;
                //            ordenData.OrdenServicio = ordenesServicios;
                //            ordenData.OrdenServicioToUDT();
                //            ordenData.Insert();
                //        }
                        
                //    }
                //    else
                //    {
                //        ordenesServicios.alertas = new AlertaMantenimiento();
                //        ordenesServicios.Costo = servicios.costo;
                //        ordenesServicios.Impuesto = servicios.impuesto;
                //        if (servicios.tipoMantenibleID == 1)
                //        {
                //            ordenesServicios.alertas.Mantenible = new Vehiculo();
                //            ordenesServicios.alertas.Mantenible.MantenibleID = servicios.vehiculoid;
                //            ordenesServicios.alertas.servicio.ServicioID = servicios.servicioid;
                //            ordenesServicios.TipoDePago.TipoPagoID = servicios.tipoPagoID;
                //            ordenesServicios.TipoDePago.DetallePago = servicios.detallePago;
                //            ordenData.OrdenServicio = ordenesServicios;
                //            ordenData.OrdenServicioToUDT();
                //            ordenData.Insert();
                //        }
                //    }
                //}
                OrdenServicio ordenesServicios = new OrdenServicio();
                //foreach (OrdenesDto os in dto.OrdenesDto)
                //{
                //    ordenesServicios = os.OrdenServicio;
                //    //ordenesServicios.alertas.Mantenible = this.GetMantenible(os.TipoMantenibleID);
                //    //ordenesServicios.alertas.Mantenible.MantenibleID = os.MantenibleID;
                //    if (ordenesServicios.alertas.Mantenible != null)
                //    {
                //        if (ordenesServicios.alertas.Mantenible.GetType() != typeof(Caja))
                //        {
                //            ordenData.OrdenServicio = ordenesServicios;
                //            ordenData.OrdenServicioToUDT();
                //            ordenData.Insert();
                //        }
                //    }
                //}
                //if (ordenesServicios.alertas.Mantenible.GetType() != typeof(Caja))
                //{
                //    this.UpdateParametersWithAlertSAP(ordenesServicios.alertas.Mantenible.MantenibleID, dto.listServicioCosto);
                //    return true;
                //}
                return false;

                #region Insertando Ordenes de Servicio
                
                //if (od.alertas != null)
                //{
                //    if (od.alertas.servicio != null)
                //    {
                //        serviciocosto svcosto = new serviciocosto();
                //        svcosto.servicio = od.alertas.servicio.Nombre;
                //        svcosto.servicioid = od.alertas.servicio.ServicioID;
                //        svcosto.costo = od.Costo;
                //        svcosto.impuesto = od.Impuesto;
                //        svcosto.tipoMantenibleID = this.GetTipoMantenibleID(od.alertas.Mantenible.GetType());
                //        svcosto.vehiculoid = od.alertas.Mantenible.MantenibleID;
                //        svcosto.tipoPagoID = od.TipoDePago.TipoPagoID;
                //        svcosto.detallePago = od.TipoDePago.DetallePago;
                //        if (listServicios == null)
                //        {
                //            listServicios = new List<serviciocosto>();
                //            listServicios.Add(svcosto);
                //        }
                //        else
                //            listServicios.Insert(0, svcosto);
                //    }
                //}

                //Kanan.Mantenimiento.BO2.OrdenServicio ordServicio = od;
                //foreach (serviciocosto svc in listServicios)
                //{
                //    if (ordServicio.alertas != null)
                //    {
                //        ordServicio.Costo = svc.costo;
                //        ordServicio.Impuesto = svc.impuesto;
                //        if (svc.tipoMantenibleID == 1)
                //        {
                //            ordServicio.alertas.Mantenible = null;
                //            ordServicio.alertas.Mantenible = new Vehiculo();
                //            ordServicio.alertas.Mantenible.MantenibleID = svc.vehiculoid;
                //            ordServicio.TipoDePago.TipoPagoID = svc.tipoPagoID;
                //            ordServicio.TipoDePago.DetallePago = svc.detallePago;
                //        }
                //        ordenData.OrdenServicio = ordServicio;
                //        ordenData.OrdenServicioToUDT();
                //        ordenData.Insert();
                //    }
                //    else
                //    {
                //        ordServicio.alertas = new AlertaMantenimiento();
                //        ordServicio.Costo = svc.costo;
                //        ordServicio.Impuesto = svc.impuesto;
                //        if (svc.tipoMantenibleID == 1)
                //        {
                //            ordServicio.alertas.Mantenible = null;
                //            ordServicio.alertas.Mantenible = new Vehiculo();
                //            ordServicio.alertas.Mantenible.MantenibleID = svc.vehiculoid;
                //            ordServicio.TipoDePago.TipoPagoID = svc.tipoPagoID;
                //            ordServicio.TipoDePago.DetallePago = svc.detallePago;
                //        }
                //        ordenData.OrdenServicio = ordServicio;
                //        ordenData.OrdenServicioToUDT();
                //        ordenData.Insert();
                //    }
                //}
                #endregion

                
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally { CloseConnection(); }
        }

        private void UpdateParametersWithAlertSAP(int? mantenibleID, List<serviciocosto> listaServicioCosto)
        {
            try
            {
                ParametroMantenimientoHelperData helpData = new ParametroMantenimientoHelperData(this.global.oCompany);
                ParametroMantenimientoData pmData = new ParametroMantenimientoData(this.global.oCompany);
                AlertaData alertaData = new AlertaData(this.global.oCompany);
                MotorMantenimientoData motorData = new MotorMantenimientoData(this.global.oCompany);
                foreach (serviciocosto svc in listaServicioCosto)
                {
                    ParametroMantenimiento parametro = new ParametroMantenimiento();
                    ParametroMantenimiento nuevoParametro = new ParametroMantenimiento();
                    SAPbobsCOM.Recordset rs =
                        motorData.ConsultarViewAlertaServicio(mantenibleID, svc.servicioid, svc.tipoMantenibleID);
                    if (motorData.HasAlertas(rs))
                    {
                        double? Distancia = 0;
                        if (rs.Fields.Item("parametroMantenimientoID").Value != null && rs.Fields.Item("parametroMantenimientoID").Value > 0)
                            parametro.ParametroMantenimientoID = rs.Fields.Item("parametroMantenimientoID").Value;
                        if (rs.Fields.Item("DistanciaRecorrida").Value != null && rs.Fields.Item("DistanciaRecorrida").Value > 0)
                            Distancia = (double)rs.Fields.Item("DistanciaRecorrida").Value;
                        rs = pmData.Consultar(parametro);
                        if (pmData.HasParametroMantenimiento(rs))
                        {
                            parametro = pmData.ConsultarCompleto(parametro.ParametroMantenimientoID);
                            nuevoParametro.ParametroMantenimientoID = parametro.ParametroMantenimientoID;
                            nuevoParametro.ParametroServicio = parametro.ParametroServicio;
                            nuevoParametro.ProximoServicio = parametro.ProximoServicio;
                            nuevoParametro.UltimaAlertaEnviada = parametro.UltimaAlertaEnviada;
                            nuevoParametro.UltimoServicio = parametro.UltimoServicio;
                            nuevoParametro.ParametroServicio.Valor = parametro.ParametroServicio.Valor;
                            nuevoParametro.ParametroServicio.Mantenible = parametro.ParametroServicio.Mantenible;
                            nuevoParametro.ParametroServicio.Valor = parametro.ParametroServicio.Valor;
                            nuevoParametro.ParametroServicio.Servicio = parametro.ParametroServicio.Servicio;
                            nuevoParametro.ParametroServicio.Mensaje = parametro.ParametroServicio.Mensaje;
                            nuevoParametro.Mantenible = parametro.Mantenible;
                            nuevoParametro.Mantenible.MantenibleID = parametro.Mantenible.MantenibleID;

                            /// Se realiza el calculo del nuevo parametro de mantenimiento
                            nuevoParametro.UltimoServicio = Distancia;
                            nuevoParametro.ProximoServicio = nuevoParametro.ParametroServicio.Valor + nuevoParametro.UltimoServicio;
                            nuevoParametro.AlertaProximoServicio = nuevoParametro.ProximoServicio - nuevoParametro.ParametroServicio.Alerta;
                            /// Se calcula el parametro por tiempo
                            int intervaloTiempo = 0;
                            int alertaTiempoTemp = 0;
                            if (parametro.ProximoServicioTiempo != null && parametro.UltimoServicioTiempo != null)
                            {
                                TimeSpan res = (TimeSpan)((DateTime)parametro.ProximoServicioTiempo - (DateTime)parametro.UltimoServicioTiempo);
                                intervaloTiempo = res.Days;
                                /// valid type of parameter for calculate de value of range alert for time
                                if (nuevoParametro.ParametroServicio.Mantenible.GetType() == typeof(Vehiculo))
                                {

                                    if (nuevoParametro.ParametroServicio.AlertaTiempo != null && intervaloTiempo > 0)
                                    {
                                        alertaTiempoTemp = (int)nuevoParametro.ParametroServicio.AlertaTiempo;
                                        //intervaloTiempo = (int)nuevoParametro.ParametroServicio.ValorTiempo;
                                        nuevoParametro.ParametroServicio.ValorTiempo = intervaloTiempo;
                                        nuevoParametro.ParametroServicio.AlertaTiempo = alertaTiempoTemp;
                                    }

                                }
                                else
                                {

                                    if (nuevoParametro.ParametroServicio.AlertaTiempo != null && intervaloTiempo > 0)
                                    {
                                        alertaTiempoTemp = (int)nuevoParametro.ParametroServicio.AlertaTiempo;
                                        //intervaloTiempo = (int)nuevoParametro.ParametroServicio.ValorTiempo;
                                    }

                                }
                                if (intervaloTiempo > 0 && alertaTiempoTemp > 0)
                                {
                                    /// Take the utc now for get date and replace the last service time
                                    DateTime lastService = DateTime.Now;
                                    nuevoParametro.UltimoServicioTiempo = lastService;
                                    nuevoParametro.ProximoServicioTiempo = nuevoParametro.UltimoServicioTiempo.Value.AddDays(intervaloTiempo);
                                    nuevoParametro.AlertaProximoServicioTiempo = nuevoParametro.ProximoServicioTiempo.Value.AddDays(-alertaTiempoTemp);
                                    ///Se actualiza el parametro de mantenimiento
                                }
                            }

                            helpData.oParametroMantenimiento = nuevoParametro;
                            helpData.ActualizarCompleto();
                            rs = motorData.ConsultarAlertasActivas(mantenibleID, svc.servicioid, null, true);
                            if (motorData.HasAlertas(rs))
                            {
                                int rows = rs.RecordCount;
                                rs.MoveFirst();
                                for (int i = 0; i < rows; i++)
                                {
                                    AlertaMantenimiento nAlerta = motorData.RecordSetToAlertaMantenimiento(rs);
                                    if (alertaData.SetUDTPMantenimientoID(nAlerta.AlertaMantenimientoID))
                                    {
                                        alertaData.Desactivar();
                                    }
                                    rs.MoveNext();
                                }
                            }

                        }//Fin if(pmData.HasParametroMantenimiento(rs))

                    }//Fin if(motorData.HasAlertas(rs))

                }//Fin foreach(serviciocosto svc in listaServicioCosto)
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region Ordenes De Servicio Sin Alerta Generada
        [HttpPost]// api/Servicios/InsertOrdenServicioSinAlerta
        public bool InsertOrdenServicioSinAlerta(OrdenServicioDto dto)
        {
            try
            {
                this.ValidateOrden(dto);
                //OrdenServicio od = new OrdenServicio();
                //od = dto.ordenServicio;
                //od.alertas.Mantenible = this.GetMantenible(dto.TipoMantenibleID);
                //od.alertas.Mantenible.MantenibleID = dto.MantenibleID;
                this.SetConnection();
                OrdenServicioData ordenData = new OrdenServicioData(this.global.oCompany);
                foreach (OrdenesDto os in dto.OrdenesDto)
                {
                    //os.OrdenServicio.alertas.Mantenible = this.GetMantenible(os.TipoMantenibleID);
                    //os.OrdenServicio.alertas.Mantenible.MantenibleID = os.MantenibleID;
                    //if(os.OrdenServicio.alertas.Mantenible != null)
                    //{
                    //    if(os.OrdenServicio.alertas.Mantenible.GetType() != typeof(Caja))
                    //    {
                    //        ordenData.OrdenServicio = os.OrdenServicio;
                    //        ordenData.OrdenServicioToUDT();
                    //        ordenData.Insert();
                    //        this.UpdateParametersWithOutAlertSAP(os.OrdenServicio);
                    //    }
                    //}
                    
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally { CloseConnection(); }
        }

        private void UpdateParametersWithOutAlertSAP(OrdenServicio orden)
        {
            try
            {
                ParametroMantenimientoHelperData helpData = new ParametroMantenimientoHelperData(this.global.oCompany);
                ParametroMantenimientoData pmData = new ParametroMantenimientoData(this.global.oCompany);
                ParametroServicioData psData = new ParametroServicioData(this.global.oCompany);
                DesplazamientoData dsData = new DesplazamientoData(this.global.oCompany);
                Desplazamiento desplazamiento = new Desplazamiento();
                ParametroMantenimiento parametro = new ParametroMantenimiento();
                ParametroMantenimiento nuevoParametro = new ParametroMantenimiento();
                parametro.ParametroServicio = new ParametroServicio();
                parametro.Mantenible = new Vehiculo();

                //parametro.ParametroServicio.Servicio = orden.alertas.servicio;
                //parametro.Mantenible = orden.alertas.Mantenible;
                SAPbobsCOM.Recordset rs = pmData.Consultar(parametro);
                if (pmData.HasParametroMantenimiento(rs))
                {
                    parametro = pmData.LastRecordSetToParametroMantenimiento(rs);
                    parametro = pmData.ConsultarCompleto(parametro.ParametroMantenimientoID);
                    nuevoParametro = (ParametroMantenimiento)parametro.Clone();
                    //if (dsData.SetUDTByVehiculoID(orden.alertas.vehiculo.VehiculoID))
                    //{
                    //    dsData.UDTToDesplazamiento();
                    //    desplazamiento = dsData.Desplazamiento;
                    //}
                    
                    nuevoParametro.UltimoServicio = desplazamiento.DistanciaRecorrida;
                    nuevoParametro.ProximoServicio = nuevoParametro.ParametroServicio.Valor + nuevoParametro.UltimoServicio;
                    nuevoParametro.AlertaProximoServicio = nuevoParametro.ProximoServicio - nuevoParametro.ParametroServicio.Alerta;
                    /// Se calcula el parametro por tiempo
                    int intervaloTiempo = 0;
                    int alertaTiempoTemp = 0;
                    if (parametro.ProximoServicioTiempo != null && parametro.UltimoServicioTiempo != null)
                    {
                        TimeSpan res = (TimeSpan)((DateTime)parametro.ProximoServicioTiempo - (DateTime)parametro.UltimoServicioTiempo);
                        intervaloTiempo = res.Days;
                        /// valid type of parameter for calculate de value of range alert for time
                        if (nuevoParametro.ParametroServicio.Mantenible.GetType() == typeof(Vehiculo))
                        {

                            if (nuevoParametro.ParametroServicio.AlertaTiempo != null && intervaloTiempo > 0)
                            {
                                alertaTiempoTemp = (int)nuevoParametro.ParametroServicio.AlertaTiempo;
                                nuevoParametro.ParametroServicio.ValorTiempo = intervaloTiempo;
                                nuevoParametro.ParametroServicio.AlertaTiempo = alertaTiempoTemp;
                            }

                        }
                        else
                        {

                            if (nuevoParametro.ParametroServicio.AlertaTiempo != null && intervaloTiempo > 0)
                            {
                                alertaTiempoTemp = (int)nuevoParametro.ParametroServicio.AlertaTiempo;
                            }

                        }
                        if (intervaloTiempo > 0 && alertaTiempoTemp > 0)
                        {
                            /// Take the utc now for get date and replace the last service time
                            DateTime lastService = DateTime.Now;
                            nuevoParametro.UltimoServicioTiempo = lastService;
                            nuevoParametro.ProximoServicioTiempo = nuevoParametro.UltimoServicioTiempo.Value.AddDays(intervaloTiempo);
                            nuevoParametro.AlertaProximoServicioTiempo = nuevoParametro.ProximoServicioTiempo.Value.AddDays(-alertaTiempoTemp);
                            ///Se actualiza el parametro de mantenimiento
                        }
                    }
                    helpData.oParametroMantenimiento = nuevoParametro;
                    helpData.ActualizarCompleto();
                }

            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        #endregion

        #endregion

    }
}