﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Kanan.Costos.BO2;
using KananSAP.Costos.Data;
using KananSAP.WebApiServices.Models;
using SAPbobsCOM;

namespace KananSAP.WebApiServices.Controllers
{
    public class CostosController : ApiController
    {
        #region Atributos
        private globals_Renamed global = new globals_Renamed();
        private SAPbobsCOM.Recordset oRecordSet;
        #endregion

        #region Conexion
        private void SetConnection()
        {
            global.InitializeCompany();
            //GetCompanyList();

            global.oCompany.DbServerType = (BoDataServerTypes)(Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("DBType")));

            global.oCompany.UseTrusted = false;
            global.oCompany.DbUserName = System.Configuration.ConfigurationManager.AppSettings.Get("ServerUser");
            global.oCompany.DbPassword = System.Configuration.ConfigurationManager.AppSettings.Get("ServerPass");
            global.oCompany.CompanyDB = System.Configuration.ConfigurationManager.AppSettings.Get("ConmpanyDataBase");
            global.oCompany.UserName = System.Configuration.ConfigurationManager.AppSettings.Get("SAPUser");
            global.oCompany.Password = System.Configuration.ConfigurationManager.AppSettings.Get("SAPPass");
            // Connecting to a company DB
            global.lRetCode = global.oCompany.Connect();

            if (global.lRetCode != 0)
            {
                int temp_int = global.lErrCode;
                string temp_string = global.sErrMsg;
                global.oCompany.GetLastError(out temp_int, out temp_string);
                throw new Exception(global.sErrMsg + "Error: " + temp_int + " - " + temp_string);
            }
        }

        private void CloseConnection()
        {
            if (global.oCompany.Connected)
            {
                global.oCompany.Disconnect();
            }
        }

        #endregion

        #region Metodos
        #region Proveedores
        // GET api/Costos/GetProveedor
        [HttpGet]
        public IHttpActionResult GetProveedor(int id)
        {
            try
            {
                SetConnection();
                ProveedorSAP sapproveedor = new ProveedorSAP(global.oCompany);
                if (!sapproveedor.SetUDTByProveedorID(id))
                {
                    throw new Exception("Identificador Invalido");
                }
                sapproveedor.SAPToProveedor();
                var proveedor = sapproveedor.proveedor;
                return Ok(proveedor);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally
            {
                CloseConnection();
            }
        }

        // Post api/Costos/Insert
        [HttpPost]
        public IHttpActionResult Insert(Proveedor proveedor)
        {
            try
            {
                SetConnection();
                ProveedorSAP sapproveedor = new ProveedorSAP(global.oCompany);
                sapproveedor.proveedor = proveedor;
                sapproveedor.ProveedorToSAP();
                sapproveedor.InsertComplete();
                sapproveedor.SetUDTByProveedorID((int)proveedor.ProveedorID);
                sapproveedor.UDTToProveedor();
                return Ok(sapproveedor.proveedor);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally
            {
                CloseConnection();
            }
        }

        // Post api/Costos/Update
        [HttpPost]
        public IHttpActionResult Update(Proveedor Proveedor)
        {
            try
            {
                SetConnection();
                ProveedorSAP sapproveedor = new ProveedorSAP(global.oCompany);
                if (Proveedor.ProveedorID != null) sapproveedor.SetUDTByProveedorID((int)Proveedor.ProveedorID);
                else throw new Exception("Dato Invalido");
                sapproveedor.oBpartner.GetByKey(sapproveedor.oUDT.Code);
                sapproveedor.ItemCode = sapproveedor.oUDT.Code;
                sapproveedor.proveedor = Proveedor;
                sapproveedor.ProveedorToSAP();
                sapproveedor.UpdateComplete();
                sapproveedor.SAPToProveedor();
                return Ok(sapproveedor.proveedor);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally
            {
                CloseConnection();
            }
        }

        // Post api/Costos/Delete
        [HttpGet]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                SetConnection();
                ProveedorSAP sapproveedor = new ProveedorSAP(global.oCompany);
                sapproveedor.SetUDTByProveedorID(id);
                sapproveedor.SAPToProveedor();
                sapproveedor.RemoveComplete();
                return Ok(sapproveedor.proveedor);
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message, ex));
            }
            finally
            {
                CloseConnection();
            }
        }

        #endregion
        #endregion
    }
}
