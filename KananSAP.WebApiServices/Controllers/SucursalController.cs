﻿using KananSAP.Helpers;
using Kanan.Operaciones.BO2;
using KananSAP.WebApiServices.Models;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using KananSAP.Operaciones.Data;

namespace KananSAP.WebApiServices.Controllers
{
    public class SucursalController : ApiController
    {
        #region Atributos
        private globals_Renamed global = new globals_Renamed();
        private SAPbobsCOM.Recordset oRecordSet;
        #endregion

        #region Conexion
        private void SetConnection()
        {
            global.InitializeCompany();
            //GetCompanyList();

            global.oCompany.DbServerType = (BoDataServerTypes)(Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("DBType")));

            global.oCompany.UseTrusted = false;
            global.oCompany.DbUserName = System.Configuration.ConfigurationManager.AppSettings.Get("ServerUser");
            global.oCompany.DbPassword = System.Configuration.ConfigurationManager.AppSettings.Get("ServerPass");
            global.oCompany.CompanyDB = System.Configuration.ConfigurationManager.AppSettings.Get("ConmpanyDataBase");
            global.oCompany.UserName = System.Configuration.ConfigurationManager.AppSettings.Get("SAPUser");
            global.oCompany.Password = System.Configuration.ConfigurationManager.AppSettings.Get("SAPPass");
            // Connecting to a company DB
            global.lRetCode = global.oCompany.Connect();

            if (global.lRetCode != 0)
            {
                int temp_int = global.lErrCode;
                string temp_string = global.sErrMsg;
                global.oCompany.GetLastError(out temp_int, out temp_string);
                throw new Exception(global.sErrMsg + "Error: " + temp_int + " - " + temp_string);
            }
        }

        private void CloseConnection()
        {
            if (global.oCompany.Connected)
            {
                global.oCompany.Disconnect();
            }
        }

        #endregion

        private string ValidarSucursal(Sucursal sc)
        {
            try
            {
                string err = string.Empty, campos = string.Empty;
                if (sc == null)
                    campos += ", Sucursal";
                if (string.IsNullOrEmpty(sc.Direccion))
                    campos += ", Nombre";
                if (sc.Base == null)
                    campos += ", Empresa";
                if (sc.Base.EmpresaID == null)
                    campos += ", EmpresaID";
                if(!string.IsNullOrEmpty(campos))
                {
                    if (campos.StartsWith(","))
                        campos = campos.Substring(1);
                    err = "Los siguientes datos son obligatorios: " + campos;
                }
                return err;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        [HttpPost]//api/Sucursal/InsertaCompleto
        public bool InsertaCompleto(Sucursal sucursal)
        {
            try
            {
                string err = this.ValidarSucursal(sucursal);
                if (!string.IsNullOrEmpty(err))
                    throw new Exception(err);
                this.SetConnection();
                SucursalHelperData helpData = new SucursalHelperData(this.global.oCompany);
                helpData.InsertarCompleto(sucursal);
                return true;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
            finally { this.CloseConnection(); }
        }

        [HttpPost]//api/Sucursal/Actualizar
        public bool Actualizar(Sucursal sucursal)
        {
            try
            {
                string err = this.ValidarSucursal(sucursal);
                if (!string.IsNullOrEmpty(err))
                    throw new Exception(err);
                if(sucursal.SucursalID == null)
                    throw new Exception("Proporcione una SucursalID válida");
                this.SetConnection();
                SucursalData scData = new SucursalData(this.global.oCompany);
                scData.ItemCode = sucursal.SucursalID.ToString();
                if(scData.ExistUDT())
                {
                    scData.Sucursal = sucursal;
                    scData.SucursalToUDT();
                    scData.Actualizar();
                    return true;
                }
                return false;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
            finally { this.CloseConnection(); }
        }

        [HttpPost]//api/Sucursal/ActualizarCompleto
        public bool ActualizarCompleto(Sucursal sucursal)
        {
            try
            {
                string err = this.ValidarSucursal(sucursal);
                if (!string.IsNullOrEmpty(err))
                    throw new Exception(err);
                if (sucursal.SucursalID == null)
                    throw new Exception("Proporcione una SucursalID válida");
                this.SetConnection();
                SucursalHelperData helpData = new SucursalHelperData(this.global.oCompany);
                helpData.ActualizarCompleto(sucursal);
                return true;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
            finally { this.CloseConnection(); }
        }

    }
}
