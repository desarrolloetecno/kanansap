﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SAPbobsCOM;

namespace KananSAP.WebApiServices.Models
{
    class globals_Renamed
    {

        public void InitializeCompany()
        {

            // Initialize the Company Object.
            // Create a new company object
            oCompany = new SAPbobsCOM.Company();

            // Set the mandatory properties for the connection to the database.
            // here I bring only 2 of the 5 mandatory fields.
            // To use a remote Db Server enter his name instead of the string "(local)"
            // This string is used to work on a DB installed on your local machine
            // the other mandatory fields are CompanyDB, UserName and Password
            // I am setting those fields in the ChooseCompany Form

            //oCompany.Server = System.Configuration.ConfigurationManager.AppSettings.Get("Server");
            //oCompany.language = SAPbobsCOM.BoSuppLangs.ln_Spanish_La;

            switch (System.Configuration.ConfigurationManager.AppSettings.Get("Language"))
            {
                case "English":
                    oCompany.language = BoSuppLangs.ln_English;
                    break;
                case "LatinAmerica":
                    oCompany.language = BoSuppLangs.ln_Spanish_La;
                    break;
                case "Spain":
                    oCompany.language = BoSuppLangs.ln_Spanish;
                    break;
                default:
                    oCompany.language = BoSuppLangs.ln_English;
                    break;
            }

            // Use Windows authentication for database server.
            // True for NT server authentication,
            // False for database server authentication.
            string b = System.Configuration.ConfigurationManager.AppSettings.Get("UseTrusted");
            if (!string.IsNullOrEmpty(b) && b.Trim().ToUpper() == "TRUE")
                oCompany.UseTrusted = true;
            else
            {
                oCompany.UseTrusted = false;
            }
        }

        public SAPbobsCOM.Company oCompany;

        public string sErrMsg = null;
        public int lErrCode = 0;
        public int lRetCode;

    }
}