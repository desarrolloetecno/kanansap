﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project1;

namespace KananaSAP.DI
{
    public class Main
    {
        #region Atributos
       // private SAPbouiCOM.Application SBO_Application;
        private SAPbobsCOM.Company Company;
        #endregion

        #region Constructor
        
        public Main()
        {
            globals_Renamed.InitializeCompany();
            ChooseCompany.DefInstance.ShowDialog();
        }
        #endregion

        #region Metodos

        private void SetApplication()
        {

            // *******************************************************************
            // Use an SboGuiApi object to establish the connection
            // with the application and return an initialized appliction object
            // *******************************************************************

           // SAPbouiCOM.SboGuiApi SboGuiApi = null;
            string sConnectionString = null;

          //  SboGuiApi = new SAPbouiCOM.SboGuiApi();

            // by following the steps specified above, the following
            // statment should be suficient for either development or run mode

            sConnectionString = System.Convert.ToString(Environment.GetCommandLineArgs().GetValue(1));

            // connect to a running SBO Application

           // SboGuiApi.Connect(sConnectionString);

            // get an initialized application object

           // SBO_Application = SboGuiApi.GetApplication(-1);
        }

        private void SetCompany()
        {
            Company = new SAPbobsCOM.Company();

           // Company =(SAPbobsCOM.Company) SBO_Application.Company.GetDICompany();
        }

        #endregion
    }
}
