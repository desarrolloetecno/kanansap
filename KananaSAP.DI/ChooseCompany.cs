#define Win32
using KananaSAP.DI.UDT;
using Microsoft.VisualBasic;
using System.Data;
using System;
using System.IO;
using System.Collections;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.ComponentModel;



//  SAP MANAGE DI API 2007 SDK Sample
//****************************************************************************
//
//  File:      ChooseCompany.vb
//
//  Copyright (c) SAP MANAGE
//
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
//****************************************************************************
using SAPbobsCOM;
using System.Xml.Linq;
using System.Xml;
using System.Reflection;

namespace Project1
{
    internal class ChooseCompany : System.Windows.Forms.Form
    {
        #region Initialize
        //MainKananFleet main = new MainKananFleet();
        OpenFileDialog openFileDialog1 = new OpenFileDialog();
        StreamReader file;
        ArrayList DatosDeTabla = new ArrayList();
        ArrayList OtrosDatos = new ArrayList();
        string nLine = "";
        private OpenFileDialog openFileDialog2;
        string name;
        String DBType;
        String UserDB;
        String PassDB;
        String CompanySAP;
        String UserSAP;
        String PassSAP;
        private TextBox txtServer;
        private Label label8;
        int NumSig = 0;

        private Boolean bCrearNuevoLogInstalacion;
        private String sNombreNuevoLog;
        private TextBox txtLicenseSever;
        private Label label9;
        private Label label10;
        private GroupBox groupBox3;
        private TextBox txtPasswordVinc;
        private Label label11;
        private TextBox txtCorrecoVinc;
        private TextBox txtUsuarioSAPVinc;
        private Label label12;
        private String sRutaAplicacion;
        #endregion Initialize

        #region "Windows Form Designer generated code "
        public ChooseCompany()
        {
            if (m_vb6FormDefInstance == null)
            {
                if (m_InitializingDefInstance)
                {
                    m_vb6FormDefInstance = this;
                }
                else
                {
                    try
                    {
                        //For the start-up form, the first instance created is the default instance.
                        if (System.Reflection.Assembly.GetExecutingAssembly().EntryPoint.DeclaringType == this.GetType())
                        {
                            m_vb6FormDefInstance = this;
                        }
                    }
                    catch
                    {
                    }
                }
            }
            //This call is required by the Windows Form Designer.
            InitializeComponent();
        }
        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool Disposing)
        {
            if (Disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(Disposing);
        }

        private System.ComponentModel.IContainer components;
        public System.Windows.Forms.ToolTip ToolTip1;
        public System.Windows.Forms.TextBox Text2;
        public System.Windows.Forms.TextBox Text1;
        public System.Windows.Forms.Label Label3;
        public System.Windows.Forms.Label Label2;
        public System.Windows.Forms.GroupBox Frame2;
        public System.Windows.Forms.ComboBox Combo1;
        public System.Windows.Forms.Label Label1;
        public System.Windows.Forms.GroupBox Frame1;
        public System.Windows.Forms.Button Command1;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        public System.Windows.Forms.ComboBox cmbDBType;
        public System.Windows.Forms.Label Label4;
        public System.Windows.Forms.GroupBox GroupBox1;
        public System.Windows.Forms.TextBox txtDBPass;
        public System.Windows.Forms.TextBox txtDBUser;
        public System.Windows.Forms.Label Label5;
        public System.Windows.Forms.Label Label6;
        public Button btnConf;
        private Button button1;
        private Button button2;
        private GroupBox groupBox2;
        private Label label7;
        private TextBox textBox1;
        internal System.Windows.Forms.Button cmdGetCompanyList;
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ToolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.Frame2 = new System.Windows.Forms.GroupBox();
            this.txtLicenseSever = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Text2 = new System.Windows.Forms.TextBox();
            this.Text1 = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Frame1 = new System.Windows.Forms.GroupBox();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDBPass = new System.Windows.Forms.TextBox();
            this.txtDBUser = new System.Windows.Forms.TextBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.cmdGetCompanyList = new System.Windows.Forms.Button();
            this.cmbDBType = new System.Windows.Forms.ComboBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.Combo1 = new System.Windows.Forms.ComboBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.Command1 = new System.Windows.Forms.Button();
            this.btnConf = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtUsuarioSAPVinc = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPasswordVinc = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtCorrecoVinc = new System.Windows.Forms.TextBox();
            this.Frame2.SuspendLayout();
            this.Frame1.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // Frame2
            // 
            this.Frame2.BackColor = System.Drawing.SystemColors.Control;
            this.Frame2.Controls.Add(this.txtLicenseSever);
            this.Frame2.Controls.Add(this.label9);
            this.Frame2.Controls.Add(this.Text2);
            this.Frame2.Controls.Add(this.Text1);
            this.Frame2.Controls.Add(this.Label3);
            this.Frame2.Controls.Add(this.Label2);
            this.Frame2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Frame2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Frame2.Location = new System.Drawing.Point(12, 216);
            this.Frame2.Name = "Frame2";
            this.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame2.Size = new System.Drawing.Size(249, 89);
            this.Frame2.TabIndex = 4;
            this.Frame2.TabStop = false;
            this.Frame2.Text = "SAP Login Info";
            this.Frame2.Enter += new System.EventHandler(this.Frame2_Enter);
            // 
            // txtLicenseSever
            // 
            this.txtLicenseSever.Location = new System.Drawing.Point(102, 13);
            this.txtLicenseSever.Name = "txtLicenseSever";
            this.txtLicenseSever.Size = new System.Drawing.Size(136, 20);
            this.txtLicenseSever.TabIndex = 10;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 14);
            this.label9.TabIndex = 9;
            this.label9.Text = "License Server";
            // 
            // Text2
            // 
            this.Text2.AcceptsReturn = true;
            this.Text2.BackColor = System.Drawing.SystemColors.Window;
            this.Text2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Text2.Enabled = false;
            this.Text2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Text2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Text2.Location = new System.Drawing.Point(102, 60);
            this.Text2.MaxLength = 0;
            this.Text2.Name = "Text2";
            this.Text2.PasswordChar = '*';
            this.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text2.Size = new System.Drawing.Size(136, 20);
            this.Text2.TabIndex = 8;
            this.Text2.TextChanged += new System.EventHandler(this.Text2_TextChanged);
            // 
            // Text1
            // 
            this.Text1.AcceptsReturn = true;
            this.Text1.BackColor = System.Drawing.SystemColors.Window;
            this.Text1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.Text1.Enabled = false;
            this.Text1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Text1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Text1.Location = new System.Drawing.Point(102, 37);
            this.Text1.MaxLength = 0;
            this.Text1.Name = "Text1";
            this.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text1.Size = new System.Drawing.Size(136, 20);
            this.Text1.TabIndex = 6;
            // 
            // Label3
            // 
            this.Label3.BackColor = System.Drawing.SystemColors.Control;
            this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label3.Enabled = false;
            this.Label3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label3.Location = new System.Drawing.Point(11, 63);
            this.Label3.Name = "Label3";
            this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label3.Size = new System.Drawing.Size(88, 17);
            this.Label3.TabIndex = 7;
            this.Label3.Text = "Password:";
            // 
            // Label2
            // 
            this.Label2.BackColor = System.Drawing.SystemColors.Control;
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Enabled = false;
            this.Label2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label2.Location = new System.Drawing.Point(11, 39);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(72, 17);
            this.Label2.TabIndex = 5;
            this.Label2.Text = "User Name:";
            // 
            // Frame1
            // 
            this.Frame1.BackColor = System.Drawing.SystemColors.Control;
            this.Frame1.Controls.Add(this.GroupBox1);
            this.Frame1.Controls.Add(this.cmdGetCompanyList);
            this.Frame1.Controls.Add(this.cmbDBType);
            this.Frame1.Controls.Add(this.Label4);
            this.Frame1.Controls.Add(this.Combo1);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Frame1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Frame1.Location = new System.Drawing.Point(12, 0);
            this.Frame1.Name = "Frame1";
            this.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Frame1.Size = new System.Drawing.Size(249, 208);
            this.Frame1.TabIndex = 1;
            this.Frame1.TabStop = false;
            // 
            // GroupBox1
            // 
            this.GroupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.GroupBox1.Controls.Add(this.txtServer);
            this.GroupBox1.Controls.Add(this.label8);
            this.GroupBox1.Controls.Add(this.txtDBPass);
            this.GroupBox1.Controls.Add(this.txtDBUser);
            this.GroupBox1.Controls.Add(this.Label5);
            this.GroupBox1.Controls.Add(this.Label6);
            this.GroupBox1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GroupBox1.Location = new System.Drawing.Point(8, 48);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.GroupBox1.Size = new System.Drawing.Size(224, 93);
            this.GroupBox1.TabIndex = 11;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "DB Login Info";
            // 
            // txtServer
            // 
            this.txtServer.Location = new System.Drawing.Point(83, 66);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(135, 20);
            this.txtServer.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 14);
            this.label8.TabIndex = 9;
            this.label8.Text = "Servidor:";
            // 
            // txtDBPass
            // 
            this.txtDBPass.AcceptsReturn = true;
            this.txtDBPass.BackColor = System.Drawing.SystemColors.Window;
            this.txtDBPass.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtDBPass.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDBPass.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtDBPass.Location = new System.Drawing.Point(83, 40);
            this.txtDBPass.MaxLength = 0;
            this.txtDBPass.Name = "txtDBPass";
            this.txtDBPass.PasswordChar = '*';
            this.txtDBPass.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtDBPass.Size = new System.Drawing.Size(135, 20);
            this.txtDBPass.TabIndex = 8;
            // 
            // txtDBUser
            // 
            this.txtDBUser.AcceptsReturn = true;
            this.txtDBUser.BackColor = System.Drawing.SystemColors.Window;
            this.txtDBUser.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtDBUser.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDBUser.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtDBUser.Location = new System.Drawing.Point(83, 16);
            this.txtDBUser.MaxLength = 0;
            this.txtDBUser.Name = "txtDBUser";
            this.txtDBUser.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtDBUser.Size = new System.Drawing.Size(135, 20);
            this.txtDBUser.TabIndex = 6;
            // 
            // Label5
            // 
            this.Label5.BackColor = System.Drawing.SystemColors.Control;
            this.Label5.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label5.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label5.Location = new System.Drawing.Point(8, 44);
            this.Label5.Name = "Label5";
            this.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label5.Size = new System.Drawing.Size(89, 17);
            this.Label5.TabIndex = 7;
            this.Label5.Text = "Password:";
            // 
            // Label6
            // 
            this.Label6.BackColor = System.Drawing.SystemColors.Control;
            this.Label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label6.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label6.Location = new System.Drawing.Point(8, 21);
            this.Label6.Name = "Label6";
            this.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label6.Size = new System.Drawing.Size(73, 17);
            this.Label6.TabIndex = 5;
            this.Label6.Text = "User Name:";
            // 
            // cmdGetCompanyList
            // 
            this.cmdGetCompanyList.Location = new System.Drawing.Point(116, 147);
            this.cmdGetCompanyList.Name = "cmdGetCompanyList";
            this.cmdGetCompanyList.Size = new System.Drawing.Size(112, 23);
            this.cmdGetCompanyList.TabIndex = 6;
            this.cmdGetCompanyList.Text = "Get Company List";
            this.cmdGetCompanyList.Click += new System.EventHandler(this.cmdGetCompanyList_Click);
            // 
            // cmbDBType
            // 
            this.cmbDBType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbDBType.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmbDBType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDBType.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDBType.ForeColor = System.Drawing.SystemColors.WindowText;
            this.cmbDBType.Items.AddRange(new object[] {
            "dst_MSSQL2005",
            "dst_MSSQL2008",
            "dst_MSSQL2012",
            "dst_MSSQL2014",
            "dst_MSSQL2016",
            "dst_MSSQL2017",
            "dst_HANADB"});
            this.cmbDBType.Location = new System.Drawing.Point(91, 20);
            this.cmbDBType.Name = "cmbDBType";
            this.cmbDBType.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmbDBType.Size = new System.Drawing.Size(137, 22);
            this.cmbDBType.TabIndex = 4;
            // 
            // Label4
            // 
            this.Label4.BackColor = System.Drawing.SystemColors.Control;
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label4.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label4.Location = new System.Drawing.Point(8, 16);
            this.Label4.Name = "Label4";
            this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label4.Size = new System.Drawing.Size(89, 17);
            this.Label4.TabIndex = 5;
            this.Label4.Text = "DB Type";
            // 
            // Combo1
            // 
            this.Combo1.BackColor = System.Drawing.SystemColors.Window;
            this.Combo1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Combo1.Enabled = false;
            this.Combo1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Combo1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Combo1.Location = new System.Drawing.Point(91, 176);
            this.Combo1.Name = "Combo1";
            this.Combo1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Combo1.Size = new System.Drawing.Size(137, 22);
            this.Combo1.TabIndex = 2;
            // 
            // Label1
            // 
            this.Label1.BackColor = System.Drawing.SystemColors.Control;
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Enabled = false;
            this.Label1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label1.Location = new System.Drawing.Point(8, 176);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(89, 17);
            this.Label1.TabIndex = 3;
            this.Label1.Text = "Company DB:";
            // 
            // Command1
            // 
            this.Command1.BackColor = System.Drawing.SystemColors.Control;
            this.Command1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Command1.Enabled = false;
            this.Command1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Command1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Command1.Location = new System.Drawing.Point(10, 448);
            this.Command1.Name = "Command1";
            this.Command1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Command1.Size = new System.Drawing.Size(249, 34);
            this.Command1.TabIndex = 0;
            this.Command1.Text = "Connect";
            this.Command1.UseVisualStyleBackColor = false;
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // btnConf
            // 
            this.btnConf.BackColor = System.Drawing.SystemColors.Control;
            this.btnConf.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnConf.Enabled = false;
            this.btnConf.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConf.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnConf.Location = new System.Drawing.Point(12, 487);
            this.btnConf.Name = "btnConf";
            this.btnConf.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnConf.Size = new System.Drawing.Size(249, 36);
            this.btnConf.TabIndex = 5;
            this.btnConf.Text = "KananFleet - SAP\r\nConfigurar DB";
            this.btnConf.UseVisualStyleBackColor = false;
            this.btnConf.Click += new System.EventHandler(this.btnConf_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(27, 45);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(78, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Leer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(146, 45);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(78, 23);
            this.button2.TabIndex = 7;
            this.button2.Text = "Guardar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Location = new System.Drawing.Point(12, 532);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(252, 78);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Importar";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 14);
            this.label7.TabIndex = 9;
            this.label7.Text = "Ruta de archivo";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(97, 19);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(149, 20);
            this.textBox1.TabIndex = 8;
            this.textBox1.Text = "Nombre";
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.FileName = "openFileDialog2";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(11, 15);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 14);
            this.label10.TabIndex = 9;
            this.label10.Text = "Correo";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtUsuarioSAPVinc);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.txtPasswordVinc);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.txtCorrecoVinc);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Location = new System.Drawing.Point(12, 310);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(249, 138);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Vinculaci�n de empresa";
            // 
            // txtUsuarioSAPVinc
            // 
            this.txtUsuarioSAPVinc.Location = new System.Drawing.Point(13, 115);
            this.txtUsuarioSAPVinc.Name = "txtUsuarioSAPVinc";
            this.txtUsuarioSAPVinc.Size = new System.Drawing.Size(225, 20);
            this.txtUsuarioSAPVinc.TabIndex = 14;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 93);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 14);
            this.label12.TabIndex = 13;
            this.label12.Text = "Usuario SAP B1";
            // 
            // txtPasswordVinc
            // 
            this.txtPasswordVinc.Location = new System.Drawing.Point(17, 68);
            this.txtPasswordVinc.Name = "txtPasswordVinc";
            this.txtPasswordVinc.Size = new System.Drawing.Size(221, 20);
            this.txtPasswordVinc.TabIndex = 12;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 53);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 14);
            this.label11.TabIndex = 11;
            this.label11.Text = "Contrase�a";
            // 
            // txtCorrecoVinc
            // 
            this.txtCorrecoVinc.Location = new System.Drawing.Point(13, 32);
            this.txtCorrecoVinc.Name = "txtCorrecoVinc";
            this.txtCorrecoVinc.Size = new System.Drawing.Size(225, 20);
            this.txtCorrecoVinc.TabIndex = 10;
            // 
            // ChooseCompany
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(281, 749);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnConf);
            this.Controls.Add(this.Frame2);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.Command1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Location = new System.Drawing.Point(184, 250);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChooseCompany";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Choose Company DB";
            this.Load += new System.EventHandler(this.ChooseCompany_Load);
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            this.Frame1.ResumeLayout(false);
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        #region "Upgrade Support "
        private static ChooseCompany m_vb6FormDefInstance;
        private static bool m_InitializingDefInstance;
        public static ChooseCompany DefInstance
        {
            get
            {
                ChooseCompany returnValue;
                if (m_vb6FormDefInstance == null || m_vb6FormDefInstance.IsDisposed)
                {
                    m_InitializingDefInstance = true;
                    m_vb6FormDefInstance = new ChooseCompany();
                    m_InitializingDefInstance = false;
                }
                returnValue = m_vb6FormDefInstance;
                return returnValue;
            }
            set
            {
                m_vb6FormDefInstance = value;
            }
        }
        #endregion

        #region ChooseCompany

        //****************************************************************************
        // The Recordset is a non-business object.
        // it is an auxiliary object for querying the DB
        //****************************************************************************
        private SAPbobsCOM.Recordset oRecordSet;
        public SAPbobsCOM.ItemGroups oItem { get; set; }

        private void ChooseCompany_Load(System.Object eventSender, System.EventArgs eventArgs)
        {
            //cmbDBType.SelectedIndex = 5;
            globals_Renamed.InitializeCompany();
            //txtDBUser.Text = globals_Renamed.oCompany.Server;
            this.sRutaAplicacion = System.IO.Path.GetDirectoryName(
                    System.Reflection.Assembly.GetExecutingAssembly().Location);
        }

        private void Command1_Click(System.Object eventSender, System.EventArgs eventArgs)
        {

            // setting the rest of the mandatory properties
            globals_Renamed.oCompany.LicenseServer = txtLicenseSever.Text;
            globals_Renamed.oCompany.CompanyDB = Combo1.Text;
            globals_Renamed.oCompany.UserName = Text1.Text;
            globals_Renamed.oCompany.Password = Text2.Text;

            this.CompanySAP = Combo1.Text;
            this.UserSAP = Text1.Text;
            this.PassSAP = Text2.Text;

            //EscribeArchivo();

            // Connecting to a company DB
            globals_Renamed.lRetCode = globals_Renamed.oCompany.Connect();

            if (globals_Renamed.lRetCode != 0)
            {
                int temp_int = globals_Renamed.lErrCode;
                string temp_string = globals_Renamed.sErrMsg;
                globals_Renamed.oCompany.GetLastError(out temp_int, out temp_string);
                MessageBox.Show(globals_Renamed.sErrMsg + "Error: " + temp_int + " - " + temp_string);

            }
            else
            {
                MessageBox.Show("Connected to " + globals_Renamed.oCompany.CompanyName);
                this.Text = this.Text + ": Connected";

                // Disable controls
                Label1.Enabled = false;
                Label2.Enabled = false;
                Label3.Enabled = false;
                Combo1.Enabled = false;
                Text1.Enabled = false;
                Text2.Enabled = false;
                txtDBUser.Enabled = false;
                txtDBPass.Enabled = false;
                txtServer.Enabled = false;
                txtLicenseSever.Enabled = false;
                Command1.Enabled = false;
                cmbDBType.Enabled = false;
                cmdGetCompanyList.Enabled = false;
                btnConf.Enabled = true;
            }

        }

        private void cmdGetCompanyList_Click(System.Object sender, System.EventArgs e)
        {
            //			int i; // to be used as an index

            globals_Renamed.oCompany = new Company();
            globals_Renamed.oCompany.Server = txtServer.Text;
            globals_Renamed.oCompany.language = SAPbobsCOM.BoSuppLangs.ln_Spanish_La;
            globals_Renamed.oCompany.UseTrusted = false;            
            globals_Renamed.oCompany.DbUserName = txtDBUser.Text;
            globals_Renamed.oCompany.DbPassword = txtDBPass.Text;
            globals_Renamed.oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes)Enum.Parse(typeof(SAPbobsCOM.BoDataServerTypes), cmbDBType.SelectedItem.ToString(), true);


            this.UserDB = txtDBUser.Text;
            this.PassDB = txtDBPass.Text;
            
            try
            {
                oRecordSet = globals_Renamed.oCompany.GetCompanyList();


                int temp_int = globals_Renamed.lErrCode;
                string temp_string = globals_Renamed.sErrMsg;
                globals_Renamed.oCompany.GetLastError(out temp_int, out temp_string);

                if (globals_Renamed.lErrCode != 0)
                {
                    MessageBox.Show(globals_Renamed.sErrMsg);
                }
                else
                {
                    if (globals_Renamed.oCompany.Connected == false)
                    {


                        // Clear the company list
                        Combo1.Items.Clear();
                        // Go through the Recordset and extract the db name
                        while (!(oRecordSet.EoF == true))
                        {
                            // add the value of the first field of the Recordset
                            Combo1.Items.Add(oRecordSet.Fields.Item(0).Value);
                            // move the record pointer to the next row
                            oRecordSet.MoveNext();
                        }

                        // Enable controls
                        Label1.Enabled = true;
                        Label2.Enabled = true;
                        Label3.Enabled = true;
                        Combo1.Enabled = true;
                        Text1.Enabled = true;
                        Text2.Enabled = true;
                        Command1.Enabled = true;
                    }

                }

                if (globals_Renamed.oCompany.Connected == true)
                {
                    // if the company object is already connected to a DB
                    // you must first Disconnect it before connecting it
                    // to a different DB
                    // bare in mind that by disconnecting the company object
                    // you lose all of the properties (Server, Language)
                    Command1.Enabled = false;
                    Combo1.SelectedText = globals_Renamed.oCompany.CompanyDB;
                    Text1.Text = globals_Renamed.oCompany.UserName;
                    Text2.Text = globals_Renamed.oCompany.Password;
                    this.Text = this.Text + ": Connected";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                globals_Renamed.InitializeCompany();
            }
        }

        private void btnConf_Click(object sender, EventArgs e)
        {
            /*Funci�n:  Se agrega los campos para control de servicios y tabla de configuracion y Carta porte
             *Autor: Alvaro May 
             * Fecha: 01/12/2017
             * */
            try
            {
                this.bCrearNuevoLogInstalacion = true;
                this.EscribirLogInstalacion("Inicia instalacion de Kanan Fleet");

                #region Registrar permisos menu
                IniciaProcesoMenus();
                #endregion Registrar permisos menu

                #region Campos de usuarios sobre documentos SAP
                this.SociosdeNegocio();
                this.Entregas();
                this.DatoMaestroArticulo();
                #endregion Campos de usuarios sobre documentos SAP

                #region UDT
                CreateEmpleado();
                CreateEmpresa();
                CreateSesionTable();
                CreateTipoMoneda();
                CreateTipoPago();
                CreateTipoMantenible();
                CreateSucursales();

                CreateVehiculosTable();
                CreateCargaCombustible();
                CreateDesplazamiento();
                //CreateDesplazamientoHistorico();
                CreateTipoVehiculo();

                CreateProveedoresTable();

                CreateLlantaTable();
                CreateILlantaTable();

                CreateServicioTable();
                CreateTipoServicio();

                CreateActivosTable();
                CreateTipoActivo();

                CreateOrdenServicioTable();
                CreatePlantillaOSTable();
                CreatePlantillaOSTableDetalle();
                CreatePlantillaOSRefacciones();
                CreatePlantillaHerramientas();
                CreatePlantillaCostosAdicionales();
                CreatePlantillaMecanicos();
                BitacoraOS();
                CreateDetalleOrdenTable();
                CreateEstatusOrdenTable();
                CreateFolioOrden();

                CreateOperadoresTable();
                CreateOperadorVehiculoTable();
                CreateLicencia();
                CreateCurso();
                CreateObservacion();
                CreateInfracciones();
                CreateTiposInfracciones();
                //Tablas de integraci�n a contabilidad
                CreaTablaConfiguracion();
                CreaTablaCartaPorte();
                CreaTablaOrdeServicioRefacciones();
                CreaTablaOrdenServicioCostoAdicional();
                CreaTablaOrdenServicioHerramientas();
                CreaTablaOrdenServicioMecanicos();
                CreaTablaCartaDetalle();


                //todos para incidencias en vehiculos
                CreateIncidenciaVehiculo();
                CreateIncidenciaLlantas();
                CreateCausaRaiz();
                CreateCausaRaizDetalle();

                //tablas de integraci�n alertas de Treadwear
                CreateTablaConfiguracionDesgaste();
                CreateTablaAlertaDesgaste();
                CambiosEstatusOrdenServicio();

                CreateTableGestoriaPlacas();
                CreateTableGestoriaPropietario();
                CreateTableGestoriaTarjeta();
                CreateTableGestoriaAlta();
                CreateTableGestoriaBaja();

                //LigarEmpresa(); NO va por que tiene valores fijos

                //Gesti�n unidades de medida
                CreateUnidadMedida();
                CreateParametroServicio();
                CreateNotificaciones();
                CreateAlertaTable();
                CreateAlertaMantenimientoTable();
                CreateAlertaRutinaTable();
                CreateParametroMantenimiento();
                CreateParamXTipoVehiculo();
                CreaTablaConceptosFuelData();
                CreateClientTable();

                //Carta Porte generado por el Kanan Service ( liquidaciones)
                CreatePagosConceptos();
                CreateCP_Impuestos();
                CreateCP_Anticipos();
                CreateCP_ConceptosGastos();
                CreateCP_Gastos();
                CreateCP_OtrosGastos();

                #endregion
                this.EscribirLogInstalacion("Fin de instalacion de Kanan Fleet");
                MessageBox.Show("Las modififcaciones a base de datos se aplicaron correctamente");
                this.Dispose(true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #region Crea registro de menu accesos
        void IniciaProcesoMenus()
        {
            #region Comentarios
            /******************
             *Comentarios:      Metodo utilizado para lectura e invocaci�n para asiganr 
             *                  registro de men�. 
             ******************/
            #endregion Comentarios
            try
            {
                XDocument oDocumentoXML = XDocument.Load(string.Format(@"{0}\XML_RegistroMenu.xml", AppDomain.CurrentDomain.BaseDirectory));
                var oElementos = from oNodos in oDocumentoXML.Descendants() where (oNodos.Name == "Autorizaciones") select oNodos;
                var oAutorizaciones = from oNodos in oElementos.Descendants() where oNodos.Name == "Autorizacion" select oNodos;

                foreach (XElement oAutorizacion in oAutorizaciones)
                {
                    this.CreaAutorizacionSistema(oAutorizacion);
                }
            }
            catch { }
        }
        bool CreaAutorizacionSistema(XElement oElementoXML)
        {
            Boolean bPrimerRegistro;
            UserPermissionTree oPermiso = null;
            try
            {
                oPermiso = (UserPermissionTree)globals_Renamed.oCompany.GetBusinessObject(BoObjectTypes.oUserPermissionTree);
                string sKey = oElementoXML.Attribute(XName.Get("Clave")).Value;
                if (oPermiso.GetByKey(sKey))
                    return true;

                oPermiso.PermissionID = oElementoXML.Attribute(XName.Get("Clave")).Value;
                oPermiso.Name = oElementoXML.Attribute(XName.Get("Nombre")).Value;
                oPermiso.Options = (BoUPTOptions)Int32.Parse(oElementoXML.Attribute(XName.Get("Opcion")).Value);

                if (oElementoXML.Attribute(XName.Get("Padre")).Value != "")
                    oPermiso.ParentID = oElementoXML.Attribute(XName.Get("Padre")).Value;

                //Agregando formas
                var oFormasAutorizacion =
                    from oNodos in oElementoXML.Descendants()
                    where oNodos.Name == "FormaAutorizacion"
                    select oNodos;

                bPrimerRegistro = true;
                foreach (XElement oFormaAutorizacion in oFormasAutorizacion)
                {
                    if (!bPrimerRegistro)
                    {

                        this.EscribirLogInstalacion("Creando autorizaci�n " + oPermiso.PermissionID);
                        oPermiso.UserPermissionForms.Add();
                    }
                    else
                        bPrimerRegistro = false;

                    oPermiso.UserPermissionForms.FormType = oFormaAutorizacion.Attribute(XName.Get("Nombre")).Value;
                }

                if (oPermiso.Add() != 0)
                    throw new Exception(globals_Renamed.oCompany.GetLastErrorDescription() + " Error al crear autorizaci�n -> " + oPermiso.Name);

                return true;
            }
            catch (Exception oError)
            {
                //this.ColocarMensaje(oError.Message, eTipoMensaje.Error);
                this.GuardarError(oError.ToString());
                return false;
            }
            finally
            {
                if (oPermiso != null)
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oPermiso);
                    oPermiso = null;
                    GC.Collect();
                }
            }
        }
        #endregion Crea registro de menu accesos


        #endregion ChooseCompany

        #region Documentos SAP
        private void SociosdeNegocio()
        {
            #region Comentarios
            /**********************
             * Comentarios:     Crea campos de usuario para tabla de socios de negocio 
             **********************/
            #endregion Comentarios
            try
            {
                const string tablename = "OCRD";
                this.EscribirLogInstalacion("Creando Campos en tabla: " + tablename);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "KF_CostCentro", "CENTRO COSTO", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void DatoMaestroArticulo()
        {
            #region Comentarios
            /**********************
             * Comentarios:     Determina si el dato maestro debe ser sincronizado por movimiento de inventario
             **********************/
            #endregion Comentarios
            try
            {
                const string tablename = "OITM";
                this.EscribirLogInstalacion("Creando campos en tabl: " + tablename);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "KF_Sincronizable", "SINCRONIZAR CON KANANFLEET", BoFieldTypes.db_Alpha, 1, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "KF_ArticuloID", "FOLIO KANANFLEET", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "KF_TIPART", "TIPO ARTICULO", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "KF_TIPART", "TIPO ARTICULO", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "Marca", "Marca", BoFieldTypes.db_Alpha, 50, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "Modelo", "Modelo", BoFieldTypes.db_Alpha, 50, BoYesNoEnum.tNO);
                GC.Collect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void Entregas()
        {
            #region Comentarios
            /**********************
             * Comentarios:     Crea campos de usuario para tabla entregas en documentos de marketing
             **********************/
            #endregion Comentarios
            try
            {
                const string tablename = "ODLN";
                this.EscribirLogInstalacion("Creando campos en tabl: " + tablename);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "KF_WebItinerario", "ITINERARIO KANANFLEET", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "KF_VehiculoFK", "Vehiculo KF", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "KF_FolioKF", "FOLIO KANANFLEET", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                this.EntregasDetalle();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void EntregasDetalle()
        {
            #region Comentarios
            /**********************
             * Comentarios:     Crea campos de usuario para tabla entregas  en lineas de marketing
             **********************/
            #endregion Comentarios
            try
            {
                const string tablename = "DLN1";
                this.EscribirLogInstalacion("Creando campos en tabl: " + tablename);
                GC.Collect();
                GC.Collect();
                AddUDT.CreateUDF(tablename, "VSKF_VH", "Placa", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "VSKF_VHNOECO", "No Economico", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion Documentos SAP

        #region LigarEmpresa
        public void LigarEmpresa()
        {
            try
            {
                //Add add = new Add();
                //add.LigarEmpresa();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Cuenta
        private void CreateEmpleado()
        {
            try
            {
                const string tablename = "VSKF_RELEMPLEADO";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Relaci�n de Empleados");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SAPEMPLEADOID", "Empleado SAP", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "KFEMPLEADOID", "Empleado KananFleet", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateEmpresa()
        {
            try
            {
                const string tablename = "VSKF_RELEMPRESA";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Relacion Empresa");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "KFEMPRESAID", "Empresa KananFleet", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SAPCOMPANY", "SAP Company", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateSesionTable()
        {
            try
            {
                string tablename = "VSKF_SESION";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Tabla de Sesion
                GC.Collect();
                //AddUDT.CreateUDT("VHKN", "VehiculosKananFleet");
                AddUDT.CreateUDT(tablename, "SesionesActivas");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "PUBLICEKY", "public key", BoFieldTypes.db_Alpha, 50, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "PRIVATEKEY", "private key", BoFieldTypes.db_Alpha, 50, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "USUARIOID", "Usuario ID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FECHASESION", "Fecha Sesion", BoFieldTypes.db_Date, 20, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FINSESION", "Fin Sesion", BoFieldTypes.db_Date, 20, BoYesNoEnum.tNO);

                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateTipoMoneda()
        {
            try
            {
                const string tablename = "VSKF_TIPOMONEDA";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Vehiculos
                GC.Collect();
                //AddUDT.CreateUDT("VHKN", "VehiculosKananFleet");
                AddUDT.CreateUDT(tablename, "Tipo Moneda");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ESTATUS", "Estatus", BoFieldTypes.db_Numeric, 2, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateTipoPago()
        {
            try
            {
                const string tablename = "VSKF_TIPODEPAGO";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Vehiculos
                GC.Collect();
                //AddUDT.CreateUDT("VHKN", "VehiculosKananFleet");
                AddUDT.CreateUDT(tablename, "Tipo De Pago");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ESTATUS", "Estatus", BoFieldTypes.db_Numeric, 2, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateTipoMantenible()
        {
            try
            {
                const string tablename = "VSKF_TIPOMANTENIBLE";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Vehiculos
                GC.Collect();
                //AddUDT.CreateUDT("VHKN", "VehiculosKananFleet");
                AddUDT.CreateUDT(tablename, "Tipo Mantenible");  // Se cambia de TipoDePago a TipoMantenible
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TMANTBLEID", "TMantbleID", BoFieldTypes.db_Numeric, 2, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "NOMBRE", "Nombre", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateSucursales()
        {
            try
            {
                string tablename = "VSKF_Sucursal";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Sucursales
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Sucursales KananFleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SucursalID", "SucursalID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Direccion", "Direccion", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpresaID", "EmpresaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EsActivo", "EsActivo", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Encargado", "Encargado", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Descripcion", "Descripcion", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Contacto", "Contacto", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Telefono", "Telefono", BoFieldTypes.db_Alpha, 50, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "CodigoPostal", "CodigoPostal", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Responsable", "Responsable", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Ciudad", "Ciudad", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Estado", "Estado", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "DireccionReal", "DireccionReal", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UUID", "UUID", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Almacen", "Almacen de la sucursal en SAP", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "BranchID", "Branch asociado a sucursal", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Centro", "Centro de costo de sucursal", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "MAILALM", "Mail Almacen", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "HRSDISP", "Horas disponibles", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Vehiculos
        private void CreateVehiculosTable()
        {
            try
            {
                string tablename = "VSKF_Vehiculo";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Vehiculos
                GC.Collect();
                AddUDT.CreateUDT(tablename, "VehiculosKananFleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "VehiculoID", "VehiculoID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Placa", "Placa", BoFieldTypes.db_Alpha, 25, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Modelo", "Modelo", BoFieldTypes.db_Alpha, 50, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Descripcion", "Descripcion", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoV_ID", "TipoV_ID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ImagenID", "ImagenID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Nombre", "Nombre", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SucursalID", "SucursalID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpresaID", "EmpresaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "NombreVehiculo", "NombreVehiculo", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Poliza", "Poliza", BoFieldTypes.db_Alpha, 50, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FechaPoliza", "FechaPoliza", BoFieldTypes.db_Date, 20, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Plazas", "Plazas", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "RendComb", "RendComb", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EsActivo", "EsActivo", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Marca", "Marca", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Anio", "Anio", BoFieldTypes.db_Numeric, 8, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ModemID", "ModemID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AvisoPoliza", "AvisoPoliza", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "RendCarga", "RendCarga", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ULongitud", "ULongitud", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UVolumen", "UVolumen", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UUID", "UUID", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoChss", "Tipo Chasis", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "COSTO", "Costo", BoFieldTypes.db_Float, 10, BoYesNoEnum.tNO, BoFldSubTypes.st_Price);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "IAVE", "IAVE", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Cobertura", "Cobertura", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ComponenteID", "ComponenteID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FechaAlta", "FechaAlta", BoFieldTypes.db_Date, 20, BoYesNoEnum.tNO, BoFldSubTypes.st_Measurement);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FechaBaja", "FechaBaja", BoFieldTypes.db_Date, 20, BoYesNoEnum.tNO, BoFldSubTypes.st_Measurement);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "NoSerie", "NoSerie", BoFieldTypes.db_Alpha, 25, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "NoMotor", "NoMotor", BoFieldTypes.db_Alpha, 25, BoYesNoEnum.tNO);
                GC.Collect();
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateCargaCombustible()
        {
            try
            {
                string tablename = "VSKF_CrgCombustible";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Carga de Combustible
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Carga de Combustible");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Litros", "Litros", BoFieldTypes.db_Float, 254, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Pesos", "Pesos", BoFieldTypes.db_Float, 254, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "VehiculoID", "VehiculoID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Fecha", "Fecha", BoFieldTypes.db_Date, 20, BoYesNoEnum.tNO, BoFldSubTypes.st_Measurement);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "HoraMinuto", "HoraMinuto", BoFieldTypes.db_Alpha, 6, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FechaCaptura", "FechaCaptura", BoFieldTypes.db_Date, 20, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Ticket", "Ticket", BoFieldTypes.db_Alpha, 50, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoCombustible", "TipoCombustible", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ProveedorID", "ProveedorID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Odometro", "Odometro", BoFieldTypes.db_Float, 254, BoYesNoEnum.tNO, BoFldSubTypes.st_Measurement);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FormaPago", "FormaPago", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "DetallePago", "DetallePago", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "CentroServicio", "CentroServicio", BoFieldTypes.db_Alpha, 50, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ULongitudID", "ULongitudID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UVolumenID", "UVolumenID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UUID", "UUID", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoMonedaID", "TipoMonedaID", BoFieldTypes.db_Numeric, 8, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Latitude", "Latitude", BoFieldTypes.db_Float, 254, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Longitude", "Longitude", BoFieldTypes.db_Float, 254, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Facturable", "Facturable", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateDesplazamiento()
        {
            try
            {
                const string tablename = "VSKF_DESPLAZAMIENTO";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Alerta
                GC.Collect();
                //AddUDT.CreateUDT("VHKN", "VehiculosKananFleet");
                AddUDT.CreateUDT(tablename, "Desplazamiento Vehiculos"); // Se cambia de Alertas KananFleet a Desplazamiento Vehiculos
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Distancia", "Distancia", BoFieldTypes.db_Float, 254, BoYesNoEnum.tNO, BoFldSubTypes.st_Measurement);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Fecha", "Fecha", BoFieldTypes.db_Date, 20, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "VehiculoID", "VehiculoID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EstaIniciado", "EstaIniciado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UUID", "UUID", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateTipoVehiculo()
        {
            try
            {
                const string tablename = "VSKF_TIPOVEHICULO";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Tabla de tipos de servicio
                GC.Collect();
                //AddUDT.CreateUDT("VHKN", "VehiculosKananFleet");
                AddUDT.CreateUDT(tablename, "Tipos Vehiculos Kanan"); // Cambia de Tipos Servicio KannaFleet  a Tipos Vehiculos Kanan
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TVeiculoID", "TVeiculoID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Nombre", "Nombre", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Descrip", "Descripcion", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SucursalID", "SucursalID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ImagenUrl", "ImagenUrl", BoFieldTypes.db_Memo, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpresaID", "EmpresaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EsActivo", "EsActivo", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UUID", "UUID", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateIncidenciaVehiculo()
        {
            try
            {
                string tablename = "VSKF_InVehiculo";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Tabla de incidencias en veh�culos
                GC.Collect();
                AddUDT.CreateUDT(tablename, "IncidenciaVehiculo");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "IncidenciaID", "IncidenciaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "VehiculoID", "VehiculoID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FechaIncidencia", "FechaIncidencia", BoFieldTypes.db_Date, 25, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Descripcion", "Descripcion", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                //GC.Collect();
                //AddUDT.CreateUDF(tablename, "ImgInciAntID", "ImgInciAntID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                //GC.Collect();
                //AddUDT.CreateUDF(tablename, "ImgInciDesID", "ImgInciDesID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Latitude", "Latitude", BoFieldTypes.db_Float, 254, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Longitude", "Longitude", BoFieldTypes.db_Float, 254, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                AddUDT.CreateUDF(tablename, "CreaOS", "Determina si se debe crear una OS", BoFieldTypes.db_Numeric, 1, BoYesNoEnum.tNO);
                GC.Collect();
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void CreateCausaRaiz()
        {
            try
            {
                string tablename = "VSKF_CAUSARAIZ";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Tabla de incidencias en veh�culos
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Sistemas de causa raiz");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Descripcion", "Nombre de sistema causa raiz", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Fecharegistro", "Fecha que se ha registrado", BoFieldTypes.db_Date, 25, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Activo", "Determina si se encuentra activo", BoFieldTypes.db_Alpha, 2, BoYesNoEnum.tNO);
                GC.Collect();
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void CreateCausaRaizDetalle()
        {
            try
            {
                string tablename = "VSKF_CAUSADETALLE";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Tabla de incidencias en veh�culos
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Detalle de causa raiz");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "CausaID", "Relacion con id de causa padre", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Componente", "Identifica la descripcion/nombre del componente", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                AddUDT.CreateUDF(tablename, "Activo", "Detemina si est� activo", BoFieldTypes.db_Alpha, 1, BoYesNoEnum.tNO);
                GC.Collect();
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Proveedores
        private void CreateProveedoresTable()
        {
            try
            {
                string tablename = "VSKF_Proveedor";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Proveedores
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Proveedores KananFleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ProveedorID", "ProveedorID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Nombre", "privateNombrekey", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Direccion", "Direccion", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Telefono", "Telefono", BoFieldTypes.db_Alpha, 50, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Contactos", "Contactos", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Correos", "Correos", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Propietario", "Propietario", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "CUP", "CUP", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Llantas
        private void CreateLlantaTable()
        {
            try
            {
                const string tablename = "VSKF_LLANTA";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Tabla de Llantas
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Llantas KananFleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ItemCode", "ItemCode", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "NumEconomico", "NumEconomico", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "NumRevestimientos", "NumRevestimientos", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SN", "SN", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Marca", "Marca", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Modelo", "Modelo", BoFieldTypes.db_Alpha, 50, BoYesNoEnum.tNO);
                GC.Collect();
                //AddUDT.CreateUDF(tablename, "Tamanio", "Tamanio", BoFieldTypes.db_Alpha, 50, BoYesNoEnum.tNO);
                //GC.Collect();
                AddUDT.CreateUDF(tablename, "NumCapas", "NumCapas", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Costo", "Costo", BoFieldTypes.db_Float, 254, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ClaseRotacion", "ClaseRotacion", BoFieldTypes.db_Alpha, 6, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ProveedorLlanta", "ProveedorLlanta", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "NumeroFactura", "NumeroFactura", BoFieldTypes.db_Alpha, 64, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FechaCompra", "FechaCompra", BoFieldTypes.db_Date, 50, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EsActivo", "EsActivo", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Observaciones", "Observaciones", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "KmsRecorridos", "KmsRecorridos", BoFieldTypes.db_Float, 254, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Propietario", "Propietario", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EstaInstalado", "EstaInstalado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SucursalID", "SucursalID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Uso", "Uso", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Ancho", "Ancho", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Aspecto", "Aspecto", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Construccion", "Construccion", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "DiametroRin", "DiametroRin", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "IndiceCarga", "IndiceCarga", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "RangoVelocidad", "RangoVelocidad", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Aplicacion", "Aplicacion", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Traccion", "Traccion", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Treadwear", "Treadwear", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Temperatura", "Temperatura", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "MaxiPresionInflado", "MaximaPresionInflado", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "MinPresionInflado", "MinimaPresionInflado", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FechaFabricacion", "FechaFabricacion", BoFieldTypes.db_Date, 50, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UUID", "UUID", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "KmCaducidad", "KmCaducidad", BoFieldTypes.db_Float, 15, BoYesNoEnum.tNO, BoFldSubTypes.st_Measurement);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FechaCaducidad", "FechaCaducidad", BoFieldTypes.db_Date, 254, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateILlantaTable()
        {
            try
            {
                const string tablename = "VSKF_ILLANTA";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Tabla de Llantas
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Llantas Asignadas KananFleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "KmsRecorridos", "KmsRecorridos", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Ubicacion1", "Ubicacion1", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Ubicacion2", "Ubicacion2", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Ubicacion3", "Ubicacion3", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Ubicacion4", "Ubicacion4", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Ubicacion5", "Ubicacion5", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Ubicacion6", "Ubicacion6", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Ubicacion7", "Ubicacion7", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Ubicacion8", "Ubicacion8", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Ubicacion9", "Ubicacion9", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Ubicacion10", "Ubicacion10", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Ubicacion11", "Ubicacion11", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Ubicacion12", "Ubicacion12", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Ubicacion13", "Ubicacion13", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Ubicacion14", "Ubicacion14", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Ubicacion15", "Ubicacion15", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Ubicacion16", "Ubicacion16", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Ubicacion17", "Ubicacion17", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Ubicacion18", "Ubicacion18", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoILLanta", "TipoILLanta", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ActivoID", "ActivoID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "VehiculoID", "VehiculoID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UUID", "UUID", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateIncidenciaLlantas()
        {
            try
            {
                string tablename = "VSKF_InLlantas";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region tabla de incidencias en llantas
                GC.Collect();
                AddUDT.CreateUDT(tablename, "IncidenciaLlantas");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "IncidenciaID", "IncidenciaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Descripcion", "Descipcion", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FechaIncidencia", "FechaIncidencia", BoFieldTypes.db_Date, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "HoraIncidencia", "HoraIncidencdia", BoFieldTypes.db_Alpha, 6, BoYesNoEnum.tNO);
                //GC.Collect();
                //AddUDT.CreateUDF(tablename, "ImagenID", "ImagenID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpresaID", "EmpresaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SoluInciLlantaID", "SoluInciLlantaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SucursalID", "SucursalID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "LlantaID", "LlantaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "MantenibleID", "MantenibleID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoMantenibleID", "TipoMantenibleID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Activo", "Activo", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Ubicacion", "Ubicacion", BoFieldTypes.db_Float, 254, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                #endregion
            }

            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateTablaConfiguracionDesgaste()
        {
            try
            {
                const string tablename = "VSKF_PMTRODESGASTE";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Tabla de ParametroDesgaste
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Parametro de desgaste");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "PmDesID", "PmDesID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpresaID", "EmpresaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SucursalID", "SucursalID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Uso", "Uso", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "LlantaID", "LlantaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "LimPorc", "LimPorc", BoFieldTypes.db_Float, 10, BoYesNoEnum.tNO, BoFldSubTypes.st_Percentage);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "LimProf", "LimProf", BoFieldTypes.db_Float, 10, BoYesNoEnum.tNO, BoFldSubTypes.st_Measurement);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AvisoLimPorc", "AvisoLimPorc", BoFieldTypes.db_Float, 10, BoYesNoEnum.tNO, BoFldSubTypes.st_Percentage);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AvisoLimProf", "AvisoLimProf", BoFieldTypes.db_Float, 10, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TiempActTread", "TiempActTread", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateTablaAlertaDesgaste()
        {
            try
            {
                const string tablename = "VSKF_ALERTADESGASTE";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Tabla de AlertaDesgaste
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Alerta de desgaste");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AlertaDesgasteID", "AlertaDesgasteID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "VehiculoID", "VehiculoID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "LlantaID", "LlantaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpresaID", "EmpresaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SucursalID", "SucursalID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoAlerta", "TipoAlerta", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Servicios
        private void CreateServicioTable()
        {
            try
            {
                const string tablename = "VSKF_SERVICIOS";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Tabla de servicios
                GC.Collect();
                //AddUDT.CreateUDT("VHKN", "VehiculosKananFleet");
                AddUDT.CreateUDT(tablename, "Servicios KananFleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ServicioID", "ServicioID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Nombre", "Nombre", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Descripcion", "Descripcion", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ImagenUrl", "ImagenUrl", BoFieldTypes.db_Memo, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SucursalID", "SucursalID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoServicioID", "TipoServicioID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpresaID", "EmpresaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EsActivo", "EsActivo", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "CuentaContable", "Cuenta contable SBO", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UUID", "UUID", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ItemCode", "Codigo de articulo dato maestro", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "NOMBRECUENTA", "Nombre de la cuenta utilizada para el servicio", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateTipoServicio()
        {
            try
            {
                const string tablename = "VSKF_TIPOSERVICIO";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Tabla de tipos de servicio
                GC.Collect();
                //AddUDT.CreateUDT("VHKN", "VehiculosKananFleet");
                AddUDT.CreateUDT(tablename, "Tipos Servicios KananFleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoServicioID", "TipoServicioID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Nombre", "Nombre", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Descripcion", "Descripcion", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpresaID", "EmpresaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SucursalID", "SucursalID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EsActivo", "EsActivo", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UUID", "UUID", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Activos
        private void CreateActivosTable()
        {
            try
            {
                string tablename = "VSKF_Activo";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Activos
                GC.Collect();
                AddUDT.CreateUDT(tablename, "ActivosKananFleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ActivoID", "ActivoID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "NumeroEconomico", "NumeroEconomico", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Observaciones", "Observaciones", BoFieldTypes.db_Alpha, 50, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EsActivo", "EsActivo", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "PropietarioID", "PropietarioID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "VehiculoID", "VehiculoID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Posicion", "Posicion", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SucursalID", "SucursalID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Tipo", "Tipo", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Marca", "Marca", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Modelo", "Modelo", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Placa", "Placa", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Descripcion", "Descripcion", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoActivoID", "TipoActivoID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UUID", "UUID", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Anio", "Anio", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Detalle", "Detalle", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Poliza", "Poliza", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FechaPoliza", "FechaPoliza", BoFieldTypes.db_Date, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AvisoPoliza", "AvisoPoliza", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Plazas", "Plazas", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "RendComb", "RendComb", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "RendCarga", "RendCarga", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoChss", "TipoChss", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "costo", "costo", BoFieldTypes.db_Float, 254, BoYesNoEnum.tNO, BoFldSubTypes.st_Price);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ComponenteID", "ComponenteID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateTipoActivo()
        {
            try
            {
                const string tablename = "VSKF_TipoActivos";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Vehiculos
                GC.Collect();
                //AddUDT.CreateUDT("VHKN", "VehiculosKananFleet");
                AddUDT.CreateUDT(tablename, "TipoActivos");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Estatus", "Estatus", BoFieldTypes.db_Numeric, 2, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Ordenes de Servicio
        private void CreateOrdenServicioTable()
        {
            try
            {
                const string tablename = "VSKF_ORDENSERVICIO";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Tabla Ordenes Servicio
                GC.Collect();
                AddUDT.CreateUDT(tablename, "ORDENSERVICIO");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "OrdenServicioID", "OrdenServicioID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpleadoID", "EmpleadoID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ServicioID", "ServicioID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Fecha", "Fecha", BoFieldTypes.db_Date, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Folio", "Folio", BoFieldTypes.db_Alpha, 50, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Lugar", "Lugar", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Descripcion", "Descripcion", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SucursalID", "SucursalID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpresaID", "EmpresaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FechaCaptura", "FechaCaptura", BoFieldTypes.db_Date, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ProveedorID", "ProveedorID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AlertaAvisoID", "AlertaAvisoID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "KMRecorridos", "KMRecorridos", BoFieldTypes.db_Float, 254, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Costo", "Costo", BoFieldTypes.db_Float, 254, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Archivo", "Archivo", BoFieldTypes.db_Memo, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "IVA", "IVA", BoFieldTypes.db_Float, 254, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoPagoID", "TipoPagoID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "DetallePago", "DetallePago", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "MantenibleID", "MantenibleID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoMantenibleID", "TipoMantenibleID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ResponsableID", "ResponsableID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AprobadorID", "AprobadorID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EstatusOrdenID", "EstatusOrdenID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FolioOrden", "FolioOrden", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AnioFolio", "AnioFolio", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UUID", "UUID", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "DocEntry", "LigaFactura", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TallerInterno", "TallerInterno", BoFieldTypes.db_Numeric, 1, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EnPro", "EnPro", BoFieldTypes.db_Date, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SalPro", "SalPro", BoFieldTypes.db_Date, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EnReal", "EnReal", BoFieldTypes.db_Date, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SalReal", "SalReal", BoFieldTypes.db_Date, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SistemaID", "Sistema id de componente", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ComponenteID", "Componente asociado al sistema", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "PrioridadID", "Representa el nivel de prioridad para la OS", BoFieldTypes.db_Numeric, 1, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ClienteSN", "Codigo de cliente para facturar", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "inspeccionID", "Folio de inspeccion para el registro de la OS", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "HoraEnP", "Hora de entrada programada", BoFieldTypes.db_Alpha, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "HoraSalP", "Hora de salida programada", BoFieldTypes.db_Alpha, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "HoraEnR", "Hora de entrada real", BoFieldTypes.db_Alpha, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "HoraSalR", "Hora de salida real", BoFieldTypes.db_Alpha, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Facturable", "Determina si se debe crear la factura de clliente", BoFieldTypes.db_Numeric, 1, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "RutinaID", "Rutina Ligada", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Odometro", "Odometro", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Horometro", "Horometro", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateDetalleOrdenTable()
        {
            try
            {
                const string tablename = "VSKF_DETALLEORDEN";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Detalle Orden de Servicio
                GC.Collect();
                AddUDT.CreateUDT(tablename, "DetalleOrdenKananFleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "DetalleOrdenID", "DetalleOrdenID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "MantenibleID", "MantenibleID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ServicioID", "ServicioID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "OrdenServicioID", "OrdenServicioID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoMantenibleID", "TipoMantenibleID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Costo", "Costo", BoFieldTypes.db_Float, 254, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UUID", "UUID", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateEstatusOrdenTable()
        {
            try
            {
                const string tablename = "VSKF_ESTATUSORDEN";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Estatus Orden de Servicio
                GC.Collect();
                AddUDT.CreateUDT(tablename, "EstatusKananFleet");
                //GC.Collect();
                //AddUDT.CreateUDF(tablename, "EstatusOrdenID", "EstatusOrdenID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                //GC.Collect();
                //AddUDT.CreateUDF(tablename, "Descripcion", "Descripcion", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Activo", "Activo", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                //GC.Collect();
                //AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                //GC.Collect();
                //AddUDT.CreateUDF(tablename, "UUID", "UUID", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateChangeDateEstatus()
        {
            try
            {
                const string tablename = "VSKF_ChngEstatus";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region ChangeDateEstatus
                GC.Collect();
                AddUDT.CreateUDT(tablename, "FechaCambioEstatus");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "OrdenID", "OrdenID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Fecha", "Fecha", BoFieldTypes.db_Date, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Estatus", "Estatus", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                #endregion ChangeDateEstatus
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateFolioOrden()
        {
            try
            {
                const string tablename = "VSKF_FolioOrden";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Folio Orden Servicio
                GC.Collect();
                AddUDT.CreateUDT(tablename, "FolioOrden");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Folio", "Folio", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Anio", "Anio", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpresaID", "EmpresaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UUID", "UUID", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Plantilla Orden Servicio
        private void CreatePlantillaOSTable()
        {
            try
            {
                string tablename = "VSKF_OSPLANTILLA";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Plantilla OServicio");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SucursalID", "SucursalID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TallerInterno", "TallerInterno", BoFieldTypes.db_Alpha, 1, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EntradaProgramada", "EntradaProgramada", BoFieldTypes.db_Date, 32, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SalidaProgramada", "SalidaProgramada", BoFieldTypes.db_Date, 32, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FechaRevision", "FechaRevision", BoFieldTypes.db_Date, 32, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Descripcion", "Descripcion", BoFieldTypes.db_Alpha, 250, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ResponsableID", "ResponsableID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AprobadorID", "AprobadorID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoVehiculoID", "TipoVehiculoID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreatePlantillaOSTableDetalle()
        {
            try
            {
                string tablename = "VSKF_OSPLANTILLADET";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Plantilla detalle");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "PlantillaPadre", "Codigo de plantilla padre", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "MantenibleID", "Codigo de mantenible vehiculo", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "VehiculoNombre", "Nombre de vehiculo mantenible", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ServicioID", "Codigo de orden servicio", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "NombreServicio", "Nombre del servicio para vehiculo", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Costo", "Importe del servicio", BoFieldTypes.db_Float, 16, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        private void CreatePlantillaOSRefacciones()
        {
            try
            {
                string tablename = "VSKF_PLANTREFACCION";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Plantilla de refacc");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "PlantillaPadre", "Identificador de plantilla", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Costo", "Costo de refaccion", BoFieldTypes.db_Float, 16, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Articulo", "Codigo articulo refaccion", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Cantidad", "Cantidad de refacciones", BoFieldTypes.db_Float, 16, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Total", "Importe total refacciones", BoFieldTypes.db_Float, 16, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Almacen", "Nombre almacen refacciones", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "CodAlmcn", "Codigo de almacen refacciones", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        
        private void CreatePlantillaCostosAdicionales()
        {
            try
            {
                string tablename = "VSKF_PLANTCOSTOSAD";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Plantilla gastos adicionales");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "PlantillaPadre", "Identificador de plantilla", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Descripcion", "Nombre de costo", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Costo", "Importe por costo", BoFieldTypes.db_Float, 254, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreatePlantillaMecanicos()
        {
            try
            {
                string tablename = "VSKF_PLANTMECANICOS";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Plantilla de mecanicos");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "PlantillaPadre", "Identificador de plantilla", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Empleadoid", "Empleado id rh", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Nombre", "Nombre completo empleado", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sueldo", "Sueldo del empleado", BoFieldTypes.db_Float, 254, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreatePlantillaHerramientas()
        {
            try
            {
                string tablename = "VSKF_PLANTHERRAMIEN";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Plantilla de herramientas");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "PlantillaPadre", "Identificador de plantilla", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FOLIO", "Folio dato maestro", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "DESCRIPCION", "Descripcion dato maestro", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "CANTIDAD", "Cantidad de herramientas", BoFieldTypes.db_Float, 16, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        #endregion Plantilla Orden Servicio

        #region Bitacora de OS 
        private void BitacoraOS()
        {
            try
            {
                string tablename = "VSKF_OSCAMESTADO";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Bitacora de movimientos en OS");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FolioSAP", "Folio control interno SAP", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FolioWeb", "Folio control Kanan Web", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Fecha", "Fecha de registro", BoFieldTypes.db_Date, 32, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Hora", "Hora de registro", BoFieldTypes.db_Alpha, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Estatusa", "Estatus actual", BoFieldTypes.db_Alpha, 20, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EstatusaID", "Identificador de estatus actual", BoFieldTypes.db_Numeric, 2, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Estatusan", "Estatus anterior", BoFieldTypes.db_Alpha, 20, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EstatusanID", "Identificador de estatus anterior", BoFieldTypes.db_Numeric, 2, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Usuario", "Usuario de sap", BoFieldTypes.db_Alpha, 20, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Origen", "Origen de movimiento", BoFieldTypes.db_Alpha, 1, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Determina si el registro es sincronizado", BoFieldTypes.db_Alpha, 1, BoYesNoEnum.tNO);
                GC.Collect();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion Bitacora de OS

        #region Operador

        private void CreateOperadoresTable()
        {
            try
            {
                string tablename = "VSKF_OPERADOR";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Tabla de Sesion
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Operadores KananFleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Nombre", "Nombre", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Apellido1", "Apellido1", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Apellido2", "Apellido2", BoFieldTypes.db_Alpha, 50, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Direccion", "Direccion", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Telefono", "Telefono", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Celular", "Celular", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Correo", "Correo", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "CURP", "CURP", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Fechanac", "Fechanac", BoFieldTypes.db_Date, 32, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Lugarnac", "Lugarnac", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Alias", "Alias", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpresaID", "EmpresaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SucursalID", "SucursalID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Correosnot", "Correosnot", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EsActivo", "EsActivo", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ImagenID", "ImagenID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpleadoID", "EmpleadoID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UUID", "UUID", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ImagenPerfil", "Imagen de perfil", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FondoFijo", "Importe de fondo fijo", BoFieldTypes.db_Float, 10, BoYesNoEnum.tNO,BoFldSubTypes.st_Price);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "CardCode", "Codigo de cliente SN", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateOperadorVehiculoTable()
        {
            try
            {
                const string tablename = "VSKF_OPERADORVEHICL";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Tabla de Asignaci�n de Operador a Vehiculo
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Asigna Operador KananFleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "VehiculoID", "VehiculoID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "OperadorID", "OperadorID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FechaAsignacion", "FechaAsignacion", BoFieldTypes.db_Date, 50, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "HoraMinutoAsigna", "HoraMinuto", BoFieldTypes.db_Alpha, 6, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FechaLiberacion", "FechaLiberacion", BoFieldTypes.db_Date, 50, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "HoraMinutoLibera", "HoraMinuto", BoFieldTypes.db_Alpha, 6, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ObservAsigna", "ObservAsigna", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ObservaLibera", "ObservLibera", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UUID", "UUID", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                AddUDT.CreateUDF(tablename, "Liberado", "Disposici�n de veh�culo", BoFieldTypes.db_Numeric, 1, BoYesNoEnum.tNO);
                GC.Collect();
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateLicencia()
        {
            try
            {
                const string tablename = "VSKF_Licencia";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Licencia
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Licencia");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ClaseLicencia", "ClaseLicencia", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Numero", "Numero", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "VigenciaLicencia", "VigenciaLicencia", BoFieldTypes.db_Date, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "OperadorID", "OperadorID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AvisoLicencia", "AvisoLicencia", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Estatus", "Estatus", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UUID", "UUID", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateCurso()
        {
            try
            {
                const string tablename = "VSKF_Curso";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Vehiculos
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Curso");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Fecha", "Fecha", BoFieldTypes.db_Date, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpresaOtorga", "EmpresaOtorga", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Lugar", "Lugar", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "OperadorID", "OperadorID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UUID", "UUID", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateObservacion()
        {
            try
            {
                const string tablename = "VSKF_Observacion";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Observacion
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Observacion");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Descripcion", "Descripcion", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FechaRegistro", "FechaRegistro", BoFieldTypes.db_Date, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FechaObservacion", "FechaObservacion", BoFieldTypes.db_Date, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "OperadorID", "OperadorID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                //GC.Collect();
                //AddUDT.CreateUDF(tablename, "Estatus", "Estatus", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UUID", "UUID", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateInfracciones()
        {
            try
            {
                const string tablename = "VSKF_Infracciones";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Observacion

                GC.Collect();
                AddUDT.CreateUDT(tablename, "Infracciones");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "InfraccionID", "InfraccionID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoInfraccionID", "TipoInfraccionID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "NombreTipoInfraccion", "NombreTipoInfraccion", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "OperadorID", "OperadorID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "VehiculoID", "VehiculoID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "NombreVehiculo", "NombreVehiculo", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FechaInfraccion", "FechaInfracccion", BoFieldTypes.db_Date, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Archivo", "Archivo", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Observacion", "Observacion", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "NombreArchivo", "NombreArchivo", BoFieldTypes.db_Alpha, 150, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoArchivo", "TipoArchivo", BoFieldTypes.db_Alpha, 150, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Folio", "Folio", BoFieldTypes.db_Alpha, 50, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Hora", "Hora", BoFieldTypes.db_Alpha, 50, BoYesNoEnum.tNO);
               

                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateTiposInfracciones()
        {
            try
            {
                const string tablename = "VSKF_TipoInfr";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Observacion

                GC.Collect();
                AddUDT.CreateUDT(tablename, "TiposInfraccion");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoInfrID", "TipoInfraccionID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Nombre", "Nombre", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpresaID", "EmpresaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Alertas
        private void CreateNotificaciones() //hay que verificar la version de Carlos para esta tabla
        {
            try
            {
                const string tablename = "VSKF_NOTIFICACION";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Notificaciones
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Notificacion KananFleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "NotificacionID", "NotificacionID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoNotificableID", "TipoNotificableID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpresaID", "EmpresaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SucursalID", "SucursalID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "VehiculoID", "VehiculoID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Correos", "Correos", BoFieldTypes.db_Memo, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Celulares", "Celulares", BoFieldTypes.db_Memo, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UUID", "UUID", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateAlertaTable()
        {
            try
            {
                const string tablename = "VSKF_ALERTA";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Alerta
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Alertas KananFleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AlertaID", "AlertaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Descripcion_Es", "Descripcion_Es", BoFieldTypes.db_Memo, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Descripcion_En", "Descripcion_En", BoFieldTypes.db_Memo, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Descripcion_Pt", "Descripcion_Pt", BoFieldTypes.db_Memo, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Descripcion_Fr", "Descripcion_Fr", BoFieldTypes.db_Memo, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UsuarioID", "UsuarioID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpresaID", "EmpresaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SucursalID", "SucursalID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EstaActivo", "EstaActivo", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "IAlertaID", "IAlertaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoAlerta", "TipoAlerta", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TypeOfType", "TypeOfType", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "NumEnviados", "NumEnviados", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "NumEnviadosTiempo", "NumEnviadosTiempo", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "OrdenServicioID", "OrdenServicioID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "RutinaID", "RutinaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UUID", "UUID", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateAlertaMantenimientoTable()
        {
            try
            {
                const string tablename = "VSKF_ALERTAMTTO";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Alerta Mantenimiento
                GC.Collect();
                AddUDT.CreateUDT(tablename, "AlertasMtto KananFleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AlertaMttoID", "AlertaMttoID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ServicioID", "ServicioID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpresaID", "EmpresaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoAlerta", "TipoAlerta", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TypeOfType", "TypeOfType", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "MantenibleID", "MantenibleID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoMantenibleID", "TipoMantenibleID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UUID", "UUID", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /*SE CREA TABLA PARA ALMACENAR RUTINAS VSKF_ALERTARUTINA*/
        private void CreateAlertaRutinaTable()
        {
            try
            {
                const string tablename = "VSKF_ALERTARUTINA";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Alerta RUTINA
                GC.Collect();
                AddUDT.CreateUDT(tablename, "AlertasRutina KananFleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AlertaRutinaID", "AlertaRutinaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ServicioID", "ServicioID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "VehiculoID", "VehiculoID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "EmpresaID", "EmpresaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoAlerta", "TipoAlerta", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TypeOfType", "TypeOfType", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "MantenibleID", "MantenibleID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoMantenibleID", "TipoMantenibleID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateParametroServicio()
        {
            try
            {
                const string tablename = "VSKF_PMTROSERVICIO";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Parametro Servicio
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Pmtro servicio KananFleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "PServID", "PServID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SucursalID", "SucursalID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ServicioID", "ServicioID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TMantbleID", "TMantbleID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "MantbleID", "MantbleID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TVeiculoID", "TVeiculoID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Valor", "Valor", BoFieldTypes.db_Float, 10, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Mensaje", "Mensaje", BoFieldTypes.db_Alpha, 252, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Alerta", "Alerta", BoFieldTypes.db_Float, 10, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ValTiempo", "ValTiempo", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AlTiempo", "AlTiempo", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UUID", "UUID", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ValHora", "ValHora", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AlHora", "AlHora", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Rutina", "Rutina", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateParamXTipoVehiculo()
        {
            try
            {
                const string tablename = "VSKF_PARAMTVEHICULO";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Parametro Servicio
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Parametrizacion por tipo vehic");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Tipo", "Tipo parametrizacion", BoFieldTypes.db_Numeric, 1, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Mod", "Modo de parametrizacion", BoFieldTypes.db_Numeric, 1, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "Frecuencia", "Frecuencia para servicio", BoFieldTypes.db_Float, 9, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AlertaPSer", "Alerta para servicio", BoFieldTypes.db_Float, 9, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                
                
                GC.Collect();
                AddUDT.CreateUDF(tablename, "RutinaID", "Identificador de rutina", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ServicioiD", "Identificador para servicio", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();


                AddUDT.CreateUDF(tablename, "FrechServ", "Frecuencia de hora para servicio", BoFieldTypes.db_Float, 9, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FrecHAlert", "Frecuencia de horas servicio", BoFieldTypes.db_Float, 9, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();


                AddUDT.CreateUDF(tablename, "TipoVehiculo", "Define tipo de vehiculo a parametrizar", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateParametroMantenimiento()
        {
            try
            {
                const string tablename = "VSKF_PMTROMTTO";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Parametro Mantenimiento
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Pmtro Mantto KananFleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "PMtoID", "PMtoID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UltServ", "UltServ", BoFieldTypes.db_Float, 10, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ProxServ", "ProxServ", BoFieldTypes.db_Float, 10, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AlProxServ", "AlProxServ", BoFieldTypes.db_Float, 10, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ServicioID", "ServicioID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "MantbleID", "MantbleID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UlAlEnviad", "UlAlEnviad", BoFieldTypes.db_Date, 250, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "PServID", "PServID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UlSvTiem", "UlSvTiem", BoFieldTypes.db_Date, 250, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ProxSvTiem", "ProxSvTiem", BoFieldTypes.db_Date, 250, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AlProSvTie", "AlProSvTie", BoFieldTypes.db_Date, 250, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TMantbleID", "TMantbleID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TipoPmtro", "TipoPmtro", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UUID", "UUID", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ValHora", "ValHora", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AlHora", "AlHora", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UltServHora", "UltServHora", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ProxServHora", "ProxServHora", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AlProxServHora", "AlProxServHora", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Rutina", "Rutina", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Configuracion Comentado
        /*
        private void CreaTablaConfiguracion()
        {

            //Funcion: Metodo que crea la tabla de Configuracion y los campos requeridos

            try
            {
                string tablename = "VSKF_CONFIGURACION";

                GC.Collect();
                AddUDT.CreateUDT(tablename, "Configuraci�nes de  KananFleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sucursal", "Centro de costo Sucursal", BoFieldTypes.db_Alpha, 1, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Vehiculos", "Centro de costo Vehiculos", BoFieldTypes.db_Alpha, 1, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "CodigoSucursal", "Codigo de Sucursal", BoFieldTypes.db_Alpha, 15, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "CodigoVehiculo", "Codigo de  vehiculo", BoFieldTypes.db_Alpha, 15, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SBO", "Control inv. SBO", BoFieldTypes.db_Alpha, 1, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "KANAN", "Control inv. Kanan", BoFieldTypes.db_Alpha, 1, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AlmacenRecarga", "Utiliza alm. recarga", BoFieldTypes.db_Alpha, 1, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "CodigoAlmacen", "Cod. alm. recarga", BoFieldTypes.db_Alpha, 15, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Cuenta", "Cta. gastos combustible", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Articulo", "Clave de inventario", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "Dimension", "Centro Costo Dimension", BoFieldTypes.db_Numeric, 2, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Servidor", "Servidor de correo", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "Puerto", "Puerto de salida de correo", BoFieldTypes.db_Alpha, 10, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "Correo", "Cuenta de correo", BoFieldTypes.db_Alpha, 200, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "Password", "Contrase�a de la cuenta", BoFieldTypes.db_Alpha, 250, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "ServidorSSL", "Determina si requiere SSL", BoFieldTypes.db_Alpha, 2, BoYesNoEnum.tNO);
                GC.Collect();
               
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
         */
        #endregion Configuracion Comentado

        #region Configuracion
        private void CreaTablaConfiguracion()
        {
            try
            {
                string tablename = "VSKF_CONFIGURACION";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region TableConfiguration
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Configuraci�nes de  KananFleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Sucursal", "Centro de costo Sucursal", BoFieldTypes.db_Alpha, 1, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Vehiculos", "Centro de costo Vehiculos", BoFieldTypes.db_Alpha, 1, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "CodigoSucursal", "Codigo de Sucursal", BoFieldTypes.db_Alpha, 15, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "CodigoVehiculo", "Codigo de  vehiculo", BoFieldTypes.db_Alpha, 15, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SBO", "Control inv. SBO", BoFieldTypes.db_Alpha, 1, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "KANAN", "Control inv. Kanan", BoFieldTypes.db_Alpha, 1, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AlmacenRecarga", "Utiliza alm. recarga", BoFieldTypes.db_Alpha, 1, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "CodigoAlmacen", "Cod. alm. recarga", BoFieldTypes.db_Alpha, 15, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Cuenta", "Cta. gastos combustible", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Articulo", "Clave de inventario", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Dimension", "Centro Costo Dimension", BoFieldTypes.db_Numeric, 2, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Servidor", "Servidor de correo", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Puerto", "Puerto de salida de correo", BoFieldTypes.db_Alpha, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Correo", "Cuenta de correo", BoFieldTypes.db_Alpha, 200, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Password", "Contrase�a de la cuenta", BoFieldTypes.db_Alpha, 250, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ServidorSSL", "Determina si requiere SSL", BoFieldTypes.db_Alpha, 2, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "CuentaCP", "Cuenta de servicio de CP", BoFieldTypes.db_Alpha, 15, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AcctAnticipo", "Cuenta de anticipos", BoFieldTypes.db_Alpha, 15, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AcctContra", "Cuenta contraparte anticipo", BoFieldTypes.db_Alpha, 15, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Borrador", "Factura preliminar", BoFieldTypes.db_Alpha, 2, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Clientes", "Determina si el centro de costo es cliente", BoFieldTypes.db_Alpha, 1, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "PagoAnticipo", "Determina si el pago op es por anticipo", BoFieldTypes.db_Alpha, 1, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FacturaOS", "Indica el estatus para crear factura en OS", BoFieldTypes.db_Numeric, 2, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Draftos", "Determina si la factura OS es preeliminar", BoFieldTypes.db_Numeric, 2, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "NACT_GASTO", "Nombre de cuenta de gasto combustible", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "NACT_SERVICP", "Nombre cuenta de servicio carta porte", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "NACT_ANTICIPOP", "Nombre de cuenta de anticipo a operador", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "NACT_CONTRAOP", "Nombre de cuenta contrapartida al operador", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SALIDAEST", "Indica el estatus para crear sal de inv", BoFieldTypes.db_Numeric, 2, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "DRAFTSI", "Determina si la sal de inv es preeliminar", BoFieldTypes.db_Numeric, 2, BoYesNoEnum.tNO);
                GC.Collect();
                #endregion TableConfiguration
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreaTablaConceptosFuelData()
        {
            try
            {
                string tablename = "VSKF_CONCEPTOFUEL";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);             
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Conceptos de  Fuel Data");
                GC.Collect();                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateClientTable()
        {
            try
            {
                string tablename = "VSKF_Cliente";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Proveedores
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Clientes Kananfleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ClienteID", "ClienteID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "CardCode", "CardCode", BoFieldTypes.db_Alpha, 15, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Nombre", "Nombre", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "NombreExt", "NombreExt", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "RFC", "RFC", BoFieldTypes.db_Alpha, 32, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Telefono", "Telefono", BoFieldTypes.db_Alpha, 20, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "TelMovil", "TelMovil", BoFieldTypes.db_Alpha, 50, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Correo", "Correo", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpresaID", "EmpresaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EsActivo", "EsActivo", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateCP_Impuestos()
        {
            try
            {
                string tablename = "VSKF_CP_IMPUESTOS";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Impuestos Kananfleet-SAP");
                GC.Collect();
                
                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void CreateCP_Anticipos()
        {
            try
            {
                string tablename = "VSKF_CP_ANTICIPOS";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Anticipos Kananfleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "CodeCP", "C�digo de carta porte", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Fecha", "Fecha de creaci�n", BoFieldTypes.db_Date, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Importe", "Fecha de creaci�n", BoFieldTypes.db_Float, 100, BoYesNoEnum.tNO, BoFldSubTypes.st_Price);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "AnticipoID", "Identificador del registro", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void CreateCP_ConceptosGastos()
        {
            try
            {
                string tablename = "VSKF_CP_CONCEPTOGTO";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Conceptos Gtos Kananfleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "CuentaMayor", "C�digo de carta porte", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateCP_Gastos()
        {
            try
            {
                string tablename = "VSKF_CP_GASTOS";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Conceptos Gtos Kananfleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "CodeGasto", "C�digo de carta porte", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Fecha", "Fecha de registro", BoFieldTypes.db_Date, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Impuesto", "C�digo de impuesto", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Importe", "Importe de gasto", BoFieldTypes.db_Float, 100, BoYesNoEnum.tNO,BoFldSubTypes.st_Price);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Total", "Total	del	gasto", BoFieldTypes.db_Float, 100, BoYesNoEnum.tNO, BoFldSubTypes.st_Price);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "CodeCP", "Folio de carta de porte", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "GastoID", "Identificador de registro", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateCP_OtrosGastos()
        {
            try
            {
                string tablename = "VSKF_CP_OTROSGTOS";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Conceptos Gtos Kananfleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "CodeCP", "C�digo de carta porte", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Fecha", "Fecha de registros", BoFieldTypes.db_Date, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SueldoTrbajador", "Sueldo Empleado", BoFieldTypes.db_Float, 100, BoYesNoEnum.tNO, BoFldSubTypes.st_Price);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Porcentaje", "C�digo de carta porte", BoFieldTypes.db_Float, 100, BoYesNoEnum.tNO,BoFldSubTypes.st_Price);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Descuento", "C�digo de carta porte", BoFieldTypes.db_Float, 100, BoYesNoEnum.tNO, BoFldSubTypes.st_Price);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Importe", "C�digo de carta porte", BoFieldTypes.db_Float, 100, BoYesNoEnum.tNO, BoFldSubTypes.st_Price);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Foliogasto", "C�digo de carta porte", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        #endregion Configuracion

        #region OrdenServicio_Refacciones
        private void CreaTablaOrdeServicioRefacciones()
        {

            /*Funcion: Metodo que crea la tabla de Configuracion y los campos requeridos
             * 
             * 
             * 
             * */
            try
            {
                string tablename = "VSKF_DTLLEREFACCION";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Detalle refacciones");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "RefaccionLinea", "Numero consecutivo registro", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "MantenibleID", "MantenibleID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "ServicioID", "ServicioID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "OrdenServicioID", "OrdenServicioID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "TipoMantenibleID", "TipoMantenibleID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "Costo", "Costo", BoFieldTypes.db_Float, 254, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "Articulo", "Articulo", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "Cantidad", "Cantidad", BoFieldTypes.db_Float, 20, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "Total", "Total", BoFieldTypes.db_Float, 254, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "Sincronizado", "Sincronizado", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "Almacen", "Almacen", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "CodAlmcn", "CodAlmcn", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                GC.Collect();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion OrdenServicio_Refacciones

        #region OrdenServicio_CostoAdicional
        private void CreaTablaOrdenServicioCostoAdicional()
        {

            try
            {
                string tablename = "VSKF_OSCOSTOSADICIO";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Costos adicional");
                GC.Collect();

                AddUDT.CreateUDF(tablename, "OrdenServico", "OrdenServico", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "Descripcion", "Descripcion", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "Costo", "Costo", BoFieldTypes.db_Float, 254, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion OrdenServicio_CostoAdicional

        #region Ordenes de servicio herramientas
        private void CreaTablaOrdenServicioHerramientas()
        {

            try
            {
                string tablename = "VSKF_HERRAMIENTAOS";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Articulos tipo herramientaKF");
                GC.Collect();


                AddUDT.CreateUDF(tablename, "Folio", "Folio dato maestro", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "Descripcion", "Descripcion dato maestro", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "Cantidad", "Cantidad de herramientas", BoFieldTypes.db_Float, 254, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "OrdenServicioID", "Folio de orden servicio", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion Ordenes de servicio herramientas

        #region Ordenes de servicio mecanicos
        private void CreaTablaOrdenServicioMecanicos()
        {

            try
            {
                string tablename = "VSKF_MECANICOSOS";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Empleados rh con rol mecanicos");
                GC.Collect();

                AddUDT.CreateUDF(tablename, "Empleadoid", "Empleado id rh", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "Nombre", "Nombre completo empleado", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "Sueldo", "Sueldo de empleado", BoFieldTypes.db_Float, 254, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();

                AddUDT.CreateUDF(tablename, "OrdenServicioID", "Folio de orden servicio", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion  Ordenes de servicio mecanicos

        #region CartaPorte
        private void CreaTablaCartaPorte()
        {
            try
            {
                string tablename = "VSKF_CARTADEPORTE";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region TableCartaPorte
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Carta porte de KananFleet");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "RutaOrigen", "Ruta de origen", BoFieldTypes.db_Alpha, 150, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "RutaDestino", "Ruta destino", BoFieldTypes.db_Alpha, 150, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Vehiculo", "Vehiculo", BoFieldTypes.db_Alpha, 50, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Operador", "OPerador", BoFieldTypes.db_Alpha, 15, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Observaciones", "Observaciones", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "DocFactura", "DocEntry Fact.", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "NumViaje", "Numero de Viaje.", BoFieldTypes.db_Alpha, 20, BoYesNoEnum.tNO);
                GC.Collect();
                #endregion TableCartaPorte
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreaTablaCartaDetalle()
        {
            try
            {
                string tablename = "VSKF_CARTADETALLE";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region TableDetalleCartaPote
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Detalle de Carta porte");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "ItemCode", "Codigo Articulo", BoFieldTypes.db_Alpha, 20, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Descripcion", "Descripcion Articulo", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Cantidad", "Cantidad viaje Art.", BoFieldTypes.db_Float, 15, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Bulto", "Bulto viaje Art.", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Carga", "Carga viaje Art.", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Peso", "Peso viaje Art.", BoFieldTypes.db_Alpha, 100, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "IDCarta", "Asociado a carta.", BoFieldTypes.db_Alpha, 30, BoYesNoEnum.tNO);
                GC.Collect();
                #endregion TableDetalleCartaPorte
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion CartaPorte

        #region CambioEstatusOrdenServicio
        private void CambiosEstatusOrdenServicio()
        {
            try
            {
                string tablename = "VSKF_CAMBESTATUS";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                GC.Collect();
                AddUDT.CreateUDT(tablename, "CambioEstatus ");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Estatus", "Estatus", BoFieldTypes.db_Numeric, 7, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FechaCambio", "FechaCambio", BoFieldTypes.db_Date, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "HoraMinuto", "HoraMinuto", BoFieldTypes.db_Numeric, 7, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "IDorSer", "IDorSer", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion CambioEstatusOrdenServicio

        #region Gestoria
        private void CreateTableGestoriaPlacas()
        {
            try
            {
                string tablename = "VSKF_GESPLA";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Campos
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Gestor�aPlacas");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "GestoriaID", "GestoriaID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "VehiculoID", "VehiculoID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                //GC.Collect();
                //AddUDT.CreateUDF(tablename, "TipoGestion", "TipoGestion", BoFieldTypes.db_Numeric, 4, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpresaID", "EmpresaID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SucursalID", "SucursalID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Fecha", "Fecha", BoFieldTypes.db_Date, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FechaRegistro", "FechaRegistro", BoFieldTypes.db_Date, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Numero", "Numero", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Observacion", "Observacion", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "MotivoBajaID", "MotivoBajaID", BoFieldTypes.db_Numeric, 4, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Especifique", "Especifique", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "DocID", "DocID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Otro", "Otro", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "MotivoBaja", "Nombre de motivo baja", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Evidencia", "Documento de evidencia", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                #endregion Campos
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateTableGestoriaPropietario()
        {
            try
            {
                string tablename = "VSKF_GESPRO";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Campos
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Gestor�aPropietario");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "GestoriaID", "GestoriaID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "VehiculoID", "VehiculoID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                //GC.Collect();
                //AddUDT.CreateUDF(tablename, "TipoGestion", "TipoGestion", BoFieldTypes.db_Numeric, 4, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpresaID", "EmpresaID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SucursalID", "SucursalID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Fecha", "Fecha", BoFieldTypes.db_Date, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FechaRegistro", "FechaRegistro", BoFieldTypes.db_Date, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Numero", "Numero", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Observacion", "Observacion", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "MotivoBajaID", "MotivoBajaID", BoFieldTypes.db_Numeric, 4, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Especifique", "Especifique", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "DocID", "DocID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Otro", "Otro", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "MotivoBaja", "Nombre de motivo baja", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Evidencia", "Documento de evidencia", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                #endregion Campos
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateTableGestoriaTarjeta()
        {
            try
            {
                string tablename = "VSKF_GESTAR";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Campos
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Gestor�aTarjeta");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "GestoriaID", "GestoriaID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "VehiculoID", "VehiculoID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                //GC.Collect();
                //AddUDT.CreateUDF(tablename, "TipoGestion", "TipoGestion", BoFieldTypes.db_Numeric, 4, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpresaID", "EmpresaID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SucursalID", "SucursalID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Fecha", "Fecha", BoFieldTypes.db_Date, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FechaRegistro", "FechaRegistro", BoFieldTypes.db_Date, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Numero", "Numero", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Observacion", "Observacion", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "MotivoBajaID", "MotivoBajaID", BoFieldTypes.db_Numeric, 4, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Especifique", "Especifique", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "DocID", "DocID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Otro", "Otro", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "MotivoBaja", "Nombre de motivo baja", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Evidencia", "Documento de evidencia", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                #endregion Campos
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateTableGestoriaAlta()
        {
            try
            {
                string tablename = "VSKF_GESALTA";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Campos
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Gestor�aAlta");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "GestoriaID", "GestoriaID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "VehiculoID", "VehiculoID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                //GC.Collect();
                //AddUDT.CreateUDF(tablename, "TipoGestion", "TipoGestion", BoFieldTypes.db_Numeric, 4, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpresaID", "EmpresaID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SucursalID", "SucursalID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Fecha", "Fecha", BoFieldTypes.db_Date, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FechaRegistro", "FechaRegistro", BoFieldTypes.db_Date, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Numero", "Numero", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Observacion", "Observacion", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "MotivoBajaID", "MotivoBajaID", BoFieldTypes.db_Numeric, 4, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Especifique", "Especifique", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "DocID", "DocID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Otro", "Otro", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "MotivoBaja", "Nombre de motivo baja", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Evidencia", "Documento de evidencia", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                #endregion Campos
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateTableGestoriaBaja()
        {
            try
            {
                string tablename = "VSKF_GESBAJA";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region Campos
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Gestor�a Baja");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "GestoriaID", "GestoriaID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "VehiculoID", "VehiculoID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                //GC.Collect();
                //AddUDT.CreateUDF(tablename, "TipoGestion", "TipoGestion", BoFieldTypes.db_Numeric, 4, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpresaID", "EmpresaID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "SucursalID", "SucursalID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Fecha", "Fecha", BoFieldTypes.db_Date, 20, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FechaRegistro", "FechaRegistro", BoFieldTypes.db_Date, 20, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Numero", "Numero", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Observacion", "Observacion", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "MotivoBajaID", "MotivoBajaID", BoFieldTypes.db_Numeric, 4, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Especifique", "Especifique", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "DocID", "DocID", BoFieldTypes.db_Numeric, 11, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Otro", "Otro", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "MotivoBaja", "Nombre de motivo baja", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Evidencia", "Documento de evidencia", BoFieldTypes.db_Alpha, 254, BoYesNoEnum.tNO);
                GC.Collect();
                #endregion Campos
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion Gestoria

        #region PagosConceptos
        private void CreatePagosConceptos()
        {
            try
            {
                const string tablename = "VSKF_PAGOSCONCEPTOS";

                #region PagosConceptos
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Conceptos de Pagos");
                #endregion PagosConceptos
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion PagosConceptos

        #region UnidadMedida
        private void CreateUnidadMedida()
        {
            try
            {
                const string tablename = "VSKF_UNIDADMEDIDA";
                this.EscribirLogInstalacion("Creando tabla: " + tablename);
                #region UnidadMedida
                GC.Collect();
                AddUDT.CreateUDT(tablename, "Unidades de medida");
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UnidadMedidaID", "UnidadMedidaID", BoFieldTypes.db_Alpha, 250, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Nombre", "Nombre", BoFieldTypes.db_Alpha, 250, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Longitud", "Longitud", BoFieldTypes.db_Float, 20, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Ancho", "Ancho", BoFieldTypes.db_Float, 20, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Altura", "Altura", BoFieldTypes.db_Float, 20, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Volumen", "Volumen", BoFieldTypes.db_Float, 20, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "UMVol", "UMVol", BoFieldTypes.db_Float, 20, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Peso", "Peso", BoFieldTypes.db_Float, 20, BoYesNoEnum.tNO, BoFldSubTypes.st_Quantity);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "EmpresaID", "EmpresaID", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "FechaModificacion", "FechaModificacion", BoFieldTypes.db_Date, 254, BoYesNoEnum.tNO);
                GC.Collect();
                AddUDT.CreateUDF(tablename, "Activo", "Activo", BoFieldTypes.db_Numeric, 10, BoYesNoEnum.tNO);
                #endregion UnidadMedida
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion UnidadMedida

        #region Button 1
        public void button1_Click(object sender, EventArgs e)
        {
            BuscarArchivo();
            LeerArchivo();

        }
        #endregion

        #region Button 2
        public void button2_Click(object sender, EventArgs e)
        {
            //GuardarArchivo();
            GuardarArchivo();
        }
        #endregion

        #region Buscar archivo
        public void BuscarArchivo()
        {
            try
            {
                Stream myStream = null;
                openFileDialog2.InitialDirectory = "c:\\";
                openFileDialog2.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog2.FilterIndex = 2;
                openFileDialog2.RestoreDirectory = true;

                if (openFileDialog2.ShowDialog() == DialogResult.OK)
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            //Proceso de lectura...
                            name = openFileDialog1.FileName;
                            file = new StreamReader(name);
                            //file.Close();
                        }
                    }
                }
                textBox1.Text = name;
            }
            catch (Exception SeleccionarArchivoError)
            {
                throw new Exception("Error al seleccionar el archivo: " + SeleccionarArchivoError);
            }
        }

        //private void LanzaEjecutable()
        //{
        //    AssemblyName Name = new AssemblyName("WindowsFormApplication1");
        //    Type t = typeof(WindowsFormsApplication11.Form1);
        //    Assembly a = t.Assembly;
        //    a.EntryPoint.Invoke(null, null);
        //}
        #endregion

        #region Leer archivo
        public void LeerArchivo()
        {
            try
            {
                //textBox1.Text = "C:/csv.txt";
                //name = textBox1.Text;

                name = "C:/" + textBox1.Text + ".csv";
                file = new StreamReader(name);
                if (textBox1.Text == null || textBox1.Text != "")
                {
                    while (nLine != null)
                    {
                        nLine = file.ReadLine();
                        //bool EmpiezaConPuntoYComa = nLine.Trim().StartsWith(";");
                        if (nLine != null)
                        {
                            if (!nLine.StartsWith(";"))
                            {
                                DatosDeTabla.Add(nLine);
                            }
                            else
                            {
                                OtrosDatos.Add(nLine);
                            }
                        }
                    }
                    //file.Close();
                    MessageBox.Show("Archivo cargado correctamente.");
                }
                else
                {
                    MessageBox.Show("Seleccione un archivo antes de intentar leerlo.");
                }
            }
            catch (Exception LeerArchivoError)
            {
                throw new Exception("Error al leer el archivo: " + LeerArchivoError);
            }
        }
        #endregion

        #region Escribir archivo
        public void EscribeArchivo()
        {
            //string path = @"c:\temp.txt";
            String cont = NumSig.ToString();
            String path = Application.StartupPath + "\\temp-" + cont + ".txt";

            if (File.Exists(path))
            {
                try { File.Delete(path); }
                catch { NumSig = NumSig + 1; }
            }

            // Crea un archivo para escribir.
            using (StreamWriter sw = File.CreateText(path))
            {
                sw.WriteLine(" Credenciales ");
                sw.WriteLine("--------------");
                sw.WriteLine("Tipo de BD: " + this.DBType);
                sw.WriteLine("UserDB: " + this.UserDB);
                sw.WriteLine("PassDB: " + this.PassDB);
                sw.WriteLine("Society: " + this.CompanySAP);
                sw.WriteLine("UserSAP: " + this.UserSAP);
                sw.WriteLine("PassSAP: " + this.PassSAP);
            }

        }
        #endregion Escribir archivo

        #region Guardar Archivo
        public void GuardarArchivo()
        {
            //Add agregar = new Add();
            //String Tabla = textBox1.Text;
            //try
            //{
            //    switch (Tabla)
            //    {
            //        case "Operador":
            //            agregar.Operadores(DatosDeTabla);
            //            MessageBox.Show("Los operadores se insertaron correctamente.");
            //            break;
            //        case "Sucursal":
            //            agregar.Sucursales(DatosDeTabla);
            //            MessageBox.Show("Las sucursales se insertaron correctamente.");
            //            break;
            //        case "Empleado":
            //            agregar.Empleados(DatosDeTabla);
            //            MessageBox.Show("Los empleados se insertaron correctamente.");
            //            break;
            //        case "Vehiculo":
            //            agregar.Vehiculos(DatosDeTabla);
            //            MessageBox.Show("Los vehiculos se insertaron correctamente.");
            //            break;
            //        case "Llanta":
            //            agregar.Llantas(DatosDeTabla);
            //            MessageBox.Show("Las llantas se insertaron correctamente.");
            //            break;
            //        case "InventarioArticulo":
            //            agregar.InventarioArticulos(DatosDeTabla);
            //            MessageBox.Show("Los articulos de inventario se insertaron correctamente.");
            //            break;
            //    }
            //}
            //catch (Exception GuardarArchivoError)
            //{
            //    throw new Exception("Error al guardar los datos del archivo: " + GuardarArchivoError);
            //}
        }
        #endregion Guardar Archivo

        private void EscribirLogInstalacion(String sMensaje)
        {

            String sFecha;
            String sHora;

            try
            {
                sFecha = DateTime.Today.ToString("yyyyMMdd");
                sHora = DateTime.Now.ToString("HH:mm");

                if (this.bCrearNuevoLogInstalacion)
                {
                    this.sNombreNuevoLog = "KF_Registrodeinstalacion" + sFecha + DateTime.Now.ToString("HHmmss") + ".txt";
                    this.bCrearNuevoLogInstalacion = false;
                }

                File.AppendAllText(this.sRutaAplicacion + @"\" + this.sNombreNuevoLog,
                    sHora + "\t" + sMensaje + Environment.NewLine + Environment.NewLine);
            }
            catch (Exception oError)
            {
                this.GuardarError(oError.ToString());
                throw new Exception(oError.Message);

            }
        }

        private void GuardarError(String sError)
        {


            String sFecha;
            String sHora;

            try
            {
                sFecha = DateTime.Today.ToString("yyyyMMdd");
                sHora = DateTime.Now.ToString("HH:mm");

                if (!Directory.Exists(this.sRutaAplicacion + @"\Errores"))
                {
                    Directory.CreateDirectory(this.sRutaAplicacion + @"\Errores");
                }

                File.AppendAllText(this.sRutaAplicacion + @"\Errores\" + sFecha + ".txt",
                    sHora + "\t" + sError + Environment.NewLine + Environment.NewLine);
            }
            catch
            {
            }
        }

        private void Frame2_Enter(object sender, EventArgs e)
        {

        }

        private void Text2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
