﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project1;
using SAPbobsCOM;
using SAPinterface.Data.tools;

namespace KananaSAP.DI.UDT
{
    sealed class AddUDT
    {
        static SAPbobsCOM.UserTablesMD oUserTablesMD;
        static SAPbobsCOM.UserFieldsMD oUserFieldsMD;

        public static void CreateUDT(string TableName, string Description)
        {
            try
            {

                oUserTablesMD = (UserTablesMD)globals_Renamed.oCompany.GetBusinessObject(BoObjectTypes.oUserTables);
                TableName = TableName.ToUpper();
                if (!oUserTablesMD.GetByKey(TableName)) // si ya existe la tabla continua
                {

                    oUserTablesMD.TableName = TableName;
                    oUserTablesMD.TableDescription = Description;
                    oUserTablesMD.TableType = BoUTBTableType.bott_NoObject;
                    globals_Renamed.lRetCode = oUserTablesMD.Add();

                    //// check for errors in the process
                    if (globals_Renamed.lRetCode != 0)
                    {
                        int temp_int = globals_Renamed.lErrCode;
                        string temp_string = globals_Renamed.sErrMsg;
                        globals_Renamed.oCompany.GetLastError(out temp_int, out temp_string);
                        throw new Exception(temp_string);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(oUserTablesMD);
                oUserTablesMD = null;
                GC.Collect();
            }
        }

        public static void CreateUDF(string TableName, string Name, string Description, BoFieldTypes Type, int EditSize, BoYesNoEnum mandatory)
        {
            string sConsulta, sLogmssge = string.Empty;
            Recordset oDatos = null;


            try
            {
                TableName = TableName.ToUpper();
                Name = Name.ToUpper();

                oUserFieldsMD = (SAPbobsCOM.UserFieldsMD)globals_Renamed.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields);

                sConsulta = "SELECT     COUNT(C.\"AliasID\") CUENTA " +
                            "FROM       CUFD C " +
                            "WHERE      C.\"AliasID\" = '" + Name + "' " +
                            "           AND C.\"TableID\" IN ('" + TableName + "'," +
                            "                             '@" + TableName + "') ";

                oDatos = (Recordset)globals_Renamed.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                oDatos.DoQuery(sConsulta);

                if (Type == BoFieldTypes.db_Numeric)
                    EditSize = EditSize > 11 ? 11 : EditSize;

                if (Type == BoFieldTypes.db_Alpha)
                    EditSize = EditSize > 254 ? 254 : EditSize;

                if (Convert.ToInt32(oDatos.Fields.Item("CUENTA").Value) == 0)
                {
                    sLogmssge = string.Format("Creando campo {0}. En tabla {1}. Descripcion: {2}", Name, TableName, Description);
                    tools.LogProceso("CreateUDF", sLogmssge);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oDatos);
                    oDatos = null;
                    GC.Collect();

                    oUserFieldsMD.TableName = TableName;
                    oUserFieldsMD.Name = Name;
                    oUserFieldsMD.Description = Description;
                    oUserFieldsMD.Type = Type;
                    //if (SubType != null)
                    //    oUserFieldsMD.SubType = (SAPbobsCOM.BoFldSubTypes)SubType;
                    oUserFieldsMD.EditSize = EditSize;
                    oUserFieldsMD.Mandatory = mandatory;

                    //// Adding the Field to the Table
                    globals_Renamed.lRetCode = oUserFieldsMD.Add();

                    //// Check for errors
                    if (globals_Renamed.lRetCode != 0)
                    {
                        int temp_int = globals_Renamed.lErrCode;
                        string temp_string = globals_Renamed.sErrMsg;
                        globals_Renamed.oCompany.GetLastError(out temp_int, out temp_string);
                        throw new Exception(temp_string);
                    }
                }
            }
            catch (Exception ex)
            {
                tools.LogError("CreateUDF", string.Format("Cadena {0}. ERROR {1}", sLogmssge, ex.Message));
                throw new Exception(ex.Message);
            }
            finally
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(oUserFieldsMD);
                oUserFieldsMD = null;
                GC.Collect();
            }
        }

        public static void CreateUDF(string TableName, string Name, string Description, BoFieldTypes Type, int EditSize, BoYesNoEnum mandatory, BoFldSubTypes SubType)
        {
            string sConsulta;
            Recordset oDatos = null;

            try
            {
                oUserFieldsMD = (SAPbobsCOM.UserFieldsMD)globals_Renamed.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields);
                TableName = TableName.ToUpper();
                Name = Name.ToUpper();

                sConsulta = "SELECT     COUNT(C.\"AliasID\") CUENTA " +
                           "FROM       CUFD C " +
                           "WHERE      C.\"AliasID\" = '" + Name + "' " +
                           "           AND C.\"TableID\" IN ('" + TableName + "'," +
                           "                             '@" + TableName + "') ";

                oDatos = (Recordset)globals_Renamed.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                oDatos.DoQuery(sConsulta);
                if (Type == BoFieldTypes.db_Numeric)
                    EditSize = EditSize > 11 ? 11 : EditSize;

                if (Type == BoFieldTypes.db_Alpha)
                    EditSize = EditSize > 254 ? 254 : EditSize;

                if (Convert.ToInt32(oDatos.Fields.Item("CUENTA").Value) == 0)
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oDatos);
                    oDatos = null;
                    GC.Collect();

                    oUserFieldsMD.TableName = TableName;
                    oUserFieldsMD.Name = Name;
                    oUserFieldsMD.Description = Description;
                    oUserFieldsMD.Type = Type;
                    oUserFieldsMD.SubType = SubType;
                    oUserFieldsMD.EditSize = EditSize;
                    oUserFieldsMD.Mandatory = mandatory;

                    //// Adding the Field to the Table
                    globals_Renamed.lRetCode = oUserFieldsMD.Add();

                    //// Check for errors
                    if (globals_Renamed.lRetCode != 0)
                    {
                        int temp_int = globals_Renamed.lErrCode;
                        string temp_string = globals_Renamed.sErrMsg;
                        globals_Renamed.oCompany.GetLastError(out temp_int, out temp_string);
                        throw new Exception(temp_string);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(oUserFieldsMD);
                oUserFieldsMD = null;
                GC.Collect();
            }
        }
    }
}
