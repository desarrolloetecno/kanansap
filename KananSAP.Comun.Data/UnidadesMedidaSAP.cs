﻿#region C#
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion C#

#region Kananfleet
using Kanan.Comun.BO2;
#endregion Kananfleet

#region SAP
using SAPbobsCOM;
#endregion SAP


namespace KananSAP.Comun.Data
{
    public class UnidadesMedidaSAP
    {
        #region Atributos
        public UnidadMedida unidad { get; set; }
        public List<UnidadMedida> ListOld { get; set; }
        public List<UnidadMedida> ListNew { get; set; }
        public List<UnidadMedida> ListToInsert { get; set; }
        public List<UnidadMedida> ListToUpdate { get; set; }
        public List<UnidadMedida> ListToDelete { get; set; }
        private Company Company;
        public String ItemCode;
        public int? EmpresaID;

        //SQL || Hana
        bool EsSQL;
        Recordset Insert;
        String Parametro;
        private String NombreTabla;
        public String NombreTablaUnidadMedidaSAP;
        #endregion Atributos

        #region Constructor
        public UnidadesMedidaSAP(SAPbobsCOM.Company company)
        {
            this.Company = company;
            this.unidad = new UnidadMedida();
            this.ListOld = new List<UnidadMedida>();
            this.ListNew = new List<UnidadMedida>();
            this.ListToInsert = new List<UnidadMedida>();

            try
            {
                this.Insert = (Recordset)this.Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                this.EsSQL = Company.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.NombreTabla = EsSQL ? "[@VSKF_UNIDADMEDIDA]" : @"""@VSKF_UNIDADMEDIDA""";
                this.NombreTablaUnidadMedidaSAP = this.NombreTabla;
            }
            catch (Exception ex)
            {
                throw new Exception("No se ha instanciado UnidadesMedidaSAP.cs Error: " + ex.Message);
            }
        }
        #endregion Constructor

        #region Métodos
        public void Insertar()
        {
            try
            {
                #region Fechas.ToString()
                String FechaModificacion;
                FechaModificacion = this.unidad.FechaModificacion != null
                    ? this.unidad.FechaModificacion.Value.ToString("yyyy-MM-dd")
                    : String.Empty;
                #endregion Fechas.ToString()

                #region SQL || Hana
                this.Parametro = String.Empty;

                this.Parametro = String.Format(@"INSERT INTO {0} (""Code"", ""Name"", U_UnidadMedidaID, U_Nombre, U_Longitud, U_Ancho, U_Altura, U_Volumen, U_UMVol, U_Peso, U_EmpresaID, U_FechaModificacion, U_Activo) ", this.NombreTabla);

                this.Parametro += String.Format("VALUES('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}')", this.NombreTabla, this.unidad.Code, this.unidad.Name, this.unidad.UnidadMedidaID, this.unidad.Nombre, this.unidad.Longitud, this.unidad.Ancho, this.unidad.Altura, this.unidad.Volumen, this.unidad.UMVol, this.unidad.Peso, this.unidad.EmpresaID, FechaModificacion, Convert.ToInt32(this.unidad.Activo));

                this.Insert.DoQuery(Parametro);
                #endregion SQL || Hana
            }
            catch (Exception ex)
            {
                #region GetLastError
                int errornum;
                string errormns;
                Company.GetLastError(out errornum, out errormns);
                throw new Exception("Error al intentar insertar una unidad de medida. Error: " + errornum + " - " + errormns);
                #endregion GetLastError
            }
        }

        public void Update()
        {
            try
            {
                #region Fechas.ToString()
                String FechaModificacion;
                FechaModificacion = this.unidad.FechaModificacion != null
                    ? this.unidad.FechaModificacion.Value.ToString("yyyy-MM-dd")
                    : String.Empty;
                #endregion Fechas.ToString()

                #region SQL || Hana
                this.Parametro = String.Empty;

                this.Parametro = String.Format(@"UPDATE {0} SET ""Code"" = '{1}', ""Name"" = '{2}', U_UnidadMedidaID = '{3}', U_Nombre = '{4}', U_Longitud = '{5}', U_Ancho = '{6}', U_Altura = '{7}', U_Volumen = '{8}', U_UMVol = '{9}', U_Peso = '{10}', U_EmpresaID = '{11}', U_FechaModificacion = '{12}', U_Activo = '{13}' WHERE Code = '{1}' and U_UnidadMedidaID = '{3}'", this.NombreTabla, this.unidad.Code, this.unidad.Name, this.unidad.UnidadMedidaID, this.unidad.Nombre, this.unidad.Longitud, this.unidad.Ancho, this.unidad.Altura, this.unidad.Volumen, this.unidad.UMVol, this.unidad.Peso, this.unidad.EmpresaID, FechaModificacion, Convert.ToInt32(this.unidad.Activo));

                this.Insert.DoQuery(Parametro);
                #endregion SQL || Hana
            }
            catch (Exception ex)
            {
                #region GetLastError
                int errornum;
                string errormns;
                Company.GetLastError(out errornum, out errormns);
                throw new Exception("Error al intentar actualizar una unidad de medida. Error: " + errornum + " - " + errormns);
                #endregion GetLastError
            }
        }

        public void Desactivate(String ID)
        {
            try
            {
                #region Validación
                if (!String.IsNullOrEmpty(ID))
                    throw new Exception("No se ha encontrado el identificador. Unidad de medida no desactivada. ");
                #endregion Validación

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET U_Activo = '0' WHERE ""Code"" = '{1}'", NombreTabla, ID);
                this.Insert.DoQuery(Parametro);
                #endregion SQL || Hana
            }
            catch (Exception ex)
            {
                #region GetLastError
                int errornum;
                string errormns;
                Company.GetLastError(out errornum, out errormns);
                throw new Exception("No se ha desactivado una unidad de medida. Error: " + errornum + " - " + errormns);
                #endregion GetLastError
            }
        }

        public void Delete()
        {
            try
            {
                #region Validación
                if (String.IsNullOrEmpty(unidad.Code))
                    throw new Exception("No se ha encontrado el indentificador. Unidad de medida no eliminada. ");
                #endregion Validación

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format("DELETE FROM {0} WHERE U_UnidadMedidaID = '{1}'", this.NombreTabla, unidad.Code);
                this.Insert.DoQuery(Parametro);
                #endregion SQL || Hana
            }
            catch (Exception ex)
            {
                #region GetLastError
                int errornum;
                string errormns;
                Company.GetLastError(out errornum, out errormns);
                throw new Exception("Error al intentar eliminar una unidad de medida. Error: " + errornum + " - " + errormns);
                #endregion GetLastError
            }
        }
        #endregion Métodos

        #region Auxiliares
        public bool ExistSAP(UnidadMedida unidad)
        {
            bool existe = false;
            try
            {
                #region Validación

                if (String.IsNullOrEmpty(unidad.Code))
                    throw new Exception("No se ha encontrado el indentificador. Unidad de medida no eliminada. ");
                #endregion Validación

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"SELECT ""UomEntry"" FROM ""OUOM"" WHERE ""UomEntry"" = '{0}'", unidad.Code);
                this.Insert.DoQuery(Parametro);
                #endregion SQL || Hana

                #region Verificación
                if (Insert.RecordCount >= 1)
                {
                    existe = true;
                }
                #endregion Verificación

                return existe;
            }
            catch (Exception ex)
            {
                #region GetLastError
                int errornum;
                string errormns;
                Company.GetLastError(out errornum, out errormns);
                throw new Exception("Se produjo un error al intentar validar la existencia de una unidad de medida en SAP. Error: " + errornum + " - " + errormns + "logic error: " + ex.Message);
                #endregion GetLastError
            }
        }

        public bool ExistKF(UnidadMedida unidad)
        {
            bool existe = false;
            try
            {
                #region Validación

                if (String.IsNullOrEmpty(unidad.Code))
                    throw new Exception("No se ha encontrado el indentificador. Unidad de medida no eliminada. ");
                #endregion Validación

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"SELECT ""Code"" FROM {0} WHERE ""Code"" = '{1}'", NombreTabla, unidad.Code);
                this.Insert.DoQuery(Parametro);
                #endregion SQL || Hana

                #region Verificación
                Insert.MoveFirst();
                if (Insert.RecordCount >= 1)
                {
                    existe = true;
                }
                #endregion Verificación

                return existe;
            }
            catch (Exception ex)
            {
                #region GetLastError
                int errornum;
                string errormns;
                Company.GetLastError(out errornum, out errormns);
                throw new Exception("Se produjo un error al intentar validar la existencia de una unidad de medida en Kananfleet. Error: " + errornum + " - " + errormns + " logic error: " + ex.Message);
                #endregion GetLastError
            }
        }

        public List<UnidadMedida> GetList()
        {
            List<UnidadMedida> lista = new List<UnidadMedida>();
            UnidadMedida temp = new UnidadMedida();
            try
            {
                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"SELECT ""UomEntry"", ""UomCode"", ""UomName"", ""Length1"", ""Width1"", ""Height1"", ""Volume"", ""VolUnit"", ""Weight1"", ""UpdateDate"" FROM ""OUOM"" ");
                this.Insert.DoQuery(Parametro);
                #endregion SQL || Hana

                #region RecordsetToList
                this.Insert.MoveFirst();
                for (int i = 0; i < Insert.RecordCount; i++)
                {
                    temp = null; temp = new UnidadMedida();
                    temp.Code = Convert.ToString(this.Insert.Fields.Item("UomEntry").Value);
                    temp.Name = this.Insert.Fields.Item("UomName").Value;
                    temp.Nombre = this.Insert.Fields.Item("UomCode").Value;
                    temp.Longitud = this.Insert.Fields.Item("Length1").Value;
                    temp.Ancho = this.Insert.Fields.Item("Width1").Value;
                    temp.Altura = this.Insert.Fields.Item("Height1").Value;
                    temp.Volumen = this.Insert.Fields.Item("Volume").Value;
                    temp.UMVol = this.Insert.Fields.Item("VolUnit").Value;
                    temp.Peso = this.Insert.Fields.Item("Weight1").Value;
                    temp.FechaModificacion = this.Insert.Fields.Item("UpdateDate").Value;

                    lista.Add(temp);
                    Insert.MoveNext();
                }
                #endregion RecordsetToList

                return lista;
            }
            catch (Exception ex)
            {
                #region GetLastError
                int errornum;
                string errormns;
                Company.GetLastError(out errornum, out errormns);
                throw new Exception("GetList() -> UnidadesMedidaSAP.cs. Error: " + errornum + " - " + errormns);
                #endregion GetLastError
            }
        }

        public List<UnidadMedida> GetSymmetricdifference()
        {
            List<UnidadMedida> List = new List<UnidadMedida>();
            int contmayor = 0;
            int contmenor = 0;
            try
            {
                if (ListNew.Count >= ListOld.Count)
                {
                    contmayor = ListNew.Count;
                    contmenor = ListOld.Count;
                }
                else
                {
                    contmayor = ListOld.Count;
                    contmenor = ListNew.Count;
                }

                for (int i = 0; i < contmenor; i++)
                {
                    for (int j = 0; j < contmayor; j++)
                    {
                        if (ListNew[i].Code == ListOld[j].Code)
                        {
                            if (ListNew[i].Name != ListOld[j].Name | ListNew[i].Nombre != ListOld[j].Nombre | ListNew[i].Longitud != ListOld[j].Longitud | ListNew[i].Ancho != ListOld[j].Ancho | ListNew[i].Altura != ListOld[j].Altura | ListNew[i].Volumen != ListOld[j].Volumen | ListNew[i].UMVol != ListOld[j].UMVol | ListNew[i].Peso != ListOld[j].Peso | ListNew[i].FechaModificacion != ListOld[j].FechaModificacion)
                            {
                                List.Add(ListNew[i]);
                            }
                        }
                    }
                }

                return List;
            }
            catch (Exception)
            {
                #region GetLastError
                int errornum;
                string errormns;
                Company.GetLastError(out errornum, out errormns);
                throw new Exception("GetSymmetricdifference() -> UnidadesMedidaSAP.cs. Error: " + errornum + " - " + errormns);
                #endregion GetLastError
            }
        }
        #endregion Auxiliares
    }
}
