﻿using Kanan.Operaciones.BO2;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KananSAP.Operaciones.Data
{
    public class SucursalSAP
    {
        #region Propiedades

        public Sucursal Sucursal { get; set; }
        public UserTable oUDT { get; set; }
        public Items oItem { get; set; }
        public SAPbobsCOM.Branch oBranch;
        public SAPbobsCOM.BranchesService oBranchService { get; set; }
        public SAPbobsCOM.BranchParams oBranchParams { get; set; }
        public SAPbobsCOM.CompanyService oCompanyService { get; set; }
        public string ItemCode { get; set; }
        private Company oCompany;
        private Sucursal Temp { get; set; }
        /// <summary>
        /// Indica la Sincronizacion entre SAP y Kanan. 
        /// Estatus: 0 = Sincronizado, 1 = Insertando, 2 = Actualizando, 3 = Eliminando
        /// </summary>
        public int? Sincronizado { get; set; }
        /// <summary>
        /// GUID
        /// </summary>
        public Guid? UUID { get; set; }
        Recordset InsertarRs;
        Recordset RsBranchOffice;
        String Sentencia = string.Empty;

        //Migracion Hana
        Recordset Insert;
        String Parametro;
        private String NombreTabla;
        private String NombreTablaBranch;
        private String NombreCostingCode;
        bool EsSQL;

        //Se agregó para ser visible desde ServicioSAP.cs en KananSAP.Mantenimiento.Data.
        public String NombreTablaSucursalSAP = String.Empty;

        #endregion

        public bool ExistUDT()
        {
            return oUDT.GetByKey(this.ItemCode);
        }

        #region Constructor

        public SucursalSAP(Company company)
        {
            this.oCompany = company;
            this.oCompanyService = company.GetCompanyService();
            oBranchService = this.oCompanyService.GetBusinessService(ServiceTypes.BranchesService);
            oBranchParams = (BranchParams)oBranchService.GetDataInterface(BranchesServiceDataInterfaces.bsBranchParams);        
            this.Sucursal = new Sucursal()
            {
                Base = new Kanan.Operaciones.BO2.Empresa(),
            };
            
            try
            {
                this.oUDT = company.UserTables.Item("VSKF_SUCURSAL");
                
                //Migración HANA
                this.InsertarRs = (Recordset)this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.Insert = (Recordset)this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.RsBranchOffice = (Recordset)this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                this.EsSQL = oCompany.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.NombreTabla = EsSQL ? "[@VSKF_SUCURSAL]" : @"""@VSKF_SUCURSAL""";
                this.NombreTablaBranch = EsSQL ? "OUBR" : @"""OUBR""";
                this.NombreCostingCode = EsSQL ? "OPRC" : @"""OPRC""";

                this.NombreTablaSucursalSAP = this.NombreTabla;

            }
            catch(Exception ex)
            {
                this.oUDT = null;
            }
        }

        #endregion

        #region Branch Office SAP
        public bool UpdateBranch()
        {
            try
            {// No se puede aplica rupdate directo a Tabla nativas de SAP
                oBranchParams.Code = Convert.ToInt32(this.ItemCode);
                oBranch = oBranchService.GetBranch(oBranchParams);
                oBranch.Name = this.Sucursal.Direccion;
                oBranch.Description = this.Sucursal.Descripcion;
                oBranchService.UpdateBranch(oBranch);
                //String nombreTablaBranch = this.NombreTablaBranch;
                //String SentenciaBranch = String.Format(@"UPDATE {0} SET ""Name""='{1}', ""Remarks""='{2}' WHERE ""Code""='{3}'", nombreTablaBranch,oBranch.Name,oBranch.Description, oBranchParams.Code);
                //this.RsBranchOffice.DoQuery(SentenciaBranch);
                return true;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public bool RemoveBranch()
        {
            try
            {
                oBranchParams.Code = Convert.ToInt32(this.ItemCode);
                oBranchService.DeleteBranch(oBranchParams);
                //String nombreTablaBranch = this.NombreTablaBranch;
                //String SentenciaBranch = String.Format(@"DELETE FROM {0} WHERE ""Code""='{1}'", nombreTablaBranch, oBranchParams.Code);
                //this.RsBranchOffice.DoQuery(SentenciaBranch);
                return true;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public bool AddBranch()
        {
            try
            {
                #region Comentado
                /*String nombreTabla = NombreTabla;

                Sentencia = String.Format("INSERT INTO {0} VALUES('{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}'),'{15}','{16}','{17}')", nombreTabla, this.Sucursal.SucursalID, this.Sucursal.Direccion, this.Sucursal.Direccion, this.Sucursal.Descripcion, this.Sucursal.Responsable, this.Sucursal.Contacto, this.Sucursal.SucursalID, this.Sucursal.Telefono, this.Sucursal.Base.EmpresaID, this.Sucursal.CodigoPostal, this.Sucursal.Responsable, this.Sucursal.Ciudad, this.Sucursal.Estado, this.Sucursal.DireccionReal, this.Sucursal.EsActivo, "0", this.UUID);
                
                this.InsertarRs.DoQuery(Sentencia);*/
                #endregion Comentado

                oBranch = (SAPbobsCOM.Branch)oBranchService.GetDataInterface(BranchesServiceDataInterfaces.bsBranch);
                oBranch.Name = this.Sucursal.Name.Length > 20? Sucursal.Name.Substring(0,20) : Sucursal.Name;
                oBranch.Description = this.Sucursal.Descripcion;
               
                oBranchService.AddBranch(oBranch);
                
                return true;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public bool ExistBranch(string name)
        {
            try
            {
                BranchesParams listBranches = oBranchService.GetBranchList();
                foreach(BranchParams branch in listBranches)
                {
                    string _name = branch.Name;
                    int code = branch.Code;
                    if(_name == name || this.ItemCode == code.ToString())
                    {
                        Temp = new Sucursal();
                        Temp.Base = new Empresa();
                        Temp.SucursalID = code;
                        Temp.Direccion = _name;
                        this.ItemCode = code.ToString();
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public bool SetItemCodeBranch(string name)
        {
            try
            {
                BranchesParams listBranches = oBranchService.GetBranchList();
                foreach (BranchParams branch in listBranches)
                {
                    string _name = branch.Name;
                    int code = branch.Code;
                    if (_name == name)
                    {
                        this.ItemCode = code.ToString();
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        #endregion

        public void SucursalToUDT()
        {
            try
            {
                this.oUDT.Code = this.Sucursal.Code;
                if (string.IsNullOrEmpty(this.Sucursal.Code))
                    this.oUDT.Code = this.Sucursal.SucursalID.ToString();
                //El campo "Name" solo admite 20 caracteres, por lo que se valida
                this.oUDT.Name = this.Sucursal.SucursalID.ToString();// this.Sucursal.Name.Length > 20 ? this.Sucursal.Name.Substring(0, 20) : Convert.ToString(this.Sucursal.Name);
                this.oUDT.UserFields.Fields.Item("U_SUCURSALID").Value = this.Sucursal.SucursalID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_DIRECCION").Value = this.Sucursal.Direccion ?? string.Empty;
                this.oUDT.UserFields.Fields.Item("U_EMPRESAID").Value = this.Sucursal.Base.EmpresaID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_ESACTIVO").Value = Convert.ToInt32(this.Sucursal.EsActivo);
                //this.oUDT.UserFields.Fields.Item("Encargado").Value = this.Sucursal.Responsable;
                this.oUDT.UserFields.Fields.Item("U_DESCRIPCION").Value = this.Sucursal.Descripcion ?? string.Empty;
                this.oUDT.UserFields.Fields.Item("U_CONTACTO").Value = this.Sucursal.Contacto ?? string.Empty;
                this.oUDT.UserFields.Fields.Item("U_TELEFONO").Value = this.Sucursal.Telefono ?? string.Empty;
                this.oUDT.UserFields.Fields.Item("U_CODIGOPOSTAL").Value = this.Sucursal.CodigoPostal ?? 0;
                this.oUDT.UserFields.Fields.Item("U_RESPONSABLE").Value = this.Sucursal.Responsable ?? string.Empty;
                this.oUDT.UserFields.Fields.Item("U_CIUDAD").Value = this.Sucursal.Ciudad ?? string.Empty;
                this.oUDT.UserFields.Fields.Item("U_ESTADO").Value = this.Sucursal.Estado ?? string.Empty;
                this.oUDT.UserFields.Fields.Item("U_DIRECCIONREAL").Value = this.Sucursal.DireccionReal ?? string.Empty;
                this.oUDT.UserFields.Fields.Item("U_SINCRONIZADO").Value = this.Sincronizado < 0 ? 0 : Sincronizado;
                this.oUDT.UserFields.Fields.Item("U_UUID").Value = this.UUID == null ? string.Empty : this.UUID.ToString();
                this.oUDT.UserFields.Fields.Item("U_Almacen").Value = this.Sucursal.Almacen ?? string.Empty;
                this.oUDT.UserFields.Fields.Item("U_BranchID").Value = this.Sucursal.BranchID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_Centro").Value = this.Sucursal.CostCenter ?? string.Empty;
                this.oUDT.UserFields.Fields.Item("U_MAILALM").Value = this.Sucursal.MailAlmacenista ?? string.Empty;
                this.oUDT.UserFields.Fields.Item("U_HRSDISP").Value = this.Sucursal.HorasDisponibilidad ?? 0;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void UDTToSucursal()
        {
            try
            {
                this.Sucursal = new Sucursal();
                this.Sucursal.Base = new Empresa();
                this.Sucursal.Code = this.oUDT.Code ?? null;
                this.Sucursal.Name = this.oUDT.Name ?? null;
                this.Sucursal.SucursalID = this.oUDT.UserFields.Fields.Item("U_SUCURSALID").Value ?? null;
                this.Sucursal.Direccion = this.oUDT.UserFields.Fields.Item("U_DIRECCION").Value ?? null;
                this.Sucursal.Base.EmpresaID = this.oUDT.UserFields.Fields.Item("U_EMPRESAID").Value ?? null;
                this.Sucursal.EsActivo = Convert.ToBoolean(this.oUDT.UserFields.Fields.Item("U_ESACTIVO").Value) ?? null;
                //this.oUDT.UserFields.Fields.Item("Encargado").Value = this.Sucursal.Responsable;
                this.Sucursal.Descripcion = this.oUDT.UserFields.Fields.Item("U_DESCRIPCION").Value ?? null;
                this.Sucursal.Contacto = this.oUDT.UserFields.Fields.Item("U_CONTACTO").Value ?? null;
                this.Sucursal.Telefono = this.oUDT.UserFields.Fields.Item("U_TELEFONO").Value ?? null;
                this.Sucursal.CodigoPostal = this.oUDT.UserFields.Fields.Item("U_CODIGOPOSTAL").Value ?? 0;
                this.Sucursal.Responsable = this.oUDT.UserFields.Fields.Item("U_RESPONSABLE").Value ?? null;
                this.Sucursal.Ciudad = this.oUDT.UserFields.Fields.Item("U_CIUDAD").Value ?? null;
                this.Sucursal.Estado = this.oUDT.UserFields.Fields.Item("U_ESTADO").Value ?? null;
                this.Sucursal.DireccionReal = this.oUDT.UserFields.Fields.Item("U_DIRECCIONREAL").Value ?? null;
                this.Sincronizado = this.oUDT.UserFields.Fields.Item("U_SINCRONIZADO").Value != null ? this.oUDT.UserFields.Fields.Item("U_SINCRONIZADO").Value : 0;
                if (!string.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_UUID").Value))
                    this.UUID = Guid.Parse(this.oUDT.UserFields.Fields.Item("U_UUID").Value);
                this.Sucursal.Almacen = Convert.ToString(this.oUDT.UserFields.Fields.Item("U_Almacen").Value) ?? string.Empty;
                this.Sucursal.BranchID = this.oUDT.UserFields.Fields.Item("U_BranchID").Value ?? 0;
                this.Sucursal.CostCenter = Convert.ToString(this.oUDT.UserFields.Fields.Item("U_Centro").Value) ?? string.Empty;
                this.Sucursal.MailAlmacenista = Convert.ToString(this.oUDT.UserFields.Fields.Item("U_MAILALM").Value) ?? string.Empty;
                this.Sucursal.HorasDisponibilidad = this.oUDT.UserFields.Fields.Item("U_HRSDISP").Value ?? 0;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void GetSyncUUID()
        {
            try
            {
                if (this.oUDT.UserFields.Fields.Item("U_SINCRONIZADO").Value != null)
                    this.Sincronizado = this.oUDT.UserFields.Fields.Item("U_SINCRONIZADO").Value;
                if (!string.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_UUID").Value))
                    this.UUID = Guid.Parse(this.oUDT.UserFields.Fields.Item("U_UUID").Value);
                else
                    this.UUID = Guid.NewGuid();
                
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void Sincronizar(Sucursal sc)
        {
            try
            {
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(@" UPDATE " + this.NombreTabla + @" SET ""Code"" = '" + sc.SucursalID + @"', U_SINCRONIZADO = " + Sincronizado + @", U_SUCURSALID = " + sc.SucursalID);

                if (UUID != null)
                    where.Append(string.Format(@" and ""U_UUID"" like '%{0}%' ", UUID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                rs.DoQuery(query.ToString());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region Insertar Duplicado en Merge
        /*public void Insertar()
        {
            try
            {

                int result = oUDT.Add();
                string errormsj;
                int errornum;
                if (result != 0)
                {
                    oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                
            }
            catch (Exception ex)
            {
                oUDT.Remove();
                throw new Exception(ex.Message);
            }
        }*/
        #endregion Insertar Duplicado en Merge

        public void Insertar()
        {
            try
            {
                #region SQL || Hana
                //Sentencia = String.Empty;
                //Sentencia = String.Format(@"INSERT INTO {0} (""Code"",""Name"",""U_SUCURSALID"",""U_DIRECCION"",""U_EMPRESAID"",""U_ESACTIVO"",""U_ENCARGADO"",""U_DESCRIPCION"",""U_CONTACTO"",""U_TELEFONO"",""U_CODIGOPOSTAL"",""U_RESPONSABLE"",""U_CIUDAD"",""U_ESTADO"",""U_DIRECCIONREAL"",""U_SINCRONIZADO"",""U_UUID"") VALUES('{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}')", this.NombreTabla, this.Sucursal.SucursalID, this.Sucursal.Name, this.Sucursal.SucursalID, this.Sucursal.Direccion, this.Sucursal.Base.EmpresaID, "1", this.Sucursal.Responsable, this.Sucursal.Descripcion, this.Sucursal.Contacto, this.Sucursal.Telefono, this.Sucursal.CodigoPostal, this.Sucursal.Responsable, this.Sucursal.Ciudad, this.Sucursal.Estado, this.Sucursal.DireccionReal, ((bool)this.Sucursal.EsActivo) ? "1" : "0",  this.UUID);
                // this.InsertarRs = (Recordset)this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                //this.InsertarRs.DoQuery(Sentencia);
                #endregion SQL || Hana

                #region SQL
                int result = oUDT.Add();
                string errormsj;
                int errornum;
                if (result != 0)
                {
                    oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL
            }
            catch (Exception ex)
            {
                //oUDT.Remove();

                string ID = String.Empty;
                if (!String.IsNullOrEmpty(oUDT.Code))
                    ID = oUDT.Code;
                else
                {
                    throw new Exception("Erron al intentar obtener identificador. ");
                }

                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code"" = '{1}'", this.NombreTabla, ID);

                this.Insert.DoQuery(this.Parametro);

                throw new Exception("Error al intentar insertar una Sucursal. " + ex.Message);
            }
        }

        public void Actualizar()
        {
            try
            {
                int Activo = 0;
                if (Convert.ToBoolean(this.Sucursal.EsActivo))
                    Activo = 1;
                int? ValSinc;
                ValSinc = String.IsNullOrEmpty(Convert.ToString(this.Sincronizado)) ? ValSinc = 0 : ValSinc = this.Sincronizado;

                #region SQL || HANA
                //this.Parametro = String.Empty;
                //this.Parametro = String.Format(@"UPDATE {0} SET ""Code""='{1}',""Name""='{2}', ""U_DIRECCION""='{3}', ""U_EMPRESAID""='{4}', ""U_ESACTIVO""='{5}', ""U_ENCARGADO""='{6}', ""U_DESCRIPCION""='{7}', ""U_CONTACTO""='{8}', ""U_TELEFONO""='{9}',""U_CODIGOPOSTAL""='{10}', ""U_RESPONSABLE""='{11}', ""U_CIUDAD""='{12}', ""U_ESTADO""='{13}', ""U_DIRECCIONREAL""='{14}', ""U_SINCRONIZADO""='{15}', ""U_UUID""='{16}'  WHERE ""Code""='{17}'", this.NombreTabla, this.Sucursal.SucursalID.ToString(), this.Sucursal.Name.ToString(), this.Sucursal.Direccion, this.Sucursal.Base.EmpresaID.ToString(), ((bool)this.Sucursal.EsActivo)?"1":"0", this.Sucursal.Contacto, this.Sucursal.Descripcion,this.Sucursal.Contacto, this.Sucursal.Telefono, this.Sucursal.CodigoPostal.ToString(), this.Sucursal.Responsable, this.Sucursal.Ciudad, this.Sucursal.Estado, this.Sucursal.DireccionReal, ValSinc, Convert.ToString(this.UUID), this.Sucursal.SucursalID.ToString());
                //this.InsertarRs.DoQuery(Parametro);
                #endregion SQL || HANA

                #region SQL
                int result = oUDT.Update();
                string errormsj;
                int errornum;
                if (result != 0)
                {
                    oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar actualizar una Sucursal. " + ex.Message);
            }

        }

        public void Eliminar()
        {
            try
            {
                String DeleteID = String.Empty;
                DeleteID = oUDT.Code;

                #region SQL || HANA
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code""='{1}'", this.NombreTabla, this.Sucursal.SucursalID.ToString());
                this.Insert.DoQuery(Parametro);
                #endregion

                #region SQL
                //int result = oUDT.Remove();
                //string errormsj;
                //int errornum;
                //if (result != 0)
                //{
                //    oCompany.GetLastError(out errornum, out errormsj);
                //    throw new Exception("Error: " + errornum + " - " + errormsj);
                //}
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar actualizar una Sucursal. " + ex.Message);
            }

        }

        public bool SetUDTBySucursalID(int? id)
        {
            try
            {
                var rs = this.ConsultarByID(id);
                int _id = 0;
                if (HashSucursal(rs))
                {
                    rs.MoveFirst();
                    _id = rs.Fields.Item("U_SUCURSALID").Value;
                    this.ItemCode = rs.Fields.Item("Code").Value;
                    if (_id == id)
                        return this.oUDT.GetByKey(rs.Fields.Item("Code").Value.ToString());

                }
                this.oUDT.Code = string.Empty;
                return false;
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }

        public Recordset ConsultarByID(int? id)
        {
            try
            {
                Recordset rs = (Recordset)this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                
                rs.DoQuery(string.Format(@"select * from {0} where ""U_SUCURSALID"" = '{1}' ", this.NombreTabla, id));
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Deactivate()
        {
            try
            {
                string ID = String.Empty;
                if (!String.IsNullOrEmpty(oUDT.Code))
                    ID = oUDT.Code;
                else
                {
                    throw new Exception("Erron al intentar obtener identificador. ");
                }

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = string.Format(@"UPDATE {0} SET ""U_SINCRONIZADO"" = 3, ""U_ESACTIVO"" = 0 WHERE ""Code"" = '{1}'", this.NombreTabla, ID);

                this.InsertarRs.DoQuery(this.Parametro);
                #endregion SQL || Hana

                #region SQL
                /*this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = 3;
                this.oUDT.UserFields.Fields.Item("U_EsActivo").Value = 0;
                Actualizar(this.NombreTablaBranch);*/
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar desactivar un registro. " + ex.Message);
            }
        }

        public bool HashSucursal(Recordset rs)
        {
            return rs.RecordCount >= 1;
        }

        public Recordset Consultar(Sucursal sc)
        {
            try
            {
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                if (sc.Base == null) sc.Base = new Kanan.Operaciones.BO2.Empresa();
                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(@" SELECT a.""Code"", a.""Name"",a.""U_SUCURSALID"", a.""U_DIRECCION"", a.""U_EMPRESAID"", a.""U_ESACTIVO"", a.""U_RESPONSABLE"",a.""U_CIUDAD"",a.""U_ESTADO"",a.""U_TELEFONO"",a.""U_CODIGOPOSTAL"", a.""U_DESCRIPCION"", a.""U_CONTACTO"", a.""U_DIRECCIONREAL"" FROM "+ this.NombreTabla + " a ");
                if (sc.SucursalID != null)
                    where.Append(string.Format(@"and a.""U_SUCURSALID"" = {0} ", sc.SucursalID));
                if(!string.IsNullOrEmpty(sc.Direccion))
                    where.Append(string.Format(@"and a.""U_DIRECCION"" like '%{0}%' ", sc.Direccion));
                if(sc.Base.EmpresaID != null)
                    where.Append(string.Format(@"and a.""U_EMPRESAID"" = {0} ", sc.Base.EmpresaID));
                if (sc.EsActivo != null)
                    where.Append(string.Format(@"and a.""U_ESACTIVO"" = {0} ", Convert.ToInt32(sc.EsActivo)));
                if (!string.IsNullOrEmpty(sc.Responsable))
                    where.Append(string.Format(@"and a.""U_RESPONSABLE"" like '%{0}%' ", sc.Responsable));
                if (!string.IsNullOrEmpty(sc.Descripcion))
                    where.Append(string.Format(@"and a.""U_DESCRIPCION"" like '%{0}%' ", sc.Descripcion));
                if (!string.IsNullOrEmpty(sc.Contacto))
                    where.Append(string.Format(@"and a.""U_CONTACTO"" like '%{0}%' ", sc.Contacto));
                if (!string.IsNullOrEmpty(sc.DireccionReal))
                    where.Append(string.Format(@"and a.""U_DIRECCIONREAL"" like '%{0}%' ", sc.DireccionReal));
                string filtro = where.ToString();
                if(filtro.Length > 0)
                {
                    if (filtro.StartsWith("and "))
                        filtro = filtro.Substring(3);
                    query.Append("where " + filtro);
                }
                rs.DoQuery(query.ToString());
                return rs;

            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public Recordset GetAllSucursales()
        {
            Recordset rs = (Recordset)this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            rs.DoQuery("select * from "+ this.NombreTabla );
            return rs;
        }

        public Recordset GetAllCostingCode()
        {
            Recordset rs = (Recordset)this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            rs.DoQuery("select * from " + this.NombreCostingCode);
            return rs;
        }

        public List<Sucursal> RecordSetToListSucursal(Recordset rs)
        {
            var result = new List<Sucursal>();
            for (int i = 0; i < rs.RecordCount; i++)
            {
                string itemcode = rs.Fields.Item("Code").Value;
                this.oUDT.GetByKey(itemcode);
                UDTToSucursal();
                var temp = (Sucursal)this.Sucursal.Clone();
                result.Add(temp);
                rs.MoveNext();
            }
            return result;
        }

        private Sucursal Clone(Sucursal suc)
        {
            var result = new Sucursal();
            result.SucursalID = suc.SucursalID;
            result.Direccion = suc.Direccion;
            result.Base = suc.Base;
            result.EsActivo = suc.EsActivo;
            result.Responsable = suc.Responsable;
            result.Descripcion = suc.Descripcion;
            result.Contacto = suc.Contacto;
            result.Ciudad = suc.Ciudad;
            result.Estado = suc.Estado;
            result.Telefono = suc.Telefono;
            result.DireccionReal = suc.DireccionReal;
            result.CodigoPostal = suc.CodigoPostal;

            return result;
        }
    }
}