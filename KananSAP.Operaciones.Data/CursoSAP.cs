﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kanan.Operaciones.BO2;
using SAPbobsCOM;

namespace KananSAP.Operaciones.Data
{
    public class CursoSAP
    {
        #region Propiedades
        private const string TABLE_SERVICE_NAME = "VSKF_CURSO";
        public Curso curso { get; set; }
        public UserTable oUDT { get; set; }
        public int? opid { get; set; }
        private Company Company;
        public int? Sincronizado { get; set; }
        public Guid? UUID { get; set; }

        //Migracion Hana
        Recordset Insertar;
        String Parametro;
        private String NombreTabla;
        bool EsSQL;

        //Se agregó para ser visible desde cualquier lugar consulta.
        public String NombreTablaCursosSAP = String.Empty;

        //public bool ExistUDT()
        //{
        //    return oUDT.GetByKey(this.ItemCode);
        //}
        #endregion

        #region Constructor
        public CursoSAP(SAPbobsCOM.Company company)
        {
            this.Company = company;
            this.curso = new Curso();
            try
            {
                this.oUDT = company.UserTables.Item(TABLE_SERVICE_NAME);

                //Migración HANA
                this.Insertar = (Recordset)this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.EsSQL = Company.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.NombreTabla = EsSQL ? "[@VSKF_CURSO]" : @"""@VSKF_CURSO""";
                //Se maneja en una variable pública para poder utilizar desde cualquier método.
                this.NombreTablaCursosSAP = this.NombreTabla;
            }
            catch (Exception)
            {
                this.oUDT = null;
            }
        } 
        #endregion

        #region Métodos

        #region Metodos Datos

        public void cursoToUDT()
        {
            string tks = DateTime.Now.Ticks.ToString();
            oUDT.Code = this.curso.CursoID != null ? this.curso.CursoID.ToString() : tks;
            oUDT.Name = curso.Nombre.Length > 30 ? this.curso.Nombre.Substring(0,30) : this.curso.Nombre;
            if (this.curso.Fecha != null)
                this.oUDT.UserFields.Fields.Item("U_Fecha").Value = this.curso.Fecha;

            this.oUDT.UserFields.Fields.Item("U_EmpresaOtorga").Value = this.curso.EmpresaOtorga ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_Lugar").Value = this.curso.Lugar ?? string.Empty;
            if (this.opid != null)
                this.oUDT.UserFields.Fields.Item("U_OperadorID").Value = this.opid;
            //this.oUDT.UserFields.Fields.Item("U_Estatus").Value = this.curso.Enabled != null ? ((bool)this.curso.Enabled ? 1 : 0) : 0; ;
            this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Sincronizado < 0? 0 : Sincronizado;
            this.oUDT.UserFields.Fields.Item("U_UUID").Value = this.UUID == null ? string.Empty : this.UUID.ToString();
        }

        public void UDTToCurso()
        {
            this.curso = new Curso();
            this.curso.CursoID = !string.IsNullOrEmpty(this.oUDT.Code) ? Convert.ToInt32(oUDT.Code) : (int?)null;
            this.curso.Nombre = this.oUDT.Name ?? string.Empty;
            this.curso.Fecha = this.oUDT.UserFields.Fields.Item("U_Fecha").Value == null ? new DateTime() : DateTime.Parse(this.oUDT.UserFields.Fields.Item("U_Fecha").Value.ToString());
            if (this.curso.Fecha != null && this.curso.Fecha.Value.Year < 1900)
            {
                this.curso.Fecha = null;
            }
            this.curso.EmpresaOtorga = string.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_EmpresaOtorga").Value.ToString()) ? string.Empty : this.oUDT.UserFields.Fields.Item("U_EmpresaOtorga").Value.ToString();
            this.curso.Lugar= string.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_Lugar").Value.ToString()) ? string.Empty : this.oUDT.UserFields.Fields.Item("U_Lugar").Value.ToString();
            this.opid = this.oUDT.UserFields.Fields.Item("U_OperadorID").Value == null ? null : Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_OperadorID").Value.ToString());
            this.Sincronizado = this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value == null ? null : (int?)this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value;
            this.UUID = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_UUID").Value.ToString()) ? null : Guid.Parse(this.oUDT.UserFields.Fields.Item("U_UUID").Value);
        }
        #endregion
        
        #region Acciones
        public void Insert()
        {
            try
            {

                this.Parametro = String.Empty;
                this.Parametro = String.Format("INSERT INTO {0} VALUES('{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')", this.NombreTabla, this.oUDT.Code, this.oUDT.Name, this.curso.Fecha.Value.ToString("yyyy/MM/dd"), this.curso.EmpresaOtorga, this.curso.Lugar, this.opid, "0", this.UUID);

                this.Insertar.DoQuery(Parametro);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Remove()
        {
            if (oUDT.GetByKey(curso.CursoID.ToString()))
            {
                string ID = String.Empty;
                if (!String.IsNullOrEmpty(oUDT.Code))
                    ID = oUDT.Code;
                else
                {
                    throw new Exception("Erron al intentar obtener identificador. ");
                }

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code"" = '{1}'", this.NombreTabla, ID);

                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana

                #region SQL
                /*int flag = oUDT.Remove();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    Company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }*/
                #endregion SQL
            }
        }

        public void Update()
        {
            try
            {
                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET ""Code"" = '{1}', ""Name"" = '{2}', U_Fecha = '{3}', U_EmpresaOtorga = '{4}', U_Lugar = '{5}', U_OperadorID = '{6}', U_Sincronizado = '{7}' WHERE ""Code"" = '{1}'", this.NombreTabla, this.oUDT.Code, this.oUDT.Name, this.curso.Fecha.Value.ToString("yyyy/MM/dd"), this.curso.EmpresaOtorga, this.curso.Lugar, this.opid, this.Sincronizado);
                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana
            }
            catch
            {
                #region SQL
                int flag = oUDT.Update();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    Company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL
            }
        }
        #endregion

        #region Services
        public Recordset Consultar(Curso cur)
        {
            try
            {
                Recordset rs = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                if (cur == null) cur = new Curso();
                

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(" SELECT * from " + this.NombreTabla + " as CUR ");

                if (cur.CursoID!= null && cur.CursoID > 0)
                    where.Append(string.Format(@" and cur.""Code"" = {0} ", cur.CursoID));
                if (!string.IsNullOrEmpty(cur.Nombre))
                    where.Append(string.Format(@" and cur.""Name"" = '{0}' ", cur.Nombre));
                if (cur.Fecha != null)
                    where.Append(string.Format(" and cur.U_Fecha = {0} ", cur.Fecha));
                if (opid != null)
                    where.Append(string.Format(" and cur.U_OperadorID = {0} ", opid));
                if (!string.IsNullOrEmpty(cur.EmpresaOtorga))
                    where.Append(string.Format(" and cur.U_EmpresaOtorga = '{0}' ", cur.EmpresaOtorga));
                if (!string.IsNullOrEmpty(cur.Lugar))
                    where.Append(string.Format(" and cur.U_Lugar = '{0}' ", cur.Lugar));
                if (Sincronizado != null)
                    where.Append(string.Format(" and cur.U_Sincronizado = {0} ", Sincronizado));
                if (UUID != null)
                    where.Append(string.Format(" and cur.U_UUID = '{0}' ", UUID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                query.Append(@" order by cur.""Code"" desc ");
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Curso> RecordSetToListCursos(Recordset rs)
        {
            var result = new List<Curso>();
            for (int i = 0; i < rs.RecordCount; i++)
            {
                string itemcode = rs.Fields.Item("Code").Value;
                this.oUDT.GetByKey(itemcode);
                UDTToCurso();
                var temp = Clone(curso);
                result.Add(temp);
                rs.MoveNext();
            }
            return result;
        }

        public Recordset ActualizarCode(Curso cur)
        {
            try
            {
                Recordset rs = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(" UPDATE " + this.NombreTabla + @" SET ""Code"" = '" + cur.CursoID + "', U_Sincronizado = " + Sincronizado);

                if (UUID != null)
                    where.Append(string.Format(" and U_UUID like '%{0}%' ", UUID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private Curso Clone(Curso cur)
        {
            var result = new Curso();
            result.CursoID = cur.CursoID;
            result.Nombre = cur.Nombre;
            result.EmpresaOtorga = cur.EmpresaOtorga;
            result.Lugar = cur.Lugar;
            result.Fecha = cur.Fecha;
            return result;
        }

        #endregion

        #endregion
    }
}
