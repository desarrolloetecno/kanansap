﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kanan.Operaciones.BO2;
using SAPbobsCOM;

namespace KananSAP.Operaciones.Data
{
    public class LicenciaSAP
    {
        #region Propiedades
        private const string TABLE_SERVICE_NAME = "VSKF_LICENCIA";
        public Licencia licencia { get; set; }
        public UserTable oUDT { get; set; }
        public int? opid { get; set; }
        private Company Company;
        public int? Sincronizado { get; set; }
        public Guid? UUID { get; set; }

        //Migracion Hana
        Recordset Insertar;
        String Parametro;
        private String NombreTabla;
        bool EsSQL;

        //Se agregó para ser visible desde cualquier lugar.
        public String NombreTablaLicenciaSAP = String.Empty;

        //public bool ExistUDT()
        //{
        //    return oUDT.GetByKey(this.ItemCode);
        //}
        #endregion

        #region Constructor
        public LicenciaSAP(SAPbobsCOM.Company company)
        {
            this.Company = company;
            this.licencia = new Licencia();
            try
            {
                this.oUDT = company.UserTables.Item(TABLE_SERVICE_NAME);

                //Migración HANA
                this.Insertar = (Recordset)this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.EsSQL = Company.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.NombreTabla = EsSQL ? "[@VSKF_LICENCIA]" : @"""@VSKF_LICENCIA""";
                //Se maneja en una variable pública para poder ser utilizada en otros lugares.
                this.NombreTablaLicenciaSAP = this.NombreTabla;
            }
            catch (Exception)
            {
                this.oUDT = null;
            }
        } 
        #endregion

        #region Métodos

        #region Metodos Datos

        public void LicenciaToUDT()
        {
            string tks = DateTime.Now.Ticks.ToString();
            oUDT.Code = this.licencia.LicenciaID != null ? this.licencia.LicenciaID.ToString() : tks;
            oUDT.Name = this.licencia.LicenciaID != null ? this.licencia.LicenciaID.ToString() : tks;
            if (this.licencia.ClaseLicencia != null)
                this.oUDT.UserFields.Fields.Item("U_ClaseLicencia").Value = this.licencia.ClaseLicencia;
            this.oUDT.UserFields.Fields.Item("U_Numero").Value = this.licencia.Numero ?? string.Empty;
            if (this.licencia.VigenciaLicencia != null)
                this.oUDT.UserFields.Fields.Item("U_VigenciaLicencia").Value = this.licencia.VigenciaLicencia;
            if (this.opid != null)
                this.oUDT.UserFields.Fields.Item("U_OperadorID").Value = this.opid;
            if (this.licencia.AvisoLicencia != null)
                this.oUDT.UserFields.Fields.Item("U_AvisoLicencia").Value = this.licencia.AvisoLicencia;
            this.oUDT.UserFields.Fields.Item("U_Estatus").Value = this.licencia.Enabled != null ? ((bool)this.licencia.Enabled ? 1 : 0) : 0; 
            this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Sincronizado < 0? 0 : Sincronizado;
            this.oUDT.UserFields.Fields.Item("U_UUID").Value = this.UUID == null ? string.Empty : this.UUID.ToString();
        }

        public void UDTToLicencia()
        {
            this.licencia = new Licencia();
            this.licencia.LicenciaID = string.IsNullOrEmpty(this.oUDT.Code) ? (int?)null : Convert.ToInt32(oUDT.Code);
            this.licencia.ClaseLicencia = string.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_ClaseLicencia").Value.ToString()) ? (TipoLicencia?)null : (TipoLicencia)(this.oUDT.UserFields.Fields.Item("U_ClaseLicencia").Value);
            this.licencia.Numero = string.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_Numero").Value.ToString()) ? string.Empty : this.oUDT.UserFields.Fields.Item("U_Numero").Value.ToString();
            this.licencia.VigenciaLicencia = this.oUDT.UserFields.Fields.Item("U_VigenciaLicencia").Value == null ? new DateTime() : DateTime.Parse(this.oUDT.UserFields.Fields.Item("U_VigenciaLicencia").Value.ToString());
            if (this.licencia.VigenciaLicencia != null && this.licencia.VigenciaLicencia.Value.Year < 1900)
            {
                this.licencia.VigenciaLicencia = null;
            }
            this.opid = this.oUDT.UserFields.Fields.Item("U_OperadorID").Value == null ? null : Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_OperadorID").Value.ToString());
            this.licencia.AvisoLicencia = this.oUDT.UserFields.Fields.Item("U_AvisoLicencia").Value == null ? null : Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_AvisoLicencia").Value.ToString());
            this.licencia.Enabled = this.oUDT.UserFields.Fields.Item("U_Estatus").Value == null ? false : (Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_Estatus").Value) == 1);
            this.Sincronizado = this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value == null ? null : (int?)this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value;
            this.UUID = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_UUID").Value.ToString()) ? null : Guid.Parse(this.oUDT.UserFields.Fields.Item("U_UUID").Value);
        }
        #endregion
        
        #region Acciones
        public void Insert()
        {
            try
            {
                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format("INSERT INTO {0} VALUES('{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}')", this.NombreTabla, this.licencia.LicenciaID, this.licencia.LicenciaID, Convert.ToInt32(this.licencia.ClaseLicencia), this.licencia.Numero, this.licencia.VigenciaLicencia.Value.ToString("yyyy/MM/dd"), this.opid, this.licencia.AvisoLicencia, Convert.ToInt32(this.licencia.Enabled), "0", this.UUID);

                this.Insertar.DoQuery(Parametro);
                #endregion SQL || Hana
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
            
        }

        public void Remove()
        {
            if (oUDT.GetByKey(licencia.LicenciaID.ToString()))
            {
                string ID = String.Empty;
                if (!String.IsNullOrEmpty(oUDT.Code))
                    ID = Convert.ToString(this.oUDT.Code);
                else
                {
                    throw new Exception("Erron al intentar obtener identificador. ");
                }

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code"" = '{1}'", this.NombreTabla, ID);

                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana

                #region SQL
                /*int flag = oUDT.Remove();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    Company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }*/
                #endregion SQL
            }
        }

        public void Update()
        {
            try
            {
                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET ""Code"" = '{1}', ""Name"" = '{2}', U_ClaseLicencia = '{3}', U_Numero = '{4}', U_VigenciaLicencia = '{5}', U_OperadorID = '{6}', U_AvisoLicencia = '{7}', U_Estatus = '{8}', U_Sincronizado = '{9}' WHERE ""Code"" = '{1}'", this.NombreTabla, this.licencia.LicenciaID, this.licencia.LicenciaID, Convert.ToInt32(this.licencia.ClaseLicencia), this.licencia.Numero, this.licencia.VigenciaLicencia.Value.ToString("yyyy/MM/dd"), this.opid, this.licencia.AvisoLicencia, Convert.ToInt32(this.licencia.Enabled), this.Sincronizado);

                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana
            }
            catch
            {
                #region SQL
                int flag = oUDT.Update();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    Company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL
            }
        }
        #endregion

        #region Services
        public Recordset Consultar(Licencia lic)
        {
            try
            {
                Recordset rs = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                if (lic == null) lic = new Licencia();
                

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(" SELECT * from " + this.NombreTabla + " as LIC ");

                if (lic.LicenciaID != null && lic.LicenciaID > 0)
                    where.Append(string.Format(@" and lic.""Code"" = {0} ", lic.LicenciaID));
                if (lic.ClaseLicencia != null)
                    where.Append(string.Format(@" and lic.U_CLASELICENCIA = {0} ", (int)lic.ClaseLicencia));
                if (!string.IsNullOrEmpty(lic.Numero))
                    where.Append(string.Format(" and lic.U_Numero = '{0}' ", lic.Numero));
                if (lic.VigenciaLicencia != null)
                    where.Append(string.Format(" and lic.U_VigenciaLicencia = {0} ", lic.VigenciaLicencia));
                if (opid != null)
                    where.Append(string.Format(" and lic.U_OperadorID = {0} ", opid));
                if (lic.AvisoLicencia != null)
                    where.Append(string.Format(" and lic.U_AvisoLicencia = {0} ", lic.AvisoLicencia));
                if (lic.Enabled != null)
                    where.Append(string.Format(" and lic.U_Estatus = {0} ", (bool)lic.Enabled ? 1 : 0));
                if (Sincronizado != null)
                    where.Append(string.Format(" and lic.U_Sincronizado = {0} ", Sincronizado));
                if (UUID != null)
                    where.Append(string.Format(" and lic.U_UUID = '{0}' ", UUID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                query.Append(@" order by lic.""Code"" desc ");
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Licencia> RecordSetToListLicencia(Recordset rs)
        {
            var result = new List<Licencia>();
            for (int i = 0; i < rs.RecordCount; i++)
            {
                string itemcode = rs.Fields.Item("Code").Value;
                this.oUDT.GetByKey(itemcode);
                UDTToLicencia();
                var temp = Clone(licencia);
                result.Add(temp);
                rs.MoveNext();
            }
            return result;
        }

        public Recordset ActualizarCode(Licencia lic)
        {
            try
            {
                Recordset rs = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(" UPDATE " + this.NombreTabla + @" SET ""Code"" = '" + lic.LicenciaID + @"', ""Name"" = '" + lic.LicenciaID +
                    "', U_Sincronizado = " + Sincronizado);

                if (UUID != null)
                    where.Append(string.Format(" and U_UUID like '%{0}%' ", UUID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private Licencia Clone(Licencia lic)
        {
            var result = new Licencia();
            result.LicenciaID = lic.LicenciaID;
            result.ClaseLicencia = lic.ClaseLicencia;
            result.Numero = lic.Numero;
            result.VigenciaLicencia = lic.VigenciaLicencia;
            result.Enabled = lic.Enabled;
            result.AvisoLicencia = lic.AvisoLicencia;
            return result;
        }

        #endregion

        #endregion
    }
}
