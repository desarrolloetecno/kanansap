﻿using System;
using System.Collections.Generic;
using System.Text;
using Kanan.Comun.BO2;
using Kanan.Operaciones.BO2;
using SAPbobsCOM;

namespace KananSAP.Operaciones.Data
{
    public class OperadorSAP
    {
        #region Propiedades
        private const string TABLE_SERVICE_NAME = "VSKF_OPERADOR";
        public Operador operador { get; set; }
        public UserTable oUDT { get; set; }
        public string ItemCode { get; set; }
        private Company Company;
        public int? Sincronizado { get; set; }
        public Guid? UUID { get; set; }

        //Migracion Hana
        Recordset Insert;
        String Parametro;
        private String NombreTabla;
        bool EsSQL;

        //Se agregó para ser visible desde cualquier lugar.
        public String NombreTablaOperadorSAP = String.Empty;

        public bool ExistUDT()
        {
            return oUDT.GetByKey(this.ItemCode);
        }
        #endregion

        #region Constructor
        public OperadorSAP(SAPbobsCOM.Company company)
        {
            this.Company = company;
            this.operador = new Operador
            {
                Propietario = new Empresa(),
                SubPropietario = new Sucursal(),
                Picture = new Imagen()
            };
            try
            {
                this.oUDT = Company.UserTables.Item(TABLE_SERVICE_NAME);
                //Migración HANA
                this.Insert = (Recordset)this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.EsSQL = Company.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.NombreTabla = EsSQL ? "[@VSKF_OPERADOR]" : @"""@VSKF_OPERADOR""";
                //Se maneja en una variable pública para poder utilizar el método desde cualquier lugar.
                this.NombreTablaOperadorSAP = this.NombreTabla;
            }
            catch (Exception)
            {
                this.oUDT = null;
            }
        }
        #endregion

        #region Métodos

        #region Metodos Datos
        public void OperadorToUDT()
        {
            oUDT.Code = this.operador.OperadorID.ToString();
            oUDT.Name = this.operador.OperadorID.ToString();
            this.oUDT.UserFields.Fields.Item("U_NOMBRE").Value = this.operador.Nombre ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_APELLIDO1").Value = this.operador.Apellido1 ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_APELLIDO2").Value = this.operador.Apellido2 ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_DIRECCION").Value = this.operador.Direccion ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_TELEFONO").Value = this.operador.Telefono ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_CELULAR").Value = this.operador.Celular ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_CORREO").Value = this.operador.Correo ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_CURP").Value = this.operador.Curp ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_FECHANAC").Value = this.operador.FechaNacimiento != null ? this.operador.FechaNacimiento : new DateTime();
            this.oUDT.UserFields.Fields.Item("U_LUGARNAC").Value = this.operador.LugarNacimiento ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_ALIAS").Value = this.operador.Alias ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_EMPRESAID").Value = this.operador.Propietario.EmpresaID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_SUCURSALID").Value = this.operador.SubPropietario.SucursalID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_CORREOSNOT").Value = this.operador.CorreosNotificacion ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_ESACTIVO").Value = this.operador.EsActivo != null ? ((bool)this.operador.EsActivo ? 1 : 0) : 0; ;
            this.oUDT.UserFields.Fields.Item("U_IMAGENID").Value = this.operador.Picture.ImagenID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_EMPLEADOID").Value = this.operador.EmpleadoID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_SINCRONIZADO").Value = this.Sincronizado < 0 ? 0 : Sincronizado;
            this.oUDT.UserFields.Fields.Item("U_UUID").Value = this.UUID == null ? string.Empty : this.UUID.ToString();
            this.oUDT.UserFields.Fields.Item("U_IMAGENPERFIL").Value = !string.IsNullOrEmpty(this.operador.sCadenaImagen) ? this.operador.sCadenaImagen : string.Empty;
            this.oUDT.UserFields.Fields.Item("U_FONDOFIJO").Value = this.operador.FondoFijo == null ? 0 : Convert.ToDouble(this.operador.FondoFijo);
            this.oUDT.UserFields.Fields.Item("U_CARDCODE").Value = string.IsNullOrEmpty(this.operador.sCardCode) ? "" : this.operador.sCardCode;
        }

        public void UDTToOperador()
        {
            this.operador = new Operador
            {
                Propietario = new Empresa(),
                SubPropietario = new Sucursal(),
                Picture = new Imagen()
            };

            this.operador.OperadorID = Convert.ToInt32(this.oUDT.Code) == 0 ? 0 : Convert.ToInt32(this.oUDT.Code);
            this.operador.Nombre = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_NOMBRE").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_NOMBRE").Value;
            this.operador.Apellido1 = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_APELLIDO1").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_APELLIDO1").Value;
            this.operador.Apellido2 = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_APELLIDO2").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_APELLIDO2").Value;
            this.operador.Direccion = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Direccion").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Direccion").Value;
            this.operador.Telefono = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Telefono").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Telefono").Value;
            this.operador.Celular = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Celular").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Celular").Value;
            this.operador.Correo = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Correo").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Correo").Value;
            this.operador.Curp = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_CURP").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_CURP").Value;
            this.operador.FechaNacimiento = this.oUDT.UserFields.Fields.Item("U_Fechanac").Value == null ? new DateTime() : (DateTime)this.oUDT.UserFields.Fields.Item("U_Fechanac").Value;
            this.operador.LugarNacimiento = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Lugarnac").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Lugarnac").Value;
            this.operador.Alias = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Alias").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Alias").Value;
            this.operador.Propietario.EmpresaID = (int)this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value;
            this.operador.SubPropietario.SucursalID = (int)this.oUDT.UserFields.Fields.Item("U_SucursalID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_SucursalID").Value;
            this.operador.CorreosNotificacion = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Correosnot").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Correosnot").Value;
            this.operador.EsActivo = this.oUDT.UserFields.Fields.Item("U_EsActivo").Value == 1;
            this.operador.Picture.ImagenID = this.oUDT.UserFields.Fields.Item("U_ImagenID").Value == null ? null : (int?)this.oUDT.UserFields.Fields.Item("U_ImagenID").Value;
            this.operador.EmpleadoID = this.oUDT.UserFields.Fields.Item("U_EmpleadoID").Value == null ? null : (int?)this.oUDT.UserFields.Fields.Item("U_EmpleadoID").Value;
            this.Sincronizado = (int)this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value;
            this.UUID = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_UUID").Value) ? new Guid() : Guid.Parse(this.oUDT.UserFields.Fields.Item("U_UUID").Value.ToString());
            this.operador.sCadenaImagen = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_ImagenPerfil").Value) ? "" : this.oUDT.UserFields.Fields.Item("U_ImagenPerfil").Value.ToString();
            this.operador.FondoFijo = this.oUDT.UserFields.Fields.Item("U_FondoFijo").Value == null ? 0 : Convert.ToDouble(this.oUDT.UserFields.Fields.Item("U_FondoFijo").Value);

            this.operador.sCardCode = this.oUDT.UserFields.Fields.Item("U_CardCode").Value == null ? "" : Convert.ToString(this.oUDT.UserFields.Fields.Item("U_CardCode").Value);

            
        }
        #endregion

        #region Acciones

        public void Insertar()
        {
            try
            {
                if (this.operador == null)
                    throw new Exception("El objeto operador no puede ser nulo");

                 int flag = oUDT.Add();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    Company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }

            }
            catch (Exception ex)
            {
                #region SQL
                int flag = oUDT.Add();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    Company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL
            }
        }

        public void Update()
        {
            try
            {
                if (this.operador == null)
                    throw new Exception("El objeto operador no puede ser nulo");

                #region Fechas
                String FechaNacimiento = String.Empty;
                FechaNacimiento = this.operador.FechaNacimiento != null ? this.operador.FechaNacimiento.Value.ToString("yyyy-MM-dd") : String.Empty;
                #endregion Fechas

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET ""Code"" = '{1}', ""Name"" = '{2}', ""U_NOMBRE"" = '{3}', ""U_APELLIDO1"" = '{4}', U_Apellido2 = '{5}', U_Direccion = '{6}', U_Telefono = '{7}', U_Celular = '{8}', U_Correo = '{9}', U_CURP = '{10}', U_Fechanac = '{11}', U_Lugarnac = '{12}', U_Alias = '{13}', U_EmpresaID = '{14}', U_SucursalID = '{15}', U_Correosnot = '{16}', U_EsActivo = '{17}', U_ImagenID = '{18}', U_EmpleadoID = '{19}', U_Sincronizado = '{20}' ", this.NombreTabla, Convert.ToString(this.operador.OperadorID), Convert.ToString(this.operador.Nombre), Convert.ToString(this.operador.Nombre), Convert.ToString(this.operador.Apellido1), Convert.ToString(this.operador.Apellido2), Convert.ToString(this.operador.Direccion), Convert.ToString(this.operador.Telefono), Convert.ToString(this.operador.Celular), Convert.ToString(this.operador.Correo), Convert.ToString(this.operador.Curp), FechaNacimiento, Convert.ToString(this.operador.LugarNacimiento), Convert.ToString(this.operador.Alias), Convert.ToString(this.operador.Propietario.EmpresaID), Convert.ToString(this.operador.SubPropietario.SucursalID), Convert.ToString(this.operador.CorreosNotificacion), "1", Convert.ToString(this.operador.Picture.ImagenID), Convert.ToString(this.operador.EmpleadoID), Convert.ToString(this.Sincronizado));
                if (!string.IsNullOrEmpty(this.operador.sCadenaImagen))
                    this.Parametro += string.Format(", U_ImagenPerfil = '{0}'", this.operador.sCadenaImagen);
                if (this.operador.FondoFijo != null)
                    this.Parametro += string.Format(", U_FondoFijo = {0}", this.operador.FondoFijo);

                if (!string.IsNullOrEmpty( this.operador.sCardCode))
                    this.Parametro += string.Format(", CardCode = {0}", this.operador.sCardCode);

                this.Parametro += string.Format(@"  WHERE ""Code"" = '{0}'", Convert.ToString(this.operador.OperadorID));
                this.Insert.DoQuery(this.Parametro);
                #endregion SQL || Hana
            }

            catch
            {
                #region SQL
                int flag = oUDT.Update();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    Company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL
            }
        }

        public void Remove()
        {
            try
            {
                if (oUDT.GetByKey(this.ItemCode))
                {
                    #region ObtenerID
                    String ID = String.Empty;
                    if (!String.IsNullOrEmpty(this.oUDT.Code))
                        ID = this.oUDT.Code;
                    else
                    {
                        throw new Exception("Erron al intentar obtener identificador. ");
                    }
                    #endregion ObtenerID

                    #region SQL || Hana
                    this.Parametro = String.Empty;
                    this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code"" = '{1}'", this.NombreTabla, ID);
                    this.Insert.DoQuery(this.Parametro);
                    #endregion SQL || Hana
                }
            }
            catch
            {
                #region SQL
                int flag = oUDT.Remove();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    Company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL
            }
        }

        public void Deactivate()
        {
            this.oUDT.GetByKey(this.ItemCode);

            #region ObtenerID
            String ID = String.Empty;
            if (!String.IsNullOrEmpty(oUDT.Code))
                ID = oUDT.Code;
            else
            {
                throw new Exception("Erron al intentar obtener identificador. ");
            }
            #endregion ObtenerID

            #region SQL || Hana
            this.Parametro = String.Empty;
            this.Parametro = String.Format(@"UPDATE {0} ""SET U_ESACTIVO""= '0' WHERE ""Code"" = '{1}'", this.NombreTabla, ID);

            this.Insert.DoQuery(this.Parametro);
            #endregion SQl || Hana

            #region SQL
            //this.oUDT.UserFields.Fields.Item("U_EsActivo").Value = 0;
            //Update();
            #endregion SQL

        }
        #endregion

        #region Services

        public int GetUserID()
        {
            int UserID = 0;

            Recordset rs = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            StringBuilder query = new StringBuilder();
            query.Append(@"SELECT * from ""OUSR"" ORDER BY ""USERID"" DESC ");
            UserID = rs.Fields.Item("U_SUCURSALID").Value;

            return UserID;
        }

        public Recordset Consultar(Operador op)
        {
            try
            {
                Recordset rs = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                if (op == null) op = new Operador();
                if (op.Propietario == null) op.Propietario = new Empresa();
                if (op.SubPropietario == null) op.SubPropietario = new Sucursal();
                if (op.Picture == null) op.Picture = new Imagen();


                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(" SELECT * from " + this.NombreTabla + " as OP ");

                if (op.OperadorID != null && op.OperadorID > 0)
                    where.Append(string.Format(@" and op.""Code"" = {0} ", op.OperadorID));
                if (!string.IsNullOrEmpty(op.Nombre))
                    where.Append(string.Format(@" and op.""U_NOMBRE"" = '{0}' ", op.Nombre));
                if (!string.IsNullOrEmpty(op.Apellido1))
                    where.Append(string.Format(@" and op.""U_APELLIDO1"" = '{0}' ", op.Apellido1));
                if (!string.IsNullOrEmpty(op.Apellido2))
                    where.Append(string.Format(@" and op.U_Apellido2 = '{0}' ", op.Apellido2));
                if (!string.IsNullOrEmpty(op.Direccion))
                    where.Append(string.Format(@" and op.U_Direccion = '{0}' ", op.Direccion));
                if (!string.IsNullOrEmpty(op.Telefono))
                    where.Append(string.Format(@" and op.U_Telefono = '{0}' ", op.Telefono));
                if (!string.IsNullOrEmpty(op.Celular))
                    where.Append(string.Format(@" and op.U_Celular = '{0}' ", op.Celular));
                if (!string.IsNullOrEmpty(op.Correo))
                    where.Append(string.Format(@" and op.U_Correo = '{0}' ", op.Correo));
                if (!string.IsNullOrEmpty(op.Curp))
                    where.Append(string.Format(@" and op.U_CURP = '{0}' ", op.Curp));
                if (op.FechaNacimiento != null)
                    where.Append(string.Format(@" and op.U_Fechanac = {0} ", op.FechaNacimiento));
                if (!string.IsNullOrEmpty(op.LugarNacimiento))
                    where.Append(string.Format(@" and op.U_Lugarnac = '{0}' ", op.LugarNacimiento));
                if (!string.IsNullOrEmpty(op.Alias))
                    where.Append(string.Format(@" and op.U_Alias = '{0}' ", op.Alias));
                if (op.Propietario.EmpresaID != null)
                    where.Append(string.Format(@" and op.U_EmpresaID = '{0}' ", op.Propietario.EmpresaID));
                if (op.SubPropietario.SucursalID != null)
                    where.Append(string.Format(@" and op.U_SucursalID = '{0}' ", op.SubPropietario.SucursalID));
                if (!string.IsNullOrEmpty(op.CorreosNotificacion))
                    where.Append(string.Format(@" and op.U_Correosnot = '{0}' ", op.CorreosNotificacion));
                if (op.EsActivo != null)
                    where.Append(string.Format(@" and op.U_EsActivo = '{0}' ", (bool)op.EsActivo ? 1 : 0));
                if (op.Picture.ImagenID != null)
                    where.Append(string.Format(@" and op.U_ImagenID = '{0}' ", op.Picture.ImagenID));
                if (op.EmpleadoID != null)
                    where.Append(string.Format(@" and op.U_EmpleadoID = '{0}' ", op.EmpleadoID));
                if (Sincronizado != null)
                    where.Append(string.Format(@" and op.U_Sincronizado = '{0}' ", Sincronizado));
                if (UUID != null)
                    where.Append(string.Format(@" and op.U_UUID = '{0}' ", UUID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                query.Append(@" order by op.""Code"" desc ");
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Recordset ActualizarCode(Operador op)
        {
            try
            {
                Recordset rs = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(@" UPDATE " + this.NombreTabla + @" SET ""Code"" = '" + op.OperadorID +
                    @"', U_Sincronizado = " + Sincronizado +
                    @" , U_EmpleadoID = " + operador.EmpleadoID);

                if (UUID != null)
                    where.Append(string.Format(@" and ""U_UUID"" like '%{0}%' ", UUID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Operador> RecordSetToListOperador(Recordset rs)
        {
            var result = new List<Operador>();
            for (int i = 0; i < rs.RecordCount; i++)
            {
                this.ItemCode = rs.Fields.Item("Code").Value;
                this.oUDT.GetByKey(this.ItemCode);
                UDTToOperador();
                var temp = (Operador)this.operador.Clone();
                result.Add(temp);
            }
            return result;
        }
        #endregion

        #region Metodos Auxiliares
        public void SetOperadorByEmpleadoID(int empleadoid)
        {
            this.Sincronizado = null;
            this.UUID = null;
            var rs = this.Consultar(new Operador { EmpleadoID = empleadoid });
            this.ItemCode = rs.Fields.Item("Code").Value;
            if (string.IsNullOrEmpty(this.ItemCode))
            {
                //Se obtiene el del empleado
                this.ItemCode = empleadoid.ToString();
                return;
            }
            this.oUDT.GetByKey(this.ItemCode);
            UDTToOperador();
        }

        public void SetSucursal(int? sucursalID)
        {
            try
            {
                String ID = String.Empty;
                if (!String.IsNullOrEmpty(oUDT.Code))
                    ID = oUDT.Code;
                else
                {
                    throw new Exception("Erron al intentar obtener identificador. ");
                }

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET ""U_SUCURSALID = '{1}' WHERE ""Code"" = '{2}'", this.NombreTabla, sucursalID, ID);

                this.Insert.DoQuery(this.Parametro);
                #endregion SQL || Hana

                #region SQL
                /*this.oUDT.UserFields.Fields.Item("U_SucursalID").Value = sucursalID == null ? 0 : sucursalID;
                this.Update();*/
                #endregion SQL
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        #endregion

        #endregion
    }
}
