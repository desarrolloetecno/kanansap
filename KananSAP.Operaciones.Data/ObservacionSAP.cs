﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kanan.Operaciones.BO2;
using SAPbobsCOM;

namespace KananSAP.Operaciones.Data
{
    public class ObservacionSAP
    {
        #region Propiedades
        private const string TABLE_SERVICE_NAME = "VSKF_OBSERVACION";
        public Observacion observacion { get; set; }
        public UserTable oUDT { get; set; }
        public int? opid { get; set; }
        private Company Company;
        public int? Sincronizado { get; set; }
        public Guid? UUID { get; set; }

        //Migracion Hana
        Recordset Insertar;
        String Parametro;
        private String NombreTabla;
        bool EsSQL;

        //Se agregó para ser visible desde cualquier lugar consulta.
        public String NombreTablaObservacionSAP = String.Empty;

        //public bool ExistUDT()
        //{
        //    return oUDT.GetByKey(this.ItemCode);
        //}
        #endregion

        #region Constructor
        public ObservacionSAP(SAPbobsCOM.Company company)
        {
            this.Company = company;
            this.observacion = new Observacion();
            try
            {
                this.oUDT = company.UserTables.Item(TABLE_SERVICE_NAME);

                //Migración HANA
                this.Insertar = (Recordset)this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.EsSQL = Company.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.NombreTabla = EsSQL ? "[@VSKF_OBSERVACION]" : @"""@VSKF_OBSERVACION""";
                //Se maneja en una variable pública para poder utilizar desde cualquier método.
                this.NombreTablaObservacionSAP = this.NombreTabla;
            }
            catch (Exception)
            {
                this.oUDT = null;
            }
        } 
        #endregion

        #region Métodos

        #region Metodos Datos

        public void observacionToUDT()
        {
            string tks = DateTime.Now.Ticks.ToString();
            oUDT.Code = this.observacion.ObservacionID != null ? this.observacion.ObservacionID.ToString() : tks;
            oUDT.Name = this.observacion.ObservacionID != null ? this.observacion.ObservacionID.ToString() : tks;
            this.oUDT.UserFields.Fields.Item("U_Descripcion").Value = this.observacion.Descripcion ?? string.Empty;
            if (this.observacion.FechaObservacion != null)
                this.oUDT.UserFields.Fields.Item("U_FechaObservacion").Value = this.observacion.FechaObservacion;
            if (this.observacion.FechaRegistro != null)
                this.oUDT.UserFields.Fields.Item("U_FechaRegistro").Value = observacion.FechaRegistro;
            if (this.opid != null)
                this.oUDT.UserFields.Fields.Item("U_OperadorID").Value = this.opid;
            //this.oUDT.UserFields.Fields.Item("U_Estatus").Value = this.observacion.Enabled != null ? ((bool)this.observacion.Enabled ? 1 : 0) : 0; ;
            this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Sincronizado < 0? 0 : Sincronizado;
            this.oUDT.UserFields.Fields.Item("U_UUID").Value = this.UUID == null ? string.Empty : this.UUID.ToString();
        }

        public void UDTToObservacion()
        {
            this.observacion = new Observacion();
            this.observacion.ObservacionID = string.IsNullOrEmpty(this.oUDT.Code) ? (int?)null : Convert.ToInt32(oUDT.Code);
            var time = this.oUDT.UserFields.Fields.Item("U_FechaObservacion").Value.ToString();
            this.observacion.FechaObservacion = this.oUDT.UserFields.Fields.Item("U_FechaObservacion").Value == null ? new DateTime() : DateTime.Parse(time);
            if (this.observacion.FechaObservacion != null && this.observacion.FechaObservacion.Value.Year < 1900)
            {
                this.observacion.FechaObservacion = null;
            }
            this.observacion.FechaRegistro = this.oUDT.UserFields.Fields.Item("U_FechaRegistro").Value == null ? new DateTime() : DateTime.Parse(this.oUDT.UserFields.Fields.Item("U_FechaRegistro").Value.ToString());
            if (this.observacion.FechaRegistro != null && this.observacion.FechaRegistro.Value.Year < 1900)
            {
                this.observacion.FechaRegistro = null;
            }
            this.observacion.Descripcion = string.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_Descripcion").Value.ToString()) ? string.Empty : this.oUDT.UserFields.Fields.Item("U_Descripcion").Value.ToString();
            this.opid = this.oUDT.UserFields.Fields.Item("U_OperadorID").Value == null ? null : Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_OperadorID").Value.ToString());
            this.Sincronizado = this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value == null ? null : (int?)this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value;
            this.UUID = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_UUID").Value.ToString()) ? null : Guid.Parse(this.oUDT.UserFields.Fields.Item("U_UUID").Value);
        }
        #endregion
        
        #region Acciones
        public void Insert()
        {
            try
            {
                this.Parametro = String.Empty;
                this.Parametro = String.Format("INSERT INTO {0} VALUES('{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')", this.NombreTabla, this.oUDT.Code, this.oUDT.Name, this.observacion.Descripcion, this.observacion.FechaRegistro.Value.ToString("yyyy/MM/dd"), this.observacion.FechaObservacion.Value.ToString("yyyy/MM/dd"), this.opid, "0", this.UUID);

                this.Insertar.DoQuery(Parametro);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Remove()
        {
            if (oUDT.GetByKey(observacion.ObservacionID.ToString()))
            {
                string ID = String.Empty;
                if (!String.IsNullOrEmpty(oUDT.Code))
                    ID = oUDT.Code;
                else
                {
                    throw new Exception("Erron al intentar obtener identificador. ");
                }

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code"" = '{1}'", this.NombreTabla, ID);

                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana

                #region SQL
                /*int flag = oUDT.Remove();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    Company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }*/
                #endregion SQL
            }
        }

        public void Update()
        {
            try
            {
                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET ""Code"" = '{1}', ""Name"" = '{2}', U_Descripcion = '{3}', U_FechaRegistro = '{4}', U_FechaObservacion = '{5}', U_OperadorID = '{6}', U_Sincronizado = '{7}' WHERE ""Code"" = '{1}'", this.NombreTabla, this.oUDT.Code, this.oUDT.Name, this.observacion.Descripcion, this.observacion.FechaRegistro.Value.ToString("yyyy/MM/dd"), this.observacion.FechaObservacion.Value.ToString("yyyy/MM/dd"), this.opid, this.Sincronizado);

                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana
            }
            catch
            {
                #region SQL
                int flag = oUDT.Update();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    Company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL
            }
        }
        #endregion

        #region Services
        public Recordset Consultar(Observacion obs)
        {
            try
            {
                Recordset rs = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                if (obs == null) obs = new Observacion();
                

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                //Verificar por qué está escrito el minúsculas el nombre de la tabla.
                query.Append(" SELECT * from " + this.NombreTabla + " as OBS ");

                if (obs.ObservacionID!= null && obs.ObservacionID > 0)
                    where.Append(string.Format(@" and obs.""Code"" = {0} ", obs.ObservacionID));
                if (!string.IsNullOrEmpty(obs.Descripcion))
                    where.Append(string.Format(" and obs.U_Descripcion = '{0}' ", obs.Descripcion));
                if (obs.FechaRegistro != null)
                    where.Append(string.Format(" and obs.U_FechaRegistro = {0} ", obs.FechaRegistro));
                if (obs.FechaObservacion != null)
                    where.Append(string.Format(" and obs.U_FechaObservacion = {0} ", obs.FechaObservacion));
                if (opid != null)
                    where.Append(string.Format(" and obs.U_OperadorID = {0} ", opid));
                if (Sincronizado != null)
                    where.Append(string.Format(" and obs.U_Sincronizado = {0} ", Sincronizado));
                if (UUID != null)
                    where.Append(string.Format(" and obs.U_UUID = '{0}' ", UUID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                query.Append(@" order by obs.""Code"" desc ");
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Observacion> RecordSetToListObservacion(Recordset rs)
        {
            var result = new List<Observacion>();
            rs.MoveFirst();
            for (int i = 0; i < rs.RecordCount; i++)
            {
                string itemcode = rs.Fields.Item("Code").Value;
                this.oUDT.GetByKey(itemcode);
                UDTToObservacion();
                var temp = Clone(observacion);
                result.Add(temp);
                rs.MoveNext();
            }
            return result;
        }

        public Recordset ActualizarCode(Observacion obs)
        {
            try
            {
                Recordset rs = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(" UPDATE " + this.NombreTabla + @" SET ""Code"" = '" + obs.ObservacionID + @"', ""Name"" = '" + obs.ObservacionID + "', U_Sincronizado = " + Sincronizado);

                if (UUID != null)
                    where.Append(string.Format(" and U_UUID like '%{0}%' ", UUID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private Observacion Clone(Observacion obs)
        {
            var result = new Observacion();
            result.ObservacionID = obs.ObservacionID;
            result.Descripcion = obs.Descripcion;
            result.FechaObservacion = obs.FechaObservacion;
            result.FechaRegistro = obs.FechaRegistro;
            return result;
        }

        #endregion

        #endregion
    }
}
