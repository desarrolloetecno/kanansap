﻿using Kanan.Alertas.BO2;
using Kanan.Mantenimiento.BO2;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KananSAP.Alertas.Data
{
    public class AlertaSAP
    {
         #region Atributos
        private const string TABLE_ALERTA = "VSKF_ALERTA";
        public Alerta Alerta { get; set; }
        public UserTable oUDT { get; set; }
        private Company oCompany;
        public string ItemCode { get; set; }

        public AlertaMantenimientoSAP AlertaMtto;

        //Migracion Hana
        Recordset Insertar;
        String Parametro;
        private String NombreTabla;
        bool EsSQL;

        public String NombreTablaAlerta;
         #endregion

        #region Contructor
        public AlertaSAP(Company company)
        {
            this.oCompany = company;
            try
            {
                this.oUDT = this.oCompany.UserTables.Item(TABLE_ALERTA);

                //Migración HANA
                this.Insertar = (Recordset)this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.EsSQL = oCompany.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.NombreTabla = EsSQL ? "[@VSKF_ALERTA]" : @"""@VSKF_ALERTA""";

                NombreTablaAlerta = NombreTabla;

                AlertaMtto = new AlertaMantenimientoSAP(this.oCompany);
            }
            catch
            {
                this.oUDT = null;
            }
        }
        #endregion

        public bool Existe()
        {
            return this.oUDT.GetByKey(this.ItemCode);
        }

        public void AlertaToUDT()
        {
            try
            {
                if (Alerta.CurrentUser == null) Alerta.CurrentUser = new Etecno.Security2.BO.Usuario();
                if (Alerta.Company == null) throw new Exception("Empresa no puede ser nulo");
                if (Alerta.SubPropietario == null) Alerta.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
                if (Alerta.IAlertaID == null) throw new Exception("IAlertaID no puede ser nulo");
                this.oUDT.Code = this.Alerta.AlertaID.ToString();
                this.oUDT.Name = this.Alerta.AlertaID.ToString();
                this.oUDT.UserFields.Fields.Item("U_AlertaID").Value = this.Alerta.AlertaID;
                this.oUDT.UserFields.Fields.Item("U_Descripcion_Es").Value = this.Alerta.Descripcion_Es;
                this.oUDT.UserFields.Fields.Item("U_Descripcion_En").Value = this.Alerta.Descripcion_En;
                this.oUDT.UserFields.Fields.Item("U_Descripcion_Pt").Value = this.Alerta.Descripcion_Pt;
                this.oUDT.UserFields.Fields.Item("U_Descripcion_Fr").Value = this.Alerta.Descripcion_Fr;
                this.oUDT.UserFields.Fields.Item("U_UsuarioID").Value = this.Alerta.CurrentUser.UsuarioID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value = this.Alerta.Company.EmpresaID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_SucursalID").Value = this.Alerta.SubPropietario.SucursalID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_EstaActivo").Value = Convert.ToInt32(this.Alerta.Activa);
                this.oUDT.UserFields.Fields.Item("U_IAlertaID").Value = this.Alerta.IAlertaID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_TipoAlerta").Value = this.Alerta.TipoAlerta ?? 0;
                this.oUDT.UserFields.Fields.Item("U_TypeOfType").Value = this.Alerta.TypeOfType ?? 0;
                //this.oUDT.UserFields.Fields.Item("U_NumEnviados").Value = this.Alerta.NumEnviados;
                //this.oUDT.UserFields.Fields.Item("U_NumEnviadosTiempo").Value = 0;
                //this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Alerta.AlertaID;
                //this.oUDT.UserFields.Fields.Item("U_UUID").Value = this.Alerta.AlertaID;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        private void PrepareAlerta()
        {
            this.Alerta = new Alerta();
            this.Alerta.Company = new Kanan.Operaciones.BO2.Empresa();
            this.Alerta.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
            this.Alerta.CurrentUser = new Etecno.Security2.BO.Usuario();
        }

        public void UDTToAlerta()
        {
            try
            {
                PrepareAlerta();
                this.oUDT.Code = this.Alerta.AlertaID.ToString();
                this.oUDT.Name = this.Alerta.AlertaID.ToString();
                this.Alerta.AlertaID = this.oUDT.UserFields.Fields.Item("U_AlertaID").Value ?? null;
                this.Alerta.Descripcion_Es = this.oUDT.UserFields.Fields.Item("U_Descripcion_Es").Value ?? null;
                this.Alerta.Descripcion_En = this.oUDT.UserFields.Fields.Item("U_Descripcion_En").Value ?? null;
                this.Alerta.Descripcion_Pt = this.oUDT.UserFields.Fields.Item("U_Descripcion_Pt").Value ?? null;
                this.Alerta.Descripcion_Fr = this.oUDT.UserFields.Fields.Item("U_Descripcion_Fr").Value ?? null;
                this.Alerta.CurrentUser.UsuarioID = this.oUDT.UserFields.Fields.Item("U_UsuarioID").Value ?? null;
                this.Alerta.Company.EmpresaID = this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value ?? null;
                this.Alerta.SubPropietario.SucursalID = this.oUDT.UserFields.Fields.Item("U_SucursalID").Value ?? null;
                this.Alerta.Activa = this.oUDT.UserFields.Fields.Item("U_EstaActivo").Value ?? null;
                this.Alerta.IAlertaID = this.oUDT.UserFields.Fields.Item("U_IAlertaID").Value ?? null;
                this.Alerta.TipoAlerta = this.oUDT.UserFields.Fields.Item("U_TipoAlerta").Value ?? null;
                this.Alerta.TypeOfType = this.oUDT.UserFields.Fields.Item("U_TypeOfType").Value ?? null;
                //this.oUDT.UserFields.Fields.Item("U_NumEnviados").Value = this.Alerta.NumEnviados;
                //this.oUDT.UserFields.Fields.Item("U_NumEnviadosTiempo").Value = 0;
                //this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Alerta.AlertaID;
                //this.oUDT.UserFields.Fields.Item("U_UUID").Value = this.Alerta.AlertaID;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public bool SetUDTPMantenimientoID(int? id)
        {
            try
            {
                var rs = this.ConsultarByID(id);
                if (HashAlertas(rs))
                {
                    rs.MoveFirst();
                    int _id = rs.Fields.Item("U_AlertaID").Value;
                    if (_id == id)
                        return this.oUDT.GetByKey(rs.Fields.Item("Code").Value);
                }
                this.oUDT.Code = string.Empty;
                return false;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public int GetVehiculoID(int AlertaID)
        {
            try
            {
                int AlertaMantenimientoID = 0;
                int VehiculoID = 0;

                Insertar.DoQuery("select U_IAlertaID from " + this.NombreTabla + " where U_AlertaID = " + AlertaID);
                AlertaMantenimientoID = Convert.ToInt32(this.Insertar.Fields.Item("U_IAlertaID").Value);
                if (AlertaMantenimientoID > 0)
                {
                    Insertar.DoQuery("select U_MantenibleID from " + this.AlertaMtto.NombreTablaAlertaMantenimiento + " where U_AlertaMttoID = " + AlertaMantenimientoID);
                    VehiculoID = Convert.ToInt32(this.Insertar.Fields.Item("U_MantenibleID").Value);
                }

                return VehiculoID;
            }
            catch (Exception ex)
            {
                throw new Exception("Error en GetAlertaMantenimientoID");
            }
        }

        public int GetServicioID(int AlertaID)
        {
            try
            {
                int AlertaMantenimientoID = 0;
                int ServicioID = 0;

                Insertar.DoQuery("select U_IAlertaID from " + this.NombreTabla + " where U_AlertaID = " + AlertaID);
                AlertaMantenimientoID = Convert.ToInt32(this.Insertar.Fields.Item("U_IAlertaID").Value);
                if (AlertaMantenimientoID > 0)
                {
                    Insertar.DoQuery("select U_ServicioID from " + this.AlertaMtto.NombreTablaAlertaMantenimiento + " where U_AlertaMttoID = " + AlertaMantenimientoID);
                    ServicioID = Convert.ToInt32(this.Insertar.Fields.Item("U_ServicioID").Value);
                }

                return ServicioID;
            }
            catch (Exception ex)
            {
                throw new Exception("Error en GetServicioID");
            }
        }

        public Recordset ConsultarByID(int? id)
        {
            try
            {
                string query = string.Format("select * from {0} where U_AlertaID = '{1}'", this.NombreTabla, id);
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                return rs;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public bool HashAlertas(Recordset rs)
        {
            if (rs.RecordCount > 0)
                return true;
            return false;
        }

        public void Insert()
        {
            //int result = -1;
            try
            {

                #region SQL || HANA
                this.Parametro = String.Empty;
                this.Parametro = String.Format("INSERT INTO {0} VALUES ('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', '{19}')", this.NombreTabla, this.Alerta.AlertaID, this.Alerta.AlertaID, this.Alerta.AlertaID, this.Alerta.Descripcion_Es, this.Alerta.Descripcion_En, this.Alerta.Descripcion_Pt, this.Alerta.Descripcion_Fr, this.Alerta.CurrentUser.UsuarioID, this.Alerta.SubPropietario.Base.EmpresaID, this.Alerta.SubPropietario.SucursalID, this.Alerta.Activa, this.Alerta.IAlertaID, this.Alerta.TipoAlerta.Value, this.Alerta.TypeOfType, this.Alerta.NumEnviados, "0", "0", "0", "");
                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || HANA

                #region SQL
                /*result = this.oUDT.Add();
                if (result != 0)
                {
                    int errornum;
                    string errormsj;
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }*/
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Actualizar()
        {
            //int result = -1;
            try
            {
                #region SQL || HANA
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET ""Code"" = '{1}', ""Name"" = '{2}', U_AlertaID = '{3}', U_Descripcion_Es = '{4}', U_Descripcion_En = '{5}', U_Descripcion_Pt = '{6}', U_Descripcion_Fr = '{7}', U_UsuarioID = '{8}', U_EmpresaID = '{9}', U_SucursalID = '{10}', U_EstaActivo = '{11}', U_IAlertaID = '{12}', U_TipoAlerta = '{13}', U_TypeOfType = '{14}', U_NumEnviados = '{15}', U_NumEnviadosTiempo = '{16}', U_OrdenServicioID = '{17}' WHERE Code = '{1}'", this.NombreTabla, this.Alerta.AlertaID, this.Alerta.AlertaID, this.Alerta.AlertaID, this.Alerta.Descripcion_Es, this.Alerta.Descripcion_En, this.Alerta.Descripcion_Pt, this.Alerta.Descripcion_Fr, this.Alerta.CurrentUser.UsuarioID, this.Alerta.SubPropietario.Base.EmpresaID, this.Alerta.SubPropietario.SucursalID, this.Alerta.Activa, this.Alerta.IAlertaID, this.Alerta.TipoAlerta.Value, this.Alerta.TypeOfType, this.Alerta.NumEnviados, "0", "0");
                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || HANA

                #region SQL
                /*int errornum;
                string errormsj;
                result = this.oUDT.Update();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }*/
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar()
        {
            //int result = -1;
            try
            {
                #region SQL || HANA
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code"" = '{1}'", this.NombreTabla, this.oUDT.Code);
                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || HANA

                #region SQL
                /*int errornum;
                string errormsj;
                result = this.oUDT.Remove();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }*/
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Desactivar()
        {
            try
            {
                #region SQL || HANA
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET U_EstaActivo = '0' WHERE ""Code"" = '{1}'", this.NombreTabla, this.oUDT.Code);
                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || HANA

                #region SQL
                /*this.oUDT.UserFields.Fields.Item("U_EstaActivo").Value = 0;
                this.Actualizar();*/
                #endregion SQL
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void DesactivarByAlertaID(int? AlertaID)
        {
            try
            {
                #region SQL || HANA
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET U_EstaActivo = '0' WHERE ""Code"" = '{1}'", this.NombreTabla, AlertaID);
                this.Insertar.DoQuery(this.Parametro);
                //this.Parametro = String.Empty;
                //this.Parametro = String.Format("UPDATE {0} SET U_EstaActivo = '0' WHERE Code = '{1}'", this.AlertaMtto.NombreTablaAlertaMantenimiento, AlertaID);
                //this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || HANA
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ActualizarNumEnviado(int numEnviado)
        {
            try
            {
                #region SQL || HANA
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET U_NumEnviados = '{1}' WHERE ""Code"" = '{2}'", this.NombreTabla, numEnviado, oUDT.Code);
                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || HANA

                #region SQL
                /*this.oUDT.UserFields.Fields.Item("U_NumEnviados").Value = numEnviado;
                this.Actualizar();*/
                #endregion SQL
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void ActualizarNumEnviadoTiempo(int numEnviado)
        {
            try
            {
                #region SQL || HANA
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET U_NumEnviadosTiempo = '{1}' WHERE ""Code"" = '{2}'", this.NombreTabla, numEnviado, oUDT.Code);
                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || HANA

                #region SQL
                /*this.oUDT.UserFields.Fields.Item("U_NumEnviadosTiempo").Value = numEnviado;
                this.Actualizar();*/
                #endregion SQL
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void ActualizaSincronizacion(int tipo)
        {
            try
            {
                #region SQL || HANA
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET U_Sincronizado = '{1}' WHERE ""Code"" = '{2}'", this.NombreTabla, tipo, this.oUDT.Code);
                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || HANA

                #region SQL
                /*this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = tipo;
                this.Actualizar();*/
                #endregion SQL
            }
            catch(Exception ex)
            { throw new Exception(ex.Message); }
        }

        public string ConsultarTop10Alertas(int? EmpresaID, int? UsuarioID, Boolean? EstaActivo)
        {
            try
            {
                #region Construyendo el query mantenimiento
                StringBuilder sCmd = new StringBuilder();
                StringBuilder s_VarWHERE = new StringBuilder();

                //sCmd.Append("SELECT top 10 * FROM (Select a.U_AlertaID as DetalleID, a.U_AlertaID, a.U_Descripcion_En, a.U_Descripcion_Es, a.U_Descripcion_Pt, a.U_Descripcion_Fr, a.U_UsuarioID, a.U_EmpresaID, a.U_SucursalID, a.U_EstaActivo, a.U_IAlertaID, a.U_TipoAlerta ");
                //sCmd.Append(" FROM [@VSKF_ALERTA] as a JOIN [@VSKF_ALERTAMTTO] am ON am.U_AlertaMttoID = a.U_IAlertaID JOIN [@VSKF_SERVICIOS] s ON s.U_ServicioID = am.U_ServicioID JOIN [@VSKF_VEHICULO] v ON v.U_VehiculoID = am.U_MantenibleID JOIN [@VSKF_TIPOSERVICIO] t ON ");
                //sCmd.Append(" t.U_TipoServicioID = s.U_TipoServicioID LEFT OUTER JOIN [@VSKF_SUCURSAL] suc ON suc.U_SucursalID = v.U_SucursalID LEFT OUTER JOIN [@VSKF_DESPLAZAMIENTO] d ON d.U_VehiculoID = v.U_VehiculoID  ");

                sCmd.Append("SELECT top 100 * FROM (Select a.U_AlertaID as DetalleID, a.U_AlertaID, a.U_Descripcion_En, a.U_Descripcion_Es, a.U_Descripcion_Pt, a.U_Descripcion_Fr, a.U_UsuarioID, a.U_EmpresaID, a.U_SucursalID, a.U_EstaActivo, a.U_IAlertaID, a.U_TipoAlerta ");
                sCmd.Append(@" FROM ""@VSKF_ALERTA"" as a  ");

               // s_VarWHERE.Append("AND a.U_TipoAlerta = 1 ");
                if (EmpresaID != null)
                {
                    s_VarWHERE.Append(string.Format("AND a.U_EmpresaID = {0} ", EmpresaID));
                }

                if (UsuarioID != null)
                {
                    s_VarWHERE.Append(string.Format("AND a.U_UsuarioID = {0} ", UsuarioID));
                }

                if (EstaActivo != null)
                {
                    s_VarWHERE.Append(string.Format("AND a.U_EstaActivo = {0} ", Convert.ToInt32(EstaActivo)));
                }

                string filtros = s_VarWHERE.ToString().Trim();
                if (filtros.Length > 0)
                {
                    if (filtros.StartsWith("AND "))
                        filtros = filtros.Substring(4);
                    else if (filtros.StartsWith("OR "))
                        filtros = filtros.Substring(3);
                    else if (filtros.StartsWith(","))
                        filtros = filtros.Substring(1);
                    sCmd.Append(" WHERE " + filtros);
                }
                
                #endregion

                sCmd.Append(") AS PartialAlert order by U_AlertaID Desc ");
                return sCmd.ToString();
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

    }
}
