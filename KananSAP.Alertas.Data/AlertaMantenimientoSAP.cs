﻿using Kanan.Mantenimiento.BO2;
using Kanan.Vehiculos.BO2;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KananSAP.Alertas.Data
{
    public class AlertaMantenimientoSAP
    {
        #region Atributos
        private const string TABLE_ALERTAMTTO = "VSKF_ALERTAMTTO";
        public AlertaMantenimiento AlertaMantenimiento { get; set; }
        public UserTable oUDT { get; set; }
        private Company oCompany;
        public string ItemCode { get; set; }

        //Migracion Hana
        Recordset Insertar;
        String Parametro;
        private String NombreTabla;
        bool EsSQL;

        public String NombreTablaAlertaMantenimiento;

        #endregion

        #region Contructor
        public AlertaMantenimientoSAP(Company company)
        {
            this.oCompany = company;
            try
            {
                this.oUDT = this.oCompany.UserTables.Item(TABLE_ALERTAMTTO);

                //Migración HANA
                this.Insertar = (Recordset)this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.EsSQL = oCompany.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.NombreTabla = EsSQL ? "[@VSKF_ALERTAMTTO]" : @"""@VSKF_ALERTAMTTO""";

                this.NombreTablaAlertaMantenimiento = !String.IsNullOrEmpty(this.NombreTabla) ? this.NombreTablaAlertaMantenimiento = this.NombreTabla : this.NombreTablaAlertaMantenimiento = String.Empty;
            }
            catch
            {
                this.oUDT = null;
            }
        }
        #endregion

        public bool Existe()
        {
            return this.oUDT.GetByKey(this.ItemCode);
        }

        private void PrepareAlertaMantenimiento()
        {
            AlertaMantenimiento = new AlertaMantenimiento();
            AlertaMantenimiento.empresa = new Kanan.Operaciones.BO2.Empresa();
            AlertaMantenimiento.servicio = new Servicio();
        }

        public void AlertaMantenimientoToUDT()
        {
            try
            {
                this.oUDT.Code = AlertaMantenimiento.AlertaMantenimientoID.ToString();
                this.oUDT.Name = AlertaMantenimiento.AlertaMantenimientoID.ToString();
                this.oUDT.UserFields.Fields.Item("U_AlertaMttoID").Value = AlertaMantenimiento.AlertaMantenimientoID;
                this.oUDT.UserFields.Fields.Item("U_ServicioID").Value = AlertaMantenimiento.servicio.ServicioID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value = AlertaMantenimiento.empresa.EmpresaID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_TipoAlerta").Value = AlertaMantenimiento.TipoAlerta ?? 0;
                this.oUDT.UserFields.Fields.Item("U_TypeOfType").Value = AlertaMantenimiento.TypeOfType ?? 0;
                this.oUDT.UserFields.Fields.Item("U_MantenibleID").Value = AlertaMantenimiento.Mantenible.MantenibleID ?? 0;
                if (AlertaMantenimiento.Mantenible.GetType() == typeof(Vehiculo))
                    this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value = 1;
                else if (AlertaMantenimiento.Mantenible.GetType() == typeof(Caja))
                    this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value = 2;
                else if (AlertaMantenimiento.Mantenible.GetType() == typeof(TipoVehiculo))
                    this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value = 3;
                //this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = AlertaMantenimiento.AlertaMantenimientoID;
                //this.oUDT.UserFields.Fields.Item("U_UUID").Value = AlertaMantenimiento.AlertaMantenimientoID;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void UDTToAlertaMantenimiento()
        {
            try
            {
                PrepareAlertaMantenimiento();
                AlertaMantenimiento.AlertaMantenimientoID = this.oUDT.UserFields.Fields.Item("U_AlertaMttoID").Value ?? null;
                AlertaMantenimiento.servicio.ServicioID  = this.oUDT.UserFields.Fields.Item("U_ServicioID").Value ?? null;
                AlertaMantenimiento.empresa.EmpresaID  = this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value ?? null;
                AlertaMantenimiento.TipoAlerta = this.oUDT.UserFields.Fields.Item("U_TipoAlerta").Value ?? null;
                AlertaMantenimiento.TypeOfType = this.oUDT.UserFields.Fields.Item("U_TypeOfType").Value ?? null;
                AlertaMantenimiento.Mantenible.MantenibleID = this.oUDT.UserFields.Fields.Item("U_MantenibleID").Value ?? null;
                if (this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value == 1)
                    AlertaMantenimiento.Mantenible = new Vehiculo();
                else if (this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value == 2)
                    AlertaMantenimiento.Mantenible = new Caja();
                else if (this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value == 3)
                    AlertaMantenimiento.Mantenible = new TipoVehiculo();
                AlertaMantenimiento.TypeObject = this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value ?? null;
                //this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = AlertaMantenimiento.AlertaMantenimientoID;
                //this.oUDT.UserFields.Fields.Item("U_UUID").Value = AlertaMantenimiento.AlertaMantenimientoID;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void Insert()
        {
            //int result = -1;
            try
            {

                String TipoMantenible = this.AlertaMantenimiento.Mantenible.GetType() == typeof(Vehiculo) ? TipoMantenible = "1" : this.AlertaMantenimiento.Mantenible.GetType() == typeof(Caja) ? TipoMantenible = "2" : this.AlertaMantenimiento.Mantenible.GetType() == typeof(TipoVehiculo) ? TipoMantenible = "3" : TipoMantenible = String.Empty;
                if (String.IsNullOrEmpty(TipoMantenible))
                    throw new Exception("Error al intentar obtener el tipo de vehículo. ");

                #region SQL || HANA
                this.Parametro = String.Empty;
                this.Parametro = String.Format("INSERT INTO {0} VALUES('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}')", this.NombreTabla, this.AlertaMantenimiento.AlertaMantenimientoID, this.AlertaMantenimiento.AlertaMantenimientoID, this.AlertaMantenimiento.AlertaMantenimientoID, this.AlertaMantenimiento.servicio.ServicioID, this.AlertaMantenimiento.empresa.EmpresaID, this.AlertaMantenimiento.TipoAlerta, this.AlertaMantenimiento.TypeOfType, this.AlertaMantenimiento.Mantenible.MantenibleID, TipoMantenible, "0", null);
                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || HANA

                #region SQL
                /*result = this.oUDT.Add();
                if (result != 0)
                {
                    int errornum;
                    string errormsj;
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }*/
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Actualizar()
        {
            //int result = -1;
            try
            {

                String TipoMantenible = this.AlertaMantenimiento.Mantenible.GetType() == typeof(Vehiculo) ? TipoMantenible = "1" : this.AlertaMantenimiento.Mantenible.GetType() == typeof(Caja) ? TipoMantenible = "2" : this.AlertaMantenimiento.Mantenible.GetType() == typeof(TipoVehiculo) ? TipoMantenible = "3" : TipoMantenible = String.Empty;
                if (String.IsNullOrEmpty(TipoMantenible))
                    throw new Exception("Error al intentar obtener el tipo de vehículo. ");

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE FROM {0} SET ""Code"" = '{1}', ""Name"" = '{2}', U_AlertaMttoID = '{3}', U_ServicioID = '{4}', U_EmpresaID = '{5}', U_TipoAlerta = '{6}', U_TypeOfType = '{7}', U_MantenibleID = '{8}', U_TipoMantenibleID = '{9}' WHERE Code = '{1}'", this.NombreTabla, this.AlertaMantenimiento.AlertaMantenimientoID, this.AlertaMantenimiento.AlertaMantenimientoID, this.AlertaMantenimiento.AlertaMantenimientoID, this.AlertaMantenimiento.servicio.ServicioID, this.AlertaMantenimiento.empresa.EmpresaID, this.AlertaMantenimiento.TipoAlerta, this.AlertaMantenimiento.TypeOfType, this.AlertaMantenimiento.Mantenible.MantenibleID, TipoMantenible);
                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana

                #region SQL
                /*int errornum;
                string errormsj;
                result = this.oUDT.Update();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }*/
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar()
        {
            //int result = -1;
            try
            {
                int ID = !String.IsNullOrEmpty(oUDT.Code) ? ID = Convert.ToInt32(oUDT.Code) :  ID = 0;
                if (ID <=0)
                    throw new Exception("Error al intentar obtener indentificador para la eliminación. ");

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code"" = '{1}'", this.NombreTabla, ID);
                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana

                #region SQL
                /*int errornum;
                string errormsj;
                result = this.oUDT.Remove();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }*/
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Recordset Consultar()
        {
            try
            {
                if (this.AlertaMantenimiento == null)
                    this.PrepareAlertaMantenimiento();
                string query = string.Format("select * from {0} ", this.NombreTabla);
                string where = string.Empty;
                if (AlertaMantenimiento.AlertaMantenimientoID != null)
                    where += string.Format("and U_AlertaMttoID = {0} ", AlertaMantenimiento.AlertaMantenimientoID);
                if (AlertaMantenimiento.servicio.ServicioID != null)
                    where += string.Format("and U_ServicioID = {0} ", AlertaMantenimiento.servicio.ServicioID);
                if (AlertaMantenimiento.empresa.EmpresaID != null)
                    where += string.Format("and U_EmpresaID = {0} ", AlertaMantenimiento.empresa.EmpresaID);
                if (AlertaMantenimiento.TipoAlerta != null)
                    where += string.Format("and U_TipoAlerta = {0} ", AlertaMantenimiento.TipoAlerta);
                if (AlertaMantenimiento.TypeOfType != null)
                    where += string.Format("and U_TypeOfType = {0} ", AlertaMantenimiento.TypeOfType);
                if (AlertaMantenimiento.Mantenible != null != null)
                {
                    if (AlertaMantenimiento.Mantenible.MantenibleID != null)
                        where += string.Format("and U_MantenibleID = {0} ", AlertaMantenimiento.Mantenible.MantenibleID);
                    if(AlertaMantenimiento.Mantenible.GetType() == typeof(Vehiculo))
                        where += string.Format("and U_TipoMantenibleID = {0} ", 1);
                    else if (AlertaMantenimiento.Mantenible.GetType() == typeof(Caja))
                        where += string.Format("and U_TipoMantenibleID = {0} ", 2);
                    else if (AlertaMantenimiento.Mantenible.GetType() == typeof(TipoVehiculo))
                        where += string.Format("and U_TipoMantenibleID = {0} ", 3);
                }
                if (where.Length > 0)
                {
                    if (where.StartsWith("and"))
                        where = " where " + where.Substring(3);
                }
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(string.Format("{0} {1} ", query, where));
                return rs;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public AlertaMantenimiento RecordSetToAlertaMantenimiento(Recordset rs)
        {
            try
            {
                AlertaMantenimiento alerta = new AlertaMantenimiento();
                alerta.empresa = new Kanan.Operaciones.BO2.Empresa();
                alerta.servicio = new Servicio();
                rs.MoveFirst();
                if (rs.Fields.Item("U_AlertaMttoID").Value != null && rs.Fields.Item("U_AlertaMttoID").Value > 0)
                    alerta.AlertaMantenimientoID = rs.Fields.Item("U_AlertaMttoID").Value;
                if (rs.Fields.Item("U_ServicioID").Value != null && rs.Fields.Item("U_ServicioID").Value > 0)
                    alerta.servicio.ServicioID = rs.Fields.Item("U_ServicioID").Value;
                if (rs.Fields.Item("U_EmpresaID").Value != null && rs.Fields.Item("U_EmpresaID").Value > 0)
                    alerta.empresa.EmpresaID = rs.Fields.Item("U_EmpresaID").Value;
                if (rs.Fields.Item("U_TipoAlerta").Value != null && rs.Fields.Item("U_TipoAlerta").Value > 0)
                    alerta.TipoAlerta = rs.Fields.Item("U_TipoAlerta").Value;
                if (rs.Fields.Item("U_TipoMantenibleID").Value != null && rs.Fields.Item("U_TipoMantenibleID").Value > 0)
                {
                    int tipo = rs.Fields.Item("U_TipoMantenibleID").Value;
                    if (tipo == 1)
                        alerta.Mantenible = new Vehiculo();
                    else if (tipo == 2)
                        alerta.Mantenible = new Caja();
                    else if (tipo == 3)
                        alerta.Mantenible = new TipoVehiculo();
                    alerta.TypeOfType = tipo;
                }
                if (rs.Fields.Item("U_MantenibleID").Value != null && rs.Fields.Item("U_MantenibleID").Value > 0)
                    alerta.Mantenible.MantenibleID = rs.Fields.Item("U_MantenibleID").Value;
                
                return alerta;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public List<AlertaMantenimiento> RecordSetToListAlertaMantenimiento(Recordset rs)
        {
            var result = new List<AlertaMantenimiento>();
            rs.MoveFirst();
            for (int i = 0; i < rs.RecordCount; i++)
            {
                string itemcode = rs.Fields.Item("Code").Value;
                this.oUDT.GetByKey(itemcode);
                var temp = (AlertaMantenimiento)this.AlertaMantenimiento.Clone();
                temp = RecordSetToAlertaMantenimiento(rs);
                result.Add(temp);
                rs.MoveNext();
            }
            return result;
        }

        public bool HashoAlertaMto(Recordset rs)
        {
            if (rs.RecordCount > 0)
                return true;
            return false;
        }
    }
}
