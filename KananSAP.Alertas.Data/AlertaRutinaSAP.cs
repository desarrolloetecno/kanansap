﻿using Kanan.Mantenimiento.BO2;
using Kanan.Vehiculos.BO2;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KananSAP.Alertas.Data
{
    public class AlertaRutinaSAP
    {
        #region Atributos
        private const string TABLE_ALERTARUTINA = "VSKF_ALERTARUTINA";
        public AlertaRutina AlertaRutina { get; set; }
        public UserTable oUDT { get; set; }
        private Company oCompany;
        public string ItemCode { get; set; }

        //Migracion Hana
        Recordset Insertar;
        String Parametro;
        private String NombreTabla;
        bool EsSQL;

        public String NombreTablaAlertaRutina;

        #endregion

        #region Contructor
        public AlertaRutinaSAP(Company company)
        {
            this.oCompany = company;
            try
            {
                this.oUDT = this.oCompany.UserTables.Item(TABLE_ALERTARUTINA);

                //Migración HANA
                this.Insertar = (Recordset)this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.EsSQL = oCompany.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.NombreTabla = EsSQL ? "[@VSKF_ALERTARUTINA]" : @"""@VSKF_ALERTARUTINA""";

                this.NombreTablaAlertaRutina = !String.IsNullOrEmpty(this.NombreTabla) ? this.NombreTablaAlertaRutina = this.NombreTabla : this.NombreTablaAlertaRutina = String.Empty;
            }
            catch
            {
                this.oUDT = null;
            }
        }
        #endregion

        public bool Existe()
        {
            return this.oUDT.GetByKey(this.ItemCode);
        }

        private void PrepareAlertaRutina()
        {
            AlertaRutina = new AlertaRutina();
            AlertaRutina.empresa = new Kanan.Operaciones.BO2.Empresa();
          
        }

        public void AlertaRutinaToUDT()
        {
            try
            {
                this.oUDT.Code = AlertaRutina.AlertaRutinaID.ToString();
                this.oUDT.Name = AlertaRutina.AlertaRutinaID.ToString();
                this.oUDT.UserFields.Fields.Item("U_ALERTARUTINAID").Value = AlertaRutina.AlertaRutinaID;
                this.oUDT.UserFields.Fields.Item("U_SERVICIOID").Value = AlertaRutina.servicio.ServicioID ?? 0;
 
                this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value = AlertaRutina.empresa.EmpresaID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_TipoAlerta").Value = AlertaRutina.TipoAlerta ?? 0;
                this.oUDT.UserFields.Fields.Item("U_TypeOfType").Value = AlertaRutina.TypeOfType ?? 0;
                this.oUDT.UserFields.Fields.Item("U_MantenibleID").Value = AlertaRutina.Mantenible.MantenibleID ?? 0;
                if (AlertaRutina.Mantenible.GetType() == typeof(Vehiculo))
                    this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value = 1;
                else if (AlertaRutina.Mantenible.GetType() == typeof(Caja))
                    this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value = 2;
                else if (AlertaRutina.Mantenible.GetType() == typeof(TipoVehiculo))
                    this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value = 3;
                //this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = AlertaRutina.AlertaRutinaID;
                //this.oUDT.UserFields.Fields.Item("U_UUID").Value = AlertaRutina.AlertaRutinaID;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void UDTToAlertaRutina()
        {
            try
            {
                PrepareAlertaRutina();
                AlertaRutina.AlertaRutinaID = this.oUDT.UserFields.Fields.Item("U_AlertaMttoID").Value ?? null;
                AlertaRutina.servicio.ServicioID  = this.oUDT.UserFields.Fields.Item("U_ServicioID").Value ?? null;
                AlertaRutina.empresa.EmpresaID  = this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value ?? null;
                AlertaRutina.TipoAlerta = this.oUDT.UserFields.Fields.Item("U_TipoAlerta").Value ?? null;
                AlertaRutina.TypeOfType = this.oUDT.UserFields.Fields.Item("U_TypeOfType").Value ?? null;
                AlertaRutina.Mantenible.MantenibleID = this.oUDT.UserFields.Fields.Item("U_MantenibleID").Value ?? null;
                if (this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value == 1)
                    AlertaRutina.Mantenible = new Vehiculo();
                else if (this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value == 2)
                    AlertaRutina.Mantenible = new Caja();
                else if (this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value == 3)
                    AlertaRutina.Mantenible = new TipoVehiculo();
                AlertaRutina.TypeObject = this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value ?? null;
                //this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = AlertaRutina.AlertaRutinaID;
                //this.oUDT.UserFields.Fields.Item("U_UUID").Value = AlertaRutina.AlertaRutinaID;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void Insert()
        {
            //int result = -1;
            try
            {

                String TipoMantenible = this.AlertaRutina.Mantenible.GetType() == typeof(Vehiculo) ? TipoMantenible = "1" : this.AlertaRutina.Mantenible.GetType() == typeof(Caja) ? TipoMantenible = "2" : this.AlertaRutina.Mantenible.GetType() == typeof(TipoVehiculo) ? TipoMantenible = "3" : TipoMantenible = String.Empty;
                if (String.IsNullOrEmpty(TipoMantenible))
                    throw new Exception("Error al intentar obtener el tipo de vehículo. ");

                #region SQL || HANA
                this.Parametro = String.Empty;
                this.Parametro = String.Format("INSERT INTO {0} VALUES('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}')", this.NombreTabla, this.AlertaRutina.AlertaRutinaID, this.AlertaRutina.AlertaRutinaID, this.AlertaRutina.AlertaRutinaID, this.AlertaRutina.servicio.ServicioID, this.AlertaRutina.empresa.EmpresaID, this.AlertaRutina.TipoAlerta, this.AlertaRutina.TypeOfType, this.AlertaRutina.Mantenible.MantenibleID, TipoMantenible, "0", null);
                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || HANA

                #region SQL
                /*result = this.oUDT.Add();
                if (result != 0)
                {
                    int errornum;
                    string errormsj;
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }*/
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Actualizar()
        {
            //int result = -1;
            try
            {

                String TipoMantenible = this.AlertaRutina.Mantenible.GetType() == typeof(Vehiculo) ? TipoMantenible = "1" : this.AlertaRutina.Mantenible.GetType() == typeof(Caja) ? TipoMantenible = "2" : this.AlertaRutina.Mantenible.GetType() == typeof(TipoVehiculo) ? TipoMantenible = "3" : TipoMantenible = String.Empty;
                if (String.IsNullOrEmpty(TipoMantenible))
                    throw new Exception("Error al intentar obtener el tipo de vehículo. ");

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE FROM {0} SET ""Code"" = '{1}', ""Name"" = '{2}', U_AlertaMttoID = '{3}', U_ServicioID = '{4}', U_EmpresaID = '{5}', U_TipoAlerta = '{6}', U_TypeOfType = '{7}', U_MantenibleID = '{8}', U_TipoMantenibleID = '{9}' WHERE Code = '{1}'", this.NombreTabla, this.AlertaRutina.AlertaRutinaID, this.AlertaRutina.AlertaRutinaID, this.AlertaRutina.AlertaRutinaID, this.AlertaRutina.servicio.ServicioID, this.AlertaRutina.empresa.EmpresaID, this.AlertaRutina.TipoAlerta, this.AlertaRutina.TypeOfType, this.AlertaRutina.Mantenible.MantenibleID, TipoMantenible);
                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana

                #region SQL
                /*int errornum;
                string errormsj;
                result = this.oUDT.Update();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }*/
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar()
        {
            //int result = -1;
            try
            {
                int ID = !String.IsNullOrEmpty(oUDT.Code) ? ID = Convert.ToInt32(oUDT.Code) :  ID = 0;
                if (ID <=0)
                    throw new Exception("Error al intentar obtener indentificador para la eliminación. ");

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code"" = '{1}'", this.NombreTabla, ID);
                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana

                #region SQL
                /*int errornum;
                string errormsj;
                result = this.oUDT.Remove();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }*/
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Recordset Consultar()
        {
            try
            {
                if (this.AlertaRutina == null)
                    this.PrepareAlertaRutina();
                string query = string.Format("select * from {0} ", this.NombreTabla);
                string where = string.Empty;
                if (AlertaRutina.AlertaRutinaID != null)
                    where += string.Format("and U_AlertaMttoID = {0} ", AlertaRutina.AlertaRutinaID);
                if (AlertaRutina.servicio.ServicioID != null)
                    where += string.Format("and U_ServicioID = {0} ", AlertaRutina.servicio.ServicioID);
                if (AlertaRutina.empresa.EmpresaID != null)
                    where += string.Format("and U_EmpresaID = {0} ", AlertaRutina.empresa.EmpresaID);
                if (AlertaRutina.TipoAlerta != null)
                    where += string.Format("and U_TipoAlerta = {0} ", AlertaRutina.TipoAlerta);
                if (AlertaRutina.TypeOfType != null)
                    where += string.Format("and U_TypeOfType = {0} ", AlertaRutina.TypeOfType);
                if (AlertaRutina.Mantenible != null != null)
                {
                    if (AlertaRutina.Mantenible.MantenibleID != null)
                        where += string.Format("and U_MantenibleID = {0} ", AlertaRutina.Mantenible.MantenibleID);
                    if(AlertaRutina.Mantenible.GetType() == typeof(Vehiculo))
                        where += string.Format("and U_TipoMantenibleID = {0} ", 1);
                    else if (AlertaRutina.Mantenible.GetType() == typeof(Caja))
                        where += string.Format("and U_TipoMantenibleID = {0} ", 2);
                    else if (AlertaRutina.Mantenible.GetType() == typeof(TipoVehiculo))
                        where += string.Format("and U_TipoMantenibleID = {0} ", 3);
                }
                if (where.Length > 0)
                {
                    if (where.StartsWith("and"))
                        where = " where " + where.Substring(3);
                }
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(string.Format("{0} {1} ", query, where));
                return rs;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public AlertaRutina RecordSetToAlertaRutina(Recordset rs)
        {
            try
            {
                AlertaRutina alerta = new AlertaRutina();
                alerta.empresa = new Kanan.Operaciones.BO2.Empresa();
                alerta.servicio = new Servicio();
                rs.MoveFirst();
                if (rs.Fields.Item("U_AlertaMttoID").Value != null && rs.Fields.Item("U_AlertaMttoID").Value > 0)
                    alerta.AlertaRutinaID = rs.Fields.Item("U_AlertaMttoID").Value;
                if (rs.Fields.Item("U_ServicioID").Value != null && rs.Fields.Item("U_ServicioID").Value > 0)
                    alerta.servicio.ServicioID = rs.Fields.Item("U_ServicioID").Value;
                if (rs.Fields.Item("U_EmpresaID").Value != null && rs.Fields.Item("U_EmpresaID").Value > 0)
                    alerta.empresa.EmpresaID = rs.Fields.Item("U_EmpresaID").Value;
                if (rs.Fields.Item("U_TipoAlerta").Value != null && rs.Fields.Item("U_TipoAlerta").Value > 0)
                    alerta.TipoAlerta = rs.Fields.Item("U_TipoAlerta").Value;
                if (rs.Fields.Item("U_TipoMantenibleID").Value != null && rs.Fields.Item("U_TipoMantenibleID").Value > 0)
                {
                    int tipo = rs.Fields.Item("U_TipoMantenibleID").Value;
                    if (tipo == 1)
                        alerta.Mantenible = new Vehiculo();
                    else if (tipo == 2)
                        alerta.Mantenible = new Caja();
                    else if (tipo == 3)
                        alerta.Mantenible = new TipoVehiculo();
                    alerta.TypeOfType = tipo;
                }
                if (rs.Fields.Item("U_MantenibleID").Value != null && rs.Fields.Item("U_MantenibleID").Value > 0)
                    alerta.Mantenible.MantenibleID = rs.Fields.Item("U_MantenibleID").Value;
                
                return alerta;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public List<AlertaRutina> RecordSetToListAlertaRutina(Recordset rs)
        {
            var result = new List<AlertaRutina>();
            rs.MoveFirst();
            for (int i = 0; i < rs.RecordCount; i++)
            {
                string itemcode = rs.Fields.Item("Code").Value;
                this.oUDT.GetByKey(itemcode);
                var temp = (AlertaRutina)this.AlertaRutina.Clone();
                temp = RecordSetToAlertaRutina(rs);
                result.Add(temp);
                rs.MoveNext();
            }
            return result;
        }

        public bool HashoAlertaMto(Recordset rs)
        {
            if (rs.RecordCount > 0)
                return true;
            return false;
        }
    }
}
