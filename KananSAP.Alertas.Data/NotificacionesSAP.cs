﻿using Kanan.Comun.BO2;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KananSAP.Alertas.Data
{
    public class NotificacionesSAP
    {
        #region Atributos
        public Notificacion Notificacion { get; set; }
        public UserTable oUDT { get; set; }
        public string ItemCode { get; set; }
        private Company Company;
        public int? Sincronizado { get; set; }
        public Guid? UUID { get; set; }

        Recordset Insertar;
        String Parametro;
        private String NombreTabla;
        bool EsSQL;

        public String NombreTablaNotificacionesSAP;

        public bool ExistUDT()
        {
            return oUDT.GetByKey(this.ItemCode);
        }
        #endregion


        #region Constructor
        public NotificacionesSAP(SAPbobsCOM.Company company)
        {
            this.Company = company;
            this.Notificacion = new Notificacion();
            try
            {
                this.oUDT = company.UserTables.Item("VSKF_NOTIFICACION");

                //Migración HANA
                this.Insertar = (Recordset)this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.EsSQL = Company.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.NombreTabla = EsSQL ? "[@VSKF_NOTIFICACION]" : @"""@VSKF_NOTIFICACION""";

                this.NombreTablaNotificacionesSAP = !String.IsNullOrEmpty(this.NombreTabla) ? this.NombreTablaNotificacionesSAP = this.NombreTabla : this.NombreTablaNotificacionesSAP = String.Empty;

            }
            catch (Exception)
            {
                this.oUDT = null;
            }
        }
        #endregion

        #region Methods
        #region Metodos interface SAP - Kananfleet

        private Notificable GetTypeNotificadorData(int? tipoNotificadorID)
        {
            try
            {
                switch (tipoNotificadorID)
                {
                    case 1:
                        return new Vehiculo() { VehiculoID = (int)this.oUDT.UserFields.Fields.Item("U_VehiculoID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_VehiculoID").Value, };
                    case 2:
                        return new Empresa() { EmpresaID = (int)this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value, };
                    case 3:
                        return new Sucursal() { SucursalID = (int)this.oUDT.UserFields.Fields.Item("U_SucursalID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_SucursalID").Value, };
                    default:
                        throw new Exception("El tipoNotificadorID proporcionado no existe. Verifique su informacón.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private int GetTipoNotificadorID(Type type)
        {
            try
            {
                int tipo = 0;
                if (type == typeof(Vehiculo))
                    tipo = 1;
                else if (type == typeof(Empresa))
                    tipo = 2;
                else if (type == typeof(Sucursal))
                    tipo = 3;
                if (tipo == 0)
                    throw new Exception("El Notificador proporcionado no existe. Verifique su información.");
                return tipo;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool SAPToNotificacion()
        {
            if (!this.oUDT.GetByKey(ItemCode)) return false;
            UDTToNotificacion();
            return true;

        }

        public void UDTToNotificacion()
        {
            this.Notificacion = new Notificacion();
            this.Notificacion.NotificacionID = (int)this.oUDT.UserFields.Fields.Item("U_NotificacionID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_NotificacionID").Value;
            int tipo = (int)this.oUDT.UserFields.Fields.Item("U_TipoNotificableID").Value == 0 ? 0 : (int)this.oUDT.UserFields.Fields.Item("U_TipoNotificableID").Value;
            this.Notificacion.Notificador = this.GetTypeNotificadorData(tipo);
            this.Notificacion.Correos = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Correos").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Correos").Value;
            this.Notificacion.Celulares = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Celulares").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Celulares").Value;
            this.Sincronizado = this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value == null ? null : (int?)this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value;
            this.UUID = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_UUID").Value.ToString()) ? null : Guid.Parse(this.oUDT.UserFields.Fields.Item("U_UUID").Value);
        }

        public void NotificacionToUDT()
        {
            //string tks = (DateTime.Now.ToString("yyyMMdd") + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second).ToString();
            oUDT.Code = "1"; // this.Notificacion.NotificacionID != null ? this.Notificacion.NotificacionID.ToString() : tks;
            oUDT.Name = "1";// this.Notificacion.NotificacionID != null ? this.Notificacion.NotificacionID.ToString() : tks;
            this.oUDT.UserFields.Fields.Item("U_NotificacionID").Value = this.Notificacion.NotificacionID ?? 0;
            int tipo = this.GetTipoNotificadorID(this.Notificacion.Notificador.GetType());
            this.oUDT.UserFields.Fields.Item("U_TipoNotificableID").Value = tipo;
            if (tipo == 1)
                this.oUDT.UserFields.Fields.Item("U_VehiculoID").Value = this.Notificacion.Notificador.NotificableID;
            else if (tipo == 2)
                this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value = this.Notificacion.Notificador.NotificableID;
            else if (tipo == 3)
                this.oUDT.UserFields.Fields.Item("U_SucursalID").Value = this.Notificacion.Notificador.NotificableID;
            this.oUDT.UserFields.Fields.Item("U_Correos").Value = this.Notificacion.Correos ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_Celulares").Value = this.Notificacion.Celulares ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Sincronizado < 0 ? 0 : Sincronizado;
            this.oUDT.UserFields.Fields.Item("U_UUID").Value = this.UUID == null ? string.Empty : this.UUID.ToString();
        }

        #endregion

        #region Acciones
        public void InsertUDT()
        {
            
            String TipoNotificableID = this.GetTipoNotificadorID(this.Notificacion.Notificador.GetType()).ToString();
            
            String VehiculoID = TipoNotificableID == "1" ? VehiculoID = this.Notificacion.Notificador.NotificableID.ToString() : null;
            String EmpresaID = TipoNotificableID == "2" ? EmpresaID = this.Notificacion.Notificador.NotificableID.ToString() : null;
            String SucursalID = TipoNotificableID == "3" ? SucursalID = this.Notificacion.Notificador.NotificableID.ToString() : null;

            #region SQL || Hana
            //this.Parametro = String.Empty;
            //this.Parametro = String.Format("INSERT INTO {0} VALUES('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}')", this.NombreTabla, this.Notificacion.NotificacionID, this.Notificacion.NotificacionID, this.Notificacion.NotificacionID, TipoNotificableID, EmpresaID, SucursalID, VehiculoID, this.Notificacion.Correos, this.Notificacion.Celulares, "0", this.UUID);
            //this.Insertar.DoQuery(this.Parametro);
            #endregion SQL || Hana

            #region SQL
            int flag = oUDT.Add();
            if (flag != 0)
            {
                int errornum;
                string errormsj;
                Company.GetLastError(out errornum, out errormsj);
                throw new Exception("Error: " + errornum + " - " + errormsj);
            }
            #endregion SQL
        }

        public void UpdateUDT()
        {
            String TipoNotificableID = this.GetTipoNotificadorID(this.Notificacion.Notificador.GetType()).ToString();
            
            String VehiculoID = TipoNotificableID == "1" ? VehiculoID = this.Notificacion.Notificador.NotificableID.ToString() : null;
            String EmpresaID = TipoNotificableID == "2" ? EmpresaID = this.Notificacion.Notificador.NotificableID.ToString() : null;
            String SucursalID = TipoNotificableID == "3" ? SucursalID = this.Notificacion.Notificador.NotificableID.ToString() : null;
            
            if (String.IsNullOrEmpty(VehiculoID) && String.IsNullOrEmpty(EmpresaID) && String.IsNullOrEmpty(SucursalID))
                throw new Exception ("Error al indentificar el destinatario a notificar. ");

            #region SQL || Hana
            this.Parametro = String.Empty;
            this.Parametro = String.Format(@"UPDATE {0} SET ""Code"" = '{1}', ""Name"" = '{2}', U_NotificacionID = '{3}', U_TipoNotificableID = '{4}', U_EmpresaID = '{5}', U_SucursalID = '{6}', U_VehiculoID = '{7}', U_Correos = '{8}', U_Celulares = '{9}' WHERE Code = '{1}'", this.NombreTabla, this.Notificacion.NotificacionID, this.Notificacion.NotificacionID, this.Notificacion.NotificacionID, TipoNotificableID, EmpresaID, SucursalID, VehiculoID, this.Notificacion.Correos, this.Notificacion.Celulares);
            this.Insertar.DoQuery(this.Parametro);
            #endregion SQL || Hana

            #region SQL
            /*var flag = oUDT.Update();

            if (flag != 0)
            {
                int errornum;
                string errormsj;
                Company.GetLastError(out errornum, out errormsj);
                throw new Exception("Error: " + errornum + " - " + errormsj);
            }*/
            #endregion SQL
        }

        public void RemoveUDT()
        {

            String NotificacionID = this.oUDT.UserFields.Fields.Item("U_NotificacionID").Value == "0" ? NotificacionID =  null : NotificacionID = this.oUDT.UserFields.Fields.Item("U_NotificacionID").Value;

            #region SQL || Hana
            this.Parametro = String.Empty;
            this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code"" = '{1}'", this.NombreTabla, NotificacionID);
            this.Insertar.DoQuery(this.Parametro);
            #endregion SQL || Hana

            #region SQL
            /*var flag = oUDT.Remove();

            if (flag != 0)
            {
                int errornum;
                string errormsj;
                Company.GetLastError(out errornum, out errormsj);
                throw new Exception("Error: " + errornum + " - " + errormsj);
            }*/
            #endregion SQL
        }

        #endregion

        #region Metodos Auxiliares

        public Recordset Consultar(Notificacion nt)
        {
            try
            {
                Recordset rs = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                if (nt == null) nt = new Notificacion();

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(@" SELECT * from ""@VSKF_NOTIFICACION"" as NT ");

                if (nt.NotificacionID != null)
                    where.Append(string.Format(" and nt.U_NotificacionID = {0} ", nt.NotificacionID));
                if(nt.Notificador != null)
                {
                    int tipo = this.GetTipoNotificadorID(nt.Notificador.GetType());
                    where.Append(string.Format("and nt.U_TipoNotificableID = {0} ", tipo));
                    if(nt.Notificador.NotificableID != null && tipo == 1)
                        where.Append(string.Format("and nt.U_VehiculoID = {0} ", nt.Notificador.NotificableID));
                    else if (nt.Notificador.NotificableID != null && tipo == 2)
                        where.Append(string.Format("and nt.U_EmpresaID = {0} ", nt.Notificador.NotificableID));
                    else if (nt.Notificador.NotificableID != null && tipo == 3)
                        where.Append(string.Format("and nt.U_SucursalID = {0} ", nt.Notificador.NotificableID));
                }
                if (!string.IsNullOrEmpty(nt.Correos))
                    where.Append(string.Format(" and nt.U_Correos = '{0}' ", nt.Correos));
                if (!string.IsNullOrEmpty(nt.Celulares))
                    where.Append(string.Format(" and nt.U_Celulares = '{0}' ", nt.Celulares));
                if (Sincronizado != null)
                    where.Append(string.Format(" and nt.U_Sincronizado = {0} ", Sincronizado));
                if (UUID != null)
                    where.Append(string.Format(" and nt.U_UUID = '{0}' ", UUID));

                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith("and"))
                        filtro = filtro.Substring(3);
                    query.Append(" where " + filtro);
                }
                query.Append(" order by nt.U_NotificacionID desc ");
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Notificacion> RecordSetToListNotificacion(Recordset rs)
        {
            var result = new List<Notificacion>();
            rs.MoveFirst();
            for (int i = 0; i < rs.RecordCount; i++)
            {
                string itemcode = rs.Fields.Item("Code").Value;
                this.oUDT.GetByKey(itemcode);
                UDTToNotificacion();
                var temp = (Notificacion)this.Notificacion.Clone();
                result.Add(temp);
                rs.MoveNext();
            }
            return result;
        }

        public Recordset ActualizarCode(Notificacion not)
        {
            try
            {
                Recordset rs = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(" UPDATE " + this.NombreTabla + @" SET ""Code"" = '" + not.NotificacionID + "', U_NotificacionID = " + not.NotificacionID
                    + @", ""Name"" = '" + not.NotificacionID + "', U_Sincronizado = " + Sincronizado);

                if (UUID != null)
                    where.Append(string.Format(" and U_UUID like '%{0}%' ", UUID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #endregion


    }
}
