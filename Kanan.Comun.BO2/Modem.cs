﻿using System;
using System.Runtime.Serialization;


namespace Kanan.Comun.BO2
{
    [Serializable]
    [DataContract]
    public class Modem:ICloneable
    {
        [DataMember]
        public int? ModemID { get; set; }
        [DataMember]
        public string ESN { get; set; }
        [DataMember]
        public string Modelo { get; set; }
        [DataMember]
        public string IMEI { get; set; }
        [DataMember]
        public int? RemoraID { get; set; }
        [DataMember]
        public bool? EstaActivo { get; set; }
        [DataMember]
        public TipoProveedor? Source { get; set; }
        //public Servidor Servidor { get; set; }
        [DataMember]
        public CuentaExterna Cuenta { get; set;}
        [DataMember]
        public bool? EsNuevo { get; set; }

        public object Clone()
        {
            Modem m = new Modem();
            m.ModemID = this.ModemID;
            m.ESN = this.ESN;
            m.Modelo = this.Modelo;
            m.RemoraID = this.RemoraID;
            m.EstaActivo = this.EstaActivo;
            m.EsNuevo = this.EsNuevo;
            if (this.Source != null)
            {
                m.Source = this.Source;
            }
          
            if (this.Cuenta != null)
            {
                m.Cuenta = (CuentaExterna)this.Cuenta.Clone();
            }
            return m;
        }
    }
}
