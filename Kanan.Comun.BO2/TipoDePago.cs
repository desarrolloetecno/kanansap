﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kanan.Comun.BO2
{
    public class TipoDePago:ICloneable
    {
        public int? TipoPagoID { get; set; }
        public string TipoPago { get; set; }
        public bool Estatus { get; set; }
        public string DetallePago { get; set; }

        public object Clone()
        {
            TipoDePago tipo = new TipoDePago();
            tipo.TipoPagoID = this.TipoPagoID;
            tipo.TipoPago = this.TipoPago;
            tipo.Estatus = this.Estatus;
            tipo.DetallePago = this.DetallePago;
            return tipo;
        }
    }
}
