﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kanan.Comun.BO2
{
    public class ModemVehiculo
    {
        /// <summary>
        /// Remora Id del equipo
        /// </summary>
        public int? ID { get; set; }
        /// <summary>
        /// ESN del equipo
        /// </summary>
        public string ESN { get; set; }
        /// <summary>
        /// IMEI del equipo
        /// </summary>
        public string IMEI { get; set; }

        public DateTime? UltimoReporte { get; set; }
        public string Descripcion { get; set; }

        /// <summary>
        /// Numero Economico del vehiculo
        /// </summary>
        public string Nombre { get; set; }
        /// <summary>
        /// Serie del vehiculo
        /// </summary>
        public string Serie { get; set; }
        /// <summary>
        /// Placa del vehiculo
        /// </summary>
        public string Placa { get; set; }
        /// <summary>
        /// Modelo del vehiculo
        /// </summary>
        public string Modelo { get; set; }
        /// <summary>
        /// Año del vehiculo
        /// </summary>
        public string Anio { get; set; }
        /// <summary>
        /// Poliza del vehiculo
        /// </summary>
        public string Poliza { get; set; }
        /// <summary>
        /// Marca del vehiculo
        /// </summary>
        public string Marca { get; set; }

        /// <summary>
        /// Number
        /// </summary>
        public int? Numero { get; set; }
    }
}
