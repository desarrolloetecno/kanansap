﻿
using System;
using System.Runtime.Serialization;

namespace Kanan.Comun.BO2
{
    [Serializable]
    [DataContract]
    public class UnidadVolumen
    {
        [DataMember]
        public int? UnidadVolumenID { get; set; }
        [DataMember]
        public string Tipo { get; set; }
        [DataMember]
        public string Nomenclatura { get; set; }
        [DataMember]
        public double? Factor { get; set; }
        [DataMember]
        public double? FactorLts { get; set; }
    }
}
