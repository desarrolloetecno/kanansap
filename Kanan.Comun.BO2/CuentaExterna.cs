﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Kanan.Comun.BO2
{
    [Serializable]
    [DataContract]
    public class CuentaExterna : ICloneable
    {
        [DataMember]
        public int? CuentaExternaID { get; set; }
        [DataMember]
        public string Usuario { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public int? EmpresaID { get; set; }
        [DataMember]
        public Servidor CurrentServer { get; set; }


        public object Clone()
        {
            CuentaExterna cuenta = new CuentaExterna();
            cuenta.CuentaExternaID = this.CuentaExternaID;
            cuenta.Usuario = this.Usuario;
            cuenta.Password = this.Password;
            cuenta.EmpresaID = this.EmpresaID;
            if (this.CurrentServer != null)
            {
                cuenta.CurrentServer = (Servidor)this.CurrentServer.Clone();
            }
            return cuenta;
        }
    }
}
