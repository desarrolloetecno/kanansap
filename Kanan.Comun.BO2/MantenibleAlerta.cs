﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kanan.Comun.BO2
{
    /// <summary>
    /// Clase auxiliar para guardar y obtener los valores de los mantenibles de alerta mantenimiento, caja o vehiculo
    /// </summary>
    public class MantenibleAlerta
    {
        public int? MantenibleID { get; set; }
        public int? AlertaMantenimientoID { get; set; }
        public int? ServicioID { get; set; }
        public int? TipoServicioID { get; set; }
        public int? EmpresaID { get; set; }
        public int? SucursalID { get; set; }
        public string ser_Nombre { get; set; }
        public string ser_Tipo_Nombre { get; set; }
        public string SucursalDireccion { get; set; }
        public string veh_Nombre { get; set; }
        public string Placa { get; set; }
        public string DescripcionServicio { get; set; }
        public int? TipoAlerta { get; set; }
        public int? TypeOfType { get; set; }
        public double? DistanciaRecorrida { get; set; }
        public double? ProximoServicio { get; set; }
        public DateTime? ProximoServicioTiempo { get; set; }
        public int? TipoMantenibleID { get; set; }
    }
}
