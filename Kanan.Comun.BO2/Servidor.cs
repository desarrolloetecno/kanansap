﻿using System;
using System.Runtime.Serialization;

namespace Kanan.Comun.BO2
{
    [Serializable]
    [DataContract]
    public class Servidor : ICloneable
    {
        [DataMember]
        public int? ServidorID { get; set; }
        [DataMember]
        public string URL { get; set; }
        [DataMember]
        public string Usuario { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string Numero { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public bool? EstaActivo { get; set; }
        [DataMember]
        public int? TipoProveedor { get; set; }

        public Servidor()
        {
            this.EstaActivo = true;
        }


        public object Clone()
        {
            Servidor serv = new Servidor();
            serv.ServidorID = this.ServidorID;
            serv.Nombre = this.Nombre;
            serv.Numero = this.Numero;
            serv.Password = this.Password;
            serv.URL = this.URL;
            serv.Usuario = this.Usuario;
            serv.EstaActivo = this.EstaActivo;
            return serv;
        }
    }
}
