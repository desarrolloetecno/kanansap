﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kanan.Comun.BO2
{
    public class GeoReferencia
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int? Gid { get; set; }
        public TipoGeoReferencia Tipo { get; set; }
    }
}
