﻿using System;

namespace Kanan.Comun.BO2
{
    [Serializable]
    public class Notificacion:ICloneable
    {
        public int? NotificacionID { get; set; }
        public string Correos { get; set; }
        public string Celulares { get; set; }
        public Notificable Notificador { get; set; }

        public object Clone()
        {
            Notificacion notificacion = new Notificacion();
            notificacion.NotificacionID = this.NotificacionID;
            notificacion.Correos = this.Correos;
            notificacion.Celulares = this.Celulares;
            notificacion.Notificador = this.Notificador;
            return notificacion;
        }
    }
}
