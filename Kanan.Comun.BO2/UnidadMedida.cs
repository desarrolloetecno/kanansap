﻿#region C#
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#endregion C#

#region Kananfleet
using System.Runtime.Serialization;
#endregion Kananfleet

#region SAP
#endregion SAP

namespace Kanan.Comun.BO2
{
    /* Descripción
     * 
     * Objeto denegocios desarrollado para replicar
     * unidades de medida registradas en un cliente
     * SAP a las BD de Kananfleet. Se contemplanron
     * campos propios de SAP para tener una tabla
     * de equivalencias.
     * 
     * Development by: RaiR_nItRoN.
     */

    [Serializable]
    [DataContract]
    public class UnidadMedida:ICloneable
    {
        /// <summary>
        /// Corresponde a UomCode en SAP
        /// </summary>
        [DataMember]
        public String Code { get; set; }

        /// <summary>
        /// Corresponde a UomName en SAP
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Consecutivo de registro Kananfleet
        /// </summary>
        [DataMember]
        public int? UnidadMedidaID { get; set; }

        /// <summary>
        /// Nombre de unidad de medida Kananfleet
        /// </summary>
        [DataMember]
        public string Nombre { get; set; }

        /// <summary>
        /// Longitud, unidad de medida SAP
        /// </summary>
        [DataMember]
        public double? Longitud { get; set; }

        /// <summary>
        /// Ancho, unidad de medida SAP
        /// </summary>
        [DataMember]
        public double? Ancho { get; set; }

        /// <summary>
        /// Altura, unidad de medida SAP
        /// </summary>
        [DataMember]
        public double? Altura { get; set; }

        /// <summary>
        /// Volumen, unidad de medida SAP
        /// </summary>
        [DataMember]
        public double? Volumen { get; set; }

        /// <summary>
        /// UMVol, unidad de medida SAP
        /// </summary>
        [DataMember]
        public double? UMVol { get; set; }

        /// <summary>
        /// Peso, unidad de medida SAP
        /// </summary>
        [DataMember]
        public double? Peso { get; set; }

        /// <summary>
        /// Empresa que registró en Kananfleet. Se usa un int para evitar errores de referencia circular.
        /// </summary>
        [DataMember]
        public int? EmpresaID { get; set; }

        /// <summary>
        /// Fecha de modificación de unidad de medida Kananfleet
        /// </summary>
        [DataMember]
        public DateTime? FechaModificacion { get; set; }

        /// <summary>
        /// Estado de unidad de medida Kananfleet
        /// </summary>
        [DataMember]
        public bool Activo { get; set; }

        /* Constructor
         * 
         * Se inicializa el BO con la varaible
         * Activo siempre en true.
         */
        public UnidadMedida()
        {
            //Se inicializa el objeto siempre en 'true'.
            this.Activo = true;
        }

        public object Clone()
        {
            UnidadMedida unidadmedida = new UnidadMedida();

            unidadmedida.Code = this.Code;
            unidadmedida.Name = this.Name;
            unidadmedida.UnidadMedidaID = this.UnidadMedidaID;
            unidadmedida.Nombre = this.Nombre;
            unidadmedida.Longitud = this.Longitud;
            unidadmedida.Ancho = this.Ancho;
            unidadmedida.Altura = this.Altura;
            unidadmedida.Volumen = this.Volumen;
            unidadmedida.UMVol = this.UMVol;
            unidadmedida.Peso = this.Peso;
            unidadmedida.EmpresaID = this.EmpresaID;
            unidadmedida.FechaModificacion = this.FechaModificacion;
            unidadmedida.Activo = this.Activo;

            return unidadmedida;
        }
    }
}
