﻿using System;

namespace Kanan.Comun.BO2
{
    public class TipoMoneda: ICloneable
    {
        public int? TipoMonedaID { get; set; }
        public string Descripcion { get; set; }
        public bool Estatus { get; set; }

        public object Clone()
        {
            TipoMoneda tipo = new TipoMoneda();
            tipo.TipoMonedaID = this.TipoMonedaID;
            tipo.Descripcion = this.Descripcion;
            tipo.Estatus = this.Estatus;
            return tipo;
        }
    }
}
