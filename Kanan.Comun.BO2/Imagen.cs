﻿using System;
using System.Runtime.Serialization;

namespace Kanan.Comun.BO2
{
    [Serializable]
    [DataContract]
    public class Imagen : ICloneable
    {
        [DataMember]
        public int? ImagenID { get; set; }
        /// <summary>
        /// URL DE LA IMAGEN
        /// </summary>
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public string TipoImagen { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        //public byte[] Foto { get; set; }

        public object Clone()
        {
            Imagen imagen = new Imagen();
            imagen.ImagenID = this.ImagenID;
            imagen.Nombre = this.Nombre;
            imagen.Descripcion = this.Descripcion;
            //imagen.Foto = this.Foto;
            imagen.TipoImagen = this.TipoImagen;
            return imagen;
        }
    }
}
