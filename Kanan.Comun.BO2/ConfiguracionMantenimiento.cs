﻿
using System;
namespace Kanan.Comun.BO2
{
    [Serializable]
    public class ConfiguracionMantenimiento
    {
        public string ServidorCorreo { get; set; }
        public string DireccionCorreo { get; set; }
        public string Clave { get; set; }
        public string Puerto { get; set; }
        public bool EsSSL { get; set; }
        public int Periodo { get; set; }
        public string TiempoEntreEnvios { get; set; }
        public string BodyAviso { get; set; }
        public string BodyAlerta { get; set; }
       
        /// <summary>
        /// Indica el número de intentos por cada error generado
        /// </summary>
        public int? Intentos { get; set; }
        /// <summary>
        /// Tiempo de envio de correos despues de haberse generado las alertas
        /// </summary>
        public string TiempoXEnvioCorreos { get; set;}
        /// <summary>
        /// 
        /// </summary>
        public int NoCorreosXMinuto { get; set;}
    }
}
