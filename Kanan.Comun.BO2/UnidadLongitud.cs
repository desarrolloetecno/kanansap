﻿
using System;
using System.Runtime.Serialization;

namespace Kanan.Comun.BO2
{
    [Serializable]
    [DataContract]
    public class UnidadLongitud
    {
        [DataMember]
        public int? UnidadLongitudID { get; set; }
        [DataMember]
        public string Tipo { get; set; }
        [DataMember]
        public string Nomenclatura { get; set; }
        [DataMember]
        public double? Factor { get; set; }
        [DataMember]
        public double? FactorKm { get; set; }
    }
}
