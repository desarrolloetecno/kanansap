﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kanan.Comun.BO2
{
    public class MobileData
    {
        /// <summary>
        /// Modem
        /// </summary>
        public Modem Equipo { get; set; }
        /// <summary>
        /// RemoraID
        /// </summary>
        public int? MobileDataID { get; set; }
        /// <summary>
        /// TimeStamp
        /// </summary>
        public DateTime? UltimoReporte { get; set; }
        public double? Latitud { get; set; }
        public double? Longitud { get; set; }
        public double? Altitud { get; set; }
        public string Georeferencia { get; set; }
    }
}
