﻿
using System.Xml.Serialization;
using System;
namespace Kanan.Comun.BO2
{
    [Serializable]
    public enum EDimension
    {
        [XmlEnum("0")] /// Ponerlo por cada valor
        NORMAL = 0,
        [XmlEnum("1")]
        MEDIANO = 1,
        [XmlEnum("2")]
        MOVIL = 2
    }
}
