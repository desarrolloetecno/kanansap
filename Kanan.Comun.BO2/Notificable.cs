﻿
//using System.Runtime.Serialization;
namespace Kanan.Comun.BO2
{
    //[DataContract]
    public interface Notificable
    {
        /// <summary>
        /// Representa los destinatarios a quienes se le enviaran las notificaciones de las alertas
        /// </summary>
        int? NotificableID
        {
            get;
            set;
        }
    }
}
