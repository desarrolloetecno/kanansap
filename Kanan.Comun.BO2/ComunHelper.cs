﻿using System;

namespace Kanan.Comun.BO2
{
   [Serializable]
   public  class ComunHelper
    {
       public TimeSpan StringToTimeSpam(string timeSpan)
       {
           if (string.IsNullOrEmpty(timeSpan))
           {
               throw new Exception("El valor No puede ser nulo");
           }
           int dias = 0;
           int horas = 0;
           int minutos = 0;
           //int segundos = 0;
           TimeSpan time;
           try
           {
               string[] valores = timeSpan.Split(':');
               if (valores.Length < 1 || valores.Length > 3) throw new Exception();
               if (valores.Length >= 1)
               {
                   minutos = Convert.ToInt32(valores[valores.Length - 1]);
               }

               if (valores.Length >= 2)
               {
                   horas = Convert.ToInt32(valores[valores.Length - 2]);
               }

               if (valores.Length >= 3)
               {
                   dias = Convert.ToInt32(valores[valores.Length - 3]);
               }
               //if (valores.Length >= 4)
               //{
               //    dias = Convert.ToInt32(valores[valores.Length - 4]);
               //}
               time = new TimeSpan(dias, horas, minutos,0);

           }
           catch (Exception e)
           {
               throw new Exception("El valor no es correcto, verifique la cadena.");
           }
           return time;
       }

       public static string TimeSpanToStrin( TimeSpan time)
       { 
           string timeSpan = string.Empty;
           //if (time.Days.v)
           //{
               if (time.Days < 10)
               {
                   if (time.Days == 0)
                   {
                       timeSpan += "0" + time.Days + ":";
                   }
                   else
                   {
                       timeSpan += "0" + time.Days + ":";
                   }
               }
               else
               {
                   timeSpan += time.Days + ":";
               }
           //}
           //if (time.Hours != null)
           //{
               if (time.Hours < 10)
               {
                   if (time.Hours == 0)
                   {
                       timeSpan += "0" + time.Hours + ":";
                   }
                   else
                   {
                       timeSpan +="0"+ time.Hours + ":";
                   }
               }
               else
               {
                   timeSpan += time.Hours + ":";
               }
           //}
           //if (time.Minutes != null )
           //{
               if (time.Minutes < 10)
               {
                   if (time.Minutes == 0)
                   {
                       timeSpan += "0" + time.Minutes;
                   }
                   else
                   {
                       timeSpan += "0" + time.Minutes;
                   }
               }
               else
               {
                   timeSpan += time.Minutes;
               }
           //}

          return timeSpan;
           
       }
    }
}
