﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
namespace Kanan.Comun.BO2
{
    [DataContract]
    public enum TipoProveedor : int
    {
        [EnumMember]
        [XmlEnum("2")]
        Remora = 2,
        [EnumMember]
        [XmlEnum("1")]
        Kanan = 1
    }
}
