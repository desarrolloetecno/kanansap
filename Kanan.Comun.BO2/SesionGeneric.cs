﻿using System;

namespace Kanan.Comun.BO2
{
    public class SesionGeneric
    {
        /// <summary>
        /// Identificador del GID en la base de datos
        /// </summary>
        public string SesionID { get; set; }
        public string Usuario { get; set; }
        public string Password { get; set; }
        /// <summary>
        /// Indica el id del servidor
        /// </summary>
        public int? Servidor { get; set; }
        public DateTime? UltimaActualizacion { get; set; }
    }
}
