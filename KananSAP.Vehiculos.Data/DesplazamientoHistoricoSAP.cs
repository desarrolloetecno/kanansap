﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kanan.Vehiculos.BO2;
using SAPbobsCOM;

namespace KananSAP.Vehiculos.Data
{
    public class DesplazamientoHistoricoSAP
    {
        #region Propiedades
        public DesplazamientoHistorico DesplaHisto { get; set; }
        private SAPbobsCOM.Company Company;
        public UserTable oUDT { get; set; }

        //SQL || Hana
        Recordset Insertar;
        String Parametro;
        private String NombreTabla;
        public String NombreTablaDesplazamientoHistorico;
        bool EsSQL;
        #endregion Propiedades

        #region Constructor
        public DesplazamientoHistoricoSAP(SAPbobsCOM.Company company)
        {
            this.Company = company;
            //this.oUDT = company.UserTables.Item("VSKF_DESPLAHISTORIC");
            this.Insertar = (Recordset)this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            this.EsSQL = Company.DbServerType.ToString().Contains("MSSQL") ? true : false;
            this.NombreTabla = EsSQL ? "[@VSKF_DESPLAHISTORIC]" : @"""@VSKF_DESPLAHISTORIC""";

            this.NombreTablaDesplazamientoHistorico = this.NombreTabla;
        }
        #endregion Constructor

        #region Métodos

        #region CRUD
        public void Insert()
        {
            try
            {
                #region Fechas || Hora
                String Fecha = String.Empty;
                Fecha = this.DesplaHisto.Fecha.ToString("yyyy/MM/dd");
                String Hora = String.Empty;
                Hora = this.DesplaHisto.Fecha.ToString("HH:mm:ss");
                #endregion Fechas || Hora

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format("INSERT INTO {0} VALUES('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}')", this.NombreTabla, this.DesplaHisto.DesplazamientoHistoricoID, this.DesplaHisto.DesplazamientoHistoricoID, this.DesplaHisto.DesplazamientoHistoricoID, this.DesplaHisto.Distancia, Fecha, Hora, this.DesplaHisto.VehiculoID, this.DesplaHisto.NombreVehiculo, this.DesplaHisto.PlacaVehiculo, this.DesplaHisto.MarcaVehiculo, this.DesplaHisto.ModeloVehiculo);
                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana
            }
            catch (Exception ex)
            {
                #region GetLastError
                int CodigoError;
                String ErrorMessage;
                this.Company.GetLastError(out CodigoError, out ErrorMessage);
                throw new Exception("Codigo de error: " + CodigoError + ", Mensaje de error: " + ErrorMessage);
                #endregion GetLastError
            }
        }
        public void Update()
        {
            try
            {
                #region SQL || Hana
                #endregion SQL || Hana
            }
            catch (Exception ex)
            {
                #region GetLastError
                int CodigoError;
                String ErrorMessage;
                this.Company.GetLastError(out CodigoError, out ErrorMessage);
                throw new Exception("Codigo de error: " + CodigoError + ", Mensaje de error: " + ErrorMessage);
                #endregion GetLastError
            }
        }
        public void Delete(int DesplazamientoHistoricoID)
        {
            try
            {
                #region Validación
                int ID = 0;
                if (DesplazamientoHistoricoID > 0)
                    ID = DesplazamientoHistoricoID;
                else
                {
                    throw new Exception("Erron al intentar obtener identificador. ");
                }
                #endregion Validación

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code"" = '{1}'", this.NombreTabla, ID);
                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana
            }
            catch (Exception ex)
            {
                #region GetLastError
                int CodigoError;
                String ErrorMessage;
                this.Company.GetLastError(out CodigoError, out ErrorMessage);
                throw new Exception("Codigo de error: " + CodigoError + ", Mensaje de error: " + ErrorMessage);
                #endregion GetLastError
            }
        }
        #endregion CRUD

        #region Auxiliares

        #region GetNextID
        /* Comentarios
         * Método encargado de validar la cantidad de
         * registros y obtener el valor siguiente al
         * consecutivo de inserción.
         */
        public int GetNextID()
        {
            try
            {
                //Inicializando variable a retornar.
                int i = 0;

                //Limpiando variable y creando consultar.
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"SELECT ""Code"" FROM {0}", this.NombreTabla);

                //Ejecutando consulta.
                this.Insertar.DoQuery(this.Parametro);

                //Obteniendo consecutivo.
                i = this.Insertar.RecordCount > 0 ? this.Insertar.RecordCount + 1 : 1;

                //Validando consecutivo.
                if (i <= 0)
                    throw new Exception("Error al obtener consecutivo, consecutivo no puede ser menor a 0. ");
                if (this.Insertar.RecordCount >= i)
                    throw new Exception("Error al obtener consecutivo, consecutivo no puede ser menor a total de registros actuales. ");

                return i;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion GetNextID

        #region ValidarExistencia
        public bool ValidarExistencia(DateTime FechaHora, int VehiculoID)
        {
            bool Existe = false;
            try
            {
                #region Fecha || Hora
                String Fecha = String.Empty;
                Fecha = FechaHora.ToString("yyyy/MM/dd");
                String Hora = String.Empty;
                Hora = FechaHora.ToString("HH:mm:ss");
                #endregion Fecha || Hora

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format("SELECT * FROM {0} WHERE U_Fecha = '{1}' and U_Hora = '{2}' and U_VehiculoID = '{3}'", this.NombreTabla, Fecha, Hora, VehiculoID);
                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana

                #region Return
                if (this.Insertar.RecordCount > 0)
                    Existe = true;

                return Existe;
                #endregion Return
            }
            catch (Exception ex)
            {
                #region GetLastError
                int CodigoError;
                String ErrorMessage;
                this.Company.GetLastError(out CodigoError, out ErrorMessage);
                throw new Exception("Codigo de error: " + CodigoError + ", Mensaje de error: " + ErrorMessage);
                #endregion GetLastError
            }
        }
        #endregion ValidarExistencia

        #endregion Auxiliares

        #endregion Métodos
    }
}
