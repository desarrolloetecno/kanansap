﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using SAPbobsCOM;

namespace KananSAP.Vehiculos.Data
{
    public class TipoCombustibleSAP
    {
        #region Propiedades
        public TipoCombustible tipo { get; set; }
        public UserTable oUDT { get; set; }
        public static String ItemCode { get; set; }
        private Company company;
        //Hana
        Recordset Insertar;
        String Parametro;
        public String NombreTabla;
        bool EsSQL;
        #endregion Propiedades

        #region Constructor
        public TipoCombustibleSAP(Company comp)
        {
            this.company = comp;
            this.tipo = new TipoCombustible()
            {
                Empresa =  new Empresa(),
                Sucursal = new Sucursal()
            };
            //oItem = (SAPbobsCOM.Items) (this.company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems));
            try
            {
                this.Insertar = (Recordset)this.company.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.EsSQL = company.DbServerType.ToString().Contains("MSSQL");
                this.NombreTabla = EsSQL ? "[@VSKF_TYPEFUEL]" : @"""@VSKF_TYPEFUEL""";
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion Constructor

        #region Métodos

        #region Servicios
        public List<TipoCombustible> GetAllTypeFuel()
        {
            List<TipoCombustible> lista = new List<TipoCombustible>();

            this.Parametro = String.Empty;
            this.Parametro = String.Format("SELECT * FROM {0}", NombreTabla);
            this.Insertar.DoQuery(Parametro);
            lista = GetListFromRecordset(this.Insertar);

            return lista;
        }
        #endregion Servicio
        /* Comentarios
         * Método que se engarga de insertar, actualizar, eliminar
         * los datos de las tabla de tipos de combustible.
         * Actualmente no se utiliza debido a que no se tiene un mó-
         * dulo de gestión de para los tipo. Es por ello que en la im-
         * plementación se define y el soporte se encarga de solven-
         * tar algún problema o agregar un tipo de combustible extra
         * que no esté en los básicos de la implementación.
         */
        #region Acciones

        #endregion Acciones
        #endregion Métodos

        #region Auxiliares
        public List<TipoCombustible> GetListFromRecordset( Recordset recordset)
        {
            List<TipoCombustible> lista = new List<TipoCombustible>();
            TipoCombustible item = new TipoCombustible();
            recordset.MoveFirst();

            for (int i = 0; i < recordset.RecordCount; i++)
            {
                try
                {
                    item = null;
                    item = new TipoCombustible();

                    item.TipoCombustibleID = recordset.Fields.Item("U_TipoCombustibleID").Value;

                    item.Nombre = recordset.Fields.Item("U_Nombre").Value;
                    item.Descripcion = recordset.Fields.Item("U_Descripcion").Value;
                    item.Empresa.EmpresaID = recordset.Fields.Item("U_EmpresaID").Value;
                    item.Sucursal.SucursalID = recordset.Fields.Item("U_SucursalID").Value;
                    item.EstaActivo = recordset.Fields.Item("U_EstaActivo").Value;
                    item.UUID = recordset.Fields.Item("U_UUID").Value;
                    item.Sincronizado = recordset.Fields.Item("U_Sincronizado").Value;

                    recordset.MoveNext();
                    lista.Add(item);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al recuperar una la lista de combustibles. " + ex.Message);
                }
            }
            return lista;
        }
        #endregion Auxiliares
    }
}
