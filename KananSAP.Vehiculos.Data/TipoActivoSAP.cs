﻿using System;
using System.Collections.Generic;
using System.Text;
using Kanan.Vehiculos.BO2;
using SAPbobsCOM;

namespace KananSAP.Vehiculos.Data
{
    public class TipoActivoSAP
    {
        #region Propiedades

        public TipoActivos TipoActivo { get; set; }
        public UserTable oUDT { get; set; }
        public Items oItem { get; set; }
        public string ItemCode { get; set; }
        private Company oCompany;
        public int? Sincronizado { get; set; }
        public Guid UUID { get; set; }
        public bool ExistUDT()
        {
            return oUDT.GetByKey(this.ItemCode);
        }
        #endregion

        #region Constructor

        public TipoActivoSAP(Company company)
        {
            this.oCompany = company;
            this.TipoActivo = new TipoActivos();
            try
            {
                this.oUDT = company.UserTables.Item("VSKF_TIPOVEHICULO");
            }
            catch(Exception ex)
            {
                this.oUDT = null;
            }
        }

        #endregion


        public void TipoVehiculoToUDT()
        {
            try
            {
                int activo = 0;
                if (Convert.ToBoolean(this.TipoActivo.EsActivo))
                    activo = 1;
                oUDT.Code = this.ItemCode ?? this.TipoActivo.TipoActivoID.ToString();
                oUDT.Name = this.TipoActivo.Nombre;
                oUDT.UserFields.Fields.Item("U_EsActivo").Value = activo;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void UDTToTipoActivo()
        {
            try
            {
                this.TipoActivo = new TipoActivos();
                TipoActivo.TipoActivoID = string.IsNullOrEmpty(this.oUDT.Code) ? 0 : Convert.ToInt32(this.oUDT.Code);
                TipoActivo.Nombre = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Nombre").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Nombre").Value;
                TipoActivo.EsActivo = this.oUDT.UserFields.Fields.Item("U_EsActivo").Value == 1;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Insertar()
        {
            try
            {
                int result = oUDT.Add();
                if (result != 0)
                {
                    string errormsj;
                    int errornum;
                    oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Actualizar()
        {
            try
            {
                int result = oUDT.Update();
                if (result != 0)
                {
                    string errormsj; 
                    int errornum;
                    oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public void Eliminar()
        {
            try
            {
                int result = oUDT.Remove();
                if (result != 0)
                {
                    string errormsj;
                    int errornum;
                    oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        /// <summary>
        /// Consultar TipoVehiculo
        /// </summary>
        /// <param name="tvh">TipoVehiculo</param>
        /// <param name="isConsiderable">Toma en cuenta la Sucursal = false</param>
        /// <returns></returns>
        public Recordset Consultar(TipoActivos tvh)
        {
            try
            {
                if (tvh == null) tvh = new Kanan.Vehiculos.BO2.TipoActivos();
                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                //query.Append("Select U_TVeiculoID, U_Nombre, U_Descrip, U_SucursalID, U_ImagenUrl, U_EmpresaID, U_EsActivo from [@VSKF_TIPOVEHICULO] ");
                query.Append(@"SELECT ta.""Code"", ta.""Name"", ta.U_Estatus");
                query.Append(@" from  ""@VSKF_TIPOACTIVOS"" ta ");
                if (tvh.TipoActivoID != null)
                    where.Append(string.Format(@"and ta.""Code"" = {0} ", tvh.TipoActivoID));
                if (tvh.Nombre != null)
                    where.Append(string.Format(@"and ta.""Name"" = '{0}' ", tvh.Nombre));
                if (tvh.EsActivo != null)
                    where.Append(string.Format("and ta.U_Estatus = {0} ", (bool)tvh.EsActivo ? 1:0));
               
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith("and"))
                        filtro = filtro.Substring(3);
                    query.Append(" where " + filtro.ToString());
                }
                query.Append(@" order by  ta.""Code"" ");
                Recordset rs = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool HashTipoVehiculo(Recordset rs)
        {
            if (rs.RecordCount < 1)
                return false;
            return true;
        }

        public TipoActivos RowRecordSetToTipo(Recordset rs)
        {
            try
            {
                var tvReturn = new TipoActivos();
                //rs.MoveFirst();
                if (rs.Fields.Item("Code").Value != null && Convert.ToInt32(rs.Fields.Item("Code").Value) > 0)
                    tvReturn.TipoActivoID = Convert.ToInt32(rs.Fields.Item("Code").Value);
                if (!string.IsNullOrEmpty(rs.Fields.Item("Name").Value))
                    tvReturn.Nombre = (string)rs.Fields.Item("Name").Value;
                if (rs.Fields.Item("U_Estatus").Value != null)
                    tvReturn.EsActivo = Convert.ToBoolean(rs.Fields.Item("U_Estatus").Value);
                return tvReturn;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        
        public List<TipoActivos> RecordSetToListTipoActivos(Recordset rs)
        {
            try
            {
                List<TipoActivos> lst = new List<TipoActivos>();
                rs.MoveFirst();
                for(int i = 0; i < rs.RecordCount; i++)
                {
                    lst.Add(RowRecordSetToTipo(rs));
                    rs.MoveNext();
                }
                return lst;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
    }
}
