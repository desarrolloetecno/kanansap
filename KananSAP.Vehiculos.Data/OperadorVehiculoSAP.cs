﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using SAPbobsCOM;

namespace KananSAP.Vehiculos.Data
{
    public class OperadorVehiculoSAP
    {
        #region Propiedades
        private const string TABLE_SERVICE_NAME = "VSKF_OPERADORVEHICL";
        public OperadorVehiculo operadorv { get; set; }
        public UserTable oUDT { get; set; }
        public string ItemCode { get; set; }
        private Company Company;
        public int? Sincronizado { get; set; }
        public Guid? UUID { get; set; }

        //Migracion Hana
        Recordset Insertar;
        String Parametro;
        private String NombreTabla;
        bool EsSQL;

        //Se agregó para ser visible desde cualquier lugar.
        public String NombreTablaOperadorVehiculoSAP = String.Empty;


        public bool ExistUDT()
        {
            return oUDT.GetByKey(this.ItemCode);
        }
        #endregion

        #region Constructor
        public OperadorVehiculoSAP(SAPbobsCOM.Company company)
        {
            this.Company = company;
            this.operadorv = new OperadorVehiculo
            {
                OperadorAsignado = new Operador(),
                VehiculoAsignado = new Vehiculo()
            };
            try
            {
                this.oUDT = company.UserTables.Item(TABLE_SERVICE_NAME);

                //Migración HANA
                this.Insertar = (Recordset)this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.EsSQL = Company.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.NombreTabla = EsSQL ? "[@VSKF_OPERADORVEHICL]" : @"""@VSKF_OPERADORVEHICL""";
                //Se maneja en una variable pública para poder ser utilizada en otros lugares.
                this.NombreTablaOperadorVehiculoSAP = this.NombreTabla;

            }
            catch (Exception)
            {
                this.oUDT = null;
            }
        } 
        #endregion

        #region Métodos
        #region Metodos Datos

        public void OperadorToUDT()
        {
            oUDT.Code = this.ItemCode;
            oUDT.Name = this.ItemCode;
            this.oUDT.UserFields.Fields.Item("U_VehiculoID").Value = this.operadorv.VehiculoAsignado.VehiculoID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_OperadorID").Value = this.operadorv.OperadorAsignado.OperadorID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_FechaAsignacion").Value = this.operadorv.FechaAsignacion ?? new DateTime();
            this.oUDT.UserFields.Fields.Item("U_FechaLiberacion").Value = this.operadorv.FechaLiberacion ?? new DateTime();
            this.oUDT.UserFields.Fields.Item("U_ObservAsigna").Value = this.operadorv.ObservacionAsignacion ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_ObservaLibera").Value = this.operadorv.ObservacionLiberacion ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Sincronizado < 0? 0 : Sincronizado;
            this.oUDT.UserFields.Fields.Item("U_UUID").Value = this.UUID == null ? string.Empty : this.UUID.ToString();
        }

        public void UDTToOperador()
        {
            this.operadorv = new OperadorVehiculo
            {
                OperadorAsignado = new Operador(),
                VehiculoAsignado = new Vehiculo()
            };

            this.operadorv.OperadorVehiculoID = Convert.ToInt32(this.oUDT.Code) == 0 ? 0 : Convert.ToInt32(this.oUDT.Code);
            this.operadorv.VehiculoAsignado.VehiculoID = (int)this.oUDT.UserFields.Fields.Item("U_VehiculoID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_VehiculoID").Value;
            this.operadorv.OperadorAsignado.OperadorID = (int)this.oUDT.UserFields.Fields.Item("U_OperadorID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_OperadorID").Value;
            this.operadorv.FechaAsignacion = this.oUDT.UserFields.Fields.Item("U_FechaAsignacion").Value == null ? new DateTime() : (DateTime)this.oUDT.UserFields.Fields.Item("U_FechaAsignacion").Value;
            this.operadorv.FechaLiberacion = this.oUDT.UserFields.Fields.Item("U_FechaLiberacion").Value == null ? null : (DateTime?)this.oUDT.UserFields.Fields.Item("U_FechaLiberacion").Value;
            this.operadorv.ObservacionAsignacion = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_ObservAsigna").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_ObservAsigna").Value;
            this.operadorv.ObservacionLiberacion = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_ObservaLibera").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_ObservaLibera").Value;
            this.Sincronizado = (int)this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value;
            this.UUID = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_UUID").Value) ? new Guid() : Guid.Parse(this.oUDT.UserFields.Fields.Item("U_UUID").Value);
        }
        #endregion
        
        #region Acciones
        public void Insert(string Codigo)
        {
            try
            {
                #region Comentado 20.12.2019 depuracion de codigo
                /*String HoraMinutoAsigna = String.Empty;
                String HoraMinutoLibera = String.Empty;

                String FechaAsignacion;
                String FechaLiberacion;

                #region Fecha

                if (this.operadorv.FechaAsignacion != null)
                {
                    FechaAsignacion = this.operadorv.FechaAsignacion.Value.ToString("yyyy-MM-dd");

                    String Hora = this.operadorv.FechaAsignacion.Value.Hour.ToString().Length > 1 ? this.operadorv.FechaAsignacion.Value.Hour.ToString() + ":" : "0" + this.operadorv.FechaAsignacion.Value.Hour.ToString() + ":";
                    String Minuto = this.operadorv.FechaAsignacion.Value.Minute.ToString().Length > 1 ? this.operadorv.FechaAsignacion.Value.Minute.ToString() : "0" + this.operadorv.FechaAsignacion.Value.Minute.ToString();
                    HoraMinutoAsigna = Hora + Minuto;
                }
                else
                    FechaAsignacion = String.Empty;

                if (this.operadorv.FechaLiberacion != null)
                {
                    FechaLiberacion = this.operadorv.FechaLiberacion.Value.ToString("yyyy-MM-dd");

                    String Hora2 = this.operadorv.FechaLiberacion.Value.Hour.ToString().Length > 1 ? this.operadorv.FechaLiberacion.Value.Hour.ToString() + ":" : "0" + this.operadorv.FechaLiberacion.Value.Hour.ToString() + ":";
                    String Minuto2 = this.operadorv.FechaLiberacion.Value.Minute.ToString().Length > 1 ? this.operadorv.FechaLiberacion.Value.Minute.ToString() : "0" + this.operadorv.FechaLiberacion.Value.Minute.ToString();
                    HoraMinutoLibera = Hora2 + Minuto2;
                }
                else
                    FechaLiberacion = String.Empty;
                #endregion Fecha*/
                
                /*Parametro = String.Format("INSERT INTO {0} VALUES('{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}')", this.NombreTabla, this.operadorv.VehiculoAsignado.VehiculoID, this.operadorv.VehiculoAsignado.VehiculoID, this.operadorv.VehiculoAsignado.VehiculoID, this.operadorv.OperadorAsignado.OperadorID, "FechaAsignacion", "HoraMinutoAsigna", this.operadorv.FechaLiberacion == null ? null : Convert.ToDateTime(this.operadorv.FechaLiberacion.Value), "HoraMinutoLibera", this.operadorv.ObservacionAsignacion, this.operadorv.ObservacionLiberacion, "0", this.UUID);*/

                //Insertar.DoQuery(Parametro);
                #endregion Comentado 20.12.2019 depuracion de codigo

                string sresponse = string.Empty;
                UserTable oTabla = this.Company.UserTables.Item("VSKF_OPERADORVEHICL");
                oTabla.Code = Codigo.ToString();
                oTabla.Name = Codigo.ToString();
                oTabla.UserFields.Fields.Item("U_VehiculoID").Value = Convert.ToInt32(this.operadorv.VehiculoAsignado.VehiculoID);
                oTabla.UserFields.Fields.Item("U_OperadorID").Value = Convert.ToInt32(this.operadorv.OperadorAsignado.OperadorID);
                oTabla.UserFields.Fields.Item("U_FechaAsignacion").Value = this.operadorv.FechaAsignacion == null ? DateTime.MinValue : Convert.ToDateTime(this.operadorv.FechaAsignacion);
                oTabla.UserFields.Fields.Item("U_HoraMinutoAsigna").Value = this.operadorv.FechaAsignacion == null ? "00:00" : Convert.ToDateTime(this.operadorv.FechaAsignacion).ToString("H:mm");
                oTabla.UserFields.Fields.Item("U_ObservAsigna").Value = this.operadorv.ObservacionAsignacion;
                oTabla.UserFields.Fields.Item("U_Sincronizado").Value = 0;
                oTabla.UserFields.Fields.Item("U_UUID").Value = this.UUID.ToString();
                oTabla.UserFields.Fields.Item("U_Liberado").Value = 1; //corresponde a ocupado
                int icommand = oTabla.Add();
                if (icommand != 0)
                {
                    this.Company.GetLastError(out icommand, out sresponse);
                    throw new Exception(string.Format("No se ha podido asignar el vehiculo. ERROR {0}", sresponse));
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Remove()
        {
            if (oUDT.GetByKey(this.ItemCode))
            {
                string ID = String.Empty;
                if (!String.IsNullOrEmpty(oUDT.Code))
                    ID = oUDT.Code;
                else
                {
                    throw new Exception("Erron al intentar obtener identificador. ");
                }

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code"" = '{1}'", this.NombreTabla, ID);

                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana

                #region SQL
                /*int flag = oUDT.Remove();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    Company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }*/
                #endregion SQL
            }
        }

        public void Update()
        {

            string sql = string.Format(@" SELECT * FROM ""@VSKF_OPERADORVEHICL"" WHERE  U_VehiculoID = {0} AND U_OperadorID = {1} AND U_Liberado = 1", this.operadorv.VehiculoAsignado.VehiculoID, this.operadorv.OperadorAsignado.OperadorID);
            Recordset oRecord = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            oRecord.DoQuery(sql);
            if (oRecord.RecordCount > 0)
            {
                oRecord.MoveFirst();
                string sresponse = string.Empty;
                UserTable oTabla = this.Company.UserTables.Item("VSKF_OPERADORVEHICL");
                oTabla.GetByKey(Convert.ToString(oRecord.Fields.Item("Code").Value));
                oTabla.UserFields.Fields.Item("U_FechaLiberacion").Value = this.operadorv.FechaLiberacion == null ? DateTime.MinValue : Convert.ToDateTime(this.operadorv.FechaLiberacion);
                oTabla.UserFields.Fields.Item("U_HoraMinutoLibera").Value = this.operadorv.FechaLiberacion == null ? "00:00" : Convert.ToDateTime(this.operadorv.FechaLiberacion).ToString("H:mm");
                oTabla.UserFields.Fields.Item("U_ObservaLibera").Value = this.operadorv.ObservacionLiberacion;
                oTabla.UserFields.Fields.Item("U_Liberado").Value = 2; //dos corresponde a liberado
                int icommand = oTabla.Update();
                if (icommand != 0)
                {
                    this.Company.GetLastError(out icommand, out sresponse);
                    throw new Exception(string.Format("No se ha podido asignar el vehiculo. ERROR {0}", sresponse));
                }
            }
            else throw new Exception("No se ha poddio recuperar el registro para su liberación");
            #region Fecha
           /* if (this.operadorv.FechaAsignacion != null)
            {
                FechaAsignacion = this.operadorv.FechaAsignacion.Value.ToString("yyyy-MM-dd");

                String Hora = this.operadorv.FechaAsignacion.Value.Hour.ToString().Length > 1 ? this.operadorv.FechaAsignacion.Value.Hour.ToString() + ":" : "0" + this.operadorv.FechaAsignacion.Value.Hour.ToString() + ":";
                String Minuto = this.operadorv.FechaAsignacion.Value.Minute.ToString().Length > 1 ? this.operadorv.FechaAsignacion.Value.Minute.ToString() : "0" + this.operadorv.FechaAsignacion.Value.Minute.ToString();
                HoraMinutoAsigna = Hora + Minuto;
            }

            if (this.operadorv.FechaLiberacion != null)
            {
                FechaLiberacion = this.operadorv.FechaLiberacion.Value.ToString("yyyy-MM-dd");

                String Hora2 = this.operadorv.FechaLiberacion.Value.Hour.ToString().Length > 1 ? this.operadorv.FechaLiberacion.Value.Hour.ToString() + ":" : "0" + this.operadorv.FechaLiberacion.Value.Hour.ToString() + ":";
                String Minuto2 = this.operadorv.FechaLiberacion.Value.Minute.ToString().Length > 1 ? this.operadorv.FechaLiberacion.Value.Minute.ToString() : "0" + this.operadorv.FechaLiberacion.Value.Minute.ToString();
                HoraMinutoLibera = Hora2 + Minuto2;
            }

            #endregion Fecha

            #region SQL || Hana
            this.Parametro = String.Empty;
            this.Parametro = String.Format(@"UPDATE {0} SET ""Code"" = '{1}', ""Name"" = '{2}', U_VehiculoID = '{3}', U_OperadorID = '{4}', U_FechaAsignacion = '{5}', U_HoraMinutoAsigna = '{6}', U_FechaLiberacion = '{7}', U_HoraMinutoLibera = '{8}', U_ObservAsigna = '{9}', U_ObservaLibera = '{10}', U_Sincronizado = '{11}' WHERE ""Code"" = '{1}'", this.NombreTabla, this.operadorv.OperadorVehiculoID, this.operadorv.OperadorVehiculoID, this.operadorv.VehiculoAsignado.VehiculoID, this.operadorv.OperadorAsignado.OperadorID, FechaAsignacion, HoraMinutoAsigna, FechaLiberacion, HoraMinutoLibera, this.operadorv.ObservacionAsignacion, this.operadorv.ObservacionLiberacion, this.Sincronizado);

            this.Insertar.DoQuery(this.Parametro);*/
            #endregion SQL || Hana

            #region SQL
            /*int flag = oUDT.Update();
            if (flag != 0)
            {
                int errornum;
                string errormsj;
                Company.GetLastError(out errornum, out errormsj);
                throw new Exception("Error: " + errornum + " - " + errormsj);
            }*/
            #endregion SQL
        }
        #endregion

        #region Services
        public Recordset Consultar(OperadorVehiculo op)
        {
            try
            {
                Recordset rs = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                if (op == null) op = new OperadorVehiculo();
                if (op.VehiculoAsignado == null) op.VehiculoAsignado = new Vehiculo();
                if (op.OperadorAsignado == null) op.OperadorAsignado = new Operador();
                

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(" SELECT * from " + this.NombreTabla + " as op ");

                if (op.OperadorVehiculoID != null && op.OperadorVehiculoID > 0)
                    where.Append(string.Format(@" and op.""Code"" = {0} ", op.OperadorVehiculoID));
                if (op.VehiculoAsignado.VehiculoID != null)
                    where.Append(string.Format(" and op.U_VehiculoID = '{0}' ", op.VehiculoAsignado.VehiculoID));
                if (op.OperadorAsignado.OperadorID != null)
                    where.Append(string.Format(" and op.U_OperadorID = '{0}' ", op.OperadorAsignado.OperadorID));
                if (op.FechaAsignacion != null)
                    where.Append(string.Format(" and op.U_FechaAsignacion = {0} ", op.FechaAsignacion));
                if (op.FechaLiberacion != null)
                    where.Append(string.Format(" and op.U_FechaLiberacion = {0} ", op.FechaLiberacion));
                if (!string.IsNullOrEmpty(op.ObservacionAsignacion))
                    where.Append(string.Format(" and op.U_ObservAsigna = '{0}' ", op.ObservacionAsignacion));
                if (!string.IsNullOrEmpty(op.ObservacionLiberacion))
                    where.Append(string.Format(" and op.U_ObservaLibera = '{0}' ", op.ObservacionLiberacion));
                if (Sincronizado != null)
                    where.Append(string.Format(" and op.U_Sincronizado = {0} ", Sincronizado));
                if (UUID != null)
                    where.Append(string.Format(" and op.U_UUID = '{0}' ", UUID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                query.Append(@" order by op.""Code"" desc ");
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<OperadorVehiculo> RecordSetToListOperador(Recordset rs)
        {
            var result = new List<OperadorVehiculo>();
            for (int i = 0; i < rs.RecordCount; i++)
            {
                this.ItemCode = rs.Fields.Item("Code").Value;
                this.oUDT.GetByKey(this.ItemCode);
                UDTToOperador();
                var temp = (OperadorVehiculo)this.operadorv.Clone();
                result.Add(temp);
            }
            return result;
        }

        public bool SetCurrentOperadorVehiculoByOperadorID(int operadorid)
        {

            try
            {
                var rs = this.Consultar(new OperadorVehiculo { OperadorAsignado = new Operador { OperadorID = operadorid } });
                var firstOrDefault = RecordSetToListOperador(rs).OrderByDescending(x => x.FechaAsignacion).FirstOrDefault(x => string.IsNullOrEmpty(x.ObservacionLiberacion));
                if (firstOrDefault != null)
                {
                    this.ItemCode = firstOrDefault.OperadorVehiculoID.ToString();
                    this.oUDT.GetByKey(this.ItemCode);
                    UDTToOperador();
                    return true;
                }
                this.operadorv = new OperadorVehiculo
                {
                    OperadorAsignado = new Operador(),
                    VehiculoAsignado = new Vehiculo()
                };
                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Recordset ActualizarCode(OperadorVehiculo op)
        {
            try
            {
                Recordset rs = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(@" UPDATE " + this.NombreTabla + @" SET  U_Sincronizado = " + Sincronizado);
                  //""Code"" = '" + op.OperadorVehiculoID + @"', ""Name"" = '" + op.OperadorVehiculoID +
                if (UUID != null)
                    where.Append(string.Format(" and U_UUID like '%{0}%' ", UUID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #endregion
    }
}
