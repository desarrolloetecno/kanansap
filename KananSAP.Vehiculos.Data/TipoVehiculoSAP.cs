﻿using Kanan.Vehiculos.BO2;
using KananSAP.Operaciones.Data;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.Vehiculos.Data
{
    public class TipoVehiculoSAP
    {
        #region Propiedades

        public TipoVehiculo TipoVehiculo { get; set; }
        public UserTable oUDT { get; set; }
        public Items oItem { get; set; }
        public string ItemCode { get; set; }
        private Company oCompany;
        public int? Sincronizado { get; set; }
        public Guid UUID { get; set; }
        public SucursalSAP SucursalSAP { get; set; }

        //Migracion Hana
        Recordset Insert;
        String Parametro;
        private String NombreTabla;
        bool EsSQL;

        //Se creó para ser visible desde otros proyectos y poder realizar consultas
        // con un recordser en DB Hana
        public String NombreTablaTipoVehiculoSAP = String.Empty;

        public bool ExistUDT()
        {
            return oUDT.GetByKey(this.ItemCode);
        }
        #endregion

        #region Constructor

        public TipoVehiculoSAP(Company company)
        {
            this.oCompany = company;
            this.TipoVehiculo = new TipoVehiculo()
            {
                Propietario = new Kanan.Operaciones.BO2.Empresa(),
                SubPropietario = new Kanan.Operaciones.BO2.Sucursal(),
            };
            this.SucursalSAP = new SucursalSAP(this.oCompany);
            try
            {
                this.oUDT = company.UserTables.Item("VSKF_TIPOVEHICULO");
                //Migración HANA
                this.Insert = (Recordset)this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.EsSQL = oCompany.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.NombreTabla = EsSQL ? "[@VSKF_TIPOVEHICULO]" : @"""@VSKF_TIPOVEHICULO""";

                this.NombreTablaTipoVehiculoSAP = this.NombreTabla;

                //Verificar que el nombre de la tabla sea correcto.
                /*String Tabla = Convert.ToString(company.UserTables);
                String NombreTablaPrueba = EsSQL ? String.Format("[@{0}]",Tabla) : String.Format(@"""@{0}""",Tabla);*/
            }
            catch (Exception ex)
            {
                this.oUDT = null;
            }
        }

        #endregion


        public void TipoVehiculoToUDT()
        {
            try
            {
                int activo = 0;
                if (Convert.ToBoolean(this.TipoVehiculo.EsActivo))
                    activo = 1;
                oUDT.Code = this.ItemCode ?? this.TipoVehiculo.TipoVehiculoID.ToString();
                //oUDT.Name = this.TipoVehiculo.Nombre;
                oUDT.Name = this.TipoVehiculo.Nombre.Length > 30 ? TipoVehiculo.Nombre.Substring(0, 30) : TipoVehiculo.Nombre;
                oUDT.UserFields.Fields.Item("U_TVeiculoID").Value = this.TipoVehiculo.TipoVehiculoID ?? 0;
                oUDT.UserFields.Fields.Item("U_Nombre").Value = this.TipoVehiculo.Nombre ?? string.Empty;
                oUDT.UserFields.Fields.Item("U_Descrip").Value = this.TipoVehiculo.Descripcion ?? string.Empty;
                oUDT.UserFields.Fields.Item("U_SucursalID").Value = this.TipoVehiculo.SubPropietario.SucursalID ?? 0;
                oUDT.UserFields.Fields.Item("U_ImagenUrl").Value = this.TipoVehiculo.ImagenUrl ?? string.Empty;
                oUDT.UserFields.Fields.Item("U_EmpresaID").Value = this.TipoVehiculo.Propietario.EmpresaID ?? 0;
                oUDT.UserFields.Fields.Item("U_EsActivo").Value = activo;
                oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Sincronizado ?? 0;
                oUDT.UserFields.Fields.Item("U_UUID").Value = this.UUID.ToString() ?? string.Empty;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Inserta un Tipo de Vehiculo.
        /// </summary>
        public void Insertar()
        {
            try
            {
                int Activo=0;
                if(Convert.ToBoolean(this.TipoVehiculo.EsActivo))
                    Activo = 1;

                #region SQL || HANA
                this.Parametro = String.Empty;
                this.Parametro = String.Format("INSERT INTO {0} VALUES('{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}')", this.NombreTabla, this.TipoVehiculo.TipoVehiculoID, this.TipoVehiculo.Nombre, this.TipoVehiculo.TipoVehiculoID, this.TipoVehiculo.Nombre, this.TipoVehiculo.Descripcion, "0", this.TipoVehiculo.ImagenUrl, this.TipoVehiculo.Propietario.EmpresaID, Activo, "0", Convert.ToString(this.UUID));
                this.Insert.DoQuery(Parametro);
                #endregion SQL || HANA
            }
            catch (Exception ex)
            {
                #region SQL
                int result = oUDT.Add();
                string errormsj;
                int errornum;
                if (result != 0)
                {
                    //oCompany.GetLastError(out errornum, out errormsj);
                    //throw new Exception("Error: " + errornum + " - " + errormsj);
                    
                    throw new Exception("Error al intentar insertar el tipo de vehículo. " + ex.Message);
                }
                #endregion SQL
            }
        }

        /// <summary>
        /// Actualiza un Tipo de Vehiculo.
        /// </summary>
        public void Actualizar()
        {
            try
            {
                int Activo = 0;
                if (Convert.ToBoolean(this.TipoVehiculo.EsActivo))
                    Activo = 1;

                int? ValSinc;
                ValSinc = String.IsNullOrEmpty(Convert.ToString(this.Sincronizado)) ? ValSinc = 0 : ValSinc = this.Sincronizado;

                #region SQL || HANA
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET ""Code""='{1}',""Name""='{2}', U_TVeiculoID='{3}', U_Nombre='{4}', U_Descrip='{5}', U_SucursalID='{6}', U_ImagenUrl='{7}', U_EmpresaID='{8}', U_EsActivo='{9}' WHERE ""Code""='{1}'", this.NombreTabla, this.TipoVehiculo.TipoVehiculoID, this.TipoVehiculo.Nombre, this.TipoVehiculo.TipoVehiculoID, this.TipoVehiculo.Nombre, this.TipoVehiculo.Descripcion, "0", this.TipoVehiculo.ImagenUrl, this.TipoVehiculo.Propietario.EmpresaID, Activo);
                this.Insert.DoQuery(this.Parametro);
                #endregion SQL || HANA

                #region SQL
                //SQL
                /*int result = oUDT.Update();
                string errormsj;
                int errornum;
                if (result != 0)
                {
                    oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }*/
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar actualizar un tipo de vehículo. " + ex.Message);
            }

        }

        /// <summary>
        /// Elimina un Tipo de Vehiculo.
        /// </summary>
        public void Eliminar()
        {
            try
            {
                //Se obtiene el ID del registro a eliminar.
                String DeleteID = String.Empty;
                DeleteID = oUDT.Code;

                #region SQL || HANA
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code""='{1}'", this.NombreTabla, DeleteID);
                this.Insert.DoQuery(this.Parametro);
                #endregion

                #region SQL
                //SQL
                /*int result = oUDT.Remove();
                string errormsj;
                int errornum;
                if (result != 0)
                {
                    oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }*/
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar eliminar un tipo de vehículo. " + ex.Message);
            }

        }

        /// <summary>
        /// Consultar TipoVehiculo
        /// </summary>
        /// <param name="tvh">TipoVehiculo</param>
        /// <returns></returns>
        public Recordset Consultar(TipoVehiculo tvh)
        {
            try
            {
                Recordset rs = this.Consultar(tvh, false);
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Consultar TipoVehiculo
        /// </summary>
        /// <param name="tvh">TipoVehiculo</param>
        /// <param name="isConsiderable">Toma en cuenta la Sucursal = false</param>
        /// <returns></returns>
        public Recordset Consultar(TipoVehiculo tvh, bool? isConsiderable)
        {
            try
            {
                if (tvh == null) tvh = new Kanan.Vehiculos.BO2.TipoVehiculo();
                if (tvh.SubPropietario == null) tvh.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
                if (tvh.Propietario == null) tvh.Propietario = new Kanan.Operaciones.BO2.Empresa();
                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                //query.Append("Select U_TVeiculoID, U_Nombre, U_Descrip, U_SucursalID, U_ImagenUrl, U_EmpresaID, U_EsActivo from [@VSKF_TIPOVEHICULO] ");
                query.Append("SELECT tp.U_TVeiculoID, tp.U_Nombre, tp.U_EsActivo, tp.U_Descrip,tp.U_SucursalID, tp.U_EmpresaID, tp.U_ImagenUrl, sc.U_Direccion as Sucursal ");

                //SQL || HANA
                query.Append(" from  " + this.NombreTabla + " tp left outer join " + this.SucursalSAP.NombreTablaSucursalSAP + " sc on sc.U_SucursalID = tp.U_SucursalID ");
                
                //SQL
                //query.Append(" from  [@VSKF_TIPOVEHICULO] tp left outer join [@VSKF_SUCURSAL] sc on sc.U_SucursalID = tp.U_SucursalID ");
                if (tvh.TipoVehiculoID != null)
                    where.Append(string.Format("and tp.U_TVeiculoID = {0} ", tvh.TipoVehiculoID));
                if (tvh.Nombre != null)
                    where.Append(string.Format("and tp.U_Nombre = '{0}' ", tvh.Nombre));
                if (tvh.Descripcion != null)
                    where.Append(string.Format("and tp.U_Descrip = '{0}' ", tvh.Descripcion));
                if (tvh.ImagenUrl != null)
                    where.Append(string.Format("and tp.U_ImagenUrl = '{0}' ", tvh.ImagenUrl));
                if (tvh.Propietario.EmpresaID != null)
                    where.Append(string.Format("and tp.U_EmpresaID = {0} ", tvh.Propietario.EmpresaID));
                if (!(bool)isConsiderable)
                {
                    if (tvh.SubPropietario.SucursalID != null)
                        where.Append(string.Format("and tp.U_SucursalID = {0} ", tvh.SubPropietario.SucursalID));
                }
                if (tvh.EsActivo != null)
                    where.Append(string.Format("and tp.U_EsActivo = {0} ", 1));

                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith("and"))
                        filtro = filtro.Substring(3);
                    query.Append(" where " + filtro.ToString());
                }
                query.Append(" order by  tp.U_TVeiculoID desc ");
                Recordset rs = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool HashTipoVehiculo(Recordset rs)
        {
            if (rs.RecordCount < 1)
                return false;
            return true;
        }

        public TipoVehiculo LastRecordSetToTipoVehiculo(Recordset rs)
        {
            try
            {
                TipoVehiculo tvReturn = new TipoVehiculo();
                tvReturn.Propietario = new Kanan.Operaciones.BO2.Empresa();
                tvReturn.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
                rs.MoveFirst();
                if (rs.Fields.Item("U_TVeiculoID").Value != null && rs.Fields.Item("U_TVeiculoID").Value > 0)
                    tvReturn.TipoVehiculoID = Convert.ToInt32(rs.Fields.Item("U_TVeiculoID").Value);
                if (!string.IsNullOrEmpty(rs.Fields.Item("U_Nombre").Value))
                    tvReturn.Nombre = (string)rs.Fields.Item("U_Nombre").Value;
                if (!string.IsNullOrEmpty(rs.Fields.Item("U_Descrip").Value))
                    tvReturn.Descripcion = (string)rs.Fields.Item("U_Descrip").Value;
                if (rs.Fields.Item("U_SucursalID").Value != null && rs.Fields.Item("U_SucursalID").Value > 0)
                    tvReturn.SubPropietario.SucursalID = Convert.ToInt32(rs.Fields.Item("U_SucursalID").Value);
                if (!string.IsNullOrEmpty(rs.Fields.Item("U_ImagenUrl").Value))
                    tvReturn.ImagenUrl = (string)rs.Fields.Item("U_ImagenUrl").Value;
                if (rs.Fields.Item("U_EmpresaID").Value != null && rs.Fields.Item("U_EmpresaID").Value > 0)
                    tvReturn.Propietario.EmpresaID = Convert.ToInt32(rs.Fields.Item("U_EmpresaID").Value);
                if (rs.Fields.Item("Sucursal").Value != null)
                    tvReturn.SubPropietario.Direccion = rs.Fields.Item("Sucursal").Value;
                if (rs.Fields.Item("U_EsActivo").Value != null)
                    tvReturn.EsActivo = Convert.ToBoolean(rs.Fields.Item("U_EsActivo").Value);
                return tvReturn;
            }
            catch (Exception ex)
            {
                throw new Exception("Error en LastRecordSetToTipoVehiculo. " + ex.Message);
            }
        }

        public TipoVehiculo RowRecordSetToTipoVehiculo(Recordset rs)
        {
            try
            {
                TipoVehiculo tvReturn = new TipoVehiculo();
                tvReturn.Propietario = new Kanan.Operaciones.BO2.Empresa();
                tvReturn.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
                //rs.MoveFirst();
                if (rs.Fields.Item("U_TVeiculoID").Value != null && rs.Fields.Item("U_TVeiculoID").Value > 0)
                    tvReturn.TipoVehiculoID = Convert.ToInt32(rs.Fields.Item("U_TVeiculoID").Value);
                if (!string.IsNullOrEmpty(rs.Fields.Item("U_Nombre").Value))
                    tvReturn.Nombre = (string)rs.Fields.Item("U_Nombre").Value;
                if (!string.IsNullOrEmpty(rs.Fields.Item("U_Descrip").Value))
                    tvReturn.Descripcion = (string)rs.Fields.Item("U_Descrip").Value;
                if (rs.Fields.Item("U_SucursalID").Value != null && rs.Fields.Item("U_SucursalID").Value > 0)
                    tvReturn.SubPropietario.SucursalID = Convert.ToInt32(rs.Fields.Item("U_SucursalID").Value);
                if (!string.IsNullOrEmpty(rs.Fields.Item("U_ImagenUrl").Value))
                    tvReturn.ImagenUrl = (string)rs.Fields.Item("U_ImagenUrl").Value;
                if (rs.Fields.Item("U_EmpresaID").Value != null && rs.Fields.Item("U_EmpresaID").Value > 0)
                    tvReturn.Propietario.EmpresaID = Convert.ToInt32(rs.Fields.Item("U_EmpresaID").Value);
                if (rs.Fields.Item("Sucursal").Value != null)
                    tvReturn.SubPropietario.Direccion = rs.Fields.Item("Sucursal").Value;
                if (rs.Fields.Item("U_EsActivo").Value != null)
                    tvReturn.EsActivo = Convert.ToBoolean(rs.Fields.Item("U_EsActivo").Value);
                return tvReturn;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public List<TipoVehiculo> RecordSetToListTipoVehiculo(Recordset rs)
        {
            try
            {
                List<TipoVehiculo> lst = new List<TipoVehiculo>();
                rs.MoveFirst();
                for (int i = 0; i < rs.RecordCount; i++)
                {
                    lst.Add(RowRecordSetToTipoVehiculo(rs));
                    rs.MoveNext();
                }
                return lst;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public bool SetUDTByTipoVehiculoID(int? id)
        {
            try
            {
                var rs = this.ConsultarByID(id);
                int _id = 0;
                if (HashTipoVehiculo(rs))
                {
                    rs.MoveFirst();
                    _id = rs.Fields.Item("U_TVeiculoID").Value;
                    if (_id == id)
                        return this.oUDT.GetByKey(rs.Fields.Item("Code").Value.ToString());

                }
                this.oUDT.Code = string.Empty;
                return false;
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }

        public Recordset ConsultarByID(int? id)
        {
            try
            {
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(string.Format("select * from " + this.NombreTabla + " where U_TVeiculoID = {0} and U_EsActivo = 1", id));
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Desactivar un tipo de vehículo.
        /// </summary>
        public void Deactivate()
        {
            try
            {
                #region SQL || HANA
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET U_EsActivo = 0 WHERE ""Code"" = '{1}'", this.NombreTabla, this.oUDT.Code);
                this.Insert.DoQuery(this.Parametro);
                #endregion SQL || HANA

                #region SQL
                //SQL
                /*this.oUDT.UserFields.Fields.Item("U_EsActivo").Value = 0;
                this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Sincronizado == null ? 0 : this.Sincronizado;
                Actualizar();*/
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar desactivar el tipo de vehículo. " + ex.Message);
            }
        }

        public void UDTToTipoVehiculo()
        {
            try
            {
                this.TipoVehiculo = new TipoVehiculo { Propietario = new Kanan.Operaciones.BO2.Empresa(), SubPropietario = new Kanan.Operaciones.BO2.Sucursal() };
                TipoVehiculo.TipoVehiculoID = (int)this.oUDT.UserFields.Fields.Item("U_TVeiculoID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_TVeiculoID").Value;
                TipoVehiculo.Nombre = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Nombre").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Nombre").Value;
                TipoVehiculo.Descripcion = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Descrip").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Descrip").Value;
                TipoVehiculo.SubPropietario.SucursalID = (int)this.oUDT.UserFields.Fields.Item("U_SucursalID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_SucursalID").Value;
                TipoVehiculo.ImagenUrl = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_ImagenUrl").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_ImagenUrl").Value;
                TipoVehiculo.Propietario.EmpresaID = (int)this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value;
                TipoVehiculo.EsActivo = this.oUDT.UserFields.Fields.Item("U_EsActivo").Value == 1;
            }
            catch (Exception ex)
            {
                throw new Exception("Error en UDTToTipoVehiculo. " + ex.Message);
            }
        }

        public void GetSyncUUID()
        {
            try
            {
                if (!string.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_UUID").Value))
                    this.UUID = Guid.Parse(this.oUDT.UserFields.Fields.Item("U_UUID").Value);
                this.Sincronizado = this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void Sincronizar()
        {
            try
            {
                int? ValSinc;
                ValSinc = this.Sincronizado == null ? 0 : this.Sincronizado;

                #region SQL || HANA
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET U_Sincronizado = '{1}' WHERE ""Code"" = '{2}'", this.NombreTabla, ValSinc, this.oUDT.Code);
                #endregion SQL || HANA

                #region SQL
                //SQL
                /*this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Sincronizado == null ? 0 : this.Sincronizado;
                this.Actualizar();*/
                #endregion SQL
            }
            catch (Exception ex) 
            { 
                throw new Exception("Error al intentar sincronizar el tipo de vehículo.  " + ex.Message);
            }
        }

        public Recordset ActualizarCode(TipoVehiculo tv)
        {
            try
            {
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                //Nombre tabla depende de la variable EsSQL para asignarle el nombre
                query.Append(" UPDATE " + this.NombreTabla + @" SET ""Code"" = '" + tv.TipoVehiculoID.ToString() +
                    "', U_TVeiculoID = " + tv.TipoVehiculoID + ", U_Sincronizado = " + Sincronizado);

                if (UUID != null)
                    where.Append(string.Format(" and U_UUID like '%{0}%' ", UUID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar actualizar Code. " + ex.Message);
            }
        }

    }
}
