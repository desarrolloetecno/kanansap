﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SAPbobsCOM;
using Kanan.Vehiculos.BO2;

namespace KananSAP.Vehiculos.Data
{
    public class GestoriaVehiculoSAP
    {
        #region Atributos
        SAPbobsCOM.Company Company;
        public string ItemCode { get; set; }
        public UserTable oUDT { get; set; }
        public Gestoria gestoria { get; set; }

        Recordset Insertar;
        String Parametro;
        private String NombreTabla;
        public String NombreTablaGestoria;
        bool EsSQL;
        #endregion Atributos

        public bool ExistUDT()
        {
            return oUDT.GetByKey(this.ItemCode);
        }

        #region Constructor
        public GestoriaVehiculoSAP(SAPbobsCOM.Company company)
        {
            this.Company = company;
            this.gestoria = new Gestoria();

            this.Insertar = (Recordset)this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            this.EsSQL = Company.DbServerType.ToString().Contains("MSSQL") ? true : false;
        }
        #endregion Constructor

        #region Metodos

        #region Acciones
        public void Insert()
        {
            this.GetNameTable();

            String Fecha = String.Empty;
            String FechaRegistro = String.Empty;

            #region Fechas
            if (this.gestoria.Fecha != null)
                Fecha = this.gestoria.Fecha.Value.ToString("yyyy/MM/dd");
            if (this.gestoria.FechaRegistro != null)
                FechaRegistro = this.gestoria.FechaRegistro.Value.ToString("yyyy/MM/dd");
            #endregion Fechas

            this.Insertar.DoQuery(String.Format("select * from {0}", this.NombreTabla));
            this.gestoria.GestoriaID = this.Insertar.RecordCount + 1;

            #region SQL || Hana
            this.Parametro = String.Empty;

            this.objToUDT();
            oUDT.Add();
            //this.Parametro = String.Format("INSERT INTO {0} VALUES('{1}','{2}',{3},{4},{5},{6},'{7}','{8}','{9}','{10}',{11},'{12}',{13},'{14}','{15}','{16}')", this.NombreTabla, this.gestoria.GestoriaID, this.gestoria.GestoriaID, this.gestoria.GestoriaID, this.gestoria.VehiculoID, this.gestoria.EmpresaID, this.gestoria.SucursalID, Fecha, FechaRegistro, this.gestoria.Numero, this.gestoria.Observacion, this.gestoria.MotivoBajaID ?? 0, this.gestoria.Especifique ?? "", this.gestoria.DocID ?? 0, this.gestoria.Otro ?? "", this.gestoria.MotivoBaja ?? "", this.gestoria.rutaevidencia ?? "");
            //this.Insertar.DoQuery(this.Parametro);
            #endregion SQL || Hana
        }

        public void Update()
        {
            this.GetNameTable();
            String Fecha = String.Empty;
            String FechaRegistro = String.Empty;

            #region Fechas
            if (this.gestoria.Fecha != null)
                Fecha = this.gestoria.Fecha.Value.ToString("yyyy/MM/dd");
            if (this.gestoria.FechaRegistro != null)
                FechaRegistro = this.gestoria.FechaRegistro.Value.ToString("yyyy/MM/dd");
            #endregion Fechas

            #region SQL || Hana
            this.Parametro = String.Empty;
            /*this.Parametro = String.Format(@"UPDATE {0} SET ""Code"" = '{1}', ""Name"" = '{2}', U_GestoriaID = {3}, U_VehiculoID = {4}, U_EmpresaID = {5}, U_SucursalID = {6}, U_Fecha = '{7}', U_FechaRegistro = '{8}', U_Numero = '{9}', U_Observacion = '{10}', U_MotivoBajaID = {11}, U_Especifique = '{12}', U_DocID = {13}, U_Otro = '{14}', U_MotivoBaja = '{15}', U_Evidencia='{16}' WHERE ""Code"" = '{1}'", this.NombreTabla, this.gestoria.GestoriaID, this.gestoria.GestoriaID, this.gestoria.GestoriaID, this.gestoria.VehiculoID, this.gestoria.EmpresaID, this.gestoria.SucursalID, Fecha, FechaRegistro, this.gestoria.Numero, this.gestoria.Observacion, this.gestoria.MotivoBajaID, this.gestoria.Especifique, this.gestoria.DocID, this.gestoria.Otro, this.gestoria.MotivoBaja, this.gestoria.rutaevidencia);*/

            this.Parametro = String.Format(@"UPDATE {0} SET ""Code"" = '{1}', ""Name"" = '{2}', U_GestoriaID = {3}, U_VehiculoID = {4}, U_EmpresaID = {5}, U_SucursalID = {6}, U_Fecha = '{7}', U_FechaRegistro = '{8}', U_Numero = '{9}', U_Observacion = '{10}', U_MotivoBajaID = {11}, U_Especifique = '{12}', U_DocID = {13}, U_Otro = '{14}', U_MotivoBaja = '{15}' ", this.NombreTabla, this.gestoria.GestoriaID, this.gestoria.GestoriaID, this.gestoria.GestoriaID, this.gestoria.VehiculoID, this.gestoria.EmpresaID, this.gestoria.SucursalID, Fecha, FechaRegistro, this.gestoria.Numero, this.gestoria.Observacion, this.gestoria.MotivoBajaID, this.gestoria.Especifique, this.gestoria.DocID, this.gestoria.Otro, this.gestoria.MotivoBaja);

            if (!string.IsNullOrEmpty(this.gestoria.rutaevidencia))
                this.Parametro += String.Format(@" ,U_Evidencia = '{0}'", this.gestoria.rutaevidencia);

            this.Parametro += String.Format(@" WHERE ""Code"" = '{0}'", this.gestoria.GestoriaID);

            this.Insertar.DoQuery(this.Parametro);
            #endregion SQL || Hana
        }

        public void Delete()
        {
            try
            {
                string ID = String.Empty;

                #region ValidarID
                if (!String.IsNullOrEmpty(oUDT.Code))
                    ID = oUDT.Code;
                else
                {
                    throw new Exception("Erron al intentar obtener identificador. ");
                }
                #endregion ValidarID

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code"" = '{1}'", this.NombreTabla, ID);

                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana

                #region SQL
                /*var flag = oUDT.Remove();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    Company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }*/
                #endregion SQL
            }
            catch (Exception e)
            {
                throw new Exception("Error al intentar eliminar una incidencia. " + e.Message);
            }
        }
        #endregion Acciones

        #region Consultas
        public void GetEntityByID(int? Code, int TipoGestion)
        {
            this.gestoria.TipoGestion = TipoGestion;
            this.GetNameTable();
            this.Insertar.DoQuery(String.Format(@"select * from {0} where ""Code"" = '{1}'", this.NombreTabla, Code));

            this.gestoria = null;
            this.gestoria = new Gestoria();

            this.gestoria.GestoriaID = this.Insertar.Fields.Item("U_GestoriaID").Value ?? null;
            this.gestoria.VehiculoID = this.Insertar.Fields.Item("U_VehiculoID").Value ?? null;
            this.gestoria.EmpresaID = this.Insertar.Fields.Item("U_EmpresaID").Value ?? null;
            this.gestoria.SucursalID = this.Insertar.Fields.Item("U_SucursalID").Value ?? null;
            this.gestoria.Fecha = this.Insertar.Fields.Item("U_Fecha").Value ?? null;
            this.gestoria.FechaRegistro = this.Insertar.Fields.Item("U_FechaRegistro").Value ?? null;
            this.gestoria.Numero = this.Insertar.Fields.Item("U_Numero").Value ?? null;
            this.gestoria.Observacion = this.Insertar.Fields.Item("U_Observacion").Value ?? null;
            this.gestoria.MotivoBajaID = this.Insertar.Fields.Item("U_MotivoBajaID").Value ?? null;
            this.gestoria.MotivoBaja = this.Insertar.Fields.Item("U_MotivoBaja").Value ?? null;
            this.gestoria.Especifique = this.Insertar.Fields.Item("U_Especifique").Value ?? null;
            this.gestoria.DocID = this.Insertar.Fields.Item("U_DocID").Value ?? null;
            this.gestoria.Otro = this.Insertar.Fields.Item("U_Otro").Value ?? null;
            this.gestoria.rutaevidencia = this.Insertar.Fields.Item("U_Evidencia").Value ?? null;
        }
        #endregion Consultas

        #region Auxiliares
        public void GetNameTable()
        {
            if (this.gestoria.TipoGestion == 1)
            {
                this.NombreTabla = EsSQL ? "[@VSKF_GESPLA]" : @"""@VSKF_GESPLA""";
                this.NombreTablaGestoria = this.NombreTabla;
                this.oUDT = Company.UserTables.Item("VSKF_GESPLA");
            }

            if (this.gestoria.TipoGestion == 2)
            {
                this.NombreTabla = EsSQL ? "[@VSKF_GESPRO]" : @"""@VSKF_GESPRO""";
                this.NombreTablaGestoria = this.NombreTabla;
                this.oUDT = Company.UserTables.Item("VSKF_GESPRO");
            }

            if (this.gestoria.TipoGestion == 3)
            {
                this.NombreTabla = EsSQL ? "[@VSKF_GESTAR]" : @"""@VSKF_GESTAR""";
                this.NombreTablaGestoria = this.NombreTabla;
                this.oUDT = Company.UserTables.Item("VSKF_GESTAR");
            }

            if (this.gestoria.TipoGestion == 4)
            {
                this.NombreTabla = EsSQL ? "[@VSKF_GESALTA]" : @"""@VSKF_GESALTA""";
                this.NombreTablaGestoria = this.NombreTabla;
                this.oUDT = Company.UserTables.Item("VSKF_GESALTA");
            }

            if (this.gestoria.TipoGestion == 5)
            {
                this.NombreTabla = EsSQL ? "[@VSKF_GESBAJA]" : @"""@VSKF_GESBAJA""";
                this.NombreTablaGestoria = this.NombreTabla;
                this.oUDT = Company.UserTables.Item("VSKF_GESBAJA");
            }
        }

        public void objToUDT()
        {
           
                oUDT.Code = this.gestoria.GestoriaID.ToString();
                oUDT.Name = this.gestoria.GestoriaID.ToString();
                this.oUDT.UserFields.Fields.Item("U_GESTORIAID").Value = this.gestoria.GestoriaID;
                this.oUDT.UserFields.Fields.Item("U_VEHICULOID").Value = this.gestoria.VehiculoID;
                this.oUDT.UserFields.Fields.Item("U_EMPRESAID").Value = this.gestoria.EmpresaID;
                this.oUDT.UserFields.Fields.Item("U_SUCURSALID").Value = this.gestoria.SucursalID;
                this.oUDT.UserFields.Fields.Item("U_FECHA").Value = this.gestoria.Fecha;
                this.oUDT.UserFields.Fields.Item("U_FECHAREGISTRO").Value = this.gestoria.FechaRegistro;
                this.oUDT.UserFields.Fields.Item("U_NUMERO").Value = this.gestoria.Numero;
                this.oUDT.UserFields.Fields.Item("U_OBSERVACION").Value = this.gestoria.Observacion;
                this.oUDT.UserFields.Fields.Item("U_MOTIVOBAJAID").Value = this.gestoria.MotivoBajaID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_ESPECIFIQUE").Value = this.gestoria.Especifique ?? "";
                this.oUDT.UserFields.Fields.Item("U_DOCID").Value = this.gestoria.DocID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_OTRO").Value = this.gestoria.Otro ?? "";
                this.oUDT.UserFields.Fields.Item("U_MOTIVOBAJA").Value = this.gestoria.MotivoBaja ?? "";
                this.oUDT.UserFields.Fields.Item("U_EVIDENCIA").Value = this.gestoria.rutaevidencia ?? "";
            
        }
        #endregion Auxiliares

        #endregion Metodos
    }
}
