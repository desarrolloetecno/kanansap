﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kanan.Comun.BO2;
using Kanan.Costos.BO2;
using Kanan.Vehiculos.BO2;
using SAPbobsCOM;

namespace KananSAP.Vehiculos.Data
{
    public class CargaCombustibleSAP
    {
        #region Propiedades
        private const string TABLE_SERVICE_NAME = "VSKF_CRGCOMBUSTIBLE";
        public CargaCombustible carga { get; set; }
        public UserTable oUDT { get; set; }
        public Items oItem { get; set; }
        public string ItemCode { get; set; }
        private Company Company;

        //Migracion Hana
        Recordset Insertar;
        String Parametro;
        private String NombreTabla;
        bool EsSQL;

        //Se agregó para ser visible desde ServicioSAP.cs en KananSAP.Mantenimiento.Data.
        public String NombreTablaCargaCombustible = String.Empty;

        public bool ExistUDT()
        {
            return oUDT.GetByKey(this.ItemCode);
        }
        #endregion

        #region Constructor
        public CargaCombustibleSAP(SAPbobsCOM.Company company)
        {
            this.Company = company;
            this.carga = new CargaCombustible
            {
                Movil = new Vehiculo(),
                SourceCenter = new Proveedor(),
                TipoPago = new TipoDePago(),
                Longitud = new UnidadLongitud(),
                Volumen = new UnidadVolumen(),
                TypeFuel = new TipoCombustible(),
                Moneda = new TipoMoneda(),
                ImagenRecarga = new Imagen(),
                ListImagenesRecarga = new List<ImagenCargaCombustible>()
            };
            this.oItem = (SAPbobsCOM.Items)company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
            try
            {
                this.oUDT = company.UserTables.Item(TABLE_SERVICE_NAME);

                //Migración HANA
                this.Insertar = (Recordset)this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.EsSQL = Company.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.NombreTabla = EsSQL ? "[@VSKF_CRGCOMBUSTIBLE]" : @"""@VSKF_CRGCOMBUSTIBLE""";

                //Se maneja en una variable pública para poder utilizar el método "Consultar" en otras clases.
                this.NombreTablaCargaCombustible = this.NombreTabla;
            }
            catch (Exception)
            {
                this.oUDT = null;
            }
        } 
        #endregion

        #region Metodos
        #region Metodos Interface

        public void CargaToSAP()
        {
            CargaToUDT();
        }

        private void CargaToUDT()
        {
            oUDT.Code = this.ItemCode;
            oUDT.Name = this.carga.Ticket;
            this.oUDT.UserFields.Fields.Item("U_Litros").Value = this.carga.Litros?? 0;
            this.oUDT.UserFields.Fields.Item("U_Pesos").Value = Convert.ToDouble(this.carga.Pesos);
            this.oUDT.UserFields.Fields.Item("U_VehiculoID").Value = this.carga.Movil.VehiculoID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_Fecha").Value = this.carga.Fecha ?? new DateTime();
            this.oUDT.UserFields.Fields.Item("U_FechaCaptura").Value = this.carga.FechaCaptura ?? new DateTime();
            this.oUDT.UserFields.Fields.Item("U_Ticket").Value = this.carga.Ticket ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_TipoCombustible").Value =  this.carga.TypeFuel.TipoCombustibleID;
            this.oUDT.UserFields.Fields.Item("U_ProveedorID").Value = this.carga.SourceCenter.ProveedorID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_Odometro").Value = this.carga.Odometro ?? 0;
            this.oUDT.UserFields.Fields.Item("U_FormaPago").Value = this.carga.TipoPago.TipoPagoID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_DetallePago").Value = this.carga.TipoPago.DetallePago ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_CentroServicio").Value = this.carga.CentroServicio ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_ULongitudID").Value = this.carga.Longitud.UnidadLongitudID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_UVolumenID").Value = (int)(this.carga.Volumen.UnidadVolumenID ?? 0);
            this.oUDT.UserFields.Fields.Item("U_UUID").Value = this.carga.UID ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_TipoMonedaID").Value = this.carga.Moneda.TipoMonedaID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_Latitude").Value = this.carga.Latitude ?? 0;
            this.oUDT.UserFields.Fields.Item("U_Longitude").Value = this.carga.Longitude ?? 0;
            this.oUDT.UserFields.Fields.Item("U_Facturable").Value = this.carga.EsFacturableSAP ?? 0;
        }

        public bool SAPToCarga(string ItemCode)
        {
            if (!this.oUDT.GetByKey(ItemCode)) return false;
            UDTToCarga();
            return true;
        }

        public bool SAPToCarga()
        {
            UDTToCarga();
            return true;
        }

        private void UDTToCarga()
        {
            this.carga = new CargaCombustible
            {
                Movil = new Vehiculo(),
                SourceCenter = new Proveedor(),
                TipoPago = new TipoDePago(),
                Longitud = new UnidadLongitud(),
                Volumen = new UnidadVolumen(),
                TypeFuel = new TipoCombustible(),
                Moneda = new TipoMoneda(),
                ImagenRecarga = new Imagen(),
                ListImagenesRecarga = new List<ImagenCargaCombustible>()
            };

            this.carga.CargaCombustibleID = Convert.ToInt32(this.oUDT.Code) == 0? 0 : Convert.ToInt32(this.oUDT.Code);
            this.carga.Litros = (double?) this.oUDT.UserFields.Fields.Item("U_Litros").Value ?? 0;
            this.carga.Pesos = this.oUDT.UserFields.Fields.Item("U_Pesos").Value == null ? 0 : (decimal)this.oUDT.UserFields.Fields.Item("U_Pesos").Value;
            this.carga.Movil.VehiculoID = (int)this.oUDT.UserFields.Fields.Item("U_VehiculoID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_VehiculoID").Value;
            this.carga.Fecha = this.oUDT.UserFields.Fields.Item("U_Fecha").Value == null ? new DateTime() : (DateTime)this.oUDT.UserFields.Fields.Item("U_Fecha").Value;
            this.carga.FechaCaptura = this.oUDT.UserFields.Fields.Item("U_FechaCaptura").Value == null ? new DateTime() : (DateTime)this.oUDT.UserFields.Fields.Item("U_FechaCaptura").Value;
            this.carga.Ticket = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Ticket").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Ticket").Value;
            //this.carga.TypeFuel = this.oUDT.UserFields.Fields.Item("U_TipoCombustible").Value == null ? new TipoCombustible() : (TipoCombustible)this.oUDT.UserFields.Fields.Item("U_TipoCombustible").Value;
             this.carga.TypeFuel.TipoCombustibleID = this.oUDT.UserFields.Fields.Item("U_TipoCombustible").Value == null ? 1: (int?)this.oUDT.UserFields.Fields.Item("U_TipoCombustible").Value;
            this.carga.SourceCenter.ProveedorID = (int)this.oUDT.UserFields.Fields.Item("U_ProveedorID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_ProveedorID").Value;
            this.carga.Odometro = this.oUDT.UserFields.Fields.Item("U_Odometro").Value == null ? null : (double?)this.oUDT.UserFields.Fields.Item("U_Odometro").Value;
            this.carga.TipoPago.TipoPagoID = (int)this.oUDT.UserFields.Fields.Item("U_FormaPago").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_FormaPago").Value;
            this.carga.DetallePago = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_DetallePago").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_DetallePago").Value;
            this.carga.CentroServicio = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_CentroServicio").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_CentroServicio").Value;
            this.carga.Longitud.UnidadLongitudID = Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_ULongitudID").Value) == 0 ? null : Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_ULongitudID").Value);
            this.carga.Volumen.UnidadVolumenID = Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_UVolumenID").Value) == 0 ? null : Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_UVolumenID").Value);
            this.carga.UID = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_UUID").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_UUID").Value;
            this.carga.Moneda.TipoMonedaID = (int)this.oUDT.UserFields.Fields.Item("U_TipoMonedaID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_TipoMonedaID").Value; ;
            this.carga.Latitude = this.oUDT.UserFields.Fields.Item("U_Latitude").Value == null ? null : Convert.ToDouble(this.oUDT.UserFields.Fields.Item("U_Latitude").Value);
            this.carga.Longitude = this.oUDT.UserFields.Fields.Item("U_Longitude").Value == null ? null : Convert.ToDouble(this.oUDT.UserFields.Fields.Item("U_Longitude").Value);
            this.carga.EsFacturableSAP = this.oUDT.UserFields.Fields.Item("U_Facturable").Value == null ? null : Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_Facturable").Value);
        }
        #endregion

        #region Acciones
        public void Insert()
        {
            try
            {
                #region Fechas
                String Hora = String.Empty;
                String Minuto = String.Empty;
                String HoraMinuto = String.Empty;

                String Fecha = String.Empty;
                String FechaCaptura = String.Empty;

                Fecha = this.carga.Fecha.Value.ToString("yyyy-MM-dd");
                FechaCaptura = this.carga.FechaCaptura.Value.ToString("yyyy-MM-dd");

                Hora = this.carga.Fecha.Value.Hour.ToString().Length < 2 ? "0" + this.carga.Fecha.Value.Hour.ToString() : this.carga.Fecha.Value.Hour.ToString();

                Minuto = this.carga.Fecha.Value.Minute.ToString().Length < 2 ? "0" + this.carga.Fecha.Value.Minute.ToString() : this.carga.Fecha.Value.Minute.ToString();

                if (!String.IsNullOrEmpty(Hora) & !String.IsNullOrEmpty(Minuto))
                    HoraMinuto = Hora + ":" + Minuto;
                else
                    HoraMinuto = "00:00";
                #endregion Fechas

                //Int32 TipoCombustible = Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_TipoCombustible").Value);

                //String Latitude = this.carga.Latitude == null ? "NULL" : this.carga.Latitude.Value.ToString();
                //String Longitude = this.carga.Longitude == null ? "NULL" : this.carga.Longitude.Value.ToString();

                #region SQL || Hana
                //this.Parametro = String.Empty;
                //this.Parametro = String.Format("INSERT INTO {0} VALUES('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', '{19}', {20}, {21})", this.NombreTabla, this.carga.CargaCombustibleID, this.carga.CargaCombustibleID, this.carga.Litros, this.carga.Pesos, this.carga.Movil.VehiculoID, Fecha, HoraMinuto, FechaCaptura, this.carga.Ticket, TipoCombustible, this.carga.SourceCenter.ProveedorID, this.carga.Odometro, this.carga.TipoPago.TipoPagoID, this.carga.TipoPago.DetallePago, this.carga.CentroServicio, this.carga.Longitud.UnidadLongitudID, this.carga.Volumen.UnidadVolumenID, this.carga.UID, this.carga.Moneda.TipoMonedaID, Latitude, Longitude);
                //this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana

                #region SQL
                int flag = oUDT.Add();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    Company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL
            }
            catch
            {
               
            }
        }

        public void Update()
        {
            String Hora = String.Empty;
            String Minuto = String.Empty;
            String HoraMinuto = String.Empty;

            Hora = this.carga.Fecha.Value.Hour.ToString().Length < 2 ? "0" + this.carga.Fecha.Value.Hour.ToString() : this.carga.Fecha.Value.Hour.ToString();

            Minuto = this.carga.Fecha.Value.Minute.ToString().Length < 2 ? "0" + this.carga.Fecha.Value.Minute.ToString() : this.carga.Fecha.Value.Minute.ToString();

            if (!String.IsNullOrEmpty(Hora) & !String.IsNullOrEmpty(Minuto))
                HoraMinuto = Hora + ":" + Minuto + ":" + "00.000";
            else
                HoraMinuto = "00:00:00.000";

            #region SQL || Hana
            this.Parametro = String.Empty;
            this.Parametro = String.Format(@"UPDATE {0} SET ""Code"" = '{1}', ""Name"" = '{2}', U_Litros = '{3}', U_Pesos = '{4}', U_VehiculoID = '{5}', U_Fecha = '{6}', U_HoraMinuto = '{7}', U_FechaCaptura = '{8}', U_Ticket = '{9}', U_TipoCombustible = '{10}', U_ProveedorID = '{11}', U_Odometro = '{12}', U_FormaPago = '{13}', U_DetallePago = '{14}', U_CentroServicio = '{15}', U_ULongitudID = '{16}', U_UVolumenID = '{17}', U_TipoMonedaID = '{18}', U_Latitude = '{19}', U_Longitude = '{20}' WHERE Code = '{1}'", this.NombreTabla, this.carga.CargaCombustibleID, this.carga.CargaCombustibleID, this.carga.Litros, this.carga.Pesos, this.carga.Movil.VehiculoID, this.carga.Fecha, HoraMinuto, this.carga.FechaCaptura, this.carga.Ticket, this.carga.TypeFuel, this.carga.SourceCenter.ProveedorID, this.carga.Odometro, this.carga.TipoPago.TipoPagoID, this.carga.TipoPago.DetallePago, this.carga.CentroServicio, this.carga.Longitud.UnidadLongitudID, this.carga.Volumen.UnidadVolumenID, this.carga.Moneda.TipoMonedaID, this.carga.Latitude, this.carga.Longitude);
            this.Insertar.DoQuery(this.Parametro);
            #endregion SQL || Hana

            #region SQL
            /*var flag = oUDT.Update();
            if (flag != 0)
            {
                int errornum;
                string errormsj;
                Company.GetLastError(out errornum, out errormsj);
                throw new Exception("Error: " + errornum + " - " + errormsj);
            }*/
            #endregion SQL
        }

        public void Delete()
        {
            string ID = !String.IsNullOrEmpty(oUDT.Code) ? 
                ID = oUDT.Code : String.Empty;
            if (String.IsNullOrEmpty(ID))
                throw new Exception("Erron al intentar obtener identificador. ");

            #region SQL || Hana
            this.Parametro = String.Empty;
            this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code"" = '{1}'", this.NombreTabla, ID);
            this.Insertar.DoQuery(this.Parametro);
            #endregion SQL || Hana

            #region SQL
            /*var flag = oUDT.Remove();
            if (flag != 0)
            {
                int errornum;
                string errormsj;
                Company.GetLastError(out errornum, out errormsj);
                throw new Exception("Error: " + errornum + " - " + errormsj);
            }*/
            #endregion SQL
        }

        #endregion

        #region Services
        public Recordset Consultar(CargaCombustible cc)
        {
            try
            {
                Recordset rs = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                if (cc == null) cc = new CargaCombustible();
                if (cc.Movil == null) cc.Movil = new Vehiculo();
                if (cc.SourceCenter == null) cc.SourceCenter = new Proveedor();
                if (cc.TipoPago == null) cc.TipoPago = new TipoDePago();
                if (cc.Longitud == null) cc.Longitud = new UnidadLongitud();
                if (cc.Volumen == null) cc.Volumen = new UnidadVolumen();
                if (cc.Moneda == null) cc.Moneda = new TipoMoneda();
                if (cc.ImagenRecarga == null) cc.ImagenRecarga = new Imagen();

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(" SELECT * from " + this.NombreTabla + " as cc ");

                if (carga.GetType() == typeof (CargaCombustibleFilter))
                {
                    CargaCombustibleFilter x = (CargaCombustibleFilter) carga;
                    if (carga.Fecha != null && x.FechaFin != null)
                    {
                        where.Append(string.Format("and cc.U_Fecha BETWEEN {0} and {1}", x.Fecha, x.FechaFin));
                    }
                    else if (carga.Fecha != null && x.FechaFin == null)
                    {
                        where.Append(string.Format(" and cc.U_Fecha >= {0} ", x.Fecha)); ;
                    }
                    else if (carga.Fecha == null && x.FechaFin != null)
                    {
                        where.Append(string.Format(" and cc.U_Fecha <= {0} ", x.FechaFin));
                    }
                }
                else
                {
                    if (cc.Fecha != null)
                        where.Append(string.Format(" and cc.U_Fecha = {0} ", cc.Fecha));
                }

                if (cc.CargaCombustibleID != null && cc.CargaCombustibleID > 0)
                    where.Append(string.Format(@" and cc.""Code"" = {0} ", cc.CargaCombustibleID));
                if (cc.Litros != null)
                    where.Append(string.Format(" and cc.U_Litros = {0} ", cc.Litros));
                if (cc.Pesos > 0)
                    where.Append(string.Format(" and cc.U_Pesos = {0} ", cc.Pesos));
                if (cc.Movil.VehiculoID != null)
                    where.Append(string.Format(" and cc.U_VehiculoID = {0} ", cc.Movil.VehiculoID));
                if (cc.FechaCaptura != null)
                    where.Append(string.Format(" and cc.U_FechaCaptura = {0} ", cc.FechaCaptura));
                if (cc.Ticket != null)
                    where.Append(string.Format(" and cc.U_Ticket = '{0}' ", cc.Ticket));
                if ((int)cc.TypeFuel.TipoCombustibleID > 0)
                    where.Append(string.Format(" and cc.U_TipoCombustible = {0} ", (int)cc.TypeFuel.TipoCombustibleID));
                if (cc.SourceCenter.ProveedorID != null)
                    where.Append(string.Format(" and cc.U_ProveedorID = {0} ", cc.SourceCenter.ProveedorID));
                if (cc.Odometro != null)
                    where.Append(string.Format(" and cc.U_Odometro = {0} ", cc.Odometro));
                if (cc.TipoPago.TipoPagoID != null)
                    where.Append(string.Format(" and cc.U_FormaPago = {0} ", cc.TipoPago.TipoPagoID));
                if (cc.DetallePago != null)
                    where.Append(string.Format(" and cc.U_DetallePago = '{0}' ", cc.DetallePago));
                if (cc.CentroServicio != null)
                    where.Append(string.Format(" and cc.U_CentroServicio = '{0}' ", cc.CentroServicio));
                if (cc.Longitud.UnidadLongitudID != null)
                    where.Append(string.Format(" and cc.U_ULongitudID = {0} ", cc.Longitud.UnidadLongitudID));
                if (cc.Volumen.UnidadVolumenID != null)
                    where.Append(string.Format(" and cc.U_UVolumenID = {0} ", cc.Volumen.UnidadVolumenID));
                if (cc.UID != null)
                    where.Append(string.Format(" and cc.U_UUID = '{0}' ", cc.UID));
                if (cc.Moneda.TipoMonedaID != null)
                    where.Append(string.Format(" and cc.U_TipoMonedaID = {0} ", cc.Moneda.TipoMonedaID));
                if (cc.Latitude != null && cc.Latitude > 0)
                    where.Append(string.Format(" and cc.U_Latitude = {0} ", cc.Latitude));
                if (cc.Longitude != null && cc.Longitude > 0)
                    where.Append(string.Format(" and cc.U_Longitude = {0} ", cc.Longitud));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                query.Append(@" order by cc.""Code"" desc ");
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<CargaCombustible> RecordSetToListCargaCombustible(Recordset rs)
        {
            var result = new List<CargaCombustible>();
            for (int i = 0; i < rs.RecordCount; i++)
            {
                this.ItemCode = rs.Fields.Item("Code").Value;
                this.oUDT.GetByKey(this.ItemCode);
                UDTToCarga();
                var temp = (CargaCombustible)this.carga.Clone();
                result.Add(temp);
                rs.MoveNext();
            }
            return result;
        }

        public List<CargaCombustible> GetByVehiculoID(int id)
        {
            try
            {
                var result = new List<CargaCombustible>();
                Recordset rs = (Recordset)this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(@"select * from " + this.NombreTabla + " where U_VehiculoID = " + id);
                for (int i = 0; i < rs.RecordCount; i++)
                {
                    this.ItemCode = rs.Fields.Item("Code").Value;
                    this.oUDT.GetByKey(this.ItemCode);
                    UDTToCarga();
                    var temp = (CargaCombustible)this.carga.Clone();
                    result.Add(temp);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        } 
        #endregion
        #endregion
    }
}
