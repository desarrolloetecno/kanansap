﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kanan.Comun.BO2;
using Kanan.Costos.BO2;
using Kanan.Vehiculos.BO2;
using SAPbobsCOM;

namespace KananSAP.Vehiculos.Data
{
    public class InciVehiculoSAP
    {
        #region Propiedades Incidencia en Vehiculo
        private const string TABLE_SERVICE_NAME = "VSKF_INVEHICULO";
        public IncidenciaVehiculox IncidenciaEnVehiculo { get; set; }
        public UserTable oUDT { get; set; }
        //public Items oItem { get; set; }
        public string ItemCode { get; set; }
        private Company Company;
        public string Filtro;

        //Migracion Hana
        Recordset Insertar;
        String Parametro;
        private String NombreTabla;
        bool EsSQL;

        //Se agregó para ser visible desde ServicioSAP.cs en KananSAP.Mantenimiento.Data.
        public String NombreTablaIncidenciaVehiculo = String.Empty;

        public bool ExistUDT()
        {
            return oUDT.GetByKey(this.ItemCode);
        }
        #endregion

        #region Constructor
        public InciVehiculoSAP(SAPbobsCOM.Company company)
        {
            this.Company = company;
            this.IncidenciaEnVehiculo = new IncidenciaVehiculox
            {
                Vehiculo = new Vehiculo(),
                ImagenesIncidenciaAntes = new Imagen(),
                ImagenesIncidenciaDespues = new Imagen()
                //ListImagenesIncidenciaVehiculo = new List<ImagenIncidenciaVehiculo>()
            };
            //this.oItem = (SAPbobsCOM.Items)company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
            try
            {
                this.oUDT = company.UserTables.Item(TABLE_SERVICE_NAME);

                //Migración HANA
                this.Insertar = (Recordset)this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.EsSQL = Company.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.NombreTabla = EsSQL ? "[@VSKF_INVEHICULO]" : @"""@VSKF_INVEHICULO""";
                //Se maneja en una variable pública para poder utilizar el método "Consultar" en ServicioSAP.cs
                // en KananSAP.Mantenimiento.Data.
                this.NombreTablaIncidenciaVehiculo = this.NombreTabla;
            }
            catch (Exception)
            {
                this.oUDT = null;
            }
        } 
        #endregion

        #region Metodos

        #region Metodos Interface

        public void IncidenciaVehiculoToSAP()
        {
            IncidenciaVehiculoToUDT();
        }

        private void IncidenciaVehiculoToUDT()
        {
            oUDT.Code = this.ItemCode;
            oUDT.Name = Convert.ToString(this.IncidenciaEnVehiculo.IncidenciaID);
            this.oUDT.UserFields.Fields.Item("U_IncidenciaID").Value = this.IncidenciaEnVehiculo.IncidenciaID;
            this.oUDT.UserFields.Fields.Item("U_VehiculoID").Value = this.IncidenciaEnVehiculo.Vehiculo.VehiculoID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_FechaIncidencia").Value = this.IncidenciaEnVehiculo.FechaIncidencia ?? new DateTime();
            this.oUDT.UserFields.Fields.Item("U_Descripcion").Value = Convert.ToString(this.IncidenciaEnVehiculo.Descripcion) ?? string.Empty;
            //this.oUDT.UserFields.Fields.Item("U_ImgInciAntID").Value = this.IncidenciaEnVehiculo.ImagenesIncidenciaAntes ?? 0;
            //this.oUDT.UserFields.Fields.Item("U_ImgInciDesID").Value = this.IncidenciaEnVehiculo.ImagenesIncidenciaDespues ?? 0;
            this.oUDT.UserFields.Fields.Item("U_Latitude").Value = this.IncidenciaEnVehiculo.Latitude ?? 0;
            this.oUDT.UserFields.Fields.Item("U_Longitude").Value = this.IncidenciaEnVehiculo.Longitude ?? 0;
            this.oUDT.UserFields.Fields.Item("U_CreaOS").Value = this.IncidenciaEnVehiculo.CreaOrdenServicio ?? 0;
        }

        //Insertar incidencias filtradas por fechas en la Tabla de Usuario R. Santos
        public bool LlenarGridFiltroFechas(List<IncidenciaVehiculox> Filtro, int numLista)
        {
            int tempLista = numLista - 1;
            for (int i = 0; i <= tempLista; i++)
            {
                oUDT.Code = this.ItemCode;
                oUDT.Name = Convert.ToString(Filtro[i].IncidenciaID);
                this.oUDT.UserFields.Fields.Item("U_VehiculoID").Value = Filtro[i].Vehiculo.VehiculoID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_FechaIncidencia").Value = Filtro[i].FechaIncidencia ?? new DateTime();
                this.oUDT.UserFields.Fields.Item("U_Descripcion").Value = Convert.ToString(Filtro[i].Descripcion) ?? string.Empty;
                //this.oUDT.UserFields.Fields.Item("U_ImgInciAntID").Value = this.IncidenciaEnVehiculo.ImagenesIncidenciaAntes ?? 0;
                //this.oUDT.UserFields.Fields.Item("U_ImgInciDesID").Value = this.IncidenciaEnVehiculo.ImagenesIncidenciaDespues ?? 0;
                this.oUDT.UserFields.Fields.Item("U_Latitude").Value = Filtro[i].Latitude ?? 0;
                this.oUDT.UserFields.Fields.Item("U_Longitude").Value = Filtro[i].Longitude ?? 0;
            }
            
            return true;
        }

        public bool SAPToIncidenciaVehiculo(string ItemCode)
        {
            if (!this.oUDT.GetByKey(ItemCode)) return false;
            UDTToIncidenciaVehiculo();
            return true;
        }

        public bool SAPToIncidenciaVehiculo()
        {
            UDTToIncidenciaVehiculo();
            return true;
        }

        private void UDTToIncidenciaVehiculo()
        {
            this.IncidenciaEnVehiculo = new IncidenciaVehiculox
            {
                Vehiculo = new Vehiculo(),
                ImagenesIncidenciaAntes = new Imagen(),
                ImagenesIncidenciaDespues = new Imagen(),
                //ListImagenesIncidenciaVehiculo = new List<ImagenIncidenciaVehiculo>()
            };

            this.IncidenciaEnVehiculo.IncidenciaID = Convert.ToInt32(this.oUDT.Code) ==0? 0 : Convert.ToInt32(this.oUDT.Code);            
            this.IncidenciaEnVehiculo.Vehiculo.VehiculoID = (int?)this.oUDT.UserFields.Fields.Item("U_VehiculoID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_VehiculoID").Value;
            this.IncidenciaEnVehiculo.FechaIncidencia = this.oUDT.UserFields.Fields.Item("U_FechaIncidencia").Value == null? new DateTime() : (DateTime)this.oUDT.UserFields.Fields.Item("U_FechaIncidencia").Value;
            this.IncidenciaEnVehiculo.Descripcion = string.IsNullOrEmpty((string) this.oUDT.UserFields.Fields.Item("U_Descripcion").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Descripcion").Value;
            //this.IncidenciaEnVehiculo.ImagenesIncidenciaAntes = (int)this.oUDT.UserFields.Fields.Item("U_ImgInciAntID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_ImgInciAntID").Value;
            //this.IncidenciaEnVehiculo.ImagenesIncidenciaAntes = (int)this.oUDT.UserFields.Fields.Item("U_ImgInciDesID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_ImgInciDesID").Value;
            this.IncidenciaEnVehiculo.Latitude = this.oUDT.UserFields.Fields.Item("U_Latitude").Value == null ? null : Convert.ToDouble(this.oUDT.UserFields.Fields.Item("U_Latitude").Value);
            this.IncidenciaEnVehiculo.Longitude = this.oUDT.UserFields.Fields.Item("U_Longitude").Value == null ? null : Convert.ToDouble(this.oUDT.UserFields.Fields.Item("U_Longitude").Value);
            this.IncidenciaEnVehiculo.CreaOrdenServicio = Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_CreaOS").Value) ?? 0;
        }
        #endregion

        #region Acciones

        public void Insert()
        {
            String hh = IncidenciaEnVehiculo.FechaIncidencia.Value.Hour.ToString().Length < 2 ? "0" + IncidenciaEnVehiculo.FechaIncidencia.Value.Hour.ToString() : IncidenciaEnVehiculo.FechaIncidencia.Value.Hour.ToString();

            String mm = IncidenciaEnVehiculo.FechaIncidencia.Value.Minute.ToString().Length < 2 ? "0" + IncidenciaEnVehiculo.FechaIncidencia.Value.Minute.ToString() : IncidenciaEnVehiculo.FechaIncidencia.Value.Minute.ToString();

            String FechaIncidencia = this.IncidenciaEnVehiculo.FechaIncidencia.Value.ToString("yyyy/MM/dd") + " " + hh + ":" + mm + ":00";

            String nombreTabla;
            try
            {
                nombreTabla = NombreTabla;
                Parametro = String.Format("INSERT INTO {0} VALUES('{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}',{9})", nombreTabla, this.IncidenciaEnVehiculo.IncidenciaID, this.IncidenciaEnVehiculo.IncidenciaID, this.IncidenciaEnVehiculo.IncidenciaID, this.IncidenciaEnVehiculo.Vehiculo.VehiculoID, FechaIncidencia, this.IncidenciaEnVehiculo.Descripcion, "0.0", "0.0", this.IncidenciaEnVehiculo.CreaOrdenServicio ?? 0);
                Insertar.DoQuery(Parametro);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar insertar una incidencia. " + ex.Message);
            }
        }

        public void Update()
        {
            try
            {
                String hh = IncidenciaEnVehiculo.FechaIncidencia.Value.Hour.ToString().Length < 2 ? "0" + IncidenciaEnVehiculo.FechaIncidencia.Value.Hour.ToString() : IncidenciaEnVehiculo.FechaIncidencia.Value.Hour.ToString();

                String mm = IncidenciaEnVehiculo.FechaIncidencia.Value.Minute.ToString().Length < 2 ? "0" + IncidenciaEnVehiculo.FechaIncidencia.Value.Minute.ToString() : IncidenciaEnVehiculo.FechaIncidencia.Value.Minute.ToString();

                String FechaIncidencia = this.IncidenciaEnVehiculo.FechaIncidencia.Value.ToString("yyyy/MM/dd") + " " + hh + ":" + mm + ":00";

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET U_IncidenciaID = '{3}', U_VehiculoID = '{4}', U_FechaIncidencia = '{5}', U_Descripcion = '{6}', U_Latitude = '{7}', U_Longitude = '{8}', U_CreaOS = {9} WHERE ""Code"" = '{1}'", this.NombreTabla, this.IncidenciaEnVehiculo.IncidenciaID, this.IncidenciaEnVehiculo.IncidenciaID, this.IncidenciaEnVehiculo.IncidenciaID, this.IncidenciaEnVehiculo.Vehiculo.VehiculoID, FechaIncidencia, this.IncidenciaEnVehiculo.Descripcion, this.IncidenciaEnVehiculo.Latitude, this.IncidenciaEnVehiculo.Longitude, this.IncidenciaEnVehiculo.CreaOrdenServicio ?? 0);

                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana

                #region SQL
                /*var flag = oUDT.Update();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    Company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }*/
                #endregion SQL

            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar actualizar una incidencia. " + ex.Message);
            }
        }

        public void Delete()
        {
            try
            {
                string ID = String.Empty;
                if (!String.IsNullOrEmpty(oUDT.Code))
                    ID = oUDT.Code;
                else
                {
                    throw new Exception("Erron al intentar obtener identificador. ");
                }

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code"" = '{1}'", this.NombreTabla, ID);

                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana

                #region SQL
                /*var flag = oUDT.Remove();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    Company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }*/
                #endregion SQL

            }

            catch (Exception ex)
            {
                throw new Exception("Error al intentar eliminar una incidencia. " + ex.Message);
            }
        }
        #endregion

        #region Services

        public Recordset Consultar(IncidenciaVehiculox cc, DateTime fechaInicio, DateTime fechaFin)
        {
            
            try
            {
                Recordset rs = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                if (cc == null) cc = new IncidenciaVehiculox();
                if (cc.Vehiculo == null) cc.Vehiculo = new Vehiculo();

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();

                query.Append("SELECT * FROM " + this.NombreTabla + " ");
                if (fechaInicio != null && fechaFin != null)
                    //Las fechas en DoQuery se utiliza de la siguiente manera: 2017-12-31
                    //Lo cual indica la fecha 31 de diciembre del 2017 R. Santos
                { where.Append(string.Format(" and U_FechaIncidencia between '{0}' and '{1}'", fechaInicio.Year + "-" + fechaInicio.Month
                    + "-" + fechaInicio.Day + " 00:00:00", fechaFin.Year + "-" + fechaFin.Month + "-" + fechaFin.Day + " 23:59:59")); }
                /*if (cc.IncidenciaID != null && cc.IncidenciaID > 0)
                { where.Append(string.Format(" and cc.Code = {0} ", cc.IncidenciaID)); }
                if (cc.Vehiculo.VehiculoID != null)
                { where.Append(string.Format(" and cc.U_VehiculoID = {0} ", cc.Vehiculo.VehiculoID)); }
                if (cc.FechaIncidencia != null)
                { where.Append(string.Format(" and cc.U_FechaIncidencia = {0} ", cc.FechaIncidencia)); }*/

                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" WHERE " + filtro);
                }
                query.Append(@" ORDER BY ""Code"" ASC");
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<IncidenciaVehiculox> RecordSetToListIncidenciaVehiculo(Recordset rs)
        {
            var result = new List<IncidenciaVehiculox>();
            for (int i = 0; i < rs.RecordCount; i++)
            {
                this.ItemCode = rs.Fields.Item("Code").Value;
                this.oUDT.GetByKey(this.ItemCode);
                UDTToIncidenciaVehiculo();
                var temp = (IncidenciaVehiculox)this.IncidenciaEnVehiculo.Clone();
                result.Add(temp);
                rs.MoveNext();
            }
            return result;
        }

        public Recordset GetByVehiculoID(int? id)
        {
            try
            {
                var result = new List<IncidenciaVehiculox>();
                Recordset rs = (Recordset)this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery("select * from " + this.NombreTabla+ " where U_VehiculoID = " + id);
                for (int i = 0; i < rs.RecordCount; i++)
                {
                    this.ItemCode = rs.Fields.Item("Code").Value;
                    this.oUDT.GetByKey(this.ItemCode);
                    UDTToIncidenciaVehiculo();
                    var temp = (IncidenciaVehiculox)this.IncidenciaEnVehiculo.Clone();
                    result.Add(temp);
                }
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #endregion
    }
}
