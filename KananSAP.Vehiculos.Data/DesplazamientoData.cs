﻿using Kanan.Vehiculos.BO2;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KananSAP.Vehiculos.Data
{
    public class DesplazamientoData
    {
        #region Propiedades

        public Desplazamiento Desplazamiento { get; set; }
        public Vehiculo Vehiculo { get; set; }
        public UserTable oUDT { get; set; }
        public Items oItem { get; set; }
        public string ItemCode { get; set; }
        private Company oCompany;

        public bool ExistUDT()
        {
            return oUDT.GetByKey(this.ItemCode);
        }
        #endregion

        #region Constructor

        public DesplazamientoData(Company company)
        {
            this.oCompany = company;
            this.Desplazamiento = new Desplazamiento();
            this.Vehiculo = new Vehiculo();
            try
            {
                this.oUDT = company.UserTables.Item("VSKF_DESPLAZAMIENTO");
            }
            catch(Exception ex)
            {
                this.oUDT = null;
            }
        }

        #endregion

        public void DesplazamientoToUDT()
        {
            try
            {
                int iniciado = 0;
                if(this.Desplazamiento.IsInitOdometro.HasValue)
                    iniciado = Convert.ToInt32(this.Desplazamiento.IsInitOdometro);
                this.oUDT.Code = this.Vehiculo.VehiculoID.ToString() ?? "0";
                this.oUDT.Name = this.Vehiculo.VehiculoID.ToString() ?? "0";
                this.oUDT.UserFields.Fields.Item("U_Distancia").Value = (double?)this.Desplazamiento.DistanciaRecorrida ?? 0;
                this.oUDT.UserFields.Fields.Item("U_Fecha").Value = (DateTime?)this.Desplazamiento.Fecha ?? null;
                this.oUDT.UserFields.Fields.Item("U_VehiculoID").Value = (int?)this.Vehiculo.VehiculoID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_EstaIniciado").Value = iniciado;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void UDTToDesplazamiento()
        {
            try
            {
                this.Desplazamiento.DistanciaRecorrida = (double?)this.oUDT.UserFields.Fields.Item("U_Distancia").Value ?? null;
                this.Desplazamiento.Fecha = (DateTime?)this.oUDT.UserFields.Fields.Item("U_Fecha").Value ?? null;
                this.Vehiculo.VehiculoID = (int?)this.oUDT.UserFields.Fields.Item("U_VehiculoID").Value ?? 0;
                this.Desplazamiento.IsInitOdometro = Convert.ToBoolean(this.oUDT.UserFields.Fields.Item("U_EstaIniciado").Value);
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void SetNewDesplazamiento()
        {
            try
            {
                this.oUDT.UserFields.Fields.Item("U_Distancia").Value = (double?)this.Desplazamiento.DistanciaRecorrida ?? 0;
                this.oUDT.UserFields.Fields.Item("U_Fecha").Value = (DateTime?)this.Desplazamiento.Fecha ?? null;
                if (this.oUDT.UserFields.Fields.Item("U_EstaIniciado").Value != 1)
                    this.oUDT.UserFields.Fields.Item("U_EstaIniciado").Value = 1;

            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void Insertar()
        {
            try
            {
                int result = oUDT.Add();
                string errormsj;
                int errornum;
                if (result != 0)
                {
                    oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Actualizar()
        {
            try
            {
                int result = oUDT.Update();
                string errormsj;
                int errornum;
                if (result != 0)
                {
                    oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public void Eliminar()
        {
            try
            {
                int result = oUDT.Remove();
                string errormsj;
                int errornum;
                if (result != 0)
                {
                    oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public bool HashDesplazamiento(Recordset rs)
        {
            if (rs.RecordCount < 1)
                return false;
            return true;
        }

        public bool SetUDTByVehiculoID(int? vehiculoID)
        {
            try
            {
                var rs = this.ConsultarByID(vehiculoID);
                int _id = 0;
                if (HashDesplazamiento(rs))
                {
                    rs.MoveFirst();
                    _id = rs.Fields.Item("U_VehiculoID").Value;
                    if (_id == vehiculoID)
                        return this.oUDT.GetByKey(rs.Fields.Item("Code").Value.ToString());

                }
                this.oUDT.Code = string.Empty;
                return false;
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }

        public Recordset ConsultarByID(int? vehiculoID)
        {
            try
            {
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(string.Format(@"select * from ""@VSKF_DESPLAZAMIENTO"" where U_VehiculoID = {0}", vehiculoID));
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Recordset Consultar(Desplazamiento desplazamiento, Vehiculo vehiculo)
        {
            try
            {
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(@"select * from ""@VSKF_DESPLAZAMIENTO"" ");
                if (desplazamiento.Fecha != null)
                    where.Append(string.Format(" and U_Fecha = '{0}' ", desplazamiento.Fecha));
                if (desplazamiento.DistanciaRecorrida != null)
                    where.Append(string.Format(" and U_Distancia = {0} ", desplazamiento.DistanciaRecorrida));
                if (vehiculo.VehiculoID != null)
                    where.Append(string.Format(" and U_VehiculoID = {0} ", vehiculo.VehiculoID));
                if (desplazamiento.IsInitOdometro != null)
                    where.Append(string.Format(" and U_EstaIniciado = {0} ", Convert.ToInt32(desplazamiento.IsInitOdometro)));
                
                string filtro = where.ToString();
                if (filtro.Length > 0)
                {
                    if (filtro.StartsWith("and"))
                        filtro = filtro.Substring(3);
                    query.Append(" where " + filtro);
                }
                query.Append(" ORDER BY U_Fecha ASC ");
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
     
        public Desplazamiento LastRecordSetToDesplazamiento(Recordset rs)
        {
            try
            {
                this.Desplazamiento = new Kanan.Vehiculos.BO2.Desplazamiento();
                rs.MoveFirst();
                if (rs.Fields.Item("U_Distancia").Value != null)
                    Desplazamiento.DistanciaRecorrida = (double)rs.Fields.Item("U_Distancia").Value;
                if (rs.Fields.Item("U_Fecha").Value != null)
                    Desplazamiento.Fecha = (DateTime)rs.Fields.Item("U_Fecha").Value;
                if (rs.Fields.Item("U_EstaIniciado").Value != null)
                    Desplazamiento.IsInitOdometro = Convert.ToInt32(rs.Fields.Item("U_EstaIniciado").Value);
                return this.Desplazamiento;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

    }
}
