﻿using System;
using Kanan.Comun.BO2;
using Kanan.Operaciones.BO2;
using KananSAP.Operaciones.Data;
using Kanan.Vehiculos.BO2;
using SAPbobsCOM;
using System.Text;
using System.Collections.Generic;

namespace KananSAP.Vehiculos.Data
{
    public class VehiculoSAP
    {
        #region Propiedades

        public Vehiculo vehiculo { get; set; }
        public CargaCombustibleSAP CrgCombustibleSAP { get; set; }
        public SucursalSAP SucursalSAP { get; set; }
        public TipoVehiculoSAP TipoVehiculoSAP { get; set; }
        public UserTable oUDT { get; set; }
        public Items oItem { get; set; }
        public string ItemCode { get; set; }
        private Company vCmp;
        public int? Sincronizado { get; set; }
        public Guid? UUID { get; set; }

        //Migracion Hana
        Recordset Insertar;
        String Parametro;
        private String NombreTabla;
        bool EsSQL;

        //Se agregó para ser visible desde otros proyerctos y realizar las consultas con
        // un recordser en DB Hana
        public String NombreTablaVehiculoSAP { get; set; }

        public bool ExistUDT()
        {
            return oUDT.GetByKey(this.ItemCode);
        }
        #endregion

        #region Constructor
        public VehiculoSAP(SAPbobsCOM.Company company)
        {
            this.vCmp = company;
            this.vehiculo = new Vehiculo
            {
                TipoVehiculo = new TipoVehiculo(),
                Propietario = new Empresa(),
                SubPropietario = new Sucursal(),
                ImagenVehiculo = new Kanan.Comun.BO2.Imagen(),
                EquipoRastreo = new Kanan.Comun.BO2.Modem(),
            };

            this.CrgCombustibleSAP = new CargaCombustibleSAP(this.vCmp);
            this.SucursalSAP = new SucursalSAP(this.vCmp);
            this.TipoVehiculoSAP = new TipoVehiculoSAP(this.vCmp);

            this.oItem = (SAPbobsCOM.Items)company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
            try
            {
                this.oUDT = company.UserTables.Item("VSKF_VEHICULO");

                //Migración HANA
                this.Insertar = (Recordset)this.vCmp.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.EsSQL = vCmp.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.NombreTabla = EsSQL ? "[@VSKF_VEHICULO]" : @"""@VSKF_VEHICULO""";

                this.NombreTablaVehiculoSAP = this.NombreTabla;
            }
            catch (Exception)
            {
                this.oUDT = null;
            }
        } 
        #endregion

        #region Metodos

        #region Metodos Interface

        public void VehiculoToSAP()
        {
            VehiculoToItem();
            VehiculoToUDT();
        }

        private void VehiculoToItem()
        {
            //this.oItem.GetByKey(this.ItemCode);
            oItem.ItemCode = this.ItemCode;
            //oItem.ItemName = this.vehiculo.Nombre;
            //oItem.ForeignName = this.vehiculo.Descripcion;
            //oItem.ItemsGroupCode = 0;
            //oItem.SerialNum = this.vehiculo.Placa;
            //oItem.QuantityOnStock = 1;
        }

        public void VehiculoToUDT()
        {
            oUDT.Code = this.ItemCode;
            oUDT.Name = this.vehiculo.Nombre; 
            this.oUDT.UserFields.Fields.Item("U_VehiculoID").Value = this.vehiculo.VehiculoID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_Placa").Value = this.vehiculo.Placa ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_Modelo").Value = this.vehiculo.Modelo ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_Descripcion").Value = this.vehiculo.Descripcion ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_TipoV_ID").Value = this.vehiculo.TipoVehiculo.TipoVehiculoID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_TipoChss").Value = this.vehiculo.TipoChasis ?? 0;
            this.oUDT.UserFields.Fields.Item("U_ImagenID").Value = this.vehiculo.ImagenVehiculo.ImagenID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_Nombre").Value = this.vehiculo.Nombre ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_SucursalID").Value = this.vehiculo.SubPropietario.SucursalID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value = this.vehiculo.Propietario.EmpresaID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_Poliza").Value = this.vehiculo.Poliza ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_FechaPoliza").Value = this.vehiculo.VigenciaPoliza ?? new DateTime();
            this.oUDT.UserFields.Fields.Item("U_Plazas").Value = this.vehiculo.Plazas ?? 0;
            this.oUDT.UserFields.Fields.Item("U_RendComb").Value = (int) Math.Round(this.vehiculo.RendimientoCombustible?? 0);
            this.oUDT.UserFields.Fields.Item("U_EsActivo").Value = this.vehiculo.EsActivo != null ? ((bool) this.vehiculo.EsActivo ? 1 : 0) : 0;
            this.oUDT.UserFields.Fields.Item("U_Marca").Value = this.vehiculo.Marca ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_Anio").Value = this.vehiculo.Anio ?? 0;
            this.oUDT.UserFields.Fields.Item("U_ModemID").Value = this.vehiculo.EquipoRastreo.ModemID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_AvisoPoliza").Value = this.vehiculo.AvisoPoliza ?? 0;
            this.oUDT.UserFields.Fields.Item("U_RendCarga").Value = (int) Math.Round(this.vehiculo.RendimientoCarga ?? 0);
            this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value =  (this.Sincronizado < 0 ? 0 : Sincronizado) ?? 0;
            this.oUDT.UserFields.Fields.Item("U_UUID").Value = this.UUID == null ? string.Empty : this.UUID.ToString();

            if (this.vehiculo.costo != null & this.vehiculo.costo > 0)
                this.oUDT.UserFields.Fields.Item("U_costo").Value = Convert.ToDouble(this.vehiculo.costo);
            if (this.vehiculo.TipoChasis != null & this.vehiculo.TipoChasis > 0)
                this.oUDT.UserFields.Fields.Item("U_TipoChss").Value = this.vehiculo.TipoChasis;

            this.oUDT.UserFields.Fields.Item("U_IAVE").Value = !String.IsNullOrEmpty(this.vehiculo.TarjetaIAVE) ? this.vehiculo.TarjetaIAVE : String.Empty;

            this.oUDT.UserFields.Fields.Item("U_Cobertura").Value = this.vehiculo.Cobertura ?? string.Empty;

            this.oUDT.UserFields.Fields.Item("U_ComponenteID").Value = this.vehiculo.ComponenteID ?? 0;

            //this.vehiculo = (string)this.oUDT.UserFields.Fields.Item("U_ULongitud").Value;
            //this.vehiculo.Anio = Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_UVolumen").Value);
            //this.vehiculo.Marca = (string)this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value;
        }

        public bool SAPToVehiculo(string ItemCode)
        {
            if (!this.oItem.GetByKey(ItemCode)) return false;
            if (!this.oUDT.GetByKey(ItemCode)) return false;
            ItemToVehiculo();
            UDTToVehiculo();
            return true;
        }

        public bool SAPToVehiculo()
        {
            ItemToVehiculo();
            UDTToVehiculo();
            return true;
        }

        private void ItemToVehiculo()
        {
            this.ItemCode = this.oItem.ItemCode;
            //this.vehiculo.Nombre = this.oItem.ItemName;
            //this.vehiculo.Descripcion = this.oItem.ForeignName;
            //this.vehiculo.Placa = this.oItem.SerialNum;
        }

        public void UDTToVehiculo()
        {
            //this.vehiculo.Nombre = (string) this.oUDT.Name;
            //this.vehiculo.Modelo = (string) this.oUDT.UserFields.Fields.Item("U_Model").Value;
            //this.vehiculo.Marca = (string) this.oUDT.UserFields.Fields.Item("U_Brand").Value;
            //this.vehiculo.Anio = Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_Year").Value);
            //this.vehiculo.Poliza = (string) this.oUDT.UserFields.Fields.Item("U_Policy").Value;
            //this.vehiculo.VigenciaPoliza = DateTime.Parse(this.oUDT.UserFields.Fields.Item("U_PolicyDate").Value.ToString());

            this.vehiculo = new Vehiculo
            {
                Propietario = new Empresa(),
                SubPropietario = new Sucursal(),
                ImagenVehiculo = new Imagen(),
                TipoVehiculo = new TipoVehiculo(),
                EquipoRastreo = new Modem()
            };

            this.vehiculo.VehiculoID = (int)this.oUDT.UserFields.Fields.Item("U_VehiculoID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_VehiculoID").Value;
            this.vehiculo.Placa = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Placa").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Placa").Value;
            this.vehiculo.Modelo = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Modelo").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Modelo").Value;
            this.vehiculo.Descripcion = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Descripcion").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Descripcion").Value;
            this.vehiculo.TipoVehiculo.TipoVehiculoID = (int)this.oUDT.UserFields.Fields.Item("U_TipoV_ID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_TipoV_ID").Value;
            this.vehiculo.TipoChasis = (int)this.oUDT.UserFields.Fields.Item("U_TipoChss").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_TipoChss").Value;
            this.vehiculo.ImagenVehiculo.ImagenID = (int)this.oUDT.UserFields.Fields.Item("U_ImagenID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_ImagenID").Value;
            this.vehiculo.Nombre = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Nombre").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Nombre").Value;
            this.vehiculo.SubPropietario.SucursalID = (int)this.oUDT.UserFields.Fields.Item("U_SucursalID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_SucursalID").Value;
            this.vehiculo.Propietario.EmpresaID = (int)this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value;
            this.vehiculo.Poliza = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Poliza").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Poliza").Value;
            try
            {
                this.vehiculo.VigenciaPoliza = DateTime.Parse(this.oUDT.UserFields.Fields.Item("U_FechaPoliza").Value.ToString());
            }
            catch 
            {
                this.vehiculo.VigenciaPoliza = null;
            }
            this.vehiculo.Plazas = (int)this.oUDT.UserFields.Fields.Item("U_Plazas").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_Plazas").Value;

            this.vehiculo.RendimientoCombustible = (int)this.oUDT.UserFields.Fields.Item("U_RendComb").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_RendComb").Value; ;

            this.vehiculo.EsActivo = this.oUDT.UserFields.Fields.Item("U_EsActivo").Value == 1;
            this.vehiculo.Marca = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Marca").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Marca").Value;
            this.vehiculo.Anio = Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_Anio").Value) == 0 ? null : Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_Anio").Value);
            this.vehiculo.EquipoRastreo.ModemID = (int)this.oUDT.UserFields.Fields.Item("U_ModemID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_ModemID").Value;
            this.vehiculo.AvisoPoliza = (int)this.oUDT.UserFields.Fields.Item("U_AvisoPoliza").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_AvisoPoliza").Value; ;
            this.vehiculo.RendimientoCarga = Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_RendCarga").Value) == 0 ? null : Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_RendCarga").Value);
            this.Sincronizado = this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value == null ? null : (int?)this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value;
            var uuid = this.oUDT.UserFields.Fields.Item("U_UUID").Value;
            this.UUID = string.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_UUID").Value.ToString()) ? null : Guid.Parse(this.oUDT.UserFields.Fields.Item("U_UUID").Value);
            
            if ((int)this.oUDT.UserFields.Fields.Item("U_TipoChss").Value != null & (int)this.oUDT.UserFields.Fields.Item("U_TipoChss").Value > 0)
                this.vehiculo.TipoChasis = (int)this.oUDT.UserFields.Fields.Item("U_TipoChss").Value;
            if ((double)this.oUDT.UserFields.Fields.Item("U_costo").Value != null & (double)this.oUDT.UserFields.Fields.Item("U_costo").Value > 0)
                this.vehiculo.costo = (double)this.oUDT.UserFields.Fields.Item("U_costo").Value;

            if (!String.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_IAVE").Value))
                this.vehiculo.TarjetaIAVE = (String)this.oUDT.UserFields.Fields.Item("U_IAVE").Value;

            if (!string.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_Cobertura").Value))
                this.vehiculo.Cobertura = Convert.ToString( this.oUDT.UserFields.Fields.Item("U_Cobertura").Value);
            //this.vehiculo.Cobertura = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Cobertura").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Cobertura").Value;

            this.vehiculo.ComponenteID = (int)this.oUDT.UserFields.Fields.Item("U_ComponenteID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_ComponenteID").Value;

            //this.vehiculo = (string)this.oUDT.UserFields.Fields.Item("U_ULongitud").Value;
            //this.vehiculo.Anio = Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_UVolumen").Value);
            //this.vehiculo.Marca = (string)this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value;
        }
        #endregion

        #region Acciones
        public void Insert()
        {
            try
            {
                InsertUDT();
            }
            catch (Exception ex)
            {
                oItem.GetByKey(this.ItemCode);
                oItem.Remove();
                throw new Exception(ex.Message);
            }
        }

        public void InsertUDT()
        {
            try
            {
                String FechaPoliza = this.vehiculo.VigenciaPoliza.Value.ToString("yyyy-MM-dd");

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format("INSERT INTO {0} VALUES('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', '{19}', '{20}', '{21}', '{22}', '{23}', '{24}', '{25}', '{26}', '{27}', '{28}', '{29}', '{30}', {31})", this.NombreTabla, this.vehiculo.Nombre, this.vehiculo.Nombre, this.vehiculo.VehiculoID, this.vehiculo.Placa, this.vehiculo.Modelo, this.vehiculo.Descripcion, this.vehiculo.TipoVehiculo.TipoVehiculoID, this.vehiculo.ImagenVehiculo.ImagenID, this.vehiculo.Nombre, this.vehiculo.SubPropietario.SucursalID, this.vehiculo.Propietario.EmpresaID, null, this.vehiculo.Poliza, FechaPoliza, this.vehiculo.Plazas, this.vehiculo.RendimientoCombustible, Convert.ToInt32(this.vehiculo.EsActivo), this.vehiculo.Marca, this.vehiculo.Anio, this.vehiculo.EquipoRastreo.ModemID, this.vehiculo.AvisoPoliza, this.vehiculo.RendimientoCarga, null, null, "0", this.UUID, this.vehiculo.TipoChasis, Convert.ToDouble(this.vehiculo.costo), Convert.ToString(this.vehiculo.TarjetaIAVE), this.vehiculo.Cobertura, this.vehiculo.ComponenteID != null & this.vehiculo.ComponenteID > 0 ? "'" + this.vehiculo.ComponenteID + "'" : "NULL");

                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana
            }
            catch (Exception ex)
            {
                //throw new Exception("Error al intentar insertar un vehículo. " + ex.Message);
                #region SQL
                int flag = oUDT.Add();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    vCmp.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL
            }
        }

        public void Remove()
        {
            if (oItem.GetByKey(this.ItemCode))
            {
                string ItemCode = String.Empty;
                if (!String.IsNullOrEmpty(this.oItem.ItemCode))
                    ItemCode = this.oItem.ItemCode;
                else
                {
                    throw new Exception("Erron al intentar obtener identificador. ");
                }

                #region SQL || Hana
                //this.Parametro = String.Empty;
                //this.Parametro = String.Format("DELETE FROM OITM WHERE ItemCode = '{0}'", ItemCode);

                //this.Insertar.DoQuery(this.Parametro);

                //this.Parametro = String.Empty;
                //this.Parametro = String.Format("DELETE FROM OITW WHERE ItemCode = '{0}'", ItemCode);

                //this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana

                #region SQL
                int flag = oItem.Remove();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    vCmp.GetLastError(out errornum, out errormsj);
                    //oItem.Add();
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL

                if (oUDT.GetByKey(this.ItemCode))
                {
                    string ID = String.Empty;
                    if (!String.IsNullOrEmpty(this.oUDT.Code))
                        ID = this.oUDT.Code;
                    else
                    {
                        throw new Exception("Erron al intentar obtener identificador. ");
                    }

                    #region SQL || Hana
                    this.Parametro = String.Empty;
                    this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code"" = '{1}'", this.NombreTabla, ID);

                    this.Insertar.DoQuery(this.Parametro);
                    #endregion SQL || Hana

                    #region SQL
                    /*flag = oUDT.Remove();
                    if (flag != 0)
                    {
                        int errornum;
                        string errormsj;
                        vCmp.GetLastError(out errornum, out errormsj);
                        //oItem.Add();
                        throw new Exception("Error: " + errornum + " - " + errormsj);
                    }*/
                    #endregion SQL
                }
            }
        }

        public void Update()
        {
            String FechaPoliza = this.vehiculo.VigenciaPoliza.Value.ToString("yyyy-MM-dd");

            try
            {
                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET ""Code"" = '{1}', ""Name"" = '{2}', U_VehiculoID = '{3}', U_Placa = '{4}', U_Modelo = '{5}', U_Descripcion = '{6}', U_TipoV_ID = '{7}', U_ImagenID = '{8}', U_Nombre = '{9}', U_SucursalID = '{10}', U_EmpresaID = '{11}', U_NombreVehiculo = '{12}', U_Poliza = '{13}', U_FechaPoliza = '{14}', U_Plazas = '{15}', U_RendComb = '{16}', U_EsActivo = '{17}', U_Marca = '{18}', U_Anio = '{19}', U_ModemID = '{20}', U_AvisoPoliza = '{21}', U_RendCarga = '{22}', U_ULongitud = '{23}', U_UVolumen = '{24}', U_Sincronizado = '{25}', U_TipoChss = '{26}', U_costo = {27}, U_IAVE = '{28}', U_Cobertura = '{29}', U_ComponeteID = {30} WHERE Code = '{1}'", this.NombreTabla, this.vehiculo.Nombre, this.vehiculo.Nombre, this.vehiculo.VehiculoID, this.vehiculo.Placa, this.vehiculo.Modelo, this.vehiculo.Descripcion, this.vehiculo.TipoVehiculo.TipoVehiculoID, this.vehiculo.ImagenVehiculo.ImagenID, this.vehiculo.Nombre, this.vehiculo.SubPropietario.SucursalID, this.vehiculo.Propietario.EmpresaID, null, this.vehiculo.Poliza, FechaPoliza, this.vehiculo.Plazas, this.vehiculo.RendimientoCombustible, Convert.ToInt32(this.vehiculo.EsActivo), this.vehiculo.Marca, this.vehiculo.Anio, this.vehiculo.EquipoRastreo.ModemID, this.vehiculo.AvisoPoliza, this.vehiculo.RendimientoCarga, null, null, this.Sincronizado, this.vehiculo.TipoChasis, Convert.ToDouble(this.vehiculo.costo), Convert.ToString(this.vehiculo.TarjetaIAVE), this.vehiculo.Cobertura, this.vehiculo.ComponenteID != null & this.vehiculo.ComponenteID > 0 ? "'" + this.vehiculo.ComponenteID + "'" : "NULL");

                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana
            }

            catch
            {
                #region SQL
                int flag = oUDT.Update();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    vCmp.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL
            }
        }

        public void Deactivate()
        {
            #region SQL || Hana
            this.Parametro = String.Empty;
            this.Parametro = string.Format(@"UPDATE {0} SET U_EsActivo = 0 WHERE ""Code"" = '{1}'", this.NombreTabla, this.oUDT.Code);

            this.Insertar.DoQuery(this.Parametro);
            #endregion SQL || Hana

            #region SQL
            /*this.oUDT.UserFields.Fields.Item("U_EsActivo").Value = 0;
            this.vehiculo.EsActivo = false;
            Update();*/
            #endregion SQL
        }
        #endregion

        #region Metodos Auxiliares

        public Recordset GetComponentes()
        {
            this.Parametro = String.Empty;

            this.Parametro = String.Format(@"SELECT * FROM {0} WHERE ""U_COMPONENTEID"" IS NULL", this.NombreTabla);
            this.Insertar.DoQuery(this.Parametro);

            return this.Insertar;
        }

        public bool SetUDTByItemCode()
        {
            return this.oUDT.GetByKey(this.ItemCode);
        }

        public bool SetUDTByVehiculoID(int id)
        {
            var rs = GetAllVehicles(null);
            rs.MoveFirst();
            for (int i = 0; i < rs.RecordCount; i++)
            {
                var flag = rs.Fields.Item("U_VEHICULOID").Value;
                if (Convert.ToInt32(rs.Fields.Item("U_VEHICULOID").Value) == id)
                {
                    return this.oUDT.GetByKey(rs.Fields.Item("Code").Value.ToString());
                }
                rs.MoveNext();
            }
            this.oUDT.Code = string.Empty;
            return false;
        }

        public Recordset GetAllVehicles(int? sucursalid)
        {
            Recordset rs = (Recordset) this.vCmp.GetBusinessObject(BoObjectTypes.BoRecordset);
            string query = "select * from " + this.NombreTabla + @" where ""U_ESACTIVO"" = 1 ";
            if (sucursalid != null)
            {
                query = query + " and U_SUCURSALID = " + sucursalid;
            }
            rs.DoQuery(query);
            return rs;
        }

        public Recordset GetVehiculoByID(int VHID)
        {
            Recordset rs = (Recordset)this.vCmp.GetBusinessObject(BoObjectTypes.BoRecordset);
            string query = "select * from " + this.NombreTabla + @" where ""U_ESACTIVO"" = 1 and U_VehiculoID =  " + VHID;
            rs.DoQuery(query);
            return rs;
        }

        public String GetVehiculoIDByITemCode(String itemcode)
        {
            String id;
            Recordset rs = (Recordset)this.vCmp.GetBusinessObject(BoObjectTypes.BoRecordset);
            string query = "select * from " + this.NombreTabla + @" where ""Code"" =  '" + itemcode + "'";
            rs.DoQuery(query);
            id = rs.Fields.Item("U_VEHICULOID").Value;
            return id;
        }

        public String GetItemCodeByID(int? vehiculoid)
        {
            String ItemCode;
            this.Parametro = String.Empty;
            this.Parametro = String.Format(@"SELECT ""Code"" FROM {0} WHERE ""U_VEHICULOID"" = '{1}'", this.NombreTabla, vehiculoid);
            this.Insertar.DoQuery(Parametro);
            ItemCode = Insertar.Fields.Item("Code").Value;
            return ItemCode;
        }

        public String GetCentroCostosByID(int? vehiculoid)
        {
            String CentroCostos;
            this.Parametro = String.Empty;
            this.Parametro = String.Format(@"SELECT ""Name"" FROM {0} WHERE ""U_VEHICULOID"" = '{1}'", this.NombreTabla, vehiculoid);
            this.Insertar.DoQuery(Parametro);
            CentroCostos = Insertar.Fields.Item("Name").Value;
            return CentroCostos;
        }

        public String GetPlacaByVehiculoID(int? vehiculoid)
        {
            try
            {
                String Placa;
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"SELECT U_Placa FROM {0} WHERE ""U_VEHICULOID"" = '{1}'", this.NombreTabla, vehiculoid);
                this.Insertar.DoQuery(Parametro);
                Placa = Insertar.Fields.Item("U_PLACA").Value;
                return Placa;

                //Comentado porque la validación se realiza en el presentador.
                //if (!String.IsNullOrEmpty(Placa))
                    //return Placa;
                //else
                    //throw new Exception("No se encontró matrícula. ");
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar recuperar la matrícula de un vehículo. " + ex.Message);
            }
        }

        public bool ExisteVehiculoID(int VHID)
        {
            bool Existe = false;
            Recordset rs = (Recordset)this.vCmp.GetBusinessObject(BoObjectTypes.BoRecordset);
            rs = GetVehiculoByID(VHID);
            Existe = HasVehiculos(rs);
            return Existe;
        }

        public Recordset Consultar(Vehiculo vehiculo)
        {
            try
            {
                return this.Consultar(vehiculo, null, false);
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public Recordset Consultar(Vehiculo vehiculo, string ids, bool? EsConsiderable)
        {
            try
            {
                #region Validacion de Objetos
                if (vehiculo == null) vehiculo = new Vehiculo();
                if (vehiculo.ImagenVehiculo == null) vehiculo.ImagenVehiculo = new Imagen();
                if (vehiculo.TipoVehiculo == null) vehiculo.TipoVehiculo = new TipoVehiculo();
                if (vehiculo.SubPropietario == null) vehiculo.SubPropietario = new Sucursal();
                if (vehiculo.Propietario == null) vehiculo.Propietario = new Empresa();
                #endregion

                Recordset rs = this.vCmp.GetBusinessObject(BoObjectTypes.BoRecordset);
                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(@" SELECT DISTINCT vh.""Code"", vh.""Name"", vh.""U_VEHICULOID"",vh.""U_RENDCARGA"",vh.""U_POLIZA"",vh.""U_MODEMID"",vh.""U_PLAZAS"", vh.""U_RENDCOMB"", vh.""U_PLACA"", vh.""U_MODELO"", vh.""U_DESCRIPCION"", vh.""U_NOMBRE"", vh.""U_TIPOV_ID"", vh.""U_POLIZA"", vh.""U_MARCA"", vh.""U_ANIO"", vh.""U_IMAGENID"",vh.""U_SUCURSALID"",vh.""U_EMPRESAID"",sc.""U_DIRECCION"" as ""SUCURSAL"",sc.""U_CIUDAD"", tv.""U_NOMBRE"" as ""TIPOVEHICULO_NOMBRE"", vh.""U_ESACTIVO"", ");
                
                if(vehiculo.VehiculoID != null)
                {
                    if(vehiculo.FechaFiltroInicio != null && vehiculo.FechaFiltroFin != null)
                    {
                        query.Append(string.Format(" (select top 1 ((select top 1 U_Odometro from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + " where U_VehiculoID = vh.U_VehiculoID and U_Fecha between '{0}' and '{1}'  order by U_Fecha desc) -  ", vehiculo.FechaFiltroInicio, vehiculo.FechaFiltroFin));
                        query.Append(string.Format(" (select top 1 U_Odometro from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + " where U_VehiculoID = vh.U_VehiculoID and U_Fecha between '{0}' and '{1}' order by U_Fecha asc)) as Odometro from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + "  ) as Distancia, ", vehiculo.FechaFiltroInicio, vehiculo.FechaFiltroFin));
                    }
                    else
                        query.Append(" (select top 1 ((select top 1 U_Odometro from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + " where U_VehiculoID = vh.U_VehiculoID and U_Fecha between (select top 1 U_Fecha from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + " where U_VehiculoID = vh.U_VehiculoID order by U_Fecha asc) and (select top 1 U_Fecha from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + " where U_VehiculoID = vh.U_VehiculoID order by U_Fecha desc)  order by U_Fecha desc) - (select top 1 U_Odometro from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + " where U_VehiculoID = vh.U_VehiculoID and U_Fecha between (select top 1 U_Fecha from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + " where U_VehiculoID = vh.U_VehiculoID order by U_Fecha asc) and (select top 1 U_Fecha from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + " where U_VehiculoID = vh.U_VehiculoID order by U_Fecha desc)  order by U_Fecha asc)) as Odometro from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + "  ) as Distancia, ");
                    where.Append(string.Format("and vh.U_VehiculoID = {0} ", vehiculo.VehiculoID));
                }
                else
                {
                    if(!string.IsNullOrEmpty(ids))
                    {
                        if (vehiculo.FechaFiltroInicio != null && vehiculo.FechaFiltroFin != null)
                        {
                            query.Append(string.Format(" (select top 1 ((select top 1 U_Odometro from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + " where U_VehiculoID = vh.U_VehiculoID and U_Fecha between '{0}' and '{1}'  order by U_Fecha desc) -  ", vehiculo.FechaFiltroInicio, vehiculo.FechaFiltroFin));
                            query.Append(string.Format(" (select top 1 U_Odometro from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + " where U_VehiculoID = vh.U_VehiculoID and U_Fecha between '{0}' and '{1}' order by U_Fecha asc)) as Odometro from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + "  ) as Distancia, ", vehiculo.FechaFiltroInicio, vehiculo.FechaFiltroFin));
                        }
                        else
                            query.Append(" (select top 1 ((select top 1 U_Odometro from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + " where U_VehiculoID = vh.U_VehiculoID and U_Fecha between (select top 1 U_Fecha from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + " where U_VehiculoID = vh.U_VehiculoID order by U_Fecha asc) and (select top 1 U_Fecha from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + " where U_VehiculoID = vh.U_VehiculoID order by U_Fecha desc)  order by U_Fecha desc) - (select top 1 U_Odometro from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + " where U_VehiculoID = vh.U_VehiculoID and U_Fecha between (select top 1 U_Fecha from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + " where U_VehiculoID = vh.U_VehiculoID order by U_Fecha asc) and (select top 1 U_Fecha from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + " where U_VehiculoID = vh.U_VehiculoID order by U_Fecha desc)  order by U_Fecha asc)) as Odometro from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + "  ) as Distancia, ");
                        where.Append(string.Format("and vh.U_VehiculoID in ({0}) ", ids));
                    }
                    else
                        query.Append(" (select top 1 ((select top 1 U_Odometro from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + " where U_VehiculoID = vh.U_VehiculoID and U_Fecha between (select top 1 U_Fecha from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + " where U_VehiculoID = vh.U_VehiculoID order by U_Fecha asc) and (select top 1 U_Fecha from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + " where U_VehiculoID = vh.U_VehiculoID order by U_Fecha desc)  order by U_Fecha desc) - (select top 1 U_Odometro from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + " where U_VehiculoID = vh.U_VehiculoID and U_Fecha between (select top 1 U_Fecha from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + " where U_VehiculoID = vh.U_VehiculoID order by U_Fecha asc) and (select top 1 U_Fecha from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + " where U_VehiculoID = vh.U_VehiculoID order by U_Fecha desc)  order by U_Fecha asc)) as Odometro from " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + "  ) as Distancia, ");
                }
                query.Append(" vh.U_AvisoPoliza  ");
                query.Append(" from " + this.NombreTabla + " vh left outer join " + this.SucursalSAP.NombreTablaSucursalSAP + " sc on sc.U_SucursalID= vh.U_SucursalID left outer join " + this.TipoVehiculoSAP.NombreTablaTipoVehiculoSAP + " tv on tv.U_TVeiculoID = vh.U_TipoV_ID  left outer join " + this.CrgCombustibleSAP.NombreTablaCargaCombustible + " ds on ds.U_VehiculoID= vh.U_VehiculoID  ");

                if (vehiculo.RendimientoCarga != null)
                    where.Append(string.Format("and vh.U_RendCarga = {0}", vehiculo.RendimientoCarga));
                if (!string.IsNullOrEmpty(vehiculo.Nombre))
                    where.Append(string.Format("and vh.U_Nombre like '%{0}%' ", vehiculo.Nombre));
                if (vehiculo.Plazas != null)
                    where.Append(string.Format("and vh.U_Plazas = {0} ", vehiculo.Plazas));
                if (vehiculo.EsActivo != null)
                    where.Append(string.Format("and vh.U_EsActivo = {0} ", Convert.ToInt32(vehiculo.EsActivo)));
                if (vehiculo.RendimientoCombustible != null)
                    where.Append(string.Format("and vh.U_RendComb = {0} ", vehiculo.RendimientoCombustible));
                if (!string.IsNullOrEmpty(vehiculo.Descripcion))
                    where.Append(string.Format("and vh.U_Descripcion like '%{0}%' ", vehiculo.Descripcion));
                if (!string.IsNullOrEmpty(vehiculo.Placa))
                    where.Append(string.Format("and vh.U_Placa like '%{0}%' ", vehiculo.Placa));
                if (vehiculo.TipoVehiculo.TipoVehiculoID != null)
                    where.Append(string.Format("and vh.U_TipoV_ID = {0} ", vehiculo.TipoVehiculo.TipoVehiculoID));
                if (!(bool)EsConsiderable)
                {
                    if (vehiculo.SubPropietario.SucursalID != null)
                        where.Append(string.Format("and vh.U_SucursalID = {0} ", vehiculo.SubPropietario.SucursalID));
                }
                else
                    where.Append("and vh.U_SucursalID IS NULL ");
                if (vehiculo.Propietario.EmpresaID != null)
                    where.Append(string.Format("and vh.U_EmpresaID = {0} ", vehiculo.Propietario.EmpresaID));
                if (!string.IsNullOrEmpty(vehiculo.Marca))
                    where.Append(string.Format("and vh.U_Marca = '{0}' ", vehiculo.Marca));
                if (vehiculo.Anio != null)
                    where.Append(string.Format("and vh.U_Anio = '{0}' ", vehiculo.Anio));
                if (vehiculo.AvisoPoliza != null)
                    where.Append(string.Format("and vh.U_AvisoPoliza = {0} ", vehiculo.AvisoPoliza));
                if (vehiculo.FechaFiltroInicio != null && vehiculo.FechaFiltroFin != null)
                    where.Append(string.Format("and ds.U_Fecha between '{0}' and '{1}' ", vehiculo.FechaFiltroInicio, vehiculo.FechaFiltroFin));
                string filtros = where.ToString().Trim();
                if (filtros.Length > 0)
                {
                    if (filtros.StartsWith("and"))
                        filtros = filtros.Substring(3);
                    query.Append(" where " + filtros);
                }
                query.Append(" ORDER BY vh.U_Nombre, U_VehiculoID ASC ");
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public Recordset ConsultarSimple(Vehiculo veh)
        {
            try
            {
                #region Validacion de Objetos
                if (veh == null) veh = new Vehiculo();
                if (veh.ImagenVehiculo == null) veh.ImagenVehiculo = new Imagen();
                if (veh.TipoVehiculo == null) veh.TipoVehiculo = new TipoVehiculo();
                if (veh.SubPropietario == null) veh.SubPropietario = new Sucursal();
                if (veh.Propietario == null) veh.Propietario = new Empresa();
                if (veh.EquipoRastreo == null) veh.EquipoRastreo = new Modem();
                #endregion

                Recordset rs = this.vCmp.GetBusinessObject(BoObjectTypes.BoRecordset);
                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(" SELECT * FROM " + this.NombreTabla + " as vh ");
                if (!string.IsNullOrEmpty(ItemCode))
                    where.Append(string.Format(@"and vh.""Code"" like '%{0}%' ", ItemCode));
                if (veh.VehiculoID != null)
                    where.Append(string.Format("and vh.U_VehiculoID = {0} ", veh.VehiculoID));
                if (!string.IsNullOrEmpty(veh.Placa))
                    where.Append(string.Format("and vh.U_Placa like '%{0}%' ", veh.Placa));
                if (!string.IsNullOrEmpty(veh.Modelo))
                    where.Append(string.Format("and vh.U_Modelo like '%{0}%' ", veh.Modelo));
                if (!string.IsNullOrEmpty(veh.Descripcion))
                    where.Append(string.Format("and vh.U_Descripcion like '%{0}%' ", veh.Descripcion));
                if (veh.TipoVehiculo != null && veh.TipoVehiculo.TipoVehiculoID != null)
                    where.Append(string.Format("and vh.U_TipoV_ID = {0} ", veh.TipoVehiculo.TipoVehiculoID));
                if (veh.TipoChasis != null && veh.TipoChasis != null)
                    where.Append(string.Format("and vh.U_TipoChss = {0} ", veh.TipoChasis));
                if (!string.IsNullOrEmpty(veh.Nombre))
                    where.Append(string.Format("and vh.U_Nombre like '%{0}%' ", veh.Nombre));
                if (veh.SubPropietario.SucursalID != null)
                    where.Append(string.Format("and vh.U_SucursalID = {0} ", veh.SubPropietario.SucursalID));
                if (veh.Propietario.EmpresaID != null)
                    where.Append(string.Format("and vh.U_EmpresaID = {0} ", veh.Propietario.EmpresaID));
                if (!string.IsNullOrEmpty(veh.Poliza))
                    where.Append(string.Format("and vh.U_Poliza = '{0}' ", veh.Poliza));
                if (veh.VigenciaPoliza != null)
                    where.Append(string.Format("and vh.U_FechaPoliza = {0} ", veh.VigenciaPoliza));
                if (veh.Plazas != null)
                    where.Append(string.Format("and vh.U_Plazas = {0} ", veh.Plazas));
                if (veh.RendimientoCombustible != null)
                    where.Append(string.Format("and vh.U_RendComb = {0} ", veh.RendimientoCombustible));
                if (veh.EsActivo != null)
                    where.Append(string.Format("and vh.U_EsActivo = {0} ", Convert.ToInt32(veh.EsActivo)));
                if (!string.IsNullOrEmpty(veh.Marca))
                    where.Append(string.Format("and vh.U_Marca = '{0}' ", veh.Marca));
                if (veh.Anio != null)
                    where.Append(string.Format("and vh.U_Anio = '{0}' ", veh.Anio));
                if (veh.EquipoRastreo.ModemID != null)
                    where.Append(string.Format("and vh.U_ModemID = {0} ", veh.EquipoRastreo.ModemID));
                if (veh.AvisoPoliza != null)
                    where.Append(string.Format("and vh.U_AvisoPoliza = {0} ", veh.AvisoPoliza));
                if (veh.RendimientoCarga != null)
                    where.Append(string.Format("and vh.U_RendCarga = {0} ", veh.RendimientoCarga));
                if (Sincronizado != null)
                    where.Append(string.Format(" and vh.U_Sincronizado = {0} ", Sincronizado));
                if (UUID != null)
                    where.Append(string.Format(" and vh.U_UUID = '{0}' ", UUID));
                if (veh.TipoChasis != null & veh.TipoChasis > 0)
                    where.Append(String.Format(" and vh.U_TipoChss = '{0}' ", veh.TipoChasis));
                if (veh.costo != null & veh.costo > 0)
                    where.Append(String.Format(" and vh.U_costo = '{0}' ", veh.costo));

                string filtros = where.ToString().Trim();
                if (filtros.Length > 0)
                {
                    if (filtros.StartsWith("and"))
                        filtros = filtros.Substring(3);
                    query.Append(" where " + filtros);
                }
                query.Append(" ORDER BY U_VehiculoID ASC ");
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool HasVehiculos(Recordset rs)
        {
            if (rs.RecordCount >= 1)
                return true;
            return false;
        }

        public List<Vehiculo> RecordSetToListVehiculos(Recordset rs)
        {
            try
            {
                List<Vehiculo> vehicles = new List<Vehiculo>();
                rs.MoveFirst();
                for(int i = 0; i < rs.RecordCount; i++)
                {
                    this.ItemCode = rs.Fields.Item("Code").Value;
                    this.oUDT.GetByKey(this.ItemCode);
                    this.UDTToVehiculo();
                    var tmp = (Vehiculo)this.vehiculo.Clone();
                    vehicles.Add(tmp);
                    rs.MoveNext();
                }
                return vehicles;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void SetSucursalToVehiculo(int? sucursalID)
        {
            try
            {
                this.oUDT.UserFields.Fields.Item("U_SucursalID").Value = sucursalID;
                this.Update();
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public int GetTotaVehiculos()
        {

            return GetAllVehicles(null).RecordCount;
        }

        public Recordset ActualizarVehicleID()
        {
            try
            {
                Recordset rs = this.vCmp.GetBusinessObject(BoObjectTypes.BoRecordset);
                string sQuery = string.Format("UPDATE " + this.NombreTabla + @"  SET U_VehiculoID = {0} WHERE ""Code"" = '{1}'", this.vehiculo.VehiculoID, this.ItemCode);
                rs.DoQuery(sQuery.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Recordset ObtenerVehiculosRegistrados(int? sucursalid, int? empleadoID)
        {
            Recordset rs = (Recordset)this.vCmp.GetBusinessObject(BoObjectTypes.BoRecordset);
            string query = "select * from " + this.NombreTabla + " where U_EsActivo = 1 AND U_VehiculoID <> 0 ";

            /*if (sucursalid != null)
            {
                if (sucursalid > 0)
                    query = query + " and U_SucursalID = " + sucursalid;

            }*/
            string sql = string.Format(@"SELECT * FROM  ""@VSKF_OPERADORVEHICL"" WHERE U_Liberado = 1");
            rs.DoQuery(sql);
            if (rs.RecordCount > 0)
            {
                query += string.Format(@" AND U_VehiculoID NOT IN ( SELECT U_VehiculoID FROM  ""@VSKF_OPERADORVEHICL"" WHERE U_Liberado = 1 ");
                if (empleadoID != null)
                {
                    if (empleadoID > 0)
                    {
                        sql = string.Format(@" SELECT ""Code"" FROM ""@VSKF_OPERADOR"" WHERE U_EmpleadoID = {0} ", empleadoID);
                        rs.DoQuery(sql);
                        if (rs.RecordCount > 0)                        

                            query += string.Format(" AND U_OperadorID <> {0} )", rs.Fields.Item("Code").Value);
                        
                        else query += ")";
                    }
                    else query += ")";
                }
                else query += ")";
            }

            rs.DoQuery(query);
            return rs;
        }
        #endregion

        #endregion
    }
}
