﻿using System;
using System.Collections.Generic;
using System.Text;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using SAPbobsCOM;

namespace KananSAP.Vehiculos.Data
{
    public class ActivoSAP
    {
        #region Propiedades

        public Caja activo { get; set; }
        public UserTable oUDT { get; set; }
        public Items oItem { get; set; }
        public string ItemCode { get; set; }
        private Company company;
        public int? Sincronizado { get; set; }
        public Guid? UUID { get; set; }

        //Migracion Hana
        Recordset Insertar;
        String Parametro;
        private String NombreTabla;
        bool EsSQL;

        //Se creó para ser visible desde otros proyectos y poder realizar consultas
        // con un recordser en DB Hana
        public String NombreTablaActivoSAP = String.Empty;


        public bool ExistUDT()
        {
            return oUDT.GetByKey(this.ItemCode);
        }
        #endregion

        #region Constructor
        public ActivoSAP(SAPbobsCOM.Company company)
        {
            this.company = company;
            this.activo = new Caja()
            {
                TipoActivos = new TipoActivos(),
                Propietario = new Empresa(),
                SubPropietario = new Sucursal()
            };
            this.oItem = (SAPbobsCOM.Items)company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
            try
            {
                this.oUDT = company.UserTables.Item("VSKF_ACTIVO");

                //Migración HANA
                this.Insertar = (Recordset)this.company.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.EsSQL = this.company.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.NombreTabla = EsSQL ? "[@VSKF_ACTIVO]" : @"""@VSKF_ACTIVO""";

                this.NombreTablaActivoSAP = this.NombreTabla;
            }
            catch (Exception)
            {
                this.oUDT = null;
            }
        } 
        #endregion

        #region Metodos

        #region Metodos Interface

        public bool SAPToActivo(string ItemCode)
        {
            if (!this.oItem.GetByKey(ItemCode)) return false;
            if (!this.oUDT.GetByKey(ItemCode)) return false;
            this.ItemCode = oItem.ItemCode;
            /*Se maneja en presenter como una accion independiente para mantener U_ActivoID*/
            //UDTToActivo();
            return true;
        }

        public void ActivoToSAP()
        {
            ActivoToItem();
            ActivoToUDT();
        }

        private void ActivoToItem()
        {
            oItem.ItemCode = this.ItemCode;
        }

        public void ActivoToUDT()
        {
            oUDT.Code = this.activo.NumeroEconomico;
            oUDT.Name = this.activo.NumeroEconomico;
            this.oUDT.UserFields.Fields.Item("U_ActivoID").Value = this.activo.CajaID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_NumeroEconomico").Value = this.activo.NumeroEconomico ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_Observaciones").Value = this.activo.Observaciones ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_EsActivo").Value = this.activo.EstaActivo != null ? ((bool)this.activo.EstaActivo ? 1 : 0) : 0;
            this.oUDT.UserFields.Fields.Item("U_PropietarioID").Value = this.activo.Propietario.EmpresaID ?? 0;
            if (this.activo.MantenibleID != null)
                this.oUDT.UserFields.Fields.Item("U_VehiculoID").Value = this.activo.MantenibleID;
            if (this.activo.Posicion != null)
                this.oUDT.UserFields.Fields.Item("U_Posicion").Value = this.activo.Posicion;
            if (this.activo.SubPropietario != null && this.activo.SubPropietario.SucursalID != null)
                this.oUDT.UserFields.Fields.Item("U_SucursalID").Value = this.activo.SubPropietario.SucursalID;
            this.oUDT.UserFields.Fields.Item("U_Tipo").Value = this.activo.Tipo ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_Marca").Value = this.activo.Marca ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_Modelo").Value = this.activo.Modelo ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_Placa").Value = this.activo.Placa ?? string.Empty;
            /*Campos agregados 28/06/2018*/
            this.oUDT.UserFields.Fields.Item("U_Anio").Value = this.activo.Anio ?? 0;
            this.oUDT.UserFields.Fields.Item("U_Detalle").Value = this.activo.Detalle ?? 0;
            this.oUDT.UserFields.Fields.Item("U_Poliza").Value = this.activo.Poliza ?? 0;
            this.oUDT.UserFields.Fields.Item("U_FechaPoliza").Value = this.activo.VencimientoPoliza ?? new DateTime();
            this.oUDT.UserFields.Fields.Item("U_AvisoPoliza").Value = this.activo.AvisoPoliza ?? (int?)null;
            this.oUDT.UserFields.Fields.Item("U_Plazas").Value = this.activo.Plazas ?? 0;
            this.oUDT.UserFields.Fields.Item("U_RendComb").Value = this.activo.RendComb ?? 0;
            this.oUDT.UserFields.Fields.Item("U_RendCarga").Value = this.activo.RendCarga ?? 0;
            this.oUDT.UserFields.Fields.Item("U_Descripcion").Value = this.activo.Descripcion ?? string.Empty;
            if (this.activo.TipoActivos != null && this.activo.TipoActivos != null)
                this.oUDT.UserFields.Fields.Item("U_TipoActivoID").Value = this.activo.TipoActivos.TipoActivoID;
            this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Sincronizado < 0 ? 0 : Sincronizado;
            this.oUDT.UserFields.Fields.Item("U_UUID").Value = this.UUID == null ? string.Empty : this.UUID.ToString();

            if (this.activo.costo != null & this.activo.costo > 0)
                this.oUDT.UserFields.Fields.Item("U_costo").Value = Convert.ToDouble(this.activo.costo);
            if (this.activo.TipoChasis != null & this.activo.TipoChasis > 0)
                this.oUDT.UserFields.Fields.Item("U_TipoChss").Value = this.activo.TipoChasis;

            this.oUDT.UserFields.Fields.Item("U_ComponenteID").Value = this.activo.ComponenteID ?? 0;
        }

        public Caja UDTToActivo()
        {
            try
            {
                this.activo.Posicion = this.oUDT.UserFields.Fields.Item("U_Posicion").Value == null ? null : (int?)this.oUDT.UserFields.Fields.Item("U_Posicion").Value;
                this.activo.SubPropietario.SucursalID = this.oUDT.UserFields.Fields.Item("U_SucursalID").Value == null ? null : (int?)this.oUDT.UserFields.Fields.Item("U_SucursalID").Value;
                this.activo.MantenibleID = this.oUDT.UserFields.Fields.Item("U_VehiculoID").Value == null ? null : (int?)this.oUDT.UserFields.Fields.Item("U_VehiculoID").Value;
                this.activo.CajaID = (int)this.oUDT.UserFields.Fields.Item("U_ActivoID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_ActivoID").Value;
                this.activo.NumeroEconomico = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_NumeroEconomico").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_NumeroEconomico").Value;
                this.activo.Observaciones = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Observaciones").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Observaciones").Value;
                this.activo.EstaActivo = this.oUDT.UserFields.Fields.Item("U_EsActivo").Value == 1;
                this.activo.Propietario.EmpresaID = this.oUDT.UserFields.Fields.Item("U_PropietarioID").Value == null ? null : (int?)this.oUDT.UserFields.Fields.Item("U_PropietarioID").Value;
                this.activo.Tipo = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Tipo").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Tipo").Value;
                this.activo.Marca = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Marca").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Marca").Value;
                this.activo.Modelo = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Modelo").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Modelo").Value;
                this.activo.Placa = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Placa").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Placa").Value;
                /*Campos agregados 28/06/2018*/
                this.activo.Anio = this.oUDT.UserFields.Fields.Item("U_Anio").Value == 0 ? null : Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_Anio").Value);
                this.activo.Detalle = this.oUDT.UserFields.Fields.Item("U_Detalle").Value == 0 ? null : Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_Detalle").Value);
                this.activo.Poliza = this.oUDT.UserFields.Fields.Item("U_Poliza").Value == 0 ? null : Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_Poliza").Value);
                this.activo.VencimientoPoliza = this.oUDT.UserFields.Fields.Item("U_FechaPoliza").Value == null ? null : Convert.ToDateTime(this.oUDT.UserFields.Fields.Item("U_FechaPoliza").Value);
                this.activo.AvisoPoliza = this.oUDT.UserFields.Fields.Item("U_AvisoPoliza").Value == 0 ? null : Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_AvisoPoliza").Value);
                this.activo.Plazas = this.oUDT.UserFields.Fields.Item("U_Plazas").Value == 0 ? null : Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_Plazas").Value);
                this.activo.RendComb = this.oUDT.UserFields.Fields.Item("U_RendComb").Value == 0 ? null : Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_RendComb").Value);
                this.activo.RendCarga = this.oUDT.UserFields.Fields.Item("U_RendCarga").Value == 0 ? null : Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_RendCarga").Value);
                this.activo.Descripcion = string.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_Descripcion").Value) ? null : Convert.ToString(this.oUDT.UserFields.Fields.Item("U_Descripcion").Value);
                this.activo.TipoActivos.TipoActivoID = this.oUDT.UserFields.Fields.Item("U_TipoActivoID").Value == 0 ? null : Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_TipoActivoID").Value);
                this.Sincronizado = this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value == 0 ? null : Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value);
                this.UUID = string.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_UUID").Value) ? new Guid() : Guid.Parse(this.oUDT.UserFields.Fields.Item("U_UUID").Value.ToString());

                if ((int)this.oUDT.UserFields.Fields.Item("U_TipoChss").Value != null & (int)this.oUDT.UserFields.Fields.Item("U_TipoChss").Value > 0)
                    this.activo.TipoChasis = (int)this.oUDT.UserFields.Fields.Item("U_TipoChss").Value;
                if ((double)this.oUDT.UserFields.Fields.Item("U_costo").Value != null & (double)this.oUDT.UserFields.Fields.Item("U_costo").Value > 0)
                    this.activo.costo = (double)this.oUDT.UserFields.Fields.Item("U_costo").Value;

                this.activo.ComponenteID = (int)this.oUDT.UserFields.Fields.Item("U_ComponenteID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_ComponenteID").Value;

                //Se retorna el BO debido a que no mantiene su valor fuera del método UDTToActivo.
                return this.activo;
            }
            catch
            {
                return null;
            }
        }

        #endregion

        #region Acciones

        public void Insert()
        {
            try
            {
                InsertUDT();
            }
            catch (Exception ex)
            {
                oItem.GetByKey(this.ItemCode);
                oItem.Remove();
                throw new Exception(ex.Message);
            }
        }

        public void InsertUDT()
        {
            try
            {
                String FechaPoliza = String.Empty;

                if (this.activo.VencimientoPoliza != null)
                    FechaPoliza = this.activo.VencimientoPoliza.Value.ToString("yyyy/MM/dd");
                else
                    FechaPoliza = String.Empty;

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format("INSERT INTO {0} VALUES('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', '{19}', '{20}', '{21}', '{22}', '{23}', '{24}', '{25}', '{26}', '{27}', '{28}', {29})", this.NombreTabla, this.activo.NumeroEconomico, this.activo.NumeroEconomico, Convert.ToInt32(this.activo.CajaID), this.activo.NumeroEconomico, this.activo.Observaciones, Convert.ToInt32(this.activo.EstaActivo), this.activo.Propietario.EmpresaID, Convert.ToInt32(this.activo.MantenibleID), this.activo.Posicion, this.activo.SubPropietario.SucursalID, this.activo.Tipo, this.activo.Marca, this.activo.Modelo, this.activo.Placa, this.activo.Descripcion, this.activo.TipoActivos.TipoActivoID, "0", this.UUID, this.activo.Anio, this.activo.Detalle, this.activo.Poliza, FechaPoliza, this.activo.AvisoPoliza, this.activo.Plazas, this.activo.RendComb, this.activo.RendCarga, this.activo.TipoChasis, Convert.ToDouble(this.activo.costo), this.activo.ComponenteID != null & this.activo.ComponenteID > 0 ? "'" + this.activo.ComponenteID + "'" : "NULL");

                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || hana
            }
            catch (Exception ex)
            {
                #region SQL
                int flag = oUDT.Add();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL
                //throw new Exception(ex.Message);
            }
        }

        public void Remove()
        {
            if (oItem.GetByKey(this.ItemCode))
            {
                #region Identificador
                /*string ID = String.Empty;
                if (!String.IsNullOrEmpty(this.oUDT.Code))
                    ID = this.oUDT.Code;
                else
                {
                    throw new Exception("Erron al intentar obtener identificador. ");
                }*/
                #endregion Identificador

                #region SQL || Hana
                /*this.Parametro = String.Empty;
                this.Parametro = String.Format("DELETE FROM OITM WHERE Code = '{0}'", ID);

                this.Insertar.DoQuery(this.Parametro);*/
                #endregion SQL || Hana

                #region SQL
                int flag = oItem.Remove();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    company.GetLastError(out errornum, out errormsj);
                    //oItem.Add();
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL

                if (oUDT.GetByKey(this.ItemCode))
                {
                    #region Identificador
                    string ID = String.Empty;
                    if (!String.IsNullOrEmpty(this.oUDT.Code))
                        ID = this.oUDT.Code;
                    else
                    {
                        throw new Exception("Erron al intentar obtener identificador. ");
                    }
                    #endregion Identificador

                    #region SQl || Hana
                    this.Parametro = String.Empty;
                    this.Parametro = String.Format("DELETE FROM {0} WHERE Code = '{1}'",this.NombreTabla, ID);

                    this.Insertar.DoQuery(this.Parametro);
                    #endregion SQL || Hana

                    #region SQL
                    /*flag = oUDT.Remove();
                    if (flag != 0)
                    {
                        int errornum;
                        string errormsj;
                        company.GetLastError(out errornum, out errormsj);
                        //oItem.Add();
                        throw new Exception("Error: " + errornum + " - " + errormsj);
                    }*/
                    #endregion SQL
                }
            }
        }

        public void Update()
        {
            try
            {
                String FechaPoliza = String.Empty;
                if (this.activo.VencimientoPoliza != null)
                    FechaPoliza = this.activo.VencimientoPoliza.Value.ToString("yyyy/MM/dd");
                else
                    FechaPoliza = String.Empty;

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET ""Code"" = '{1}', ""Name"" = '{2}', U_ActivoID = '{3}', U_NumeroEconomico = '{4}', U_Observaciones = '{5}', U_EsActivo = '{6}', U_PropietarioID = '{7}', U_VehiculoID = '{8}', U_Posicion = '{9}', U_SucursalID = '{10}', U_Tipo = '{11}', U_Marca = '{12}', U_Modelo = '{13}', U_Placa = '{14}', U_Descripcion = '{15}', U_TipoActivoID = '{16}', U_Sincronizado = '{17}', U_UUID = '{18}', U_Anio = '{19}', U_Detalle = '{20}', U_Poliza = '{21}', U_FechaPoliza = '{22}', U_AvisoPoliza = '{23}', U_Plazas = '{24}', U_RendComb = '{25}', U_RendCarga = '{26}', U_TipoChss = '{27}', U_costo = '{28}', U_ComponenteID = {29} WHERE Code = '{1}'", this.NombreTabla, this.activo.NumeroEconomico, this.activo.NumeroEconomico, Convert.ToInt32(this.activo.CajaID), this.activo.NumeroEconomico, this.activo.Observaciones, Convert.ToInt32(this.activo.EstaActivo), this.activo.Propietario.EmpresaID, Convert.ToInt32(this.activo.MantenibleID), this.activo.Posicion, this.activo.SubPropietario.SucursalID, this.activo.Tipo, this.activo.Marca, this.activo.Modelo, this.activo.Placa, this.activo.Descripcion, this.activo.TipoActivos.TipoActivoID, "0", this.UUID, this.activo.Anio, this.activo.Detalle, this.activo.Poliza, FechaPoliza, this.activo.AvisoPoliza, this.activo.Plazas, this.activo.RendComb, this.activo.RendCarga, this.activo.TipoChasis, Convert.ToDouble(this.activo.costo), this.activo.ComponenteID != null & this.activo.ComponenteID > 0 ? "'" + this.activo.ComponenteID + "'" : "NULL");

                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana
            }
            catch
            {
                #region SQL
                int flag = oUDT.Update();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL
            }
        }

        public void Deactivate()
        {
            try
            {
                #region SQL || HANA
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET U_EsActivo = '0' WHERE ""Code"" = '{2}'", this.NombreTabla, this.oUDT.Code);
                this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || HANA

                #region SQL
                //this.oUDT.UserFields.Fields.Item("U_EsActivo").Value = 0;
                //Update();
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar desacticar un activo. " + ex.Message);
            }
        }

        #endregion

        #region Metodos Auxiliares

        public Recordset GetComponentes()
        {
            this.Parametro = String.Empty;

            this.Parametro = String.Format("SELECT * FROM {0} WHERE U_ComponenteID IS NULL", this.NombreTabla);
            this.Insertar.DoQuery(this.Parametro);

            return this.Insertar;
        }

        public bool SetUDTByItemCode()
        {
            return this.oUDT.GetByKey(this.ItemCode);
        }

        public bool SetUDTByActivoID(int id)
        {
            var rs = GetAllAssets();
            rs.MoveFirst();
            for (int i = 0; i < rs.RecordCount; i++)
            {
                var flag = rs.Fields.Item("U_ActivoID").Value;
                if (Convert.ToInt32(rs.Fields.Item("U_ActivoID").Value) == id)
                {
                    return this.oUDT.GetByKey(rs.Fields.Item("Code").Value.ToString());
                }
                rs.MoveNext();
            }
            this.oUDT.Code = string.Empty;
            return false;
        }

        public String GetItemCodeByID(int? activoid)
        {
            String ItemCode;
            this.Parametro = String.Empty;
            this.Parametro = String.Format("SELECT Code FROM {0} WHERE U_VehiculoID = '{1}'", this.NombreTabla, activoid);
            this.Insertar.DoQuery(Parametro);
            ItemCode = Insertar.Fields.Item("Code").Value;
            return ItemCode;
        }

        public String GetPlacaByActivoID(int? activoid)
        {
            String Placa;
            this.Parametro = String.Empty;
            this.Parametro = String.Format("SELECT U_Placa FROM {0} WHERE U_ActivoID = '{1}'", this.NombreTabla, activoid);
            this.Insertar.DoQuery(Parametro);
            Placa = Insertar.Fields.Item("U_Placa").Value;
            return Placa;
        }

        public int GetTotalActivos()
        {
            return GetAllAssets().RecordCount;
        }

        public Recordset GetAllAssets()
        {
            Recordset rs = (Recordset)this.company.GetBusinessObject(BoObjectTypes.BoRecordset);
            rs.DoQuery("select * from " + this.NombreTabla);
            return rs;
        }

        public Recordset Consultar(Caja activo)
        {
            try
            {
                #region Validacion de Objetos
                if (activo == null) activo = new Caja();
                if (activo.TipoActivos == null) activo.TipoActivos = new TipoActivos();
                if (activo.SubPropietario == null) activo.SubPropietario = new Sucursal();
                if (activo.Propietario == null) activo.Propietario = new Empresa();
                #endregion
                Recordset rs = this.company.GetBusinessObject(BoObjectTypes.BoRecordset);
                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(@" SELECT at.""Code"",at.""Name"",at.U_ActivoID,at.U_NumeroEconomico,at.U_Observaciones, at.U_EsActivo, at.U_PropietarioID, at.U_VehiculoID, at.U_Posicion, at.U_SucursalID, at.U_Tipo, at.U_Marca, at.U_Modelo, at.U_Placa, at.U_Descripcion, at.U_TipoActivoID, at.U_Sincronizado, at.U_UUID  ");
                query.Append(" from " + this.NombreTabla +" ");

                if (activo.CajaID != null)
                {
                    where.Append(string.Format("and at.U_activoID = {0} ", activo.CajaID));
                }
                if (!string.IsNullOrEmpty(activo.NumeroEconomico))
                    where.Append(string.Format("and at.U_NumeroEconomico = '{0}'", activo.NumeroEconomico));
                if (!string.IsNullOrEmpty(activo.Observaciones))
                    where.Append(string.Format("and at.U_Observaciones like '%{0}%' ", activo.Observaciones));
                if (activo.Propietario != null && activo.Propietario.EmpresaID != null)
                    where.Append(string.Format("and at.U_PropietarioID = {0} ", activo.Propietario.EmpresaID));
                if (activo.EstaActivo != null)
                    where.Append(string.Format("and at.U_EsActivo = {0} ", Convert.ToInt32(activo.EstaActivo)));
                if (activo.MantenibleID != null)
                    where.Append(string.Format("and at.U_VehiculoID = {0} ", activo.MantenibleID));
                if (activo.Posicion != null)
                    where.Append(string.Format("and at.U_Posicion = {0} ", activo.Posicion));
                if (activo.SubPropietario != null && activo.SubPropietario.SucursalID != null)
                    where.Append(string.Format("and at.U_SucursalID = {0} ", activo.SubPropietario.SucursalID));
                if (!string.IsNullOrEmpty(activo.Tipo))
                    where.Append(string.Format("and at.U_Tipo = '{0}' ", activo.Tipo));
                if (!string.IsNullOrEmpty(activo.Marca))
                    where.Append(string.Format("and at.U_Marca = '{0}' ", activo.Marca));
                if (!string.IsNullOrEmpty(activo.Modelo))
                    where.Append(string.Format("and at.U_Modelo = '{0}' ", activo.Modelo));
                if (!string.IsNullOrEmpty(activo.Placa))
                    where.Append(string.Format("and at.U_Placa like '%{0}%' ", activo.Placa));
                if (!string.IsNullOrEmpty(activo.Descripcion))
                    where.Append(string.Format("and at.U_Descripcion like '%{0}%' ", activo.Descripcion));
                if (activo.TipoActivos != null && activo.TipoActivos.TipoActivoID != null)
                    where.Append(string.Format("and at.U_SucursalID = {0} ", activo.SubPropietario.SucursalID));
                if (Sincronizado != null)
                    where.Append(string.Format(" and op.U_Sincronizado = '{0}' ", Sincronizado));
                if (UUID != null)
                    where.Append(string.Format(" and op.U_UUID = '{0}' ", UUID));
                
                string filtros = where.ToString().Trim();
                if (filtros.Length > 0)
                {
                    if (filtros.StartsWith("and"))
                        filtros = filtros.Substring(3);
                    query.Append(" where " + filtros);
                }
                query.Append(" ORDER BY vh.U_Nombre, U_activoID ASC ");
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public bool HasActivos(Recordset rs)
        {
            if (rs.RecordCount >= 1)
                return true;
            return false;
        }

        public List<Caja> RecordSetToListActivos(Recordset rs)
        {
            try
            {
                List<Caja> vehicles = new List<Caja>();
                rs.MoveFirst();
                for (int i = 0; i < rs.RecordCount; i++)
                {
                    this.ItemCode = rs.Fields.Item("Code").Value;
                    this.oUDT.GetByKey(this.ItemCode);
                    this.UDTToActivo();
                    var tmp = (Caja)this.activo.Clone();
                    vehicles.Add(tmp);
                    rs.MoveNext();
                }
                return vehicles;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void SetSucursalToActivo(int? sucursalID)
        {
            try
            {
                this.oUDT.UserFields.Fields.Item("U_SucursalID").Value = sucursalID;
                this.Update();
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public Recordset ActualizarCode(Caja ca)
        {
            try
            {
                Recordset rs = this.company.GetBusinessObject(BoObjectTypes.BoRecordset);

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(" UPDATE " + this.NombreTabla + " SET U_ActivoID = '" + ca.CajaID +
                    "', U_Sincronizado = " + Sincronizado);

                if (UUID != null)
                    where.Append(string.Format(" and U_UUID like '%{0}%' ", UUID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #endregion
    }
}
