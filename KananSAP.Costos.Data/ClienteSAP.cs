﻿#region C#
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Security.Cryptography.X509Certificates;
#endregion C#

#region Kananfleet
using Kanan.Costos.BO2;
#endregion Kananfleet

#region SAP
using SAPbobsCOM;
#endregion SAP

namespace KananSAP.Costos.Data
{
    public class ClienteSAP
    {
        #region Atributos
        public Cliente cliente { get; set; }
        public SAPbobsCOM.BusinessPartners oBpartnet { get; set; }
        private Company Company;
        public String ItemCode;
        public int? EmpresaID;

        //SQL || Hana
        bool sql;
        Recordset Insertar;
        String Parametro;
        private String NombreTabla;
        public String NombreTablaClienteSAP;
        #endregion Atributos

        #region Constructor
        public ClienteSAP(SAPbobsCOM.Company company)
        {
            this.Company = company;
            this.cliente = new Cliente();
            this.oBpartnet = (SAPbobsCOM.BusinessPartners)company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
            try
            {
                this.Insertar = (Recordset)this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.sql = Company.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.NombreTabla = sql ? "[@VSKF_CLIENTE]" : @"""@VSKF_CLIENTE""";
                this.NombreTablaClienteSAP = this.NombreTabla;
            }
            catch (Exception ex)
            {
                throw new Exception("No se ha instanciado ClienteSAP.cs. Error: " + ex.Message);
            }
        }
        #endregion Constructor

        #region Métodos

        public bool ExistClient(String cardcode)
        {
            try
            {
                #region SQL || Hana
                Parametro = String.Empty;
                Parametro = String.Format("SELECT * FROM {0} where U_CardCode = '{1}'", this.NombreTabla, cardcode);
                this.Insertar.DoQuery(Parametro);
                return Insertar.RecordCount > 0 ? true : false;
                #endregion SQL || Hana
            }
            catch (Exception ex)
            {
                int errornum;
                string errormns;
                Company.GetLastError(out errornum, out errormns);
                throw new Exception("Error al intentar consultar un cliente. Error: " + errornum + " - " + errormns);
            }
        }

        public int GetClienteIDByCardCore(String cardcode)
        {
            int ClienteID;
            try
            {
                #region SQL || Hana
                Parametro = String.Empty;
                Parametro = String.Format("SELECT U_ClienteID FROM {0} where U_CardCode = '{1}'", this.NombreTabla, cardcode);
                this.Insertar.DoQuery(Parametro);
                ClienteID = Insertar.Fields.Item("U_ClienteID").Value;
                if (ClienteID > 0)
                    return ClienteID;
                else
                    throw new Exception("No se encontró un ID para un cliente registrado. ");
                #endregion SQL || Hana
            }
            catch (Exception ex)
            {
                int errornum;
                string errormns;
                Company.GetLastError(out errornum, out errormns);
                throw new Exception("Error al obtener el identificador del cliente. Error: " + errornum + " - " + errormns);
            }
        }

        public Cliente GetClienteByCardCode(String cardcode)
        {
            Cliente cli = new Cliente();
            try
            {
                #region SQL || Hana
                Parametro = String.Empty;
                Parametro = String.Format("SELECT * FROM {0} where U_CardCode = '{1}'", this.NombreTabla, cardcode);
                this.Insertar.DoQuery(Parametro);
                return Insertar.RecordCount > 0 ? RecordSetToCliente(Insertar) : null;
                #endregion SQL || Hana
            }
            catch (Exception ex)
            {
                int errornum;
                string errormns;
                Company.GetLastError(out errornum, out errormns);
                throw new Exception("No se encontró cliente. Error: " + errornum + " - " + errormns);
            }
        }

        public void Insert()
        {
            try
            {
                #region SQL || Hana
                this.Parametro = String.Empty;

                this.Parametro = String.Format(@"INSERT INTO {0} (""Code"", ""Name"", U_ClienteID, U_CardCode, U_Nombre, U_NombreExt, U_RFC, U_Telefono, U_TelMovil, U_Correo, U_EmpresaID, U_EsActivo) ", this.NombreTabla);

                this.Parametro += String.Format("VALUES('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}')", cliente.ClienteID, cliente.ClienteID, cliente.ClienteID, cliente.CardCode, cliente.Nombre, cliente.NombreExtranjero, cliente.RFC, cliente.Telefono, cliente.TelefonoMovil, cliente.Correo, cliente.EmpresaID, /*cliente.EsActivo = */ "1");

                Insertar.DoQuery(Parametro);
                #endregion SQL || Hana
            }
            catch (Exception ex)
            {
                int errornum;
                string errormns;
                Company.GetLastError(out errornum, out errormns);
                throw new Exception("Error al intentar insertar un cliente. Error: " + errornum + " - " + errormns);
            }
        }

        public void Update()
        {
            try
            {
                #region SQL || Hana
                this.Parametro = String.Empty;

                this.Parametro = String.Format(@"UPDATE {0} SET ""Code"" = '{1}', ""Name"" = '{2}', U_ClienteID = '{3}', U_CardCode = '{4}', U_Nombre = '{5}', U_NombreExt = '{6}', U_RFC = '{7}', U_Telefono = '{8}', U_TelMovil = '{9}', U_Correo = '{10}', U_EmpresaID = '{11}', U_EsActivo = '{12}' WHERE Code = '{1}'", this.NombreTabla, cliente.ClienteID, cliente.ClienteID, cliente.ClienteID, cliente.CardCode, cliente.Nombre, cliente.NombreExtranjero, cliente.RFC, cliente.Telefono, cliente.TelefonoMovil, cliente.Correo, cliente.EmpresaID, Convert.ToInt32(cliente.EsActivo));

                Insertar.DoQuery(Parametro);
                #endregion SQL || Hana
            }
            catch(Exception ex)
            {
                int errornum;
                string errormns;
                Company.GetLastError(out errornum, out errormns);
                throw new Exception("Error al intentar actualizar un cliente. Error: " + errornum + " - " + errormns);
            }
        }

        public void Desactivate(String ID)
        {
            try
            {
                if (!String.IsNullOrEmpty(ID))
                    throw new Exception("No se ha encontrado el identificador. Cliente no desactivado. ");

                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format(@"UPDATE {0} SET U_EsActivo = '0' WHERE ""Code"" = '{1}'", NombreTabla, ID);
                this.Insertar.DoQuery(Parametro);
                #endregion SQL || Hana
            }
            catch (Exception ex)
            {
                int errornum;
                string errormns;
                Company.GetLastError(out errornum, out errormns);
                throw new Exception("No se ha desactivado un cliente. Error: " + errornum + " - " + errormns);
            }
        }

        public void Delete(string ID)
        {
            try
            {
                if (String.IsNullOrEmpty(ID))
                    throw new Exception("No se ha encontrado el indentificador. Cliente no eliminado. ");
                
                #region SQL || Hana
                this.Parametro = String.Empty;
                this.Parametro = String.Format("DELETE FROM {0} WHERE U_ClienteID = '{1}'", this.NombreTabla, ID);
                this.Insertar.DoQuery(Parametro);
                #endregion SQL || Hana
            }
            catch (Exception ex)
            {
                int errornum;
                string errormns;
                Company.GetLastError(out errornum, out errormns);
                throw new Exception("Error al intentar eliminar un cliente. Error: " + errornum + " - " + errormns);
            }
        }

        #endregion Métodos

        #region MétodosAuxiliares
        public Cliente RecordSetToCliente(Recordset rcliente)
        {
            try
            {
                Cliente cli = new Cliente();
                cli.ClienteID = rcliente.Fields.Item("U_ClienteID").Value;
                cli.CardCode = rcliente.Fields.Item("U_CardCode").Value;
                cli.Nombre = rcliente.Fields.Item("U_Nombre").Value;
                cli.NombreExtranjero = rcliente.Fields.Item("U_NombreExt").Value;
                cli.RFC = rcliente.Fields.Item("U_RFC").Value;
                cli.Telefono = rcliente.Fields.Item("U_Telefono").Value;
                cli.TelefonoMovil = rcliente.Fields.Item("U_TelMovil").Value;
                cli.Correo = rcliente.Fields.Item("U_Correo").Value;
                cli.EmpresaID = rcliente.Fields.Item("U_EmpresaID").Value;
                cli.EsActivo = rcliente.Fields.Item("U_EsActivo").Value;
                return cli;
            }
            catch (Exception ex)
            {
                throw new Exception("No se tranfirieron correctamente los datos del cliente. ClienteSAP.cs" + ex.Message);
            }
        }
        #endregion MétodosAuxiliares
    }
}
