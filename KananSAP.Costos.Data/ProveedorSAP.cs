﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Security.Cryptography.X509Certificates;
using Kanan.Costos.BO2;
using SAPbobsCOM;
using System.Text;

namespace KananSAP.Costos.Data
{
    public class ProveedorSAP
    {
        #region Atributos
        public Proveedor proveedor { get; set; }
        public UserTable oUDT { get; set; }
        public SAPbobsCOM.BusinessPartners oBpartner { get; set; }
        public string ItemCode { get; set; }
        private Company Company;
        public String CardCode;
        public String CardName;
        private int EmpresaID;

        //Migracion Hana
        Recordset Insertar;
        String Parametro;
        private String NombreTabla;
        bool EsSQL;

        //Se agregó para ser visible desde cualquier lugar consulta.
        public String NombreTablaProveedorSAP = String.Empty;

        public bool ExistUDT()
        {
            return oUDT.GetByKey(this.ItemCode);
        }
        #endregion

        #region Constructor
        public ProveedorSAP(SAPbobsCOM.Company company)
        {
            this.Company = company;
            this.proveedor = new Proveedor();
            this.oBpartner = (SAPbobsCOM.BusinessPartners)company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oBusinessPartners);
            try
            {
                this.oUDT = company.UserTables.Item("VSKF_PROVEEDOR");

                //Migración HANA
                this.Insertar = (Recordset)this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                this.EsSQL = Company.DbServerType.ToString().Contains("MSSQL") ? true : false;
                this.NombreTabla = EsSQL ? "[@VSKF_PROVEEDOR]" : @"""@VSKF_PROVEEDOR""";
                //Se maneja en una variable pública para poder utilizar desde cualquier método.
                this.NombreTablaProveedorSAP = this.NombreTabla;
            }
            catch (Exception)
            {
                this.oUDT = null;
            }
        }
        #endregion

        #region Methods

        #region Metodos interface SAP - Kananfleet
        public bool SAPToProveedor()
        {
            if (!this.oUDT.GetByKey(ItemCode)) return false;
            UDTToProveedor();
            return true;
        }

        public void UDTToProveedor()
        {
            this.proveedor = new Proveedor();

            this.proveedor.ProveedorID = (int)this.oUDT.UserFields.Fields.Item("U_ProveedorID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_ProveedorID").Value;
            this.proveedor.Nombre = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Nombre").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Nombre").Value;
            //this.proveedor.Direprion = GetAddres();
            this.proveedor.Telefono = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Telefono").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Telefono").Value;
            //this.proveedor.Contactos = 
            this.proveedor.Correos = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Correos").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Correos").Value;
            this.proveedor.CUP = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_CUP").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_CUP").Value;
        }

        public void ProveedorToSAP()
        {
            ProveedorToBP();
            ProveedorToUDT();
        }

        private void ProveedorToBP()
        {
            oBpartner.CardCode = this.ItemCode;
            oBpartner.CardName = this.proveedor.Nombre?? string.Empty;
            oBpartner.Phone1 = this.proveedor.Telefono?? string.Empty;
            oBpartner.EmailAddress = this.proveedor.Correos ?? string.Empty;
            oBpartner.FederalTaxID = this.proveedor.CUP ?? string.Empty;

            //Salva CardCode y CardName de proveedores SAP.
            this.CardCode = oBpartner.CardCode;
            this.CardName = oBpartner.CardName;
        }

        public void ProveedorToUDT()
        {
            oUDT.Code = this.ItemCode;
            oUDT.Name = this.proveedor.Nombre;
            this.oUDT.UserFields.Fields.Item("U_ProveedorID").Value = this.proveedor.ProveedorID;
            this.oUDT.UserFields.Fields.Item("U_Nombre").Value = this.proveedor.Nombre;
            this.oUDT.UserFields.Fields.Item("U_Telefono").Value = this.proveedor.Telefono??"";
            this.oUDT.UserFields.Fields.Item("U_Correos").Value = this.proveedor.Correos??"";
            this.oUDT.UserFields.Fields.Item("U_CUP").Value = this.proveedor.CUP;
        }

        public void SetPropietarioToUDT(int? Propietario)
        {
            try
            {
                this.EmpresaID = Convert.ToInt32(Propietario);
                //Se comentó para manejar una variable globan con el ID de la empresa;
                //this.oUDT.UserFields.Fields.Item("U_Propietario").Value = EmpresaID;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar estableser el Identificador del Propietario con el Proveedor. " + ex.Message);
            }
        }

        #endregion

        #region Acciones
        public void InsertUDT()
        {
            try
            {
                ProveedorToBP();

                #region SQL || Hana
                //this.Parametro = String.Empty;
                //this.Parametro = String.Format("INSERT INTO {0} VALUES('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}')", this.NombreTabla, this.CardCode, this.CardName, this.proveedor.ProveedorID, this.proveedor.Nombre, this.proveedor.Direccion, this.proveedor.Telefono, this.proveedor.Contactos, this.proveedor.Correos, this.EmpresaID, this.proveedor.CUP);

                //this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana

                #region SQL
                int flag = oUDT.Add();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    Company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar insertar un proveedor. " + ex.Message);
            }
        }

        public void InsertComplete()
        {
            try
            {
                ProveedorToBP();

                //String GrupoID = GetGroupPartner();

                //#region OCRD

                //#region SQL || Hana
                //this.Parametro = String.Empty;
                //this.Parametro = String.Format("INSERT INTO OCRD (CardCode, CardName, GroupCode, Phone1, E_Mail, LicTradNum) VAULES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')", this.oBpartner.CardCode, this.oBpartner.CardName, GrupoID, oBpartner.Phone1, this.oBpartner.EmailAddress, this.oBpartner.FederalTaxID);

                //this.Insertar.DoQuery(this.Parametro);
                //#endregion SQL || Hana

                //#region SQL
                int flag = oBpartner.Add();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    Company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                //#endregion SQL

                //#endregion OCRD

                //#region SQL || Hana
                //this.Parametro = String.Empty;
                //this.Parametro = String.Format("INSERT INTO {0} VALUES('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}')", this.NombreTabla, this.CardCode, this.CardName, this.proveedor.ProveedorID, this.proveedor.Nombre, this.proveedor.Direccion, this.proveedor.Telefono, this.proveedor.Contactos, this.proveedor.Correos, this.EmpresaID, this.proveedor.CUP);

                //this.Insertar.DoQuery(this.Parametro);
                //#endregion SQL || Hana

                //#region SQL
                flag = oUDT.Add();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    Company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                //#endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception("Error al inentar insertar dos proveedores. " + ex.Message);
            }
        }

        public void UpdateUDT()
        {
            try
            {
                #region SQL || Hana
                //this.Parametro = String.Empty;
                //this.Parametro = String.Format(@"UPDATE {0} SET ""Code"" = '{1}', ""Name"" = '{2}', U_ProveedorID= '{3}', U_Nombre = '{4}', U_Direccion = '{5}', U_Telefono = '{6}', U_Contactos = '{7}', U_Correos = '{8}', U_Propietario = '{9}', U_CUP = '{10}' WHERE Code = '{1}'", this.NombreTabla, this.CardCode, this.CardName, this.proveedor.ProveedorID, this.proveedor.Nombre, this.proveedor.Direccion, this.proveedor.Telefono, this.proveedor.Contactos, this.proveedor.Correos, this.EmpresaID, this.proveedor.CUP);

                //this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana

                #region SQL
                var flag = oUDT.Update();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    Company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar actualuizar un proveedor. " + ex.Message);
            }
        }

        public void UpdateComplete()
        {
            try
            {
                //#region OCRD

                //String GrupoID = GetGroupPartner();

                //#region SQL || Hana
                //this.Parametro = String.Empty;
                //this.Parametro = String.Format("UPDATE OCRD SET CardCode = '{0}', CardName = '{1}', GroupCode = '{2}', Phone1 = '{3}', E_Mail = '{4}', LicTradNum = '{5}' WHERE CardCode = '{0}'", this.CardCode, this.oBpartner.CardCode, this.oBpartner.CardName, GrupoID, oBpartner.Phone1, this.oBpartner.EmailAddress, this.oBpartner.FederalTaxID);

                //this.Insertar.DoQuery(this.Parametro);
                //#endregion SQL || Hana

                //#region SQL
                var flag = oBpartner.Update();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    Company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                //#endregion SQL

                //#endregion OCRD

                //#region SQL || Hana
                //this.Parametro = String.Empty;
                //this.Parametro = String.Format("UPDATE {0} SET Code = '{1}', Name = '{2}', U_ProveedorID= '{3}', U_Nombre = '{4}', U_Direccion = '{5}', U_Telefono = '{6}', U_Contactos = '{7}', U_Correos = '{8}', U_Propietario = '{9}', U_CUP = '{10}' WHERE Code = '{1}'", this.NombreTabla, this.CardCode, this.CardName, this.proveedor.ProveedorID, this.proveedor.Nombre, this.proveedor.Direccion, this.proveedor.Telefono, this.proveedor.Contactos, this.proveedor.Correos, this.EmpresaID, this.proveedor.CUP);

                //this.Insertar.DoQuery(this.Parametro);
                //#endregion SQL || Hana

                #region SQL
                flag = oUDT.Update();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    Company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intetar actualizar dos proveedores. " + ex.Message);
            }
        }

        public void RemoveUDT()
        {
            try
            {
                //string ID = String.Empty;
                //if (!String.IsNullOrEmpty(oUDT.Code))
                //    ID = oUDT.Code;
                //else
                //{
                //    throw new Exception("Erron al intentar obtener identificador. ");
                //}

                #region SQL || Hana
                //this.Parametro = String.Empty;
                //this.Parametro = String.Format(@"DELETE FROM {0} WHERE ""Code"" = '{1}'", this.NombreTabla, ID);

                //this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana

                #region SQL
                var flag = oUDT.Remove();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    Company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar eliminar un proveedor. " + ex.Message);
            }
        }

        public void RemoveComplete()
        {
            try
            {
                #region OUBR

                //String CardCode = this.oBpartner.CardCode;

                #region SQL || Hana
                //this.Parametro = String.Empty;
                //this.Parametro = String.Format("DELETE FROM OCRD WHERE CardCode = '{0}'", CardCode);

                //this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana

                #region SQL
                var flag = oBpartner.Remove();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    Company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL

                #endregion OUBR

                string ID = String.Empty;
                if (!String.IsNullOrEmpty(oUDT.Code))
                    ID = oUDT.Code;
                else
                {
                    throw new Exception("Erron al intentar obtener identificador. ");
                }

                #region SQL || Hana
                //this.Parametro = String.Empty;
                //this.Parametro = String.Format("DELETE FROM {0} WHERE Code = '{1}'", this.NombreTabla, ID);

                //this.Insertar.DoQuery(this.Parametro);
                #endregion SQL || Hana

                #region SQL
                 flag = oUDT.Remove();
                if (flag != 0)
                {
                    int errornum;
                    string errormsj;
                    Company.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
                #endregion SQL
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar remover dos proveedores. " + ex.Message);
            }
        }

        #endregion

        #region Metodos Auxiliares
        public List<Proveedor> GetProveedoresKF()
        {
            List<Proveedor> result = new List<Proveedor>();
            Recordset rs = (Recordset)this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            rs.DoQuery(@"select * from OCRG where Upper(""GroupName"") = 'PROVEEDORES KF'");
            if (rs.RecordCount > 0)
            {
                rs.MoveFirst();
                string groupcode = rs.Fields.Item("GroupCode").Value.ToString();
                rs.DoQuery(@"select * from OCRD where ""GroupCode"" = " + groupcode);
                for (int i = 0; i < rs.RecordCount; i++)
                {
                    this.ItemCode = rs.Fields.Item("CardCode").Value.ToString();
                    this.oUDT.GetByKey(this.ItemCode);
                    UDTToProveedor();
                    var temp = (Proveedor)this.proveedor.Clone();
                    if(temp!=null && temp.ProveedorID !=null)
                        result.Add(temp);
                    rs.MoveNext();
                }
            }
            return result;
        }

        public String GetGroupPartner()
        {
            try
            {
                String GroupCode = String.Empty;
                this.Insertar.DoQuery(@"select * from OCRG where Upper(""GroupName"") = 'PROVEEDORES KF'");
                if (Insertar.RecordCount > 0)
                {
                    this.Insertar.MoveFirst();
                    GroupCode = this.Insertar.Fields.Item("GroupCode").Value.ToString();
                }

                return GroupCode;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar obtener el GroupCode. " + ex.Message);
            }
        }

        public Recordset Consultar(Proveedor pr)
        {
            try
            {
                Recordset rs = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                if (pr == null) pr = new Proveedor();

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(" SELECT * from " + this.NombreTabla + " as pr ");

                if (pr.ProveedorID != null && pr.ProveedorID > 0)
                    where.Append(string.Format(" and pr.U_ProveedorID = {0} ", pr.ProveedorID));
                if (!string.IsNullOrEmpty(pr.Nombre))
                    where.Append(string.Format(" and pr.U_Nombre = '{0}' ", pr.Nombre));
                //if (pr.Direccion != null)
                //    where.Append(string.Format(" and pr.[U_Direccion] = '{0}' ", pr.Direccion));
                if (!string.IsNullOrEmpty(pr.Telefono))
                    where.Append(string.Format(" and pr.U_Telefono = '{0}' ", pr.Telefono));
                //if (!string.IsNullOrEmpty(pr.Contactos))
                //    where.Append(string.Format(" and pr.[U_Contactos] = '{0}' ", pr.Contactos));
                if (!string.IsNullOrEmpty(pr.Correos))
                    where.Append(string.Format(" and pr.U_Correos = '{0}' ", pr.Correos));
                //if (!string.IsNullOrEmpty(pr.Telefono))
                //    where.Append(string.Format(" and pr.[U_Propietario] = '{0}' ", pr.Telefono));
                if (!string.IsNullOrEmpty(pr.CUP))
                    where.Append(string.Format(" and pr.U_CUP = '{0}' ", pr.CUP));
                
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(3);
                    query.Append(" where " + filtro);
                }
                query.Append(" order by pr.U_ProveedorID desc ");
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Proveedor> RecordSetToListCargaCombustible(Recordset rs)
        {
            var result = new List<Proveedor>();
            for (int i = 0; i < rs.RecordCount; i++)
            {
                this.ItemCode = rs.Fields.Item("Code").Value.ToString();
                this.oUDT.GetByKey(this.ItemCode);
                UDTToProveedor();
                var temp = (Proveedor) this.proveedor.Clone();
                result.Add(temp);
            }
            return result;
        }

        public bool SetUDTByProveedorID(int id)
        {
            Recordset rs = (Recordset)this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            rs.DoQuery("select * from " + this.NombreTabla + " where U_ProveedorID = " + id);
            if (rs.RecordCount > 0)
            {
                rs.MoveFirst();
                this.ItemCode = rs.Fields.Item("Code").Value.ToString();
                return this.oUDT.GetByKey(this.ItemCode); 
            }
            
            this.oUDT.Code = string.Empty;
            return false;
        }
        #endregion

        #endregion
    }
}
