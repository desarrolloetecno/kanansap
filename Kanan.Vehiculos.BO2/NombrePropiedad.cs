﻿using System;
using Kanan.Operaciones.BO2;
namespace Kanan.Vehiculos.BO2
{
     [Serializable]
    public class NombrePropiedad:ICloneable
    {
        public int? NombrePropiedadID { get; set; }
        public Empresa Propietario { get; set; }
        public Sucursal SubPropietario { get; set; }
        /// <summary>
        /// Nombre principal de la propiedad
        /// </summary>
        public string Nombre { get; set; }
        public bool? EsActivo { get; set; }

        public NombrePropiedad()
        {
            this.EsActivo = true;
        }
        public object Clone()
        {
           NombrePropiedad nombrePropiedad = new NombrePropiedad();
           nombrePropiedad.NombrePropiedadID = this.NombrePropiedadID;
           nombrePropiedad.Nombre = this.Nombre;
           if (this.Propietario != null)
           {
               nombrePropiedad.Propietario =(Empresa)this.Propietario.Clone();
           }
           if (this.SubPropietario != null)
           {
               nombrePropiedad.SubPropietario =(Sucursal)this.SubPropietario.Clone();
           }

           return nombrePropiedad;
        }
    }
}
