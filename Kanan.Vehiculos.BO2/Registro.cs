﻿using System;


namespace Kanan.Vehiculos.BO2
{
    [Serializable]

    public class Registro
    {

        public int? RegistroID { get; set; }

        public decimal? DistanciaRecorrida { get; set; }

        public DateTime? FechaRegistro { get; set; }

        public DateTime? FechaCaptura { get; set; }

        public decimal? Latitud { get; set; }

        public decimal? Longitud { get; set; }

        public decimal? Altitud { get; set; }
    }
}
