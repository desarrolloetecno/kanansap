﻿using System;
using Kanan.Llantas.BO2;
using Kanan.Operaciones.BO2;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Kanan.Costos.BO2;
namespace Kanan.Vehiculos.BO2
{
    [Serializable]
    [DataContract]
    public class Caja : ILlanta, ICloneable, IMantenible
    {
        /// <summary>
        /// int? Representa el ID del Activo/Caja
        /// </summary>
        [DataMember]
        public int? CajaID { get; set; }
        //[DataMember]
        //public string NumEconomico { get; set; }
        //[DataMember]
        //public decimal? KilometroRecorrido { get; set; }
        /// <summary>
        /// string Observaciones, debe ser igual al campo descripción.
        /// </summary>
        [DataMember]
        public string Observaciones { get; set; }
        /// <summary>
        /// bool? Representa si un activo puede ser visible o utilizado.
        /// </summary>
        [DataMember]
        public bool? EstaActivo { get; set; }
        /// <summary>
        /// int? Representa la posición en caso de ser una Caja
        /// </summary>
        [DataMember]
        public int? Posicion { get; set; }
        /// <summary>
        /// Empresa Representa el objeto Empresa (propietario)
        /// </summary>
        [DataMember]
        public Empresa Propietario { get; set; }
        //[DataMember]
        //public List<Refaccion> llantasRefaccion { get; set; }
        /// <summary>
        /// Sucursal Representa el objeto sucursal (subropietario)
        /// </summary>
        [DataMember]
        public Sucursal SubPropietario { get; set; }
        /// <summary>
        /// string Representa el modelo del activo
        /// </summary>
        [DataMember]
        public string Modelo { get; set; }
        /// <summary>
        /// string Representa la placa del activo en caso de tener una
        /// es decir, en caso de ser una activo de tipo "Caja".
        /// </summary>
        [DataMember]
        public string Placa { get; set; }
        /// <summary>
        /// string Representa un tipo en referencia a la denominación del usuario.
        /// </summary>
        [DataMember]
        public string Tipo { get; set; }
        /// <summary>
        /// Representa la marca del activo.
        /// </summary>
        [DataMember]
        public string Marca { get; set; }
        /// <summary>
        /// int? Representa el año de fabricación del activo.
        /// </summary>
        [DataMember]
        public int? Anio { get; set; }
        /// <summary>
        /// int? *Sin descripción*
        /// </summary>
        [DataMember]
        public int? Detalle { get; set; }
        /// <summary>
        /// int? Representa la póliza de seguro en caso de contar con ella.
        /// </summary>
        [DataMember]
        public int? Poliza { get; set; }
        /// <summary>
        /// DateTime? Representa la vigencia de la póliza.
        /// </summary>
        [DataMember]
        public DateTime? VencimientoPoliza { get; set; }
        /// <summary>
        /// int? Representa la cantidad en días antes para alertar al usuario
        /// sobre la vigencia de su póliza.
        /// </summary>
        [DataMember]
        public int? AvisoPoliza { get; set; }
        /// <summary>
        /// int? Representa la plaza
        /// </summary>
        [DataMember]
        public int? Plazas { get; set; }
        /// <summary>
        /// int? Representa el rendimiento de combustible del activo (En caso de ser de tipo Caja).
        /// </summary>
        [DataMember]
        public int? RendComb { get; set; }
        /// <summary>
        /// int? Representa el redimiento de combustible del activo con carga (En caso de ser de tipo Caja).
        /// </summary>
        [DataMember]
        public int? RendCarga { get; set; }
        /// <summary>
        /// string Representa una breve descripción sobre el activo.
        /// </summary>
        [DataMember]
        public string Descripcion { get; set; }
        /// <summary>
        /// TipoActivos Representa un tipo de activo en referencia a VSKF_TIPOACTIVO
        /// </summary>
        [DataMember]
        public TipoActivos TipoActivos { get; set; }
        /// <summary>
        /// TipoChasis representa un tipo chasis de activo en referencia a VSKF_TIPOACTIVO
        /// </summary>
        [DataMember]
        public int? TipoChasis { get; set; }
        /// <summary>
        /// Proveedor Representa el objeto proveedor.
        /// </summary>
        [DataMember]
        public Proveedor Proveedor { get; set; }
        /// <summary>
        /// string Representa el número de serie del activo.
        /// </summary>
        [DataMember]
        public string NoSerie { get; set; }
        /// <summary>
        /// DateTime? Representa la fecha de compra del activo.
        /// </summary>
        [DataMember]
        public DateTime? FechaCompra { get; set; }
        /// <summary>
        /// int? Representa la sincronización del registro en diversas plataformas.
        /// </summary>
        [DataMember]
        public int? Sincronizado { get; set; }
        /// <summary>
        /// string Representa el UUID de las tablas SQL.
        /// </summary>
        [DataMember]
        public string UUID { get; set; }
        /// <summary>
        /// double?. Representa el costo de compra del vehículo.
        /// </summary>
        [DataMember]
        public double? costo { get; set; }

        public string FechaAlta{get;set;}
        public string FechaBaja { get; set; }

        public Caja()
        {
            this.EstaActivo = true;
        }

        #region Comentado
        //Llanta punto1 { get; set; }
        //Llanta punto2 { get; set; }
        //Llanta punto3 { get; set; }
        //Llanta punto4 { get; set; }
        //Llanta punto5 { get; set; }
        //Llanta punto6 { get; set; }
        //Llanta punto7 { get; set; }
        //Llanta punto8 { get; set; }
        //Llanta punto9 { get; set; }
        //Llanta punto10 { get; set; }
        //Llanta punto11 { get; set; }
        //Llanta punto12 { get; set; }
        //Llanta punto13 { get; set; }
        //Llanta punto14 { get; set; }
        //Llanta punto15 { get; set; }
        //Llanta punto16 { get; set; }
        //Llanta punto17 { get; set; }
        //Llanta punto18 { get; set; }
        #endregion Comentado

        public object Clone()
        {
            Caja cj = new Caja();
            cj.CajaID = this.CajaID;
            cj.NumeroEconomico = this.NumeroEconomico;
            cj.EstaActivo = this.EstaActivo;
            cj.Observaciones = this.Observaciones;
            cj.Anio = this.Anio;
            cj.Detalle = this.Detalle;
            cj.Poliza = this.Poliza;
            cj.VencimientoPoliza = this.VencimientoPoliza;
            cj.AvisoPoliza = this.AvisoPoliza;
            cj.Plazas = this.Plazas;
            cj.RendComb = this.RendComb;
            cj.RendCarga = this.RendCarga;
            cj.Posicion = this.Posicion;
            cj.Tipo = this.Tipo;
            cj.Modelo = this.Modelo;
            cj.Marca = this.Marca;
            cj.Placa = this.Placa;
            cj.Descripcion = this.Descripcion;
            cj.MantenibleID = this.MantenibleID;
            if (this.Propietario != null)
            {
                cj.Propietario = this.Propietario;
            }
            if (this.SubPropietario != null)
            {
                cj.SubPropietario = (Sucursal)this.SubPropietario.Clone();
            }
            if (this.TipoActivos != null)
                cj.TipoActivos = (TipoActivos)this.TipoActivos.Clone();
            if (this.Proveedor != null)
                cj.Proveedor = (Proveedor)this.Proveedor.Clone();
            cj.FechaCompra = this.FechaCompra;
            cj.NoSerie = this.NoSerie;

            #region Clonar objetos de la interfaz
            if (this.Refacciones != null)
            {
                cj.Refacciones = this.Refacciones;
            }
            if (this.ubicacion1 != null)
            {
                cj.ubicacion1 = this.ubicacion1;
            }
            if (this.ubicacion2 != null)
            {
                cj.ubicacion2 = this.ubicacion2;
            }
            if (this.ubicacion3 != null)
            {
                cj.ubicacion3 = this.ubicacion3;
            }
            if (this.ubicacion4 != null)
            {
                cj.ubicacion4 = this.ubicacion4;
            }
            if (this.ubicacion5 != null)
            {
                cj.ubicacion5 = this.ubicacion5;
            }
            if (this.ubicacion6 != null)
            {
                cj.ubicacion6 = this.ubicacion6;
            }
            if (this.ubicacion7 != null)
            {
                cj.ubicacion7 = this.ubicacion7;
            }
            if (this.ubicacion8 != null)
            {
                cj.ubicacion8 = this.ubicacion8;
            }
            if (this.ubicacion9 != null)
            {
                cj.ubicacion9 = this.ubicacion9;
            }
            if (this.ubicacion10 != null)
            {
                cj.ubicacion10 = this.ubicacion10;
            }
            if (this.ubicacion11 != null)
            {
                cj.ubicacion11 = this.ubicacion11;
            }
            if (this.ubicacion12 != null)
            {
                cj.ubicacion12 = this.ubicacion12;
            }
            if (this.ubicacion13 != null)
            {
                cj.ubicacion13 = this.ubicacion13;
            }
            if (this.ubicacion14 != null)
            {
                cj.ubicacion14 = this.ubicacion14;
            }
            if (this.ubicacion15 != null)
            {
                cj.ubicacion15 = this.ubicacion15;
            }
            if (this.ubicacion16 != null)
            {
                cj.ubicacion16 = this.ubicacion16;
            }
            if (this.ubicacion17 != null)
            {
                cj.ubicacion17 = this.ubicacion17;
            }
            if (this.ubicacion18 != null)
            {
                cj.ubicacion18 = this.ubicacion18;
            }
            #endregion

            return cj;
        }


        //#region Implementacion de la interface ILlanta

        //public List<Refaccion> Refacciones
        //{
        //    get { return this.llantasRefaccion; }
        //    set { this.llantasRefaccion=value; }
        //}

        //public int? ID
        //{
        //    get
        //   ;
        //    set
        //    ;
        //}

        [DataMember]
        public override int? ILlantaID
        {
            get
            {
                return this.CajaID;
            }
            set
            {
                this.CajaID = value;
            }
        }

        [DataMember]
        public int? MantenibleID
        {
            get
            {
                return this.CajaID;
            }
            set
            {
                this.CajaID = value;
            }
        }

        [DataMember]
        public int? ComponenteID
        {
            get;
            set;
        }
    }
}
