﻿using System;

namespace Kanan.Vehiculos.BO2
{
    [Serializable]

    public class Propiedad : ICloneable
    {

        public int? PropiedadID { get; set; }
        /// <summary>
        /// Valor que puede ser numerico o caracter
        /// </summary>

        public string Valor { get; set; }

        public NombrePropiedad NombrePrincipal { get; set; }

        public object Clone()
        {
            Propiedad propiedad = new Propiedad();
            propiedad.PropiedadID = this.PropiedadID;
            if (this.NombrePrincipal != null)
            {
                propiedad.NombrePrincipal = this.NombrePrincipal;
            }
            return propiedad;
        }
    }
}
