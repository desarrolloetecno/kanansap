﻿using System;

namespace Kanan.Vehiculos.BO2
{
     [Serializable]
    public class OperadorVehiculoFilter:OperadorVehiculo
    {
        public DateTime? FechaFinAsignacion { get; set; }
        public DateTime? FechaFinLiberacion { get; set; }
    }
}
