﻿using Kanan.Comun.BO2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kanan.Vehiculos.BO2
{
    [Serializable]
    public class ImagenVehiculo
    {
        public int? VehiculoID { get; set; }
        public Imagen Imagen { get; set; }
        /// <summary>
        /// Tipo de lado de la imagen del vehiculo (Frente = 1, Lado izquierdo = 2, Lado derecho = 3, Trasera = 4)
        /// </summary>
        public int? TipoImagen { get; set; }
    }
}