﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Kanan.Operaciones.BO2;

namespace Kanan.Vehiculos.BO2
{
    [Serializable]
    [DataContract]
    public class TipoCombustible : ICloneable
    {
        #region Propiedades
        /// <summary>
        /// Representa el identificador del tipo de combustible.
        /// </summary>
        [DataMember]
        public int? TipoCombustibleID { get; set; }
        /// <summary>
        /// Representa el nombre del combustible a visualizar.
        /// </summary>
        [DataMember]
        public String Nombre { get; set; }
        /// <summary>
        /// Representa una descripción no visible al usuario.
        /// </summary>
        [DataMember]
        public String Descripcion { get; set; }
        /// <summary>
        /// Representa una la empresa a la cual pertence el tipo de combustible.
        /// </summary>
        [DataMember]
        public Empresa Empresa { get; set; }
        /// <summary>
        /// Representa una sucursal de alguna empresa la cual tiene cierto tipo de combustible.
        /// </summary>
        [DataMember]
        public Sucursal Sucursal { get; set; }
        /// <summary>
        /// Indica si el tipo de combusible se ha eliminado.
        /// </summary>
        [DataMember]
        public int? EstaActivo { get; set; }
        /// <summary>
        /// Contiene un código que utiliza FuelData.
        /// </summary>
        [DataMember]
        public String UUID { get; set; }
        /// <summary>
        /// Indica si se ha sincronizado con SAP.
        /// </summary>
        [DataMember]
        public int? Sincronizado { get; set; }
        #endregion Propiedades

        #region MétodoPrincipal
        public TipoCombustible()
        {
            Empresa = new Empresa();
            Sucursal = new Sucursal();
        }
        #endregion MétodoPrincipal

        #region Clone
        public object Clone()
        {
            TipoCombustible tipo = new TipoCombustible();
            tipo.TipoCombustibleID = this.TipoCombustibleID;
            tipo.Nombre = this.Nombre;
            tipo.Descripcion = this.Descripcion;
            tipo.Empresa = this.Empresa;
            tipo.Sucursal = this.Sucursal;
            tipo.EstaActivo = this.EstaActivo;
            tipo.UUID = this.UUID;
            tipo.Sincronizado = this.Sincronizado;
            return tipo;
        }
        #endregion Clone
    }
}