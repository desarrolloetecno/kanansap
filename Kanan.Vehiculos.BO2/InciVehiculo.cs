﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kanan.Comun.BO2;
namespace Kanan.Vehiculos.BO2
{
    public class IncidenciaVehiculox
    {
        /// <summary>
        /// Indica el ID de la incidencia.
        /// </summary>
        public int IncidenciaID { get; set; }

        /// <summary>
        /// Objeto de tipo Vehiculo.
        /// </summary>
        public Vehiculo Vehiculo { get; set; }

        /// <summary>
        /// Fecha en que ocurrió la incidencia.
        /// </summary>
        public DateTime? FechaIncidencia { get; set; }
        
        /// <summary>
        /// Descripcion de la incidencia.
        /// </summary>
        public string Descripcion { get; set; }
        
        /// <summary>
        /// Imagen de la incidencia antes
        /// </summary>
        public Imagen ImagenesIncidenciaAntes { get; set; }

        /// <summary>
        ///  Imagen de incidencia despues
        /// </summary>
        public Imagen ImagenesIncidenciaDespues { get; set; }

        /// <summary>
        /// Indica latitud donde ocurrio el incidente.
        /// </summary>
        public double? Latitude { get; set; }

        /// <summary>
        /// Indica longitud donde ocurrio el incidente.
        /// </summary>
        public double? Longitude { get; set; }

        public string TimeZone { get; set; }
        //public List<ImagenIncidenciaVehiculo> ListImagenesIncidenciaVehiculo { get; set; }
        /*public string UID { get; set; }*/
        public int? CreaOrdenServicio { get; set; }

        public object Clone()
        {
            IncidenciaVehiculox IncidenciaEnVehiculo = new IncidenciaVehiculox
            {
                IncidenciaID = this.IncidenciaID,
                Vehiculo = this.Vehiculo,
                FechaIncidencia = this.FechaIncidencia,
                Descripcion = this.Descripcion,
                ImagenesIncidenciaAntes = this.ImagenesIncidenciaAntes,
                ImagenesIncidenciaDespues = this.ImagenesIncidenciaDespues,
                Latitude = this.Latitude,
                Longitude = this.Longitude,
                TimeZone = this.TimeZone
                //ListImagenesIncidenciaVehiculo = this.ListImagenesIncidenciaVehiculo,
                //UID = this.UID
            };
            return IncidenciaEnVehiculo;
        }

    }
}
