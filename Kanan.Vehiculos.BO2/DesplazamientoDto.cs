﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kanan.Vehiculos.BO2
{
    /// <summary>
    /// Dto que sirve para el manejo de informacion para el historial de consumo de combustible
    /// </summary>
    public class DesplazamientoDto
    {
        /// <summary>
        /// ID del vehiculo
        /// </summary>
        public int vehiculoid { get; set;}
        /// <summary>
        /// Número economico del vehiculo
        /// </summary>
        public string vehiculo { get; set;}
        /// <summary>
        /// Rendimiento de combustible con carga de vehiculo
        /// </summary>
        public decimal? rendimientoCnCarga { get; set; }
        /// <summary>
        /// Rendimiento de combustible sin carga del vehiculo
        /// </summary>
        public decimal? rendimientoSnCarga { get; set; }
        /// <summary>
        /// Distancia recorrida actual minima del vehiculo
        /// </summary>
        public decimal? distMin { get; set;}
        /// <summary>
        /// Distancia recorrida actual maxima del vehiculo
        /// </summary>
        public decimal? distMax { get; set; }

        public string Empresa { get; set;}
        public string Sucursal { get; set;}
    }
}
