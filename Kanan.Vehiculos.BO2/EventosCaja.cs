﻿using System;

namespace Kanan.Vehiculos.BO2
{
   public class EventosCaja:ICloneable
    {
       public int? EventosCajaID { get; set; }
       public DateTime? Fecha { get; set; }
       public string Observaciones { get; set; }


       public object Clone()
       {
           EventosCaja ev = new EventosCaja();
           ev.EventosCajaID = this.EventosCajaID;
           ev.Fecha = this.Fecha;
           ev.Observaciones = this.Observaciones;
           return ev;
       }
    }
}
