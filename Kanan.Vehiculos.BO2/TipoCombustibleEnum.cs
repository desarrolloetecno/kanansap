﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Kanan.Vehiculos.BO2
{
     [Serializable]
    public enum TipoCombustibleEnum
    {

        [XmlEnum("1")] /// Ponerlo por cada valor
        GASOLINA_MAGNA= 1,
        [XmlEnum("2")]
        GASOLINA_PREMIUM = 2,
        [XmlEnum("3")]
        DIESEL = 3,
        [XmlEnum("4")]
        GAS = 4
    }
}
