﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kanan.Vehiculos.BO2
{
    public class recargadto
    {
       
        /// <summary>
        /// Nombre del vehículo
        /// </summary>
        public string vehiculo { get; set; }
        /// <summary>
        /// id del vehiculo
        /// </summary>
        public int vehiculoid { get; set;}
        /// <summary>
        /// total de litros
        /// </summary>
        public decimal? totallitros { get; set;}
        /// <summary>
        /// total del coste de todas las recargas
        /// </summary>
        public decimal? totalpesos { get; set;}
        /// <summary>
        /// total de recargas hechas
        /// </summary>
        public int? totalrecargas { get; set; }
        /// <summary>
        /// distancia minima de todas las recargas
        /// </summary>
        public decimal? minodometro { get; set;}
        /// <summary>
        /// 
        /// </summary>
        public decimal? maxodometro { get; set; }

        public int? modem { get; set; }

        public bool tienegps { get { return modem != null? true : false; } }

        public string tipocombustible { get; set; }

        public int? idTipoCombustible { get; set; }

        public int? unidadLongitudID { get; set; }

        public int? unidadVolumenID { get; set; }

        public DateTime Fecha { get; set; }
    }
}
