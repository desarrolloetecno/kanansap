﻿using System;

using Kanan.Comun.BO2;

namespace Kanan.Vehiculos.BO2
{
    [Serializable]

    public class ImagenIncidenciaVehiculo
    {

        public int? ImagenID { get; set; }

        public string Nombre { get; set; }

        //public Imagen Imagen { get; set; }
        /// <summary>
        /// Tipo de imagen de la recarga (Ticket = 1, Odometro = 2, Otro = 3)
        /// </summary>
        
        public int? TipoImagen { get; set; }

        public string Descripcion { get; set; }
        
        //public ImagenIncidenciaVehiculo()
        //{
        //    this.TipoImagen = 1;
        //}
    }
}
