﻿using System;
using System.Data;

namespace Kanan.Vehiculos.BO2
{
    [Serializable]
    public class QueryVehiculo
    {
        string sortField = string.Empty;
        /// <summary>
        /// Pagina que se esta presentando actualmente
        /// </summary>
        private int currentPage = 0;
        /// <summary>
        /// Numero total de elementos que puede arrojar la consulta
        /// </summary>
        private int? itemsCount;
        /// <summary>
        /// Numero de elementos por pagina
        /// </summary>
        private int? pageSize;
        /// <summary>
        /// Es un valor positivo que representa la pagina que se esta presentando actualmente
        /// </summary>
        public int CurrentPage {
            get { return this.currentPage; }
            set 
            {
                if (value < 0) 
                {
                    throw new ArgumentOutOfRangeException("La pagina actual no puede ser menor a cero.");
                }
                this.currentPage = value;
            }
        }
        /// <summary>
        /// Numero total de elementos que puede arrojar la consulta
        /// </summary>
        public int? ItemsCount
        {
            get { return this.itemsCount; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("El numero de elementos no puede ser menor a cero.");
                }
                this.itemsCount = value;
            }
        }
        /// <summary>
        /// Numero de elementos por pagina, por default es 10
        /// </summary>
        public int? PageSize
        {
            get { return this.pageSize; }
            set
            {
                if (value < 1)
                {
                    throw new ArgumentOutOfRangeException("El numero de elementos por pagina no puede ser menor a 1.");
                }
                this.pageSize = value;
            }
        }
        /// <summary>
        /// Numero de paginas en las que se puede dividir el resultado de la consulta segun el tamaño de la pagina.
        /// </summary>
        public int? PageCount { 
            get { 
                return this.ItemsCount % this.PageSize == 0 ? this.ItemsCount / this.PageSize : (this.ItemsCount / this.PageSize) + 1; 
            } 
        }


        /// <summary>
        /// Objeto que representa la consulta a realizar en la base de datos
        /// </summary>
        public object FilterObject { get; set; }
        /// <summary>
        /// Campo por el cual se pretende ordenar el resultado de la columna
        /// </summary>
        public string SortField { 
            get{
                return this.sortField;
            }
            set { 
                if (value.Contains(" ")) 
                    throw new Exception("Riesgo de seguridad encontrado en el valor");
                this.sortField = value;
            } 
        }
        /// <summary>
        /// Indica la manera en que se va ordenar los resultados, Ascendete o Desendente, por default es null
        /// </summary>
        public bool? IsAscendent { get; set; }
        /// <summary>
        /// Resulta de la consulta, unicamente se presenta la pagina actual segun CurrentPage
        /// </summary>
        public DataSet Result { get; set; }

        /// <summary>
        /// Rango ideal del primer elemento del resultado de la pagina actual
        /// </summary>
        public int? FirstItemIdex 
        {
            get 
            {
                if (this.pageSize == null) return null;                
                if((int)this.currentPage >0){
                    if (this.currentPage == 1) return 1;
                    return ((this.currentPage -1)* pageSize)+1;
                }
                else
                {
                    return 0;
                }
            }
        }
        /// <summary>
        /// Rango ideal del ultimo elemento del resultado de la pagina actual
        /// </summary>
        public int? LastItemIdex
        {
            get
            {
                if (this.PageSize == null) return null;
                if (this.CurrentPage == null) return null;
                    return this.FirstItemIdex + this.pageSize-1;                                                
            }
        }
        public QueryVehiculo() 
        {
            this.IsAscendent = null;
            this.pageSize = null;
        }

        
    }
}
