﻿using System;

namespace Kanan.Vehiculos.BO2
{
     [Serializable]
    public class Desplazamiento
    {
        public int? DesplazamientoID { get; set; }
        public DateTime? Fecha { get; set; }
        public double? DistanciaRecorrida { get; set; }
        /// <summary>
        /// Indica que el odómetro esta iniciado
        /// </summary>
        public bool? IsInitOdometro { get; set; }
    }
}
