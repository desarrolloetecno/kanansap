﻿
using System;
using System.Runtime.Serialization;
//
namespace Kanan.Vehiculos.BO2
{
    /// <summary>
    /// Interfaz que sirve para representar un objecto cualquier capaz de ser del modulo de mantenimiento
    /// </summary>
    //[DataContract]
    public interface IMantenible:ICloneable
    {
        /// <summary>
        /// Representa un objeto cualquier capaz de ser del modulo de mantenimiento
        /// </summary>
        int? MantenibleID
        {
            get;
            set;
        }
        int? ComponenteID
        {
            get;
            set;
        }
    }
}
