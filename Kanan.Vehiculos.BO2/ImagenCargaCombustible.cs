﻿using System;

using Kanan.Comun.BO2;

namespace Kanan.Vehiculos.BO2
{
    [Serializable]

    public class ImagenCargaCombustible
    {

        public int? ImagenCargaCombustibleID { get; set; }

        public int? CargaCombustibleID { get; set; }

        public Imagen Imagen { get; set; }
        /// <summary>
        /// Tipo de imagen de la recarga (Ticket = 1, Odometro = 2, Otro = 3)
        /// </summary>

        public int? TipoImagen { get; set; }

        public string UrlServerImagen { get; set; }

        //public ImagenCargaCombustible()
        //{
        //    this.TipoImagen = 1;
        //}
    }
}
