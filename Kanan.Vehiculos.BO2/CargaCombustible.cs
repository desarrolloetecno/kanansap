﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kanan.Costos.BO2;
using Kanan.Comun.BO2;
namespace Kanan.Vehiculos.BO2
{
    public class CargaCombustible
    {
        /// <summary>
        /// Id principal del objeto
        /// </summary>
        public int CargaCombustibleID { get; set;}
        /// <summary>
        /// Número de litros de carga
        /// </summary>
        //public decimal? Litros { get; set; }
        public double? Litros { get; set; }
        /// <summary>
        /// Costo total de la recarga
        /// </summary>
        public decimal Pesos { get; set; }
        /// <summary>
        /// Vehiculo quien realizo la recarga
        /// </summary>
        public Vehiculo Movil { get; set; }
        /// <summary>
        /// Fecha de la actividad, ingresada por el usuario
        /// </summary>
        public DateTime? Fecha { get; set; }

        /// <summary>
        /// Proveedor donde se realiza la recarga de combustible
        /// </summary>
        public Proveedor SourceCenter { get; set;}
        /// <summary>
        /// Indica el total de distancia actual del odometro
        /// </summary>
        //public decimal? Odometro { get; set; }
        public double? Odometro { get; set; }
        /// <summary>
        /// Fecha automatica del sistema
        /// </summary>
        public DateTime? FechaCaptura { get; set; }
        /// <summary>
        /// Indica el número de ticket o folio de la recarga
        /// </summary>
        public string Ticket { get; set; }
        /// <summary>
        /// Indica el tipo de combustible
        /// </summary>
        public TipoCombustible TypeFuel { get; set; }
        /// <summary>
        /// Indica sucursal o un identificador mas sobresaliente de la recarga
        /// </summary>
        public string CentroServicio { get; set;}

        public TipoDePago TipoPago { get; set; }

        //public int? FormaPago { get; set; }

        public string DetallePago { get; set; }

        public UnidadLongitud Longitud { get; set; }

        public UnidadVolumen Volumen { get; set; }

        /// <summary>
        /// Variable que se utiliza para identificar que la carga de combustible no se duplique
        /// </summary>
        public string UID { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public TipoMoneda Moneda { get; set; }

        public Imagen ImagenRecarga { get; set; }

        public string UrlServerImagen { get; set; }
        public string sMovimientoSAP { get; set; }

        public int? TipoPagoID { get; set; }

        public int? TipoMonedaID { get; set; }
        public int? EsFacturableSAP { get; set; }
        public List<ImagenCargaCombustible> ListImagenesRecarga { get; set; }

        public object Clone()
        {
            CargaCombustible carga = new CargaCombustible
            {
                CargaCombustibleID = this.CargaCombustibleID,
                Litros = this.Litros,
                Pesos = this.Pesos,
                Movil = this.Movil,
                Fecha = this.Fecha,
                SourceCenter = this.SourceCenter,
                Odometro = this.Odometro,
                FechaCaptura = this.FechaCaptura,
                Ticket = this.Ticket,
                TypeFuel = this.TypeFuel,
                CentroServicio = this.CentroServicio,
                //FormaPago = this.FormaPago,
                DetallePago = this.DetallePago,
                Longitud = this.Longitud,
                Volumen = this.Volumen,
                UID = this.UID,
                Latitude = this.Latitude,
                Longitude = this.Longitude,
                Moneda = this.Moneda,
                ImagenRecarga = this.ImagenRecarga,
                UrlServerImagen = this.UrlServerImagen,
                ListImagenesRecarga = this.ListImagenesRecarga,
                TipoPago = this.TipoPago
            };

            return carga;
        }
    }
}
