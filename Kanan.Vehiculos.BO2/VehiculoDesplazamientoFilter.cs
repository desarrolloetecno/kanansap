﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kanan.Vehiculos.BO2
{
    [Serializable]
    public class VehiculoDesplazamientoFilter:VehiculoDesplazamiento
    {
        /// <summary>
        /// Para realizar filtros del historico
        /// </summary>
        public DateTime? FechaFin { get; set;}
    }
}
