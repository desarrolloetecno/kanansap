﻿using System;
using System.Runtime.Serialization;
using Kanan.Operaciones.BO2;

namespace Kanan.Vehiculos.BO2
{
    [Serializable]
    [DataContract]

    public class TipoVehiculo : IMantenible, IAuditable, ICloneable
    {

        [DataMember]
        public int? TipoVehiculoID { get; set; }

        [DataMember]
        public string Nombre { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public string ImagenUrl { get; set; }

        [DataMember]
        public Empresa Propietario { get; set; }

        [DataMember]
        public Sucursal SubPropietario { get; set; }

        [DataMember]
        public bool? EsActivo { get; set; }

        public TipoVehiculo()
        {
            this.EsActivo = true;
        }

        [DataMember]
        public int? MantenibleID
        {
            get
            {
                return this.TipoVehiculoID;
            }
            set
            {
                this.TipoVehiculoID = value;
            }
        }

        [DataMember]
        public int? ComponenteID
        {
            get;
            set;
        }

        [DataMember]
        public int? IAuditableID
        {
            get
            {
                return this.TipoVehiculoID;
            }
            set
            {
                this.TipoVehiculoID = value;
            }
        }

        public object Clone()
        {
            TipoVehiculo tipoVehiculo = new TipoVehiculo();
            tipoVehiculo.TipoVehiculoID = this.TipoVehiculoID;
            tipoVehiculo.Nombre = this.Nombre;
            tipoVehiculo.Descripcion = this.Descripcion;
            tipoVehiculo.EsActivo = this.EsActivo;
            tipoVehiculo.ImagenUrl = this.ImagenUrl;
            if (this.Propietario != null)
            {
                tipoVehiculo.Propietario = (Empresa)this.Propietario.Clone();
            }
            if (this.SubPropietario != null)
            {
                tipoVehiculo.SubPropietario = (Sucursal)this.SubPropietario.Clone();
            }
            return tipoVehiculo;
        }

    }
}
