﻿
using System;
//
namespace Kanan.Vehiculos.BO2
{
    //[DataContract]
    public interface IAuditable
    {
        /// <summary>
        /// Representa del Vehiculo
        /// </summary>
        int? IAuditableID
        {
            get;
            set;
        }
    }
}
