﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kanan.Vehiculos.BO2
{
    public class Gestoria
    {
        /// <summary>
        /// Indica el ID del registro de gestión.
        /// </summary>
        public int? GestoriaID { get; set; }

        /// <summary>
        /// Indica el ID del vehículo.
        /// </summary>
        public int? VehiculoID { get; set; }

        /// <summary>
        /// Indica el TipoGestion (1 = Placas, 2 = Propietario, 3 = Tarjeta, 4 = Alta, 5 = Baja).
        /// </summary>
        public int? TipoGestion { get; set; }

        /// <summary>
        /// Indica el ID de la empresa.
        /// </summary>
        public int? EmpresaID { get; set; }

        /// <summary>
        /// Indica el ID de la sucursal.
        /// </summary>
        public int? SucursalID { get; set; }

        /// <summary>
        /// Indica la fecha capturada por el usuario.
        /// </summary>
        public DateTime? Fecha { get; set; }

        /// <summary>
        /// Indica la fecha real del registro.
        /// </summary>
        public DateTime? FechaRegistro { get; set; }

        /// <summary>
        /// Indica el numero capturado por el usuario.
        /// </summary>
        public String Numero { get; set; }

        /// <summary>
        /// Indica la observación.
        /// </summary>
        public String Observacion { get; set; }

        /// <summary>
        /// Indica ID del motivo de la baja.
        /// </summary>
        public int? MotivoBajaID { get; set; }
        public string MotivoBaja { get; set; }

        /// <summary>
        /// Indica un tipo de baja definida por el usuario en caso seleccionar "Otro".
        /// </summary>
        public String Especifique { get; set; }

        /// <summary>
        /// Indica el ID del documeto adjuntado.
        /// </summary>
        public int? DocID { get; set; }

        /// <summary>
        /// Campo extra.
        /// </summary>
        public String Otro { get; set; }
        public String rutaevidencia { get; set; }

        public object Clone()
        {
            Gestoria gestoria = new Gestoria()
            {
                GestoriaID = this.GestoriaID,
                VehiculoID = this.VehiculoID,
                TipoGestion = this.TipoGestion,
                EmpresaID = this.EmpresaID,
                SucursalID = this.SucursalID,
                Fecha = this.Fecha,
                FechaRegistro = this.FechaRegistro,
                Numero = this.Numero,
                Observacion = this.Observacion,
                MotivoBajaID = this.MotivoBajaID,
                Especifique = this.Especifique,
                DocID = this.DocID,
                Otro = this.Otro
            };
            return gestoria;
        }
    }
}
