﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Kanan.Vehiculos.BO2
{
    [Serializable]
    [DataContract]
    public class DesplazamientoHistorico
    {
        /// <summary>
        /// Indica el identificador de registro en SAP.
        /// </summary>
        [DataMember]
        public int DesplazamientoHistoricoID { get; set; }

        /// <summary>
        /// Indica la distancia con el que se cuenta en un momento específico.
        /// </summary>
        [DataMember]
        public double Distancia { get; set; }

        /// <summary>
        /// Indica la fecha en la que se realizó un registro.
        /// </summary>
        [DataMember]
        public DateTime Fecha { get; set; }

        /// <summary>
        /// Indica el identificador de registro de un vehículo.
        /// </summary>
        [DataMember]
        public int VehiculoID { get; set; }

        /// <summary>
        /// Indica el nombre de un vehículo.
        /// </summary>
        [DataMember]
        public String NombreVehiculo { get; set; }

        /// <summary>
        /// Indica la matrícula de un vehículo.
        /// </summary>
        [DataMember]
        public String PlacaVehiculo { get; set; }

        /// <summary>
        /// Indica la marca de un vehículo.
        /// </summary>
        [DataMember]
        public String MarcaVehiculo { get; set; }

        /// <summary>
        /// Indica el modelo de un vehículo.
        /// </summary>
        [DataMember]
        public String ModeloVehiculo { get; set; }
    }
}
