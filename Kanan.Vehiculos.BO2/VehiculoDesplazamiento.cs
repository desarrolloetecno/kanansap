﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kanan.Vehiculos.BO2
{
    public class VehiculoDesplazamiento
    {
         public int  remoraid {get;set;}
        /// <summary>
        /// Distancia recorrida actual
        /// </summary>
         public double distancia {get;set;}
        /// <summary>
        /// Fecha de ultimo reporte
        /// </summary>
         public DateTime? fechaultimoDp {get;set;}
         public int? vehiculoid { get; set; }
    }
}
