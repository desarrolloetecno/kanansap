﻿using System;
using Kanan.Operaciones.BO2;
namespace Kanan.Vehiculos.BO2
{
     [Serializable]
    public class OperadorVehiculo:ICloneable
    {
        public int? OperadorVehiculoID { get; set; }
        public DateTime? FechaAsignacion { get; set; }
        public DateTime? FechaLiberacion { get; set; }
        public Vehiculo VehiculoAsignado { get; set; }
        public Operador OperadorAsignado { get; set; }
        public string ObservacionLiberacion { get; set; }
        public string ObservacionAsignacion { get; set; }
        public object Clone()
        {
            OperadorVehiculo operadorVehiculo = new OperadorVehiculo();
            operadorVehiculo.OperadorVehiculoID = this.OperadorVehiculoID;
            operadorVehiculo.FechaAsignacion = this.FechaAsignacion;
            operadorVehiculo.FechaLiberacion = this.FechaLiberacion;
            if (this.VehiculoAsignado != null)
            {
                operadorVehiculo.VehiculoAsignado = this.VehiculoAsignado;
            }
            if (this.OperadorAsignado != null)
            {
                operadorVehiculo.OperadorAsignado = this.OperadorAsignado;
            }
            operadorVehiculo.ObservacionLiberacion = this.ObservacionLiberacion;
            operadorVehiculo.ObservacionAsignacion = this.ObservacionAsignacion;
            return operadorVehiculo;
        }
    }
}
