﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Kanan.Vehiculos.BO2
{
    [Serializable]
    [DataContract]
    public class TipoParametroVH
    { //#ManejoTipoParametroVehiculo
        [DataMember]
        public int? TipoParametroVHID { get; set; }
        [DataMember]
        public string DescripcionParametro { get; set; }
        [DataMember]
        public bool EsActivo { get; set; }
    }
}
