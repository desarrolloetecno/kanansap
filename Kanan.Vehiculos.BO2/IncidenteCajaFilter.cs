﻿using System;

namespace Kanan.Vehiculos.BO2
{
     [Serializable]
    public class IncidenteCajaFilter:IncidenteCaja
    {
        /// <summary>
        /// Fecha de instalacion
        /// </summary>
        public DateTime? FechaFin { get; set; }
    }
}
