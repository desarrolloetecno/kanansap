﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kanan.Vehiculos.BO2
{
    public class TipoActivos:ICloneable
    {
        public int? TipoActivoID { get; set; }
        public string Nombre { get; set; }
        public bool? EsActivo { get; set; }
        public TipoActivos() { this.EsActivo = true; }
        public object Clone()
        {
            TipoActivos tp = new TipoActivos();
            tp.TipoActivoID = this.TipoActivoID;
            tp.Nombre = this.Nombre;
            tp.EsActivo = this.EsActivo;
            return tp;
        }
    }
}
