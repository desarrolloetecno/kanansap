﻿using System;
namespace Kanan.Vehiculos.BO2
{
     [Serializable]
    public class IncidenteCaja:ICloneable
    {
        public int? IncidenteCajaID { get; set; }
         /// <summary>
         /// Indica la fecha de instalación o desinstalación
         /// </summary>
        public DateTime? Fecha { get; set; }
        public DateTime? FechaCaptura { get; set; }
        public string Accion { get; set; }
        public string Posicion { get; set; }
        public string Observaciones { get; set; }
        public Vehiculo Movil { get; set; }
        public Caja Remolque { get; set; }


        public object Clone()
        {
            IncidenteCaja ic = new IncidenteCaja();
            ic.IncidenteCajaID = this.IncidenteCajaID;
            ic.Fecha = this.Fecha;
            ic.FechaCaptura = this.FechaCaptura;
            ic.Accion = this.Accion;
            ic.Posicion = this.Posicion;
            ic.Observaciones = this.Observaciones;
            if (this.Movil != null)
            {
                ic.Movil = (Vehiculo)this.Movil.Clone();
            }
            if (this.Remolque != null)
            {
                ic.Remolque = (Caja)this.Remolque.Clone();
            }
            return ic;
        }
    }
}
