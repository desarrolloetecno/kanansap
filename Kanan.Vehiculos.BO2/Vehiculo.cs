﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Kanan.Comun.BO2;
using Kanan.Llantas.BO2;
using Kanan.Operaciones.BO2;
using System.Data;
using System.Collections;
using Kanan.Alertas.BO2;
using System.Runtime.Serialization;

namespace Kanan.Vehiculos.BO2
{
    [Serializable]
    [DataContract]
    public class Vehiculo : ILlanta, IMantenible, IAuditable, ICloneable, Notificable
    {
        [DataMember]
        public int? VehiculoID { get; set; }

        [DataMember]
        public string Nombre { get; set; }
        [DataMember]

        public string Descripcion { get; set; }
        [DataMember]

        public string Cobertura { get; set; }
        [DataMember]

        public string Placa { get; set; }
        [DataMember]

        public string Marca { get; set; }
        [DataMember]

        public int? Anio { get; set; }
        [DataMember]

        public string Modelo { get; set; }
        [DataMember]

        public List<ImagenVehiculo> ListaImagenesVehiculos { get; set; }
        //public string ESN { get; set;}
        /// <summary>
        /// No utilizar esta propiedad ya que lo utilizabamos para agreagr el nombre que traen del remora
        /// </summary>
        //public string NombreVehiculo { get; set; }
        /// <summary>
        /// No utiliar esta propieda, por lo que ahora el objeto vehiculo tiene el objeto modem.
        /// </summary>
        [DataMember]

        public Empresa Propietario { get; set; }
        [DataMember]

        public Sucursal SubPropietario { get; set; }
        [DataMember]

        public TipoVehiculo TipoVehiculo { get; set; }
        [DataMember]

        public int? TipoChasis { get; set; }
        [DataMember]

        public Imagen ImagenVehiculo { get; set; }
        [DataMember]

        public List<Propiedad> Propiedades { get; set; }
        [DataMember]

        public List<Registro> Registros { get; set; }
        [DataMember]

        public DateTime? VigenciaPoliza { get; set; }
        [DataMember]

        public string Poliza { get; set; }
        [DataMember]

        public int? AvisoPoliza { get; set; }
        [DataMember]

        public int? Plazas { get; set; }
        /// <summary>
        /// Rendimiento sin carga
        /// </summary>
        [DataMember]

        public double? RendimientoCombustible { get; set; }
        /// <summary>
        /// Rendimiento con carga
        /// </summary>
        [DataMember]

        public double? RendimientoCarga { get; set; }
        [DataMember]

        public bool? EsActivo { get; set; }
        [DataMember]

        public Modem EquipoRastreo { get; set; }
        [DataMember]

        public Caja caja1 { get; set; }
        [DataMember]

        public Caja caja2 { get; set; }
        [DataMember]

        public Caja caja3 { get; set; }

        /// <summary>
        /// Indica la fecha de inicio que se usara para filtrar datos del vehiculo
        /// como reportes, o consultas con fechas especificas
        /// </summary>
        [DataMember]
        public DateTime? FechaFiltroInicio { get; set; }
        /// <summary>
        /// Indica la fecha final que se usara para filtrar datos del vehiculo
        /// como reportes, o consultas con fechas especificas
        /// </summary>
        [DataMember]
        public DateTime? FechaFiltroFin { get; set; }
        /// <summary>
        /// Representa el costo de compra del vehículo.
        /// </summary>
        [DataMember]
        public double? costo { get; set; }

        /// <summary>
        /// Representa el costo la clave IAVE del vehículo.
        /// </summary>
        [DataMember]
        public String TarjetaIAVE
        { get; set; }

        public string FechaAlta { get; set; }
        public string FechaBaja { get; set; }


        [DataMember]
        public string NoSerie { get; set; }

        [DataMember]
        public string NoMotor { get; set; }

        [DataMember]
        public Double? CapacidadMaximaLitros { get; set; }
        //List<Refaccion> llantasRefaccion { get; set; }

        #region propiedades y objetos de la interface


        //public decimal? KilometroRecorrido { get; set; }

        //Llanta punto1 { get; set; }
        //Llanta punto2 { get; set; }
        //Llanta punto3 { get; set; }
        //Llanta punto4 { get; set; }
        //Llanta punto5 { get; set; }
        //Llanta punto6 { get; set; }
        //Llanta punto7 { get; set; }
        //Llanta punto8 { get; set; }
        //Llanta punto9 { get; set; }
        //Llanta punto10 { get; set; }
        //Llanta punto11 { get; set; }
        //Llanta punto12 { get; set; }
        //Llanta punto13 { get; set; }
        //Llanta punto14 { get; set; }
        //Llanta punto15 { get; set; }
        //Llanta punto16 { get; set; }
        //Llanta punto17 { get; set; }
        //Llanta punto18 { get; set; }


        #endregion


        /// <summary>
        /// Devuelve un DataTable que contiene los datos de las llantas asignadas y sus refacciones a partir del vehiculo completo
        /// </summary>
        public System.Data.DataTable ObjectToDataTable()
        {
            DataTable data = new DataTable();
            /// Crear columnas de la tabla
            data.Columns.Add("Dato", typeof(string));
            data.Columns.Add("Valor", typeof(string));
            data.Columns.Add("Posicion", typeof(string));
            data.Columns.Add("Llanta", typeof(string));
            data.Columns.Add("Refaccion", typeof(string));
            data.TableName = "Vehiculo";

            List<LlantaInstalada> llantas = this.getLlantasInstaladas();
            int i = 0;
            if (llantas.Count > i)
                i = llantas.Count;
            if (this.Refacciones.Count > i)
                i = Refacciones.Count;
            if (this.Propiedades.Count > i)
                i = Propiedades.Count;

            for (int j = 0; j < i; j++)
            {
                DataRow dr = data.NewRow();
                if (this.Propiedades.Count > j)
                {
                    dr["Dato"] = this.Propiedades[j] != null ? this.Propiedades[j].NombrePrincipal.Nombre : "";
                    dr["Valor"] = this.Propiedades[j] != null ? this.Propiedades[j].Valor : "";
                }
                if (llantas.Count > j)
                {
                    dr["Posicion"] = llantas[j] != null ? llantas[j].Posicion + "" : "";
                    dr["Llanta"] = llantas[j] != null ? llantas[j].Rueda.NumeroEconomico : "";

                }
                if (this.Refacciones.Count > j)
                {
                    dr["Refaccion"] = this.Refacciones[j] != null ? this.Refacciones[j].Neumatico.NumeroEconomico : "";
                }
                data.Rows.Add(dr);
            }

            return data;
        }

        ///// <summary>
        ///// Agrega los rows al DataTable
        ///// </summary>
        //private void AddDataToTable(DataTable data)
        //{

        //}

        ///// <summary>
        ///// Obtiene una lista de LlantaItem que contiene las llantas instaladas en el vehículo con su respectiva ubicación
        ///// </summary>
        ///// <returns></returns>
        //private List<LlantaInstalada> GetLlantas()
        //{

        //    List<LlantaInstalada> llantas = new List<LlantaInstalada>();
        //    if (this.ubicacion1 != null && this.ubicacion1.LlantaID != null)
        //    {

        //        llantas.Add(new LlantaInstalada() { posicion = "1", numeroeconomico = this.ubicacion1.NumeroEconomico });
        //    }
        //    if (this.ubicacion2 != null && this.ubicacion2.LlantaID != null)
        //    {
        //        llantas.Add(new LlantaInstalada() { posicion = "2", numeroeconomico = this.ubicacion2.NumeroEconomico });
        //    }

        //    if (this.ubicacion3 != null && this.ubicacion3.LlantaID != null)
        //    {
        //        llantas.Add(new LlantaInstalada() { posicion = "3", numeroeconomico = this.ubicacion3.NumeroEconomico });
        //    }

        //    if (this.ubicacion4 != null && this.ubicacion4.LlantaID != null)
        //    {
        //        llantas.Add(new LlantaInstalada() { posicion = "4", numeroeconomico = this.ubicacion4.NumeroEconomico });
        //    }

        //    if (this.ubicacion5 != null && this.ubicacion5.LlantaID != null)
        //    {
        //        llantas.Add(new LlantaInstalada() { posicion = "5", numeroeconomico = this.ubicacion5.NumeroEconomico });
        //    }

        //    if (this.ubicacion6 != null && this.ubicacion6.LlantaID != null)
        //    {
        //        llantas.Add(new LlantaInstalada() { posicion = "6", numeroeconomico = this.ubicacion6.NumeroEconomico });
        //    }

        //    if (this.ubicacion7 != null && this.ubicacion7.LlantaID != null)
        //    {
        //        llantas.Add(new LlantaInstalada() { posicion = "7", numeroeconomico = this.ubicacion7.NumeroEconomico });
        //    }

        //    if (this.ubicacion8 != null && this.ubicacion8.LlantaID != null)
        //    {
        //        llantas.Add(new LlantaInstalada() { posicion = "8", numeroeconomico = this.ubicacion8.NumeroEconomico });
        //    }

        //    if (this.ubicacion9 != null && this.ubicacion9.LlantaID != null)
        //    {
        //        llantas.Add(new LlantaInstalada() { posicion = "9", numeroeconomico = this.ubicacion9.NumeroEconomico });
        //    }

        //    if (this.ubicacion10 != null && this.ubicacion10.LlantaID != null)
        //    {
        //        llantas.Add(new LlantaInstalada() { posicion = "10", numeroeconomico = this.ubicacion10.NumeroEconomico });
        //    }

        //    if (this.ubicacion11 != null && this.ubicacion11.LlantaID != null)
        //    {
        //        llantas.Add(new LlantaInstalada() { posicion = "11", numeroeconomico = this.ubicacion11.NumeroEconomico });
        //    }

        //    if (this.ubicacion12 != null && this.ubicacion12.LlantaID != null)
        //    {
        //        llantas.Add(new LlantaInstalada() { posicion = "12", numeroeconomico = this.ubicacion12.NumeroEconomico });
        //    }

        //    if (this.ubicacion13 != null && this.ubicacion13.LlantaID != null)
        //    {
        //        llantas.Add(new LlantaInstalada() { posicion = "13", numeroeconomico = this.ubicacion13.NumeroEconomico });
        //    }

        //    if (this.ubicacion14 != null && this.ubicacion14.LlantaID != null)
        //    {
        //        llantas.Add(new LlantaInstalada() { posicion = "14", numeroeconomico = this.ubicacion14.NumeroEconomico });
        //    }

        //    if (this.ubicacion15 != null && this.ubicacion15.LlantaID != null)
        //    {
        //        llantas.Add(new LlantaInstalada() { posicion = "15", numeroeconomico = this.ubicacion15.NumeroEconomico });
        //    }

        //    if (this.ubicacion16 != null && this.ubicacion16.LlantaID != null)
        //    {
        //        llantas.Add(new LlantaInstalada() { posicion = "16", numeroeconomico = this.ubicacion15.NumeroEconomico });
        //    }

        //    if (this.ubicacion17 != null && this.ubicacion17.LlantaID != null)
        //    {
        //        llantas.Add(new LlantaInstalada() { posicion = "17", numeroeconomico = this.ubicacion17.NumeroEconomico });
        //    }
        //    if (this.ubicacion18 != null && this.ubicacion18.LlantaID != null)
        //    {
        //        llantas.Add(new LlantaInstalada() { posicion = "18", numeroeconomico = this.ubicacion18.NumeroEconomico });
        //    }




        //    return llantas;
        //}

        public Vehiculo()
        {
            this.EsActivo = true;
        }
        [DataMember]
        public int? MantenibleID
        {
            get
            {
                return this.VehiculoID;
            }
            set
            {
                this.VehiculoID = value;
            }
        }
        [DataMember]
        public int? ComponenteID
        {
            get;
            set;
        }
        [DataMember]
        public int? IAuditableID
        {
            get
            {
                return this.VehiculoID;
            }
            set
            {
                this.VehiculoID = value;
            }
        }

        public object Clone()
        {
            Vehiculo vehiculo = new Vehiculo();
            vehiculo.VehiculoID = this.VehiculoID;
            vehiculo.Nombre = this.Nombre;
            vehiculo.Placa = this.Placa;
            vehiculo.Plazas = this.Plazas;
            vehiculo.Poliza = this.Poliza;
            vehiculo.Modelo = this.Modelo;
            vehiculo.Anio = this.Anio;
            vehiculo.VigenciaPoliza = this.VigenciaPoliza;
            vehiculo.Descripcion = this.Descripcion;
            vehiculo.RendimientoCarga = this.RendimientoCarga;
            vehiculo.Marca = this.Marca;
            vehiculo.RendimientoCombustible = this.RendimientoCombustible;
            vehiculo.IAuditableID = this.IAuditableID;
            vehiculo.MantenibleID = this.MantenibleID;
            vehiculo.ComponenteID = this.ComponenteID;
            vehiculo.ImagenVehiculo = this.ImagenVehiculo;
            vehiculo.TipoVehiculo = this.TipoVehiculo;
            vehiculo.AvisoPoliza = this.AvisoPoliza;
            vehiculo.ListaImagenesVehiculos = this.ListaImagenesVehiculos;
            vehiculo.FechaFiltroInicio = this.FechaFiltroInicio;
            vehiculo.FechaFiltroFin = this.FechaFiltroFin;
            if (this.Propietario != null)
            {
                vehiculo.Propietario = (Empresa)this.Propietario.Clone();
            }
            if (this.SubPropietario != null)
            {
                vehiculo.SubPropietario = (Sucursal)this.SubPropietario.Clone();
            }
            //vehiculo.TipoProveedorID = this.TipoProveedorID;
            vehiculo.Registros = this.Registros;
            vehiculo.Propiedades = this.Propiedades;
            //vehiculo.ProveedorID = this.ProveedorID;
            vehiculo.EsActivo = this.EsActivo;
            if (this.EquipoRastreo != null)
            {
                vehiculo.EquipoRastreo = (Modem)this.EquipoRastreo.Clone();
            }
            if (this.caja1 != null)
            {
                vehiculo.caja1 = (Caja)this.caja1.Clone();
            }
            if (this.caja2 != null)
            {
                vehiculo.caja2 = (Caja)this.caja2.Clone();
            }
            if (this.caja3 != null)
            {
                vehiculo.caja3 = (Caja)this.caja3.Clone();
            }

            #region Clonar objetos de la interfaz
            if (this.ubicacion1 != null)
            {
                vehiculo.ubicacion1 = this.ubicacion1;
            }
            if (this.ubicacion2 != null)
            {
                vehiculo.ubicacion2 = this.ubicacion2;
            }
            if (this.ubicacion3 != null)
            {
                vehiculo.ubicacion3 = this.ubicacion3;
            }
            if (this.ubicacion4 != null)
            {
                vehiculo.ubicacion4 = this.ubicacion4;
            }
            if (this.ubicacion5 != null)
            {
                vehiculo.ubicacion5 = this.ubicacion5;
            }
            if (this.ubicacion6 != null)
            {
                vehiculo.ubicacion6 = this.ubicacion6;
            }
            if (this.ubicacion7 != null)
            {
                vehiculo.ubicacion7 = this.ubicacion7;
            }
            if (this.ubicacion8 != null)
            {
                vehiculo.ubicacion8 = this.ubicacion8;
            }
            if (this.ubicacion9 != null)
            {
                vehiculo.ubicacion9 = this.ubicacion9;
            }
            if (this.ubicacion10 != null)
            {
                vehiculo.ubicacion10 = this.ubicacion10;
            }
            if (this.ubicacion11 != null)
            {
                vehiculo.ubicacion11 = this.ubicacion11;
            }
            if (this.ubicacion12 != null)
            {
                vehiculo.ubicacion12 = this.ubicacion12;
            }
            if (this.ubicacion13 != null)
            {
                vehiculo.ubicacion13 = this.ubicacion13;
            }
            if (this.ubicacion14 != null)
            {
                vehiculo.ubicacion14 = this.ubicacion14;
            }
            if (this.ubicacion15 != null)
            {
                vehiculo.ubicacion15 = this.ubicacion15;
            }
            if (this.ubicacion16 != null)
            {
                vehiculo.ubicacion16 = this.ubicacion16;
            }
            if (this.ubicacion17 != null)
            {
                vehiculo.ubicacion17 = this.ubicacion17;
            }
            if (this.ubicacion18 != null)
            {
                vehiculo.ubicacion18 = this.ubicacion18;
            }
            if (this.Refacciones != null)
            {
                vehiculo.Refacciones = this.Refacciones;
            }
            #endregion
            return vehiculo;
        }
        [DataMember]
        public int? NotificableID
        {
            get
            {
                return this.VehiculoID;
            }
            set
            {
                this.VehiculoID = value;
            }
        }

        #region Implementacion de la interface ILlanta

        //public List<Refaccion> Refacciones
        //{
        //    get { return this.llantasRefaccion; }
        //    set { this.llantasRefaccion = value; }
        //}

        //public int? ID
        //{
        //    get
        //   ;
        //    set
        //    ;
        //}
        [DataMember]
        public override int? ILlantaID
        {
            get
            {
                return this.VehiculoID;
            }
            set
            {
                this.VehiculoID = value;
            }
        }

        //public string NumeroEconomico
        //{
        //    get
        //    {
        //        return this.Nombre;
        //    }
        //    set
        //    {
        //        this.Nombre = value;
        //    }
        //}

        //public decimal? KilometrosRecorridos
        //{
        //    get
        //    {
        //        return this.KilometroRecorrido;
        //    }
        //    set
        //    {
        //        this.KilometroRecorrido = value;
        //    }
        //}

        //public Llanta ubicacion1
        //{
        //    get
        //    {
        //        return this.punto1;
        //    }
        //    set
        //    {
        //        this.punto1 = value;
        //    }
        //}

        //public Llanta ubicacion2
        //{
        //    get
        //    {
        //        return this.punto2;
        //    }
        //    set
        //    {
        //        this.punto2 = value;
        //    }
        //}

        //public Llanta ubicacion3
        //{
        //    get
        //    {
        //        return this.punto3;
        //    }
        //    set
        //    {
        //        this.punto3 = value;
        //    }
        //}

        //public Llanta ubicacion4
        //{
        //    get
        //    {
        //        return this.punto4;
        //    }
        //    set
        //    {
        //        this.punto4 = value;
        //    }
        //}

        //public Llanta ubicacion5
        //{
        //    get
        //    {
        //        return this.punto5;
        //    }
        //    set
        //    {
        //        this.punto5 = value;
        //    }
        //}

        //public Llanta ubicacion6
        //{
        //    get
        //    {
        //        return this.punto6;
        //    }
        //    set
        //    {
        //        this.punto6 = value;
        //    }
        //}

        //public Llanta ubicacion7
        //{
        //    get
        //    {
        //        return this.punto7;
        //    }
        //    set
        //    {
        //        this.punto7 = value;
        //    }
        //}

        //public Llanta ubicacion8
        //{
        //    get
        //    {
        //        return this.punto8;
        //    }
        //    set
        //    {
        //        this.punto8 = value;
        //    }
        //}

        //public Llanta ubicacion9
        //{
        //    get
        //    {
        //        return this.punto9;
        //    }
        //    set
        //    {
        //        this.punto9 = value;
        //    }
        //}

        //public Llanta ubicacion10
        //{
        //    get
        //    {
        //        return this.punto10;
        //    }
        //    set
        //    {
        //        this.punto10 = value;
        //    }
        //}

        //public Llanta ubicacion11
        //{
        //    get
        //    {
        //        return this.punto11;
        //    }
        //    set
        //    {
        //        this.punto11 = value;
        //    }
        //}

        //public Llanta ubicacion12
        //{
        //    get
        //    {
        //        return this.punto12;
        //    }
        //    set
        //    {
        //        this.punto12 = value;
        //    }
        //}

        //public Llanta ubicacion13
        //{
        //    get
        //    {
        //        return this.punto13;
        //    }
        //    set
        //    {
        //        this.punto13 = value;
        //    }
        //}

        //public Llanta ubicacion14
        //{
        //    get
        //    {
        //        return this.punto14;
        //    }
        //    set
        //    {
        //        this.punto14 = value;
        //    }
        //}

        //public Llanta ubicacion15
        //{
        //    get
        //    {
        //        return this.punto15;
        //    }
        //    set
        //    {
        //        this.punto15 = value;
        //    }
        //}

        //public Llanta ubicacion16
        //{
        //    get
        //    {
        //        return this.punto16;
        //    }
        //    set
        //    {
        //        this.punto16 = value;
        //    }
        //}

        //public Llanta ubicacion17
        //{
        //    get
        //    {
        //        return this.punto17;
        //    }
        //    set
        //    {
        //        this.punto17 = value;
        //    }
        //}

        //public Llanta ubicacion18
        //{
        //    get
        //    {
        //        return this.punto18;
        //    }
        //    set
        //    {
        //        this.punto18 = value;
        //    }
        //}

        #endregion

    }
}
