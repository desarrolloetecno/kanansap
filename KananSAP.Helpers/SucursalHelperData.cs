﻿using KananSAP.Mantenimiento.Data;
using SAPbobsCOM;
using Kanan.Mantenimiento.BO2;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using KananSAP.Vehiculos.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KananSAP.Operaciones.Data;
using System.Data;

namespace KananSAP.Helpers
{
    public class SucursalHelperData
    {
        private SAPbobsCOM.Company oCompany;
        public SucursalHelperData(SAPbobsCOM.Company company)
        {
            this.oCompany = company;
        }

        public void InsertarCompleto(Sucursal sucursal)
        {
            try
            {
                SucursalSAP sucursalData = new SucursalSAP(this.oCompany);
                sucursalData.AddBranch();
                sucursalData.SetItemCodeBranch(sucursal.Direccion);
                sucursalData.SucursalToUDT();
                sucursalData.Insertar();
                //sucursalData.Sucursal = sucursal;//Originalmente V
                //sucursalData.SucursalToUDT();
                //sucursalData.Insertar();
            }
            catch (Exception ex) { throw new Exception(ex.Message); }

        }

        public void ActualizarCompleto(Sucursal sucursal)
        {
            try
            {
                TipoVehiculoSAP tipoVehiculoData = new TipoVehiculoSAP(this.oCompany);
                VehiculoSAP vehiculoData = new VehiculoSAP(this.oCompany);
                TipoServicioData tipoServicioData = new TipoServicioData(this.oCompany);
                ServicioSAP servicioData = new ServicioSAP(this.oCompany);
                OrdenServicioSAP ordenData = new OrdenServicioSAP(ref this.oCompany);
                ParametroMantenimientoData pMantenimientoData = new ParametroMantenimientoData(this.oCompany);
                ParametroServicioData pServicioData = new ParametroServicioData(this.oCompany);
                SucursalSAP sucursalData = new SucursalSAP(this.oCompany);

                #region actualizando los objetos del paquete de vehiculo
                List<Vehiculo> vehiculos = new List<Vehiculo>();
                Vehiculo anteriorVehiculo = new Vehiculo();
                anteriorVehiculo.Propietario = sucursal.Base;
                anteriorVehiculo.SubPropietario = sucursal;
                Recordset rsVehiculo = vehiculoData.Consultar(anteriorVehiculo);
                if (vehiculoData.HasVehiculos(rsVehiculo))
                {
                    vehiculos = vehiculoData.RecordSetToListVehiculos(rsVehiculo);
                    foreach (Vehiculo vhAnterior in vehiculos)
                    {
                        Vehiculo vehiculoNuevo = (Vehiculo)vhAnterior.Clone();
                        vehiculoNuevo.SubPropietario.SucursalID = null;
                        vehiculoData.ItemCode = vehiculoNuevo.VehiculoID.ToString();
                        if(vehiculoData.ExistUDT())
                        {
                            vehiculoData.vehiculo = vehiculoNuevo;
                            vehiculoData.VehiculoToSAP();
                            vehiculoData.Update();
                        }
                    }
                }
                List<TipoVehiculo> tiposVehiculo = new List<TipoVehiculo>();
                TipoVehiculo tipovehiculo = new TipoVehiculo();
                tipovehiculo.Propietario = sucursal.Base;
                tipovehiculo.SubPropietario = sucursal;
                Recordset rsTipoVehiculo = tipoVehiculoData.Consultar(tipovehiculo);
                if (tipoVehiculoData.HashTipoVehiculo(rsTipoVehiculo))
                {
                    tiposVehiculo = tipoVehiculoData.RecordSetToListTipoVehiculo(rsTipoVehiculo);
                    foreach (TipoVehiculo tipovhAnterior in tiposVehiculo)
                    {
                        TipoVehiculo tipovhNuevo = (TipoVehiculo)tipovhAnterior.Clone();
                        tipovhNuevo.SubPropietario.SucursalID = null;
                        tipoVehiculoData.ItemCode = tipovhNuevo.TipoVehiculoID.ToString();
                        if (tipoVehiculoData.ExistUDT())
                        {
                            tipoVehiculoData.TipoVehiculo = tipovhNuevo;
                            tipoVehiculoData.TipoVehiculoToUDT();
                            tipoVehiculoData.Actualizar();
                        }
                    }
                }
                
                #endregion
                #region actualizando los objetos del paquete de mantenimiento
                List<TipoServicio> tiposServicio = new List<TipoServicio>();
                TipoServicio tiposervicio = new TipoServicio();
                tiposervicio.Propietario = sucursal.Base;
                tiposervicio.SubPropietario = sucursal;
                Recordset rsTiposervicio = tipoServicioData.Consultar(tiposervicio);
                if (tipoServicioData.HasTipoServicio(rsTiposervicio))
                {
                    tiposServicio = tipoServicioData.RecordSetToListTipoServicio(rsTiposervicio);
                    foreach (TipoServicio tipoSvAnterior in tiposServicio)
                    {
                        TipoServicio tipoSvNuevo = (TipoServicio)tipoSvAnterior.Clone();
                        tipoSvNuevo.SubPropietario.SucursalID = null;
                        tipoServicioData.ItemCode = tipoSvNuevo.TipoServicioID.ToString();
                        if(tipoServicioData.Existe())
                        {
                            tipoServicioData.oTipoServicio = tipoSvNuevo;
                            tipoServicioData.TipoServicioToUDT();
                            tipoServicioData.Actualizar();
                        }
                    }
                }

                List<Servicio> servicios = new List<Servicio>();
                Servicio servicio = new Servicio();
                servicio.Propietario = sucursal.Base;
                servicio.SubPropietario = sucursal;
                Recordset rsServicio = servicioData.Consultar(servicio);
                if (servicioData.HashServicios(rsServicio))
                {
                    servicios = servicioData.RecordSetToListServicio(rsServicio);
                    foreach (Servicio servicioAnterior in servicios)
                    {
                        Servicio servicioNuevo = (Servicio)servicioAnterior.Clone();
                        servicioNuevo.SubPropietario.SucursalID = null;
                        servicioData.ItemCode = servicioNuevo.ServicioID.ToString();
                        if(servicioData.Existe())
                        {
                            servicioData.oServicio = servicioNuevo;
                            servicioData.ServicioToUDT();
                            servicioData.Actualizar();
                        }
                    }
                }
                #endregion
                sucursalData.ItemCode = sucursal.SucursalID.ToString();
                if(sucursalData.ExistUDT())
                {
                    if (sucursalData.SetItemCodeBranch(sucursal.Direccion))
                        sucursalData.RemoveBranch();
                    sucursalData.Deactivate();
                }
                else
                {
                    if(sucursalData.SetUDTBySucursalID(sucursal.SucursalID))
                    {
                        if(sucursalData.SetItemCodeBranch(sucursal.Direccion))
                            sucursalData.RemoveBranch();
                        sucursalData.Deactivate();
                    }
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

    }
}
