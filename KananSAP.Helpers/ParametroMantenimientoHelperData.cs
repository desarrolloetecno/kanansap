﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kanan.Mantenimiento.BO2;
using Kanan.Vehiculos.BO2;
using SAPbobsCOM;
using KananSAP.Mantenimiento.Data;

namespace KananSAP.Helpers
{
    public class ParametroMantenimientoHelperData
    {
        #region Atributos
        public ParametroMantenimiento oParametroMantenimiento { get; set; }
        private Company oCompany;
        #endregion
        #region Constructor
        public ParametroMantenimientoHelperData(Company company)
        {
            this.oCompany = company;
        }
        #endregion
        /// <summary>
        /// Guarda las configuraciones de parametro de mantenimiento cuando el mantenible es de tipo vehiculo
        /// </summary>
        private void InsertaParametroMantenimiento()
        {
            try
            {
                ParametroMantenimientoData pmData = new ParametroMantenimientoData(this.oCompany);
                pmData.oParametroMantenimiento = this.oParametroMantenimiento;
                pmData.ParametroMantenimientoToUDT();
                pmData.Insert();
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Guarda las configuraciones de parametro de servicio
        /// </summary>
        private void InsertarParametroServicio()
        {
            try
            {
                ParametroServicioData psData = new ParametroServicioData(this.oCompany);
                psData.oParametroServicio = this.oParametroMantenimiento.ParametroServicio;
                psData.ParametroServicioToUDT();
                psData.Insert();
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Guarda las configuraciones de parametro de mantenimiento y servicio cuando no es de tipo vehiculo
        /// </summary>
        public void InsertaCompleto()
        {
            try
            {
                ParametroMantenimientoData paraMantenimientoData = new ParametroMantenimientoData(this.oCompany);
                ParametroServicioData paraServicioData = new ParametroServicioData(this.oCompany);
                paraServicioData.oParametroServicio = this.oParametroMantenimiento.ParametroServicio;
                paraServicioData.ParametroServicioToUDT();
                paraServicioData.Insert();
                Recordset rs = paraServicioData.Consultar(this.oParametroMantenimiento.ParametroServicio);
                if (paraServicioData.HashParametroServicio(rs))
                    this.oParametroMantenimiento.ParametroServicio = paraServicioData.LastRecordSetToParametroServicio(rs);
                paraMantenimientoData.oParametroMantenimiento = this.oParametroMantenimiento;
                paraMantenimientoData.ParametroMantenimientoToUDT();
                paraMantenimientoData.Insert();
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Verifica si existe algun parametro de mantenimiento o serivio de un vehiculo seleccionado
        /// </summary>
        /// <returns></returns>
        public bool ExisteParametro()
        {
            bool existe = false;
            try
            {
               
                if(this.oParametroMantenimiento.Mantenible != null)
                {
                    if(this.oParametroMantenimiento.Mantenible.GetType() == typeof(Vehiculo))
                    {
                        ParametroMantenimientoData pmData = new ParametroMantenimientoData(this.oCompany);
                        Recordset rs = pmData.Consultar(this.oParametroMantenimiento);
                        if (pmData.HasParametroMantenimiento(rs))
                            existe = true;
                    }
                    else
                    {
                        ParametroServicioData psData = new ParametroServicioData(this.oCompany);
                        Recordset rs = psData.Consultar(this.oParametroMantenimiento.ParametroServicio);
                        if (psData.HashParametroServicio(rs))
                            existe = true;
                    }
                    
                }
                return existe;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Obtiene el ID del parametro de mantenimiento
        /// </summary>
        /// <returns></returns>
        private int GetParametroMantenimientoID()
        {
            try
            {
                int id = 0;
                ParametroMantenimientoData pmData = new ParametroMantenimientoData(this.oCompany);
                ParametroMantenimiento pm = new ParametroMantenimiento();
                Recordset rs = pmData.Consultar(this.oParametroMantenimiento);
                if (pmData.HasParametroMantenimiento(rs))
                    pm = pmData.LastRecordSetToParametroMantenimiento(rs);
                id = Convert.ToInt32(pm.ParametroMantenimientoID);
                if (id <= 0)
                    throw new Exception("No se contro ningun Parametro de mantenimiento con el identificador proporcionado");
                return id;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Realiza una consulta de los parametros de mantenimiento y de servicio
        /// para obtener las configuraciones guardadas por el usuario
        /// </summary>
        /// <returns></returns>
        public ParametroMantenimiento ConsultarCompleto()
        {
            try
            {
                ParametroMantenimiento param = new ParametroMantenimiento();
                ParametroMantenimientoData pmData = new ParametroMantenimientoData(this.oCompany);
                ParametroServicioData psData = new ParametroServicioData(this.oCompany);
                ServicioSAP svData = new ServicioSAP(this.oCompany);
                param.ParametroMantenimientoID = this.GetParametroMantenimientoID();
                Recordset rspm = pmData.Consultar(param);
                if (pmData.HasParametroMantenimiento(rspm))
                    param = pmData.LastRecordSetToParametroMantenimiento(rspm);
                Recordset rsps = psData.Consultar(param.ParametroServicio);
                if (psData.HashParametroServicio(rsps))
                    param.ParametroServicio = psData.LastRecordSetToParametroServicio(rsps);
                Recordset rssv = svData.Consultar(param.ParametroServicio.Servicio);
                if (svData.HashServicios(rssv))
                    param.ParametroServicio.Servicio = svData.LastRecordSetToServicio(rssv);
                //Falta agregar la consulta para los mantenibles
                return param;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Actualiza el parametro de mantenimiento cuando el mantenible es de tipo vehiculo
        /// </summary>
        public void ActualizarParametroMantenimiento()
        {
            ParametroMantenimientoData pmData = new ParametroMantenimientoData(this.oCompany);
            pmData.ItemCode = this.oParametroMantenimiento.ParametroMantenimientoID.ToString();
            if(!pmData.Existe())
                throw new Exception("El registro que intenta actualizar no existe");
            pmData.oParametroMantenimiento = this.oParametroMantenimiento;
            pmData.ParametroMantenimientoToUDT();
            pmData.Actualizar();
        }

        /// <summary>
        /// Actualiza las configuraciones de parametro de manenimiento y de servicio
        /// </summary>
        public void InsertaOActualizarParametros()
        {
            try
            {
                ParametroServicioData psData = new ParametroServicioData(this.oCompany);
                ParametroMantenimientoData pmData = new ParametroMantenimientoData(this.oCompany);
                
                
                ParametroServicio psTemp = new ParametroServicio();
                psTemp.Servicio = this.oParametroMantenimiento.ParametroServicio.Servicio;
                psTemp.Mantenible = this.oParametroMantenimiento.Mantenible;
                Recordset rs = psData.Consultar(psTemp);
                if(psData.HashParametroServicio(rs))
                {
                    psData.oParametroServicio = this.oParametroMantenimiento.ParametroServicio;
                    psData.ItemCode = this.oParametroMantenimiento.ParametroServicio.ParametroServicioID.ToString();
                    if (!psData.Existe())
                        throw new Exception("El registro que intenta actualizar no existe");
                    psData.ParametroServicioToUDT();
                    psData.Actualizar();
                }
                else
                {
                    ParametroMantenimiento pm = new ParametroMantenimiento();
                    pm.Mantenible = this.oParametroMantenimiento.Mantenible;
                    psData.oParametroServicio = this.oParametroMantenimiento.ParametroServicio;
                    psData.ParametroServicioToUDT();
                    psData.Insert();
                    Recordset rss = psData.Consultar(psTemp);
                    if (psData.HashParametroServicio(rss))
                        this.oParametroMantenimiento.ParametroServicio = psData.LastRecordSetToParametroServicio(rss);
                }
                pmData.oParametroMantenimiento = this.oParametroMantenimiento;
                pmData.ItemCode = this.oParametroMantenimiento.ParametroMantenimientoID.ToString();
                if(!pmData.Existe())
                    throw new Exception("El registro que intenta actualizar no existe");
                pmData.ParametroMantenimientoToUDT();
                pmData.Actualizar();
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Actualiza los parametros de mantenimiento y de servicio, generalmente cuando se esta realizando una orden de servicio por alerta generada
        /// </summary>
        public void ActualizarCompleto()
        {
            try
            {
                ParametroMantenimientoData pmData = new ParametroMantenimientoData(this.oCompany);
                ParametroServicioData psData = new ParametroServicioData(this.oCompany);
                pmData.ItemCode = this.oParametroMantenimiento.ParametroMantenimientoID.ToString();
                if(pmData.Existe())
                {
                    pmData.oParametroMantenimiento = this.oParametroMantenimiento;
                    pmData.Actualizar();
                }
                psData.ItemCode = this.oParametroMantenimiento.ParametroServicio.ParametroServicioID.ToString();
                if(psData.Existe())
                {
                    psData.oParametroServicio = this.oParametroMantenimiento.ParametroServicio;
                    psData.Actualizar();
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

    }
}
