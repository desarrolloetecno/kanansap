﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kanan.Mantenimiento.BO2;
using SAPbobsCOM;
using Kanan.Operaciones.BO2;

namespace Kanan.SAP.Mantenimiento.Data
{
    public class ServicioSAP
    {
        #region Atributos
        private const string TABLE_SERVICE_NAME = "VSKF_SERVICIOS";
        public Servicio oServicio { get; set; }
        public UserTable oUDT { get; set; }
        private Company Company;
        public string ItemCode { get; set; }
        public int? Sincronizado { get; set; }
        public Guid UUID { get; set; }
        #endregion

        #region Constructor
        public ServicioSAP(Company company)
        {
            this.Company = company;
            this.oServicio = new Servicio
            {
                Propietario = new Empresa(),
                SubPropietario = new Sucursal(),
                TipoServicio = new TipoServicio(),
            };
            try
            {
                this.oUDT = company.UserTables.Item(TABLE_SERVICE_NAME);
            }
            catch(Exception ex)
            {
                this.oUDT = null;
            }
        }
        #endregion

        public bool Existe()
        {
            return this.oUDT.GetByKey(this.ItemCode);
        }

        public Recordset GetSAPServicioList()
        {
            Recordset rs = (Recordset)this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
            rs.DoQuery(string.Format("select * from [@{0}] ", TABLE_SERVICE_NAME));
            return rs;
        }

        /// <summary>
        /// Establece los valores de la interfaz de usuario a un objeto SAP UserTable
        /// </summary>
        public void ServicioToUDT()
        {
            if (this.oServicio.SubPropietario == null) this.oServicio.SubPropietario = new Sucursal();
            int ? activo = 0;
            if(Convert.ToBoolean(this.oServicio.EsActivo))
                activo = 1;
            this.oUDT.Code = ItemCode ?? "0"; //this.oServicio.ServicioID.ToString() ?? ItemCode;
            this.oUDT.Name = this.oServicio.Nombre;
            this.oUDT.UserFields.Fields.Item("U_ServicioID").Value = this.oServicio.ServicioID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_Nombre").Value = this.oServicio.Nombre ?? string.Empty; ;
            this.oUDT.UserFields.Fields.Item("U_Descripcion").Value = this.oServicio.Descripcion ?? string.Empty; ;
            this.oUDT.UserFields.Fields.Item("U_ImagenUrl").Value = this.oServicio.ImagenUrl ?? string.Empty; ;
            this.oUDT.UserFields.Fields.Item("U_SucursalID").Value = this.oServicio.SubPropietario.SucursalID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_TipoServicioID").Value = this.oServicio.TipoServicio.TipoServicioID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value = this.oServicio.Propietario.EmpresaID ?? 0;
            this.oUDT.UserFields.Fields.Item("U_EsActivo").Value = activo;
            this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Sincronizado ?? 0;
            this.oUDT.UserFields.Fields.Item("U_UUID").Value = this.UUID.ToString() ?? string.Empty;
            this.oUDT.UserFields.Fields.Item("U_CuentaContable").Value = this.oServicio.CuentaContable.Trim() ?? string.Empty;

        }

        public void Insertar()
        {
            int i = oUDT.Add();
            if (i != 0)
            {
                int errornum;
                string errormns;
                Company.GetLastError(out errornum, out errormns);
                throw new Exception("Error: " + errornum + " - " + errormns);
            }
        }

        public void Actualizar()
        {
            try
            {
                int i = oUDT.Update();
                if (i != 0)
                {
                    int errornum;
                    string errormns;
                    Company.GetLastError(out errornum, out errormns);
                    throw new Exception("Error: " + errornum + " - " + errormns);
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar()
        {
            try
            {
                int i = oUDT.Remove();
                if (i != 0)
                {
                    int errornum;
                    string errormns;
                    Company.GetLastError(out errornum, out errormns);
                    throw new Exception("Error: " + errornum + " - " + errormns);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void EliminarParametroServicio()
        {
            try
            {
                ParametroServicioData psData = new ParametroServicioData(this.Company);
                List<ParametroServicio> lstParametroServicio = new List<ParametroServicio>();
                ParametroServicio ps = new ParametroServicio();
                ps.Servicio = new Servicio() { ServicioID = this.oServicio.ServicioID };
                Recordset rs = psData.Consultar(ps);
                if(psData.HashParametroServicio(rs))
                {
                    lstParametroServicio = psData.RecordSetToList(rs);
                    foreach(ParametroServicio data in lstParametroServicio)
                    {
                        psData.ItemCode = data.ParametroServicioID.ToString();
                        if(psData.Existe())
                        {
                            psData.oParametroServicio = data;
                            psData.ParametroServicioToUDT();
                            psData.EliminarCompleto();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void EliminarCompleto()
        {
            try
            {
                this.EliminarParametroServicio();
                this.Deactivate();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Recordset Consultar(Servicio sv)
        {
            try
            {
                Recordset rs = this.Consultar(sv, false);
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Recordset Consultar(Servicio sv, bool? isConsiderable)
        {
            try
            {
                Recordset rs = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                if (sv == null) sv = new Servicio();
                if (sv.SubPropietario == null) sv.SubPropietario = new Sucursal();
                if (sv.Propietario == null) sv.Propietario = new Empresa();
                if(sv.TipoServicio == null) sv.TipoServicio = new TipoServicio();
                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                //query.Append(" SELECT sv.U_ServicioID,sv.U_Nombre as Servicio,sv.U_Descripcion,sv.U_ImagenUrl, ");
                //query.Append(" sv.U_SucursalID,sv.U_TipoServicioID,sv.U_EmpresaID,tps.U_Nombre as TipoServicio ");//, sv.U_Sincronizado, sv.U_UUID ");
                //query.Append(" FROM [@VSKF_SERVICIOS] as sv inner join [@VSKF_TIPOSERVICIO] tps on sv.U_TipoServicioID = tps.U_TipoServicioID ");
                query.Append(" SELECT sv.U_ServicioID,sv.U_Nombre as Servicio,sv.U_Descripcion,sv.U_EsActivo, sv.U_EmpresaID,sv.U_SucursalID,sv.U_ImagenUrl,sv.U_TipoServicioID, sc.U_Direccion as Sucursal,tps.U_Nombre as TipoServicio ");
                query.Append(" FROM [@VSKF_SERVICIOS] as sv left outer join [@VSKF_SUCURSAL] sc on sc.U_SucursalID = sv.U_SucursalID left outer join [@VSKF_TIPOSERVICIO] tps on sv.U_TipoServicioID = tps.U_TipoServicioID ");
                if (sv.ServicioID != null)
                    where.Append(string.Format("and sv.U_ServicioID = {0} ", sv.ServicioID));
                if(sv.Nombre != null)
                    where.Append(string.Format("and sv.U_Nombre = '{0}' ", sv.Nombre));
                if(sv.Descripcion != null)
                    where.Append(string.Format("and sv.U_Descripcion = '{0}' ", sv.Descripcion));
                if(sv.ImagenUrl != null)
                    where.Append(string.Format("and sv.U_ImagenUrl = '{0}' ", sv.ImagenUrl));
                if (sv.TipoServicio.TipoServicioID != null)
                    where.Append(string.Format("and sv.U_TipoServicioID = {0} ", sv.TipoServicio.TipoServicioID));
                if (sv.Propietario.EmpresaID != null)
                    where.Append(string.Format("and sv.U_EmpresaID = {0} ", sv.Propietario.EmpresaID));
                if(!(bool)isConsiderable)
                {
                    if (sv.SubPropietario.SucursalID != null)
                        where.Append(string.Format("and sv.U_SucursalID = {0} ", sv.SubPropietario.SucursalID));
                }
                if (sv.TipoServicio.Nombre != null)
                    where.Append(string.Format("and tps.U_Nombre = '{0}' ", sv.TipoServicio.Nombre));
                if(sv.EsActivo != null)
                    where.Append("and sv.U_EsActivo = 1 ");
                if(where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith("and"))
                        filtro = filtro.Substring(3);
                    query.Append(" where " + filtro);
                }
                query.Append(" order by sv.U_ServicioID desc ");
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Servicio LastRecordSetToServicio(Recordset rs)
        {
            try
            {
                Servicio sv = new Servicio();
                sv.SubPropietario = new Sucursal();
                sv.Propietario = new Empresa();
                sv.TipoServicio = new TipoServicio();
                rs.MoveFirst();
                if (rs.Fields.Item("U_ServicioID").Value != null || rs.Fields.Item("U_ServicioID").Value > 0)
                    sv.ServicioID = Convert.ToInt32(rs.Fields.Item("U_ServicioID").Value);
                if (rs.Fields.Item("Servicio").Value != null)
                    sv.Nombre = rs.Fields.Item("Servicio").Value;
                if (rs.Fields.Item("U_Descripcion").Value != null)
                    sv.Descripcion = rs.Fields.Item("U_Descripcion").Value;
                if (rs.Fields.Item("U_EsActivo").Value != null)
                    sv.EsActivo = Convert.ToBoolean(rs.Fields.Item("U_EsActivo").Value);
                if (rs.Fields.Item("U_EmpresaID").Value != null || rs.Fields.Item("U_EmpresaID").Value > 0)
                    sv.Propietario.EmpresaID = Convert.ToInt32(rs.Fields.Item("U_EmpresaID").Value);
                if (rs.Fields.Item("U_SucursalID").Value != null || rs.Fields.Item("U_SucursalID").Value > 0)
                    sv.SubPropietario.SucursalID = Convert.ToInt32(rs.Fields.Item("U_SucursalID").Value);
                if (rs.Fields.Item("U_ImagenUrl").Value != null)
                    sv.ImagenUrl = rs.Fields.Item("U_ImagenUrl").Value;
                if (rs.Fields.Item("U_TipoServicioID").Value != null || rs.Fields.Item("U_TipoServicioID").Value > 0)
                    sv.TipoServicio.TipoServicioID = Convert.ToInt32(rs.Fields.Item("U_TipoServicioID").Value);
                if (rs.Fields.Item("Sucursal").Value != null)
                    sv.SubPropietario.Direccion = rs.Fields.Item("Sucursal").Value;
                if (rs.Fields.Item("TipoServicio").Value != null)
                    sv.TipoServicio.Nombre = rs.Fields.Item("TipoServicio").Value;

                if (rs.Fields.Item("U_CuentaContable").Value != null)
                    sv.CuentaContable = rs.Fields.Item("U_CuentaContable").Value;
                //if (rs.Fields.Item("U_Sincronizado").Value != null)
                //    sv.Sincronizacion = (TipoSync)rs.Fields.Item("U_Sincronizado").Value;
                //if (rs.Fields.Item("U_UUID").Value != null)
                //    sv.UUID = rs.Fields.Item("U_UUID").Value;

                return sv;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Servicio RowRecordSetToServicio(Recordset rs)
        {
            try
            {
                Servicio sv = new Servicio();
                sv.SubPropietario = new Sucursal();
                sv.Propietario = new Empresa();
                sv.TipoServicio = new TipoServicio();
                //rs.MoveFirst();
                if (rs.Fields.Item("U_ServicioID").Value != null || rs.Fields.Item("U_ServicioID").Value > 0)
                    sv.ServicioID = Convert.ToInt32(rs.Fields.Item("U_ServicioID").Value);
                if (rs.Fields.Item("Servicio").Value != null)
                    sv.Nombre = rs.Fields.Item("Servicio").Value;
                if (rs.Fields.Item("U_Descripcion").Value != null)
                    sv.Descripcion = rs.Fields.Item("U_Descripcion").Value;
                if (rs.Fields.Item("U_EsActivo").Value != null)
                    sv.EsActivo = Convert.ToBoolean(rs.Fields.Item("U_EsActivo").Value);
                if (rs.Fields.Item("U_EmpresaID").Value != null || rs.Fields.Item("U_EmpresaID").Value > 0)
                    sv.Propietario.EmpresaID = Convert.ToInt32(rs.Fields.Item("U_EmpresaID").Value);
                if (rs.Fields.Item("U_SucursalID").Value != null || rs.Fields.Item("U_SucursalID").Value > 0)
                    sv.SubPropietario.SucursalID = Convert.ToInt32(rs.Fields.Item("U_SucursalID").Value);
                if (rs.Fields.Item("U_ImagenUrl").Value != null)
                    sv.ImagenUrl = rs.Fields.Item("U_ImagenUrl").Value;
                if (rs.Fields.Item("U_TipoServicioID").Value != null || rs.Fields.Item("U_TipoServicioID").Value > 0)
                    sv.TipoServicio.TipoServicioID = Convert.ToInt32(rs.Fields.Item("U_TipoServicioID").Value);
                if (rs.Fields.Item("Sucursal").Value != null)
                    sv.SubPropietario.Direccion = rs.Fields.Item("Sucursal").Value;
                if (rs.Fields.Item("TipoServicio").Value != null)
                    sv.TipoServicio.Nombre = rs.Fields.Item("TipoServicio").Value;

                return sv;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Servicio> RecordSetToListServicio(Recordset rs)
        {
            try
            {
                List<Servicio> lstRet = new List<Servicio>();
                rs.MoveFirst();
                for(int i = 0; i < rs.RecordCount; i++)
                {
                    lstRet.Add(RowRecordSetToServicio(rs));
                    rs.MoveNext();
                }
                return lstRet;
            }
            catch (Exception ex) { throw new Exception("Error al convertir RecordSet a List<Servicio>: " + ex.Message); }
        }

        public bool HashServicios(Recordset rs)
        {
            if (rs.RecordCount < 1)
                return false;
            return true;
        }

        public bool SetUDTByServicioID(int? id)
        {
            try
            {
                var rs = this.ConsultarByID(id);
                int _id = 0;
                if (HashServicios(rs))
                {
                    rs.MoveFirst();
                    _id = rs.Fields.Item("U_ServicioID").Value;
                    if (_id == id)
                        return this.oUDT.GetByKey(rs.Fields.Item("Code").Value.ToString());

                }
                this.oUDT.Code = string.Empty;
                return false;
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }

        public Recordset ConsultarByID(int? id)
        {
            try
            {
                Recordset rs = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(string.Format("select * from [@VSKF_SERVICIOS] where U_ServicioID = {0} and U_EsActivo = 1", id));
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Deactivate()
        {
            this.oUDT.UserFields.Fields.Item("U_EsActivo").Value = 0;
            Actualizar();
        }

        public void UDTToServicio()
        {
            try
            {
                oServicio = new Servicio { Propietario = new Empresa(), SubPropietario = new Sucursal(), TipoServicio = new TipoServicio() };
                oServicio.ServicioID = (int)this.oUDT.UserFields.Fields.Item("U_ServicioID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_ServicioID").Value;
                oServicio.Nombre = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Nombre").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Nombre").Value;
                oServicio.Descripcion = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Descripcion").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Descripcion").Value;
                oServicio.ImagenUrl = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Descripcion").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Descripcion").Value;
                oServicio.SubPropietario.SucursalID = (int)this.oUDT.UserFields.Fields.Item("U_SucursalID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_SucursalID").Value;
                oServicio.TipoServicio.TipoServicioID = (int)this.oUDT.UserFields.Fields.Item("U_TipoServicioID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_TipoServicioID").Value;
                oServicio.Propietario.EmpresaID = (int)this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value;
                oServicio.EsActivo = this.oUDT.UserFields.Fields.Item("U_EsActivo").Value == 1;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Sincronizar()
        {
            try
            {
                this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Sincronizado == null ? 0 : this.Sincronizado;
                this.Actualizar();
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public Recordset ActualizarCode(Servicio sv)
        {
            try
            {
                Recordset rs = this.Company.GetBusinessObject(BoObjectTypes.BoRecordset);

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(" UPDATE [@VSKF_SERVICIOS] SET Code = '" + sv.ServicioID.ToString() +
                    "', U_ServicioID = " + sv.ServicioID + ", U_Sincronizado = " + Sincronizado);

                if (UUID != null)
                    where.Append(string.Format(" and U_UUID like '%{0}%' ", UUID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void GetSyncUUID()
        {
            try
            {
                if (!string.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_UUID").Value))
                    this.UUID = Guid.Parse(this.oUDT.UserFields.Fields.Item("U_UUID").Value);
                this.Sincronizado = this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
    }
}
