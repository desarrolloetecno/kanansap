﻿using Kanan.Mantenimiento.BO2;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KananSAP.Mantenimiento.Data
{
    public class TipoServicioData
    {

        #region Atributos
        private Company oCompany;
        public UserTable oUDT { get; set; }
        public Kanan.Mantenimiento.BO2.TipoServicio oTipoServicio { get; set; }
        private const string TABLE_TSERVICIO = "VSKF_TIPOSERVICIO";
        public string ItemCode { get; set; }
        public int? Sincronizado { get; set; }
        public Guid UUID { get; set; }
        #endregion

        #region Constructor
        public TipoServicioData(Company company)
        {
            this.oCompany = company;
            this.oTipoServicio = new Kanan.Mantenimiento.BO2.TipoServicio();
            this.oTipoServicio.Propietario = new Kanan.Operaciones.BO2.Empresa();
            this.oTipoServicio.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
            try
            {
                this.oUDT = this.oCompany.UserTables.Item(TABLE_TSERVICIO);
            }
            catch(Exception ex)
            {
                this.oUDT = null;
            }
        }
        #endregion

        public bool Existe()
        {
            return this.oUDT.GetByKey(this.ItemCode);
        }

        public void TipoServicioToUDT()
        {
            try
            {
                if (this.oTipoServicio.SubPropietario == null) this.oTipoServicio.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
                int activo = 0;
                if (Convert.ToBoolean(oTipoServicio.EsActivo))
                    activo = 1;
                oUDT.Code = oTipoServicio.TipoServicioID.ToString() ?? this.ItemCode;
                oUDT.Name =  this.ItemCode ?? oTipoServicio.TipoServicioID.ToString() ;
                oUDT.UserFields.Fields.Item("U_TipoServicioID").Value = oTipoServicio.TipoServicioID ?? 0;
                oUDT.UserFields.Fields.Item("U_Nombre").Value = oTipoServicio.Nombre ?? string.Empty;
                oUDT.UserFields.Fields.Item("U_Descripcion").Value = oTipoServicio.Descripcion ?? string.Empty;
                oUDT.UserFields.Fields.Item("U_EmpresaID").Value = oTipoServicio.Propietario.EmpresaID ?? 0;
                oUDT.UserFields.Fields.Item("U_SucursalID").Value = oTipoServicio.SubPropietario.SucursalID ?? 0;
                oUDT.UserFields.Fields.Item("U_EsActivo").Value = activo;
                oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Sincronizado ?? 0;
                oUDT.UserFields.Fields.Item("U_UUID").Value = this.UUID.ToString() ?? string.Empty;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Inserta()
        {
            try
            {
                string err;
                int code;
                int result = -1;
                result = this.oUDT.Add();
                if(result != 0)
                {
                    this.oCompany.GetLastError(out code, out err);
                    throw new Exception(string.Format("ErrorCode: {0}, ErrorMessage:{1}.", code, err));
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Actualizar()
        {
            try
            {
                string err;
                int code;
                int result = -1;
                result = this.oUDT.Update();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out code, out err);
                    throw new Exception(string.Format("ErrorCode: {0}, ErrorMessage:{1}.", code, err));
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void ActualizarCompleto()
        {
            try
            {
                ServicioSAP svData = new ServicioSAP(this.oCompany);
                List<Servicio> servicios = new List<Servicio>();
                Servicio sv = new Servicio();
                sv.TipoServicio = this.oTipoServicio;
                //sv.Propietario = this.oTipoServicio.Propietario;
                //sv.SubPropietario = this.oTipoServicio.SubPropietario;
                Recordset rs = svData.Consultar(sv);
                if(svData.HashServicios(rs))
                {
                    servicios = svData.RecordSetToListServicio(rs);
                    foreach (Servicio data in servicios)
                    {
                        if(svData.SetUDTByServicioID(data.ServicioID))
                        {
                            //svData.oServicio = data;
                            //svData.ServicioToUDT();
                            svData.UDTToServicio();
                            svData.EliminarCompleto();
                        }
                    }
                }
                this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Sincronizado ?? 0;
                this.Deactivate();
            }
            catch (Exception ex)
            {
                throw new Exception("ActualizarCompleto: " + ex.Message);
            }
        }

        public void Eliminar()
        {
            try
            {
                string err;
                int code;
                int result = -1;
                result = this.oUDT.Remove();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out code, out err);
                    throw new Exception(string.Format("ErrorCode: {0}, ErrorMessage:{1}.", code, err));
                }
            }
            catch (Exception ex)
            {

            }
        }

        public Recordset Consultar(TipoServicio ts)
        {
            try
            {
                if (ts == null) ts = new TipoServicio();
                if (ts.Propietario == null) ts.Propietario = new Kanan.Operaciones.BO2.Empresa();
                if (ts.SubPropietario == null) ts.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(" SELECT ts.U_TipoServicioID, ts.U_Nombre, ts.U_Descripcion,ts.U_EmpresaID, ts.U_SucursalID, ts.U_EsActivo,sc.U_Direccion as Sucursal from [@VSKF_TIPOSERVICIO] ts left outer  join [@VSKF_SUCURSAL] sc on sc.U_SucursalID= ts.U_SucursalID  ");
                if (ts.TipoServicioID != null)
                    where.Append(string.Format("and ts.U_TipoServicioID = {0} ", ts.TipoServicioID));
                if (ts.Nombre != null)
                    where.Append(string.Format("and ts.U_Nombre = '{0}' ", ts.Nombre));
                if (ts.Descripcion != null)
                    where.Append(string.Format("and ts.U_Descripcion like '%{0}%' ", ts.Descripcion));
                if (ts.Propietario.EmpresaID != null)
                    where.Append(string.Format("and ts.U_EmpresaID = {0} ", ts.Propietario.EmpresaID));
                if (ts.SubPropietario.SucursalID != null)
                    where.Append(string.Format("and ts.U_SucursalID = {0} ", ts.SubPropietario.SucursalID));
                if (ts.EsActivo != null)
                    where.Append(string.Format("and ts.U_EsActivo = {0} ", Convert.ToInt32(ts.EsActivo)));
                
                if(where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith("and"))
                        filtro = filtro.Substring(3);
                    query.Append(" where " + filtro);
                }
                query.Append(" order by ts.U_TipoServicioID asc ");
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool HasTipoServicio(Recordset rs)
        {
            if (rs.RecordCount < 1)
                return false;
            return true;
        }

        public TipoServicio LastRecordSetToTipoServicio(Recordset rs)
        {
            try
            {
                TipoServicio ts = new TipoServicio();
                ts.Propietario = new Kanan.Operaciones.BO2.Empresa();
                ts.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
                rs.MoveFirst();
                if(rs.Fields.Item("U_TipoServicioID").Value != null && rs.Fields.Item("U_TipoServicioID").Value > 0)
                    ts.TipoServicioID = Convert.ToInt32(rs.Fields.Item("U_TipoServicioID").Value);
                if (!string.IsNullOrEmpty(rs.Fields.Item("U_Nombre").Value))
                    ts.Nombre = rs.Fields.Item("U_Nombre").Value;
                if (!string.IsNullOrEmpty(rs.Fields.Item("U_Descripcion").Value))
                    ts.Descripcion = rs.Fields.Item("U_Descripcion").Value;
                if (rs.Fields.Item("U_EmpresaID").Value != null && rs.Fields.Item("U_EmpresaID").Value > 0)
                    ts.Propietario.EmpresaID = Convert.ToInt32(rs.Fields.Item("U_EmpresaID").Value);
                if (rs.Fields.Item("U_SucursalID").Value != null && rs.Fields.Item("U_SucursalID").Value > 0)
                    ts.SubPropietario.SucursalID = Convert.ToInt32(rs.Fields.Item("U_SucursalID").Value);
                if (rs.Fields.Item("U_EsActivo").Value != null && rs.Fields.Item("U_EsActivo").Value > 0)
                    ts.EsActivo = Convert.ToBoolean(rs.Fields.Item("U_EsActivo").Value);
                if (rs.Fields.Item("Sucursal").Value != null)
                    ts.SubPropietario.Direccion = rs.Fields.Item("Sucursal").Value;
                return ts;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public TipoServicio RowRecordSetToTipoServicio(Recordset rs)
        {
            try
            {
                TipoServicio ts = new TipoServicio();
                ts.Propietario = new Kanan.Operaciones.BO2.Empresa();
                ts.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
                if (rs.Fields.Item("U_TipoServicioID").Value != null && rs.Fields.Item("U_TipoServicioID").Value > 0)
                    ts.TipoServicioID = Convert.ToInt32(rs.Fields.Item("U_TipoServicioID").Value);
                if (!string.IsNullOrEmpty(rs.Fields.Item("U_Nombre").Value))
                    ts.Nombre = rs.Fields.Item("U_Nombre").Value;
                if (!string.IsNullOrEmpty(rs.Fields.Item("U_Descripcion").Value))
                    ts.Descripcion = rs.Fields.Item("U_Descripcion").Value;
                if (rs.Fields.Item("U_EmpresaID").Value != null && rs.Fields.Item("U_EmpresaID").Value > 0)
                    ts.Propietario.EmpresaID = Convert.ToInt32(rs.Fields.Item("U_EmpresaID").Value);
                if (rs.Fields.Item("U_SucursalID").Value != null && rs.Fields.Item("U_SucursalID").Value > 0)
                    ts.SubPropietario.SucursalID = Convert.ToInt32(rs.Fields.Item("U_SucursalID").Value);
                if (rs.Fields.Item("U_EsActivo").Value != null && rs.Fields.Item("U_EsActivo").Value > 0)
                    ts.EsActivo = Convert.ToBoolean(rs.Fields.Item("U_EsActivo").Value);
                if (rs.Fields.Item("Sucursal").Value != null)
                    ts.SubPropietario.Direccion = rs.Fields.Item("Sucursal").Value;
                return ts;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<TipoServicio> RecordSetToListTipoServicio(Recordset rs)
        {
            try
            {
                List<TipoServicio> list = new List<TipoServicio>();
                rs.MoveFirst();
                for(int i = 0; i < rs.RecordCount; i++)
                {
                    list.Add(this.RowRecordSetToTipoServicio(rs));
                    rs.MoveNext();
                }
                return list;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public bool SetUDTByServicioID(int? id)
        {
            try
            {
                var rs = this.ConsultarByID(id);
                int _id = 0;
                if (HasTipoServicio(rs))
                {
                    rs.MoveFirst();
                    _id = rs.Fields.Item("U_TipoServicioID").Value;
                    if (_id == id)
                        return this.oUDT.GetByKey(rs.Fields.Item("Code").Value.ToString());

                }
                this.oUDT.Code = string.Empty;
                return false;
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }

        public Recordset ConsultarByID(int? id)
        {
            try
            {
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(string.Format("select * from [@VSKF_TIPOSERVICIO] where U_TipoServicioID = {0} and U_EsActivo = 1", id));
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Deactivate()
        {
            this.oUDT.UserFields.Fields.Item("U_EsActivo").Value = 0;
            Actualizar();
        }

        public void UDTToTipoServicio()
        {
            try
            {
                oTipoServicio = new TipoServicio { Propietario = new Kanan.Operaciones.BO2.Empresa(), SubPropietario = new Kanan.Operaciones.BO2.Sucursal() };
                oTipoServicio.TipoServicioID = (int)this.oUDT.UserFields.Fields.Item("U_TipoServicioID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_TipoServicioID").Value;
                oTipoServicio.Nombre = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Nombre").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Nombre").Value;
                oTipoServicio.Descripcion = string.IsNullOrEmpty((string)this.oUDT.UserFields.Fields.Item("U_Descripcion").Value) ? null : (string)this.oUDT.UserFields.Fields.Item("U_Descripcion").Value;
                oTipoServicio.Propietario.EmpresaID = (int)this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value;
                oTipoServicio.SubPropietario.SucursalID = (int)this.oUDT.UserFields.Fields.Item("U_SucursalID").Value == 0 ? null : (int?)this.oUDT.UserFields.Fields.Item("U_SucursalID").Value;
                oTipoServicio.EsActivo = this.oUDT.UserFields.Fields.Item("U_EsActivo").Value == 1;

            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public Recordset ActualizarCode(TipoServicio ts)
        {
            try
            {
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(" UPDATE [@VSKF_TIPOSERVICIO] SET Code = '" + ts.TipoServicioID.ToString() +
                    "', U_TipoServicioID = " + ts.TipoServicioID + ", U_Sincronizado = " + Sincronizado);

                if (UUID != null)
                    where.Append(string.Format(" and U_UUID like '%{0}%' ", UUID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void GetSyncUUID()
        {
            try
            {
                if (!string.IsNullOrEmpty(this.oUDT.UserFields.Fields.Item("U_UUID").Value))
                    this.UUID = Guid.Parse(this.oUDT.UserFields.Fields.Item("U_UUID").Value);
                this.Sincronizado = this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void Sincronizar()
        {
            try
            {
                this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Sincronizado == null ? 0 : this.Sincronizado;
                this.Actualizar();
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

    }
}
