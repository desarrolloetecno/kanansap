﻿using Kanan.Mantenimiento.BO2;
using Kanan.Vehiculos.BO2;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KananSAP.Mantenimiento.Data
{
    public class DetalleOrdenSAP
    {
        #region Atributos
        private const string TABLE_DETALLEORDEN = "VSKF_DETALLEORDEN";
        public DetalleOrden oDetalleOrden { get; set; }
        public UserTable oUDT { get; set; }
        private Company oCompany;
        public string ItemCode { get; set; }
        /// <summary>
        /// 0 = Sincronizado, 1 = Insertando, 2 = Actualizando, 3 = Eliminando
        /// </summary>
        public int? Sincronizado { get; set; }
        /// <summary>
        /// Guid que se tomará para la sincronización SAP <-> Kanan Fleet
        /// </summary>
        public Guid UUID { get; set; }
        #endregion

        #region Contructor
        public DetalleOrdenSAP(Company company)
        {
            this.oCompany = company;
            try
            {
                this.oUDT = this.oCompany.UserTables.Item(TABLE_DETALLEORDEN);
            }
            catch
            {
                this.oUDT = null;
            }
        }
        #endregion

        public bool Existe()
        {
            return this.oUDT.GetByKey(this.ItemCode);
        }

        public void DetalleOrdenToUDT()
        {
            try
            {
                this.oUDT.Code = this.ItemCode ?? this.oDetalleOrden.DetalleOrdenID.ToString();
                this.oUDT.Name = this.ItemCode ?? this.oDetalleOrden.DetalleOrdenID.ToString();
                this.oUDT.UserFields.Fields.Item("U_MantenibleID").Value = this.oDetalleOrden.Mantenible.MantenibleID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_ServicioID").Value = this.oDetalleOrden.Servicio.ServicioID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_OrdenServicioID").Value = this.oDetalleOrden.OrdenServicio.OrdenServicioID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value = this.oDetalleOrden.TipoMantenibleID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_Costo").Value = (double?)this.oDetalleOrden.Costo ?? 0;
                this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Sincronizado == null ? 0 : this.Sincronizado;
                this.oUDT.UserFields.Fields.Item("U_UUID").Value = this.UUID == null ? string.Empty : this.UUID.ToString();
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        
        private void InitializeDetalleOrden()
        {
            this.oDetalleOrden = new DetalleOrden();
        }

        public void UDTTooDetalleOrden()
        {
            try
            {
                this.InitializeDetalleOrden();
                int id;
                if (int.TryParse(this.oUDT.Code, out id))
                    this.oDetalleOrden.DetalleOrdenID = id;
                else
                    this.oDetalleOrden.DetalleOrdenID = null;
                this.oDetalleOrden.Servicio.ServicioID = this.oUDT.UserFields.Fields.Item("U_ServicioID").Value ?? null;
                this.oDetalleOrden.OrdenServicio.OrdenServicioID = this.oUDT.UserFields.Fields.Item("U_OrdenServicioID").Value ?? null;
                this.oDetalleOrden.TipoMantenibleID = this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value ?? null;
                if(this.oDetalleOrden.TipoMantenibleID !=null)
                {
                    if (this.oDetalleOrden.TipoMantenibleID == 1)
                        this.oDetalleOrden.Mantenible = new Vehiculo();
                    else if (this.oDetalleOrden.TipoMantenibleID == 2)
                        this.oDetalleOrden.Mantenible = new Caja();
                    this.oDetalleOrden.Mantenible.MantenibleID = this.oUDT.UserFields.Fields.Item("U_MantenibleID").Value ?? null;
                }
                this.oDetalleOrden.Costo = (decimal?)Convert.ChangeType(this.oUDT.UserFields.Fields.Item("U_Costo").Value, typeof(decimal)) ?? Convert.ToDecimal(0);
                this.Sincronizado = this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value ?? null;
                this.UUID = Guid.Parse(this.oUDT.UserFields.Fields.Item("U_UUID").Value) ?? null;
                
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public bool SetUDTDetalleOrdenID(int? id)
        {
            try
            {
                var rs = this.ConsultarByID(id);
                if (HashoDetalleOrden(rs))
                {
                    rs.MoveFirst();
                    int _id = rs.Fields.Item("Code").Value;
                    if (_id == id)
                        return this.oUDT.GetByKey(rs.Fields.Item("Code").Value);
                }
                this.oUDT.Code = string.Empty;
                return false;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public Recordset ConsultarByID(int? id)
        {
            try
            {
                string query = string.Format("select * from [@{0}] where Code = {1}", TABLE_DETALLEORDEN, id);
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                return rs;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public bool HashoDetalleOrden(Recordset rs)
        {
            if (rs.RecordCount > 0)
                return true;
            return false;
        }

        public void Insert()
        {
            int result = -1;
            try
            {
                result = this.oUDT.Add();
                if (result != 0)
                {
                    int errornum;
                    string errormsj;
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Actualizar()
        {
            int result = -1;
            try
            {
                int errornum;
                string errormsj;
                result = this.oUDT.Update();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar()
        {
            int result = -1;
            try
            {
                int errornum;
                string errormsj;
                result = this.oUDT.Remove();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Recordset ActualizarCode(DetalleOrden detalle)
        {
            try
            {
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                string strUpdate = string.Format("UPDATE [@VSKF_DETALLEORDEN] SET Code = '{0}', Name = '{0}', U_Sincronizado = {1}, U_OrdenServicioID = {2} ", detalle.DetalleOrdenID,
                    this.Sincronizado, detalle.OrdenServicio.OrdenServicioID);
                query.Append(strUpdate);
                //query.Append(" UPDATE [@VSKF_ORDENSERVICIO] SET Code = '" + detalle.DetalleOrdenID + "', U_Sincronizado = " + Sincronizado + ", Name = ' " + detalle.DetalleOrdenID + "'");
                //if (detalle.OrdenServicio != null && detalle.OrdenServicio.OrdenServicioID != null)
                //    query.Append(string.Format(", U_OrdenServicioID = {0} ", detalle.OrdenServicio.OrdenServicioID));
                if (UUID != null)
                    where.Append(string.Format(" and U_UUID like '%{0}%' ", UUID));
                //if (detalle.Mantenible != null && detalle.Mantenible.MantenibleID != null)
                //    where.Append(string.Format(" and U_MantenibleID = {0} ", detalle.Mantenible.MantenibleID));
                //if(detalle.Servicio != null && detalle.Servicio.ServicioID != null)
                //    where.Append(string.Format(" and U_ServicioID = {0} ", detalle.Servicio.ServicioID));
                //if(detalle.TipoMantenibleID != null)
                //    where.Append(string.Format(" and U_TipoMantenibleID = {0} ", detalle.TipoMantenibleID));
                //if (detalle.Costo != null)
                //    where.Append(string.Format(" and U_Costo = {0} ", detalle.Costo));

                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
