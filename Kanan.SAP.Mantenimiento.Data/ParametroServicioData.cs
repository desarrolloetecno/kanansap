﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kanan.Mantenimiento.BO2;
using Kanan.Vehiculos.BO2;
using SAPbobsCOM;


namespace KananSAP.Mantenimiento.Data
{
    public class ParametroServicioData
    {
        #region Atributos
        private const string TABLE_SERVICIO = "VSKF_PMTROSERVICIO";
        public ParametroServicio oParametroServicio { get; set; }
        public UserTable oUDT { get; set; }
        public string ItemCode { get; set; }
        private Company oCompany;
        #endregion

        #region Constructor
        public ParametroServicioData(Company company)
        {
            this.oCompany = company;
            try
            {
                this.oUDT = this.oCompany.UserTables.Item(TABLE_SERVICIO);
            }
            catch(Exception ex)
            {
                this.oUDT = null;
            }
        }
        #endregion
        /// <summary>
        /// Verifica si existe un registro por medio de Code
        /// </summary>
        /// <returns></returns>
        public bool Existe()
        {
            return this.oUDT.GetByKey(this.ItemCode);
        }
        /// <summary>
        /// Asigna los valores del parametro de servicio al objeto oUDT
        /// </summary>
        private void PrepareUserTable()
        {
            try
            {
                this.oUDT.Code = this.oParametroServicio.ParametroServicioID.ToString();
                this.oUDT.Name = this.oParametroServicio.ParametroServicioID.ToString() ?? string.Empty;
                this.oUDT.UserFields.Fields.Item("U_PServID").Value = this.oParametroServicio.ParametroServicioID;
                this.oUDT.UserFields.Fields.Item("U_ServicioID").Value = oParametroServicio.Servicio.ServicioID ?? 0;
                if (oParametroServicio.Mantenible != null)
                {
                    int? tipoMant = null;
                    if (oParametroServicio.Mantenible.GetType() == typeof(Vehiculo))
                        tipoMant = 1;
                    else if (oParametroServicio.Mantenible.GetType() == typeof(Caja))
                        tipoMant = 2;
                    else if (oParametroServicio.Mantenible.GetType() == typeof(TipoVehiculo))
                        tipoMant = 3;

                    if (tipoMant != null)
                        this.oUDT.UserFields.Fields.Item("U_TMantbleID").Value = tipoMant;
                    if (oParametroServicio.Mantenible.MantenibleID != null)
                        this.oUDT.UserFields.Fields.Item("U_MantbleID").Value = oParametroServicio.Mantenible.MantenibleID;
                }
                this.oUDT.UserFields.Fields.Item("U_Valor").Value = oParametroServicio.Valor ?? 0;
                this.oUDT.UserFields.Fields.Item("U_Mensaje").Value = oParametroServicio.Mensaje ?? string.Empty;
                this.oUDT.UserFields.Fields.Item("U_Alerta").Value = oParametroServicio.Alerta ?? 0;
                this.oUDT.UserFields.Fields.Item("U_ValTiempo").Value = oParametroServicio.ValorTiempo ?? 0;
                this.oUDT.UserFields.Fields.Item("U_AlTiempo").Value = oParametroServicio.AlertaTiempo ?? 0;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
            
        }
        /// <summary>
        /// Hace una llamada al metodo PrepareUserTable que se encarga de asignar los valores
        /// al UserTable para guardar o modificar en la base de datos
        /// </summary>
        public void ParametroServicioToUDT()
        {
            try
            {
                ValidateData();
                this.PrepareUserTable();
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Valida que los campos obligatorios contengan informacion
        /// </summary>
        private void ValidateData()
        {
            try
            {
                if (this.oParametroServicio == null) throw new Exception("ParametroServicio no puede ser nulo");
                if (this.oParametroServicio.Mantenible == null) throw new Exception("Mantenible no puede ser nulo");
                if (this.oParametroServicio.Servicio == null) throw new Exception("Servicio no puede ser nulo");
                if (this.oParametroServicio.Mantenible.MantenibleID == null) throw new Exception("MantenibleID no puede ser nulo");
                if (this.oParametroServicio.Servicio.ServicioID == null) throw new Exception("ServicioID no puede ser nulo");
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Guarda la configuracion de parametros de servicio
        /// </summary>
        public void Insert()
        {
            int result = -1;
            try
            {
                result = this.oUDT.Add();
                if (result != 0)
                {
                    int errornum;
                    string errormsj;
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Actualiza la configuracion del parametro de servicio
        /// </summary>
        public void Actualizar()
        {
            int result = -1;
            try
            {
                int errornum;
                string errormsj;
                result = this.oUDT.Update();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Actualiza el parametro de servicio y todos los parametros de mantenimiento del tipo de vehiculo seleccionado
        /// </summary>
        public void ActualizarTodos()
        {
            try
            {
                this.Actualizar();
                KananSAP.Mantenimiento.Data.ParametroMantenimientoData pmData = new Mantenimiento.Data.ParametroMantenimientoData(this.oCompany);
                Kanan.Mantenimiento.BO2.ParametroMantenimiento pm = new Kanan.Mantenimiento.BO2.ParametroMantenimiento();
                pm.ParametroServicio = this.oParametroServicio;
                SAPbobsCOM.Recordset rs = pmData.Consultar(pm);
                if (pmData.HasParametroMantenimiento(rs))
                {
                    pm = pmData.LastRecordSetToParametroMantenimiento(rs);
                    if (pmData.SetUDTPMantenimientoID(pm.ParametroMantenimientoID))
                    {
                        pmData.oParametroMantenimiento = pm;
                        pmData.ActualizarPMantenimientoTipoVehiculo();
                    }
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar()
        {
            int result = -1;
            try
            {
                int errornum;
                string errormsj;
                result = this.oUDT.Remove();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void EliminarCompleto()
        {
            try
            {
                ParametroMantenimientoData pmData = new ParametroMantenimientoData(this.oCompany);
                ParametroMantenimiento pm = new ParametroMantenimiento();
                List<ParametroMantenimiento> lstPmtro = new List<ParametroMantenimiento>();
                pm.ParametroServicio = this.oParametroServicio;
                Recordset rs = pmData.Consultar(pm);
                if(pmData.HasParametroMantenimiento(rs))
                {
                    lstPmtro = pmData.RecordSetToList(rs);
                    foreach(ParametroMantenimiento data in lstPmtro)
                    {
                        pmData.ItemCode = data.ParametroMantenimientoID.ToString();
                        if(pmData.Existe())
                        {
                            pmData.oParametroMantenimiento = data;
                            pmData.ParametroMantenimientoToUDT();
                            pmData.Eliminar();
                        }
                    }
                }
                this.Eliminar();
            }
            catch(Exception ex)
            {
                throw new Exception("Eliminar completo: " + ex.Message);
            }
        }

        /// <summary>
        /// Realiza un consulta de acuerdo al parametro de servicio proporcionado, vehiculos
        /// </summary>
        /// <param name="ps"></param>
        /// <returns></returns>
        public Recordset Consultar(ParametroServicio ps)
        {
            try
            {
                Recordset rs = (Recordset)this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                if (ps == null) ps = new ParametroServicio();
                if (ps.Servicio == null) ps.Servicio = new Servicio();
                //if (ps.Mantenible == null) ps.Mantenible = new Vehiculo();
                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append("select ps.U_PServID, ps.U_SucursalID, ps.U_Valor, ps.U_Alerta, ps.U_ValTiempo, ps.U_AlTiempo, ps.U_Mensaje, ps.U_ServicioID, ps.U_TMantbleID, ps.U_MantbleID, ");
                query.Append(" sv.U_Nombre as Servicio, vh.U_NombreVehiculo as Vehiculo, tvh.U_Nombre as TipoVehiculo, tvh.U_Descrip as TVDescrip ");
                query.Append(" from [@VSKF_PMTROSERVICIO] as ps inner join [@VSKF_SERVICIOS] as sv on sv.U_ServicioID = ps.U_ServicioID left outer join [@VSKF_VEHICULO] as vh ");
                query.Append(" on vh.U_VehiculoID = ps.U_MantbleID left outer join [@VSKF_TIPOVEHICULO] as tvh on tvh.U_TVeiculoID = ps.U_MantbleID");
                if (ps.ParametroServicioID != null)
                    where.Append(string.Format("and ps.U_PServID = {0} ", ps.ParametroServicioID));
                if (ps.Servicio.ServicioID != null)
                    where.Append(string.Format("and ps.U_ServicioID = {0} ", ps.Servicio.ServicioID));
                if (ps.Mantenible != null)
                {
                    int tipoMant = 1;
                    if (ps.Mantenible.GetType() == typeof(Vehiculo))
                        tipoMant = 1;
                    else if (ps.Mantenible.GetType() == typeof(Caja))
                        tipoMant = 2;
                    else if (ps.Mantenible.GetType() == typeof(TipoVehiculo))
                        tipoMant = 3;
                    where.Append(string.Format("and ps.U_TMantbleID = {0} ", tipoMant));
                    if(ps.Mantenible.MantenibleID.HasValue)
                        where.Append(string.Format("and ps.U_MantbleID = {0} ", ps.Mantenible.MantenibleID));
                }
                if (ps.Valor != null)
                    where.Append(string.Format("and ps.U_Valor = {0} ", ps.Valor));
                if (!string.IsNullOrEmpty(ps.Mensaje))
                    where.Append(string.Format("and ps.U_Mensaje = '{0}' ", ps.Mensaje));
                if (ps.Alerta != null)
                    where.Append(string.Format("and ps.U_Alerta = {0} ", ps.Alerta));
                if (ps.ValorTiempo != null)
                    where.Append(string.Format("and ps.U_ValTiempo = {0} ", ps.ValorTiempo));
                if (ps.AlertaTiempo != null)
                    where.Append(string.Format("and ps.U_AlTiempo = {0} ", ps.AlertaTiempo));
                if(ps.Servicio.ServicioID != null)
                    where.Append(string.Format("and ps.U_ServicioID = {0} ", ps.Servicio.ServicioID));
                if (!string.IsNullOrEmpty(ps.Servicio.Nombre))
                    where.Append(string.Format("and sv.U_Nombre = '{0}' ", ps.Servicio.Nombre));
                string filtro = string.Empty;
                if(where.Length > 0)
                {
                     filtro = where.ToString();
                    if (filtro.StartsWith("and"))
                        filtro = filtro.Substring(3);
                    query.Append(string.Format(" where {0} ", filtro));
                }
                query.Append(" order by ps.U_PServID desc ");
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Obtiene la ultima fila un recordset y lo convierte en un objeto parametro de servicio
        /// </summary>
        /// <param name="rs"></param>
        /// <returns></returns>
        public ParametroServicio LastRecordSetToParametroServicio(Recordset rs)
        {
            try
            {
                ParametroServicio ps = null;
                if(this.HashParametroServicio(rs))
                {
                    rs.MoveFirst();
                    ps = new ParametroServicio();
                    ps.Servicio = new Servicio();
                    ps.Servicio.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
                    ps.Servicio.Propietario = new Kanan.Operaciones.BO2.Empresa();
                    if (rs.Fields.Item("U_PServID").Value != null)
                        ps.ParametroServicioID = Convert.ToInt32(rs.Fields.Item("U_PServID").Value);
                    if (rs.Fields.Item("U_SucursalID").Value != null || rs.Fields.Item("U_SucursalID").Value > 0)
                        ps.Servicio.SubPropietario.SucursalID = Convert.ToInt32(rs.Fields.Item("U_SucursalID").Value);
                    if (rs.Fields.Item("U_ServicioID").Value != null)
                        ps.Servicio.ServicioID = Convert.ToInt32(rs.Fields.Item("U_ServicioID").Value);
                    if (rs.Fields.Item("U_TMantbleID").Value != null)
                    {
                        int tMant = Convert.ToInt32(rs.Fields.Item("U_TMantbleID").Value);
                        if (tMant == 1)
                        {
                            Vehiculo vh = new Vehiculo();
                            if (rs.Fields.Item("Vehiculo").Value != null)
                                vh.Nombre = rs.Fields.Item("Vehiculo").Value;
                            if (rs.Fields.Item("U_MantbleID").Value != null)
                                vh.VehiculoID = Convert.ToInt32(rs.Fields.Item("U_MantbleID").Value);
                            ps.Mantenible = vh;
                            ps.Mantenible.MantenibleID = vh.VehiculoID;
                        }
                        else if(tMant == 3)
                        {
                            TipoVehiculo tvh = new TipoVehiculo();
                            if (rs.Fields.Item("TipoVehiculo").Value != null)
                                tvh.Nombre = rs.Fields.Item("TipoVehiculo").Value;
                            if (rs.Fields.Item("U_MantbleID").Value != null)
                                tvh.TipoVehiculoID = Convert.ToInt32(rs.Fields.Item("U_MantbleID").Value);
                            if (rs.Fields.Item("TVDescrip").Value != null)
                                tvh.Descripcion = rs.Fields.Item("TVDescrip").Value;
                            ps.Mantenible = tvh;
                            ps.Mantenible.MantenibleID = tvh.TipoVehiculoID;
                        }
                    }
                    
                    if (rs.Fields.Item("U_Valor").Value != null)
                        ps.Valor = Convert.ToDouble(rs.Fields.Item("U_Valor").Value);
                    if (rs.Fields.Item("U_Mensaje").Value != null)
                        ps.Mensaje = rs.Fields.Item("U_Mensaje").Value;
                    if (rs.Fields.Item("U_Alerta").Value != null)
                        ps.Alerta = Convert.ToDouble(rs.Fields.Item("U_Alerta").Value);
                    if (rs.Fields.Item("U_ValTiempo").Value != null)
                        ps.ValorTiempo = Convert.ToInt32(rs.Fields.Item("U_ValTiempo").Value);
                    if (rs.Fields.Item("U_AlTiempo").Value != null)
                        ps.AlertaTiempo = Convert.ToInt32(rs.Fields.Item("U_AlTiempo").Value);
                    if (rs.Fields.Item("Servicio").Value != null)
                        ps.Servicio.Nombre = rs.Fields.Item("Servicio").Value;
                }
                return ps;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ParametroServicio RowRecordSetToParametroServicio(Recordset rs)
        {
            try
            {
                ParametroServicio ps = new ParametroServicio();
                ps.Servicio = new Servicio();
                ps.Servicio.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
                ps.Servicio.Propietario = new Kanan.Operaciones.BO2.Empresa();
                if (rs.Fields.Item("U_PServID").Value != null)
                    ps.ParametroServicioID = Convert.ToInt32(rs.Fields.Item("U_PServID").Value);
                if (rs.Fields.Item("U_SucursalID").Value != null || rs.Fields.Item("U_SucursalID").Value > 0)
                    ps.Servicio.SubPropietario.SucursalID = Convert.ToInt32(rs.Fields.Item("U_SucursalID").Value);
                if (rs.Fields.Item("U_ServicioID").Value != null)
                    ps.Servicio.ServicioID = Convert.ToInt32(rs.Fields.Item("U_ServicioID").Value);
                if (rs.Fields.Item("U_TMantbleID").Value != null)
                {
                    int tMant = Convert.ToInt32(rs.Fields.Item("U_TMantbleID").Value);
                    if (tMant == 1)
                    {
                        Vehiculo vh = new Vehiculo();
                        if (rs.Fields.Item("Vehiculo").Value != null)
                            vh.Nombre = rs.Fields.Item("Vehiculo").Value;
                        if (rs.Fields.Item("U_MantbleID").Value != null)
                            vh.VehiculoID = Convert.ToInt32(rs.Fields.Item("U_MantbleID").Value);
                        ps.Mantenible = vh;
                        ps.Mantenible.MantenibleID = vh.VehiculoID;
                    }
                    else if (tMant == 3)
                    {
                        TipoVehiculo tvh = new TipoVehiculo();
                        if (rs.Fields.Item("TipoVehiculo").Value != null)
                            tvh.Nombre = rs.Fields.Item("TipoVehiculo").Value;
                        if (rs.Fields.Item("U_MantbleID").Value != null)
                            tvh.TipoVehiculoID = Convert.ToInt32(rs.Fields.Item("U_MantbleID").Value);
                        if (rs.Fields.Item("TVDescrip").Value != null)
                            tvh.Descripcion = rs.Fields.Item("TVDescrip").Value;
                        ps.Mantenible = tvh;
                        ps.Mantenible.MantenibleID = tvh.TipoVehiculoID;
                    }
                }

                if (rs.Fields.Item("U_Valor").Value != null)
                    ps.Valor = Convert.ToDouble(rs.Fields.Item("U_Valor").Value);
                if (rs.Fields.Item("U_Mensaje").Value != null)
                    ps.Mensaje = rs.Fields.Item("U_Mensaje").Value;
                if (rs.Fields.Item("U_Alerta").Value != null)
                    ps.Alerta = Convert.ToDouble(rs.Fields.Item("U_Alerta").Value);
                if (rs.Fields.Item("U_ValTiempo").Value != null)
                    ps.ValorTiempo = Convert.ToInt32(rs.Fields.Item("U_ValTiempo").Value);
                if (rs.Fields.Item("U_AlTiempo").Value != null)
                    ps.AlertaTiempo = Convert.ToInt32(rs.Fields.Item("U_AlTiempo").Value);
                if (rs.Fields.Item("Servicio").Value != null)
                    ps.Servicio.Nombre = rs.Fields.Item("Servicio").Value;
                return ps;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Verifica si un recordset contiene informacion de alguna consulta especifica
        /// </summary>
        /// <param name="rs"></param>
        /// <returns></returns>
        public bool HashParametroServicio(Recordset rs)
        {
            if (rs.RecordCount < 1)
                return false;
            return true;
        }

        public List<ParametroServicio> RecordSetToList(Recordset rs)
        {
            try
            {
                List<ParametroServicio> lstPmtro = new List<ParametroServicio>();
                rs.MoveFirst();
                for(int i = 0; i < rs.RecordCount; i++)
                {
                    lstPmtro.Add(RowRecordSetToParametroServicio(rs));
                    rs.MoveNext();
                }
                return lstPmtro;
            }
            catch(Exception ex)
            {
                throw new Exception("Error al convertir RecordSet a List<ParametroServicio> : " + ex.Message);
            }
        }

        public bool SetUDTToPServicio(int? id)
        {
            try
            {
                var rs = ConsultarByID(id);
                if(HashParametroServicio(rs))
                {
                    int _id = rs.Fields.Item("U_PServID").Value;
                    if (_id == id)
                        return this.oUDT.GetByKey(rs.Fields.Item("Code").Value);
                }
                this.oUDT.Code = string.Empty;
                return false;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public Recordset ConsultarByID(int? id)
        {
            try
            {
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(string.Format("select * from [@VSKF_PMTROSERVICIO] where U_PServID = {0}", id));
                return rs;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
    }
}
