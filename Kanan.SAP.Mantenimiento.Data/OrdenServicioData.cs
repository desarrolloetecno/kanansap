﻿using Kanan.Mantenimiento.BO2;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KananSAP.Mantenimiento.Data
{
    public class OrdenServicioData
    {
        #region Atributos
        private const string TABLE_ORDENSERVICIO = "VSKF_ORDENSERVICIO";
        public OrdenServicio OrdenServicio { get; set; }
        public UserTable oUDT { get; set; }
        private Company oCompany;
        public string ItemCode { get; set; }
        public int? Sincronizado { get; set; }
        public Guid? UUID { get; set; }
        #endregion

        #region Contructor
        public OrdenServicioData(Company company)
        {
            this.oCompany = company;
            try
            {
                this.oUDT = this.oCompany.UserTables.Item(TABLE_ORDENSERVICIO);
            }
            catch
            {
                this.oUDT = null;
            }
        }
        #endregion

        #region Métodos de Orden de Servicio
        public bool Existe()
        {
            return this.oUDT.GetByKey(this.ItemCode);
        }

        private int? GetTipoMantenibleID(Type mantenible)
        {
            try
            {
                if (mantenible == typeof(Kanan.Vehiculos.BO2.Vehiculo))
                    return 1;
                if (mantenible == typeof(Kanan.Vehiculos.BO2.Caja))
                    return 2;
                if (mantenible == typeof(Kanan.Vehiculos.BO2.TipoVehiculo))
                    return 1;
                return null;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public void OrdenServicioToUDT()
        {
            try
            {
                //if (this.OrdenServicio.alertas.SubPropietario == null) this.OrdenServicio.alertas.SubPropietario = new Kanan.Operaciones.BO2.Sucursal();
                this.oUDT.Code = this.ItemCode ?? this.OrdenServicio.OrdenServicioID.ToString();
                this.oUDT.Name = this.ItemCode ?? this.OrdenServicio.OrdenServicioID.ToString();
                this.oUDT.UserFields.Fields.Item("U_OrdenServicioID").Value = this.OrdenServicio.OrdenServicioID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_EmpleadoID").Value = this.OrdenServicio.EncargadoOrdenServicio.EmpleadoID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_ServicioID").Value = 0;//this.OrdenServicio.alertas.servicio.ServicioID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_Fecha").Value = this.OrdenServicio.Fecha;
                this.oUDT.UserFields.Fields.Item("U_Folio").Value = this.OrdenServicio.Folio ?? string.Empty;
                this.oUDT.UserFields.Fields.Item("U_Lugar").Value = this.OrdenServicio.Lugar ?? string.Empty;
                this.oUDT.UserFields.Fields.Item("U_Descripcion").Value = this.OrdenServicio.Descripcion ?? string.Empty;
                this.oUDT.UserFields.Fields.Item("U_SucursalID").Value = this.OrdenServicio.SucursalOrden.SucursalID ?? 0;//.alertas.SubPropietario.SucursalID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value = this.OrdenServicio.EmpresaOrden.EmpresaID ?? 0;//.alertas.empresa.EmpresaID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_FechaCaptura").Value = this.OrdenServicio.FechaCaptura;
                this.oUDT.UserFields.Fields.Item("U_ProveedorID").Value = this.OrdenServicio.ProveedorServicio.ProveedorID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_AlertaAvisoID").Value = 0;//this.OrdenServicio.alertas.AlertaMantenimientoID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_KMRecorridos").Value = (double?)this.OrdenServicio.KilometrosRecorridos ?? 0;
                this.oUDT.UserFields.Fields.Item("U_Costo").Value = (double?)this.OrdenServicio.Costo ?? 0;
                //this.oUDT.UserFields.Fields.Item("U_Archivo").Value = this.OrdenServicio.ImagenArchivo ?? string.Empty;
                this.oUDT.UserFields.Fields.Item("U_IVA").Value = (double?)this.OrdenServicio.Impuesto ?? 0;
                this.oUDT.UserFields.Fields.Item("U_TipoPagoID").Value = this.OrdenServicio.TipoDePago.TipoPagoID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_DetallePago").Value = this.OrdenServicio.TipoDePago.DetallePago ?? string.Empty;
                this.oUDT.UserFields.Fields.Item("U_MantenibleID").Value = 0;// this.OrdenServicio.alertas.Mantenible.MantenibleID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value = 0;// (int?)this.GetTipoMantenibleID(this.OrdenServicio.alertas.Mantenible.GetType()) ?? 0;
                this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Sincronizado == null ? 0 : this.Sincronizado;
                this.oUDT.UserFields.Fields.Item("U_UUID").Value = this.UUID == null ? string.Empty : this.UUID.ToString();
                this.oUDT.UserFields.Fields.Item("U_ResponsableID").Value = this.OrdenServicio.ResponsableOrdenServicio.EmpleadoID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_AprobadorID").Value = this.OrdenServicio.AprobadorOrdenServicio.EmpleadoID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_EstatusOrdenID").Value = this.OrdenServicio.EstatusOrden.EstatusOrdenID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_FolioOrden").Value = this.OrdenServicio.FolioOrden.Folio ?? 0;
                this.oUDT.UserFields.Fields.Item("U_AnioOrden").Value = this.OrdenServicio.FolioOrden.AnioFolio ?? 0;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        
        public void UDTToOrdenServicio()
        {
            try
            {
                this.OrdenServicio = new OrdenServicio();
                this.OrdenServicio.OrdenServicioID = this.oUDT.UserFields.Fields.Item("U_OrdenServicioID").Value ?? null;
                this.OrdenServicio.EncargadoOrdenServicio.EmpleadoID = this.oUDT.UserFields.Fields.Item("U_EmpleadoID").Value ?? null;
                //this.OrdenServicio.alertas.servicio.ServicioID = this.oUDT.UserFields.Fields.Item("U_ServicioID").Value ?? null;
                this.OrdenServicio.Fecha = this.oUDT.UserFields.Fields.Item("U_Fecha").Value ?? null;
                this.OrdenServicio.Folio = this.oUDT.UserFields.Fields.Item("U_Folio").Value ?? null;
                this.OrdenServicio.Lugar = this.oUDT.UserFields.Fields.Item("U_Lugar").Value ?? null;
                this.OrdenServicio.Descripcion = this.oUDT.UserFields.Fields.Item("U_Descripcion").Value ?? null;
                //this.OrdenServicio.alertas.SubPropietario.SucursalID = this.oUDT.UserFields.Fields.Item("U_SucursalID").Value ?? null;
                //this.OrdenServicio.alertas.empresa.EmpresaID  = this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value ?? null;
                this.OrdenServicio.FechaCaptura = this.oUDT.UserFields.Fields.Item("U_FechaCaptura").Value ?? null;
                this.OrdenServicio.ProveedorServicio.ProveedorID  = this.oUDT.UserFields.Fields.Item("U_ProveedorID").Value ?? null;
                //this.OrdenServicio.alertas.AlertaMantenimientoID = this.oUDT.UserFields.Fields.Item("U_AlertaAvisoID").Value ?? null;
                this.OrdenServicio.KilometrosRecorridos = (decimal?)Convert.ChangeType(this.oUDT.UserFields.Fields.Item("U_KMRecorridos").Value, typeof(decimal)) ?? null;
                this.OrdenServicio.Costo = (decimal?)Convert.ChangeType(this.oUDT.UserFields.Fields.Item("U_Costo").Value, typeof(decimal)) ?? null;
                //this.OrdenServicio.ImagenArchivo = null;
                this.OrdenServicio.Impuesto = (decimal?)Convert.ChangeType(this.oUDT.UserFields.Fields.Item("U_IVA").Value, typeof(decimal)) ?? null;
                this.OrdenServicio.TipoDePago.TipoPagoID = this.oUDT.UserFields.Fields.Item("U_TipoPagoID").Value ?? null;
                this.OrdenServicio.TipoDePago.DetallePago = this.oUDT.UserFields.Fields.Item("U_DetallePago").Value ?? null;
                //this.OrdenServicio.alertas.Mantenible.MantenibleID  = this.oUDT.UserFields.Fields.Item("U_MantenibleID").Value ?? null;
                //this.OrdenServicio.alertas.Mantenible.MantenibleID = this.oUDT.UserFields.Fields.Item("U_TipoMantenibleID").Value ?? null;
                this.Sincronizado = this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value ?? null;
                this.UUID = Guid.Parse(this.oUDT.UserFields.Fields.Item("U_UUID").Value) ?? null;
                this.OrdenServicio.ResponsableOrdenServicio.EmpleadoID = this.oUDT.UserFields.Fields.Item("U_ResponsableID").Value ?? null;
                this.OrdenServicio.AprobadorOrdenServicio.EmpleadoID = this.oUDT.UserFields.Fields.Item("U_AprobadorID").Value ?? null;
                this.OrdenServicio.EstatusOrden.EstatusOrdenID = this.oUDT.UserFields.Fields.Item("U_EstatusOrdenID").Value ?? null;
                this.OrdenServicio.FolioOrden.Folio = this.oUDT.UserFields.Fields.Item("U_FolioOrden").Value ?? null;
                this.OrdenServicio.FolioOrden.AnioFolio = this.oUDT.UserFields.Fields.Item("U_AnioFolio").Value ?? null;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public bool SetUDTPMantenimientoID(int? id)
        {
            try
            {
                var rs = this.ConsultarByID(id);
                if (HashOrdenServicio(rs))
                {
                    rs.MoveFirst();
                    int _id = rs.Fields.Item("U_OrdenServicioID").Value;
                    if (_id == id)
                        return this.oUDT.GetByKey(rs.Fields.Item("Code").Value);
                }
                this.oUDT.Code = string.Empty;
                return false;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public Recordset ConsultarByID(int? id)
        {
            try
            {
                string query = string.Format("select * from [@{0}] where U_OrdenServicioID = {1}", TABLE_ORDENSERVICIO, id);
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                return rs;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public bool HashOrdenServicio(Recordset rs)
        {
            if (rs.RecordCount > 0)
                return true;
            return false;
        }

        public void Insert()
        {
            int result = -1;
            try
            {
                result = this.oUDT.Add();
                if (result != 0)
                {
                    int errornum;
                    string errormsj;
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Actualizar()
        {
            int result = -1;
            try
            {
                int errornum;
                string errormsj;
                result = this.oUDT.Update();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar()
        {
            int result = -1;
            try
            {
                int errornum;
                string errormsj;
                result = this.oUDT.Remove();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Recordset Consultar(OrdenServicio orden, DateTime? fecinicio, DateTime? fecfin)
        {
            try
            {
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(string.Format(" SELECT * FROM [@VSKF_ORDENSERVICIO] as os "));
                //query.Append(" UPDATE [@VSKF_ORDENSERVICIO] SET Code = '" + orden.OrdenServicioID + "', U_Sincronizado = " + Sincronizado);

                if (OrdenServicio.OrdenServicioID != null)
                    where.Append(string.Format(" and os.U_OrdenServicio = {0}", orden.OrdenServicioID));
                if (OrdenServicio.EncargadoOrdenServicio != null && OrdenServicio.EncargadoOrdenServicio.EmpleadoID != null)
                    where.Append(string.Format(" and U_Empleado = {0} ", OrdenServicio.EncargadoOrdenServicio.EmpleadoID));
                if (orden.GetType() == typeof(OrdenServicioFilter))
                {
                    OrdenServicioFilter x = (OrdenServicioFilter)orden;
                    if (orden.FechaCaptura != null && x.FechaFinFilter != null)
                    {
                        where.Append(string.Format(" AND os.Fecha BETWEEN {0} AND {1} ", x.FechaCaptura.Value.ToShortDateString(), x.FechaFinFilter.Value.ToShortDateString()) );
                    }
                    else if (orden.FechaCaptura != null && x.FechaFinFilter == null)
                    {
                        where.Append(" AND od.Fecha >= " + x.FechaCaptura + " ");
                    }
                    else if (orden.FechaCaptura == null && x.FechaFinFilter != null)
                    {
                        where.Append(" AND od.Fecha <= " + x.FechaFinFilter + " ");
                    }
                }
                else if (orden.FechaCaptura != null)
                {
                    where.Append(" AND od.Fecha = " + orden.FechaCaptura + " ");
                }

                if (string.IsNullOrEmpty(orden.Folio))
                    where.Append(string.Format(" and U_Folio like '%{0}%' ", orden.Folio));
                if (orden.EmpresaOrden != null && orden.EmpresaOrden.EmpresaID != null)
                    where.Append(string.Format(" and U_EmpresaID = {0} ", orden.EmpresaOrden.EmpresaID));
                if (orden.ProveedorServicio != null && orden.ProveedorServicio.ProveedorID != null)
                    where.Append(string.Format(" and U_ProveedorID = {0} ", orden.ProveedorServicio.ProveedorID));
                if (orden.EstatusOrden != null && orden.EstatusOrden.EstatusOrdenID != null)
                    where.Append(string.Format(" and U_EstatusOrdenID = {0} ", orden.EstatusOrden.EstatusOrdenID));
                if (orden.FolioOrden != null && orden.FolioOrden.Folio != null)
                    where.Append(string.Format(" and U_FolioOrden = {0} ", orden.FolioOrden.Folio));
                if (orden.FolioOrden != null && orden.FolioOrden.AnioFolio != null)
                    where.Append(string.Format(" and U_AnioFolio = {0} ", orden.FolioOrden.AnioFolio));
                if (Sincronizado != null)
                    where.Append(string.Format(" and U_Sincronizado = {0} ", Sincronizado));
                if (UUID != null)
                    where.Append(string.Format(" and U_UUID like '%{0}%' ", UUID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Recordset ActualizarCode(OrdenServicio orden)
        {
            try
            {
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                query.Append(string.Format(" UPDATE [@VSKF_ORDENSERVICIO] SET Code = '{0}', Name = '{0}', U_OrdenServicioID = {0}, U_Sincronizado = {1} ", orden.OrdenServicioID, this.Sincronizado));
                //query.Append(" UPDATE [@VSKF_ORDENSERVICIO] SET Code = '" + orden.OrdenServicioID + "', U_Sincronizado = " + Sincronizado);

                if (UUID != null)
                    where.Append(string.Format(" and U_UUID like '%{0}%' ", UUID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion 

        #region Métodos Detalle de la Orden de Servicio
        private List<string> ItemsDetalle { get; set; }
        public void InsertDetalleOrden()
        {
            try
            {
                if (!this.Existe())
                    return;
                if (this.OrdenServicio.DetalleOrden != null && this.OrdenServicio.DetalleOrden.Count > 0)
                {
                    DetalleOrdenSAP dorden = new DetalleOrdenSAP(this.oCompany);
                    ItemsDetalle = new List<string>();
                    foreach(DetalleOrden d in this.OrdenServicio.DetalleOrden)
                    {
                        d.OrdenServicio.OrdenServicioID = Convert.ToInt32(this.ItemCode);
                        dorden.oDetalleOrden = d;
                        dorden.ItemCode = DateTime.Now.Ticks.ToString();
                        dorden.Sincronizado = 1;
                        dorden.UUID = Guid.NewGuid();
                        dorden.DetalleOrdenToUDT();
                        dorden.Insert();
                        ItemsDetalle.Add(dorden.ItemCode);
                    }
                }
            }
            catch(Exception ex)
            {
                throw new Exception("OrdenServicioData.InsetDetalleOrden: " + ex.Message);
            }
            
        }

        public void ActualizarCodeDetalle(List<DetalleOrden> detalle)
        {
            try
            {
                if (detalle != null && detalle.Count > 0)
                {
                    DetalleOrdenSAP dtsap = new DetalleOrdenSAP(this.oCompany);
                    int cont = 0;
                    foreach (DetalleOrden d in detalle)
                    {
                        string i = this.ItemsDetalle[cont].ToString();
                        dtsap.ItemCode = i;
                        if (dtsap.Existe())
                        {
                            dtsap.UDTTooDetalleOrden();
                            dtsap.Sincronizado = 0;
                            dtsap.ActualizarCode(d);
                        }
                        cont++;
                    }
                }
                if (this.ItemsDetalle != null)
                    this.ItemsDetalle.Clear();
            }
            catch(Exception ex)
            {
                this.ItemsDetalle.Clear();
                throw new Exception("OrdenServicioData.ActualizarCodeDetalle: " + ex.Message);
            }
            
        }
        #endregion

    }
}
