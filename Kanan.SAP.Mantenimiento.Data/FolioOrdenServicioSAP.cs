﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kanan.Mantenimiento.BO2;
using Kanan.Operaciones.BO2;
using Kanan.Vehiculos.BO2;
using SAPbobsCOM;

namespace Kanan.SAP.Mantenimiento.Data
{
    public class FolioOrdenServicioSAP
    {
        #region Atributos
        private const string TABLE_FOLIOORDEN = "VSKF_FOLIOORDEN";
        public FolioOrden folio { get; set; }
        public UserTable oUDT { get; set; }
        private Company oCompany;
        //public string ItemCode { get; set; }
        /// <summary>
        /// 0 = Sincronizado, 1 = Insertando, 2 = Actualizando, 3 = Eliminando
        /// </summary>
        public int? Sincronizado { get; set; }
        /// <summary>
        /// Guid que se tomará para la sincronización SAP <-> Kanan Fleet
        /// </summary>
        public Guid UUID { get; set; }
        #endregion

        #region Contructor
        public FolioOrdenServicioSAP(Company company)
        {
            this.oCompany = company;
            try
            {
                this.oUDT = this.oCompany.UserTables.Item(TABLE_FOLIOORDEN);
            }
            catch
            {
                this.oUDT = null;
            }
        }
        #endregion

        //public bool Existe()
        //{
        //    return this.oUDT.GetByKey(this.ItemCode);
        //}

        public void FolioToUDT()
        {
            try
            {
                string tks = DateTime.Now.Ticks.ToString();
                this.oUDT.Code = this.folio.FolioOrdenID != null ? this.folio.FolioOrdenID.ToString() : tks;
                this.oUDT.Name = this.folio.FolioOrdenID != null ? this.folio.FolioOrdenID.ToString() : tks;
                this.oUDT.UserFields.Fields.Item("U_Folio").Value = this.folio.Folio ?? 0;
                this.oUDT.UserFields.Fields.Item("U_Anio").Value = this.folio.AnioFolio ?? 0;
                this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value = this.folio.Propietario.EmpresaID ?? 0;
                this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value = this.Sincronizado == null ? 0 : this.Sincronizado;
                this.oUDT.UserFields.Fields.Item("U_UUID").Value = this.UUID == null ? string.Empty : this.UUID.ToString();
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        
        public void UDTToFolio()
        {
            try
            {
                this.folio = new FolioOrden();
                this.folio.Propietario = new Empresa();
                int id;
                if (int.TryParse(this.oUDT.Code, out id))
                    this.folio.FolioOrdenID = id;
                else
                    this.folio.FolioOrdenID = null;
                this.folio.Folio = this.oUDT.UserFields.Fields.Item("U_Folio").Value == null ? null : Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_Folio").Value.ToString());
                this.folio.AnioFolio = this.oUDT.UserFields.Fields.Item("U_Anio").Value == null ? null : Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_Anio").Value.ToString());
                this.folio.Propietario.EmpresaID = this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value == null ? null : Convert.ToInt32(this.oUDT.UserFields.Fields.Item("U_EmpresaID").Value.ToString());
                this.Sincronizado = this.oUDT.UserFields.Fields.Item("U_Sincronizado").Value ?? null;
                this.UUID = Guid.Parse(this.oUDT.UserFields.Fields.Item("U_UUID").Value) ?? null;
                
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public Recordset ConsultarByEmpresaID(int? id)
        {
            try
            {
                string query = string.Format("select * from [@{0}] where U_EmpresaID = {1}", TABLE_FOLIOORDEN, id);
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                rs.DoQuery(query);
                return rs;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public bool HashFolioOrden(Recordset rs)
        {
            if (rs.RecordCount > 0)
                return true;
            return false;
        }

        public void Insert()
        {
            int result = -1;
            try
            {
                result = this.oUDT.Add();
                if (result != 0)
                {
                    int errornum;
                    string errormsj;
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Actualizar()
        {
            int result = -1;
            try
            {
                int errornum;
                string errormsj;
                result = this.oUDT.Update();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Eliminar()
        {
            int result = -1;
            try
            {
                int errornum;
                string errormsj;
                result = this.oUDT.Remove();
                if (result != 0)
                {
                    this.oCompany.GetLastError(out errornum, out errormsj);
                    throw new Exception("Error: " + errornum + " - " + errormsj);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Recordset ActualizarCode(FolioOrden folioOrden)
        {
            try
            {
                Recordset rs = this.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                StringBuilder query = new StringBuilder();
                StringBuilder where = new StringBuilder();
                string strUpdate = string.Format("UPDATE [@VSKF_FOLIOORDEN] SET Code = '{0}', Name = '{0}', U_Sincronizado = {1}, U_Folio = {2} ", folioOrden.FolioOrdenID,
                    this.Sincronizado, folioOrden.Folio);
                query.Append(strUpdate);
                if (UUID != null)
                    where.Append(string.Format(" and U_UUID like '%{0}%' ", UUID));
                if (where.Length > 0)
                {
                    string filtro = where.ToString();
                    if (filtro.StartsWith(" and"))
                        filtro = filtro.Substring(4);
                    query.Append(" where " + filtro);
                }
                rs.DoQuery(query.ToString());
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
