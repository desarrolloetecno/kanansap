﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.Motor.Alertas
{
    public class Company
    {
        #region CONSTANTES
        private const string Clase = "kanan_Company.cs";
        private const string Modulo = "FiltraCompanies";
        #endregion

        #region PROPIEDADES
        private Herramientas oTool = new Herramientas();
        private Recordset oRecordset;
        #endregion

        public String BuscarCatalogos(ref List<vskf_Catalogos_INFO> lstCatalogos)
        {
            string resultado = "", sql = "";

            try
            {
                DataSet _dataSet = new DataSet();
                String serverName = "";
                vskf_SqlConnection oConnexionSQL = null;
                vskf_HANAConnection oConnectionHana = null;

                sql = vskf_Globales.bSqlConnection ? "SELECT * FROM [SBO-COMMON].DBO.SRGC" : @"SELECT * FROM ""SBOCOMMON"".""SRGC""";

                if (vskf_Globales.bSqlConnection)
                {
                    serverName = string.Format("Data Source={0};User ID={1};Password={2}", vskf_Globales.ServerName, vskf_Globales.UserName, vskf_Globales.PassWordUser);
                    oConnexionSQL = new vskf_SqlConnection(serverName);
                    oConnexionSQL.llenarDataSet(sql);
                    _dataSet = oConnexionSQL.dataSet;
                }
                else
                {
                    serverName = string.Format(@"DRIVER={{0}};UID={1};PWD={2};SERVERNODE={3};DATABASENAME=NDB", "HDBODBC", vskf_Globales.UserName.Trim(), vskf_Globales.PassWordUser.Trim(), vskf_Globales.ServerName.Trim());
                    oConnectionHana = new vskf_HANAConnection(serverName);
                    oConnectionHana.llenarDataSet(sql);
                    _dataSet = oConnectionHana.dataSet;

                }

                if (_dataSet.Tables[0].Rows.Count > 0)
                {
                    resultado = "OK";
                    foreach (DataRow oRow in _dataSet.Tables[0].Rows)
                    {
                        vskf_Catalogos_INFO tmp = new vskf_Catalogos_INFO();
                        tmp.Database = oRow["dbName"] == DBNull.Value ? "" : Convert.ToString(oRow["dbName"]);
                        tmp.Company = oRow["cmpName"] == DBNull.Value ? "" : Convert.ToString(oRow["cmpName"]);
                        tmp.Version = oRow["versStr"] == DBNull.Value ? "" : Convert.ToString(oRow["versStr"]);
                        tmp.User = oRow["dbUser"] == DBNull.Value ? "" : Convert.ToString(oRow["dbUser"]);
                        tmp.Log = oRow["LOC"] == DBNull.Value ? "" : Convert.ToString(oRow["LOC"]);
                        lstCatalogos.Add(tmp);
                    }

                }
                else resultado = "SIN RESULTADOS";


            }
            catch (Exception ex)
            {
                resultado = "No se pudo leer la informacion de Base de datos, ERROR: " + ex.Message;
                oTool.GuardarLogError(ex, "BuscarCatalogos", Clase, Modulo, sDatos: "Consulta :" + sql);
            }


            return resultado;
        }

        public Boolean creaCatalgosValidos()
        {
            #region Comentarios
            #endregion
            string resultado = "", query = "";
            Boolean bContinuar = false;
            try
            {
                List<vskf_Catalogos_INFO> lstCatalogos = new List<vskf_Catalogos_INFO>();
                List<vskf_Catalogos_INFO> lstValidos = new List<vskf_Catalogos_INFO>();
                resultado = BuscarCatalogos(ref lstCatalogos);

                if (resultado == "OK" && lstCatalogos.Count > 0)
                {
                    vskf_SqlConnection oSqlConn = new vskf_SqlConnection(vskf_Globales.CadenaConexion);
                    vskf_HANAConnection oHannaConn = new vskf_HANAConnection(vskf_Globales.CadenaConexion);

                    foreach (vskf_Catalogos_INFO data in lstCatalogos)
                    {
                        try
                        {
                            query = vskf_Globales.bSqlConnection ? string.Format("SELECT * FROM [{0}].DBO.[@VSKF_CONFIGURACION]", data.Database) : string.Format(@"SELECT * FROM ""{0}"".""@VSKF_CONFIGURACION""", data.Database);
                            if (vskf_Globales.bSqlConnection)
                                oSqlConn.llenarDataSet(query);
                            else
                                oHannaConn.llenarDataSet(query);
                            lstValidos.Add(data);
                        }
                        catch (Exception ex)
                        {
                            oTool.GuardarLogError(ex, "EscribirCatalogosValidos", Clase, Modulo);
                        }

                    }

                    if (lstValidos.Count > 0)
                    {
                        resultado = oTool.GuardaCatalogos(lstValidos);
                        bContinuar = true;
                    }
                    else bContinuar = false;

                }
            }
            catch (Exception ex)
            {
                bContinuar = false;
                oTool.GuardarLogError(ex, "creaCatalgosValidos", Clase, Modulo, sDatos: "Consulta :" + query);
            }

            return bContinuar;
        }

        public vskf_Settings EmpresaConfiguracion(string BaseDatos, out string sConsulta)
        {

            string sql = "";
            vskf_Settings KNSettings = null;
            sql = string.Format(vskf_Globales.bSqlConnection ? @"SELECT * FROM [@VSKF_CONFIGURACION]" :
                @"SELECT * FROM ""@VSKF_CONFIGURACION""");
            sConsulta = sql;
            oRecordset = (Recordset)ConexionSAP.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            oRecordset.DoQuery(sql);

            if (oRecordset.RecordCount > 0)
            {
                KNSettings = new vskf_Settings();
                oRecordset.MoveFirst();
                KNSettings.Code = Convert.ToString(oRecordset.Fields.Item("Code").Value);
                KNSettings.Name = Convert.ToString(oRecordset.Fields.Item("Name").Value);
                KNSettings.cSucursal = Convert.ToChar(oRecordset.Fields.Item("U_Sucursal").Value);
                KNSettings.cVehiculo = Convert.ToChar(oRecordset.Fields.Item("U_Vehiculos").Value);
                KNSettings.cCliente = string.IsNullOrEmpty(oRecordset.Fields.Item("U_Clientes").Value) ? 'N' : Convert.ToChar(oRecordset.Fields.Item("U_Clientes").Value);

                KNSettings.sTipoAnticipo = string.IsNullOrEmpty(oRecordset.Fields.Item("U_PagoAnticipo").Value) ? "" : Convert.ToString(oRecordset.Fields.Item("U_PagoAnticipo").Value);

                KNSettings.Sucursal = Convert.ToString(oRecordset.Fields.Item("U_CodigoSucursal").Value);
                KNSettings.Vehiculo = Convert.ToString(oRecordset.Fields.Item("U_CodigoVehiculo").Value);

                KNSettings.cSBO = Convert.ToChar(oRecordset.Fields.Item("U_SBO").Value);
                KNSettings.cSSL = oRecordset.Fields.Item("U_ServidorSSL").Value == "" ? 'N' : Convert.ToChar(oRecordset.Fields.Item("U_ServidorSSL").Value);
                KNSettings.cKANAN = Convert.ToChar(oRecordset.Fields.Item("U_KANAN").Value);
                KNSettings.cAlmacen = Convert.ToChar(oRecordset.Fields.Item("U_AlmacenRecarga").Value);
                KNSettings.Almacen = Convert.ToString(oRecordset.Fields.Item("U_CodigoAlmacen").Value);
                KNSettings.Articulo = Convert.ToString(oRecordset.Fields.Item("U_Articulo").Value);
                KNSettings.Cuenta = Convert.ToString(oRecordset.Fields.Item("U_Cuenta").Value);
                KNSettings.CuentaServicioCP = Convert.ToString(oRecordset.Fields.Item("U_CuentaCP").Value);
                KNSettings.CuentaAnticipo = Convert.ToString(oRecordset.Fields.Item("U_AcctAnticipo").Value);
                KNSettings.CuentaContrapartida = Convert.ToString(oRecordset.Fields.Item("U_AcctContra").Value);
                KNSettings.Borrador = Convert.ToString(oRecordset.Fields.Item("U_Borrador").Value);
                //

                if (((int)oRecordset.Fields.Item("U_Dimension").Value) != 0)
                    KNSettings.DimCode = Convert.ToInt32(oRecordset.Fields.Item("U_Dimension").Value);
                else
                    KNSettings.DimCode = 0;

                if (!string.IsNullOrEmpty((string)oRecordset.Fields.Item("U_Servidor").Value))
                    KNSettings.Server = Convert.ToString(oRecordset.Fields.Item("U_Servidor").Value);
                else
                    KNSettings.Server = "";

                if (!string.IsNullOrEmpty((string)oRecordset.Fields.Item("U_Puerto").Value))
                    KNSettings.Puerto = Convert.ToInt32(oRecordset.Fields.Item("U_Puerto").Value);
                else
                    KNSettings.Puerto = 0;

                if (!string.IsNullOrEmpty((string)oRecordset.Fields.Item("U_Correo").Value))
                    KNSettings.CorreoCuenta = Convert.ToString(oRecordset.Fields.Item("U_Correo").Value);
                else
                    KNSettings.CorreoCuenta = "";

                if (!string.IsNullOrEmpty((string)oRecordset.Fields.Item("U_Password").Value))
                    KNSettings.PassWord = Convert.ToString(oRecordset.Fields.Item("U_Password").Value);
                else
                    KNSettings.PassWord = "";


                sql = string.Format(vskf_Globales.bSqlConnection ? "SELECT * FROM [@VSKF_RELEMPRESA]" : @"SELECT * FROM ""@VSKF_RELEMPRESA""");
                oRecordset.DoQuery(sql);
                if (oRecordset.RecordCount > 0)
                {
                    oRecordset.MoveFirst();
                    KNSettings.EmpresaID = Convert.ToString(oRecordset.Fields.Item("U_KFEmpresaID").Value);
                }
                else KNSettings = null;
            }

            return KNSettings;
        }

        public Boolean DatosCompany(string DataBase, ref vskf_Company_INFO oCompany, out string sConsulta)
        {
            Boolean bContinuar = false;
            string sql = "";

            sql = string.Format(vskf_Globales.bSqlConnection ? "SELECT * FROM OADM" : @"SELECT * FROM ""OADM""");
            sConsulta = sql;
            oRecordset = (Recordset)ConexionSAP.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            oRecordset.DoQuery(sql);
            if (oRecordset.RecordCount == 0)
                oCompany = null;
            else
            {

                oCompany.RFC = oRecordset.Fields.Item("TaxIdNum").Value.Tostring();
                oCompany.CompanyName = oRecordset.Fields.Item("CompnyName").Value.Tostring();
                oCompany.CompanyAdres = oRecordset.Fields.Item("CompnyAddr").Value.Tostring();
            }

            return bContinuar;
        }

        public Int64 DevolverMaximoCode(string Tabla, out string sConsulta)
        {
            Int64 iCode = -1;
            string sql = string.Format(vskf_Globales.bSqlConnection ? @"SELECT * FROM [@{0}]" : @"SELECT * FROM ""@{0}""", Tabla);
            sConsulta = sql;
            oRecordset = (Recordset)ConexionSAP.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            oRecordset.DoQuery(sql);
            if (oRecordset.RecordCount > 0)
            {

                sql = string.Format(vskf_Globales.bSqlConnection ? @"SELECT MAX(Code) AS Code FROM [@{0}]" : @"SELECT MAX(""Code"") AS ""Code"" FROM ""@{0}""", Tabla);
                sConsulta = sql;
                oRecordset.DoQuery(sql);
                if (oRecordset.RecordCount == 0)
                    iCode = 1;
                else
                {
                    iCode = Convert.ToInt64(oRecordset.Fields.Item("Code").Value);
                    iCode++;
                }
            }
            else iCode = 1;
            return iCode;
        }

        public Boolean RecuperaProveedor(ref string CardCode, out string sConsulta, int ProveedorID)
        {
            Boolean bContinuar = false;
            string sql = "";

            oRecordset = (Recordset)ConexionSAP.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);                    
            sql = string.Format(vskf_Globales.bSqlConnection ? "SELECT Code FROM [@VSKF_PROVEEDOR] WHERE U_ProveedorID = {0}" : @"SELECT ""Code"" FROM ""@VSKF_PROVEEDOR"" WHERE U_ProveedorID = {0}", ProveedorID);
            sConsulta = sql;
            oRecordset.DoQuery(sql);
            if (oRecordset.RecordCount > 0)
            {
                bContinuar = true;
                oRecordset.MoveFirst();
                CardCode =  Convert.ToString(oRecordset.Fields.Item("Code").Value);
            }


            return bContinuar;
        }

        public Boolean RecuperaCuenta(int servicioID, ref  string oAcctCode, out string sConsulta)
        {
            Boolean bContinuar = false;
            string sql = "";

            oRecordset = (Recordset)ConexionSAP.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            sql = string.Format(vskf_Globales.bSqlConnection ? @"SELECT U_CuentaContable FROM ""@VSKF_SERVICIOS""  WHERE U_ServicioID = {0}" : @"SELECT U_CuentaContable FROM ""@VSKF_SERVICIOS""  WHERE U_ServicioID = {0}", servicioID);
            sConsulta = sql;
            oRecordset.DoQuery(sql);


            if (oRecordset.RecordCount > 0)
            {
                bContinuar = true;
                oRecordset.MoveFirst();
                oAcctCode = oRecordset.Fields.Item("U_CuentaContable").Value;
            }

            return bContinuar;
        }

        public  String ConceptoFuel(string Code, out string sConsulta)
        {
            String sql = "", ItemCode = string.Empty;
            sql = string.Format(vskf_Globales.bSqlConnection ? "SELECT * FROM [@VSKF_CONCEPTOFUEL] WHERE Code = '{0}'" : @"SELECT * FROM ""@VSKF_CONCEPTOFUEL"" WHERE ""Code"" = '{0}'", Code);

            vskf_Settings oSetting = EmpresaConfiguracion("", out  sConsulta);
        

            oRecordset = (Recordset)ConexionSAP.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);  
             oRecordset.DoQuery(sql);
             sConsulta = sql;
             if (oRecordset.RecordCount > 0)
             {
                 oRecordset.MoveFirst();
                 ItemCode = Convert.ToString(oRecordset.Fields.Item("Name").Value);

             }
             else throw new Exception("No se encuentra el codigo del articulo para el concepto de combustible");

            return ItemCode;
        }

        public Boolean CentroCostoConfiguracion(ref string CentroCosto, out string sConsulta, vskf_Settings oSetting, int SucursalID, int VehiculoID)
        {
            Boolean bContinuar = false;
            string sql = "";
            sConsulta = sql;
            if (oSetting != null)
            {
                oRecordset = (Recordset)ConexionSAP.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);                    
                if (oSetting.cSucursal == 'Y')
                {                    
                    sql = string.Format(vskf_Globales.bSqlConnection ? "SELECT Name FROM [@VSKF_SUCURSAL] WHERE U_SucursalID = {0}" : @"SELECT ""Name"" FROM ""@VSKF_SUCURSAL"" WHERE U_SucursalID = {0} ", SucursalID);
                    sConsulta = sql;                    
                    oRecordset.DoQuery(sql);
                    if (oRecordset.RecordCount > 0)
                    {
                        oRecordset.MoveFirst();
                        CentroCosto = Convert.ToString(oRecordset.Fields.Item("Name").Value);
                    }
                    else
                        CentroCosto = oSetting.Sucursal;
                }
                else
                {

                    sql = string.Format(vskf_Globales.bSqlConnection ? @"SELECT Name FROM [@VSKF_VEHICULO] WHERE U_VehiculoID ={0}" : @"SELECT ""Name"" FROM ""@VSKF_VEHICULO"" WHERE U_VehiculoID ={0}", VehiculoID);
                    sConsulta = sql;
                    if (oRecordset.RecordCount > 0)
                    {
                        oRecordset.MoveFirst();
                        CentroCosto = Convert.ToString(oRecordset.Fields.Item("Name").Value);
                    }
                    else CentroCosto = oSetting.Vehiculo;

                }
                bContinuar = true;

            }

            return bContinuar;
        }
    }
}
