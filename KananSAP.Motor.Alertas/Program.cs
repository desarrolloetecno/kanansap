﻿#region Referencias
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Lectura de archivos
using System.IO;
using System.Collections;

//Referencias
using KananWS.Interface;
using Kanan.Alertas.BO2;
using Etecno.Security2.BO;
using Kanan.Operaciones.BO2;
using Kanan.Comun.BO2;
using Kanan.Vehiculos.BO2;
using KananSAP.Vehiculos.Data;
using Kanan.Llantas.BO2;
using KananSAP.Llantas.Data;

using Kanan.Mantenimiento.BO2;
using KananSAP.Alertas.Data;

using System.Configuration;

//Referencias SAP
using SAPbobsCOM;
using SAPbouiCOM;

using SAPinterface.Data.INFO.Tablas;

using KananSAP.Motor.Alertas.BO;

#endregion Referencias

namespace KananSAP.Motor.Alertas
{
    public class Program
    {
        #region Propiedades

        /* EmpresaID
         * Se genera la asignación de la EmpresaID, dicho valor
         * es obtenido del App.config del proyecto.
         */
        //Propiedades para alertas
        private static int EmpID = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("EmpresaID").ToString());
        //public static SAPbobsCOM.Company Comp;
        private static SAPbouiCOM.Application App;
        private static String Query;
        private static MotorAlertasWS CAlertas;
        private static MotorAlertasWS CDespHisto;
        private static AlertaMantenimientoSAP AlertaMantData;
        private static AlertaRutinaSAP AlertaRutinaData;
        private static AlertaSAP AlertaData;
        private static Recordset Accion;
        private static String TablaAlerta;
        private static String TablaMTTO;
        private static String TablaRelEmpresa;
        private static List<Alerta> list;

        private static AlertaMantenimiento alertaMant;

        //Propiedades para conexión
        private static vskf_Settings configuraciones;
        private static Herramientas herramientas;
        //public static ConexionSAP conexionsap;
        private static Company ocompany;
        private static String MensajeSAP;
        private static String database;
        private static String Consola;
        private static bool aprobado;

        //Propiedades para llantas
        private static IncidenteLlantaWS ConsultarIncidentes;
        private static List<IncidenteLlanta> listIncidente;

        private static VehiculoSAP vehiculosap;
        private static ActivoSAP activosap;
        private static LlantaSAP llantasap;

        //Propiedades para Desplazamiento histórico
        private static List<DesplazamientoHistorico> listDespla;
        private static VehiculoWS DesplazamientoWS;
        private static DesplazamientoHistoricoSAP DesplaHistoSAP;

        #endregion Propiedades

        #region DTO

        public class AlertaMantenimientoDto
        {
            public AlertaMantenimiento AlertaMantenimiento { get; set; }
            public int? MantenibleID { get; set; }
            public int? TipoMantenibleID { get; set; }

            /// <summary>
            /// Indica el tipo de parámetro por el cual se genera la alerta
            /// Distancia = 1, Tiempo = 2;
            /// </summary>
            public int? TypeOfType { get; set; }
        }

        #endregion DTO

        #region Constructor
        /* ---Método principal---
         * El método main genera la conexión con SAP con los credenciales y
         * configuraciones de un usuario, todo esto definido en un XML que
         * se genera en el proyecto KananSettings.
         * Su función inicial fue obtener alertas aun que ahora debe generar
         * la salida de inventario de las llantas que están asignadas.
         * 
         * Development by: ing. Rair Santos.
         */
        private static void Main(string[] args)
        {
            try
            {
                try { ConexionServidorSAP(); }
                catch (Exception ex)
                { throw new Exception(ex.Message); }

                //try { Alertas(); }
                //catch (Exception ex)
                //{ throw new Exception(ex.Message); }

                //////////try { Llantas(); }
                //////////catch (Exception ex)
                //////////{ throw new Exception(ex.Message); }

                //////////try { /*HistoricoDesplazamiento();*/ }
                //////////catch (Exception ex)
                //////////{ throw new Exception(ex.Message); }

                try
                {
                    RegistraVehiculos();
                }
                catch(Exception ex)
                {
                    System.Console.WriteLine(String.Format("Ocurrió el siguiente error: {0}",ex.Message));
                }

                System.Console.WriteLine("Proceso de soncronización terminado. ");
                System.Console.ReadLine();
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                System.Console.WriteLine("Favor de comunicarse con el área de soporte de Kananfleet. ");
                //System.Console.ReadLine();
            }
        }
        #endregion Constructor

        #region Llantas

        /* Comentarios
         * Método en cargado de realizar el proceso de
         * generar las salidas de inventario de las llantas.
         */

        public static void Llantas()
        {
            /*Se limpia la consola*/
            //System.Console.Clear();
            /*Se establecen colores. */
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Green;
            System.Console.WriteLine("Proceso de sincronización de llantas iniciado. ");
            try
            {

                /*Método que obtiene los incidentes de llantas por empresa.*/
                ObtenerIncidentes();

                #region Comentarios
                /*
                 * Método que valida cuales incidentes deben tomarse en cuenta
                 * debido a que cuando se envía una llanta instalada a un vehículo
                 * directamente a la pila de desecho, sin antes ser desinstalada,
                 * esta llanta genera dos registros de incidente, desinstalación y
                 * baja teniendo las columnas fecha identicas en la tabla de SQL.
                 */
                #endregion Comentarios
                //Validaciones();

                /*Método que genera la salida de invetario de las llantas asignadas.*/
                for (int i = 0; i < listIncidente.Count; i++)
                {
                    try
                    {
                        switch (listIncidente[i].Accion)
                        {
                            case "INSTALACION":
                                if (ExisteSalidaSAP(listIncidente[i]))
                                    SincronizarSalidaWEB(listIncidente[i]);
                                else
                                    if (!SalidaInventario(listIncidente[i]))
                                    System.Console.WriteLine("Error proceso de salida de mercancía No: " + i + " IncidenteID: " + listIncidente[i].IncidenteID);
                                break;
                            case "DESINSTALACION":
                                if (ExisteEntradaSAP(listIncidente[i]))
                                    SincronizarEntradaWEB(listIncidente[i]);
                                else
                                    if (!EntradaInventario(listIncidente[i]))
                                    System.Console.WriteLine("Error proceso de entrada de mercancía No: " + i + " IncidenteID: " + listIncidente[i].IncidenteID);
                                break;
                            case "ALTA":
                                if (ExisteEntradaSAP(listIncidente[i]))
                                    SincronizarEntradaWEB(listIncidente[i]);
                                else
                                    if (!EntradaInventario(listIncidente[i]))
                                    System.Console.WriteLine("Error en el proceso de entrada de mercancía No: " + i + " IncidenteID: " + listIncidente[i].IncidenteID);
                                break;
                            case "BAJA":
                                if (ExisteSalidaSAP(listIncidente[i]))
                                    SincronizarSalidaWEB(listIncidente[i]);
                                else
                                    if (!SalidaInventario(listIncidente[i]))
                                    System.Console.WriteLine("Error en el proceso de salida de mercancía No: " + i + " IncidenteID: " + listIncidente[i].IncidenteID);
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        /*No se realiza ningún proceso de catch de escepciones
                         * debido a que el proceso de generación de salidas de 
                         * inventario debe seguir generando salidas en caso de que
                         * existieran más.
                         */
                        System.Console.WriteLine("Error en incidencia No: " + i + " Error: " + ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error en proceso de salidas de inventario. " + ex.Message);
            }
        }

        #region ObtenerIncidentes
        /*Comentarios
         * Método encargado de obtener los todos los registros de los incidentes
         * que servirán para el proceso que determina si se genera la salida o entrada
         * de inventario con respecto a las llantas.
         * 
         * Development by: ing. Rair Santos.
         */
        public static void ObtenerIncidentes()
        {
            /*Se crea la instancia del webservice para consultar a la tabla*/
            System.Console.WriteLine("Creando instancias. ");
            String publickey = ConfigurationSettings.AppSettings.Get("publickey").ToString();
            String privatekey = ConfigurationSettings.AppSettings.Get("privatekey").ToString();
            ConsultarIncidentes = new IncidenteLlantaWS(Guid.Parse(publickey), privatekey);
            System.Console.WriteLine("Consultando incidentes de llantas. ");

            try
            {
                var IncidentesWS = ConsultarIncidentes.GetOutOfPhase(EmpID);
                listIncidente = IncidentesWS;
                System.Console.WriteLine("Ok.");
                System.Console.WriteLine("Incidentes obtenidos: " + listIncidente.Count);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al consultar los incidentes. " + ex.Message);
            }
        }
        #endregion ObtenerIncidentes

        #region ObtenerItemCodeLlanta

        public static String RecuperaItemCodeByLlantaID(int? LlantaID)
        {
            String ItemCode = String.Empty;
            try
            {
                llantasap = new LlantaSAP(ref ConexionSAP.oCompany);
                ItemCode = llantasap.GetItemCodeByID(LlantaID);

                if (!String.IsNullOrEmpty(ItemCode))
                    return ItemCode;
                else
                    throw new Exception("La llanta no tiene configurado un código de artículo. ");
            }
            catch (Exception ex)
            {
                throw new Exception("No fue posible recuperar el código de artículo de la llanta: " + LlantaID + ". Error: " + ex.Message);
            }
        }

        #endregion ObtenerItemCodeLlanta

        #region ObtenerWhsCode

        public static String RecuperaWhsCodeByLlantaID(int? LlantaID)
        {
            String WhsCode = String.Empty;
            try
            {
                llantasap = new LlantaSAP(ref ConexionSAP.oCompany);
                WhsCode = llantasap.GetWhsCodeByID(LlantaID);

                if (!String.IsNullOrEmpty(WhsCode))
                    return WhsCode;
                else
                    throw new Exception("La llanta no tiene configurado un código de almacén. ");
            }
            catch (Exception ex)
            {
                throw new Exception("No fue posible recuperar el código del almacén de la llanta: " + LlantaID +
                                    ". Error: " + ex.Message);
            }
        }

        #endregion ObtenerWhsCode

        #region ObtenerPlacaMantenible

        /* Comentarios
         * Método encargado de recuperar la placa del mantenible
         * tomando en cuenta que podría ser una caja o un vehículo.
         * donde TipoMantenible = TipoILlanta de la clase llamada
         * IncidenteLlanta y TipoMantenible = 1 para los vehículos
         * TipoMantenible = 2 para las cajas.
         * 
         * Development by: ing. Rair Santos.
         */

        public static String RecuperaPlacaByMantenibleID(int? TipoMantenible, int? MantenibleID)
        {
            String Placa = String.Empty;
            try
            {
                switch (TipoMantenible)
                {
                    case 1:
                        vehiculosap = new VehiculoSAP(ConexionSAP.oCompany);
                        Placa = vehiculosap.GetPlacaByVehiculoID(MantenibleID);
                        break;
                    case 2:
                        activosap = new ActivoSAP(ConexionSAP.oCompany);
                        Placa = activosap.GetPlacaByActivoID(MantenibleID);
                        break;
                }
                //String message = TipoMantenible == 1 ? "No fue posible identificar la placa del vehículo. " : TipoMantenible == 2 ? "No fue posible identificar la placa del activo. " : "No es posible identificar el tipo de mantenible. ";

                //Comentado porque si la llanta no está asginada y se registra
                //como una baja no tendrá los datos de un vehículo.
                //if (!String.IsNullOrEmpty(Placa))
                return Placa;
                //else
                //    throw new Exception(message);
            }
            catch (Exception ex)
            {
                throw new Exception("No fue posible recuperar la placa del mantenible: " + MantenibleID + ". Error: " +
                                    ex.Message);
            }
        }

        #endregion ObtenerPlacaMantenible

        #region ObtenerItemCodeMantenible

        /*Comentarios
         * Método que retorna el código de artículo de los datos
         * maestros de artículo de SAP.
         * 
         * Development by: ing. Rair Santos.
         */

        public static String RecuperaItemCodeByMantenibleID(int? TipoMantenible, int? MantenibleID)
        {
            String ItemCode = String.Empty;
            try
            {
                switch (TipoMantenible)
                {
                    case 1:
                        vehiculosap = new VehiculoSAP(ConexionSAP.oCompany);
                        ItemCode = vehiculosap.GetItemCodeByID(MantenibleID);
                        break;
                    case 2:
                        activosap = new ActivoSAP(ConexionSAP.oCompany);
                        ItemCode = activosap.GetItemCodeByID(MantenibleID);
                        break;
                }
                //String message = TipoMantenible == 1 ? "No fue posible identificar el código de artículo del vehículo. " : TipoMantenible == 2 ? "No fue posible identificar el código de activo fijo del activo. " : "No es posible identificar el tipo de mantenible. ";

                //Comentado porque si se envía la llanta de la pila de llantas disponibles
                // a la pila de desecho, entonces no tiene los datos del vehículo.
                //if (!String.IsNullOrEmpty(ItemCode))
                return ItemCode;
                //else
                //    throw new Exception(message);
            }
            catch (Exception ex)
            {
                throw new Exception("No fue posible recuperar el código de artículo del mantenible: " + MantenibleID +
                                    ". Error: " + ex.Message);
            }
        }

        #endregion ObtenerItemCodeMantenible

        #region ObtenerCentroCostos

        /* Comentarios
         * Clase encargada de obtener el centro de costos
         * para generar la salida de mercacías.
         * 
         * Development by: ing. Rair Santos.
         */

        public static String RecuperaCentroCostos()
        {
            String CentroCostos = String.Empty;
            try
            {
                //Validar el tipo de gestión
                //String Vehiculo/Sucursal = GetTipoGestion();

                //Obtener el centro de costos.
                //CentroCosotos = GetCentroCostos(Vehiculo/Sucursal);
                /* Identificar donde obtener el centro de costos (vehículo o sucursal);*/

                if (!String.IsNullOrEmpty(CentroCostos))
                    return CentroCostos;
                else
                    throw new Exception("No se encontró un centro de costos. ");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion ObtenerCentroCostos

        #region ObtenerDimensión
        /* Comentarios
         * Método encargado de consultar las configuraciones con el fin
         * de saber la dimensión para el centros de costos al que se
         * estará generando la salida de mercancía.
         * 
         * Development by: ing. Rair Santos.
         */
        public static int GetDimension()
        {
            int Dimension;
            String Sentencia;
            try
            {
                String Tabla = (ConexionSAP.oCompany.DbServerType.ToString().Contains("MSSQL") ? true : false) == true
                    ? "[@VSKF_CONFIGURACION]"
                    : @"""@VSKF_CONFIGURACION""";
                Recordset recordset = (Recordset)ConexionSAP.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                Sentencia = String.Empty;
                Sentencia = String.Format("SELECT * FROM {0}", Tabla);
                recordset.DoQuery(Sentencia);
                Dimension = recordset.RecordCount > 0 ? recordset.Fields.Item("U_Dimension").Value : 0;
                return Dimension;
            }
            catch (Exception ex)
            {
                throw new Exception("No fue posible recuperar la dimensión. Error: " + ex.Message);
            }
        }
        #endregion ObtenerDimensión

        #region Validaciones
        /* Comentarios
         * Método encargado de validar la lista de las incidencia
         * que retorna el ws con el fin de evitar que se creen salidas
         * de inventario por parte del motor con datos incompletos.
         * 
         * Development by ing. Rair Santos.
         */
        public static void Validaciones()
        {
            try
            {
                for (int i = 0; i < listIncidente.Count; i++)
                {
                    for (int h = 0; h < listIncidente.Count; h++)
                    {
                        if (h != i)
                        {
                            if (listIncidente[i].Fecha == listIncidente[h].Fecha)
                            {
                                //Actualiza web con Sincronizado = 1 para que
                                //no se tome en cuenta en la siguiente vuelta.
                                IncidenteLlanta temp = listIncidente[h];
                                temp.Sincronizado = 1;
                                SincronizarSalidaWEB(temp);
                                listIncidente.RemoveAt(h);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion Validaciones

        #region GenerarDocumentosInventario
        /*Comentarios
         * Método que genera la salida de inventario únicamente
         * de las llantas asignadas a un vehículo, dichas llantas
         * pueden tener varias salidas de inventario.
         * 
         * Development by: ing. Rair Santos.
         */
        public static bool SalidaInventario(IncidenteLlanta incidente)
        {
            bool Proceso = false;
            try
            {
                SAPbobsCOM.Documents GIssue = (SAPbobsCOM.Documents)ConexionSAP.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenExit);
                String Referencia = incidente.IncidenteID.ToString() + (incidente.Accion == "INSTALACION" ? "01" : incidente.Accion == "DESINSTALACION" ? "02" : incidente.Accion == "BAJA" ? "03" : "04");

                if (Referencia != incidente.IncidenteID.ToString() + "01" & Referencia != incidente.IncidenteID.ToString() + "03")
                    throw new Exception("No es una salida de inventario. ");

                GIssue.DocDate = incidente.Fecha.Value;
                GIssue.TaxDate = incidente.Fecha.Value;
                GIssue.Reference2 = Referencia;
                GIssue.Lines.UserFields.Fields.Item("U_VSKF_VH").Value =
                    RecuperaPlacaByMantenibleID(incidente.TipoMantenible, incidente.MantenibleID);
                GIssue.Lines.UserFields.Fields.Item("U_VSKF_VHNoEco").Value =
                    RecuperaItemCodeByMantenibleID(incidente.TipoMantenible, incidente.MantenibleID);
                GIssue.Comments = String.Format("Salida de mercancía de una llanta emitida por Kananfleet en la fecha {0} con referencia: {1}. ",
                    DateTime.Now.ToString("dd/MM/yyyy"), Referencia);
                GIssue.Lines.ItemCode = RecuperaItemCodeByLlantaID(incidente.Neumatico);
                GIssue.Lines.Quantity = 1;
                GIssue.Lines.WarehouseCode = RecuperaWhsCodeByLlantaID(incidente.Neumatico);

                #region CentroCostos comentado

                /*
                switch(GetDimension())
                {
                    case 0:
                    case 1:
                        GIssue.Lines.CostingCode = RecuperaCentroCostos();
                        break;
                    case 2:
                        GIssue.Lines.CostingCode2 = RecuperaCentroCostos();
                        break;
                    case 3:
                        GIssue.Lines.CostingCode3 = RecuperaCentroCostos();
                        break;
                    case 4:
                        GIssue.Lines.CostingCode4 = RecuperaCentroCostos();
                        break;
                    case 5:
                        GIssue.Lines.CostingCode5 = RecuperaCentroCostos();
                        break;
                }
                 */

                #endregion CentroCostos comentado

                GIssue.Lines.Add();
                try
                {
                    int iSave = GIssue.Add();
                    if (iSave == 0)
                    {
                        System.Console.WriteLine("Salida de inventario. Referencia: " + Referencia);
                        incidente.Referencia = Referencia;
                        incidente.Sincronizado = 1;
                        Proceso = SincronizarSalida(incidente);
                    }
                    else
                    {
                        Proceso = false;
                        throw new Exception();
                    }
                    return Proceso;
                }
                catch (Exception ex)
                {
                    int iError;
                    String sError;
                    String Error;
                    ConexionSAP.oCompany.GetLastError(out iError, out sError);
                    Error = "Código de error: " + iError + " Mensaje de error: " + sError;
                    System.Console.WriteLine(Error);
                    throw new Exception(Error + " " + ex.Message);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al genera la salida de inventario. " + ex.Message);
            }
        }

        /*Comentarios
         * Método que genera la entrada de inventario únicamente
         * de las llantas desasignadas a un vehículo, dichas llantas
         * pueden tener varias entradas de inventario.
         * 
         * Development by: ing. Rair Santos.
         */
        public static bool EntradaInventario(IncidenteLlanta incidente)
        {
            bool Proceso = false;
            try
            {
                SAPbobsCOM.Documents GIssue = (SAPbobsCOM.Documents)ConexionSAP.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);
                String Referencia = incidente.IncidenteID.ToString() + (incidente.Accion == "INSTALACION" ? "01" : incidente.Accion == "DESINSTALACION" ? "02" : incidente.Accion == "BAJA" ? "03" : "04");

                if (Referencia != incidente.IncidenteID.ToString() + "02" & Referencia != incidente.IncidenteID.ToString() + "04")
                    throw new Exception("No es una entrada de inventario. ");

                GIssue.DocDate = incidente.Fecha.Value;
                GIssue.TaxDate = incidente.Fecha.Value;
                GIssue.Reference2 = Referencia;
                GIssue.Lines.UserFields.Fields.Item("U_VSKF_VH").Value = RecuperaPlacaByMantenibleID(incidente.TipoMantenible, incidente.MantenibleID);
                GIssue.Lines.UserFields.Fields.Item("U_VSKF_VHNOECO").Value = RecuperaItemCodeByMantenibleID(incidente.TipoMantenible, incidente.MantenibleID);
                GIssue.Comments = String.Format("Entrada de mercancía de una llanta emitida por Kananfleet en la fecha {0} con referencia: {1}. ",
                    DateTime.Now.ToString("dd/MM/yyyy"), Referencia);
                GIssue.Lines.ItemCode = RecuperaItemCodeByLlantaID(incidente.Neumatico);
                GIssue.Lines.Quantity = 1;
                GIssue.Lines.WarehouseCode = RecuperaWhsCodeByLlantaID(incidente.Neumatico);

                #region CentroCostos comentado

                /*
                switch(GetDimension())
                {
                    case 0:
                    case 1:
                        GIssue.Lines.CostingCode = RecuperaCentroCostos();
                        break;
                    case 2:
                        GIssue.Lines.CostingCode2 = RecuperaCentroCostos();
                        break;
                    case 3:
                        GIssue.Lines.CostingCode3 = RecuperaCentroCostos();
                        break;
                    case 4:
                        GIssue.Lines.CostingCode4 = RecuperaCentroCostos();
                        break;
                    case 5:
                        GIssue.Lines.CostingCode5 = RecuperaCentroCostos();
                        break;
                }
                 */

                #endregion CentroCostos comentado

                GIssue.Lines.Add();
                try
                {
                    int iSave = GIssue.Add();
                    if (iSave == 0)
                    {
                        System.Console.WriteLine("Entrada de inventario. Referencia: " + Referencia);
                        incidente.Referencia = Referencia;
                        incidente.Sincronizado = 1;
                        Proceso = SincronizarEntrada(incidente);
                    }
                    else
                    {
                        Proceso = false;
                        throw new Exception();
                    }
                    return Proceso;
                }
                catch (Exception ex)
                {
                    int iError;
                    String sError;
                    String Error;
                    ConexionSAP.oCompany.GetLastError(out iError, out sError);
                    Error = "Código de error: " + iError + " Mensaje de error: " + sError;
                    System.Console.WriteLine(Error);
                    throw new Exception(Error + " " + ex.Message);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al genera la entrada de inventario. " + ex.Message);
            }
        }
        #endregion GenerarDocumentosInventario

        #region SincronizarSalida
        /* Comentarios
         * Método encargado de realizar la sincronización de la salida de inventario
         * Sincroniza en SAP (VSKF_IncidenteLlanta, U_sincronizado) y Sincroniza en web ()
         * en caso de SAP el 0 = es no sincronizado y 1 = sincronizado con web.
         */
        public static bool SincronizarSalida(IncidenteLlanta incidente)
        {
            bool Sinc = false;
            try
            {
                if (ExisteSalidaSAP(incidente))
                    Sinc = SincronizarSalidaWEB(incidente);
                else
                    Sinc = false;
                return Sinc;
            }
            catch (Exception ex)
            {
                throw new Exception("No fue posible sincronizar la salida de mercancía. " + ex.Message);
            }
        }

        /* Comentarios
         * Método que valida la existencia de un documento de tipo salida
         * de mercacías en SAP.
         * 
         * Development by: ing. Rair Santos.
         */
        public static bool ExisteSalidaSAP(IncidenteLlanta incidente)
        {
            bool existe = false;
            String consulta = String.Empty;
            try
            {
                Accion = (Recordset)ConexionSAP.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                //Comentado debido a que una salida de inventario de igual manera puede ser una baja.
                //String Referencia = incidente.IncidenteID.ToString() + "01";
                String Referencia = incidente.IncidenteID.ToString() + (incidente.Accion == "INSTALACION" ? "01" : incidente.Accion == "DESINSTALACION" ? "02" : incidente.Accion == "BAJA" ? "03" : "04");
                String Tabla = (ConexionSAP.oCompany.DbServerType.ToString().Contains("MSSQL") ? true : false) == true ? "OIGE" : @"""OIGE""";
                consulta = String.Format(@"SELECT ""Ref2"", ""Comments"" FROM {0} WHERE ""Ref2"" = '{1}'", Tabla, Referencia);
                Accion.DoQuery(consulta);
                String ref2 = Accion.Fields.Item("Ref2").Value;
                String coments = Accion.Fields.Item("Comments").Value;
                if (ref2 == Referencia)
                    if (coments.Contains(Referencia) & coments.Contains("Kananfleet"))
                        existe = true;

                return existe;
            }
            catch (Exception ex)
            {
                throw new Exception("Salida no soncronizada con SAP. " + ex.Message);
            }
        }

        /* Comentarios
         * Método encargado de generar la sincronización de la salida
         * donde actualiza el campo "Sincronizado" de la tabla IncidenteLlanta
         * y le asigna el número de referencia de la salida en el campo
         * "Referencia".
         * 
         * Development by: ing. Rair Santos.
         */
        public static bool SincronizarSalidaWEB(IncidenteLlanta incidente)
        {
            String publickey = ConfigurationSettings.AppSettings.Get("publickey").ToString();
            String privatekey = ConfigurationSettings.AppSettings.Get("privatekey").ToString();
            //Comentado debido a que una salida de inventario de igual manera puede ser una baja.
            //incidente.Referencia = incidente.IncidenteID.ToString() + "01";
            incidente.Referencia = incidente.IncidenteID.ToString() + (incidente.Accion == "INSTALACION" ? "01" : incidente.Accion == "DESINSTALACION" ? "02" : incidente.Accion == "BAJA" ? "03" : "04");
            incidente.Sincronizado = 1;
            IncidenteLlanta local;
            bool Sinc = false;
            try
            {
                ConsultarIncidentes = new IncidenteLlantaWS(Guid.Parse(publickey), privatekey);
                local = ConsultarIncidentes.UpdateSAP(incidente);
                if (local.Sincronizado == 1 & local.Referencia == incidente.Referencia)
                    Sinc = true;
                else
                    Sinc = false;
                return Sinc;
            }
            catch (Exception ex)
            {
                throw new Exception("No fue posible sincronizar las salidas de mercacía con la DB en linea. " + ex.Message);
            }
        }
        #endregion SincronizarSalida

        #region SincronizarEntrada
        public static bool SincronizarEntrada(IncidenteLlanta incidente)
        {
            bool Sinc = false;
            try
            {
                if (ExisteEntradaSAP(incidente))
                    Sinc = SincronizarEntradaWEB(incidente);
                else
                    Sinc = false;
                return Sinc;
            }
            catch (Exception ex)
            {
                throw new Exception("No fue posible sincronizar la salida de mercancía. " + ex.Message);
            }
        }

        /* Comentarios
         * Método que valida la existencia de un documento de tipo entrada
         * de mercacías en SAP.
         * 
         * Development by: ing. Rair Santos.
         */
        public static bool ExisteEntradaSAP(IncidenteLlanta incidente)
        {
            bool existe = false;
            String consulta = String.Empty;
            try
            {
                Accion = (Recordset)ConexionSAP.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                String Referencia = incidente.IncidenteID.ToString() + (incidente.Accion == "INSTALACION" ? "01" : incidente.Accion == "DESINSTALACION" ? "02" : incidente.Accion == "BAJA" ? "03" : "04");
                String Tabla = (ConexionSAP.oCompany.DbServerType.ToString().Contains("MSSQL") ? true : false) == true ? "OIGN" : @"""OIGN""";
                consulta = String.Format(@"SELECT ""Ref2"", ""Comments"" FROM {0} WHERE ""Ref2"" = {1}", Tabla, Referencia);
                Accion.DoQuery(consulta);
                String ref2 = Accion.Fields.Item("Ref2").Value;
                String coments = Accion.Fields.Item("Comments").Value;
                if (ref2 == Referencia)
                    if (coments.Contains(Referencia) & coments.Contains("Kananfleet"))
                        existe = true;

                return existe;
            }
            catch (Exception ex)
            {
                throw new Exception("Entrada no soncronizada con SAP. " + ex.Message);
            }
        }

        /* Comentarios
         * Método encargado de generar la sincronización de la entrada
         * donde actualiza el campo "Sincronizado" de la tabla IncidenteLlanta
         * y le asigna el número de referencia de la entrada en el campo
         * "Referencia".
         * 
         * Development by: ing. Rair Santos.
         */
        public static bool SincronizarEntradaWEB(IncidenteLlanta incidente)
        {
            String publickey = ConfigurationSettings.AppSettings.Get("publickey").ToString();
            String privatekey = ConfigurationSettings.AppSettings.Get("privatekey").ToString();
            incidente.Referencia = incidente.IncidenteID.ToString() + (incidente.Accion == "INSTALACION" ? "01" : incidente.Accion == "DESINSTALACION" ? "02" : incidente.Accion == "BAJA" ? "03" : "04");
            incidente.Sincronizado = 1;
            IncidenteLlanta local;
            bool Sinc = false;
            try
            {
                ConsultarIncidentes = new IncidenteLlantaWS(Guid.Parse(publickey), privatekey);
                local = ConsultarIncidentes.UpdateSAP(incidente);
                if (local.Sincronizado == 1 & local.Referencia == incidente.Referencia)
                    Sinc = true;
                else
                    Sinc = false;
                return Sinc;
            }
            catch (Exception ex)
            {
                throw new Exception("No fue posible sincronizar las entrada de mercacía con la DB en linea. " + ex.Message);
            }
        }
        #endregion SincronizarEntrada

        #endregion Llantas

        #region Alertas
        /* Comentarios
         * Método que corresponde al proceso de generación de las alertas.
         * 
         * Development by: ing. Rair Santos.
         */
        public static void Alertas()
        {
            /*Se limpia la consola*/
            //System.Console.Clear();
            /*Se establecen colores. */
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Green;
            try
            {
                /*Método que obtiene las alertas por empresa.*/
                ConsultarAlertas();

                /*Método que inserta las alertas de una empresa.*/
                InsertarAlertas();
            }
            catch (Exception ex)
            {
                throw new Exception("Error en proceso de alertas. " + ex.Message);
            }
        }

        #region ConsultarAlertas
        public static void ConsultarAlertas()
        {
            System.Console.WriteLine("Obteniendo parámetros. ");
            String publickey = ConfigurationSettings.AppSettings.Get("publickey").ToString();
            String privatekey = ConfigurationSettings.AppSettings.Get("privatekey").ToString();
            System.Console.WriteLine("Creando instancias. ");
            CAlertas = new MotorAlertasWS(Guid.Parse(publickey), privatekey);
            System.Console.WriteLine("Consultando alertas.");
            try
            {
                var AlertaWS = CAlertas.GetAllAlertasByEmpresaID(EmpID);
                list = AlertaWS.ListAlerts;
                System.Console.WriteLine("Ok.");
                System.Console.WriteLine("Alertas obtenidas: " + list.Count);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("No fue posible obtener alertas." + ex.Message);

                //if (ConexionSAP.bConectado)
                //{
                //    System.Console.WriteLine("Cerrando conexiones activas...");
                //    ConexionSAP.finalizarDIAPI(database);
                //    configuraciones = null;
                //    database = string.Empty;
                //}
                throw new Exception("Error al consultar alertas. ");
            }
        }
        #endregion ConsultarAlertas

        #region InsertarAlertas
        public static void InsertarAlertas()
        {
            try
            {
                foreach (vskf_Catalogos_INFO data in vskf_Globales.Catalogos)
                {
                    database = data.Database;
                    if (ConexionSAP.inicializaDIAPI(data.Database, out MensajeSAP))
                    {
                        System.Console.WriteLine("Identificando configuraciónes de la sociedad.");
                        configuraciones = ocompany.EmpresaConfiguracion(data.Database, out Consola);
                        if (configuraciones != null)
                        {
                            System.Console.WriteLine("Proceso de Insertar/Actualizar inciado. ");
                            InsertOrUpdate();
                        }
                    }
                   // else
                       // throw new Exception("Error:" + MensajeSAP);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al insertar. " + ex.Message);
            }
        }
        #endregion InsertarAlertas

        #region InsertOrUpdate
        public static void InsertOrUpdate()
        {
            System.Console.WriteLine("Creando instancias. ");
            try
            {
                Accion = (Recordset)ConexionSAP.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                bool sql = ConexionSAP.oCompany.DbServerType.ToString().Contains("MSSQL") ? true : false;
                TablaRelEmpresa = sql ? "[@VSKF_RELEMPRESA]" : @"""@VSKF_RELEMPRESA""";

                AlertaMantData = new AlertaMantenimientoSAP(ConexionSAP.oCompany);
                AlertaRutinaData = new AlertaRutinaSAP(ConexionSAP.oCompany);
                AlertaData = new AlertaSAP(ConexionSAP.oCompany);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al crear instancias en SAP. " + ex.Message);
            }

            for (int i = 0; i < list.Count; i++)
            {
                /*Validar si existe*/

                /*Inserta las alertas de todo tipo. */
                if (list[i].AlertaID != null & list[i].AlertaID > 0)
                {
                    String Fr = list[i].Descripcion_Fr.Replace("'", "''");
                    Query = String.Empty;
                    Query = String.Format("INSERT INTO {0} VALUES('{1}','{2}',{3},'{4}','{5}','{6}','{7}',{8},{9},'{10}',{11},{12},{13},{14},{15},{16},{17},{18},'{19}','{20}')",
                        AlertaData.NombreTablaAlerta, list[i].AlertaID, list[i].AlertaID, list[i].AlertaID, list[i].Descripcion_Es, list[i].Descripcion_En, list[i].Descripcion_Pt, Fr, list[i].CurrentUser.UsuarioID ?? 0, list[i].Company.EmpresaID, list[i].SubPropietario.SucursalID.ToString(), /*EstaActivo*/"1", list[i].IAlertaID ?? 0, list[i].TipoAlerta ?? 0, list[i].TypeOfType ?? 0, list[i].NumEnviados ?? 0, "0", "0", "1", Guid.NewGuid().ToString(), list[i].RutinaID ?? 0);

                    try
                    {
                        Accion.DoQuery(Query);
                        System.Console.WriteLine(Query);
                        System.Console.WriteLine("Registro de alerta insertado. No: " + i);
                    }
                    catch (Exception ex)
                    {
                        Query = String.Empty;
                        int? RutinaID =  (string.IsNullOrEmpty( list[i].RutinaID.ToString()))? 0 :list[i].RutinaID;
                        Query = String.Format(@"UPDATE {0} SET ""Code"" = '{1}', ""Name"" = '{2}', U_AlertaID = '{3}', U_Descripcion_Es = '{4}', U_Descripcion_En = '{5}', U_Descripcion_Pt = '{6}', U_Descripcion_Fr = '{7}', U_UsuarioID = '{8}', U_EmpresaID = '{9}', U_SucursalID = '{10}', U_EstaActivo = '{11}', U_IAlertaID = '{12}', U_TipoAlerta = '{13}', U_TypeOfType = '{14}', U_NumEnviados = '{15}', U_NumEnviadosTiempo = '{16}', U_OrdenServicioID = '{17}', U_RutinaID = {18} WHERE ""Code"" = '{1}'", AlertaData.NombreTablaAlerta, list[i].AlertaID, list[i].AlertaID, list[i].AlertaID, list[i].Descripcion_Es, list[i].Descripcion_En, list[i].Descripcion_Pt, Fr, Convert.ToString(list[i].CurrentUser.UsuarioID), Convert.ToString(list[i].Company.EmpresaID), Convert.ToString(list[i].SubPropietario.SucursalID), Convert.ToInt32(list[i].Activa), list[i].IAlertaID, list[i].TipoAlerta, Convert.ToString(list[i].TypeOfType), Convert.ToString(list[i].NumEnviados), "0", "0", RutinaID);
                        System.Console.WriteLine(Query);
                        Accion.DoQuery(Query);
                        System.Console.WriteLine("Registro de alerta actualizado. No: " + i);
                    }

                    /*Validar si existe*/
                    /*Inserta alertas de tipo mantenimiento con los detalles. */
                    if (list[i].TipoAlerta == 1)
                    {
                        var AlertaMantWS = CAlertas.GetAlertaMttoByID(list[i].IAlertaID);

                        Query = String.Empty;
                        Query = String.Format("INSERT INTO {0} VALUES('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}')", AlertaMantData.NombreTablaAlertaMantenimiento, AlertaMantWS.AlertaMantenimiento.AlertaMantenimientoID, AlertaMantWS.AlertaMantenimiento.AlertaMantenimientoID, AlertaMantWS.AlertaMantenimiento.AlertaMantenimientoID, AlertaMantWS.AlertaMantenimiento.servicio.ServicioID, AlertaMantWS.AlertaMantenimiento.empresa.EmpresaID, Convert.ToInt32(AlertaMantWS.AlertaMantenimiento.TipoAlerta), Convert.ToInt32(AlertaMantWS.AlertaMantenimiento.TypeOfType), AlertaMantWS.MantenibleID, AlertaMantWS.TipoMantenibleID, "1", "");
                        try
                        {
                            System.Console.WriteLine(Query);
                            Accion.DoQuery(Query);

                            System.Console.WriteLine("Registro de detalle de alerta insertado. N: " + 1);
                        }
                        catch (Exception ex)
                        {
                            Query = String.Empty;
                            Query = String.Format(@"UPDATE {0} SET ""Code"" = '{1}', ""Name"" = '{2}', U_AlertaMttoID = '{3}', U_ServicioID = '{4}', U_EmpresaID = '{5}', U_TipoAlerta = '{6}', U_TypeOfType = '{7}', U_MantenibleID = '{8}', U_TipoMantenibleID = '{9}' WHERE ""Code"" = '{1}'", AlertaMantData.NombreTablaAlertaMantenimiento, AlertaMantWS.AlertaMantenimiento.AlertaMantenimientoID, AlertaMantWS.AlertaMantenimiento.AlertaMantenimientoID, AlertaMantWS.AlertaMantenimiento.AlertaMantenimientoID, AlertaMantWS.AlertaMantenimiento.servicio.ServicioID, AlertaMantWS.AlertaMantenimiento.empresa.EmpresaID, AlertaMantWS.AlertaMantenimiento.TipoAlerta, Convert.ToInt32(AlertaMantWS.AlertaMantenimiento.TypeOfType), AlertaMantWS.MantenibleID, AlertaMantWS.TipoMantenibleID);
                            System.Console.WriteLine(Query);
                            Accion.DoQuery(Query);
                            System.Console.WriteLine("Registro de detalle de alerta actualizado. No: " + 1);
                        }
                    }

                    /*Inserta alertas de tipo mantenimiento de rutina con los detalles. */
                    if (list[i].TipoAlerta == 8)
                    {
                        var AlertaRutWS = CAlertas.GetAlertaRutinaByID(list[i].IAlertaID);

                        Query = String.Empty;
                        Query = String.Format("INSERT INTO {0} VALUES('{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}')", AlertaRutinaData.NombreTablaAlertaRutina, AlertaRutWS.AlertaRutinaID, AlertaRutWS.AlertaRutinaID, AlertaRutWS.AlertaRutinaID, AlertaRutWS.ServicioID, AlertaRutWS.VehiculoID, AlertaRutWS.EmpresaID, AlertaRutWS.TipoAlerta, AlertaRutWS.TypeOfType, AlertaRutWS.MantenibleID, AlertaRutWS.TipoMantenibleID, AlertaRutWS.Sincronizado);
                        try
                        {
                            System.Console.WriteLine(Query);
                            Accion.DoQuery(Query);
                            System.Console.WriteLine("Registro de detalle de alerta insertado. N: " + 1);
                        }
                        catch (Exception ex)
                        {
                            Query = String.Empty;
                            Query = String.Format(@"UPDATE {0} SET ""Code"" = '{1}', ""Name"" = '{2}', U_ALERTARUTINAID = '{3}', U_SERVICIOID = '{4}', U_VEHICULOID = '{5}', U_EMPRESAID = '{6}', U_TIPOALERTA = '{7}', U_TYPEOFTYPE = '{8}', U_MANTENIBLEID = '{9}', U_TIPOMANTENIBLEID = '{10}', U_SINCRONIZADO = '{11}' WHERE ""Code"" = '{1}'", AlertaRutinaData.NombreTablaAlertaRutina, AlertaRutWS.AlertaRutinaID, AlertaRutWS.AlertaRutinaID, AlertaRutWS.AlertaRutinaID, AlertaRutWS.ServicioID, AlertaRutWS.VehiculoID, AlertaRutWS.EmpresaID, AlertaRutWS.TipoAlerta, AlertaRutWS.TypeOfType, AlertaRutWS.MantenibleID, AlertaRutWS.TipoMantenibleID, AlertaRutWS.Sincronizado);
                            System.Console.WriteLine(Query);
                            Accion.DoQuery(Query);
                            System.Console.WriteLine("Registro de detalle de alerta actualizado. No: " + 1);
                        }
                    }

                }
            }
            System.Console.WriteLine("Proceso finalizado.");
            //System.Console.ReadLine();
        }
        #endregion InsertOrUpdate

        #region Vehiculos
        SBO_KF_SUCURSAL_INFO s = new SBO_KF_SUCURSAL_INFO();

        #endregion Vehiculos

        public static void RegistraVehiculos()
        {
            List<VSKF_Vehiculos_INFO> objDev = new List<VSKF_Vehiculos_INFO>();
            tools_vskf tool = new tools_vskf();
            
            objDev = tool.ObtenVehiculos();

            
            

            foreach (vskf_Catalogos_INFO data in vskf_Globales.Catalogos)
            {
                database = data.Database;
                if (ConexionSAP.inicializaDIAPI(data.Database, out MensajeSAP))
                {
                    System.Console.WriteLine("Identificando configuraciónes de la sociedad.");
                    configuraciones = ocompany.EmpresaConfiguracion(data.Database, out Consola);
                    if (configuraciones != null)
                    {
                        VehiculoSAP vSAP = new VehiculoSAP(ConexionSAP.oCompany);

                        System.Console.WriteLine("Proceso de Insertar/Actualizar inciado. ");
                        foreach (VSKF_Vehiculos_INFO vh in objDev)
                        {
                            
                            Vehiculo v = tool.Add(vh);
                            Console.WriteLine(String.Format("Intentando isnsertar el vh: ", v.Nombre));
                            //tools_vskf.vhKFtovhsap(v, ref vSAP);
                            vh.U_VEHICULOID = v.VehiculoID;
                            vSAP.ItemCode = v.Nombre;
                            vSAP.VehiculoToSAP();
                            vSAP.Sincronizado = 0;
                            vSAP.UUID = Guid.NewGuid();
                            vSAP.vehiculo = v;
                            try
                            {
                                vSAP.Insert();
                            }
                            catch(Exception ex)
                            {
                                Console.WriteLine(String.Format("No se insertó en SAP el vh: ", vSAP.ItemCode));
                            }
                            
                            vSAP.ActualizarVehicleID();
                            

                            //this.view.Entity.vehiculo.Propietario.EmpresaID = this.configs.UserFull.Dependencia.EmpresaID;
                            //this.view.Entity.VehiculoToSAP();
                            //this.view.Entity.Sincronizado = 0;
                            //this.view.Entity.UUID = Guid.NewGuid();
                            //Kanan.Vehiculos.BO2.Vehiculo oTemp = this.VehiculoService.Add(this.view.Entity.vehiculo);
                            //this.view.Entity.vehiculo = oTemp;
                            //this.view.Entity.Insert();
                            //if (oTemp == null)
                            //    oHerramientas.ColocarMensaje("No se ha sincronizado el vehículo con la nube. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                            //else
                            //{
                            //    this.view.Entity.vehiculo.VehiculoID = oTemp.VehiculoID;
                            //    //this.view.Entity.Update();
                            //    this.view.Entity.ActualizarVehicleID();
                            //}




                        }
                    }
                }
                else
                    throw new Exception("Error:" + MensajeSAP);
            }
            

            

        }

        #endregion Alertas

        #region HistoricoDesplasamiento
        /* Comentarios
         * Método encargado de realizar la inserción de
         * los históricos de desplazamiento realizado por
         * el motor de desplazamiento (integración de
         * proveedor de rastreo satelital).
         * 
         * Development by: Rair Santos.
         */
        public static void HistoricoDesplazamiento()
        {
            /*Se limpia la consola*/
            //System.Console.Clear();
            /*Se establecen colores. */
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Green;
            System.Console.WriteLine("Obteniendo parámetros. ");
            String publickey = ConfigurationSettings.AppSettings.Get("publickey").ToString();
            String privatekey = ConfigurationSettings.AppSettings.Get("privatekey").ToString();
            System.Console.WriteLine("Creando instancias. ");
            CDespHisto = new MotorAlertasWS(Guid.Parse(publickey), privatekey);
            System.Console.WriteLine("Consultando historico de desplazamientos. ");
            try
            {
                var HistoricoDesplazamientoWS = CDespHisto.GetDesplazamientoHistoricoByEmpresaID(EmpID);
                listDespla = HistoricoDesplazamientoWS;
                System.Console.WriteLine("Ok.");
                System.Console.WriteLine("Historicos obtenidos: " + listDespla.Count);

                System.Console.WriteLine("Proceso de inserción inciado. ");
                foreach (vskf_Catalogos_INFO data in vskf_Globales.Catalogos)
                {
                    database = data.Database;
                    if (ConexionSAP.inicializaDIAPI(data.Database, out MensajeSAP))
                    {
                        System.Console.WriteLine("Identificando configuraciónes de la sociedad.");
                        configuraciones = ocompany.EmpresaConfiguracion(data.Database, out Consola);
                        if (configuraciones != null)
                        {
                            System.Console.WriteLine("Proceso de insertar/actualizar inciado. ");
                            InsertHistoricoDesplazamiento();
                        }
                    }
                    else
                        throw new Exception("Error:" + MensajeSAP);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region InsertHistoricoDesplazamiento
        public static void InsertHistoricoDesplazamiento()
        {
            try
            {
                System.Console.WriteLine("Creando instancias. ");
                DesplaHistoSAP = new DesplazamientoHistoricoSAP(ConexionSAP.oCompany);
                Accion = (Recordset)ConexionSAP.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                for (int t = 0; t < listDespla.Count; t++)
                {
                    //Validando objeto.
                    System.Console.WriteLine("Validando datos de registro. ");
                    if (ValidarHistoDesp(listDespla[t]))
                    {
                        System.Console.WriteLine("Ok.");
                        //Obteniendo consecutigo.
                        System.Console.WriteLine("Obteniendo consecutivo. ");
                        int i = DesplaHistoSAP.GetNextID();

                        if (i > 0)
                        {
                            System.Console.WriteLine("Ok.");
                            //Insertando registro de listado.
                            System.Console.WriteLine("Preparando para insertar. ");
                            DesplaHistoSAP.DesplaHisto = listDespla[t];
                            System.Console.WriteLine("Asignando consecutivo. ");
                            DesplaHistoSAP.DesplaHisto.DesplazamientoHistoricoID = i;
                            try
                            {
                                System.Console.WriteLine("Insertando. No. " + (t + 1));
                                DesplaHistoSAP.Insert();
                                System.Console.WriteLine("Ok.");
                            }
                            catch (Exception ex)
                            {
                                /*El proceso continua.*/
                                System.Console.WriteLine("Registro no insertado. Error: " + ex.Message);
                            }
                        }
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion InsertHistoricoDesplazamiento

        #region ValidarHistoDesp
        /* Comentarios
         * 
         * Método encargado de realizar la validación
         * de los BO para los historicos de desplazamiento.
         * Consulta existencia y valida propiedades contenidas.
         */
        public static bool ValidarHistoDesp(DesplazamientoHistorico HDes)
        {
            try
            {
                #region ValidarBO
                if (HDes.Distancia <= 0)
                {
                    return false;
                }
                if (HDes.Fecha == null)
                {
                    return false;
                }
                if (HDes.VehiculoID <= 0)
                    return false;
                if (String.IsNullOrEmpty(HDes.NombreVehiculo))
                    return false;
                if (String.IsNullOrEmpty(HDes.PlacaVehiculo))
                    return false;
                if (String.IsNullOrEmpty(HDes.MarcaVehiculo))
                    return false;
                if (String.IsNullOrEmpty(HDes.ModeloVehiculo))
                    return false;
                #endregion ValidarBO

                #region ValidarExistencia
                if (DesplaHistoSAP.ValidarExistencia(HDes.Fecha, HDes.VehiculoID))
                {
                    System.Console.WriteLine("Registro ya insertado.");
                    return false;
                }
                #endregion ValidarExistencia

                return true;
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion ValidarHistoDesp

        #endregion HistoricoDesplasamiento

        #region ConexiónSAP
        /*Comentarios
         * Método que conecta la aplicación de consola al
         * serviro sap configurado en el xml.
         */
        public static void ConexionServidorSAP()
        {
            try
            {
                #region ConexiónSAP - old
                /*
            System.Console.WriteLine("Iniciando procesos...");
            SAPbouiCOM.SboGuiApi SboGuiApi = null;
            string sConnectionString = null;
            SboGuiApi = new SAPbouiCOM.SboGuiApi();
            System.Console.WriteLine("Obteniendo parámetros de conexión.");
            try
            {
                sConnectionString = System.Convert.ToString(Environment.GetCommandLineArgs().GetValue(1));
                System.Console.WriteLine("ConectionString obtenida.");
            }
            catch
            {
                sConnectionString = "0030002C0030002C00530041005000420044005F00440061007400650076002C0050004C006F006D0056004900490056";
                System.Console.WriteLine("Default sconectionString.");
            }

            System.Console.WriteLine("Conectando a la DB SAP.");
            try
            {
                System.Console.WriteLine("Conectando al servidor.");
                SboGuiApi.Connect(sConnectionString);
                System.Console.WriteLine("Ok.");
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("Error en la conexión con su servidor. Verificar red.");
                //System.Console.ReadLine();
            }

            App = SboGuiApi.GetApplication(-1);
            try
            {
                System.Console.WriteLine("Obteniendo parámetros de la sociedad.");
                Comp = App.Company.GetDICompany();
                System.Console.WriteLine("Ok.");
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("Error en devolucion con el servidor.");
                System.Console.ReadLine();
            }
            */
                #endregion ConexiónSAP - old

                #region ConexiónSAP - XML

                #region Instancias
                /*Se establecen colores. */
                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.Green;
                System.Console.WriteLine("Iniciando procesos...");
                try
                {
                    herramientas = new Herramientas();
                    //conexionsap = new ConexionSAP();
                    ocompany = new Company();
                    System.Console.WriteLine("Ok. ");
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al crear instancias. " + ex.Message);
                }
                #endregion Instancias

                #region Configuraciones
                try
                {
                    System.Console.WriteLine("Cargando archivo de configuración...");
                    aprobado = herramientas.LeerConfiguracion();
                    if (aprobado)
                        System.Console.WriteLine("Cargado correctamente.");
                    else
                        throw new Exception();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al leer la configuración. " + ex.Message);
                }
                #endregion Configuraciones

                #region Catálogos
                try
                {
                    System.Console.WriteLine("Cargando catálogos. ");
                    aprobado = herramientas.buscaCatalgos();
                    if (aprobado)
                        System.Console.WriteLine("Cargado correctamente. ");
                    else
                    {
                        System.Console.WriteLine("No se encontró algún catálogo. ");
                        System.Console.WriteLine("Creando catálogos. ");
                        ocompany.creaCatalgosValidos();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("No encontró catálogo. " + ex.Message);
                }
                #endregion Catálogos

                #region Conectando
                try
                {
                    System.Console.WriteLine("Verificando usuario conectado. ");
                    if (ConexionSAP.bConectado)
                    {
                        System.Console.WriteLine("Cerrando conexiónes activas. ");
                        ConexionSAP.finalizarDIAPI(database);
                        if (!ConexionSAP.bConectado)
                            System.Console.WriteLine("Ok. ");
                        else
                            System.Console.WriteLine("Error al desconectar a usuario. ");
                    }
                    else
                        System.Console.WriteLine("Usuario no conectado. ");
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al inciar/finalizar conexión de usuario. ");
                }
                #endregion Conectando

                #endregion ConexiónSAP - XML
            }
            catch (Exception ex)
            {
                throw new Exception("Error en conexión con el servidor SAP. " + ex.Message);
            }
        }
        #endregion ConexiónSAP
    }
}