﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.Motor.Alertas
{
    #region Comentarios
    /**
     * Comentarios: Clase utilizada para manejar propiedades
     * a nivel global, asignadas en tiempo de carga para mantener 
     * claves de usuario y/o realizar procesos.
     * Fecha: 28/02/2018
     * Autor: Carlos Santiago
     **/
    #endregion
    public class vskf_Globales
    {
        public static string ServerName { get; set; }
        public static string UserName { get; set; }
        public static string PassWordUser { get; set; }
        public static string TipoServidor { get; set; }
        public static string UsuarioSAP { get; set; }
        public static string PassWordSAP { get; set; }
        public static string ServidorLicencia { get; set; }
        public static string BaseDatosTest { get; set; }
        public static Boolean bSqlConnection { get; set; }
        public static string ServiceAPI { get; set; }
        public static string ApiKey { get; set; }
        public static List<vskf_Catalogos_INFO> Catalogos { get; set; }
        public static String CadenaConexion { get; set; }
    }
}
