﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.Motor.Alertas
{
    public class vskf_Catalogos_INFO
    {
        public string Database { get; set; }
        public string Company { get; set; }
        public string Version { get; set; }
        public string User { get; set; }
        public string Log { get; set; }
    }
}
