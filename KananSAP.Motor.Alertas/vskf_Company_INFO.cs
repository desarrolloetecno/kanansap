﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.Motor.Alertas
{
    public class vskf_Company_INFO
    {
        public string CompanyName { get; set; }
        public string CompanyAdres { get; set; }
        public string RFC { get; set; }
    }
}
