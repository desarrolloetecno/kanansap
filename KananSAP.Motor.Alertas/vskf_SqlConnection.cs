﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.Motor.Alertas
{
    public class vskf_SqlConnection
    {
         private String _strConnRhm;
        private SqlCommand _sqlCmd = new SqlCommand();
        private SqlConnection _sqlConn = new SqlConnection();
        private DataSet _dataSet = new DataSet();
        private SqlDataAdapter _dataAdapter = new SqlDataAdapter();
        private SqlTransaction _sqlTrans;
        
        public  String strConn 
        { 
            get
            {
                return _strConnRhm;
            }
            
        }


        public vskf_SqlConnection(string strConn)
        {
            _strConnRhm = strConn;
        }
        

        public void abrirConexion()
        {

            _sqlConn.ConnectionString = _strConnRhm;
                _sqlConn.Open();            
        }
        

        public void cerrarConexion()
        {
            if (_sqlConn.State == ConnectionState.Open)
                _sqlConn.Close();
        }
        
        public void llenarDataSet (String strQuery, int timeOutmiliSeconds=0)
        {
            _sqlCmd.Connection = _sqlConn;
            Boolean abierta = false;
            if (_sqlConn.State != ConnectionState.Open)
            {
                abrirConexion();
                abierta = true;
            }

            _sqlCmd.CommandType = CommandType.Text;
            _sqlCmd.CommandText = strQuery;
            if (timeOutmiliSeconds > 0)
                _sqlCmd.CommandTimeout = timeOutmiliSeconds;
            _dataAdapter = new SqlDataAdapter(_sqlCmd);
            _dataSet = new DataSet();         

            _dataAdapter.Fill(_dataSet);

            if (abierta)
            {
                if (_sqlConn.State == ConnectionState.Open)
                    cerrarConexion();
            }


        }       

        public  DataSet dataSet { get { return _dataSet; } }
        public SqlConnection sqlConn { get { return _sqlConn; } }
        
        public void iniciaTrans()
        {
            _sqlTrans = _sqlConn.BeginTransaction();
            _sqlCmd.Connection = _sqlConn;
            _sqlCmd.Transaction = _sqlTrans;
        }
        

        public void commitTrans()
        {
            _sqlTrans.Commit();
        }
        public void rollBack()
        {
            _sqlTrans.Rollback();
        }       
    }
}
    
