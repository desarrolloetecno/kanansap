﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace KananSAP.Motor.Alertas
{
    #region Comentarios
    /** Comentarios: Clase utilizada para 
     * implementar metodos auxiliares en proceso o validaciones.     
     * Autor: Carlos Santiago
     **/
    #endregion
    public class Herramientas
    {
        #region Metodos

        public void GuardarLogError(Exception oError, String sMetodo, String sClase, String sModulo, String sDatos = "", String sRazonSocial = "")
        {
            #region Comentarios
            /**
             * Comentarios: Metodo encargado de crear o modificar archivo xml con informacion 
             * de error catcheado en ejecucion de codigo.
             * Autor: Carlos Santiago
             */
            #endregion

            try
            {
                XmlDocument docXML = new XmlDocument();
                String pathProject = AppDomain.CurrentDomain.BaseDirectory + "\\Errores";
                if (sRazonSocial != "")
                    pathProject += "\\" + sRazonSocial;

                String pathXML = string.Format(@"{0}\{1}{2}.xml", pathProject, DateTime.Now.ToString("ddMMyyyy"), DateTime.Now.Hour);

                if (!Directory.Exists(pathProject))
                    Directory.CreateDirectory(pathProject);
                if (!(File.Exists(pathXML)))
                {
                    StreamWriter strWr;
                    strWr = File.CreateText(pathXML);
                    strWr.WriteLine("<?xml version='1.0' encoding='UTF-8'?>");
                    strWr.WriteLine("<errores>");
                    strWr.WriteLine("</errores>");
                    strWr.Close();
                }
                docXML.Load(pathXML);


                XmlElement nodoMetodo = docXML.CreateElement("metodo");
                XmlElement nodoClase = docXML.CreateElement("clase");
                XmlElement nodoDescripcion = docXML.CreateElement("descripcion");
                XmlElement nodoFecha = docXML.CreateElement("fecha");
                XmlElement nodoOtros = docXML.CreateElement("otrosDatosError");


                nodoMetodo.InnerText = sMetodo;
                nodoClase.InnerText = sClase;
                nodoFecha.InnerText = Convert.ToString(DateTime.Now);
                nodoDescripcion.InnerText = "Message: " + oError.Message + "  StackTrace: " + oError.StackTrace + " InnerException:" + oError.InnerException + " Source: " + oError.Source + " Data: " + oError.Data + " TargetSite: " + oError.TargetSite;
                if (!string.IsNullOrEmpty(sDatos))
                    sDatos = sDatos.Replace("&", "&amp;").Replace("\"", "&quot;").Replace("<", "&lt;").Replace(">", "&gt;");

                nodoOtros.InnerText = sDatos;
                XmlNode nuevoError = docXML.DocumentElement;
                nuevoError = docXML.CreateElement("error");
                nuevoError.AppendChild(nodoFecha);
                nuevoError.AppendChild(nodoClase);
                nuevoError.AppendChild(nodoMetodo);
                nuevoError.AppendChild(nodoDescripcion);
                nuevoError.AppendChild(nodoOtros);
                docXML.DocumentElement.AppendChild(nuevoError);
                docXML.Save(pathXML);
            }
            catch
            {

            }
        }

        public string ArchivoConfiguracion(String sServerDB, String sUserDB, String sPassDB, String sServerSAP, String sUserSAP, String sPassSAP, String sTipoBD, String url, String apikey="")
        {
            #region Comentarios
            /**
             * Comentarios: Metodo encargado de crear/editar archivo de configuracion XML
             * para utilizar propiedades de conexion y/o informativas dentro del proyecto             
             * Autor: Carlos Santiago
             **/
            #endregion

            string resultado = "";
            try
            {
                XmlDocument docXML = new XmlDocument();
                String ruta = AppDomain.CurrentDomain.BaseDirectory + "configuracion.xml";

                if (Directory.Exists(AppDomain.CurrentDomain.BaseDirectory))
                {
                    String[] archivos = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "" + "*" + ".xml");
                    foreach (String nombreArchivo in archivos)
                        if (nombreArchivo.Contains("configuracion"))
                            File.Delete(nombreArchivo);
                }

                if (!(File.Exists(ruta)))
                {
                    StreamWriter strWr;
                    strWr = File.CreateText(ruta);
                    strWr.WriteLine("<?xml version='1.0' encoding='UTF-8'?>");
                    strWr.WriteLine("<configuration>");
                    strWr.WriteLine("</configuration>");
                    strWr.Close();
                }
                docXML.Load(ruta);
                XmlElement serverDB = docXML.CreateElement("serverDB");
                XmlElement serverSAP = docXML.CreateElement("serverSAP");
                XmlElement userDB = docXML.CreateElement("userDB");
                XmlElement typeDB = docXML.CreateElement("typeDB");
                XmlElement userSAP = docXML.CreateElement("userSAP");
                XmlElement passwdDB = docXML.CreateElement("passwdDB");
                XmlElement passwdSAP = docXML.CreateElement("passwdSAP");

                XmlElement urlAPI = docXML.CreateElement("urlApi");
                XmlElement APIKey = docXML.CreateElement("apiKey");

                serverDB.InnerText = sServerDB;
                serverSAP.InnerText = sServerSAP;
                userDB.InnerText = sUserDB;
                typeDB.InnerText = sTipoBD;
                userSAP.InnerText = sUserSAP;
                passwdDB.InnerText = sPassDB;
                passwdSAP.InnerText = sPassSAP;

                urlAPI.InnerText = url;
                APIKey.InnerText = apikey;



                XmlNode DB = docXML.DocumentElement;
                XmlNode SAP = docXML.DocumentElement;
                XmlNode SERVER = docXML.DocumentElement;

                DB = docXML.CreateElement("DataBase");
                DB.AppendChild(serverDB);
                DB.AppendChild(userDB);
                DB.AppendChild(passwdDB);
                DB.AppendChild(typeDB);
                SAP = docXML.CreateElement("SAP");
                SAP.AppendChild(serverSAP);
                SAP.AppendChild(userSAP);
                SAP.AppendChild(passwdSAP);

                SERVER = docXML.CreateElement("API");
                SERVER.AppendChild(urlAPI);
                SERVER.AppendChild(APIKey);



                docXML.DocumentElement.AppendChild(DB);
                docXML.DocumentElement.AppendChild(SAP);
                docXML.DocumentElement.AppendChild(SERVER);

                docXML.Save(ruta);
                resultado = "OK";
            }
            catch (Exception ex)
            {
                resultado = "ERROR: " + ex.Message;
                GuardarLogError(ex, "creaXMLConfiguracion", "tools.cs", "Configuracion");
            }
            return resultado;

        }

        public Boolean LeerConfiguracion()
        {
            Boolean bContinuar = true;
            XmlDocument xmlDoc = new XmlDocument();
            CifradoRijndael oCifrar = new CifradoRijndael();
            try
            {
                String ruta = AppDomain.CurrentDomain.BaseDirectory + "configuracion.xml";

                if (File.Exists(ruta))
                {
                    xmlDoc.Load(ruta);
                    XmlNodeList nodos = xmlDoc.GetElementsByTagName("configuration");
                    XmlNodeList nodosDB = ((XmlElement)nodos[0]).GetElementsByTagName("DataBase");
                    XmlNodeList nodosSAP = ((XmlElement)nodos[0]).GetElementsByTagName("SAP");
                    XmlNodeList nodosSERVICE = ((XmlElement)nodos[0]).GetElementsByTagName("API");
                    XmlNodeList userDB, serverDB, passDB, typeDB;
                    XmlNodeList userSAP, serverSAP, passSAP;
                    XmlNodeList URL, APiKey;
                    userDB = ((XmlElement)nodosDB[0]).GetElementsByTagName("userDB");
                    serverDB = ((XmlElement)nodosDB[0]).GetElementsByTagName("serverDB");
                    passDB = ((XmlElement)nodosDB[0]).GetElementsByTagName("passwdDB");
                    typeDB = ((XmlElement)nodosDB[0]).GetElementsByTagName("typeDB");
                    userSAP = ((XmlElement)nodosSAP[0]).GetElementsByTagName("userSAP");
                    serverSAP = ((XmlElement)nodosSAP[0]).GetElementsByTagName("serverSAP");
                    passSAP = ((XmlElement)nodosSAP[0]).GetElementsByTagName("passwdSAP");

                    URL = ((XmlElement)nodosSERVICE[0]).GetElementsByTagName("urlApi");
                    APiKey = ((XmlElement)nodosSERVICE[0]).GetElementsByTagName("apiKey");

                    vskf_Globales.ServerName = oCifrar.DecryptRijndael(serverDB[0].InnerText);
                    vskf_Globales.UserName = oCifrar.DecryptRijndael(userDB[0].InnerText);
                    vskf_Globales.PassWordUser = oCifrar.DecryptRijndael(passDB[0].InnerText);
                    vskf_Globales.ServidorLicencia = oCifrar.DecryptRijndael(serverSAP[0].InnerText);
                    vskf_Globales.UsuarioSAP = oCifrar.DecryptRijndael(userSAP[0].InnerText);
                    vskf_Globales.PassWordSAP = oCifrar.DecryptRijndael(passSAP[0].InnerText);
                    vskf_Globales.TipoServidor = oCifrar.DecryptRijndael(typeDB[0].InnerText);

                    vskf_Globales.ServiceAPI = oCifrar.DecryptRijndael(URL[0].InnerText);
                    //vskf_Globales.ApiKey = oCifrar.DecryptRijndael(APiKey[0].InnerText);

                    if (vskf_Globales.TipoServidor == "dst_HANADB")
                        vskf_Globales.CadenaConexion = string.Format(@"DRIVER={{0}};UID={1};PWD={2};SERVERNODE={3};DATABASENAME=NDB", "HDBODBC", vskf_Globales.UserName.Trim(), vskf_Globales.PassWordUser.Trim(), vskf_Globales.ServerName.Trim());
                    else
                    {
                        vskf_Globales.CadenaConexion = string.Format("Data Source={0};User ID={1};Password={2}", vskf_Globales.ServerName, vskf_Globales.UserName, vskf_Globales.PassWordUser);
                        vskf_Globales.bSqlConnection = true;
                    }
                        


                }
                else throw new Exception("El archivo de configuracion no existe dentro del directorio.");

            }
            catch (Exception ex)
            {
                bContinuar = false;
                GuardarLogError(ex, "LeerConfiguracion", "Tools.cs", "Configuracion");
            }
            return bContinuar;
        }

        public void guardarLogProceso(String nombre, String descripcion, String metodo, String clase, String sModulo, string sRazonSocial)
        {


            try
            {
                XmlDocument docXML = new XmlDocument();
                String pathProject = AppDomain.CurrentDomain.BaseDirectory + "\\Proceso";
                if (sRazonSocial != "")
                    pathProject += "\\" + sRazonSocial;
                String rutaProcesosXML = pathProject + "\\" + sModulo + "_" + DateTime.Now.ToString("ddMMyyyyHH") + ".xml";
                if (!Directory.Exists(pathProject))
                    Directory.CreateDirectory(pathProject);


                if (!(File.Exists(rutaProcesosXML)))
                {
                    StreamWriter strWr;
                    strWr = File.CreateText(rutaProcesosXML);
                    strWr.WriteLine("<?xml version='1.0' encoding='UTF-8'?>");
                    strWr.WriteLine("<procesos>");
                    strWr.WriteLine("</procesos>");
                    strWr.Close();
                }
                docXML.Load(rutaProcesosXML);


                XmlElement nodoMetodo = docXML.CreateElement("metodo");
                XmlElement nodoClase = docXML.CreateElement("clase");
                XmlElement nodoDescripcion = docXML.CreateElement("descripcion");
                XmlElement nodoFecha = docXML.CreateElement("fecha");

                nodoMetodo.InnerText = clearStringXML(metodo);
                nodoClase.InnerText = clase;
                nodoFecha.InnerText = Convert.ToString(DateTime.Now);
                nodoDescripcion.InnerText = clearStringXML(descripcion);

                XmlNode nuevoProceso = docXML.DocumentElement;
                nuevoProceso = docXML.CreateElement("proceso");
                nuevoProceso.AppendChild(nodoFecha);
                nuevoProceso.AppendChild(nodoClase);
                nuevoProceso.AppendChild(nodoMetodo);
                nuevoProceso.AppendChild(nodoDescripcion);
                docXML.DocumentElement.AppendChild(nuevoProceso);
                docXML.Save(rutaProcesosXML);
            }
            catch (Exception ex)
            {
                this.GuardarLogError(ex, "guardarLogProceso", "vskf_Tools.cs", "", sRazonSocial: sRazonSocial);
            }
        }

        public String GuardaCatalogos(List<vskf_Catalogos_INFO> Catalogos)
        {
            String resultado = "ERROR";
            try
            {
                string jSonString = JsonConvert.SerializeObject(Catalogos);
                string sPathFile = string.Format(@"{0}\{1}", AppDomain.CurrentDomain.BaseDirectory, "catalogosvalidos.json");

                if (!File.Exists(sPathFile))
                    File.Delete(sPathFile);
                System.IO.File.WriteAllText(sPathFile, jSonString);
                resultado = "OK";
            }
            catch (Exception ex)
            {
                GuardarLogError(ex, "GuardaCatalogos", "vskf_Tools.cs", "GuardarCatalogos");
                resultado = "ERROR: "+ex.Message;
            }

            return resultado;
        }

        private string clearStringXML(string sTexto)
        {
            #region Comentarios
            /*Comentarios: Metodo que reemplaza caracteres especiales
             * de la cadena #sTexto# obtenida como parametro             
             *Autor: Carlos Santiago
             */
            #endregion
            return sTexto.Replace("&", "&amp;").Replace("\"", "&quot;").Replace("<", "&lt;").Replace(">", "&gt;");
        }

        public Boolean ValidarURL(string URL)
        {
            #region Comentarios
            /**
             * Comentarios: Metodo encargado de validar formato de url             
             * Autor: Carlos Santiago
             */
            #endregion
            string pattern = @"^(http|https|ftp|)\://|[a-zA-Z0-9\-\.]+\.[a-zA-Z](:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*[^\.\,\)\(\s]$";
            Regex reg = new Regex(pattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);

            return reg.IsMatch(URL);
        }

        public Boolean buscaCatalgos()
        {
            Boolean bContinuar = false;
            String pathCatalogo = "";
            try
            {
                 pathCatalogo = string.Format(@"{0}\catalogosvalidos.json", AppDomain.CurrentDomain.BaseDirectory);
                bContinuar = File.Exists(pathCatalogo);
                if(bContinuar)
                vskf_Globales.Catalogos = JsonConvert.DeserializeObject<List<vskf_Catalogos_INFO>>(System.IO.File.ReadAllText(pathCatalogo));

            }
            catch (Exception ex)
            {
                if (File.Exists(pathCatalogo))
                    File.Delete(pathCatalogo);
                GuardarLogError(ex, "buscaCatalgos", "Tools.cs", "Inicializar");
            }

            return bContinuar;
        }

        public Boolean bConnectionTest(String sURLEndPoint, String sRazonSocial = "")
        {
            #region Comentarios
            /*Comentarios: Metodo que realiza peticiones de conexion
             * para validar la disponibilidad de punto final              
             * Autor: Carlos Santiago
             */
            #endregion
            Boolean bContinuar = false;

            string Message = string.Empty;

            HttpWebResponse oResponse = null;
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(sURLEndPoint);
            request.Credentials = System.Net.CredentialCache.DefaultCredentials;
            request.Method = "GET";

            try
            {
                oResponse = (HttpWebResponse)request.GetResponse();
                if (oResponse.StatusCode == HttpStatusCode.OK || oResponse.StatusCode == HttpStatusCode.Accepted)
                    bContinuar = true;
            }
            catch (WebException ex)
            {
                try
                {
                    throw new Exception(string.Format("Servidor no disponible: ERROR {0}", ex.Message));
                }
                catch (Exception oEx)
                {
                    GuardarLogError(oEx, "bConnectionTest", "Tools.cs", "ConexionWEB", "", sRazonSocial);
                }

            }


            return bContinuar;
        }

        public  bool CertificateValidationCallBack(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {

            if (sslPolicyErrors == System.Net.Security.SslPolicyErrors.None)
                return true;

            if ((sslPolicyErrors & System.Net.Security.SslPolicyErrors.RemoteCertificateChainErrors) != 0)
            {
                if (chain != null && chain.ChainStatus != null)
                {
                    foreach (System.Security.Cryptography.X509Certificates.X509ChainStatus status in chain.ChainStatus)
                    {
                        if ((certificate.Subject == certificate.Issuer) &&
                           (status.Status == System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.UntrustedRoot))
                            continue;
                        else
                            if (status.Status != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
                                return false;
                    }
                }

                return true;
            }
            else
                return false;
        }

        #endregion
    }
}
