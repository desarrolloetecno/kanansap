﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.Motor.Alertas
{
    public class vskf_Settings
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public Char cSucursal { get; set; }
        public Char cVehiculo { get; set; }
        public Char cCliente { get; set; }
        public string sTipoAnticipo { get; set; }
        public string Sucursal { get; set; }
        public string Vehiculo { get; set; }
        public string Cliente { get; set; }
        public Char cSBO { get; set; }
        public Char cKANAN { get; set; }
        public Char cAlmacen { get; set; }
        public Char cSSL { get; set; }
        public string Almacen { get; set; }
        public string Cuenta { get; set; }

        public string CuentaServicioCP { get; set; }//guarda la cuenta para servicios de carta de porte
        public string CuentaAnticipo { get; set; }
        public string CuentaContrapartida { get; set; }
        public string Borrador { get; set; }

        public string Articulo { get; set; }
        public Int32 DimCode { get; set; }

        public String Server { get; set; }
        public Int32 Puerto { get; set; }
        public String CorreoCuenta { get; set; }
        public String PassWord { get; set; }
        public string EmpresaID { get; set; }
    }
}
