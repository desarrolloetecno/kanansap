﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KananSAP.Motor.Alertas
{
    public class ConexionSAP
    {
        #region propiedades
        public Recordset oRecordSet;
        public GeneralService oServices;
        private static Herramientas oTool = new Herramientas();
        public static bool bConectado, _statusconnection = false;
        public static SAPbobsCOM.Company oCompany = null;
        public static string ultimomensaje = string.Empty;
        public static int lRetCode;
        #endregion

        #region Constantes
        private const string Modulo = "ConexionDIAPI";
        private const string Clase = "kanan_SAPConexion.cs";
        #endregion

        #region Metodos
        public static Boolean inicializaDIAPI(string sCompany, out string sMessage)
        {
            Boolean bContinuar = false;
            oCompany = new SAPbobsCOM.Company();
            sMessage = "";

            System.Console.WriteLine("Obteniendo parámetros de conexión.");

            oTool.guardarLogProceso("Conectar DIAPI", sMessage, "inicializaDIAPI", Clase, Modulo, sCompany);
            System.GC.Collect();
            try
            {
                SAPbobsCOM.BoDataServerTypes boType = 0;
                switch (vskf_Globales.TipoServidor)
                {
                    case "dst_MSSQL2005":
                        boType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2005;
                        break;

                    case "dst_MSSQL2008":
                        boType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008;
                        break;

                    case "dst_MSSQL2012":
                        boType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012;
                        break;
                    case "dst_MSSQL2014":
                        boType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014;
                        break;
                    case "dst_MSSQL2016":
                        boType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2016;
                        break;
                    case "dst_MSSQL2017":
                        boType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2017;
                        break;

                    case "dst_HANADB":
                        boType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;
                        break;
                }

                oCompany.Server = vskf_Globales.ServerName;
                oCompany.language = SAPbobsCOM.BoSuppLangs.ln_English;
                oCompany.UseTrusted = false;
                oCompany.UserName = vskf_Globales.UsuarioSAP;
                oCompany.Password = vskf_Globales.PassWordSAP;
                oCompany.DbServerType = boType;
                System.Console.WriteLine("Tipo de BD: " + boType);
                oCompany.CompanyDB = sCompany;
                System.Console.WriteLine("Sociedad: " + sCompany);
                oCompany.DbUserName = vskf_Globales.UserName;
                oCompany.DbPassword = vskf_Globales.PassWordUser;
                oCompany.LicenseServer = vskf_Globales.ServidorLicencia;

                System.Console.WriteLine("Intentando conectar...");
                lRetCode = oCompany.Connect();

                if (lRetCode == 0)
                {
                    bContinuar = true;
                    bConectado = true;
                    sMessage = "Conectado a SAP";
                    System.Console.WriteLine("Conexión establecida.");
                }
                else
                {
                    int iError = 0;
                    bConectado = false;
                    oCompany.GetLastError(out iError, out ultimomensaje);
                    sMessage = ultimomensaje;
                    System.Console.WriteLine("No fue posible estableser la conexión. ");
                }

            }
            catch (Exception ex)
            {
                bContinuar = false;
                oTool.GuardarLogError(ex, "inicializaDIAPI", Clase, Modulo, sRazonSocial: sCompany);
            }
            return bContinuar;
        }

        public static Boolean finalizarDIAPI(string sCompany)
        {
            Boolean bContinuar = false;
            try
            {
                oTool.guardarLogProceso("Desconectar DIAPI", "", "finalizarDIAPI", Clase, Modulo, sCompany);
                oCompany.Disconnect();
                ultimomensaje = oCompany.GetLastErrorDescription();
                bConectado = false;
                System.GC.Collect();
            }
            catch (Exception ex)
            {
                oTool.GuardarLogError(ex, "finalizarDIAPI", Clase, Modulo);
                System.GC.Collect();
            }

            return bContinuar;
        }

        public void InitTransaction()
        {
            ConexionSAP.oCompany.StartTransaction();
        }

        public void RollBack()
        {
            if (ConexionSAP.oCompany.InTransaction)
                ConexionSAP.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
        }

        public void CommintTrans()
        {
            if (ConexionSAP.oCompany.InTransaction)
                ConexionSAP.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
        }
        #endregion
    }
}